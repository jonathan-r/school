import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

class BracketRemover {
	private static final int LIT = -2;
	private static final int BLANK = -1;
	private static final int OP_ADD = 0;
	private static final int OP_SUB = 1;
	private static final int OP_DIV = 2;
	private static final int OP_MUL = 3;
	private static final int OP_EXP = 4;
	private static final int OP_LB = 5; //(
	private static final int OP_RB = 6; //)
	
	private static final char[] OP_SYMBOLS = {'+', '-', '/', '*', '^', '(', ')'};

	public static String removeBrackets(String expr) {
		//tokenize the expression
		ArrayList<String> literals = new ArrayList<String>();
		ArrayList<Integer> tokens = new ArrayList<Integer>();
		String literal = "";

		for (int i = 0; i < expr.length(); i++){
			char c = expr.charAt(i);

			if (literal.length() > 0 && !Character.isLetterOrDigit(c)){
				//end of a literal or number
				literals.add(literal);
				tokens.add(LIT);
				literal = "";
			}

			else switch(c){
				case ' ': continue;
				case ')': tokens.add(OP_RB); break;
				case '(': tokens.add(OP_LB); break;
				case '^': tokens.add(OP_EXP); break;
				case '+': tokens.add(OP_ADD); break;
				case '-': tokens.add(OP_SUB); break;
				case '*': tokens.add(OP_MUL); break;
				case '/': tokens.add(OP_DIV); break;
				default: literal += c;
			}
		}
		
		int op_before = BLANK; //nothing before
		int op_after = BLANK; //nothing after
		
		for (int i = 0; i < tokens.size(); i++){
			if (tokens.get(i) == OP_LB){
				int bias = 1;
				for (int j = i + 1; j < tokens.size(); j++){
					if (tokens.get(i) == OP_LB) bias++;
					else if (tokens.get(i) == OP_RB) bias--;
					if (bias == 0){
						//found the matching right brace
						
						//now find the next operator after the brace
						op_after = BLANK;
						if (j < tokens.size() - 1) if (tokens.get(j + 1) >= 0) op_after = tokens.get(j + 1);
						
						int op = (op_before > op_after)? op_before : op_after;
						
						if (op_before == BLANK || op_after == BLANK){
							if (op_before == op_after){
								//there are useless braces around the entire expression
								tokens.remove(i);
								tokens.remove(j);
							}
							
						}
					}
				}
			}
		}
	}

	public static void main(String[] args) throws java.io.IOException{
		System.out.println("enter the expresison");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String expr = br.readLine();
		System.out.println(removeBrackets(expr));
	}
}
