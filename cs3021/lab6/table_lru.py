from base import BaseCache

class TableLruCache(BaseCache):
	"""Cache with bitfield-table least-recently-used replacement policy."""
	def __init__(self, L, N, K, addr_bits):
		BaseCache.__init__(self, L, N, K, addr_bits)

	def on_reset(self):
		#K by K bit fields for every line
		self.table = [[[False for x in range(self.K)] for y in range(self.K)] \
			for j in range(self.N)]

	def update_table(self, set_id, way_id):
		#set row to all 1s
		for i in range(self.K):
			self.table[set_id][way_id][i] = True

		#set column to all 0s
		for j in range(self.K):
			self.table[set_id][j][way_id] = False

	def on_hit(self, tag_id, set_id, way_id):
		self.update_table(set_id, way_id)

	#returns which way to place the data
	def on_miss(self, tag_id, set_id):
		if self.K == 1:
			return 0

		else:
			way_id = -1
			for j in range(self.K):
				if not any(self.table[set_id][j]):
					way_id = j

			assert way_id != -1
			self.update_table(set_id, way_id)
			return way_id

	def show_extra_info_column_name(self):
		return 'lru table'

	def show_extra_info(self, set_id):
		#table; sadly must be flattened to a line
		return ''#, '.join(map(lambda x : ''.join(x), self.table[set_id]))