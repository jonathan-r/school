N <- 5000

wait <- rexp(n=N, rate=1/5);
serv <- rnorm(n=N, mean=10, sd=2);
total <- wait + serv;

hist(total);
summary(total);

long_wait <- length(which(total > 24));
print(long_wait / N);
