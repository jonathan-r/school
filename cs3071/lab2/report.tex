\documentclass[12pt,article]{article}
\usepackage[hmargin=1cm,vmargin=1.5cm]{geometry}
\usepackage[romanian]{babel}
\usepackage{combelow}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{fancyvrb}

\begin{document}
\begin{center}
{\large \bf CS3071}\\
{\bf  Exercise 2}\\ 
\vspace{0.25 cm}
\small{\textbf{Jonathan Ro\cb{s}ca}}\\
\small{\textbf{12306261}}\\
\end{center}

\section{Overview}
This is a lexical analyser that validates and parses numeric literals of the pattern
\begin{center}
$([+-]?[0-9]+) | ([0-7]+[bB]) | ([0-9a-fA-F]+[hH])$,
\end{center}
where each group corresponds to decimal(base 10), octal(base 8) and hexadecimal(base 16), respsectively.
The values accepted have a limited range, as the system is designed for a 32-bit system.

\section{Analysis}
The input alphabet consists of the characters $\{+, -, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f, A, B, C, D, E, F, h, H\}$. Decimal characters $0$ to $9$ overlap with octal characters $0$ to $7$. $b$ and $B$ can be either hexadecimal digits, or the octal terminator. Hence it is hard to tell what base (valid) input is in, before the very last character. However, octal and hexadecimal can be ruled out if a sign is encountered at the start.

\section{Design}
The lexical analyser works in two passes. 
\subsection{Validator}
Semantically indentical characters from the input are transliterated into unique symbols.
\begin{center}
	\begin{tabular}{ | c | c | c | c | c | c | c | c |}
		\hline
		Character range & +, -  & 0 - 7 & 8, 9 & a, c - f, A, C - F & b, B & h, H & any other \\
		\hline
		Symbol          &   s   &   l   &   u  &         x          &  b   &   h  &  $\dashv$ \\
		\hline
	\end{tabular}
\end{center}


A finite state acceptor is used to validate the input. It uses the following transition table, where blank cells mean transitions to error states. Also shown, on the right, is what base the input can still be at that stage.
\begin{center}
	\begin{tabular}{ | c | c | c | c | c | c | c | c || c | c | c | }
		\hline
		State & s & l & u & x & b & h & $\dashv$      & Decimal &  Octal  & Hex\\
		\hline
		  0   & 3 & 1 & 2 & 6 & 6 &   &               & $\surd$ & $\surd$ & $\surd$\\
		\hline
		  1   &   & 1 & 2 & 6 & 5 & 7 & valid decimal & $\surd$ & $\surd$ & $\surd$\\
		\hline
		  2   &   & 2 & 2 & 6 & 6 & 7 & valid decimal & $\surd$ &         & $\surd$\\
		\hline
		  3   &   & 4 & 4 &   &   &   &               & $\surd$ &         &        \\
		\hline
		  4   &   & 4 & 4 &   &   &   & valid decimal & $\surd$ &         &        \\
		\hline
		  5   &   & 6 & 6 & 6 & 6 & 7 & valid octal   &         & $\surd$ & $\surd$\\
		\hline
		  6   &   & 6 & 6 & 6 & 6 & 7 &               &         &         & $\surd$\\
		\hline
		  7   &   &   &   &   &   &   & valid hex     &         &         & $\surd$\\
		\hline
	\end{tabular}
\end{center}

Little computation is done apart from state transitions in this stage. The value of the sign, and the number of digits are kept track of. A valid exit transition will also determine the base of the input numeral.

\begin{center}
	\begin{tabular}{ | c | c | }
		\hline
		Transition to state & Action  \\
		\hline
		  initial       & $sign \gets 1$, $digits \gets 0$, $first\_digit \gets 0$ \\
		\hline
		  4             & $sign \gets -1$ if input is `-', else $1$, $first\_digit \gets first\_digit + 1$ \\
		\hline
		  valid decimal & $input\_type \gets$ ``decimal'' \\
		\hline
		  valid octal   & $input\_type \gets$ ``octal'', $digits \gets digits - 1$ \\
		\hline
		  valid hex     & $input\_type \gets$ ``hexadecimal'', $digits \gets digits - 1$ \\
		\hline
		  any other non-error state & $digits \gets digits + 1$ if current input is not $0$, else $first\_digit \gets first\_digit + 1$ \\
		\hline
	\end{tabular}
\end{center}

\subsection{Parser}
The number of characters is checked against a known maximum value in the source representation. If there are exactly as many characters in the input as in the known maximum value, the digits (excluding leading zeros) are compared lexicographically against the maximum value (only for decimal and octal). If both of these tests pass, the parser can be certain the input will not overflow, and proceeds to parse the input character by character into a binary value.


\section{Implementation}
The system described above is implemented as a C library. It does not depend on any architecture or operating system specific functionality. All functionality is exposed through the $parse$ function, the prototype of which is found in the header file $literal\_lexer.h$.

The finite state machine is implemented as an array of state transitions. After each transition the actions described above are done, to learn more about the value being parsed.

An attempt was made to create useful error messages for every error state. When an error is encountered, the place it occurred and a description of the error (as a string) are optionally passable back to the caller. 

Due to the ambigous nature of the input language, specific error messages are not always possible. Take, for instance, the input ``23B5''. The validator will detect an error on the character $5$, but it is not possible to tell whether the input was meant as a hexadecimal value (and is thus missing the terminator), or an octal value (so the $5$ is extraneous).


\section{Testing}
A small command line testing program accompanies the parser library. It takes one argument, the input to be parsed. It calls the parsing routing, then displays, if the input is valid, the name of the base of the input, the value of the input in the original base as well as decimal. If an error was encountered, it points where, and displays the error message. The testing program was compiled into a binary called $test$.

\subsection{Unit tests}
This is a collection of tests designed to make the validator go through every possible transition for valid input, found in the file $tests.sh$. The following is the terminal output from running the script. Included are tests of the lowest and highest values possible for every base. Note how the input ``2147483648'' leads to overflow, even though the input ``-2147483648'' does not.

\begin{lstlisting}
trying 239854
decimal 239854

trying 99
decimal 99

trying -2399
decimal -2399

trying -9932
decimal -9932

trying 12B
octal 12B; value in decimal: 10

trying 19fdbb92h
hexadecimal 19fdbb92H; value in decimal: 436059026

trying 7bfH
hexadecimal 7bfH; value in decimal: 1983

trying 4dH
hexadecimal 4dH; value in decimal: 77

trying 9aH
hexadecimal 9aH; value in decimal: 154

trying 1h
hexadecimal 1H; value in decimal: 1

trying 8H
hexadecimal 8H; value in decimal: 8

trying eh
hexadecimal eH; value in decimal: 14

trying bH
hexadecimal bH; value in decimal: 11

trying 2147483647
decimal 2147483647

trying 2147483648
2147483648
^
error: Numerical overflow

trying -2147483648
decimal -2147483648

trying -000000002147483648
decimal -2147483648

trying 37777777777b
octal 37777777777B; value in decimal: 4294967295

trying FFFFFFFFh
hexadecimal ffffffffH; value in decimal: 4294967295

trying 0h
hexadecimal 0H; value in decimal: 0

trying 0b
octal 0B; value in decimal: 0
\end{lstlisting}

\end{document}
