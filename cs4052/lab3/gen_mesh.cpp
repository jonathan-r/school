#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <vector>

#include "vertex.h"
#include "maths/vec.h"
#include "gen_mesh.h"

using namespace std;

void generateIcoSphere(int iter, vector<vertex>& iso) {
	//algorithm from http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
	assert(iso.size() == 0);

	//generate isosphere
	vec3 cc1(1.0f, 0.5f, 0.5f);
	vec3 cc2(0.5f, 1.0f, 0.5f);
	vec3 cc3(0.5f, 0.5f, 1.0f);
	vec3  nn(0.0f, 0.0f, 0.0f);

	vector<vertex> isoInit;
	vector<int> isoFace;
	float t = (1.0f + sqrtf(5.0)) / 2.0f;
	isoInit.emplace_back(vec3(-1.0f,  t, 0.0f), nn, cc1);
	isoInit.emplace_back(vec3( 1.0f,  t, 0.0f), nn, cc1);
	isoInit.emplace_back(vec3(-1.0f, -t, 0.0f), nn, cc1);
	isoInit.emplace_back(vec3( 1.0f, -t, 0.0f), nn, cc1);

	isoInit.emplace_back(vec3( 0.0f, -1.0f,  t), nn, cc2);
	isoInit.emplace_back(vec3( 0.0f,  1.0f,  t), nn, cc2);
	isoInit.emplace_back(vec3( 0.0f, -1.0f, -t), nn, cc2);
	isoInit.emplace_back(vec3( 0.0f,  1.0f, -t), nn, cc2);
	
	isoInit.emplace_back(vec3( t, 0.0f, -1.0f), nn, cc3);
	isoInit.emplace_back(vec3( t, 0.0f,  1.0f), nn, cc3);
	isoInit.emplace_back(vec3(-t, 0.0f, -1.0f), nn, cc3);
	isoInit.emplace_back(vec3(-t, 0.0f,  1.0f), nn, cc3);

	#define ADD_FACE_IDX(a, b, c) { isoFace.push_back(a); isoFace.push_back(b); isoFace.push_back(c); }
	
	ADD_FACE_IDX(0, 11, 5);
	ADD_FACE_IDX(0, 5, 1);
	ADD_FACE_IDX(0, 1, 7);
	ADD_FACE_IDX(0, 7, 10);
	ADD_FACE_IDX(0, 10, 11);

	// 5 adjacent faces 
	ADD_FACE_IDX(1, 5, 9);
	ADD_FACE_IDX(5, 11, 4);
	ADD_FACE_IDX(11, 10, 2);
	ADD_FACE_IDX(10, 7, 6);
	ADD_FACE_IDX(7, 1, 8);

	// 5 faces around point 3
	ADD_FACE_IDX(3, 9, 4);
	ADD_FACE_IDX(3, 4, 2);
	ADD_FACE_IDX(3, 2, 6);
	ADD_FACE_IDX(3, 6, 8);
	ADD_FACE_IDX(3, 8, 9);

	// 5 adjacent faces 
	ADD_FACE_IDX(4, 9, 5);
	ADD_FACE_IDX(2, 4, 11);
	ADD_FACE_IDX(6, 2, 10);
	ADD_FACE_IDX(8, 6, 7);
	ADD_FACE_IDX(9, 8, 1);

	//just duplicate vertices, can't be bothered to do indexing
	for (int i = 0; i < isoFace.size(); i += 3) {
		iso.push_back(isoInit[isoFace[i+0]]);
		iso.push_back(isoInit[isoFace[i+1]]);
		iso.push_back(isoInit[isoFace[i+2]]);
	}
	
	for (int i = 0; i < iso.size(); i++) {
		vec3& v = iso[i].v;
		v.normalize();
	}

	for (int deep = 0; deep < iter; deep++) {
		vector<vertex> next;

		for (int i = 0; i < iso.size(); i += 3) {
			vertex& a = iso[i+0];
			vertex& b = iso[i+1];
			vertex& c = iso[i+2];

			vec3 ab = ((a.v + b.v) / 2.0f).normal();
			vec3 ac = ((a.v + c.v) / 2.0f).normal();
			vec3 bc = ((b.v + c.v) / 2.0f).normal();

			vec3 abcc = ((a.c + b.c) / 2.0f);
			vec3 accc = ((a.c + c.c) / 2.0f);
			vec3 bccc = ((b.c + c.c) / 2.0f);

			next.emplace_back(a.v, nn, a.c); next.emplace_back(ab, nn, abcc); next.emplace_back(ac, nn, accc);
			next.emplace_back(ab, nn, abcc); next.emplace_back(b.v, nn, b.c); next.emplace_back(bc, nn, bccc);
			next.emplace_back(ac, nn, accc); next.emplace_back(bc, nn, bccc); next.emplace_back(c.v, nn, c.c);
			next.emplace_back(ac, nn, accc); next.emplace_back(ab, nn, abcc); next.emplace_back(bc, nn, bccc);
		}
		
		//smooth out the sphere
		for (int i = 0; i < iso.size(); i++) {
			vec3& v = iso[i].v;
			v.normalize();
		}
	
		//printf("iter %d, in %d, out %d\n", deep, iso.size(), next.size());
		iso = next;
	}
	
	//normals on final pass only
	for (int i = 0; i < iso.size(); i += 3) {
		vertex& a = iso[i+0];
		vertex& b = iso[i+1];
		vertex& c = iso[i+2];

		vec3 ba = b.v - a.v;
		vec3 ca = c.v - a.v;

		vec3 nor = (ba ^ ca).normal();
		a.n = nor;
		b.n = nor;
		c.n = nor;
	}
}

void generateCylinder(int radial, int vertical, vec3 boundSize, bool closed, std::vector<vertex>& out) {
	vec3 col(1.0f, 1.0f, 1.0f);
	float radFrac = 2 * M_PI / radial;
	float lonFrac = boundSize.y / vertical;
	if (closed) {
		for (int yi = -1; yi < 2; yi += 2) {
			float y = yi * boundSize.y / 2;
			vec3 center = vec3(0.0f, y, 0.0f);
			vec3 up = vec3(0.0f, yi * 1.0f, 0.0f);
			for (int r = 0; r < radial; r++) {
				float a1 = (float)(r+0) * radFrac;
				float a2 = (float)(r+1) * radFrac;
				
				out.emplace_back(vec3(cosf(a1), y, sinf(a1)), up, col);
				out.emplace_back(center, up, col);
				out.emplace_back(vec3(cosf(a2), y, sinf(a2)), up, col);
			}
		}
	}

	for (int v = 0; v < vertical; v++) {
		float y1 = ((float)(v+0) * lonFrac) - boundSize.y/2;
		float y2 = ((float)(v+1) * lonFrac) - boundSize.y/2;

		vec3 up = vec3(0.0f, 1.0f, 0.0f);
		for (int r = 0; r < radial; r++) {
			float a1 = (float)(r+0) * radFrac;
			float a2 = (float)(r+1) * radFrac;

			vec3 x(cosf(a1), y1, sinf(a1));
			vec3 y(cosf(a1), y2, sinf(a1));
			vec3 z(cosf(a2), y1, sinf(a2));
			vec3 w(cosf(a2), y2, sinf(a2));
			vec3 nor = ((y - x) ^ (z - x)).normal();

			out.emplace_back(x, nor, col);
			out.emplace_back(y, nor, col);
			out.emplace_back(z, nor, col);
			out.emplace_back(y, nor, col);
			out.emplace_back(w, nor, col);
			out.emplace_back(z, nor, col);
		}
	}
}
