library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;
 
entity mux8_16bit_tb is
end mux8_16bit_tb;
 
architecture behavior of mux8_16bit_tb is 
	component mux8_16bit
		port(	s : in  std_logic_vector(2 downto 0);
			in_data : in data_array;
			z : out  std_logic_vector(15 downto 0) );
	end component;
    
	signal s_in : std_logic_vector(2 downto 0) := (others => '0');
	signal data_in : data_array := (others => "0000000000000000");
	signal z_out : std_logic_vector(15 downto 0);
	
	constant test_data : std_logic_vector(15 downto 0) := "0101011001011010";

begin
	uut: mux8_16bit port map (s_in, data_in, z_out);

	stim_proc: process
	begin
		wait for 1 ns;
		--same value on all
		for i in 0 to 7 loop
			data_in(i) <= test_data;
			s_in <= std_logic_vector(to_unsigned(i, 3));
			wait for 1 ns;
			assert z_out = test_data;
		end loop;

		--unique value on each one
		data_in(0) <= x"0123";
		data_in(1) <= x"F12A";
		data_in(2) <= x"E12B";
		data_in(3) <= x"D12C";
		data_in(4) <= x"C12D";
		data_in(5) <= x"B12E";
		data_in(6) <= x"A12F";
		data_in(7) <= x"9120";

		s_in <= "000";
		wait for 1 ns;
		assert z_out = x"0123";

		s_in <= "001";
		wait for 1 ns;
		assert z_out = x"F12A";

		s_in <= "010";
		wait for 1 ns;
		assert z_out = x"E12B";

		s_in <= "011";
		wait for 1 ns;
		assert z_out = x"D12C";

		s_in <= "100";
		wait for 1 ns;
		assert z_out = x"C12D";

		s_in <= "101";
		wait for 1 ns;
		assert z_out = x"B12E";

		s_in <= "110";
		wait for 1 ns;
		assert z_out = x"A12F";

		s_in <= "111";
		wait for 1 ns;
		assert z_out = x"9120";
	end process;
end;
