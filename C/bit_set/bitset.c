#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "bitset.h"

bitset* bitset_new(){
	bitset* n = malloc(sizeof(bitset));
	memset(n->pages, 0, BITSET_PAGES * sizeof(bitset_page*)); //no page is allocated yet

	return n;
}


void bitset_destroy(bitset* this){
	assert(this);

	int i;
	for (i = 0; i < BITSET_PAGES; i++){
		if (this->pages[i]){
			free(this->pages[i]);
		}
	}
	free(this);
}


int bitset_lookup(bitset* this, uint32_t item){
	assert(this);

	int word = item >> BITSET_DATA_BITS; 
	int page_id = word >> BITSET_PAGE_BITS; //which page
	int data_id = word & BITSET_PAGE_MASK; //which data item in the page
	int bit = 1 << (item & BITSET_BIT_MASK); //which bit in the data item

	bitset_page* page = this->pages[page_id];
	if (page){
		//page is allocated contains at least one item
		uint32_t data = page->data[data_id];
		return (data & bit) != 0;
	}

	else return 0;
}


int bitset_add(bitset* this, uint32_t item){
	assert(this);

	int word = item >> BITSET_DATA_BITS; 
	int page_id = word >> BITSET_PAGE_BITS; //which page
	int data_id = word & BITSET_PAGE_MASK; //which data item in the page
	int bit = 1 << (item & BITSET_BIT_MASK); //which bit in the data item

	bitset_page* page = this->pages[page_id];
	if (!page){
		//allocate page
		page = this->pages[page_id] = malloc(sizeof(bitset_page));
		memset(page, 0, BITSET_PAGE_SIZE * sizeof(uint32_t));
		page->elements = 0;
	}

	uint32_t data = page->data[data_id];
	if (data & bit) return 0; //it's there already
	else{
		page->data[data_id] |= bit;
		page->elements++;
		return 1;
	}
}


int bitset_remove(bitset* this, uint32_t item){
	assert(this);

	int word = item >> BITSET_DATA_BITS; 
	int page_id = word >> BITSET_PAGE_BITS; //which page
	int data_id = word & BITSET_PAGE_MASK; //which data item in the page
	int bit = 1 << (item & BITSET_BIT_MASK); //which bit in the data item

	bitset_page* page = this->pages[page_id];
	if (page){
		if ((page->data[data_id] & bit) == 0) return 0; //it's not there
		else{
			//it is there, removing it
			page->data[data_id] &= ~bit;
			page->elements--;

			if (page->elements == 0){
				//that was the last element, so delete the page
				free(page);
				this->pages[page_id] = NULL;
			}

			return 1;
		}
	}

	else return 0; //it's really not there
}


void bitset_union(bitset* dest, bitset* src, bitset* src2){
	assert(dest);
	assert(src);
	assert(src2);

	int i, j;
	for (i = 0; i < BITSET_PAGES; i++){
		bitset_page* a = src->pages[i];
		bitset_page* b = src2->pages[i];
		bitset_page* d = dest->pages[i];

		if (a || b){
			if (!d) d = dest->pages[i] = malloc(sizeof(bitset_page));
			
			if (a && b){
				for (j = 0; j < BITSET_PAGE_SIZE; j++){
					d->data[j] = a->data[j] | b->data[j];
				}
			}
			else{
				bitset_page* p = (bitset_page*)((uintptr_t)a | (uintptr_t)b); //the one that's not NULL
				for (j = 0; j < BITSET_PAGE_SIZE; j++){
					d->data[j] = p->data[j];
				}
			}
		}

		else if (d){
			//this page would be blank anyway
			free(d);
			dest->pages[i] = NULL;
		}
	}
}


void bitset_intersect(bitset* dest, bitset* src, bitset* src2){
	assert(dest);
	assert(src);
	assert(src2);

	int i, j;
	for (i = 0; i < BITSET_PAGES; i++){
		bitset_page* a = src->pages[i];
		bitset_page* b = src2->pages[i];
		bitset_page* d = dest->pages[i];

		if (a && b){
			if (!d) d = dest->pages[i] = malloc(sizeof(bitset_page));
			
			for (j = 0; j < BITSET_PAGE_SIZE; j++){
				d->data[j] = a->data[j] & b->data[j];
			}
		}

		else if (d){
			//this page would be blank anyway
			free(d);
			dest->pages[i] = NULL;
		}
	}
}

