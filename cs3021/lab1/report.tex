\documentclass[12pt,article]{article}
\usepackage[hmargin=1cm,vmargin=1.5cm]{geometry}
\usepackage[romanian]{babel}
\usepackage{combelow}
\usepackage{amsmath}
\usepackage{listings}
\usepackage[nocolor]{drawstack}

\begin{document}
\begin{center}
{\large \bf CS3021}\\
{\bf  Tutorial 1}\\ 
\vspace{0.25 cm}
\small{\textbf{Jonathan Ro\cb{s}ca}}\\
\small{\textbf{12306261}}\\
\end{center}

\section*{IA32 assembly implementation}
The following is a hand made implementation of the functions $p(i, j)$, $q(i)$ and $f(n)$, for use with the gcc compiler on the GNU+Linux operating system. Each function uses the \textit{cdecl} (`C Declaration') calling convention, where every argument is passed on the stack, and the (integer) return variable is left in the $eax$ register upon return.
\lstset{language=[x86masm]Assembler, tabsize=3, basicstyle=\small}%\scriptsize\ttfamily}
\begin{lstlisting}
.file "assembly.s"
	;export a global int "g" and assign it the value 256
	.globl	g
	.data
	.align	4
	.type		g, @object
	.size		g, 4
g:
	.long		256

	;use intel syntax rather than the default at&t
	.intel_syntax noprefix
	.text
	
	;export the functions defined in this file,
	;specifying the type of symbol they are
	.globl	p_asm
	.type		p_asm, @function
	
	.globl	q_asm
	.type		q_asm, @function
	
	.globl	f_asm
	.type		f_asm, @function

;int p(int i, int j);
;computes (i + j) * 4 - 1
p_asm:
	push		ebp             ;save previous stack frame
	                         ;no need for a new stack frame

	mov		eax, [esp + 8]  ;load i; note esp relative
	mov		ecx, [esp + 12] ;load j; ditto
	add		eax, ecx        ;compute i+j
	shl		eax, 2          ;compute (i+j) << 2
	dec		eax             ;and finally ((i+j) << 2) - 1

	pop		ebp             ;restore previous stack frame
	ret                      ;esp is at the return address already

;int q(int i);
;computes p(g, -i)
q_asm:
	push		ebp             ;save previous stack frame
	mov		ebp, esp        ;create a new stack frame

	mov		eax, [ebp + 8]  ;load i arg
	neg		eax             ;compute -i
	push		eax             ;prepare -i as second argument
	mov		eax, g          ;load g; the address will be resolved by the linker
	push		eax             ;prepare g as the first argument
	call		p_asm           ;execute p(g, -i)
	add		esp, 8          ;remove args off the stack

	mov		esp, ebp        ;clear current stack frame, bringing esp to the return addr
	pop		ebp             ;restore previous stack frame
	ret                      ;jump to address pointed to by esp

;int f(int n);
;computes n!, for n > 0
;returns 1 when n <= 0
f_asm:
	push		ebp             ;save previous stack frame
	mov		ebp, esp        ;new stack frame

	mov		eax, [ebp + 8]  ;load n
	test		eax, eax        ;see if it's <= 0
	jle		f_else          ;if so, return 1

	dec		eax             ;compute n-1

	push		eax             ;prepare 1st and only argument (n-1)
	call		f_asm           ;execute f(n-1)
	add		esp, 4          ;clean argument off the stack

	mov		ecx, [ebp + 8]  ;reload n
	imul		ecx             ;compute f(n-1) * n, with the result of f(n-1) being in eax

	jmp		f_ret           ;now f(n-1) * n is in eax

f_else: ;when n <= 0
	xor		eax, eax        ;set to 0
	inc		eax             ;return 1

f_ret: ;common exit
	mov		esp, ebp        ;clear current stack frame, bringing esp to the return addr
	pop		ebp             ;restore previous stack frame
	ret                      ;and return
\end{lstlisting}
\vspace{5 mm}

\section*{Explanation of f(n)}
The function $f(n)$ recursively computes the factorial of a positive integer \textit{n}, that is, the product of all integers from 1 to $n$ (inclusive). The factorial function is not defined for the negative integer range. This implementation returns 1 when $n \leq 0$.

\subsection*{Overview of execution}
The execution of $f(13)$ calls $f(12)$, which in turn calls $f(11)$, which is turn calls $f(10)$. The following is a snapshot of the state of the stack, just inside the call to $f(10)$ from within $f(11)$, before any instructions from $f(10)$ are executed. On the left the stack frames of each $f$ invocation are delimited.

\vspace{5 mm}
\begin{drawstack}
  \cell{$13$ arg} \cellcom{$f(13)$'s $ebp + 8$}

  \startframe %f(13)
  \cell{caller's return addr} \cellcom{$f(13)$'s $ebp + 4$}
  \cell{caller's $ebp$} \cellcom{$f(13)$'s $ebp$}
  \cell{$12$ arg} \cellcom{$f(12)$'s ebp + 8}
  \finishframe{$f(13)$}

  \startframe %f(12)
  \cell{caller's return addr} \cellcom{$f(12)$'s $ebp + 4$}
  \cell{$f(13)$'s $ebp$} \cellcom{$f(12)$'s $ebp$}
  \cell{$11$ arg} \cellcom{$f(11)$'s ebp + 8}
  \finishframe{$f(12)$}

  \startframe %f(11)
  \cell{caller's return addr} \cellcom{$f(11)$'s $ebp + 4$}
  \cell{$f(12)$'s $ebp$} \cellcom{$f(11)$'s $ebp$}
  \cell{$10$ arg} \cellcom{$f(10)$'s ebp + 8}
  \finishframe{$f(11)$}

  \startframe %f(10)
  \cell{caller's return addr} \cellcom{current $esp$}
  \finishframe{$f(10)$}
\end{drawstack}

\section*{Testing platform}
This is a small C++ testing program for the above code. The \textit{cdecl} calling convention is the default, hence it need not be specified in the function prototypes.
\lstset{language=C++}
\begin{lstlisting}
#include <cstdio>

//assembly.s exported symbols
extern "C" int p_asm(int i, int j);
extern "C" int q_asm(int i);
extern "C" int f_asm(int n);
extern "C" int g;

//reference implementations
int p_reference(int i, int j){
	int k = i + j;
	return (k << 2) - 1;
}

int q_reference(int i){
	return p_reference(g, -i);
}

int f_reference(int n){
	if (n > 0){
		return n * f_reference(n - 1);
	}
	else{
		return 1;
	}
}

//poor man's integer logarithm
int leading(int n){
	int space = 0;
	if (n < 0){
		space++;
		n = -(n / 10);
		if (n != 0) space++;
	}
	
	if (n == 0) {
		space++;
	}
	
	while (n > 0){
		space++;
		n /= 10;
	}

	return space;
}

int main(){
	printf("testing p:\n");
	for (int i = -1; i <= 20; i += 4){
		for (int j = -1; j <= 20; j += 4){
			int sample = p_asm(i, j);
			int control = p_reference(i, j);

			printf("i=%*s%d, j=%*s%d: expected %*s%d, got %*s%d; %s\n", 
				2-leading(i), "", i, 2-leading(j), "", j, 4-leading(control), "", 
				control, 4-leading(sample), "", sample, 
				(sample == control)? "match" : "wrong");
		}
	}

	printf("\ntesting q:\n");
	for (int i = -1; i <= 20; i++){
		int sample = q_asm(i);
		int control = q_reference(i);
		printf("i=%*s%d: expected %*s%d, got %*s%d; %s\n", 2-leading(i), 
			"", i, 11-leading(control), "", control, 11-leading(sample), 
			"", sample, (sample == control)? "match" : "wrong");
	}

	printf("\ntesting f:\n");
	for (int i = -1; i <= 20; i++){
		int sample = f_asm(i);
		int control = f_reference(i);
		printf("n=%*s%d: expected %*s%d, got %*s%d; %s\n", 2-leading(i), 
			"", i, 11-leading(control), "", control, 11-leading(sample), 
			"", sample, (sample == control)? "match" : "wrong");
	}
	printf("done\n");
	return 0;
}
\end{lstlisting}

\subsection*{Building}
Compile with \textit{g++ -m32 testing.c assembly.s -o testing}, assuming the testing platform is in a file called \textit{testing.c}. This will produce an IA32 binary called \textit{testing}.

\subsection*{Running}
The testing program yields the following output to the terminal.

\begin{lstlisting}
testing p:
i=-1, j=-1: expected   -9, got   -9; match
i=-1, j= 3: expected    7, got    7; match
i=-1, j= 7: expected   23, got   23; match
i=-1, j=11: expected   39, got   39; match
i=-1, j=15: expected   55, got   55; match
i=-1, j=19: expected   71, got   71; match
i= 3, j=-1: expected    7, got    7; match
i= 3, j= 3: expected   23, got   23; match
<many more such lines>
i=19, j= 7: expected  103, got  103; match
i=19, j=11: expected  119, got  119; match
i=19, j=15: expected  135, got  135; match
i=19, j=19: expected  151, got  151; match

testing q:
i=-1: expected        1027, got        1027; match
i= 0: expected        1023, got        1023; match
i= 1: expected        1019, got        1019; match
i= 2: expected        1015, got        1015; match
i= 3: expected        1011, got        1011; match
i= 4: expected        1007, got        1007; match
i= 5: expected        1003, got        1003; match
i= 6: expected         999, got         999; match
i= 7: expected         995, got         995; match
i= 8: expected         991, got         991; match
i= 9: expected         987, got         987; match
i=10: expected         983, got         983; match
i=11: expected         979, got         979; match
i=12: expected         975, got         975; match
i=13: expected         971, got         971; match
i=14: expected         967, got         967; match
i=15: expected         963, got         963; match
i=16: expected         959, got         959; match
i=17: expected         955, got         955; match
i=18: expected         951, got         951; match
i=19: expected         947, got         947; match
i=20: expected         943, got         943; match

testing f:
n=-1: expected           1, got           1; match
n= 0: expected           1, got           1; match
n= 1: expected           1, got           1; match
n= 2: expected           2, got           2; match
n= 3: expected           6, got           6; match
n= 4: expected          24, got          24; match
n= 5: expected         120, got         120; match
n= 6: expected         720, got         720; match
n= 7: expected        5040, got        5040; match
n= 8: expected       40320, got       40320; match
n= 9: expected      362880, got      362880; match
n=10: expected     3628800, got     3628800; match
n=11: expected    39916800, got    39916800; match
n=12: expected   479001600, got   479001600; match
n=13: expected  1932053504, got  1932053504; match
n=14: expected  1278945280, got  1278945280; match
n=15: expected  2004310016, got  2004310016; match
n=16: expected  2004189184, got  2004189184; match
n=17: expected  -288522240, got  -288522240; match
n=18: expected  -898433024, got  -898433024; match
n=19: expected   109641728, got   109641728; match
n=20: expected -2102132736, got -2102132736; match
done
\end{lstlisting}


\end{document}
