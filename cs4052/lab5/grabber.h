#pragma once

#include <vector>

#include "asset.h"

#include "vertex.h"
#include "object.h"
#include "maths/mat4.h"
#include "maths/quat.h"

enum GrabberState {
	GRABBER_TOO_FAR = 0,
	GRABBER_REACHING,
	GRABBER_REACED,
	GRABBER_IDLE
};

extern const char* GrabberStateStr[];

class Grabber {
public:
	Grabber(int segments, float segLength, vec3 p = vec3(0.0f, 0.0f, 0.0f)) {
		init();

		segs = segments;
		segLen = segLength;
		armLen = segLength * segs;

		joints = new joint[segs+1];
		memset(joints, 0, (segs+1) * sizeof(joint));

		objPool = new QuatObject[segs];
		for (int i = 1; i < segs; i++) {
			objPool[i-1].shoots.push_back(&objPool[i]);
			objPool[i].stem.y = segLength;

			//init position of points, no motion
			joint& j = joints[i];
			j.tangent.set(1, 0, 0); //right side
			j.lastPoint.y = j.point.y = segLength * i;
		}

		//tip
		joints[segs].tangent.set(1, 0, 0); //right side
		joints[segs].lastPoint.y = joints[segs].point.y = segLength * segs;

		objPool[0].stem = p;
		objPool[0].bakeAll();

		state = GRABBER_IDLE;
		target = NULL;
		
		//set assets
		for (int i = 0; i < segs; i++) {
			objPool[i].model = segmentMesh;
			objPool[i].texture = segmentTexture;
		}
	}
	
	Grabber(const Grabber& other); //don't

	~Grabber() {
		delete [] objPool;
		delete [] joints;
	}

	void makeSpineLineStrip(std::vector<vertex>& out) {
		out.clear();
		for (int i = 0; i < segs; i++) { //NB: segs+1 points
			joint& j = joints[i];
			vec3 diff = joints[i+1].point - j.point;
			float ratio = (diff.length() - segLen) / segLen;
			
			//spine itself
			out.emplace_back(j.point,  vec3(1.0f), vec2(0.6f, 1.0f));
			out.emplace_back(j.point + diff, vec3(1.0f), vec2(0.5f, 1.0f));
			
			//rot tangent
			out.emplace_back(j.point, vec3(1.0f), vec2(0.75f, 1.0f));
			out.emplace_back(j.point + j.rotTangent * 3, vec3(1.0f), vec2(0.8f, 1.0f));
			
			//obj tangent
			out.emplace_back(j.point, vec3(1.0f), vec2(0.25f, 1.0f));
			out.emplace_back(j.point + j.tangent * 3, vec3(1.0f), vec2(0.3f, 1.0f));
		}
	}

	vec3& pos() {
		return joints[0].point;
	}
	
	vec3& end() const {
		//return objPool[segs-1].global.getCol(3).xyz;
		return joints[segs].point;
	}

	float distToTarget() {
		rootObj().bakeAll();
		if (target) {
			return (end() - (*target)).length();
		}
		else {
			return 0.0f;
		}
	}

	void update(float dt);
	void setTarget(vec3* at) {
		target = at;
		if (target) {
			float dist = (pos() - *at).length();
			if (dist > armLen) {
				state = GRABBER_TOO_FAR;
			}
			else {
				state = GRABBER_REACHING;
			}
		}
		else {
			state = GRABBER_IDLE;
		}
	}

	const mat4& getMat (int id) const {
		return objPool[id].global;
	}
	
	Object& rootObj() {
		return *((Object*)&objPool[0]);
	}

	static void init() {
		if (!segmentMesh) {
			segmentMesh = loadMesh("grabber_section.obj");
			clawBaseMesh = loadMesh("grabber_claw_base.obj");
			clawLeftMesh = loadMesh("grabber_claw_left.obj");
			clawRightMesh = loadMesh("grabber_claw_right.obj");
			segmentTexture = loadTexture("grabber_section_col.png");
			clawTexture = loadTexture("grabber_claw_col.png");
		}
	}

	static void finalize() {
		delete segmentMesh;
		delete clawBaseMesh;
		delete clawLeftMesh;
		delete clawRightMesh;
		delete segmentTexture;
		delete clawTexture;
	}

private:
	GrabberState state;
	
	//mass-spring IK
	int segs;
	float segLen;
	float armLen;
	vec3* target; //world space

	struct joint {
		vec3 point; //of base, world space
		vec3 lastPoint; //for verlet integrator
		vec3 tangent; //side direction, world space
		vec3 force; //sum of forces, reset every frame
		vec3 rotTangent; //vector rotated on, world space
	};
	joint* joints;

	//display
	QuatObject* objPool; //first one is in world space

	//returns sum of contraction/extension forces
	float simulate(float dt, bool chase);

	//assets
	static Mesh* segmentMesh;
	static Mesh* clawBaseMesh;
	static Mesh* clawLeftMesh;
	static Mesh* clawRightMesh;
	static Texture* segmentTexture;
	static Texture* clawTexture;
};
