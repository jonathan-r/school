#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <unistd.h> //sysconf
#include <pthread.h> //threads

#include "ring.h"

pthread_cond_t wait_signal;
pthread_mutex_t wait_mutex;

//pthread_cont_t wait_put_cond;
//pthread_cont_t wait_put_mutex;

ring_buffer ring;
int chunk_size = 0;
int num_takers = 0;
int turn = 0; //the next taker thread to be woken up when there's data

pthread_mutex_t printf_lock;
void atomic_fprintf(FILE* file, const char* format, ...){
	va_list args;

	pthread_mutex_lock(&printf_lock);

	va_start(args, format);
	vfprintf(file, format, args);
	va_end(args);

	pthread_mutex_unlock(&printf_lock);
}

void atomic_printf_colored(int id, FILE* file, const char* format, ...){
	va_list args;
	char buf[1024];

	pthread_mutex_lock(&printf_lock);

	va_start(args, format);
	vsnprintf(buf, 1024, format, args);
	va_end(args);
	
	//if the message ends in \n, move it past the ansi reset code
	int len = strlen(buf);
	int end = '\0';
	if (buf[len - 1] == '\n'){
		buf[len - 1] = '\0';
		end = '\n';
	}
	
	const int const type_table[] = {10, 1, 2, 3, 4, 7, 9};
	int type = id / (7 * 7);
	int bg = (id % (7 * 7)) / 7;
	int col = (id % (7 * 7)) % 7;
	if (col == bg && col < 6) bg++;
	if (type > 6) type = 6;
	
	fprintf(file, "\x1b[%dm\x1b[%d;%dm%s\x1b[0m%c", 40 + bg, 30 + col, type_table[type], buf, end); //set, message, reset
	fflush(file); //so that the reset code goes through
	pthread_mutex_unlock(&printf_lock);
}

void giver(FILE* input_file){
	const int buf_size = 1024;
	char buf[buf_size];

	while(!feof(input_file)){
		int clearance = ring_space_left(&ring);
		int get = (clearance <= buf_size - 1)? clearance + 1 : buf_size;
		
		if (get > 0){
			char* f = fgets(buf, get, input_file); //blocking until some data arrives
			int got = 0;
			if (f == buf){
				got = strlen(buf);
				ring_put(&ring, buf, got);
			}
			
			//atomic_fprintf(stdout, "read %d\n", got);
		}
		
		while (ring_data_left(&ring) > 0){
			//wake up the thread whose turn it is
			pthread_mutex_lock(&wait_mutex);
			pthread_cond_broadcast(&wait_signal);
			pthread_mutex_unlock(&wait_mutex);
			
			//atomic_printf_colored(7, stdout, "loop end\n");
			usleep(2000);
		}
	}
	
	usleep(20000);
	turn = -1;
	pthread_mutex_lock(&wait_mutex);
	pthread_cond_broadcast(&wait_signal);
	pthread_mutex_unlock(&wait_mutex);
	
	atomic_fprintf(stdout, "file ended\n");
}

typedef struct worker_data_s{
	int index;
	int times_got;
} worker_data;

void* taker(void* data){
	worker_data* self = data;
	char buffer[1024];

	int running = 1;
	while (running){
		//read some stuff
		pthread_mutex_lock(&wait_mutex); //one by one the threads will acquire this lock
		pthread_cond_wait(&wait_signal, &wait_mutex);
		
		int data = ring_data_left(&ring);
		//atomic_printf_colored(self->index, stdout, "thread %d: woke up, data left: %d\n", self->index, data);
		
		if (turn == self->index){
			int got = ring_get(&ring, buffer, chunk_size);
			buffer[got] = '\0';

			atomic_printf_colored(self->index, stdout, "%s", buffer);

			//let the next thread go
			turn++;
			if(turn == num_takers) turn = 0;
			
			self->times_got++;
		}

		else if (turn == -1){
			running = 0;
		}

		pthread_mutex_unlock(&wait_mutex);
	}

	atomic_printf_colored(self->index, stdout, "printed %d times\n", self->times_got);
	pthread_exit(NULL);
}


int main(int argc, char** argv){
	if (argc != 2 && argc != 3){
		printf("Usage: distribute <chunk_size> [threads]\n");
		printf("If not specified, threads is the number of hardware threads\n");
	}

	else{
		num_takers = 0;
		chunk_size = 0;

		chunk_size = strtol(argv[1], NULL, 10);

		if (argc == 3) num_takers = strtol(argv[2], NULL, 10);
		else num_takers = sysconf(_SC_NPROCESSORS_ONLN);

		if (num_takers > 0 && chunk_size > 0){
			ring_init(&ring, 128);

			pthread_t* handles = malloc(num_takers * sizeof(pthread_t));
			worker_data* data = malloc(num_takers * sizeof(worker_data));

			pthread_cond_init(&wait_signal, NULL);
			pthread_mutex_init(&wait_mutex, NULL);

			for (int i = 0; i < num_takers; i++){
				data[i].index = i;
				pthread_create(&handles[i], NULL, taker, &data[i]);
			}

			giver(stdin);
			
			for (int i = 0; i < num_takers; i++){
				pthread_join(handles[i], NULL);
				//printf("thread %d: %f\n", i, data[i].fraction);
			}

			pthread_mutex_destroy(&wait_mutex);
			pthread_cond_destroy(&wait_signal);

			free(handles);
			free(data);
		}

		else{
			if (chunk_size <= 0) fprintf(stderr, "chunk_size must be greater than 0.\n");
			if (num_takers <= 0) fprintf(stderr, "threads must be greater than 0.\n");
		}
	}

	return 0;
}
