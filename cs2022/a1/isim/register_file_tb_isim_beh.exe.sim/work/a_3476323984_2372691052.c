/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x6dd86d03 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//tholos.scss.tcd.ie/ugrad/roscaj/cs2022/a1_final/register_file_tb.vhd";
extern char *IEEE_P_1242562249;
extern char *STD_STANDARD;

char *ieee_p_1242562249_sub_180853171_1035706684(char *, char *, int , int );


static void work_a_3476323984_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 1780U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(33, ng0);
    t2 = (t0 + 2156);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(34, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 1680);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 2156);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(37, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 1680);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_3476323984_2372691052_p_1(char *t0)
{
    char t12[16];
    char t37[16];
    char t38[16];
    char t39[16];
    char t40[16];
    char t42[16];
    char t47[16];
    char *t1;
    char *t2;
    char *t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    int t10;
    int t11;
    char *t13;
    char *t14;
    int t15;
    int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned char t20;
    unsigned int t21;
    int t22;
    int t23;
    int t24;
    int t25;
    int t26;
    int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;
    unsigned char t31;
    unsigned int t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t48;
    char *t49;
    char *t50;

LAB0:    t1 = (t0 + 1924U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(41, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 1824);
    xsi_process_wait(t2, t4);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(42, ng0);
    t2 = (t0 + 4492);
    t5 = (t0 + 2192);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(45, ng0);
    t2 = (t0 + 2228);
    t3 = (t2 + 32U);
    t5 = *((char **)t3);
    t6 = (t5 + 40U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(46, ng0);
    t2 = (t0 + 4495);
    *((int *)t2) = 0;
    t3 = (t0 + 4499);
    *((int *)t3) = 7;
    t10 = 0;
    t11 = 7;

LAB8:    if (t10 <= t11)
        goto LAB9;

LAB11:    xsi_set_current_line(54, ng0);
    t2 = (t0 + 4555);
    *((int *)t2) = 0;
    t3 = (t0 + 4559);
    *((int *)t3) = 7;
    t10 = 0;
    t11 = 7;

LAB25:    if (t10 <= t11)
        goto LAB26;

LAB28:    goto LAB2;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(47, ng0);
    t5 = (t0 + 4495);
    t6 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t12, *((int *)t5), 3);
    t7 = (t0 + 2264);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t13 = (t9 + 40U);
    t14 = *((char **)t13);
    memcpy(t14, t6, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(48, ng0);
    t2 = (t0 + 4503);
    t5 = (t0 + 2300);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(49, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 1824);
    xsi_process_wait(t2, t4);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 4495);
    t10 = *((int *)t2);
    t3 = (t0 + 4499);
    t11 = *((int *)t3);
    if (t10 == t11)
        goto LAB11;

LAB24:    t15 = (t10 + 1);
    t10 = t15;
    t5 = (t0 + 4495);
    *((int *)t5) = t10;
    goto LAB8;

LAB12:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 1052U);
    t3 = *((char **)t2);
    t2 = (t0 + 4495);
    t15 = *((int *)t2);
    t16 = (t15 - 0);
    t17 = (t16 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, *((int *)t2));
    t18 = (16U * t17);
    t19 = (0 + t18);
    t5 = (t3 + t19);
    t6 = (t0 + 4519);
    t20 = 1;
    if (16U == 16U)
        goto LAB18;

LAB19:    t20 = 0;

LAB20:    if (t20 == 0)
        goto LAB16;

LAB17:    goto LAB10;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    t13 = (t0 + 4535);
    xsi_report(t13, 19U, 2);
    goto LAB17;

LAB18:    t21 = 0;

LAB21:    if (t21 < 16U)
        goto LAB22;
    else
        goto LAB20;

LAB22:    t8 = (t5 + t21);
    t9 = (t6 + t21);
    if (*((unsigned char *)t8) != *((unsigned char *)t9))
        goto LAB19;

LAB23:    t21 = (t21 + 1);
    goto LAB21;

LAB26:    xsi_set_current_line(57, ng0);
    t5 = (t0 + 2228);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)2;
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(58, ng0);
    t2 = (t0 + 4555);
    t3 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t12, *((int *)t2), 3);
    t5 = (t0 + 2264);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t3, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(59, ng0);
    t2 = (t0 + 4563);
    t5 = (t0 + 2300);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(60, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 1824);
    xsi_process_wait(t2, t4);

LAB31:    *((char **)t1) = &&LAB32;
    goto LAB1;

LAB27:    t2 = (t0 + 4555);
    t10 = *((int *)t2);
    t3 = (t0 + 4559);
    t11 = *((int *)t3);
    if (t10 == t11)
        goto LAB28;

LAB72:    t15 = (t10 + 1);
    t10 = t15;
    t5 = (t0 + 4555);
    *((int *)t5) = t10;
    goto LAB25;

LAB29:    xsi_set_current_line(63, ng0);
    t2 = (t0 + 4555);
    t3 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t12, *((int *)t2), 3);
    t5 = (t0 + 2192);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t3, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(65, ng0);
    t2 = (t0 + 4579);
    *((int *)t2) = 0;
    t3 = (t0 + 4583);
    *((int *)t3) = 7;
    t15 = 0;
    t16 = 7;

LAB33:    if (t15 <= t16)
        goto LAB34;

LAB36:    xsi_set_current_line(87, ng0);
    t2 = (t0 + 2228);
    t3 = (t2 + 32U);
    t5 = *((char **)t3);
    t6 = (t5 + 40U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(88, ng0);
    t2 = (t0 + 4555);
    t3 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t12, *((int *)t2), 3);
    t5 = (t0 + 2264);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t3, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 4660);
    t5 = (t0 + 2300);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(90, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 1824);
    xsi_process_wait(t2, t4);

LAB70:    *((char **)t1) = &&LAB71;
    goto LAB1;

LAB30:    goto LAB29;

LAB32:    goto LAB30;

LAB34:    xsi_set_current_line(66, ng0);
    t5 = (t0 + 4555);
    t6 = (t0 + 4579);
    t22 = *((int *)t5);
    t23 = *((int *)t6);
    t20 = (t22 != t23);
    if (t20 != 0)
        goto LAB37;

LAB39:
LAB38:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 4607);
    t5 = ((STD_STANDARD) + 240);
    t6 = (t0 + 4555);
    t7 = xsi_int_to_mem(*((int *)t6));
    t8 = xsi_string_variable_get_image(t12, t5, t7);
    t13 = ((STD_STANDARD) + 664);
    t14 = (t38 + 0U);
    t30 = (t14 + 0U);
    *((int *)t30) = 1;
    t30 = (t14 + 4U);
    *((int *)t30) = 13;
    t30 = (t14 + 8U);
    *((int *)t30) = 1;
    t22 = (13 - 1);
    t17 = (t22 * 1);
    t17 = (t17 + 1);
    t30 = (t14 + 12U);
    *((unsigned int *)t30) = t17;
    t9 = xsi_base_array_concat(t9, t37, t13, (char)97, t2, t38, (char)97, t8, t12, (char)101);
    t30 = (t0 + 4620);
    t35 = ((STD_STANDARD) + 664);
    t36 = (t40 + 0U);
    t41 = (t36 + 0U);
    *((int *)t41) = 1;
    t41 = (t36 + 4U);
    *((int *)t41) = 4;
    t41 = (t36 + 8U);
    *((int *)t41) = 1;
    t23 = (4 - 1);
    t17 = (t23 * 1);
    t17 = (t17 + 1);
    t41 = (t36 + 12U);
    *((unsigned int *)t41) = t17;
    t34 = xsi_base_array_concat(t34, t39, t35, (char)97, t9, t37, (char)97, t30, t40, (char)101);
    t41 = ((STD_STANDARD) + 240);
    t43 = (t0 + 4579);
    t44 = xsi_int_to_mem(*((int *)t43));
    t45 = xsi_string_variable_get_image(t42, t41, t44);
    t48 = ((STD_STANDARD) + 664);
    t46 = xsi_base_array_concat(t46, t47, t48, (char)97, t34, t39, (char)97, t45, t42, (char)101);
    t49 = (t12 + 12U);
    t17 = *((unsigned int *)t49);
    t18 = (13U + t17);
    t19 = (t18 + 4U);
    t50 = (t42 + 12U);
    t21 = *((unsigned int *)t50);
    t28 = (t19 + t21);
    xsi_report(t46, t28, 0);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2228);
    t3 = (t2 + 32U);
    t5 = *((char **)t3);
    t6 = (t5 + 40U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 4579);
    t3 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t12, *((int *)t2), 3);
    t5 = (t0 + 2264);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t3, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 1824);
    xsi_process_wait(t2, t4);

LAB50:    *((char **)t1) = &&LAB51;
    goto LAB1;

LAB35:    t2 = (t0 + 4579);
    t15 = *((int *)t2);
    t3 = (t0 + 4583);
    t16 = *((int *)t3);
    if (t15 == t16)
        goto LAB36;

LAB67:    t22 = (t15 + 1);
    t15 = t22;
    t5 = (t0 + 4579);
    *((int *)t5) = t15;
    goto LAB33;

LAB37:    xsi_set_current_line(67, ng0);
    t7 = (t0 + 1052U);
    t8 = *((char **)t7);
    t7 = (t0 + 4555);
    t24 = *((int *)t7);
    t25 = (t24 - 0);
    t17 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, *((int *)t7));
    t18 = (16U * t17);
    t19 = (0 + t18);
    t9 = (t8 + t19);
    t13 = (t0 + 1052U);
    t14 = *((char **)t13);
    t13 = (t0 + 4579);
    t26 = *((int *)t13);
    t27 = (t26 - 0);
    t21 = (t27 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, *((int *)t13));
    t28 = (16U * t21);
    t29 = (0 + t28);
    t30 = (t14 + t29);
    t31 = 1;
    if (16U == 16U)
        goto LAB42;

LAB43:    t31 = 0;

LAB44:    if ((!(t31)) == 0)
        goto LAB40;

LAB41:    goto LAB38;

LAB40:    t35 = (t0 + 4587);
    xsi_report(t35, 19U, 2);
    goto LAB41;

LAB42:    t32 = 0;

LAB45:    if (t32 < 16U)
        goto LAB46;
    else
        goto LAB44;

LAB46:    t33 = (t9 + t32);
    t34 = (t30 + t32);
    if (*((unsigned char *)t33) != *((unsigned char *)t34))
        goto LAB43;

LAB47:    t32 = (t32 + 1);
    goto LAB45;

LAB48:    xsi_set_current_line(75, ng0);
    t2 = (t0 + 1052U);
    t3 = *((char **)t2);
    t2 = (t0 + 4555);
    t22 = *((int *)t2);
    t23 = (t22 - 0);
    t17 = (t23 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, *((int *)t2));
    t18 = (16U * t17);
    t19 = (0 + t18);
    t5 = (t3 + t19);
    t6 = (t0 + 1052U);
    t7 = *((char **)t6);
    t6 = (t0 + 4579);
    t24 = *((int *)t6);
    t25 = (t24 - 0);
    t21 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, *((int *)t6));
    t28 = (16U * t21);
    t29 = (0 + t28);
    t8 = (t7 + t29);
    t20 = 1;
    if (16U == 16U)
        goto LAB54;

LAB55:    t20 = 0;

LAB56:    if (t20 == 0)
        goto LAB52;

LAB53:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 4555);
    t3 = (t0 + 4579);
    t22 = *((int *)t2);
    t23 = *((int *)t3);
    t20 = (t22 != t23);
    if (t20 != 0)
        goto LAB60;

LAB62:
LAB61:    goto LAB35;

LAB49:    goto LAB48;

LAB51:    goto LAB49;

LAB52:    t14 = (t0 + 4624);
    xsi_report(t14, 19U, 2);
    goto LAB53;

LAB54:    t32 = 0;

LAB57:    if (t32 < 16U)
        goto LAB58;
    else
        goto LAB56;

LAB58:    t9 = (t5 + t32);
    t13 = (t8 + t32);
    if (*((unsigned char *)t9) != *((unsigned char *)t13))
        goto LAB55;

LAB59:    t32 = (t32 + 1);
    goto LAB57;

LAB60:    xsi_set_current_line(79, ng0);
    t5 = (t0 + 2228);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)2;
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 4579);
    t3 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t12, *((int *)t2), 3);
    t5 = (t0 + 2264);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t3, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 4644);
    t5 = (t0 + 2300);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t2, 16U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 1224U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 1824);
    xsi_process_wait(t2, t4);

LAB65:    *((char **)t1) = &&LAB66;
    goto LAB1;

LAB63:    goto LAB61;

LAB64:    goto LAB63;

LAB66:    goto LAB64;

LAB68:    goto LAB27;

LAB69:    goto LAB68;

LAB71:    goto LAB69;

}


extern void work_a_3476323984_2372691052_init()
{
	static char *pe[] = {(void *)work_a_3476323984_2372691052_p_0,(void *)work_a_3476323984_2372691052_p_1};
	xsi_register_didat("work_a_3476323984_2372691052", "isim/register_file_tb_isim_beh.exe.sim/work/a_3476323984_2372691052.didat");
	xsi_register_executes(pe);
}
