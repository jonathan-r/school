library ieee;
use ieee.std_logic_1164.all;
 
entity full_adder_tb is
end full_adder_tb;
 
architecture behavior of full_adder_tb is 
	component full_adder
    port(
         carry_in : in  std_logic;
         a : in  std_logic;
         b : in  std_logic;
         sum : out  std_logic;
         carry_out : out  std_logic
        );
    end component;
    
   signal carry_in : std_logic := '0';
   signal a : std_logic := '0';
   signal b : std_logic := '0';
   signal sum : std_logic;
   signal carry_out : std_logic;
 
begin
   uut: full_adder port map (carry_in, a, b, sum, carry_out);

   stim_proc: process
   begin	
      wait for 100 ns;
		
		carry_in <= '0';
		a <= '0';
		b <= '0';
		wait for 10ns;
		assert sum = '0';
		assert carry_out = '0';
		
		carry_in <= '1';
		a <= '0';
		b <= '0';
		wait for 10ns;
		assert sum = '1';
		assert carry_out = '0';
		
		carry_in <= '0';
		a <= '1';
		b <= '1';
		wait for 10ns;
		assert sum = '0';
		assert carry_out = '1';
		
		carry_in <= '1';
		a <= '1';
		b <= '1';
		wait for 10ns;
		assert sum = '1';	
		assert carry_out = '1';

      wait;
   end process;
end;
