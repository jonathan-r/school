from random import randint

class alum:
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.courses = []
		self.since = 9999999
		
	def __repr__(self):
		return str(self.id) + ' | ' + str(self.name) + ' | since ' + str(self.since)

class course:
	def __init__(self, code, year):
		self.code = code
		self.year = year
		self.alums = set()
	
	def add_alum(self, alum):	
		self.alums.add(alum)
		alum.courses.append(self)
		if self.year < alum.since:
			alum.since = self.year
			
	def __repr__(self):
		return str(self.code) + '@' + str(self.year) + ':' + str(self.alums) + '\n'

ids = set()
def gen_id():
	global ids
	while True:
		i = randint(245, 10000)
		if i not in ids:
			ids.add(i)
			return i

f = open('/usr/share/dict/web2', 'r')
names = [x[:len(x)-1] for x in f.readlines() if len(x) > 2 and x[0].isupper()] #exclude newline

def gen_name():
	global names
	a = randint(0, len(names) - 1)
	b = randint(0, len(names) - 2)
	
	return names.pop(a) + ' ' + names.pop(b)

alums = []
courses = []

#add courses
for i in xrange(5):
	courses.append(course(hex(randint(16, 255))[2:].upper(), randint(1995, 2014)))

#add alums
for i in xrange(100):
	alums.append(alum(gen_id(), gen_name()))
	courses[randint(0, len(courses) - 1)].add_alum(alums[-1])

print courses
