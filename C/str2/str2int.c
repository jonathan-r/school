#include <stdio.h>

//This function takes in a null terminated string and reads in one integer from it, in the format [+-]<decimals>.
//It reads either until the number ends, the null terminator, or until it has read more than max_length character.
//max_length may be 0, in which case the latter condition does not apply.
//If at least one character was read and the number does not overflow the signed integer range, 
//the argument success (if not NULL) is updated with either 1. Otherwise it is updated with 0.
int str2int(char* str, int max_length, int* success){
	int s = 0;
	int neg = 0;
	int valid = 0;
	int value = 0;
	
	//remove the length limitation
	if (max_length == 0) max_length = 0xEFFFFFFF;

	//skip leading space
	while (*str == ' ' && s < max_length){
		str++; s++;
	}

	if (s < max_length){
		//optional minus
		if (*str == '-'){
			neg = 1;
			str++;
			s++;
		}

		//or an optional plus, not both
		else if (*str == '+'){
			str++;
			s++;
		}

		//read every decimal digit
		while (*str >= 0x30 && *str <= 0x39 && s < max_length){
			valid = 1; //at least was read, so it is a success

			int old = value;
			value = value * 10 + (*str - 0x30);

			if (value < old){
				//overflow
				valid = 0;
				break;
			}
			str++; s++;
		}
	}

	if (neg) value = -value; //make it negative
	if (success) *success = valid; //update success
	return value; //return value read
}

int main(int argc, char** argv){
	const int max = 5;
	char buffer[max];

	printf("type a number\n");
	fgets(buffer, max, stdin);

	int success;
	int val = str2int(buffer, max, &success);

	if (success) printf("valid:\n%d\n", val);
	else printf("invalid\n");

	return 0;
}
