samp_gen <- function(N = 150, m = 200, s = 30) {
  samp = rnorm(n=N, mean=m, sd=s)
  samp_mean = mean(samp)
  samp_sd = sd(samp)
  CI_low = samp_mean - 1.96 * samp_sd / sqrt(N)
  CI_high = samp_mean + 1.96 * samp_sd / sqrt(N)
  return (c(CI_low, CI_high))
}
in_range = 0
for (i in 1 : 100) {
  s <- samp_gen()
  if (s[1] <= 200 && s[2] >= 200) {
    in_range <- in_range + 1
  }
}
