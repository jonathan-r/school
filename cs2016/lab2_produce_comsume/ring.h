#ifndef RING_BUFFER_H
#define RING_BUFFER_H

typedef struct ring_buffer_s{
	int data_start; //index of first slow with data
	int data_end; //index of last slot with data
	int stuff; //amount of stuff
	int size; //size of the buffer
	char* data; //buffer
} ring_buffer;

//initializes a new ring buffer with no data
void ring_init(ring_buffer* ring, int size);

//safely deletes a ring buffer (whether it was initialized or not)
void ring_destroy(ring_buffer* ring);

//returns how much space in the buffer is unoccupied
int ring_space_left(ring_buffer* ring);

//returns how much space in the buffer is occupied
int ring_data_left(ring_buffer* ring);

//attempts to the buffer with as many bytes from input (at most max)
//returns the number of bytes copied
int ring_put(ring_buffer* ring, char* input, int max);

//attempts to read as many characters from the buffer into output (at most max)
//returns the number of bytes copied
int ring_get(ring_buffer* ring, char* output, int max);

#endif