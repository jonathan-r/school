'''
int n;

proctype thread(){
	int temp;
	for (i : 1 .. 10){
		temp = n;
		temp = temp + 1;
		n = temp;
		printf("n is %d\n", n);
'''

pcs = [0] * 2
temps = [0] * 2
loop = [0] * 2
n = 0
stack = []
solutions = [] #[(pcs, n)]
iterations = 5

def run():
	global pcs, temps, loop, n, stack

	stack.append((pcs[:], temps[:], loop[:], n))

	if n == 2 and loop[0] == loop[1] and loop[0] == iterations:
		print ('solution')
		solutions.append(stack[:])
	
	#print(pcs, n)
	for i in range(len(pcs)):
		if loop[i] < iterations:
			#do
			old_n = n
			old_temp = temps[i]

			if pcs[i] == 0:
				temps[i] = n

			elif pcs[i] == 1:
				n = temps[i] + 1

			pcs[i] += 1
			if pcs[i] == 2:
				loop[i] += 1
				pcs[i] = 0

			run()
			
			#undo
			pcs[i] -= 1
			if pcs[i] == -1:
				pcs[i] = 1
				n = old_n
			
			elif pcs[i] == 1:
				temps[i] = old_temp

	stack.pop(-1)

run()

print (len(solutions))
'''
for sol in solutions:
	print('solution:')
	for (pcs, temps, loops, n) in sol:
		print (pcs, temps, loops, n)'''
