import java.util.Random;
import java.util.Stack;
import java.util.Iterator;
import java.io.PrintWriter;

/**
 * This implements a Random Maze Runner for any kind of mazes.
 *
 * @author 
 */
public class DFSMazeRunner<MC extends MazeCell> extends MazeRunner<MC> {
	/**
	 * Tries to find a solution to the given maze. 
	 *
	 * @param maze		The maze to solve.
	 * @param writer	The printwriter on which to output the
	 * 			maze solution.
	 */
	public void solveMaze(Maze<MC> maze, PrintWriter writer) {
		MC currCell = maze.getStart();
		Stack<MC> cellsInProgress = new Stack<MC>();

		/*
		 * Cases:
		 * 1. notVisited neighbor
		 * 		-push the currCell onto stack
		 * 		-change the currCell to the neighbor
		 * 
		 * 2. visited (all neighbors)
		 * 		-set the currCell's state to visited
		 * 		-set the currCell to stack.pop();
		 * 
		 * 3. visitInPRogress neighbor
		 * 		-ignore(because they are all on the stack anyway)
		 * 
		 * */

		currCell.setState(MazeCell.CellState.visitInProgress);
		while(!currCell.isDonut()){
			//Pop the value that was mistaken for a doughnut at the last iteration
			if(!cellsInProgress.isEmpty()){
				cellsInProgress.pop();
			}
			
			Iterator<MC> it = maze.getNeighbors(currCell);
			boolean notVisitedFound = false;
			
			while(it.hasNext()){
				//iterate through all the neighbors
				MC c = it.next();
				
				if(c.getState()==MazeCell.CellState.notVisited){
					//if neighbor cell has not been visited
					notVisitedFound = true;

					currCell.setState(MazeCell.CellState.visitInProgress);
					cellsInProgress.push(currCell);
					currCell = c;
					
					break;
				}
			}
		
			if(!notVisitedFound){
				//No notVisited cell was found so go back becuase of a dead end
				currCell.setState(MazeCell.CellState.visited);
				if( !cellsInProgress.isEmpty() ){
					currCell = cellsInProgress.pop();
				}else{
					writer.print("no solution\n");
					return;
				}
			}
			
			//add the last time and if it will be donut it will not get popped at the next iteration otherwise this value will be popped
			MC doughnut = currCell;
			cellsInProgress.push(doughnut);
		}
		
		// NOW PRINT OUT THE STACK AND SET ALL THE CELLS TO SOLUTIONPATH STATE AS U GO
		String strPath = "";
		System.out.println(cellsInProgress.size());
		while(!cellsInProgress.isEmpty()){
			MC c = cellsInProgress.pop();
			c.setState(MazeCell.CellState.solutionPath);
			String temp = c.toString() + " ";
			temp += strPath;
			strPath =temp;
		}
		writer.print(strPath+"\n");//using println makes newlines "\r\n" on windows
	}
	
	
}
