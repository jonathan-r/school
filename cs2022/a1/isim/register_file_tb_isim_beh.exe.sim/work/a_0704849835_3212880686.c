/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x6dd86d03 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//tholos.scss.tcd.ie/ugrad/roscaj/cs2022/a1_final/mux8_16bit.vhd";



static void work_a_0704849835_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    char *t5;
    char *t6;
    int t7;
    char *t8;
    char *t9;
    int t10;
    char *t11;
    int t13;
    char *t14;
    int t16;
    char *t17;
    int t19;
    char *t20;
    int t22;
    char *t23;
    int t25;
    char *t26;
    char *t27;
    int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;

LAB0:    xsi_set_current_line(14, ng0);
    t1 = (t0 + 592U);
    t2 = *((char **)t1);
    t1 = (t0 + 3099);
    t4 = xsi_mem_cmp(t1, t2, 3U);
    if (t4 == 1)
        goto LAB3;

LAB12:    t5 = (t0 + 3102);
    t7 = xsi_mem_cmp(t5, t2, 3U);
    if (t7 == 1)
        goto LAB4;

LAB13:    t8 = (t0 + 3105);
    t10 = xsi_mem_cmp(t8, t2, 3U);
    if (t10 == 1)
        goto LAB5;

LAB14:    t11 = (t0 + 3108);
    t13 = xsi_mem_cmp(t11, t2, 3U);
    if (t13 == 1)
        goto LAB6;

LAB15:    t14 = (t0 + 3111);
    t16 = xsi_mem_cmp(t14, t2, 3U);
    if (t16 == 1)
        goto LAB7;

LAB16:    t17 = (t0 + 3114);
    t19 = xsi_mem_cmp(t17, t2, 3U);
    if (t19 == 1)
        goto LAB8;

LAB17:    t20 = (t0 + 3117);
    t22 = xsi_mem_cmp(t20, t2, 3U);
    if (t22 == 1)
        goto LAB9;

LAB18:    t23 = (t0 + 3120);
    t25 = xsi_mem_cmp(t23, t2, 3U);
    if (t25 == 1)
        goto LAB10;

LAB19:
LAB11:    xsi_set_current_line(23, ng0);
    t1 = (t0 + 3123);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);

LAB2:    t1 = (t0 + 1632);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(15, ng0);
    t26 = (t0 + 684U);
    t27 = *((char **)t26);
    t28 = (0 - 0);
    t29 = (t28 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t26 = (t27 + t31);
    t32 = (t0 + 1676);
    t33 = (t32 + 32U);
    t34 = *((char **)t33);
    t35 = (t34 + 40U);
    t36 = *((char **)t35);
    memcpy(t36, t26, 16U);
    xsi_driver_first_trans_fast_port(t32);
    goto LAB2;

LAB4:    xsi_set_current_line(16, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (1 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB5:    xsi_set_current_line(17, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (2 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB6:    xsi_set_current_line(18, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (3 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB7:    xsi_set_current_line(19, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (4 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB8:    xsi_set_current_line(20, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (5 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB9:    xsi_set_current_line(21, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (6 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB10:    xsi_set_current_line(22, ng0);
    t1 = (t0 + 684U);
    t2 = *((char **)t1);
    t4 = (7 - 0);
    t29 = (t4 * 1);
    t30 = (16U * t29);
    t31 = (0 + t30);
    t1 = (t2 + t31);
    t3 = (t0 + 1676);
    t5 = (t3 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t3);
    goto LAB2;

LAB20:;
}


extern void work_a_0704849835_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0704849835_3212880686_p_0};
	xsi_register_didat("work_a_0704849835_3212880686", "isim/register_file_tb_isim_beh.exe.sim/work/a_0704849835_3212880686.didat");
	xsi_register_executes(pe);
}
