#pragma once

//#include <stdint.h>

#include <thread>

#include "maths/vec.h"
#include "maths/util.h"

#include "frustum.h"
#include "object.h"
#include "noise.h"
#include "vbo.h"

#define TERRAIN_MAX_DEPTH 7
#define TERRAIN_LEAF_ACROSS 16 //must be even
#define TERRAIN_SPLIT_RATIO 0.9f
#define TERRAIN_GOOD_NORMALS //really really expensive and stupid

//http://vterrain.org/LOD/Papers/
//(actually used) http://www.slideshare.net/repii/terrain-rendering-in-frostbite-using-procedural-shader-splatting-presentation?from=ss_embed

class Terrain {
public:
	Terrain(NoiseGen& n, const vec2& size, float fov, float aspect, float f) :
		noise(n), clip(fov, aspect, 0, f), samplingSize(size), root(0) {
		distBound = 30.0f;

		vertStorage = 10000; points = (vertex*) malloc(vertStorage * sizeof(vertex));
		indStorage = 50000; indices = (uint32_t*) malloc(indStorage * sizeof(uint32_t));
		vertUsed = indUsed = 0;
		//allocate(TERRAIN_LEAF_ACROSS * TERRAIN_LEAF_ACROSS * 1000); //expect that many patches at most

		leafCount = 1; //root

		object.model = &vbo;
		object.drawMode = GL_TRIANGLE_FAN; //with restart
		object.texture = nullptr;

		object.texture = n.makeNoiseTexture(1024, 1024, 5, 1.3, size);
		object.texture->bind(0);
		object.texture->updateAccessMode(GL_LINEAR, GL_LINEAR, 16);
		//glGenerateMipmap(GL_TEXTURE_2D);
		object.texture->unBind();

		debugVerts = nullptr;

		reprCol[0].set(0.7f, 0.3f, 0.3f);
		reprCol[1].set(0.3f, 0.7f, 0.3f);
		reprCol[2].set(0.3f, 0.3f, 0.7f);
		reprCol[3].set(0.6f, 0.2f, 0.6f);
	}

	void update(const vec3& eye3, const vec3& target3, const vec3& up3, bool drawLines = false);
	float sampleHeight(const vec2& p);

	Object& getOb() {
		return *(dynamic_cast<Object*>(&object));
	}

	/*void drawPoints(GenericShader& shader) {
		vbo.bind(shader);
		glDrawArrays(GL_POINTS, 0, vbo.getVerticesAssigned());
	}*/

	UIMesh overlay;

private:
	struct node {
		// child ordering ((y < mid) << 1) | (x >= mid)
		// 0|1
		// -+-
		// 2|3

		// neighbour ordering
		//  \0/
		// 3 x 1
		//  /2\

		node* ch; //nw, ne, sw, se children; none or all active
		bool visible;
		int level; //root is 0

		node(int lev) {
			ch = nullptr; //leaf
			visible = false;
			level = lev;
		}

		~node() {
			//only delete self - use flatten for recursive deletion
		}

		void markAllInvisible() {
			visible = false;
			if (ch) {
				for (int i = 0; i < 4; i++) {
					ch[i].markAllInvisible();
				}
			}
		}

		void split() {
			assert(!ch);
			ch = (node*) malloc(4 * sizeof(node));
			for (int i = 0; i < 4; i++) {
				new (&ch[i]) node(level+1);
			}
		}

		//returns number of leaves lost (excludes the one being gained after the operation is complete)
		int flatten() { 
			if (ch) {
				int ret = 0;
				for (int i = 0; i < 4; i++) {
					ret += ch[i].flatten();
				}
				delete ch; //deletes all 4
				ch = nullptr;
				return ret;
			}
			else return 1;
		}

		bool isLeaf() {
			return !ch;
		}
	};

	bool testPoint(const vec2& p);

	void splitTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se);
	void stitchTraverse(node& at, node* nn, node* ee, node* ss, node* ww, int maxDepth, int pos);
	void configTraverse(node& at,  const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se,
		node* nn, node* ee, node* ss, node* ww/*, int nwi, int nei, int swi, int sei*/);
	void reprTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se, int pos);

	int travDepth;
	int leafCount;

	FrustumT<float> clip;
	Mesh vbo;
	QuatObject object;

	bool topologyChanged;

	node root;
	//TODO: node memory pool

	//parameters
	NoiseGen& noise;
	vec2 samplingSize; //bounds

	//at each update
	vec3 eye;
	float distBound; //anything further if offscreen

	//output
	//void allocate(int count);
	uint32_t addVert(const vec2& p);
	void addIndex(uint32_t ind);

	int vertStorage;
	int indStorage;

	int vertUsed;
	int indUsed;

	vertex* points;
	uint32_t* indices;
	uint32_t rowStart[TERRAIN_LEAF_ACROSS+1];

	int uiVertsUsed;
	vertex_ui* debugVerts;
	vec3 reprCol[4];

	//TODO: worker thread
};
