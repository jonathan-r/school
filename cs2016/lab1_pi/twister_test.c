#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "twister.h"

int main(int argc, char** argv){
	twister t;
	twister_init(&t, time(NULL));

	int numbers = 1;
	if (argc == 2) numbers = atoi(argv[1]);

	for (int i = 0; i < numbers; i++){
		double d = twister_unit(&t);
		printf("%f ", d);
	}

	printf("\n");
}