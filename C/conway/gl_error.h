#ifndef GLERROR_H
#define GLERROR_H

class GLerror{
public:
	static void list(const char* from){
		GLenum err;
		while ((err = glGetError()) != GL_NO_ERROR){
			printf("gl error from %s: %s\n", from, gluErrorString(err));
		}
	}
};

#endif
