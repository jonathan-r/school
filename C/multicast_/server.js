'use strict';

var dgram = require("dgram");
var server = dgram.createSocket("udp4");
var group = "225.1.1.1";

server.bind();
server.setBroadcast(true);
server.setMulticastTTL(128);
server.addMembership(group);//, "192.168.15.2"); //multicast group, outbound interface

function broadcast(){
	var buf = new Buffer("Terran eclipse");
	server.send(buf, 0, buf.length, 8088, group);
}

broadcast();


