#version 120

uniform mat4 fullMatrix; //proj * view * model
uniform mat4 worldMatrix; //model
uniform float time; //seconds

attribute vec3 vPosition; //object
attribute vec3 vNormal; //object
//attribute vec3 vTangent; //d tex / d surface
attribute vec2 vTexCoord;

varying vec3 worldVec; //world
varying vec3 interpNormal; //world
varying vec2 tex;
//varying vec2 tangent; //world
//varting vec2 biTangent; //world

void main() {
	gl_Position = fullMatrix * vec4(vPosition, 1.0); //eye space
	worldVec = (worldMatrix * vec4(vPosition, 1.0)).xyz; //world space
	interpNormal = (worldMatrix * vec4(vNormal, 0.0)).xyz; //world space - normalization needed for terrain

	tex = vTexCoord;
	//tangent = vTangent; //world
	//biTangent = cross(vNormal, biTangent); //world
}
