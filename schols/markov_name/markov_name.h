#ifndef MARKOV_NAME_H
#define MARKOV_NAME_H

#define MARKOV_VOWELS "aeiouy"
#define MARKOV_LETTERS 26 //english letters

typedef struct markov_state_s{
	int transition[MARKOV_LETTERS][MARKOV_LETTERS + 1]; //the probability of a transition from one letter to another (+1 for when a word ends)
	int transition_sum[MARKOV_LETTERS]; //the sum of all transition probabilities for that letter
	int opener[MARKOV_LETTERS]; //the probability of being the first letter
	int opener_sum; //the sum of all probabilities of being the first letter
} markov_state;


void markov_init(markov_state* state);
void markov_train(markov_state* state, char* str, int len);
void markov_train_file(markov_state* state, const char* filename);
char* markov_word(markov_state* state, int min_length, int max_length);
char* markov_name(markov_state* state, int min_length, int max_length);


#endif
