library ieee;
use ieee.std_logic_1164.all;

 
entity logic_unit_tb is
end logic_unit_tb;
 
architecture behavior of logic_unit_tb is 
    component logic_unit
    port(a : in  std_logic_vector(15 downto 0);
         b : in  std_logic_vector(15 downto 0);
         op : in  std_logic_vector(1 downto 0);
         f : out  std_logic_vector(15 downto 0) );
    end component;
    
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');
   signal op : std_logic_vector(1 downto 0) := (others => '0');
   signal f : std_logic_vector(15 downto 0);
 
begin
   uut: logic_unit port map (a, b, op, f);

   stim_proc: process begin		
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"d47f"; --1101 0100 0111 1111
	
		--conjunction
		op <= "00";
		wait for 10ns;
		assert f = x"804d"; --1000 0000 0100 1101
		
		--disjunction
		op <= "01";
		wait for 10ns;
		assert f = x"ffff"; --1111 1111 1111 1111
		
		--exclusive
		op <= "10";
		wait for 10ns;
		assert f = x"7fb2"; --0111 1111 1011 0010
		
		--inverted
		op <= "11";
		wait for 10ns;
		assert f = x"5432"; --0101 0100 0011 0010
		
		wait;
   end process;
end;
