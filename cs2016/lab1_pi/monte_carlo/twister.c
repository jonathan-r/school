#include "twister.h"

void twister_init(twister* t, uint32_t seed){
	// /http://www.mcs.anl.gov/~kazutomo/hugepage-old/twister.c
	t->index = 0;
	t->state[0] = seed;
	t->state[TWISTER_STATE_SIZE] = seed;
	uint32_t x = seed | 1;

	for (int i = 1; i < TWISTER_STATE_SIZE; i++){
		x *= 69069;
		t->state[i] = x;
	}
}

void twister_generate(twister* t){
	for (int i = 0; i < TWISTER_STATE_SIZE; i++){
		int32_t y = (t->state[i] & 0x80000000U) | (t->state[i + 1] & 0x7FFFFFFF);
		t->state[i] = t->state[(i + TWISTER_M) % TWISTER_STATE_SIZE] ^ (y >> 1);
		if (y & 0x1) t->state[i] ^= 0x9908B0DF;
	}

	/*for (int i = 0; i < TWISTER_STATE_SIZE - TWISTER_M; i++){
		int32_t y = (t->state[i] & 0x80000000U) | (t->state[i + 1] & 0x7FFFFFFF);
		t->state[i] = t->state[(i + TWISTER_M)] ^ (y >> 1);
		if (y & 0x1) t->state[i] ^= 0x9908B0DF;
	}

	int j = 0;
	for (int i = TWISTER_STATE_SIZE - TWISTER_M; i < TWISTER_STATE_SIZE; i++){
		int32_t y = (t->state[i] & 0x80000000U) | (t->state[i + 1] & 0x7FFFFFFF);
		t->state[i] = t->state[j++] ^ (y >> 1);
		if (y & 0x1) t->state[i] ^= 0x9908B0DF;
	}*/

	t->state[TWISTER_STATE_SIZE] = t->state[0];
	t->index = 0; //new batch
}

uint32_t twister_uint(twister* t){
	if (t->index == TWISTER_STATE_SIZE) twister_generate(t);

	uint32_t y = t->state[t->index++];
	y ^= y >> 11;
	y ^= (y <<  7) & 0x9D2C5680U;
	y ^= (y << 15) & 0xEFC60000U;
	return (y ^ (y >> 18));
}

double twister_unit(twister* t){
	uint64_t a = twister_uint(t);
	uint64_t b = twister_uint(t);
	uint64_t mantissa = ((a & 0xFFFFFULL) << 32) | b;

	//edge cases
	if (mantissa == 0x0ULL) return 0.0;
	if (mantissa == 0xFFFFFFFFFFFFFULL) return 1.0;

	uint64_t r = ((a & 0x80000000ULL) | (0x3FFULL << 20)) << 32; //fixed exponent, keep it in the +/-(1, 2) range
	r |=  mantissa;

	//non converting cast
	if (a >> 31) return *((double*)&r) + 1.0;
	else         return *((double*)&r) - 1.0; 
}