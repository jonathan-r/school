#include <stdint.h>

enum numeral_type{
	NUMERAL_DECIMAL = 0,
	NUMERAL_OCTAL,
	NUMERAL_HEXADECIMAL,
	NUMERAL_ERROR
};

//validates and parses numeral literals of the pattern
//([+-]?[0-9]+) | ([0-7]+[bB]) | ([0-9a-fA-F]+[hH])

//literal: a null terminated string containing the input to be validated/parsed
//value: a pointer to where the result will be stored
//error_str: optionally NULL pointer, set in case of error to point to an error description
//error_position: optionally NULL pointer, set in case of error to the index in literal where the error occurred

//returns a numeral_type symbollic constant
int parse(char* literal, uint32_t* value, const char** error_str, int* error_position);
