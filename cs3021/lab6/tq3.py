from perfect_lru import PerfectLruCache

if __name__ == "__main__":
	addrs = (0x0000, 0x0010, 0x0020, 0x0030, 
	         0x0034, 0x0020, 0x0010, 0x000C, 
			 0x0050, 0x0040, 0x002C, 0x0008, 
			 0x0030, 0x0020, 0x0010, 0x0000) 
	
	c = PerfectLruCache(16, 4, 1, 16)
	print(c.size())
	for a in addrs:
		msg = c.access(a)[1]
		print(msg) #uncoment this to see the outcome of the access
		#print(c) #uncomment this to see the state of the whole cache

	misses = c.compulsory_misses + c.conflict_misses + c.capacity_misses
	print("%d hits and %d missed, %.2f hit rate," %
		(c.hits, misses, float(c.hits) / len(addrs)))
	print("%d compulsory + %d conflict + %d capacity\n" % 
		(c.compulsory_misses, c.conflict_misses, c.capacity_misses))
