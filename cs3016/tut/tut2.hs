data Colour = Black | Green | Red | Blue deriving (Eq, Show)

type OwnerName = String

type Address = [String]

data Details = Details OwnerName Address deriving Show

type AnimalName = String

data Id = Microchip AnimalName Details deriving Show

type Weight = Int

data Animal = Dog Id Colour Weight
			| Cat Id Colour Weight
			| Mouse Colour Weight
			| Frog Colour Weight
			| Snake Id Colour Weight
			deriving Show

testAnimal = Dog (Microchip "Spot" (Details "A. N. Other" ["23 Fake St.", "Faketon"])) Green 43

{- 1 -}
isGreen :: Animal -> Bool
isGreen animal = ((== Green) . getCol) animal
	where
		getCol animal = 
			case animal of
				(Dog _ col _) -> col
				(Cat _ col _) -> col
				(Snake _ col _) -> col
				(Mouse col _) -> col
				(Frog col _) -> col

{- 2better -}
getOwner :: Animal -> Maybe OwnerName
getOwner animal = (getName . getDetails . getId) animal
	where
		{- getId :: Animal -> Maybe Id -}
		getId animal = 
			case animal of
				(Dog id _ _) -> Just id
				(Cat id _ _) -> Just id
				(Snake id _ _) -> Just id
				_ -> Nothing
		
		getDetails id = 
			case id of
				Just (Microchip _ det) -> Just det
				Nothing -> Nothing

		getName det =
			case det of
				Just (Details ownerName _) -> Just ownerName
				Nothing -> Nothing

{- 3 -}
findAnimals :: OwnerName -> [Animal] -> [Animal]
findAnimals on animals = 
	--filter (\ b -> (Just on) == getOwner b)  (filter (\ a -> isJust (getOwner a)) animals)
	filter (\ a -> (Just on) == (getOwner a)) animals
