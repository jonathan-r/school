	.file	"sort.c"
	.intel_syntax noprefix
	.text
	.p2align 4,,15
	.globl	sort
	.type	sort, @function
sort:
.LFB0:
	.cfi_startproc
	push	ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	push	edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	push	esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	push	ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	sub	esp, 4                   #allocte one int on the stack, for the (size-1) temporary
	.cfi_def_cfa_offset 24
	mov	eax, DWORD PTR [esp+28]  #load size argument from the stack
	mov	ebp, DWORD PTR [esp+24]  #load array argument from the stack (it'll stay in ebp)
	sub	eax, 1                   #compute (size - 1), for the for loop termination
	test	eax, eax                 #check if (size-1) <= 0
	mov	DWORD PTR [esp], eax     #store the (size - 1) value on the stack
	jle	.L1                      #if (size-1) <= 0, there are 1 or 0 elements in the array, so there's nothing to do
	.p2align 4,,7
	.p2align 3

.L11: #set i to 0, either at the start of the for loop, or when i gets < 0 after a while body execution
	xor	edx, edx                 #set variable i to 0, for loop initialization

.L3:
	lea	edi, [edx+1]             #compute index (i+1)
	lea	eax, [0+edi*4]           #offset of element (i+1)
	lea	esi, [ebp+0+eax]         #pointer to the (i+1)th element
	lea	eax, [ebp-4+eax]         #pointer to the (i)th element
	mov	ecx, DWORD PTR [esi]     #value of (i+1)th element
	mov	ebx, DWORD PTR [eax]     #value of (i)th element
	cmp	ecx, ebx                 #1st part of while condition: compare the values of (i+1)th and (i)th elements
	jle	.L4                      #short circuit AND fail to ???
	test	edx, edx                 #2nd part of while condition: see if i >= 0
	js	.L4                      #fail (negative)
	mov	edi, eax                 #assign pointer of the (i)th element to edi
	jmp	.L7                      #condition success, go to while body
	.p2align 4,,7
	.p2align 3

.L9: #(i >= 0) while condition check
	cmp	edx, -1                  #see if i >= 0
	mov	edi, eax                 #*not sure about this
	.p2align 4,,2
	je	.L11                     #condition fail, go set i to 0 and do the next round of the for loop

.L7: #while body
	mov	DWORD PTR [eax], ecx     #assign value of (i+1)th element to the (i)th element
	sub	edx, 1                   #decrement i variable, line 7
	mov	DWORD PTR [esi], ebx     #assign value of (i)th element to the (i+1)th element
	mov	ecx, DWORD PTR [eax]     #*wtf? reload ecx back?
	#transition into the next iteration
	mov	esi, eax                 #save the pointer (i+1)th element (note the decremente)
	mov	ebx, DWORD PTR [edi-4]   #load value of the (i)th element (note the decrement above)
	sub	eax, 4                   #rather than recompute the pointer to the (i)th element after i is decremented, just decrement the pointer
	cmp	ecx, ebx                 #1st part of the while loop condition, compare (i+1)th and (i)th elements' values
	jg	.L9                      #success so far, go to 2nd part of the condition
	lea	edi, [edx+1]             #on fail, make a copy of (i+1) in preparation for the for loop condition check

.L4: #while condition fail
	cmp	edi, DWORD PTR [esp]     #for loop condition, see if i(from edi, not edx) got to size-1
	mov	edx, edi                 #*not sure why line 69 didn't just increment edx
	jl	.L3                      #if (i < size-1), go to the next for iteration

.L1: #epilogue
	add	esp, 4 #pop the (size-1) temporary off the stack
	.cfi_def_cfa_offset 20
	pop	ebx    #restore clobbered registers
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	pop	esi    #ditto
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	pop	edi    #ditto
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	pop	ebp    #restore previous stack frame
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret            #return (0?)
	.cfi_endproc
.LFE0:
	.size	sort, .-sort
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
