#pragma once

#include <vector>
#include <string>
#include <map>

#include "Variable.h"

/*! \brief represents a 'trial' of the coin tossing scenario 
 *
 *  a coin is tossed to choose between
 *  two other coins and then the chosen coin is tossed some number of times
 *
 * eg. the show method will make something like the following be displayed
 * CHOICE: A
 * TOSSES: HHHHHHHHTT H:8 T:2  
 */
class CoinTrial {
public:
	// a choice of coin to throw
	// and N outcomes of the throws of that coin
	CoinTrial();
	std::vector<int> outcomes; //!< vector of codes of the heads or tails coin toss outcomes
	int coin_choice; //!< code of the chosen coin
	void show(Variable& chce, Variable& ht);
	std::string outcomes_string(Variable& ht);
	void set_ht_cnts(); //!< makes the heads and tails counts based on 'outcomes'
	int ht_cnts[2]; //!< stores counts heads and tails
};
