--CS2022 assignment 2
--Jonathan Rosca, 12306261

--Layout
--The function unit and register file are implemented in a container module. 
--The function unit is composed of the arithmetic, logical and shifter units. The arithmetic unit contains a 16 bit ripple adder, which is used for both addition and subtraction. 
--The logical unit computes all operation separately and selects the result using a 4 to 1 multiplexer. 
--The shifter is a simple multiplexer that can shift by one bit to the left or right. It implements logical shifting, meaning that the bits introduced from shifting are always 0.
--The modifed register file along with tests is also included.

--Each module comes with a testbench that considers most use cases. All the assertions succeed in simulations
--A clock cycle must take a minimum of about 70 ns, the longest path being through the arithmetic unit due to the ripple adder. This does not take into account the propagation delays the register file would have.

--The testbench container_tb.vhd contains two tests for the overall design as a whole. One involved moving data to and from every register, using both the contstant_in and data_in lines. The other is a short multiplication routine, that takes every bit of one operand one by and adds the other operands whenever the bit is 1, accumulating the result.


--Source code:
--listing of container.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity container is
    port ( reg_write : in  std_logic;
           reg_dest : in  std_logic_vector (2 downto 0);
           reg_src_a : in  std_logic_vector (2 downto 0);
           reg_src_b : in  std_logic_vector (2 downto 0);
           constant_in : in  std_logic_vector (15 downto 0);
           mb_select : in  std_logic;
			  func : in std_logic_vector (4 downto 0);
           md_select : in  std_logic;
           data_in : in  std_logic_vector (15 downto 0);
			  clk : in std_logic;
           addr_out : out  std_logic_vector (15 downto 0);
           data_out : out  std_logic_vector (15 downto 0);
			  v : out std_logic;
			  c : out std_logic;
			  n : out std_logic;
			  z : out std_logic );
end container;

architecture behavioral of container is
	component register_file is
		port ( write_reg   : in std_logic;
		    dest_select : in   std_logic_vector (2 downto 0);
			 data_in     : in std_logic_vector (15 downto 0);
		    clk         : in   std_logic;
		    a_select    : in   std_logic_vector (2 downto 0);
			 b_select    : in   std_logic_vector (2 downto 0);
			 a_out       : out  std_logic_vector (15 downto 0);
			 b_out       : out  std_logic_vector (15 downto 0) );
	end component;
	
	component function_unit is
		port ( a : in  std_logic_vector (15 downto 0);
           b : in  std_logic_vector (15 downto 0);
           func : in  std_logic_vector (4 downto 0);
           v : out  std_logic;
           c : out  std_logic;
           n : out  std_logic;
           z : out  std_logic;
           f : out  std_logic_vector (15 downto 0) );
	end component;
	
	component mux2_16bit is
		port ( in0 : in  std_logic_vector (15 downto 0);
				 in1 : in  std_logic_vector (15 downto 0);
		       addr : in std_logic;
				 output : out  std_logic_vector (15 downto 0) );
	end component;
	
	signal reg_out_b : std_logic_vector (15 downto 0); --regfile b output
	signal bus_a : std_logic_vector (15 downto 0); --reg_file a to alu and addr_out
	signal bus_b : std_logic_vector (15 downto 0); --const_mux to alu and data_out
	signal alu_out : std_logic_vector(15 downto 0); --alu to md_mux
	signal md_out : std_logic_vector(15 downto 0); --md_mux to regfile data in

begin
	--wire directly
	addr_out <= bus_a;
	data_out <= bus_b;
	
	reg_file : register_file port map(reg_write, reg_dest, md_out, clk, reg_src_a, reg_src_b, bus_a, reg_out_b);
	const_mux : mux2_16bit port map(reg_out_b, constant_in, mb_select, bus_b);
	functional : function_unit port map(bus_a, bus_b, func, v, c, n, z, alu_out);
	md_mux : mux2_16bit port map(alu_out, data_in, md_select, md_out);
	
	
end behavioral;

--listing of container_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity container_tb is
end container_tb;
 
architecture behavior of container_tb is 
	component container is
		port ( reg_write : in  std_logic;
         reg_dest : in  std_logic_vector(2 downto 0);
         reg_src_a : in  std_logic_vector(2 downto 0);
         reg_src_b : in  std_logic_vector(2 downto 0);
         constant_in : in  std_logic_vector(15 downto 0);
         mb_select : in  std_logic;
         func : in  std_logic_vector(4 downto 0);
         md_select : in  std_logic;
         data_in : in  std_logic_vector(15 downto 0);
         clk : in  std_logic;
         addr_out : out  std_logic_vector(15 downto 0);
         data_out : out  std_logic_vector(15 downto 0);
         v : out  std_logic;
         c : out  std_logic;
         n : out  std_logic;
         z : out  std_logic);
	end component;

	 --inputs
   signal reg_write : std_logic := '0';
   signal reg_dest : std_logic_vector(2 downto 0) := (others => '0');
   signal reg_src_a : std_logic_vector(2 downto 0) := (others => '0');
   signal reg_src_b : std_logic_vector(2 downto 0) := (others => '0');
   signal constant_in : std_logic_vector(15 downto 0) := (others => '0');
   signal mb_select : std_logic := '0';
   signal func : std_logic_vector(4 downto 0) := (others => '0');
   signal md_select : std_logic := '0';
   signal data_in : std_logic_vector(15 downto 0) := (others => '0');
   signal clk : std_logic := '0';

 	--outputs
   signal addr_out : std_logic_vector(15 downto 0);
   signal data_out : std_logic_vector(15 downto 0);
   signal v : std_logic;
   signal c : std_logic;
   signal n : std_logic;
   signal z : std_logic; 

	constant clk_period : time := 70ns; --50ns on the arithmetic unit + muxes and stuff around it
	
begin
	uut: container port map (reg_write, reg_dest, reg_src_a, reg_src_b, constant_in, mb_select, func, md_select, data_in, clk, addr_out, data_out, v, c, n, z);

	clk_process : process begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period * 3 / 2; --init
		
		report "contstant loading";
		reg_write <= '1';
		mb_select <= '1';
		func <= "10000"; --b passes through the shifter
		md_select <= '0'; --from the function unit
		
		for i in 0 to 7 loop
			report "write to r" & integer'image(i);
			reg_write <= '1';
			reg_dest <= std_logic_vector(to_unsigned(i, 3));
			constant_in <= std_logic_vector(to_unsigned(i, 16)) xor x"cdcd";
			wait for clk_period;
			
			report "disabled writing to r" & integer'image(i);
			reg_write <= '0';
			reg_dest <= std_logic_vector(to_unsigned(i, 3));
			data_in <= std_logic_vector(to_unsigned(i, 16)) xor x"eeee";
			wait for clk_period;
			
			report "read from r" & integer'image(i);
			reg_src_a <= std_logic_vector(to_unsigned(i, 3));
			wait for clk_period;
			assert addr_out = (std_logic_vector(to_unsigned(i, 16)) xor x"cdcd");
		end loop;
		
		report "data loading"; --load the value 0 to 7 in each register
		reg_write <= '1';
		mb_select <= '0';
		md_select <= '1';
		for i in 0 to 7 loop
			report "write to r" & integer'image(i);
			reg_dest <= std_logic_vector(to_unsigned(i, 3));
			data_in <= std_logic_vector(to_unsigned(i, 16));
			wait for clk_period;
			
			report "read from r" & integer'image(i);
			reg_src_a <= std_logic_vector(to_unsigned(i, 3));
			reg_src_b <= std_logic_vector(to_unsigned(i, 3));
			wait for clk_period;
			assert addr_out = std_logic_vector(to_unsigned(i, 16));
			assert data_out = std_logic_vector(to_unsigned(i, 16));
		end loop;
		
		
		report "sum of every register, into r0"; --sum the above values
		reg_src_a <= "000";
		reg_dest <= "000";
		reg_write <= '1';
		func <= "00010"; --sum
		md_select <= '0'; --reg input from function unit
		for i in 1 to 7 loop
			report "r0 += r" & integer'image(i);
			reg_src_b <= std_logic_vector(to_unsigned(i, 3));
			wait for clk_period;
		end loop;

		wait for 1ps; --assertion fails for some reason otherwise
		assert addr_out = x"001c"; -- 7 * (7 + 1) / 2 = 28 = 0000 0000 0001 1100
		assert c = '0';
		assert v = '0';
		assert n = '0';
		assert z = '0';
		wait for clk_period - 1ps;
		
		data_in <= x"0000";
		
		
		report "(destructive, unsigned) multiplication of 731(r2) by 375(r3), accumulate in r0, mask in r1";
		reg_write <= '1';
		md_select <= '1';
		
		reg_dest <= "000";
		data_in <= x"0000"; 
		wait for clk_period; --load 0 in r0
		
		reg_dest <= "010";
		data_in <= x"02db"; --
		wait for clk_period; --load 731 in r2
		
		reg_dest <= "011";
		data_in <= x"0177";
		wait for clk_period; --load 375 in r3
		
		reg_dest <= "100";
		data_in <= x"0000";
		wait for clk_period; --load 0 in r4
		md_select <= '0';
		
		for i in 0 to 15 loop --for every bit except the last one (cause x << 15 is always 0)
			report "multiplication round " & integer'image(i);
			report "isolate the first bit of r2";
			mb_select <= '1';
			constant_in <= x"0001"; --0x1
			func <= "01000"; -- bitwise and
			reg_src_a <= "010"; --r2
			reg_dest <= "001"; --r1
			wait for clk_period; --r1 = r2 & 0x1
			
			report "expand the first bit to every bit bits";
			mb_select <= '0';
			reg_src_a <= "100"; --r4, which stays 0 throughout
			reg_src_b <= "001"; --r1
			reg_dest <= "001"; --r1
			func <= "00101"; -- difference
			wait for clk_period; --r1 = 0 - r1
			assert (data_out = x"0000") or (data_out = x"0001");
			
			report "and r3 with the mask";
			reg_src_a <= "001"; --r1
			reg_src_b <= "011"; --r3
			reg_dest <= "001"; --r1
			func <= "01000"; -- bitwise and
			wait for clk_period; --r1 &= r3
			assert (addr_out = x"0000") or (addr_out = x"ffff");
			
			report "accumulate the masked r3";
			reg_src_a <= "000"; --r0
			reg_src_b <= "001"; --r1
			reg_dest <= "000"; --r0
			func <= "00010"; --sum
			wait for clk_period; --r0 += r1
			
			report "shift r3 left for next round";
			reg_src_b <= "011"; --r3
			reg_dest <= "011"; --r3
			func <= "11000"; --logical shift left
			wait for clk_period; --r3 <<= 1
			
			report "make the next bit of r2 the lsb";
			reg_src_b <= "010"; --r2
			reg_dest <= "010"; --r2
			func <= "10100"; --logical shift right
			wait for clk_period; --r2 >>= 1
		end loop;
		
		reg_write <= '0';
		reg_src_a <= "000";
		wait for clk_period;
		assert addr_out = std_logic_vector(to_unsigned(731 * 375, 16)); --x"2ecd"; --0010 1110 1100 1101
		
		wait;
	end process;
end;

--listing of register_file.vhd:
library ieee;
use ieee.std_logic_1164.all;
use work.array_types.all;


entity register_file is
	port ( write_reg   : in std_logic;
		    dest_select : in   std_logic_vector (2 downto 0);
			 data_in     : in std_logic_vector (15 downto 0);
		    clk         : in   std_logic;
		    a_select    : in   std_logic_vector (2 downto 0);
			 b_select    : in   std_logic_vector (2 downto 0);
			 a_out       : out  std_logic_vector (15 downto 0);
			 b_out       : out  std_logic_vector (15 downto 0) );
end register_file;

architecture behavioral of register_file is
	component demux_3to8_1bit is
		port ( data : in std_logic;
			  addr : in  std_logic_vector (2 downto 0);
           output : out std_logic_vector (7 downto 0) );
	end component;

	component register_16bit is
		port ( load : in  std_logic;
			    clk  : in  std_logic;
			    d    : in  std_logic_vector (15 downto 0);
			    q    : out std_logic_vector (15 downto 0) );
	end component;

	component mux8_16bit is
		port ( s      : in  std_logic_vector (2 downto 0);
			   in_data : in  data_array;
			   z       : out std_logic_vector (15 downto 0) );
	end component;

	signal load : std_logic_vector (7 downto 0); --load demux to load of each register
	signal reg_out_data : data_array; --register output to bus muxes

begin
	write_demux : demux_3to8_1bit port map(write_reg, dest_select, load);
	gen_reg : for i in 0 to 7 generate
		regx : register_16bit port map(load(i), clk, data_in, reg_out_data(i));
	end generate gen_reg;
	bus_a_mux : mux8_16bit port map(a_select, reg_out_data, a_out);
	bus_b_mux : mux8_16bit port map(b_select, reg_out_data, b_out);
end behavioral;

--listing of register_file_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity register_file_tb is
end register_file_tb;
 
architecture behavior of register_file_tb is 
	component register_file is
	port ( write_reg   : in std_logic;
			dest_select : in   std_logic_vector (2 downto 0);
			data_in     : in std_logic_vector (15 downto 0);
			clk         : in   std_logic;
			a_select    : in   std_logic_vector (2 downto 0);
			b_select    : in   std_logic_vector (2 downto 0);
			a_out       : out  std_logic_vector (15 downto 0);
			b_out       : out  std_logic_vector (15 downto 0));
	end component;

	signal write_reg   : std_logic := '0';
	signal dest_select : std_logic_vector(2 downto 0) := (others => '0');
	signal data_in     : std_logic_vector (15 downto 0) := (others => '0');
	signal clk         : std_logic := '0';
	signal a_select    : std_logic_vector (2 downto 0) := (others => '0');
	signal b_select    : std_logic_vector (2 downto 0) := (others => '0');
	signal a_out       : std_logic_vector (15 downto 0) := (others => '0');
	signal b_out       : std_logic_vector (15 downto 0) := (others => '0');

	constant clk_period : time := 50 ns;
	
begin
	uut: register_file port map (write_reg, dest_select, data_in, clk, a_select, b_select, a_out, b_out);

	clk_process : process begin
		clk <= '0';
		wait for clk_period/2;

		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period;

		-- register writing, bus reading and data preservation
		for i in 0 to 7 loop
			data_in <= x"1ba4" xor std_logic_vector(to_unsigned(i, 16));
			dest_select <= std_logic_vector(to_unsigned(i, 3));
			a_select <= std_logic_vector(to_unsigned(i, 3));
			b_select <= std_logic_vector(to_unsigned(i, 3));
			write_reg <= '1'; --write

			wait for clk_period; --transfer

			assert a_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --after the transfer
			assert b_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --after the transfer

			write_reg <= '0'; --don't write
			data_in <= x"cbac";

			wait for clk_period; --transfer

			assert a_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --data unchanged
			assert b_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --data unchange;
		end loop;
		
		wait;
	end process;
end;

--listing of register_16bit.vhd:
library ieee;
use ieee.std_logic_1164.all;


entity register_16bit is
    port ( load : in  std_logic;
           clk  : in  std_logic;
           d    : in  std_logic_vector (15 downto 0);
           q    : out std_logic_vector (15 downto 0) );
end register_16bit;

architecture behavioral of register_16bit is begin 
	process(clk) begin
		if clk'event and clk = '1' then
			if load = '1' then
				q <= d;
			end if;
		end if;
	end process;
end behavioral;

--listing of reg_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
 
entity reg_tb is
end reg_tb;
 
architecture behavior of reg_tb is 
	component register_16bit
	port( load : in  std_logic;
	      clk : in  std_logic;
	      d : in  std_logic_vector(15 downto 0);
	      q : out  std_logic_vector(15 downto 0) );
	end component;

	signal load_in : std_logic := '0';
	signal clk_in : std_logic := '0';
	signal d_in : std_logic_vector(15 downto 0) := (others => '0');
	signal q_out : std_logic_vector(15 downto 0);

	constant clk_period : time := 1 ns;
	
begin
	uut: register_16bit port map (load_in, clk_in, d_in, q_out);

	clk_process :process begin
		clk_in <= '0';
		wait for clk_period/2;
		clk_in <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period;

		--load a value
		d_in <= x"abab";
		load_in <= '1';
		wait for clk_period;
		assert q_out = x"abab";

		--make sure the value stays past a clock even if the input data changes
		d_in <= x"0123";
		load_in <= '0';
		wait for clk_period;
		assert q_out = x"abab";

		--change the value
		d_in <= x"fafc";
		load_in <= '1';
		wait for clk_period;
		assert q_out = x"fafc";
		
		wait;
	end process;
end;

--listing of array_types.vhd:
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package array_types is
	type data_array is array (0 to 7) of STD_LOGIC_VECTOR(15 downto 0);
end package;

--listing of function_unit.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity function_unit is
    port ( a : in  std_logic_vector (15 downto 0);
           b : in  std_logic_vector (15 downto 0);
           func : in  std_logic_vector (4 downto 0);
           v : out  std_logic;
           c : out  std_logic;
           n : out  std_logic;
           z : out  std_logic;
           f : out  std_logic_vector (15 downto 0));
end function_unit;

architecture behavioral of function_unit is
	component logic_unit is
		port ( a  : in  std_logic_vector (15 downto 0);
				 b  : in  std_logic_vector (15 downto 0);
             op : in  std_logic_vector  (1 downto 0);
             f  : out  std_logic_vector (15 downto 0) );
	end component;
	
	component arithmetic_unit is
		 port ( a : in  std_logic_vector (15 downto 0);
				  b : in  std_logic_vector (15 downto 0);
              op : in  std_logic_vector (2 downto 0);
              result : out  std_logic_vector (15 downto 0);
              v : out  std_logic;
              c : out  std_logic );
	end component;
	
	component shifter_unit is
		port ( input : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
	end component;

	component mux4_16bit is
		port ( in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
           in2 : in  std_logic_vector (15 downto 0);
           in3 : in  std_logic_vector (15 downto 0);
           addr : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
	end component;
	
	signal arith_out : std_logic_vector (15 downto 0);
	signal logic_out : std_logic_vector (15 downto 0);
	signal shift_out : std_logic_vector (15 downto 0);
	signal mux_out : std_logic_vector (15 downto 0);

begin
	arith : arithmetic_unit port map(a, b, func(2 downto 0), arith_out, v, c);
	logic : logic_unit port map(a, b, func(2 downto 1), logic_out);
	shift : shifter_unit port map(b, func(3 downto 2), shift_out);
	mux : mux4_16bit port map(arith_out, logic_out, shift_out, shift_out, func(4 downto 3), mux_out);
	
	--wire directly
	f <= mux_out; 
	n <= mux_out(15);
	
	z <= ((mux_out(0) nor mux_out(1)) and (mux_out(2) nor mux_out(3))) and
	     ((mux_out(4) nor mux_out(5)) and (mux_out(6) nor mux_out(7))) and
	     ((mux_out(8) nor mux_out(9)) and (mux_out(10) nor mux_out(11))) and
	     ((mux_out(12) nor mux_out(13)) and (mux_out(14) nor mux_out(15))) after 4ns; --log2(16)
end behavioral;

--listing of function_unit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity function_unit_tb is
end function_unit_tb;
 
architecture behavior of function_unit_tb is 
    component function_unit
    port(a : in  std_logic_vector(15 downto 0);
         b : in  std_logic_vector(15 downto 0);
         func : in  std_logic_vector(4 downto 0);
         v : out  std_logic;
         c : out  std_logic;
         n : out  std_logic;
         z : out  std_logic;
         f : out  std_logic_vector(15 downto 0) );
    end component;
    
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');
   signal func : std_logic_vector(4 downto 0) := (others => '0');
	
   signal v : std_logic;
   signal c : std_logic;
   signal n : std_logic;
   signal z : std_logic;
   signal f : std_logic_vector(15 downto 0);
	
	constant delay : time := 50ns;
 
begin
   uut: function_unit port map (a, b, func, v, c, n, z, f);

   stim_proc: process
   begin
		wait for delay;
	
		report "arithmetic, difference";
		a <= x"af3d"; --1010 1111 0011 1101
		b <= x"c7ac"; --1100 0111 1010 1100
		func <= "00101";
		wait for delay;
		assert f = x"e791"; --1110 0111 1001 0001
		assert v = '0';
		assert c = '0';
		assert n = '1';
		assert z = '0';
		
		report "arithmetic, difference, zero";
		func <= "00101";
		b <= x"af3d"; --1010 1111 0011 1101
		wait for delay;
		assert f = x"0000";
		assert v = '0';
		assert c = '1';
		assert n = '0';
		assert z = '1';
		
		report "logic, xor";
		b <= x"c7ac"; --1100 0111 1010 1100
		func <= "01100";
		wait for delay;
		assert f = x"6891"; --0110 1000 1001 0001
		assert n = '0';
		assert z = '0';
		
		report "shift, left";
		b <= x"c7ac"; --1100 0111 1010 1100
		func <= "11000";
		wait for delay;
		assert f = x"8f58"; --1000 1111 0101 1000
		assert n = '1';
		assert z = '0';
		
		wait;
   end process;
end;

--listing of arithmetic_unit.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity arithmetic_unit is
    port ( a : in  std_logic_vector (15 downto 0);
           b : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (2 downto 0);
           result : out  std_logic_vector (15 downto 0);
           v : out  std_logic;
           c : out  std_logic );
end arithmetic_unit;

architecture behavioral of arithmetic_unit is
	component full_adder is
		port ( carry_in  : in   std_logic;
				  a         : in   std_logic;
				  b         : in   std_logic;
				  sum       : out  std_logic;
				  carry_out : out  std_logic);
	end component;
	
	--second adder input
	signal adder_b : std_logic_vector (15 downto 0);
	
	--adder outputs
	signal adder_sum : std_logic_vector (15 downto 0);
	signal adder_carry : std_logic_vector (16 downto 0); --[i] is input, [i+1] is output

begin
	--ripple adder
	gen_adder : 
		for i in 0 to 15 generate
			adder : full_adder port map(adder_carry(i), a(i), adder_b(i), adder_sum(i), adder_carry(i + 1));
		end generate gen_adder;
		
	--directly connect adder_sum to the unit output
	result <= adder_sum;
	
	--carry in
	adder_carry(0) <= op(0) and (op(2) nand op(1)) after 2ns;
	
	--overflow and carry out, according to https://www.scss.tcd.ie/Michael.Manzke/CS2022/08-CS2022_p2_eighth_lecture.pdf
	v <= adder_carry(16) xor adder_carry(15) after 1ns;
	c <= adder_carry(16);
	
	process(a, b, op) begin
		--select b adder input
		case op is
			when "000" => adder_b <= x"0000" after 1ns; --result = a
			when "001" => adder_b <= x"0000" after 1ns; --result = a + 1
			when "010" => adder_b <= b after 1ns; --result = a + b  
			when "011" => adder_b <= b after 1ns; --result = a + b + 1
			when "100" => adder_b <= not b after 2ns; --result = a + b'
			when "101" => adder_b <= not b after 2ns; --result = a + b' + 1
			when "110" => adder_b <= x"FFFF" after 1ns; --result = a - 1
			when "111" => adder_b <= x"0000" after 1ns; --result = a
			when others => adder_b <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

--listing of arithmetic_unit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
 
entity arithmetic_unit_tb is
end arithmetic_unit_tb;
 
architecture behavior of arithmetic_unit_tb is 
    component arithmetic_unit
    port(a : in  std_logic_vector(15 downto 0);
         b : in  std_logic_vector(15 downto 0);
         op : in  std_logic_vector(2 downto 0);
         result : out  std_logic_vector(15 downto 0);
         v : out  std_logic;
         c : out  std_logic);
    end component;
    
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');
   signal op : std_logic_vector(2 downto 0) := (others => '0');
   signal result : std_logic_vector(15 downto 0);
   signal v : std_logic;
   signal c : std_logic;
	
	constant delay : time := 50ns;
	
begin
   uut: arithmetic_unit port map (a, b, op, result, v, c);

   stim_proc: process begin
		wait for delay;
		
		report "pass through";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010
		op <= "000";
		wait for delay;
		assert result = x"abcd";
		assert v = '0';
		assert c = '0';
		
		report "pass through";
		op <= "111";
		wait for delay;
		assert result = x"abcd";
		assert v = '0';
		assert c = '0';
		
		report "increment";
		a <= x"abcd"; --1010 1011 1100 1101
		op <= "001";
		wait for delay;
		report "increment";
		assert result = x"abce";
		assert v = '0';
		assert c = '0';
		
		report "increment with carry out";
		a <= x"ffff"; --1111 1111 1111 1111
		op <= "001";
		wait for delay;
		assert result = x"0000";
		assert v = '0';
		assert c = '1';
		
		report "sum with no overflow";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010
		op <= "010";
		wait for delay;
		assert result = x"e1a7"; --1110 0001 1010 0111
		assert v = '0';
		assert c = '0';
		
		report "sum with overflow and carry";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"b5da"; --1011 0101 1101 1010
		op <= "010";
		wait for delay;
		assert result = x"61a7"; --0110 0001 1010 0111
		assert v = '1';
		assert c = '1';
		
		report "sum + 1";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010
		op <= "011";
		wait for delay;
		assert result = x"e1a8"; --1110 0001 1010 1000
		assert v = '0';
		assert c = '0';
		
		report "difference - 1";
		a <= x"4bcd"; --0100 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010 
		--	  x"ca25"; --1100 1010 0010 0101 (~b)
		op <= "100";
		wait for delay;
		assert result = x"15f2"; --0001 0101 1111 0010
		assert v = '0'; --result should be positive
		assert c = '1';
		
		report "difference";
		a <= x"4bcd"; --0100 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010 
		--	  x"ca26"; --1100 1010 0010 0110 (~b + 1)
		op <= "101";
		wait for delay;
		assert result = x"15f3"; --0001 0101 1111 0011
		assert v = '0'; --result should be positive
		assert c = '1';
		
		report "decrement with borrow";
		a <= x"abcd"; --1010 1011 1100 1101
		op <= "110";
		wait for delay;
		assert result = x"abcc";
		assert v = '0';
		assert c = '1'; --borrow
		
		report "decrement without borrow";
		a <= x"0000";
		op <= "110";
		wait for delay;
		assert result = x"FFFF";
		assert v = '0';
		assert c = '0'; --borrow
		
		wait;
   end process;
end;

--listing of logic_unit.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity logic_unit is
    port ( a  : in  std_logic_vector (15 downto 0);
           b  : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector  (1 downto 0);
           f  : out  std_logic_vector (15 downto 0) );
end logic_unit;

architecture behavioral of logic_unit is
	component mux4_16bit is
		port ( in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
           in2 : in  std_logic_vector (15 downto 0);
           in3 : in  std_logic_vector (15 downto 0);
           addr : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
	end component;
	
	signal conjunction : std_logic_vector (15 downto 0);
	signal disjunction : std_logic_vector (15 downto 0);
	signal exclusive : std_logic_vector (15 downto 0);
	signal inverted : std_logic_vector (15 downto 0);

begin
	mux : mux4_16bit port map(conjunction, disjunction, exclusive, inverted, op, f);

	conjunction <= a and b after 1ns;
	disjunction <= a or b after 1ns;
	exclusive <= a xor b after 1ns;
	inverted <= not a after 1ns;
end behavioral;

--listing of logic_unit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;

 
entity logic_unit_tb is
end logic_unit_tb;
 
architecture behavior of logic_unit_tb is 
    component logic_unit
    port(a : in  std_logic_vector(15 downto 0);
         b : in  std_logic_vector(15 downto 0);
         op : in  std_logic_vector(1 downto 0);
         f : out  std_logic_vector(15 downto 0) );
    end component;
    
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');
   signal op : std_logic_vector(1 downto 0) := (others => '0');
   signal f : std_logic_vector(15 downto 0);
 
begin
   uut: logic_unit port map (a, b, op, f);

   stim_proc: process begin		
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"d47f"; --1101 0100 0111 1111
	
		--conjunction
		op <= "00";
		wait for 10ns;
		assert f = x"804d"; --1000 0000 0100 1101
		
		--disjunction
		op <= "01";
		wait for 10ns;
		assert f = x"ffff"; --1111 1111 1111 1111
		
		--exclusive
		op <= "10";
		wait for 10ns;
		assert f = x"7fb2"; --0111 1111 1011 0010
		
		--inverted
		op <= "11";
		wait for 10ns;
		assert f = x"5432"; --0101 0100 0011 0010
		
		wait;
   end process;
end;

--listing of shifter_unit.vhd:
library ieee;
use ieee.std_logic_1164.all;


entity shifter_unit is
    port ( input : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
end shifter_unit;

architecture behavioral of shifter_unit is

begin
	process(input, op) begin
		--end bits
		case op is
			when "00" => output(0) <= input(0) after 1ns;
						 output(15) <= input(15) after 1ns;

			when "01" => output(0) <= input(1) after 1ns;
						 output(15) <= '0' after 1ns;

			when "10" => output(0) <= '0' after 1ns;
						 output(15) <= input(14) after 1ns;

			when others => output(0) <= 'U';
						   output(15) <= 'U';
		end case;
		
		--middle bits, assumed to be the same as above
		for i in 1 to 14 loop
			case op is
				when "00" => output(i) <= input(i) after 1ns;
				when "01" => output(i) <= input(i+1) after 1ns;
				when "10" => output(i) <= input(i-1) after 1ns;
				when others => output(i) <= 'U';
			end case;
		end loop;
	end process;
end behavioral;

--listing of shifter_unit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity shifter_unit_tb is
end shifter_unit_tb;
 
architecture behavior of shifter_unit_tb is 
	component shifter_unit is
		port ( input : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0) );
	end component;

	signal input : std_logic_vector (15 downto 0) := (others => '0');
	signal op :  std_logic_vector (1 downto 0) := (others => '0');
   signal output :  std_logic_vector (15 downto 0) := (others => '0');
	
begin
	uut: shifter_unit port map(input, op, output);

	stim_proc: process begin
		input <= x"abcd"; --1010 1011 1100 1101
	
		--pass through
		op <= "00";
		wait for 10ns;
		assert output = x"abcd";
		
		--shift right
		op <= "01";
		wait for 10ns;
		assert output = x"55e6";  --0101 0101 1110 0110
		
		--shift left
		op <= "10";
		wait for 10ns;
		assert output = x"579a";  --0101 0111 1001 1010
		
		wait;
	end process;
end;

--listing of full_adder.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity full_adder is
    port ( carry_in  : in   std_logic;
           a         : in   std_logic;
           b         : in   std_logic;
           sum       : out  std_logic;
           carry_out : out  std_logic);
end full_adder;

architecture behavioral of full_adder is
begin
	process(carry_in, a, b) begin
		sum <= (a xor b) xor carry_in after 2ns;
		carry_out <= ((a xor b) and carry_in) or (a and b) after 3ns;
	end process;
end behavioral;

--listing of full_adder_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
 
entity full_adder_tb is
end full_adder_tb;
 
architecture behavior of full_adder_tb is 
	component full_adder
    port(
         carry_in : in  std_logic;
         a : in  std_logic;
         b : in  std_logic;
         sum : out  std_logic;
         carry_out : out  std_logic
        );
    end component;
    
   signal carry_in : std_logic := '0';
   signal a : std_logic := '0';
   signal b : std_logic := '0';
   signal sum : std_logic;
   signal carry_out : std_logic;
 
begin
   uut: full_adder port map (carry_in, a, b, sum, carry_out);

   stim_proc: process
   begin	
      wait for 100 ns;
		
		carry_in <= '0';
		a <= '0';
		b <= '0';
		wait for 10ns;
		assert sum = '0';
		assert carry_out = '0';
		
		carry_in <= '1';
		a <= '0';
		b <= '0';
		wait for 10ns;
		assert sum = '1';
		assert carry_out = '0';
		
		carry_in <= '0';
		a <= '1';
		b <= '1';
		wait for 10ns;
		assert sum = '0';
		assert carry_out = '1';
		
		carry_in <= '1';
		a <= '1';
		b <= '1';
		wait for 10ns;
		assert sum = '1';	
		assert carry_out = '1';

      wait;
   end process;
end;
--listing of mux4_16bit.vhd:
library ieee;
use ieee.std_logic_1164.all;

entity mux4_16bit is
    port ( in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
           in2 : in  std_logic_vector (15 downto 0);
           in3 : in  std_logic_vector (15 downto 0);
           addr : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
end mux4_16bit;

architecture behavioral of mux4_16bit is

begin
	process(in0, in1, in2, in3, addr) begin
		case addr is
			when "00" => output <= in0 after 1ns;
			when "01" => output <= in1 after 1ns;
			when "10" => output <= in2 after 1ns;
			when "11" => output <= in3 after 1ns;
			when others => output <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

--listing of mux8_16bit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;
 
entity mux8_16bit_tb is
end mux8_16bit_tb;
 
architecture behavior of mux8_16bit_tb is 
	component mux8_16bit
		port(	s : in  std_logic_vector(2 downto 0);
			in_data : in data_array;
			z : out  std_logic_vector(15 downto 0) );
	end component;
    
	signal s_in : std_logic_vector(2 downto 0) := (others => '0');
	signal data_in : data_array := (others => "0000000000000000");
	signal z_out : std_logic_vector(15 downto 0);
	
	constant test_data : std_logic_vector(15 downto 0) := "0101011001011010";

begin
	uut: mux8_16bit port map (s_in, data_in, z_out);

	stim_proc: process
	begin
		wait for 1 ns;
		--same value on all
		for i in 0 to 7 loop
			data_in(i) <= test_data;
			s_in <= std_logic_vector(to_unsigned(i, 3));
			wait for 1 ns;
			assert z_out = test_data;
		end loop;

		--unique value on each one
		data_in(0) <= x"0123";
		data_in(1) <= x"f12a";
		data_in(2) <= x"e12b";
		data_in(3) <= x"d12c";
		data_in(4) <= x"c12d";
		data_in(5) <= x"b12e";
		data_in(6) <= x"a12f";
		data_in(7) <= x"9120";

		s_in <= "000";
		wait for 1 ns;
		assert z_out = x"0123";

		s_in <= "001";
		wait for 1 ns;
		assert z_out = x"f12a";

		s_in <= "010";
		wait for 1 ns;
		assert z_out = x"e12b";

		s_in <= "011";
		wait for 1 ns;
		assert z_out = x"d12c";

		s_in <= "100";
		wait for 1 ns;
		assert z_out = x"c12d";

		s_in <= "101";
		wait for 1 ns;
		assert z_out = x"b12e";

		s_in <= "110";
		wait for 1 ns;
		assert z_out = x"a12f";

		s_in <= "111";
		wait for 1 ns;
		assert z_out = x"9120";
		
		wait;
	end process;
end;
--listing of mux2_16bit.vhd:
library ieee;
use ieee.std_logic_1164.all;


entity mux2_16bit is
	port (in0 : in  std_logic_vector (15 downto 0);
			in1 : in  std_logic_vector (15 downto 0);
		   addr : in std_logic;
			output : out  std_logic_vector (15 downto 0) );
end mux2_16bit;

architecture behavioral of mux2_16bit is begin
	process(in0, in1, addr)
		begin case addr is
			when '0' => output <= in0 after 1ns;
			when '1' => output <= in1 after 1ns;
			when others => output <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

--listing of mux2_16bit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
 
entity mux2_16bit_tb is
end mux2_16bit_tb;
 
architecture behavior of mux2_16bit_tb is 
	component mux2_16bit
		port (in0 : in  std_logic_vector (15 downto 0);
				in1 : in  std_logic_vector (15 downto 0);
				addr : in std_logic;
				output : out  std_logic_vector (15 downto 0) );
	end component;

	signal addr : std_logic := '0';
	signal in0 : std_logic_vector(15 downto 0) := (others => '0');
	signal in1 : std_logic_vector(15 downto 0) := (others => '0');
	signal output : std_logic_vector(15 downto 0);

begin
	uut: mux2_16bit port map (in0, in1, addr, output);
	
	stim_proc: process begin		
		wait for 1 ns;
		in0 <= x"abab";
		in1 <= x"cfcf";
		
		addr <= '0';
		wait for 1 ns;
		assert (output = x"abab");

		addr <= '1';
		wait for 1 ns;
		assert (output = x"cfcf");	
		
		wait;
	end process;
end;
--listing of mux8_16bit.vhd:
library ieee;
use ieee.std_logic_1164.all;
use work.array_types.all;


entity mux8_16bit is
    port ( s       : in  std_logic_vector (2 downto 0);
           in_data : in  data_array;
           z       : out std_logic_vector (15 downto 0) );
end mux8_16bit;

architecture behavioral of mux8_16bit is begin
	process(s, in_data)
		begin case s is
			when "000" => z <= in_data(0);
			when "001" => z <= in_data(1);
			when "010" => z <= in_data(2);
			when "011" => z <= in_data(3);
			when "100" => z <= in_data(4);
			when "101" => z <= in_data(5);
			when "110" => z <= in_data(6);
			when "111" => z <= in_data(7);
			when others => z <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;--listing of mux4_16bit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
 
entity mux4_16bit_tb is
end mux4_16bit_tb;
 
architecture behavior of mux4_16bit_tb is 
	component mux4_16bit
    port(in0 : in  std_logic_vector(15 downto 0);
         in1 : in  std_logic_vector(15 downto 0);
         in2 : in  std_logic_vector(15 downto 0);
         in3 : in  std_logic_vector(15 downto 0);
         addr : in  std_logic_vector(1 downto 0);
         output : out  std_logic_vector(15 downto 0) );
    end component;
    

   signal in0 : std_logic_vector(15 downto 0) := (others => '0');
   signal in1 : std_logic_vector(15 downto 0) := (others => '0');
   signal in2 : std_logic_vector(15 downto 0) := (others => '0');
   signal in3 : std_logic_vector(15 downto 0) := (others => '0');
   signal addr : std_logic_vector(1 downto 0) := (others => '0');
   signal output : std_logic_vector(15 downto 0);
 
begin
   uut: mux4_16bit port map (in0, in1, in2, in3, addr, output);

   stim_proc: process
   begin
      wait for 10 ns;
		
		--same value on all
		in0 <= "0101011001011010";
		in1 <= "0101011001011010";
		in2 <= "0101011001011010";
		in3 <= "0101011001011010";
		wait for 10ns;
		
		for i in 0 to 3 loop
			addr <= std_logic_vector(to_unsigned(i, 2));
			wait for 1 ns;
			assert output = "0101011001011010";
		end loop;

		--unique value on each one
		in0 <= x"0123";
		in1 <= x"f12a";
		in2 <= x"e12b";
		in3 <= x"d12c";

		s_in <= "000";
		wait for 1 ns;
		assert z_out = x"0123";

		s_in <= "001";
		wait for 1 ns;
		assert z_out = x"f12a";

		s_in <= "010";
		wait for 1 ns;
		assert z_out = x"e12b";

		s_in <= "011";
		wait for 1 ns;
		assert z_out = x"d12c";

      wait;
   end process;

end;
--listing of demux_3to8_1bit.vhd:
library ieee;
use ieee.std_logic_1164.all;


entity demux_3to8_1bit is
   port ( data : in std_logic;
			  addr : in  std_logic_vector (2 downto 0);
           output : out std_logic_vector (7 downto 0) );
end demux_3to8_1bit;

architecture behavioral of demux_3to8_1bit is 
begin
	process(data, addr) begin
		if data = '1' then
			case addr is
				when "000" => output <= "00000001";
				when "001" => output <= "00000010";
				when "010" => output <= "00000100";
				when "011" => output <= "00001000";
				when "100" => output <= "00010000";
				when "101" => output <= "00100000";
				when "110" => output <= "01000000";
				when "111" => output <= "10000000";
				when others => output <= "UUUUUUUU";
			end case;
		else
			output <= "00000000";
		end if;
	end process;
end behavioral;

--listing of demux_3to8_1bit_tb.vhd:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity demux_3to8_1bit_tb is
end demux_3to8_1bit_tb;
 
architecture behavior of demux_3to8_1bit_tb is 
	component demux_3to8_1bit is
	 port ( data : in std_logic;
			  addr : in  std_logic_vector (2 downto 0);
           output : out std_logic_vector (7 downto 0) );
	end component;

	signal addr_in : std_logic_vector(2 downto 0) := (others => '0');
	signal data_in : std_logic_vector(0 downto 0) := "0"; --to get around the "Cannot index the result of a type conversion" silliness
	signal output : std_logic_vector(7 downto 0);

begin
	uut: demux_3to8_1bit port map (data_in(0), addr_in, output);

	stim_proc: process begin		
		wait for 1 ns;
		for d in 0 to 1 loop --data
			data_in <= std_logic_vector(to_unsigned(d, 1));
			for a in 0 to 7 loop --addr
				addr_in <= std_logic_vector(to_unsigned(a, 3));
				wait for 1 ns;
				assert output = std_logic_vector(to_unsigned(d, 8) sll a);
			end loop;
		end loop;
		
		wait;
	end process;
end;
