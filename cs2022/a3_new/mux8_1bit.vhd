library ieee;
use ieee.std_logic_1164.all;

entity mux8_1bit is port (
	input : in  std_logic_vector (7 downto 0);
	selection : in std_logic_vector(2 downto 0);
	output : out  std_logic );
end mux8_1bit;

architecture behavioral of mux8_1bit is begin
	process(input, selection)
		begin case selection is
			when "000" => output <= input(0) after 1ns;
			when "001" => output <= input(1) after 1ns;
			when "010" => output <= input(2) after 1ns;
			when "011" => output <= input(3) after 1ns;
			when "100" => output <= input(4) after 1ns;
			when "101" => output <= input(5) after 1ns;
			when "110" => output <= input(6) after 1ns;
			when "111" => output <= input(7) after 1ns;
			when others => output <= 'U';
		end case;
	end process;
end behavioral;

