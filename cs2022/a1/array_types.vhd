library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package array_types is
	type data_array is array (0 to 7) of STD_LOGIC_VECTOR(15 downto 0);
end package;
