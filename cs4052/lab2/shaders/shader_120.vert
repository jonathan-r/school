#version 120

uniform mat4 matrix;
uniform float time;

attribute vec3 vPosition;
attribute vec3 vNormal;
attribute vec3 vColor;
varying vec3 normal;
varying vec3 color;

void main() {
	vec3 vec = vec3(matrix * vec4(vPosition, 1.0));
    gl_Position = vec4(vec, 1.0);
	normal = vNormal;
	color = vColor;
}
