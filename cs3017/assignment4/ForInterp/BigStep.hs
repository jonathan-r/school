module BigStep where

import Syntax
import State


----------------------------------------------------
----------------------------------------------------
--  Big-Step Interpreter Arithmetic Expressions  ---
----------------------------------------------------
----------------------------------------------------

bigStepExp :: EArith -> State -> Val
bigStepExp (CNum n)    _  = VArith n

bigStepExp (Var name)  st = VArith (stateLookup st name)

bigStepExp (Add e1 e2) st = let (VArith n1) = bigStepExp e1 st in
                            let (VArith n2) = bigStepExp e2 st in
                            let n3        = (n1 + n2) in
                            (VArith n3)

bigStepExp (Sub e1 e2) st = let (VArith n1) = bigStepExp e1 st in
                            let (VArith n2) = bigStepExp e2 st in
                            let n3        = (n1 - n2) in
                            (VArith n3)

bigStepExp (Mul e1 e2) st = let (VArith n1) = bigStepExp e1 st in
                            let (VArith n2) = bigStepExp e2 st in
                            let n3        = (n1 * n2) in
                            (VArith n3)

bigStepExp (Div e1 e2) st = let (VArith n1) = bigStepExp e1 st in
                            let (VArith n2) = bigStepExp e2 st in
                            let n3        = (n1 `div` n2) in
                            (VArith n3)


-----------------------------------------------------
-----------------------------------------------------
--  Big-Step Interpreter for Boolean Expressions  ---
-----------------------------------------------------
-----------------------------------------------------

bigStepBool :: EBool -> State -> Val
bigStepBool (CBool b)   st = VBool b

bigStepBool (And b1 b2) st = let (VBool v1) = bigStepBool b1 st in
                             let (VBool v2) = bigStepBool b2 st in
                             let v3         = (v1 && v2) in
                             (VBool v3)

bigStepBool (Or b1 b2)  st = let (VBool v1) = bigStepBool b1 st in
                             let (VBool v2) = bigStepBool b2 st in
                             let v3         = (v1 || v2) in
                             (VBool v3)

bigStepBool (Eq e1 e2)  st = let (VArith n1) = bigStepExp e1 st in
                             let (VArith n2) = bigStepExp e2 st in
                             let v           = (n1 == n2) in
                             (VBool v)

bigStepBool (Lt e1 e2)  st = let (VArith n1) = bigStepExp e1 st in
                             let (VArith n2) = bigStepExp e2 st in
                             let v           = (n1 < n2) in
                             (VBool v)

bigStepBool (Not b)     st = let (VBool v) = bigStepBool b st in
                             let notV      = not v in
                             (VBool notV)



------------------------------------------------
------------------------------------------
--  Big-Step Interpreter for Commands  ---
------------------------------------------------
------------------------------------------------

bigStepCom :: Com -> State -> State
bigStepCom Skip s = s

bigStepCom (Asgn var exp)       s = let (VArith n) = bigStepExp exp s in
                                    (stateUpdate s var n)

bigStepCom (Seq c1 c2)          s = let s1 = bigStepCom c1 s in
                                    bigStepCom c2 s1

bigStepCom (IfThenElse b c1 c2) s = case (bigStepBool b s) of
                                         (VBool True)  -> bigStepCom c1 s
                                         (VBool False) -> bigStepCom c2 s
                        

bigStepCom (While b c)          s = case (bigStepBool b s) of
                                         (VBool True)  -> let s1 = bigStepCom c s in
                                                          bigStepCom (While b c) s1
                                         (VBool False) -> s

bigStepCom (For var n c)        s = let counter = (stateLookup s var) in
                                        if counter <= n then
                                            --do a round of the loop body
                                            let s1 = bigStepCom c s in --state after the loop body
                                                if (stateLookup s1 var) /= counter then
                                                    error $ "Loop counter changed in loop body"
                                                else
                                                    let s2 = stateUpdate s1 var (counter+1) in
                                                        bigStepCom (For var n c) s2
                                        else --terminate the loop
                                            s
