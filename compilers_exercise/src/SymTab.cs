using System;
using System.Collections.Generic;
 
namespace Tastier { 

	public class Obj { // properties of declared symbol
	   public string name; // its name
	   public int kind;    // var, proc or scope
	   public int type;    // its type if var (undef for proc)
	   public int level;   // lexic level: 0 = global; >= 1 local
	   public int adr;     // address (displacement) in scope 
	   public Obj next;    // ptr to next object in scope
	   // for scopes
	   public Obj outer;   // ptr to enclosing scope
	   public Obj locals;  // ptr to locally declared objects
	   public int nextAdr; // next free address in scope
	}

	public abstract class ASVisitor {
		abstract void visit (ASNode node);
	}
		
	public abstract class ASNode {
		public abstract void visitAll (ASVisitor visitor);
	}

	public abstract class Statement {
		Statement next;
		void visitAll(ASVisitor visitor) {
			visitor.visit (this);
			if (next != null)
				next.visit (visitor);
		}
	}

	public class IfWhileStat : Statement {

	}

	public class IoStat : Statement {
	}

	public class ASIdent : ASNode {
	}




	public abstract class SSANode {
	}

	public abstract class SSAValue {
	}

	public class SSAVar : SSAValue {
		ASIdent ident;
		int id;
		LinkedList<SSANode> usages;
		bool constness;

	}

	public abstract class SSAExpr {
		
	}

	public class SSAPhi : SSAExpr {
		LinkedList<SSAValue> vals;
	}

	public class SSAAssign : SSANode {
		SSAVar dest;
		SSAExpr expression;
	}

	public class SymbolTable {

	   const int // object kinds
	      var = 0, proc = 1, scope = 2; 

	   const int // types
	      undef = 0, integer = 1, boolean = 2;

	   public Obj topScope; // topmost procedure scope
	   public int curLevel; // nesting level of current scope
	   public Obj undefObj; // object node for erroneous symbols
	   
	   Parser parser;
	   
	   public SymbolTable(Parser parser) {
	      curLevel = -1; 
	      topScope = null;
	      undefObj = new Obj();
	      undefObj.name = "undef";
	      undefObj.kind = var;
	      undefObj.type = undef;
	      undefObj.level = 0;
	      undefObj.adr = 0;
	      undefObj.next = null;
	      this.parser = parser; 
	   }

	// open new scope and make it the current scope (topScope)
	   public void OpenScope() {
	      Obj scop = new Obj();
	      scop.name = "";
	      scop.kind = scope; 
	      scop.outer = topScope; 
	      scop.locals = null;
	      scop.nextAdr = 0;
	      topScope = scop; 
	      curLevel++;
	   }

	// close current scope
	   public void CloseScope() {
	      topScope = topScope.outer;
	      curLevel--;
	   }

	// create new object node in current scope
	   public Obj NewObj(string name, int kind, int type) {
	      Obj p, last; 
	      Obj obj = new Obj();
	      obj.name = name; obj.kind = kind;
	      obj.type = type; obj.level = curLevel; 
	      obj.next = null; 
	      p = topScope.locals; last = null;
	      while (p != null) { 
	         if (p.name == name)
	            parser.SemErr("name declared twice");
	         last = p; p = p.next;
	      }
	      if (last == null)
	         topScope.locals = obj; else last.next = obj;
	      if (kind == var)
	         obj.adr = topScope.nextAdr++;
	      return obj;
	   }

	// search for name in open scopes and return its object node
	   public Obj Find(string name) {
	      Obj obj, scope;
	      scope = topScope;
	      while (scope != null) { // for all open scopes
	         obj = scope.locals;
	         while (obj != null) { // for all objects in this scope
	            if (obj.name == name) return obj;
	            obj = obj.next;
	         }
	         scope = scope.outer;
	      }
	      parser.SemErr(name + " is undeclared");
	      return undefObj;
	   }

	} // end SymbolTable

} // end namespace
