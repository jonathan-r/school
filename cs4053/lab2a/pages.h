#pragma once

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <stdio.h>
#include <stdarg.h>
#include <limits>
#include <string>
#include <algorithm>

#include "name_template.h"
#include "bound.h"

//UI element names
#define UI_IMG_ID "image sequence id"

#define CANNY_LOW 50
#define CANNY_HIGH 150
#define CMP_SIZE 600

//page_detect.cpp
bool raysIntersect(const cv::Point2f& p1, const cv::Point2f& p2, const cv::Point2f& D1, const cv::Point2f& D2, cv::Point2f& out);
void orderPointsNearest(const std::vector<cv::Point2f>& points, std::vector<cv::Point2f>& ordered, std::vector<float>& dist);
bool extractPage(const cv::Mat& bluePixels, double thresh, double normalizeVal, int dilations, int histBins, const cv::Mat& testImg, float outSize, cv::Mat& isolatedSquarePage, cv::Mat* debugImg = nullptr);

//pages.cpp
void trackbarChange(int, void*);
void drawText(cv::Mat& on, const cv::Point& p, const char* fmt, ...);
void loadTemplates(NameTemplate& namePattern, std::vector<cv::Mat>& cannied);
bool matchPage(const cv::Mat& sourceImage, const std::vector<cv::Mat>& referenceImages, const cv::Mat& bluePixels, int& indexMatched, cv::Mat* debugImg = nullptr);
int getGroundTruth(int idx);