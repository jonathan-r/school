void del(node* r, node** q){
	if (r->right) del(r->right, q);
	else{
		memcpy(*q, r, sizeof(node));
		*q = r;
		r = r->left;
	}
}

void delete(int x, node** p){
	node* n = *p;
	
	if (p){
		if (x < n->key) delete(x, n->left);
		else if (x > n->key) delete(x, n->right);
		else{
			//this is the node
			
			
			if (n->left && n-right){
				//non-trivial
				node* q = n;
				del(n->left, &q);
			}
			
			else{
				//simply replace this node with the node that's not null
				if (!n->left) *p = n->right;
				else if (!n->right) *p = n->left;
				free(n);
			}
		}
	}
}
