.text

// sample pressing one of the switches makes the "next" of the 4 LEDs P1.16, P1.17, P1.18, P1.19
// go on and off in sequence
// (c) Mike Brady, 2012.

// this program sample is only partically complete:
// it does not wait for the end of the switch press

//.EXPORT	start_asm
.global start_asm
start_asm:

.equ IO1DIR, 0xE0028018
.equ IO1SET, 0xE0028014
.equ IO1CLR, 0xE002801C
.equ IO1PIN, 0xE0028010

		ldr	r1,=IO1DIR
		ldr	r2,=0x000f0000	//select P1.19--P1.16
		str	r2,[r1]			//make them outputs

		ldr	r1,=IO1SET
		str	r2,[r1]			//set them to turn the LEDs off
		ldr	r2,=IO1CLR

// r1 points to the SET register
// r2 points to the CLEAR register

//		ldr	r5,=0x00100000	// end when the mask reaches this value
//wloop	ldr	r3,=0x0		// clear everything //=0x00010000	// start with P1.16.
//floop	str	r3,[r2]	   		// clear the bit -> turn on the 

		ldr	r0,=IO1PIN
		ldr	r1,=IO1SET
repeek:
		ldr	r6,[r0]
		//ldr	r7,=0x00F00000	// mask for switch bits
		
		mov r6, r6, lsr #20	//each bit in r7 corresponds to a button state
		and r6, r6, #0xF//#0x000F0000
		eor r6, r6, #0xF

		mov r3, #0
		mov r5, #0x00010000
ledloop:
		mov r4, r6, lsr r3	
		and r4, r4, #0x1

		cmp r4, #1
		streq r5, [r2]
		strne r5, [r1]

		mov r5, r5, lsl #1

		add r3, r3, #1
		cmp r3, #5
		bne ledloop

		b repeek

stop:	B	stop

.END
