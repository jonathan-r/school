#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <ncurses.h>

#include "twister.h"

const int num_printers = 3;
int page_width; //in characters
int page_height; //in characters

int log_height; //from the bottom of the page
int log_at; //line on which the next log line will be printed

//print job
typedef struct print_job_s{
	char hash[16];
	int ink_color;
	int page_color;
} print_job;

//queue
typedef struct queue_node_s{
	struct queue_node_s* next;
	print_job job;
	int dest;
} queue_node;

//semaphore
pthread_mutex_t printer_wait_lock;
int printers_available;
queue_node* queue_first; //highest priority job
pthread_mutex_t queue_lock; //queue is protected by this


//this is only locked for single, atomic (screen) print operations, not part of the semaphore system
pthread_mutex_t screen_lock;
void log_message(const char* format, ...){
	va_list args;
	pthread_mutex_lock(&screen_lock);

	va_start(args, format);
	move(log_at, 0);
	vwprintw(stdscr, format, args);
	va_end(args);

	log_at++;
	if (log_at == LINES) log_at = LINES - log_height;

	pthread_mutex_unlock(&screen_lock);
}

//printer thread.
//one instance for each printer
typedef struct printer_data_s{
	int available;
	int jobs_done;
	print_job* job;
	char* page_buffer;
} printer_data;

void* printer(void* data){
	printer_data* self = data;

	while (1){
		self->available = 1;

		pthread_mutex_lock(&wait_mutex); //one by one the threads will acquire this lock
		pthread_cond_wait(&wait_signal, &wait_mutex);
		pthread_mutex_unlock(&wait_mutex);

		//usleep(2000);

		//do the deed
		memset(self->page_buffer, 0, page_width * page_height);

		//clear the page
		for (int l = 0; l < page_height; l++){

		}

		

		free(self->job);
	}

	pthread_exit(NULL);
}


typedef struct client_data_s{
	int seed;
	int jobs_sent;
} client_data;
void* client(void* data){
	client_data* self = data;

	twister rand;
	twister_init(&rand, self->seed);

	while(1){
		if (self->jobs_sent > 10){
			if ((twister_uint(&rand) & 0xFF) == 0) //one in 256
				break;
		}

		//design a page
		print_job* job = malloc(sizeof(print_job));
		int points = 10 + (twister_uint(&rand) % 40);



		self->jobs_sent++;
	}
}

void terminal_resize(int sig){
	//just don't
	exit(1);
}

int main(){
	initscr(); //ncurses

	page_width = COLS / num_printers;
	page_height = LINES * 2 / 3;

	//ncurses terminal resize handler
	struct sigaction sa;
	memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_handler = terminal_resize;
	sigaction(SIGWINCH, &sa, NULL);

	while (1){ //busywork
		pthread_mutex_lock(&screen_lock);
		int key = getch();
		pthread_mutex_unlock(&screen_lock);
		if (key == 27) break;
	}

	//kill all threads here

	endwin();
	return 0;
}