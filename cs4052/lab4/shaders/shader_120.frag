#version 120

uniform float lightFraction;

varying vec3 normal;
varying vec3 color;

void main() {
	//float ndcDepth = (2.0 * gl_FragCoord.z - gl_DepthRange.near - gl_DepthRange.far) / (gl_DepthRange.far - gl_DepthRange.near);
	//float cd = ndcDepth / gl_FragCoord.w;

	float lum = dot(normal, vec3(0.5, 0.5, 0.5));
	vec3 final = mix(color, lum * color, lightFraction); //lerp
	gl_FragColor = vec4(final, 1.0);
}
