import numpy as np

"""
f(x, y)_i: (x - a_i) ^ 2 + (y - b_i) ^ 2 = d_i ^ 2
partial df_i/dx: 2(x - a_i)
partial df_i/dy: 2(x - b_i)
""" 

def jacobian(est, positions, distances):
    """
    Partial derivatives, evaluated with values of the current estimate
    
    Arguments:
    est       - an estimate solution
    positions - position of transmitters
    distances - observed distances (not used)
    
    Returns (i by 2) jacobian matrix as a row-major numpy.array object
    """
    
    rows = [] 
    for pos in positions:
        rows.append([2 * (est[0] - pos[0]), 2 * (est[1] - pos[1])])
        
    return np.array(rows)


def residuals(est, positions, distances):
    """
    Residuals of current estimate
    
    Arguments:
    est       - an estimate solution
    positions - position of transmitters
    distances - observed distances
    
    Returns (lenght i) residuals as a vector
    """
    
    resid = [] #vector
    for (pos, dis) in zip(positions, distances):
        #(distance from transmitter to estimate ^ 2) - f(pos)
        resid.append(((pos[0] - est[0]) ** 2 + (pos[1] - est[1]) ** 2) - dis ** 2)
        
    return np.array(resid)

class CannotConvergeException(Exception):
    pass

def solve_system(initial_guess, transmitters, distances, tolerance = 1e-5, max_iterations = 150):
    """
    Newton-Raphson solver
    
    Arguments:
    initial_guess  - an initial guess of type np.array
    transmitters     - position of transmitters
    distances      - observed distances
    tolerance      - convergence epsilon (iteration stops when dx is less than this)
    max_iterations - a strict upper bound on the number of iterations (if convergence is not achieved)

    Returns a generator object that yields either:
    a (current guess, delta from previous guess) tuple for each iteration until convergence is reached,
    or a (curent guess, False) tuple when convergence isn't reached within max_iterations
    """
    
    guess = initial_guess
    for i in range(max_iterations):
        jacob = jacobian(guess, transmitters, distances)
        resids = residuals(guess, transmitters, distances)
        
        #Moore-Penrose pseudo-inverse of the jacobian
        #       T   -1  T
        #iJ = (J  J)   J
        inv_j = np.linalg.pinv(jacob)
        
        #J * dx = r
        #iJ * J * dx = iJ * r
        #I * dx = iJ * r
        #dx = Ji * r
        delta = np.dot(inv_j, resids)
        
        #x_n = x_(n-1) - dx
        guess = guess - delta
        yield (guess, delta)
        
        #convergence check
        #if np.all(np.abs(delta) < tolerance):
        if np.linalg.norm(delta) < tolerance:
            return #stop yielding values, exit cleanly
        
    #did not converge
    raise CannotConvergeException
