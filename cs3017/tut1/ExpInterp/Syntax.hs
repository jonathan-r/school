module Syntax where

-- Arithmetic Expressions
data Exp = CNum Integer  -- n
         | Add Exp Exp  -- e + e
         | Sub Exp Exp  -- e - e
         | Mul Exp Exp  -- e * e
         | Div Exp Exp  -- e / e

-- Values
data Val = VNum Integer  -- n




instance Show Exp where
  show (CNum i)     = show i
  show (Add e1 e2)  = "(" ++ show e1 ++ " + " ++ show e2 ++ ")"
  show (Sub e1 e2)  = "(" ++ show e1 ++ " - " ++ show e2 ++ ")"
  show (Mul e1 e2)  = "(" ++ show e1 ++ " * " ++ show e2 ++ ")"
  show (Div e1 e2)  = "(" ++ show e1 ++ " / " ++ show e2 ++ ")"

instance Show Val where
  show (VNum i)     = show i


-- Boolean Expressions
data BExp = CTrue
          | CFalse
          | And BExp BExp
          | Or BExp BExp
          | Not BExp
          deriving Show
-- While Statements
