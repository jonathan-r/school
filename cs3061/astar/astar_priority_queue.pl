arc(N, M, Seed, Cost) :- M is N * Seed, Cost = 1.
arc(N, M, Seed, Cost) :- M is N * Seed + 1, Cost = 2.

goal(N, Target) :- 0 is N mod Target.

h(N, Hvalue, Target) :-
	goal(N, Target), !, 
	Hvalue is 0;
	Hvalue is 1/N.

arc_add(N, M, Seed, NCost, MCost) :-
	arc(N, M, Seed, XCost),
	MCost is NCost + XCost.

% search(+PQ, +Seed, +Target, ?Found)
search(t([N, _], _, _), _, Target, N) :- goal(N, Target).
search(FPQIn, Seed, Target, Found) :-
	remove(FPQIn, Target, [N, NCost], FPQMinLess),
	setof([M, MCost], arc_add(N, M, Seed, NCost, MCost), FNodes),
	add_to_frontier(FNodes, FPQMinLess, Target, FPQNew),
	search(FPQNew, Seed, Target, Found).

% less_than(+Node1, +Node2, +Target), negation as failure
less_than([Node1, Cost1], [Node2, Cost2], Target) :-
	h(Node1, Hvalue1, Target), h(Node2, Hvalue2, Target),
	F1 is Cost1 + Hvalue1, F2 is Cost2 + Hvalue2,
	F1 =< F2. %TODO: might have to be < instead

% insert(+Item, +Pqueue, +Target, ?NewPQ)
insert(Item, [], _, t(Item, [], [])).
insert(Item, t(T, L, R), Target, NPQ) :-
	less_than(T, Item, Target), !,
	insert(Item, R, Target, NR), NPQ = t(T, NR, L);
	insert(T, R, Target, NR), NPQ = t(Item, NR, L).

% remove(+PQueue, +Target, ?Item, ?NewPQ)
remove(t(Item, L, []), _, Item, L).
remove(t(T, t(LT, LL, LR), t(RT, RL, RR)), Target, T, NPQ) :-
	less_than(LT, RT, Target), !,
	remove(t(LT, LL, LR), Target, NT, NL), NPQ = t(NT, t(RT, RL, RR), NL);
	remove(t(RT, RL, RR), Target, NT, NR), NPQ = t(NT, t(LT, LL, LR), NR).

listToPq([], PQ, _, PQ).
listToPq([H | T], PQIn, Target, PQOut) :-
	insert(H, PQIn, Target, NPQ),
	listToPq(T, NPQ, Target, PQOut).

% listToPq(+List, +Target, ?PQueue)
listToPq(Ar, Target, PQ) :-
	listToPq(Ar, [], Target, PQ).

% pqToList(+PQ, +Target, ?List)
pqToList([], _, []).
pqToList(PQ, Target, Out) :-
	remove(PQ, Target, Item, NPQ),
	pqToList(NPQ, Target, NL),
	append([Item], NL, Out).

add_to_frontier([], PQ, _, PQ).
add_to_frontier([H | Rest], PQIn, Target, PQOut) :-
	insert(H, PQIn, Target, PQPart),
	add_to_frontier(Rest, PQPart, Target, PQOut).

%a_star(+Start, +Seed, +Target, ?Found)
a-star(Start, Seed, Target, Found):-
	search(t([Start, 0], [], []), Seed, Target, Found).
