library ieee;
use ieee.std_logic_1164.all;

entity function_unit_tb is
end function_unit_tb;
 
architecture behavior of function_unit_tb is 
    component function_unit
    port(a : in  std_logic_vector(15 downto 0);
         b : in  std_logic_vector(15 downto 0);
         func : in  std_logic_vector(4 downto 0);
         v : out  std_logic;
         c : out  std_logic;
         n : out  std_logic;
         z : out  std_logic;
         f : out  std_logic_vector(15 downto 0) );
    end component;
    
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');
   signal func : std_logic_vector(4 downto 0) := (others => '0');
	
   signal v : std_logic;
   signal c : std_logic;
   signal n : std_logic;
   signal z : std_logic;
   signal f : std_logic_vector(15 downto 0);
	
	constant delay : time := 50ns;
 
begin
   uut: function_unit port map (a, b, func, v, c, n, z, f);

   stim_proc: process
   begin
		wait for delay;
	
		report "arithmetic, difference";
		a <= x"af3d"; --1010 1111 0011 1101
		b <= x"c7ac"; --1100 0111 1010 1100
		func <= "00101";
		wait for delay;
		assert f = x"e791"; --1110 0111 1001 0001
		assert v = '0';
		assert c = '0';
		assert n = '1';
		assert z = '0';
		
		report "arithmetic, difference, zero";
		func <= "00101";
		b <= x"af3d"; --1010 1111 0011 1101
		wait for delay;
		assert f = x"0000";
		assert v = '0';
		assert c = '1';
		assert n = '0';
		assert z = '1';
		
		report "logic, xor";
		b <= x"c7ac"; --1100 0111 1010 1100
		func <= "01100";
		wait for delay;
		assert f = x"6891"; --0110 1000 1001 0001
		assert n = '0';
		assert z = '0';
		
		report "shift, left";
		b <= x"c7ac"; --1100 0111 1010 1100
		func <= "11000";
		wait for delay;
		assert f = x"8f58"; --1000 1111 0101 1000
		assert n = '1';
		assert z = '0';
		
		wait;
   end process;
end;
