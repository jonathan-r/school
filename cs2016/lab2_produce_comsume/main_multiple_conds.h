#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <unistd.h> //sysconf
#include <pthread.h> //threads

#include "ring.h"

pthread_mutex_t printf_lock;
pthread_cond_t* wait_signals = NULL; //as many as there are taker threads
pthread_mutex_t* wait_mutexes = NULL; //as many as there are taker threads
int turn = 0; //the next taker thread to be woken up when there's data

ring_buffer ring;

void atomic_fprintf(FILE* file, const char* format, ...){
	va_list args;

	pthread_mutex_lock(&printf_lock);

	va_start(args, format);
	vfprintf(file, format, args);
	va_end(args);

	pthread_mutex_unlock(&printf_lock);
}

typedef struct worker_data_s{
	int index;
} worker_data;


void giver(int takers, FILE* input_file){
	const int buf_size = 1024;
	char buf[buf_size];

	int clearance = ring_space_left(&ring);
	int get = (clearance <= buf_size - 1)? clearance + 1 : buf_size;

	while(!feof(input_file)){
		char* f = fgets(buf, get, input_file); //blocking until some data arrives
		if (f == buf){
			int got = strlen(buf);

			if (got > 0){
				ring_put(&ring, buf, got);
				//TODO: testing here
			}

			else{
				atomic_fprintf(stderr, "somehow, got no data\n");
				exit(1);
			}
		}

		else break; //no more data

		while (ring_data_left(&ring) > 0){
			//wake up the thread whose turn it is
			pthread_mutex_lock(&wait_mutexes[turn]);
			pthread_cond_signal(&wait_signals[turn]);
			pthread_mutex_unlock(&wait_mutexes[turn]);
			turn++;

			if(turn == takers) turn = 0;
		}
	}

	//wake up all the workers cause it's time to die
	for (int i = 0; i < takers; i++){
		pthread_mutex_lock(&wait_mutexes[turn]);
		pthread_cond_signal(&wait_signals[turn]);
		pthread_mutex_unlock(&wait_mutexes[turn]);
	}
}


void* taker(void* data){
	worker_data* self = data;
	int last_get = time(NULL);

	const int buffer_size = 128;
	char buffer[buffer_size + 1];

	int running = 1;

	while (running){
		//read some stuff
		pthread_mutex_lock(&wait_mutexes[self->index]); //one by one the threads will acquire this lock
		pthread_cond_wait(&wait_signals[self->index], &wait_mutexes[self->index]);
		printf("thread %d: cond woke up\n", self->index);

		//do stuff here
		int got = ring_get(&ring, buffer, buffer_size);
		buffer[got] = '\0';

		if (got > 0){
			atomic_fprintf(stdout, "thread %d: \"%s\"\n", buffer);
		}

		else{
			atomic_fprintf(stdout, "thread %d: bye\n");
			running = 0;
		}

		pthread_mutex_unlock(&wait_mutexes[self->index]);
	}

	pthread_exit(NULL);
}


int main(int argc, char** argv){
	if (argc != 2 && argc != 3){
		printf("Usage: distribute <ring_size> [threads]\n");
		printf("If not specified, threads is the number of hardware threads\n");
	}

	else{
		int num_takers = 0;
		int ring_size = 0;

		ring_size = strtol(argv[2], NULL, 10);

		if (argc == 3)
			num_takers = strtol(argv[2], NULL, 10);

		else
			num_takers = sysconf(_SC_NPROCESSORS_ONLN);


		if (num_takers > 0 && ring_size > 0){
			ring_init(&ring, ring_size);

			pthread_t* handles = malloc(num_takers * sizeof(pthread_t));
			worker_data* data = malloc(num_takers * sizeof(worker_data));

			wait_signals = malloc(num_takers * sizeof(pthread_cond_t));
			wait_mutexes = malloc(num_takers * sizeof(pthread_mutex_t));

			for (int i = 0; i < num_takers; i++){
				//dynamically initialize mutexes and condition variables
				pthread_cond_init(&wait_signals[i], NULL);
				pthread_mutex_init(&wait_mutexes[i], NULL);
				pthread_create(&handles[i], NULL, taker, &data[i]);
			}
			
			for (int i = 0; i < num_takers; i++){
				pthread_join(handles[i], NULL);
				//printf("thread %d: %f\n", i, data[i].fraction);

				pthread_mutex_destroy(&wait_mutexes[i]);
				pthread_cond_destroy(&wait_signals[i]);
			}

			free(wait_signals);
			free(wait_mutexes);
			free(handles);
			free(data);
		}

		else{
			if (ring_size <= 0) fprintf(stderr, "ring_size must be greater than 0.\n");
			if (num_takers <= 0) fprintf(stderr, "threads must be greater than 0.\n");
		}
	}

	return 0;
}
