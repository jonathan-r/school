#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct nodeStruct{
	int number;
	int neighbourCount;
	struct nodeStruct** neighbours;
} node;

typedef struct linkStruct{
	int a, b;
} link;

const int maxNodes = 20;
node nodes[maxNodes];
node* links[maxNodes * maxNodes];

#define MARK 0x80000000
#define MASK 0x000000FF


void remove_mark(node* from, node* at){
	at->number &= MASK;
	
	for (int i = 0; i < at->neighbourCount; i++){
		node* to = at->neighbours[i];
		if (to != from){ //don't go back directly
			if (to->number & MARK){
				remove_mark(at, to);
			}
		}
	}
}


void spanning_tree_marking(node* from, node* n, link** keeping, link** deleting){
	//mark it once
	int self = n->number;
	n->number |= MARK;
	
	for (int i = 0; i < n->neighbourCount; i++){
		node* to = n->neighbours[i];
		if (to != from){ //don't go back directly
			int other = to->number & MASK;

			if (to->number & MARK){
				(*deleting)->a = self;
				(*deleting)->b = other;
				(*deleting)++; //increment end pointer
			}
		}
	}
	

	for (int i = 0; i < n->neighbourCount; i++){
		node* to = n->neighbours[i];
		if (to != from && !(to->number & MARK)){ //don't go back directly
			int other = to->number & MASK;

			(*keeping)->a = self;
			(*keeping)->b = other;
			(*keeping)++; //increment end pointer

			spanning_tree_marking(n, to, keeping, deleting);
		}
	}
}


//O(N) for a N vertices
void spanning_tree(node* start){
	link keep[20];
	link delete[20];

	link* keep_end = keep;
	link* delete_end = delete;

	spanning_tree_marking(NULL, start, &keep_end, &delete_end);

	printf("keep:  ");
	for (link* k = keep; k < keep_end; k++)
		printf(" {%d %d}", k->a, k->b);
	
	printf(".\n");
	
	printf("remove:");
	for (link* d = delete; d < delete_end; d++)
		printf(" {%d %d}", d->a, d->b);

	printf(".\n");
}


void connect(node* a, node* b){
	a->neighbours[a->neighbourCount++] = b;
	b->neighbours[b->neighbourCount++] = a;
}

int main(){
	int i;
	for (i = 0; i < maxNodes; i++){
		nodes[i].number = i;
		nodes[i].neighbourCount = 0;
		nodes[i].neighbours = &links[i * maxNodes];
	}

	memset(links, 0, maxNodes * maxNodes * sizeof(node*));

	/*connect(&nodes[0], &nodes[1]);
	connect(&nodes[0], &nodes[2]);
	connect(&nodes[2], &nodes[3]);
	connect(&nodes[2], &nodes[5]);
	connect(&nodes[3], &nodes[6]);
	//connect(&nodes[3], &nodes[4]);
	connect(&nodes[4], &nodes[5]);
	connect(&nodes[4], &nodes[7]);
	connect(&nodes[7], &nodes[8]);*/

	connect(&nodes[0], &nodes[3]);
	connect(&nodes[0], &nodes[6]);
	connect(&nodes[3], &nodes[2]);
	connect(&nodes[2], &nodes[1]);
	connect(&nodes[1], &nodes[6]);
	connect(&nodes[5], &nodes[6]);
	connect(&nodes[5], &nodes[4]);
	connect(&nodes[4], &nodes[6]);
	
	connect(&nodes[4], &nodes[3]);

	node* start = &nodes[0];
	spanning_tree(start);

	return 0;
}
