import java.lang.Thread;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;

import java.io.File;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.util.ArrayList;

public class Sender {
	private static final int STATE_SEND_FILE_HEADER = 0;
	private static final int STATE_SEND_FILE_DATA = 1;
	private static final int STATE_WAIT_FOR_ACK = 2;
	
	private int id; //this is used to differentiate between senders
	private DatagramSocket socket;
	private InetSocketAddress dstAddress;
	private int ownPort;
	private int dstPort;
	private DatagramPacket[] window; //a ringbuffer
	private int packetsStored;
	private int highestRequest;
	
	private Listener listener;
	
	Sender(String dstHost, int srcPort, int destPort, int windowSize) {
		try {
			ownPort = srcPort;
			dstPort = destPort;
			dstAddress = new InetSocketAddress(dstHost, dstPort);
			listener = new Listener(srcPort, dstAddress.getAddress()); //construct the receiver first so that it binds the destination port first (and the random port given to this.socket is not the same)
		
			socket = new DatagramSocket();
			socket.setSoTimeout(1000);
			
			window = new DatagramPacket[windowSize];
			packetsStored = 0;
			highestRequest = 0;
			
			listener.start();
		}
		
		catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}
	
	boolean initializeConnection() throws java.lang.Exception{
		System.out.println("Initializing connection to " + dstAddress.getHostName() + ":" + dstAddress.getPort());
	
		for (int i = 1; i <= 5; i++){
			System.out.print("Attempt " + i + " .. ");
			
			ByteArrayOutputStream bytesStream = new ByteArrayOutputStream(Symbols.PACKET_SIZE);
			DataOutputStream out = new DataOutputStream(bytesStream);
			
			out.writeByte(Symbols.PACKET_SENDER_INIT);
			out.writeShort(0); //sequence id does not matter
			out.writeShort(0); //sender id is unknown as of yet
			out.writeShort(ownPort); //own receiving port
			out.writeShort(window.length); //window size
			
			byte[] bytes = bytesStream.toByteArray();
			DatagramPacket packet = new DatagramPacket(bytes, bytes.length, dstAddress);
			socket.send(packet);
			
			Thread.sleep(500 * i);
			
			DatagramPacket ack;
			while ((ack = listener.getPacket()) != null){
				System.out.println("success");
				
				ByteArrayInputStream inBytes = new ByteArrayInputStream(ack.getData());
				DataInputStream in = new DataInputStream(inBytes);
				
				if (in.readByte() == Symbols.PACKET_ACK_INIT){
					in.readUnsignedShort(); //ignore sequence
					id = in.readUnsignedShort();
					
					System.out.println("Connection initialized with sender id: " + id);
					return true;
				}
				
				else{
					System.out.println("wrong kind of packet");
				}
			}
			
			System.out.println("time out");
		}
		
		listener.kill();
		return false;
	}
	
	private void sendWindow(int upTo){
		System.out.println("sending window of " + upTo + " packets");
	
		for (int t = 0; t < 1000; t++){
			//send every unacknowledged window packet
			for (int i = highestRequest; i < upTo; i++){
				try{
					if (Math.random() < 0.3) socket.send(window[i]);
					Thread.sleep(1);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
			try{
				Thread.sleep(t / 10 + 10);
			}
			catch(java.lang.InterruptedException e){
			}
			
			DatagramPacket ack;
			while ((ack = listener.getPacket()) != null){
				byte[] data = ack.getData();
			
				if (data[0] == Symbols.PACKET_ACK_SEQ){
					int ackSeq = Symbols.bytesToShort(data[1], data[2]);
					if (ackSeq > highestRequest) highestRequest = ackSeq;
					System.out.println("Received ack for packet " + ackSeq);
				}
				else{
					System.out.println("Received the wrong kind of ack packet");
				}
			}
			
			if (highestRequest >= upTo){
				return;
			}
		}
		
		System.out.println("Timed out on sending");
		System.exit(1);
	}
	
	private void sendPacket(byte type, byte[] data){
		//fill in the packet header
		byte[] buffer = new byte[Symbols.PACKET_SIZE];
		buffer[0] = type;
		Symbols.writeShortToBytes(packetsStored, buffer, 1);
		Symbols.writeShortToBytes(id, buffer, 3);
		
		//copy the data
		System.arraycopy(data, 0, buffer, Symbols.PACKET_HEADER_SIZE, data.length);
		
		//construct and store the packet so it can be sent at least once
		DatagramPacket packet = null;
		try {
			packet = new DatagramPacket(buffer, Symbols.PACKET_SIZE, dstAddress);
		}
		catch(java.net.SocketException e){
			System.out.println("could not create packet");
			e.printStackTrace();
			System.exit(1);
		}
		
		window[packetsStored++] = packet;
		
		if (packetsStored == window.length){
			//make sure the window gets to the destination before sending the next packet
			sendWindow(window.length);
			
			//reset window
			highestRequest = 0;
			packetsStored = 0;
		}
	}
	
	private void finishSending(){
		//if there are any remaining packets, not enough to complete a window, send them
		if (packetsStored > 0){
			System.out.println("" + packetsStored + " packets left");
			sendWindow(packetsStored);
		}
	}

	void sendFile(String path) {
		int fileChunkSize = Symbols.PACKET_SIZE - Symbols.PACKET_HEADER_SIZE;
		byte[] fileChunk = new byte[fileChunkSize];
		
		try {
			File file = new File(path);
			long fileSize = file.length();
			FileInputStream fileIn = new FileInputStream(file);
			
			//make a file header packet
			ByteArrayOutputStream bytes = new ByteArrayOutputStream(Symbols.PACKET_SIZE);
			DataOutputStream out = new DataOutputStream(bytes);
			
			out.writeLong(fileSize);
			String fileName = file.getName();
			out.writeUTF(fileName); //TODO:this might overflow the buffer
			
			byte[] headerBytes = bytes.toByteArray();
			sendPacket(Symbols.PACKET_FILE_HEADER, headerBytes);
			
			//now send the file, chunk by chunk
			long bytesSent = 0;
			do {
				int read = fileIn.read(fileChunk, 0, fileChunkSize);
				System.out.println("read " + read + " bytes from file");
				sendPacket(Symbols.PACKET_FILE_DATA, fileChunk);
				bytesSent += read;
				System.out.println("sent " + bytesSent + " out of " + fileSize);
			}
			while (bytesSent < fileSize);
			
			finishSending();
			listener.kill();
			
			System.out.println("Send complete");
		}
		catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Sender s;
		try {
			if (args.length == 3 || args.length == 4) {
				String dstHost = args[0];
				int destPort = Symbols.DEFAULT_RECEIVER_PORT;
				int colon = dstHost.indexOf(':');
				try{
					if (colon != -1){
						dstHost = args[0].substring(0, colon);
						destPort = Integer.parseInt(args[0].substring(colon + 1));
					}
				}
				catch (Exception e){
					System.out.println("Badly formed host specification");
					return;
				}
				
				int srcPort = Integer.parseInt(args[1]);
				int windowSize = Symbols.DEFAULT_WINDOWS_SIZE;
				if (args.length == 4) windowSize = Integer.parseInt(args[3]);
				
				s = new Sender(dstHost, srcPort, destPort, windowSize);
				
				if (s.initializeConnection()){
					System.out.println("Connection established");
					s.sendFile(args[2]);
				}
			
				else{
					System.out.println("Could not establish connection");
				}
			}
			
			else{
				System.out.println("Usage: sender <destination host [: port]> <own port> <file path> [window size]");
				System.out.println("Destination port defaults to " + Symbols.DEFAULT_RECEIVER_PORT);
				return;
			}
		}

		catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}
}
