#pragma once

#include "asset.h"

#include "vertex.h"
#include "object.h"
#include "maths/mat4.h"
#include "maths/quat.h"

class Plane {
public:
	Plane() {
		bodyOb.model = bodyMesh;
		bodyOb.texture = bodyTexture;

		rudderOb.model = rudderMesh;
		rudderOb.texture = bodyTexture;
		rudderOb.stem.set(0, -0.29f, -7.69f); //offset
		bodyOb.shoots.push_back(&rudderOb);

		elevatorOb.model = elevatorMesh;
		elevatorOb.texture = bodyTexture;
		elevatorOb.stem.set(0, 0.15f, -7.39f); //offset
		bodyOb.shoots.push_back(&elevatorOb);

		wheelLOb.model = wheelMesh;
		wheelLOb.texture = bodyTexture;
		wheelLOb.stem.set(0.92f, -1.89f, 0); //offset
		bodyOb.shoots.push_back(&wheelLOb);

		wheelROb.model = wheelMesh;
		wheelROb.texture = bodyTexture;
		wheelROb.stem.set(-0.92f, -1.89f, 0); //offset
		bodyOb.shoots.push_back(&wheelROb);

		propOb.model = propMesh;
		propOb.texture = bodyTexture;
		propOb.stem.set(0, 0, 1); //offset
		bodyOb.shoots.push_back(&propOb);
	}

	~Plane() {

	}

	static void init() {
		bodyMesh = loadMesh("plane_body.obj");
		rudderMesh = loadMesh("rudder.obj");
		elevatorMesh = loadMesh("elevator.obj");
		wheelMesh = loadMesh("wheel.obj");
		propMesh = loadMesh("prop.obj");
		bodyTexture = loadTexture("HSVS255.jpg"); //placeholder
	}

	static void finalize() {
		delete bodyMesh;
		delete rudderMesh;
		delete elevatorMesh;
		delete wheelMesh;
		delete propMesh;
		delete bodyTexture;
	}

	Object& getObj() {
		return *(dynamic_cast<Object*>(&bodyOb));
	}

	void update(float dt) {
		float alpha = atan(W / U);
		float beta = atan(V / U);
		float alphaDeriv = ((du / U) - (W * dU / (U * U))) / ((W*W) / (U*U) + 1.0f);

		float velMagsq = U*U + V*V + W*W;
		float velMag = sqrt(velMagsq);

		float rho = 0.15f; //TODO: air pressure
		float Q = velMagsq * rho * 0.5f; //dynamic pressure

		float delWindVel = windVel - lastWindVel;
		float q = (velMag + delWindVel.length()) / velMag;

		float Ldash = (CL0 + CLa * alpha + (CLQ * Q + CLda * alphaDeriv) * (chord / (2*velMag)) + CLde * de * q*q) * Q * S;
		float D = (CD0 + CDa * alpha + CDde * de * q*q) * Q * S;
		float SF = (CYb * beta + CYdr * dr) * Q * S;

		float Faz = -Ldash * cos(alpha) - D * sin(alpha);
		float Fax = Ldash * alpha - D * cos(alpha) - SF * sin(beta);
		float Fay = SF * cos(beta);


	}

private:
	QuatObject bodyOb;
	QuatObject rudderOb;
	QuatObject elevatorOb;
	QuatObject wheelLOb;
	QuatObject wheelROb;
	QuatObject propOb;

	//constants, for each aircraft
	float CL0; //reference lift at zero angle of attack
	float CD0; //reference drag at zero angle of attack
	float CLa; //lift curve slope
	float CDa; //drag curve slope
	float CM0; //pitch moment (reference?)
	float CMa; //pitch moment due to angle of attach
	float CLQ; //lift due to pitch rate
	float CMQ; //pitch moment due to pitch rate
	float CLad; //lift due to angle of attach deriv
	float CMad; //pitch moment due to angle of attach deriv

	float CYb; //side force due to sideslip
	float CLb; //dihedral effect
	float CLP; //roll damping
	float CLR; //roll due to yaw rate
	float CNb; //weather cocking stability
	float CNP; //rudder adverse yaw
	float CNR; //yaw damping

	float CLde; //lift due to elevator
	float CDde; //drag due to elevator
	float CMde; //pitch due to elevator
	float CLda; //roll due to aileron

	float chord; //on the airfoil, the geomethic distance between the leading and trailing edge
	float S; //wing area

	//variables
	// (forward, right side, down)
	float U, V, W; //linear velocity
	float P, Q, R; //angular velocity
	float L, M, N; //moments
	float dU, dV, dW; //linear acceleration
	float de, dr, da; //elevator, rudder, aileron (position as radians deflection)
	vec3 windVel;
	vec3 lastWindVel;

	//assets
	static Mesh* bodyMesh;
	static Mesh* propMesh;
	static Mesh* rudderMesh;
	static Mesh* elevatorMesh;
	static Mesh* wheelMesh;
	static Texture* bodyTexture;
};