predicate isPrefixPred(pre:string, str:string)
{
	(|pre| <= |str|) && 
	pre == str[..|pre|]
}

predicate isNotPrefixPred(pre:string, str:string)
{
	(|pre| > |str|) || 
	pre != str[..|pre|]
}

lemma PrefixNegationLemma(pre:string, str:string)
	ensures  isPrefixPred(pre,str) <==> !isNotPrefixPred(pre,str)
	ensures !isPrefixPred(pre,str) <==>  isNotPrefixPred(pre,str)
{}

method isPrefix(pre: string, str: string) returns (res:bool)
	ensures !res <==> isNotPrefixPred(pre,str)
	ensures  res <==> isPrefixPred(pre,str)
{
	if (|pre| > |str|){
		return false;
	}
	else{
		res := (pre == str[0..|pre|]);
	}
}


predicate isSubstringPred(sub:string, str:string)
{
	(exists i :: 0 <= i <= |str| &&  isPrefixPred(sub, str[i..]))
}

predicate isNotSubstringPred(sub:string, str:string)
{
	(forall i :: 0 <= i <= |str| ==> isNotPrefixPred(sub, str[i..]))
}

lemma SubstringNegationLemma(sub:string, str:string)
	ensures  isSubstringPred(sub,str) <==> !isNotSubstringPred(sub,str)
	ensures !isSubstringPred(sub,str) <==>  isNotSubstringPred(sub,str)
{}


method isSubstring(sub: string, str: string) returns (res:bool)
	ensures  res <==> isSubstringPred(sub, str)
	ensures !res <==> isNotSubstringPred(sub, str) // This postcondition follows from the above lemma.
{
	if (|sub| == 0) { 
		//special check needed because forall x :: isPrefixPred("", x) and hence forall x :: isSubstringPred("", x)
		//this confuses Dafny, I think

		res := true;
		assert isPrefixPred(sub, str[0..]); //this is actually needed, to point out..
		//assert (exists i :: 0 <= i <= |str| && isPrefixPred(sub, str[i..])); //..that i == 0
		//assert isSubstringPred(sub, str);
		//assert res <==> isSubstringPred(sub, str);
	}
	else {
		res := false;

		if (|sub| <= |str|) {
			var i := 0;
			//assert !res ==> forall j :: 0 <= j < i ==> isNotPrefixPred(sub, str[j..]);

			while (i < |str| && !res)
				invariant 0 <= i <= |str|
				invariant res ==> isSubstringPred(sub, str)
				invariant !res && !isPrefixPred(sub, str[i..]) ==> (forall j :: 0 <= j <= i ==> isNotPrefixPred(sub, str[j..]))
				decreases |str| - i
			{
				res := isPrefix(sub, str[i..]);
				i := i + 1;
			}

			//assert !res ==> i == |str|;
			//assert !res ==> !isPrefixPred(sub, str[i..]);
			//assert !res && !isPrefixPred(sub, str[i..]) ==> isNotSubstringPred(sub, str);
			//assert !res ==> isNotSubstringPred(sub, str);
		}
	}
}

predicate haveCommonKSubstringPred(k:nat, str1:string, str2:string)
{
	exists i1, j1 :: 0 <= i1 <= |str1|- k && j1 == i1 + k && isSubstringPred(str1[i1..j1],str2)
}

predicate haveNotCommonKSubstringPred(k:nat, str1:string, str2:string)
{
	forall i1, j1 :: 0 <= i1 <= |str1|- k && j1 == i1 + k ==>  isNotSubstringPred(str1[i1..j1],str2)
}

lemma commonKSubstringLemma(k:nat, str1:string, str2:string)
	ensures  haveCommonKSubstringPred(k,str1,str2) <==> !haveNotCommonKSubstringPred(k,str1,str2)
	ensures !haveCommonKSubstringPred(k,str1,str2) <==>  haveNotCommonKSubstringPred(k,str1,str2)
{}

method haveCommonKSubstring(k: nat, str1: string, str2: string) returns (found: bool)
	ensures found  <==>  haveCommonKSubstringPred(k,str1,str2)
	ensures !found <==> haveNotCommonKSubstringPred(k,str1,str2) // This postcondition follows from the above lemma.
{
	if (k == 0) {
		//same deal here as in isSubstring
		found := true;
		assert isPrefixPred(str1[0..0], str2[0..]); //this is actually needed, to point out..
		//assert (exists i :: 0 <= i <= |str2| && isPrefixPred(str1[0..0], str2[i..])); //..that i == 0
		//assert isSubstringPred(str1[0..0], str2);
		//assert exists i1, j1 :: 0 <= i1 <= |str1|- k && j1 == i1 + k && isSubstringPred(str1[i1..j1], str2);
		//assert haveCommonKSubstringPred(k, str1, str2);
		//assert found  <==>  haveCommonKSubstringPred(k, str1, str2);
	}

	else if (k <= |str1| && k <= |str2|) {
		found := false;	
		var i := 0;
		while (i <= |str1| - k && !found)
			invariant 0 <= i <= |str1| - k + 1
			invariant found ==> haveCommonKSubstringPred(k, str1, str2)
			invariant !found ==> forall i1, j1 :: 0 <= i1 < i && j1 == i1 + k ==> isNotSubstringPred(str1[i1..j1], str2)
			decreases |str1| - i
		{
			found := isSubstring(str1[i .. i+k], str2);
			i := i + 1;
		}

		//assert !found ==> i == |str1| - k + 1;
		//assert !found ==> i+k == |str1| + 1;
		//assert !found ==> forall i1, j1 :: 0 <= i1 < i && j1 == i1 + k ==>  isNotSubstringPred(str1[i1..j1], str2);
		//assert !found ==> forall i1, j1 :: 0 <= i1 <= |str1| - k && j1 == i1 + k ==>  isNotSubstringPred(str1[i1..j1], str2);
		//assert !found ==> haveNotCommonKSubstringPred(k, str1, str2);
	}

	else {
		found := false;
		//assert haveNotCommonKSubstringPred(k, str1, str2);
		//assert found <==> haveCommonKSubstringPred(k, str1, str2);
	}
}


method maxCommonSubstringLength(str1: string, str2: string) returns (len:nat)
	requires (|str1| <= |str2|)
	ensures (forall k :: len < k <= |str1| ==> !haveCommonKSubstringPred(k,str1,str2))
	ensures haveCommonKSubstringPred(len,str1,str2)
{
	var maxLen := |str1|;
	while 0 < maxLen 
		decreases maxLen
		invariant 0 <= maxLen <= |str1|
		invariant forall k: int :: maxLen < k <= |str1| ==> !haveCommonKSubstringPred(k, str1, str2)
	{
		var common := haveCommonKSubstring(maxLen, str1, str2);
		if common {
			return maxLen;
		}
		maxLen := maxLen - 1;
	}

	assert isPrefixPred(str1[0..0],str2[0..]);
	return 0;
}

