module State where

import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map

-- pretty printing
import Text.PrettyPrint.HughesPJ(text, (<>), Doc, render)


-- Program states are implemented using Haskell's Map datatype
type State = Map String Integer

-- Update the value of an existing identifier (or add a new identifier) in the state
stateUpdate :: State -> String -> Integer -> State
stateUpdate st ident val = Map.insert ident val st

-- Lookup the value of an identifier in a state
stateLookup :: State -> String -> Maybe Integer 
stateLookup st ident = Map.lookup ident st 


showState :: State -> String
showState = render . ppState


ppState :: State -> Doc
ppState st = text "[" <> text (intercalate ", " assocs) <> text "]"
  where assocs  = map (\(key, val) -> key ++ "->" ++ show val) pairs 
        pairs   = Map.toList st     -- key/value pairs        