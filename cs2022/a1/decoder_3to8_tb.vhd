library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity decoder_3to8_tb is
end decoder_3to8_tb;
 
architecture behavior of decoder_3to8_tb is 
	component decoder_3to8
	port(	a : in  std_logic_vector(2 downto 0);
		q : out  std_logic_vector(7 downto 0) );
	end component;

	signal a_in : std_logic_vector(2 downto 0) := (others => '0');
	signal q_out : std_logic_vector(7 downto 0);

begin
	uut: decoder_3to8 port map (a_in, q_out);

	stim_proc: process begin		
		wait for 1 ns;
		for i in 0 to 7 loop
			a_in <= std_logic_vector(to_unsigned(i, 3));
			wait for 1 ns;
			assert q_out = std_logic_vector(to_unsigned(1, 8) sll i);
		end loop;
	end process;
end;
