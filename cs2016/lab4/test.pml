int n;
int done = 0;


proctype thread(){
	int temp;
	int i;
	for (i : 1 .. 10){
		temp = n;
		n = temp + 1;
	}
	done++;
}

init {
	run thread(); run thread();
	(done == 2);
	assert(n != 2);
}
