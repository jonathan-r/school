#ifndef STACK_DEQUE_H
#define STACK_DEQUE_H

#define STACK_NODE_SIZE 10

typedef struct stack_node_struct{
	struct stack_node_struct* prev;
	double data[STACK_NODE_SIZE];
	int np; //this points to the last free element, so it shouldn't be 0
} stack_node;

typedef struct stack_struct{
	stack_node* end;
} stack;

//create a new, empty stack
stack* stack_create();

//safely delete the stack
void stack_delete(stack* st);

//push val onto the stack, increasing the storage if needed
void stack_push(stack* st, double val);

//returns 1 if there is an element and stores it in out if out is not NULL.
//returns 0 if there is no element in the stack.
int stack_pop(stack* st, double* out);

//return 1 if there is one element, 2 if there are more, 0 if none
int stack_contains(stack* st);

#endif //STACK_DEQUE_H

