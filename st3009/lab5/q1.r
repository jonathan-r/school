data <- read.csv(file = "lab4times.csv", header = F)[,];
N <- length(data)
data_mean<- mean(data)
data_sd <- sd(data)
low  <- data_mean - 1.96 * data_sd / sqrt(N)
high <- data_mean + 1.96 * data_sd / sqrt(N)