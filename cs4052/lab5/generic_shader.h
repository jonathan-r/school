#pragma once

#include <stdlib.h>

#include "maths/mat3.h"
#include "maths/mat4.h"
#include "maths/vec.h"
#include "shader_program.h"

class GenericShader {
public:
	GenericShader(ShaderProgram& progObj): prog(progObj) {
		initialize();
	}
	
	~GenericShader() {
	}

	void setFullMatrix(const mat4& m) {
		glUniformMatrix4fv(fullMatLoc, 1, GL_FALSE, m.data);
	}

	void setWorldMatrix(const mat4& m) {
		glUniformMatrix4fv(worldMatLoc, 1, GL_FALSE, m.data);
	}

	void setTime(float t) {
		glUniform1f(timeLoc, t);
	}

	void setLightFraction(float f) {
		glUniform1f(lightFractionLoc, f);
	}

	void setLightParams(const vec3& dir, const vec3& dif, const vec3& spe, const vec3& amb) {
		glUniform3f(lightDirLoc, dir.x, dir.y, dir.z);
		glUniform3f(lightDiffuseLoc, dif.x, dif.y, dif.z);
		glUniform3f(lightSpecularLoc, spe.x, spe.y, spe.z);
		glUniform3f(lightAmbientLoc, amb.x, amb.y, amb.z);
	}
	
	void setMatParams(const vec3& dif, const vec3& spe, const vec3& amb, float shine) {
		glUniform3f(matDiffuseLoc, dif.x, dif.y, dif.z);
		glUniform3f(matSpecularLoc, spe.x, spe.y, spe.z);
		glUniform3f(matAmbientLoc, amb.x, amb.y, amb.z);
		glUniform1f(matShineLoc, shine);
	}
	
	void setCameraPos(const vec3& cam) {
		glUniform3f(eyeWorldPos, cam.x, cam.y, cam.z);
	}

	void setTextureId(GLuint tex) {
		glUniform1i(textureLoc, tex);
	}

	void use() {
		prog.use();
	}

private:
	void initialize() {
		use();
		const GLuint progId = prog.getProgObj();

		posLoc = glGetAttribLocation(progId, "vPosition");
		norLoc = glGetAttribLocation(progId, "vNormal");
		//tanLoc = glGetAttribLocation(progId, "vTangent");
		texLoc = glGetAttribLocation(progId, "vTexCoord");

		printf("vertex attribute locations: pos: %d, nor: %d, tan: %d, tex: %d\n", posLoc, norLoc, tanLoc, texLoc);

		fullMatLoc = glGetUniformLocation(progId, "fullMatrix");
		worldMatLoc = glGetUniformLocation(progId, "worldMatrix");
		timeLoc = glGetUniformLocation(progId, "time");
		
		lightFractionLoc = glGetUniformLocation(progId, "lightFraction");
		textureLoc = glGetUniformLocation(progId, "texture");
		eyeWorldPos = glGetUniformLocation(progId, "eyeWorld");

		lightDirLoc = glGetUniformLocation(progId, "lightDir");
		lightDiffuseLoc = glGetUniformLocation(progId, "lightDiffuse");
		lightSpecularLoc = glGetUniformLocation(progId, "lightSpecular");
		lightAmbientLoc = glGetUniformLocation(progId, "lightAmbient");
		eyeWorldLoc = glGetUniformLocation(progId, "eyeWorld");
		matDiffuseLoc = glGetUniformLocation(progId, "matDiffuse");
		matSpecularLoc = glGetUniformLocation(progId, "matSpecular");
		matAmbientLoc = glGetUniformLocation(progId, "matAmbient");
		matShineLoc = glGetUniformLocation(progId, "matShine");
	}

	ShaderProgram& prog;

public:
	//attributes
	GLuint posLoc, norLoc, tanLoc, texLoc;

	//vertex shader uniforms
	GLuint fullMatLoc, worldMatLoc;
	
	//fragment shader uniforms
	GLuint lightFractionLoc, timeLoc, textureLoc, eyeWorldPos;
	GLuint lightDirLoc, lightDiffuseLoc, lightSpecularLoc, lightAmbientLoc, eyeWorldLoc;
	GLuint matDiffuseLoc, matSpecularLoc, matAmbientLoc, matShineLoc;
};

class UIShader {
public:
	UIShader(ShaderProgram& progObj): prog(progObj) {
		initialize();
	}
	
	~UIShader() {
	}

	void setMatrix(const mat3& m) {
		glUniformMatrix3fv(matLoc, 1, GL_FALSE, m.data);
	}

	void setWindow(float width, float height) {
		float aspect = width / height;
	}

	void setTextureId(GLuint tex) {
		glUniform1i(textureLoc, tex);
	}

	void use() {
		prog.use();
	}

private:
	void initialize() {
		use();
		const GLuint progId = prog.getProgObj();

		posLoc = glGetAttribLocation(progId, "vPosition");
		texLoc = glGetAttribLocation(progId, "vTexCoord");
		colLoc = glGetAttribLocation(progId, "vColor");

		matLoc = glGetUniformLocation(progId, "matrix");
		
		textureLoc = glGetUniformLocation(progId, "texture");
	}

	ShaderProgram& prog;

public:
	//attributes
	GLuint posLoc, texLoc, colLoc;

	//vertex shader uniforms
	GLuint matLoc;
	
	//fragment shader uniforms
	GLuint textureLoc;
};