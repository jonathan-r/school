from __future__ import print_function #python 3 like print in python 2

#non-list type range in both python 2 and python 3
try:
    xrange
except NameError:
    xrange = range

import numpy as np
from scipy import optimize
from random import random as rnd # uniformly distributed floating point random numbers in the range [0, 1]
import matplotlib.pyplot as plt
from matplotlib.colors import (LogNorm, BoundaryNorm)
from matplotlib.ticker import MaxNLocator

import trilat

def annotate_point(plot, position, text):
    plot.annotate(text, xy = position, xytext = (20, 0),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 1.0))

#creates a new figure/window; call before plt.show
def plot_residual_map(positions, distances, start = (-5, -5), end = (5, 5), samples = (100, 100)):
    fig = plt.figure()
    fig.canvas.set_window_title("Residuals at every point")
    plot = fig.add_subplot(111)
    plot.axes.set_aspect('equal', 'datalim') #set aspect ratio to 1:1

    #sample heatmap of the product of the residuals at every point
    xs = np.linspace(start[0], end[0], samples[0])
    ys = np.linspace(start[1], end[0], samples[1])
    zs = np.zeros(shape = samples, dtype = np.float32)
    for y in xrange(samples[1]):
        for x in xrange(samples[0]):
            point = np.array([xs[x], ys[y]])
            #zs[y, x] = np.prod(trilat.residuals(point, positions, distances))
            zs[y, x] = np.sum(trilat.residuals(point, positions, distances) ** 2)

    im = plot.pcolormesh(xs, ys, zs, cmap = plt.get_cmap('bone'), norm = LogNorm(vmin = zs.min(), vmax = zs.max()))
    plt.colorbar(im)

    return plot

#creates a new figure/window; call before plt.show
def plot_initial_guess_error_map(receiver, positions, distances, start = (-5, -5), end = (5, 5), samples = (30, 30)):
    fig = plt.figure()
    fig.canvas.set_window_title("Solution error based on initial guess at every point")
    plot = fig.add_subplot(111)
    plot.axes.set_aspect('equal', 'datalim') #set aspect ratio to 1:1

    #sample heatmap of the distance of the final solution of the solver given the initial guess at every point
    xs = np.linspace(start[0], end[0], samples[0])
    ys = np.linspace(start[1], end[0], samples[1])
    zs = np.zeros(shape = samples, dtype = np.float32)

    for y in xrange(samples[1]):
        for x in xrange(samples[0]):
            point = np.array([xs[x], ys[y]])
            solution = point
            try:
                for (guess, delta) in trilat.solve_system(point, positions, distances, 1e-3, 50):
                    solution = guess
                zs[y, x] = np.linalg.norm(solution - receiver)

            except trilat.CannotConvergeException:
                zs[y, x] = 100
    
    im = plot.pcolormesh(xs, ys, zs, cmap = plt.get_cmap('bone'))
    plt.colorbar(im)

    return plot

def plot_input_data(plot, receiver, positions, distances):
    plot.scatter([receiver[0]], [receiver[1]], s=[20], marker='o', color='r', label='receiver') #receiver positon
    plot.scatter(positions[:,0], positions[:,1], marker='o', color='b', label='trasmitters') #trasmitter positions

    #distances as circles around the transmitters
    for (pos, dis) in zip(positions, distances):
        circle = plt.Circle(pos, dis, color='grey', fill=False)
        plot.add_artist(circle)

def plot_single_run(plot, initial, receiver, positions, distances):
    print ('initial guess:', initial)
    points = [initial]
    try:
        for (guess, delta) in trilat.solve_system(initial, positions, distances, 1e-4, 150):
            print ('delta:', delta, 'next guess:', guess)
            points.append(guess)

    except trilat.CannotConvergeException:
        print('could not converge')

    print('solution found:', points[-1])
    print('distance:', np.linalg.norm(points[-1] - receiver))

    #scipy.optimize.leastsq, for comparison
    print('scipy.optimize.leastsq:')
    scipy_solution = optimize.leastsq(trilat.residuals, initial, args=(positions, distances), Dfun=trilat.jacobian, col_deriv=False)[0]
    print(scipy_solution)
    print('distance:', np.linalg.norm(scipy_solution - receiver))

    #plot the point
    plot.plot([scipy_solution[0]], [scipy_solution[1]], marker='d', markersize=10, color='orange', label='scipy solution')

    points = np.array(points)
    plot.plot(points[:,0], points[:,1], marker='D', markersize=10, linestyle='-', linewidth=3, color='green', label='solutions')

    #annote solutions
    for i, p in enumerate(points):
        annotate_point(plot, p, str(i))

def generate_data(receiver, num_transmitters, measurement_error, start = (-5, -5), end = (5, 5)):
    positions = [] #accurate position of satellites
    distances = [] #distances from satellites to point, with some error
    for i in xrange(num_transmitters):
        pos = [start[0] + rnd() * (end[0] - start[0]), start[1] + rnd() * (end[1] - start[1])]
        #dis = np.linalg.norm(receiver - pos) * (1.0 + measurement_error - rnd() * (2 * measurement_error))
        dis = np.linalg.norm(receiver - pos) * (1.0 + (rnd() - 0.5) * measurement_error)
        positions.append(pos)
        distances.append(dis)

    return np.array(positions), np.array(distances)


#bounds of the world
start = np.array((0, 0))
end = np.array((10, 10))

#actual value we're trying to discern
receiver = (end - start) / 2 #put it in the center of the world
print('actual position of the receiver:', receiver)

#randomly generated data
positions, distances = generate_data(receiver, 20, 0.2, start, end)

pl1 = plot_residual_map(positions, distances, start, end)
plot_input_data(pl1, receiver, positions, distances)

pl2 = plot_initial_guess_error_map(receiver, positions, distances, start, end)
plot_input_data(pl2, receiver, positions, distances)

#weighted average as the initial guess (x_0)
plot_single_run(pl1, np.mean(positions, axis = 0), receiver, positions, distances)

pl1.legend(scatterpoints=1, loc='lower left')
pl2.legend(scatterpoints=1, loc='lower left')
plt.show()
