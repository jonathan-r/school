library ieee;
use ieee.std_logic_1164.all;

entity pc_reg is port (
	load, increment : in std_logic;
	offset : in std_logic_vector(15 downto 0);
	clock, reset : in std_logic;
	output : inout std_logic_vector(15 downto 0) );
end pc_reg;

architecture behavioral of pc_reg is
	component full_adder is port(
		carry_in, a, b : in   std_logic;
		sum, carry_out : out  std_logic);
	end component;
	
	signal adder_b : std_logic_vector(15 downto 0);
	signal adder_carry : std_logic_vector(16 downto 0); --last bit is carry out
	signal adder_sum : std_logic_vector(15 downto 0);
	
begin	
	--each clock, pc = pc + load * offset + increment * 1
	adder_carry(0) <= increment; --carry in
	adder_b <= (0 to 15 => load) and offset; --and each offset bit with load
	
	gen_adder : for i in 0 to 15 generate
		adder : full_adder port map(adder_carry(i), output(i), adder_b(i), adder_sum(i), adder_carry(i + 1));
	end generate gen_adder;
	
	--load new address into register on every clock
	process(clock, reset) begin
		if (reset = '1') then
			output <= x"0000" after 1ns; --initial value
			
		elsif (clock'event and clock = '1') then
			output <= adder_sum after 1ns;
			
		end if;
	end process;
end behavioral;

