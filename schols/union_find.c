#include <stdio.h>
#include <stdlib.h>

int* vec = NULL; //which set every vector belongs to
int* depths = NULL; //depth of the farthest member of this set


//returns the id of the set this vector belongs to
int find(int id){
	int last;
	do {
		last = id;
		vec[id] = vec[vec[id]]; //flatten the tree
		id = vec[id]; //get the ancestor
	} 
	while (id != last); //until the root is found

	return id;
}

//"union" is taken
//rl and rr must be roots
void unite(int rl, int rr){
	//make the shallower set part of the deeper set, so that the highest depth doesn't increase
	if (depths[rl] < depths[rr]){
		vec[rl] = rr;
		depths[rr] += depths[rl];
	}
	else{
		vec[rr] = ll
		depths[rl] += depths[rr];
	}
}

int main(){
	int n, m;
	sscanf(buf, "%d\n", &n);
	sscanf(buf, "%d\n", &m);
	printf("n is %d and m is %d\n", n, m);
	
	//allocate
	vec = malloc(n * sizeof(int));
	depths = malloc(n * sizeof(int));
	
	for (int i = 0; i < n; i++){
		vec[i] = i; //make a set for each vector, that only contains the vector itself
		depths[i] = 0; //since each set contains one element, its depth is 0
	}
	
	//read in the edges
	for (int i = 0; i < m; i++){
		int l, r;
		scanf("(%d %d)%*c", &l, &r); //every of (\d+ \d+)[\n ]
		
		//0 based from now on
		l--; r--;
		
		//find the roots of the set of both vecors
		int rl = find(l);
		int rr = find(r);
		
		if (rl == rr){
			//they're already part of the same set
			printf("there's a loop\n");
			return 0;
		}
		
		//union them
		unite(rl, rr);
	}
	
	printf("there are no loops\n");
	return 0;
}
		
