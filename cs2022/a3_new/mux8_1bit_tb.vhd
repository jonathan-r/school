library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity mux8_1bit_tb is
end mux8_1bit_tb;
 
architecture behavior of mux8_1bit_tb is 
    component mux8_1bit port(
		input : in std_logic_vector(7 downto 0);
		selection : in std_logic_vector(2 downto 0);
		output : out std_logic );
    end component;
    
   signal input : std_logic_vector(7 downto 0) := (others => '0');
   signal selection : std_logic_vector(2 downto 0) := (others => '0');
   signal output : std_logic;
 
begin
	uut: mux8_1bit port map (input, selection, output);
	
	stim_proc: process begin
		input <= x"5b";
		for i in 0 to 7 loop
			selection <= std_logic_vector(to_unsigned(i, 3));
			wait for 2 ns;
			assert output = input(i);
		end loop;
		wait;
	end process;
end;
