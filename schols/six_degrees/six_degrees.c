#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <math.h>

typedef struct person_s{
	char* name;
	int knowns;
	struct person_s** known;
} person;


//baseline, O(n^2 * (len(V) + len(E))); n is the number of population(people), len(V) is n, len(E) is number of links between the population of the graph between n and n^2 (around n * average degree of a vertex)

//these are global just so that they're not pointlessly duplicated on the stack during recursion in the distance function
char* bf_mark; //array of booleans for every person, used in searching
person* bf_base; //value of people[], so that the index of a person can be derived from its pointer
int bf_k; //largesst permissible distance


//breadth first searh, finds the shortest distance between two nodes 
int bfs_distance(person* at, person* to){
	//mark self
	int self_id = (int)(at - bf_base);
	bf_mark[self_id] = 1;
	
	//breadth first
	for (int n = 0; n < at->knowns; n++){
		if (at->known[n] == to){
			bf_mark[self_id] = 0;
			return 1; //one hop
		}
	}
	
	//otherwise recurse
	int min = -1; //shortest path from here to the destination
	for (int n = 0; n < at->knowns; n++){
		person* next = at->known[n];
		int id = (int)(next - bf_base);
		if (!bf_mark[id]){
			int len = bfs_distance(next, to);
			if (len != -1 && (len < min || min == -1)) min = len;
		}
	}
	
	//remove the mark
	bf_mark[self_id] = 0;
	return (min == -1)? -1 : min + 1; //+1 if there is a path because of the one hop from this node to the shortest path
}


//determines whether there is a path (at least one) within (bf_)k hops
int bfs_distance_within_k(person* at, person* to, int hops){
	//mark self
	int self_id = (int)(at - bf_base);
	bf_mark[self_id] = 1;
	
	//breadth first
	for (int n = 0; n < at->knowns; n++){
		if (at->known[n] == to){
			bf_mark[self_id] = 0;
			return 1; //one hop
		}
	}
	
	//otherwise recurse
	int min = -1; //shortest path from here to the destination
	
	if (hops < bf_k){ //there's no point if the path would be longer than k
		for (int n = 0; n < at->knowns; n++){
			person* next = at->known[n];
			int id = (int)(next - bf_base);
			if (!bf_mark[id]){
				int len = bfs_distance_within_k(next, to, hops + 1);
				if (len != -1 && (len < min || min == -1)) min = len;
			}
		}
	}
	
	//remove the mark
	bf_mark[self_id] = 0;
	return (min == -1)? -1 : min + 1; //+1 if there is a path because of the one hop from this node to the shortest path
}

int within_k_degrees_bruteforce(person* people, int num_people, int k){
	bf_mark = malloc(num_people);
	memset(bf_mark, 0, num_people);
	bf_base = people;
	bf_k = k;
	
	for (int i = 0; i < num_people; i++){
		for(int j = i + 1; j < num_people; j++){
			int dist = bfs_distance_within_k(&people[i], &people[j], 1);
			
			if (dist == -1){
				return 0;
			}
		}
	}
	
	free(bf_mark);
	return 1;
}


int person_ptr_cmp(void* l, void* r){
	return (int)(*((person**)l) - *(person**)r);
}

//this one requires O(k*n^2) time and loads of auxiliary space
int within_k_degrees_memory_hungry(person* people[], int num_people, int k){
	person** known_next[num_people]; //
	
	//sort the knowns so that we can then do O(n) union
	for (int p = 0; p < num_people; p++){
		qsort(people[p]->known, people[p]->knowns, sizeof(person*), person_ptr_cmp);
	}
	
	
	
	person** within[num_people]; //array of people that are within i(in the loop) degrees of separation
	
	for (int i = 1; i < k; i++){
		for (int p = 0; p < num_people; p++){
*/

//O(1)
void link(person* a, person* b){
	a->known[a->knowns++] = b;
	b->known[b->knowns++] = a;
}

//O(k), for max k degree vertices
void safe_link(person* a, person* b, int max_degree){
	if (a->knowns == max_degree || b->knowns == max_degree) return;
	for (int i = 0; i < a->knowns; i++) if (a->known[i] == b) return;
	for (int i = 0; i < b->knowns; i++) if (b->known[i] == a){
		assert(!"unidirectional link");
		return;
	}
	link(a, b);
}

void generate_network(int population, int max_degree, person* people){
	/*person** edge = malloc(max_degree * sizeof(person*)); //working nodes
	int* age = malloc(max_degree * sizeof(int)); //how long has this node been kept as an edge node
	memset(edge, 0, max_degree * sizeof(person*));
	memset(age, 0, max_degree * sizeof(int));
		
	int held = 0; //amount of edge nodes
	
	for (int i = 0; i < population; i++){
		//attach it to the edge
		for (int h = 0; h < max_degree; h++){
			if (edge[h]){
				//maybe connect the node to the edge
				if (rand() % max_degree >= edge[h]->knowns){
					link(&people[i], edge[h]);
				}
				
				//maybe remove the edge node
				if (held > max_degree / 2){
					if (rand() % max_degree < edge[h]->knowns){
						edge[h] = NULL;
					}
				}
				
				if (people[i].knowns == max_degree) break;
			}
		}
		
		if (held < max_degree && people[i].knowns < max_degree){
			if (i < max_degree || (rand() % max_degree >= people[i].knowns)){
				for (int h = 0; h < max_degree; h++){
					if (!edge[h]){
						edge[h] = &people[i];
						held++;
						break;
					}
				}
			}
		}
		
		person* last = NULL;
		for (int h = 0; h < held; h++){
			if (!last){
				if (edge[h]) last = edge[h];
			}
			
			else{
				if (edge[h]){
					//maybe link this edge node and the last node
					if (rand() % (max_degree * 2) >= (last->knowns + edge[h]->knowns)){
						safe_link(last, edge[h], max_degree);
					}
				}
			}
		}
	}
	
	free(edge);
	free(age);*/
	
	
	//Efficient generation of large random networks, Batagelj, Brandes
	//algorithm 1, O(population + edges)
	int v = 1;
	int w = -1;
	
	float p = (float)max_degree / (population);
	float log_1p = log(1 - p);
	
	while (v < population){
		//r is in [0, 1)
		int ri = rand();
		if (ri == RAND_MAX) ri--;
		float r = (float)ri / RAND_MAX; 
		
		w += 1 + (int) (log(1 - r) / log_1p);
		while (w >= v && v < population){
			w -= v;
			v++;
		}
		if (v < population) safe_link(&people[v], &people[w], max_degree);
	}
}

int main(int argc, char** argv){
	srand(time(NULL));
	
	if (argc == 3 || argc == 4){
		int population = atoi(argv[1]);
		int max_degree = atoi(argv[2]);
		char* dot_file = (argc > 3)? argv[3] : NULL;
		
		if (population <= 0 || max_degree <= 0){
			printf("population must be greater than 0; max degree must be greater than 0");
			return 1;
		}
		
		person** link_storage = malloc(population * max_degree * sizeof(person*));
		memset(link_storage, 0, population * max_degree * sizeof(person*));
	
		person* people = malloc(population * sizeof(person));
		for (int i = 0; i < population; i++){
			char* name = malloc(11);
			sprintf(name, "%d", i);
			people[i].name = name;
			people[i].knowns = 0;
			people[i].known = link_storage + i * max_degree;
		}

		generate_network(population, max_degree, people);
		printf("generated network\n");
		
		if (dot_file){
			//write a dot representation of the graph
			FILE* f = fopen(dot_file, "wb");
			
			if (f){
				fprintf(f, "graph Network{\n");
				for (int i = 0; i < population; i++){
					for (int k = 0; k < people[i].knowns; k++){
						int id = (int)(people[i].known[k] - people);
						if (id > i){ //don't print duplicate links
							fprintf(f, "\t%s -- %s;\n", people[i].name, people[i].known[k]->name);
						}
					}
				}
				
				fprintf(f, "}\n");
				fclose(f);
			}
			else{
				perror("could not write dot file");
			}
		}
		
		
		
		int within = within_k_degrees_bruteforce(people, population, 6);
		printf("within 6: %d\n", within);
		
		//and free all that memory
		for (int i = 0; i < population; i++) free(people[i].name);
		free(people);
		free(link_storage);
	}
	
	else printf("usage: %s <population> <max degree> [dot output file]\n", argv[0]);
	
	return 0;
}	
	

