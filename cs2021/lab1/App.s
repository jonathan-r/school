	AREA	AsmTemplate, CODE, READONLY
	IMPORT	main

; 	Partial sample solution for 7-segment display lab -- lab 1
;	(c) Mike Brady, 2011.

	EXPORT	start
start

IO0DIR	EQU	0xE0028008
IO0SET	EQU	0xE0028004
IO0CLR	EQU	0xE002800C

; The follow equates describe how each each segment is connected to a pin on port P0.
; You may have to change them to reflect the actual wiring you have chosen on your board
SEGABIT		equ	9	; this means that segment a is wired to P0.9
SEGBBIT		equ	10	; this means that segment b is wired to P0.10
SEGCBIT		equ	22;11;12
SEGDBIT		equ	12;13
SEGEBIT		equ	13;16
SEGFBIT		equ	23;14;8
SEGGBIT		equ	15;19
SEGDPBIT	equ	16;20 


		ldr	r1,=IO0DIR
;select the pins that are driving the segments & d.p.
		ldr	r2,=(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT)
		str	r2,[r1]		;make them outputs

		ldr	r1,=IO0SET
		ldr	r2,=IO0CLR
; r1 points to the SET register
; r2 points to the CLEAR register
		ldr	r5,=lut		; point to the look-up table
wloop	mov	r0,#15		; start with 0

floop	ldr	r3,[r5,r0,lsl #2]	; pick up the 7-seg code for this number
		str	r3,[r1]	 	; turn segments on

;delay for about a half second
		ldr	r4,=10000000
dloop	subs	r4,r4,#1
		bne	dloop

		str	r3,[r2]		; turn segments off

		sub	r0,#1
		cmp	r0,#-1		; only doing up as far as 3 (so 4 is too far...)s
		bne	floop

		b	wloop

stop	B	stop		; never reached


lut	DCD	(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT) 				; '0'
	DCD	(1<<SEGBBIT)+(1<<SEGCBIT) 																	; '1'
	DCD	(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGGBIT) 							; '2'
	DCD	(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGGBIT) 							; '3'
	
	DCD	(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGFBIT)+(1<<SEGGBIT) 														; 4
	DCD (1<<SEGABIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)											; 5
	DCD (1<<SEGABIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)								; 6
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT) 																		; 7
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT) 					; 8
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGFBIT)+(1<<SEGGBIT) 											; 9
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT)					; A
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT)	; B
	DCD (1<<SEGABIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGDPBIT) 											; C
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGDPBIT) 				; D
	DCD (1<<SEGABIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT) 								; E
	DCD (1<<SEGABIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT) 											; F

;etc for all 10 (or all 16) digits
	END