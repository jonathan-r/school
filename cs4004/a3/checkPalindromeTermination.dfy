method checkPalindrome(s : seq<int>) returns (res: int)
    requires |s| > 0 
{
    res := 1;
    var i := 0;
    var j := |s|-1;

    while (i < j && res == 1)
        invariant 0 <= i < |s|
        invariant 0 <= j < |s|
        decreases j - i + res
    {
        if (s[i] != s[j]) {
            res := 0;
        }
        else {
            i := i + 1;
            j := j - 1;
        }
    }
}
