library ieee;
use ieee.std_logic_1164.all;


entity mux2_16bit is
	port (in0 : in  std_logic_vector (15 downto 0);
			in1 : in  std_logic_vector (15 downto 0);
		   addr : in std_logic;
			output : out  std_logic_vector (15 downto 0) );
end mux2_16bit;

architecture behavioral of mux2_16bit is begin
	process(in0, in1, addr)
		begin case addr is
			when '0' => output <= in0 after 1ns;
			when '1' => output <= in1 after 1ns;
			when others => output <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

