--------------------------------------------------------------------------------
-- Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: O.61xd
--  \   \         Application: netgen
--  /   /         Filename: stopwatch_translate.vhd
-- /___/   /\     Timestamp: Tue Feb 04 11:03:34 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -tpw 0 -ar Structure -tm stopwatch -w -dir netgen/translate -ofmt vhdl -sim stopwatch.ngd stopwatch_translate.vhd 
-- Device	: 3s700afg484-4
-- Input file	: stopwatch.ngd
-- Output file	: \\tholos.scss.tcd.ie\ugrad\roscaj\cs2022\lab1\netgen\translate\stopwatch_translate.vhd
-- # of Entities	: 1
-- Design Name	: stopwatch
-- Xilinx	: C:\Xilinx\13.2\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity stopwatch is
  port (
    lap_load : in STD_LOGIC := 'X'; 
    clk : in STD_LOGIC := 'X'; 
    lcd_e : out STD_LOGIC; 
    strtstop : in STD_LOGIC := 'X'; 
    reset : in STD_LOGIC := 'X'; 
    mode : in STD_LOGIC := 'X'; 
    lcd_rs : out STD_LOGIC; 
    lcd_rw : out STD_LOGIC; 
    sf_d : out STD_LOGIC_VECTOR ( 7 downto 0 ) 
  );
end stopwatch;

architecture Structure of stopwatch is
  signal Inst_dcm1_CLK0_BUF : STD_LOGIC; 
  signal Inst_dcm1_CLK2X_BUF : STD_LOGIC; 
  signal Inst_dcm1_CLKFB_IN : STD_LOGIC; 
  signal Inst_dcm1_CLKFX_BUF : STD_LOGIC; 
  signal Inst_dcm1_CLKIN_IBUFG : STD_LOGIC; 
  signal Mcount_address : STD_LOGIC; 
  signal Mcount_address1 : STD_LOGIC; 
  signal Mcount_address2 : STD_LOGIC; 
  signal Mcount_address3 : STD_LOGIC; 
  signal Mcount_address4 : STD_LOGIC; 
  signal Mcount_address5 : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal N103 : STD_LOGIC; 
  signal N105 : STD_LOGIC; 
  signal N107 : STD_LOGIC; 
  signal N109 : STD_LOGIC; 
  signal N111 : STD_LOGIC; 
  signal N115 : STD_LOGIC; 
  signal N116 : STD_LOGIC; 
  signal N118 : STD_LOGIC; 
  signal N120 : STD_LOGIC; 
  signal N122 : STD_LOGIC; 
  signal N126 : STD_LOGIC; 
  signal N128 : STD_LOGIC; 
  signal N130 : STD_LOGIC; 
  signal N132 : STD_LOGIC; 
  signal N136 : STD_LOGIC; 
  signal N138 : STD_LOGIC; 
  signal N139 : STD_LOGIC; 
  signal N141 : STD_LOGIC; 
  signal N142 : STD_LOGIC; 
  signal N146 : STD_LOGIC; 
  signal N147 : STD_LOGIC; 
  signal N148 : STD_LOGIC; 
  signal N149 : STD_LOGIC; 
  signal N15 : STD_LOGIC; 
  signal N150 : STD_LOGIC; 
  signal N151 : STD_LOGIC; 
  signal N152 : STD_LOGIC; 
  signal N153 : STD_LOGIC; 
  signal N154 : STD_LOGIC; 
  signal N155 : STD_LOGIC; 
  signal N156 : STD_LOGIC; 
  signal N157 : STD_LOGIC; 
  signal N158 : STD_LOGIC; 
  signal N159 : STD_LOGIC; 
  signal N160 : STD_LOGIC; 
  signal N161 : STD_LOGIC; 
  signal N162 : STD_LOGIC; 
  signal N163 : STD_LOGIC; 
  signal N164 : STD_LOGIC; 
  signal N165 : STD_LOGIC; 
  signal N166 : STD_LOGIC; 
  signal N167 : STD_LOGIC; 
  signal N168 : STD_LOGIC; 
  signal N169 : STD_LOGIC; 
  signal N17 : STD_LOGIC; 
  signal N170 : STD_LOGIC; 
  signal N19 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal N21 : STD_LOGIC; 
  signal N23 : STD_LOGIC; 
  signal N25 : STD_LOGIC; 
  signal N30 : STD_LOGIC; 
  signal N42 : STD_LOGIC; 
  signal N43 : STD_LOGIC; 
  signal N47 : STD_LOGIC; 
  signal N49 : STD_LOGIC; 
  signal N51 : STD_LOGIC; 
  signal N52 : STD_LOGIC; 
  signal N60 : STD_LOGIC; 
  signal N62 : STD_LOGIC; 
  signal N67 : STD_LOGIC; 
  signal N69 : STD_LOGIC; 
  signal N71 : STD_LOGIC; 
  signal N73 : STD_LOGIC; 
  signal N75 : STD_LOGIC; 
  signal N79 : STD_LOGIC; 
  signal N8 : STD_LOGIC; 
  signal N83 : STD_LOGIC; 
  signal N90 : STD_LOGIC; 
  signal N92 : STD_LOGIC; 
  signal N96 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal clk_26214k : STD_LOGIC; 
  signal clk_divider_Mcompar_cnt_cmp_ge0000_cy_0_rt_180 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_10_rt_192 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_11_rt_194 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_12_rt_196 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_13_rt_198 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_14_rt_200 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_15_rt_202 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_16_rt_204 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_17_rt_206 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_18_rt_208 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_19_rt_210 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_1_rt_212 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_20_rt_214 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_21_rt_216 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_22_rt_218 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_23_rt_220 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_24_rt_222 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_25_rt_224 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_26_rt_226 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_27_rt_228 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_28_rt_230 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_29_rt_232 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_2_rt_234 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_30_rt_236 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_3_rt_238 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_4_rt_240 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_5_rt_242 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_6_rt_244 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_7_rt_246 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_8_rt_248 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_cy_9_rt_250 : STD_LOGIC; 
  signal clk_divider_Mcount_cnt_xor_31_rt_252 : STD_LOGIC; 
  signal clk_divider_cnt_cmp_ge0000 : STD_LOGIC; 
  signal clk_divider_div_262144_286 : STD_LOGIC; 
  signal clk_divider_div_2621441 : STD_LOGIC; 
  signal clk_divider_div_temp_288 : STD_LOGIC; 
  signal clk_divider_div_temp_not0001 : STD_LOGIC; 
  signal lap_load_IBUF_291 : STD_LOGIC; 
  signal lap_load_debounce_Q1_292 : STD_LOGIC; 
  signal lap_load_debounce_Q2_293 : STD_LOGIC; 
  signal lap_load_debounce_Q3_294 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_1_296 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_10_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_11_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_12_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_13_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_1_307 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_0_rt_309 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_311 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_rt_313 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_1_315 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_0_rt_317 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_319 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_rt_321 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_1_323 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_0_rt_325 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_327 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_rt_329 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_1_331 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_1_334 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_9_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_1_339 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_2_340 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_10_1_342 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_11_1_344 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_12_1_346 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_13_1_348 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_14_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_1_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_2_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_3_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_4_1_358 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_5_1_360 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_6_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_1_364 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_2 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_1_367 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_2_368 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_9_1_370 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_8_rt_382 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_0_Q_384 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_10_Q_385 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_11_Q_386 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_12_Q : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_1_Q : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_2_Q_389 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_3_Q_390 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_4_Q_391 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_5_Q : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_6_Q_393 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_7_Q : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_9_Q_395 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_10_rt_398 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_11_rt_400 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_12_rt_402 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_13_rt_404 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_14_rt_406 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_15_rt_408 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_16_rt_410 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_17_rt_412 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_18_rt_414 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_19_rt_416 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_1_rt_418 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_20_rt_420 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_21_rt_422 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_22_rt_424 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_23_rt_426 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_24_rt_428 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_25_rt_430 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_26_rt_432 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_27_rt_434 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_28_rt_436 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_29_rt_438 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_2_rt_440 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_30_rt_442 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_3_rt_444 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_4_rt_446 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_5_rt_448 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_6_rt_450 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_7_rt_452 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_8_rt_454 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_cy_9_rt_456 : STD_LOGIC; 
  signal lcd_cntrl_inst_Mcount_count_temp_xor_31_rt_458 : STD_LOGIC; 
  signal lcd_cntrl_inst_N0 : STD_LOGIC; 
  signal lcd_cntrl_inst_N111 : STD_LOGIC; 
  signal lcd_cntrl_inst_N112 : STD_LOGIC; 
  signal lcd_cntrl_inst_N12 : STD_LOGIC; 
  signal lcd_cntrl_inst_N13 : STD_LOGIC; 
  signal lcd_cntrl_inst_N16 : STD_LOGIC; 
  signal lcd_cntrl_inst_N4 : STD_LOGIC; 
  signal lcd_cntrl_inst_N6 : STD_LOGIC; 
  signal lcd_cntrl_inst_N7 : STD_LOGIC; 
  signal lcd_cntrl_inst_N71 : STD_LOGIC; 
  signal lcd_cntrl_inst_N8 : STD_LOGIC; 
  signal lcd_cntrl_inst_N9 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_503 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux0000 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux0000111112_505 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux0000111117_506 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux0000114_507 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux0000119_508 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux00002 : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux00008_510 : STD_LOGIC; 
  signal lcd_cntrl_inst_control_0_12_511 : STD_LOGIC; 
  signal lcd_cntrl_inst_control_0_25_512 : STD_LOGIC; 
  signal lcd_cntrl_inst_control_cmp_le0000 : STD_LOGIC; 
  signal lcd_cntrl_inst_control_cmp_le0001 : STD_LOGIC; 
  signal lcd_cntrl_inst_count_temp_or0000_579 : STD_LOGIC; 
  signal lcd_cntrl_inst_lap_flag_580 : STD_LOGIC; 
  signal lcd_cntrl_inst_lap_flag_not0001 : STD_LOGIC; 
  signal lcd_cntrl_inst_lap_ones_or0000 : STD_LOGIC; 
  signal lcd_cntrl_inst_mode_state_603 : STD_LOGIC; 
  signal lcd_cntrl_inst_next_mode_state_604 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_103_613 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_117_614 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_123_615 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_136_616 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_142_617 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_152_618 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_173 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_2_620 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_201_621 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_220_622 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_221_623 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_239_624 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_242_625 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_25_626 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_250_627 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_253_628 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_261_629 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_28_630 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_32_631 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_41_632 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_47_633 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_57_634 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_66_635 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_73_636 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_8_637 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_0_80_638 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_Q : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_0_Q_640 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_1_Q_641 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_2_Q_642 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_3_Q_643 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_4_Q_644 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_5_Q_645 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_6_Q_646 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_7_Q_647 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_8_Q_648 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_cy_9_Q_649 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_0_Q_650 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_10_Q_651 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_1_Q_652 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_2_Q_653 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_3_Q_654 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_4_Q_655 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_5_Q_656 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_6_Q_657 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_7_Q_658 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_8_Q_659 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_wg_lut_9_Q_660 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_107 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_12_662 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_22_663 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_23_664 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_45_665 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_5_666 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_50_667 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_61_668 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_68_669 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_70_670 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_2_85_671 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_Q : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_cy_0_Q_673 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_cy_1_Q_674 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_cy_2_Q_675 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_cy_3_Q_676 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_cy_4_Q_677 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_cy_5_Q_678 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_0_Q_679 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_1_Q_680 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_2_Q_681 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_3_Q_682 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_4_Q_683 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_5_Q_684 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_3_wg_lut_6_Q_685 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_106_686 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_120_687 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_13_688 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_147_689 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_172_690 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_181_691 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_200_692 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_228_693 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_242_694 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_256_695 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_27_696 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_52_697 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_66_698 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_4_80_699 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_5_1_700 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_6_15 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_6_151_702 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_6_18 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_6_181 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_7_1 : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_7_11_706 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd1_707 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd10_708 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd11_709 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd12_710 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd13_711 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd14_712 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd14_In1_713 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd15_714 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd16_715 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd17_716 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd18_717 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd19_718 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd2_719 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd20_720 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd21_721 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd22_722 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd23_723 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd24_724 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd25_725 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd26_726 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd27_727 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd28_728 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd29_729 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd3_730 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd30_731 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd31_732 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd32_733 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd33_734 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd33_In10_735 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd33_In33 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd34_737 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd34_In1 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd35_739 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd35_In1 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd36_741 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd37_742 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd38_743 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd39_744 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd4_745 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd40_746 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd41_747 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd41_In22 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd41_In221_749 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd41_In222_750 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd42_751 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd43_752 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd43_In : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd44_754 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd45_755 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd46_756 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd46_In : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd47_758 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd5_759 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd6_760 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd7_761 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd8_762 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_FSM_FFd9_763 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq0000 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq000010_765 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq000013_766 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq000024_779 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq0001 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq000110_781 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq000113_782 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_eq000124_783 : STD_LOGIC; 
  signal lcd_cntrl_inst_state_cmp_ge0000 : STD_LOGIC; 
  signal lcd_cntrl_inst_timer_flag_785 : STD_LOGIC; 
  signal lcd_cntrl_inst_timer_flag_mux0000 : STD_LOGIC; 
  signal lcd_cntrl_inst_timer_flag_mux000017 : STD_LOGIC; 
  signal lcd_cntrl_inst_timer_flag_not0001 : STD_LOGIC; 
  signal lcd_e_OBUF_790 : STD_LOGIC; 
  signal lcd_rs_OBUF_792 : STD_LOGIC; 
  signal ll_debounced : STD_LOGIC; 
  signal locked : STD_LOGIC; 
  signal mode_IBUF_797 : STD_LOGIC; 
  signal mode_control : STD_LOGIC; 
  signal mode_debounce_Q1_799 : STD_LOGIC; 
  signal mode_debounce_Q2_800 : STD_LOGIC; 
  signal mode_debounce_Q3_801 : STD_LOGIC; 
  signal mode_debounced : STD_LOGIC; 
  signal preset_time_0_Q : STD_LOGIC; 
  signal preset_time_10_Q : STD_LOGIC; 
  signal preset_time_11_Q : STD_LOGIC; 
  signal preset_time_12_Q : STD_LOGIC; 
  signal preset_time_13_Q : STD_LOGIC; 
  signal preset_time_14_Q : STD_LOGIC; 
  signal preset_time_15_Q : STD_LOGIC; 
  signal preset_time_16_Q : STD_LOGIC; 
  signal preset_time_17_Q : STD_LOGIC; 
  signal preset_time_18_Q : STD_LOGIC; 
  signal preset_time_19_Q : STD_LOGIC; 
  signal preset_time_4_Q : STD_LOGIC; 
  signal preset_time_6_Q : STD_LOGIC; 
  signal preset_time_8_Q : STD_LOGIC; 
  signal preset_time_9_Q : STD_LOGIC; 
  signal reset_IBUF_824 : STD_LOGIC; 
  signal strtstop_IBUF_834 : STD_LOGIC; 
  signal strtstop_debounce_Q1_835 : STD_LOGIC; 
  signal strtstop_debounce_Q2_836 : STD_LOGIC; 
  signal strtstop_debounce_Q3_837 : STD_LOGIC; 
  signal strtstop_debounced : STD_LOGIC; 
  signal timer_inst_N14 : STD_LOGIC; 
  signal timer_inst_N3 : STD_LOGIC; 
  signal timer_inst_N4 : STD_LOGIC; 
  signal timer_inst_N5 : STD_LOGIC; 
  signal timer_inst_N6 : STD_LOGIC; 
  signal timer_inst_hundredths_cnt_mux0003_1_0_862 : STD_LOGIC; 
  signal timer_inst_hundredths_cnt_mux0003_1_31_863 : STD_LOGIC; 
  signal timer_inst_hundredths_cnt_mux0003_2_0_865 : STD_LOGIC; 
  signal timer_inst_hundredths_cnt_mux0003_3_32_867 : STD_LOGIC; 
  signal timer_inst_mins_cnt_and0000 : STD_LOGIC; 
  signal timer_inst_mins_cnt_and0001 : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_16_19_875 : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_16_4_876 : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_17_1_878 : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_18_1_880 : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_19_37_882 : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_19_4_883 : STD_LOGIC; 
  signal timer_inst_ones_cnt_and0001 : STD_LOGIC; 
  signal timer_inst_ones_cnt_and0002 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_10_1_891 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_11_2 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_11_37_894 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_11_4_895 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_8_19_897 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_8_5_898 : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_9_1_900 : STD_LOGIC; 
  signal timer_inst_ones_cnt_not0002 : STD_LOGIC; 
  signal timer_inst_tc_1 : STD_LOGIC; 
  signal timer_inst_tc_1_and0001 : STD_LOGIC; 
  signal timer_inst_tc_1_cmp_eq0000 : STD_LOGIC; 
  signal timer_inst_tc_1_cmp_eq0001 : STD_LOGIC; 
  signal timer_inst_tc_2_cmp_eq0000 : STD_LOGIC; 
  signal timer_inst_tc_2_cmp_eq0001 : STD_LOGIC; 
  signal timer_inst_tc_3_cmp_eq0000 : STD_LOGIC; 
  signal timer_inst_tc_3_cmp_eq0001 : STD_LOGIC; 
  signal timer_inst_tc_4_cmp_eq0000 : STD_LOGIC; 
  signal timer_inst_tc_4_cmp_eq0001 : STD_LOGIC; 
  signal timer_inst_tc_5_cmp_eq0000 : STD_LOGIC; 
  signal timer_inst_tc_5_cmp_eq0001 : STD_LOGIC; 
  signal timer_inst_tens_cnt_and0000 : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_12_62 : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_13_1_922 : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_13_18_923 : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_14_11_925 : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_14_5_926 : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_15_1_928 : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_5_17_935 : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_5_20_936 : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_6_17_938 : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_6_20_939 : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_7_34_941 : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_7_8_942 : STD_LOGIC; 
  signal timer_state_clken_943 : STD_LOGIC; 
  signal timer_state_next_clken : STD_LOGIC; 
  signal timer_state_next_clken0_945 : STD_LOGIC; 
  signal timer_state_next_lap_trigger1 : STD_LOGIC; 
  signal timer_state_rst_947 : STD_LOGIC; 
  signal timer_state_sreg1_FSM_FFd1_948 : STD_LOGIC; 
  signal timer_state_sreg1_FSM_FFd2_949 : STD_LOGIC; 
  signal timer_state_sreg1_FSM_FFd2_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd1_951 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd1_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd10_953 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd10_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd11_955 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd11_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd12_957 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd12_In_958 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd2_959 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd2_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd3_961 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd3_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd4_963 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd4_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd4_In10_965 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd4_In21_966 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd5_967 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd5_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd6_969 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd6_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd7_971 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd7_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd8_973 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd8_In : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd9_975 : STD_LOGIC; 
  signal timer_state_sreg_FSM_FFd9_In_976 : STD_LOGIC; 
  signal timer_state_state_reset : STD_LOGIC; 
  signal lcd_cntrl_inst_sf_d_temp_1_1_SW0_O : STD_LOGIC; 
  signal lcd_cntrl_inst_clock_flag_mux0000111_SW0_O : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_13_11_O : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_13_1_O : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_15_1_O : STD_LOGIC; 
  signal timer_inst_Maddsub_hundredths_cnt_share0000_cy_1_11_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_18_40_SW0_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_17_40_SW0_O : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_7_34_O : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_11_4_O : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_6_20_O : STD_LOGIC; 
  signal timer_inst_Maddsub_hundredths_cnt_share0000_cy_2_11_O : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_14_5_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_mux0002_16_19_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_and000011_SW3_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_and000011_SW4_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_and000011_SW5_O : STD_LOGIC; 
  signal timer_inst_mins_cnt_and000011_SW7_O : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_9_40_SW0_O : STD_LOGIC; 
  signal timer_inst_ones_cnt_mux0002_10_25_SW1_O : STD_LOGIC; 
  signal timer_inst_tens_cnt_mux0002_14_11_O : STD_LOGIC; 
  signal timer_inst_tenths_cnt_mux0002_5_17_O : STD_LOGIC; 
  signal timer_inst_Maddsub_tenths_cnt_share0000_lut_2_1_SW0_O : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712221 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712222_52 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00008111 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271222 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271284_49 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271281_48 : STD_LOGIC; 
  signal t_preset_N8 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016132_46 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613_f5_45 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016131 : STD_LOGIC; 
  signal t_preset_N6 : STD_LOGIC; 
  signal t_preset_N2 : STD_LOGIC; 
  signal t_preset_N01 : STD_LOGIC; 
  signal t_preset_spo_18_bdd0 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000312_39 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031_f5_38 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000311_37 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000302_35 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_f5_34 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000301_33 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_32 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000282_31 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f5_30 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f51_29 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000281_28 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f51 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000243_25 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000242_24 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f5_23 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000241 : STD_LOGIC; 
  signal t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_21 : STD_LOGIC; 
  signal VCC : STD_LOGIC; 
  signal GND : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_CLK90_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_CLK180_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_CLK270_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_CLK2X180_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_CLKDV_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_CLKFX180_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_PSDONE_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_DCM_SP_INST_STATUS_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_Inst_dcm1_CLK2X_BUFG_INST_O_UNCONNECTED : STD_LOGIC; 
  signal Result : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal address : STD_LOGIC_VECTOR ( 5 downto 0 ); 
  signal clk_divider_Mcompar_cnt_cmp_ge0000_cy : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal clk_divider_Mcompar_cnt_cmp_ge0000_lut : STD_LOGIC_VECTOR ( 5 downto 1 ); 
  signal clk_divider_Mcount_cnt_cy : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal clk_divider_Mcount_cnt_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal clk_divider_cnt : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy : STD_LOGIC_VECTOR ( 13 downto 0 ); 
  signal lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut : STD_LOGIC_VECTOR ( 14 downto 0 ); 
  signal lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal lcd_cntrl_inst_Mcount_count_temp_cy : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal lcd_cntrl_inst_Mcount_count_temp_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal lcd_cntrl_inst_Result : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal lcd_cntrl_inst_count : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal lcd_cntrl_inst_count_temp : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal lcd_cntrl_inst_lap_hundredths : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal lcd_cntrl_inst_lap_min : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal lcd_cntrl_inst_lap_ones : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal lcd_cntrl_inst_lap_tens : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal lcd_cntrl_inst_lap_tenths : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal lcd_cntrl_inst_sf_d : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal lcd_cntrl_inst_state_cmp_eq00001_wg_cy : STD_LOGIC_VECTOR ( 5 downto 0 ); 
  signal lcd_cntrl_inst_state_cmp_eq00001_wg_lut : STD_LOGIC_VECTOR ( 5 downto 0 ); 
  signal timer_inst_Maddsub_hundredths_cnt_share0000_cy : STD_LOGIC_VECTOR ( 2 downto 1 ); 
  signal timer_inst_Maddsub_hundredths_cnt_share0000_lut : STD_LOGIC_VECTOR ( 2 downto 1 ); 
  signal timer_inst_Maddsub_mins_cnt_share0000_cy : STD_LOGIC_VECTOR ( 1 downto 1 ); 
  signal timer_inst_Maddsub_mins_cnt_share0000_lut : STD_LOGIC_VECTOR ( 2 downto 1 ); 
  signal timer_inst_Maddsub_ones_cnt_share0000_lut : STD_LOGIC_VECTOR ( 1 downto 1 ); 
  signal timer_inst_Maddsub_tens_cnt_share0000_cy : STD_LOGIC_VECTOR ( 2 downto 1 ); 
  signal timer_inst_Maddsub_tenths_cnt_share0000_cy : STD_LOGIC_VECTOR ( 1 downto 1 ); 
  signal timer_inst_Maddsub_tenths_cnt_share0000_lut : STD_LOGIC_VECTOR ( 2 downto 2 ); 
  signal timer_inst_hundredths_cnt : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_inst_hundredths_cnt_mux0003 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_inst_mins_cnt : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_inst_mins_cnt_mux0002 : STD_LOGIC_VECTOR ( 19 downto 16 ); 
  signal timer_inst_ones_cnt : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_inst_ones_cnt_mux0002 : STD_LOGIC_VECTOR ( 11 downto 8 ); 
  signal timer_inst_tens_cnt : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_inst_tens_cnt_mux0002 : STD_LOGIC_VECTOR ( 15 downto 12 ); 
  signal timer_inst_tenths_cnt : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_inst_tenths_cnt_mux0002 : STD_LOGIC_VECTOR ( 7 downto 4 ); 
begin
  XST_GND : X_ZERO
    port map (
      O => N0
    );
  XST_VCC : X_ONE
    port map (
      O => N1
    );
  clk_divider_div_262144 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => clk_divider_div_temp_288,
      O => clk_divider_div_2621441,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  clk_divider_div_temp : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => clk_divider_cnt_cmp_ge0000,
      I => clk_divider_div_temp_not0001,
      O => clk_divider_div_temp_288,
      SET => GND,
      RST => GND
    );
  lap_load_debounce_Q3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lap_load_debounce_Q2_293,
      O => lap_load_debounce_Q3_294,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lap_load_debounce_Q2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lap_load_debounce_Q1_292,
      O => lap_load_debounce_Q2_293,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lap_load_debounce_Q1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lap_load_IBUF_291,
      O => lap_load_debounce_Q1_292,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  strtstop_debounce_Q3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => strtstop_debounce_Q2_836,
      O => strtstop_debounce_Q3_837,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  strtstop_debounce_Q2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => strtstop_debounce_Q1_835,
      O => strtstop_debounce_Q2_836,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  strtstop_debounce_Q1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => strtstop_IBUF_834,
      O => strtstop_debounce_Q1_835,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  mode_debounce_Q3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => mode_debounce_Q2_800,
      O => mode_debounce_Q3_801,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  mode_debounce_Q2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => mode_debounce_Q1_799,
      O => mode_debounce_Q2_800,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  mode_debounce_Q1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => mode_IBUF_797,
      O => mode_debounce_Q1_799,
      CE => VCC,
      SET => GND,
      RST => GND
    );
  address_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_state_sreg_FSM_FFd5_967,
      RST => mode_control,
      I => Mcount_address,
      O => address(0),
      SET => GND
    );
  address_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_state_sreg_FSM_FFd5_967,
      RST => mode_control,
      I => Mcount_address1,
      O => address(1),
      SET => GND
    );
  address_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_state_sreg_FSM_FFd5_967,
      RST => mode_control,
      I => Mcount_address2,
      O => address(2),
      SET => GND
    );
  address_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_state_sreg_FSM_FFd5_967,
      RST => mode_control,
      I => Mcount_address3,
      O => address(3),
      SET => GND
    );
  address_4 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_state_sreg_FSM_FFd5_967,
      RST => mode_control,
      I => Mcount_address4,
      O => address(4),
      SET => GND
    );
  address_5 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_state_sreg_FSM_FFd5_967,
      RST => mode_control,
      I => Mcount_address5,
      O => address(5),
      SET => GND
    );
  clk_divider_cnt_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(0),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(0),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(1),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(1),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(2),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(2),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(3),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(3),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_4 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(4),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(4),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_5 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(5),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(5),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_6 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(6),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(6),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_7 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(7),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(7),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_8 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(8),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(8),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_9 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(9),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(9),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_10 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(10),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(10),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_11 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(11),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(11),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_12 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(12),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(12),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_13 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(13),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(13),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_14 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(14),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(14),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_15 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(15),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(15),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_16 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(16),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(16),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_17 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(17),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(17),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_18 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(18),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(18),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_19 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(19),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(19),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_20 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(20),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(20),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_21 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(21),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(21),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_22 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(22),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(22),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_23 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(23),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(23),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_24 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(24),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(24),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_25 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(25),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(25),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_26 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(26),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(26),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_27 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(27),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(27),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_28 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(28),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(28),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_29 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(29),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(29),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_30 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(30),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(30),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_cnt_31 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => Result(31),
      SRST => clk_divider_cnt_cmp_ge0000,
      O => clk_divider_cnt(31),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_0_Q : X_MUX2
    port map (
      IB => N1,
      IA => N0,
      SEL => clk_divider_Mcompar_cnt_cmp_ge0000_cy_0_rt_180,
      O => clk_divider_Mcompar_cnt_cmp_ge0000_cy(0)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_lut_1_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => clk_divider_cnt(18),
      ADR1 => clk_divider_cnt(19),
      ADR2 => clk_divider_cnt(20),
      ADR3 => clk_divider_cnt(21),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_lut(1)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_1_Q : X_MUX2
    port map (
      IB => clk_divider_Mcompar_cnt_cmp_ge0000_cy(0),
      IA => N1,
      SEL => clk_divider_Mcompar_cnt_cmp_ge0000_lut(1),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_cy(1)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_lut_2_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => clk_divider_cnt(22),
      ADR1 => clk_divider_cnt(23),
      ADR2 => clk_divider_cnt(24),
      ADR3 => clk_divider_cnt(25),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_lut(2)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_2_Q : X_MUX2
    port map (
      IB => clk_divider_Mcompar_cnt_cmp_ge0000_cy(1),
      IA => N1,
      SEL => clk_divider_Mcompar_cnt_cmp_ge0000_lut(2),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_cy(2)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_lut_3_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => clk_divider_cnt(26),
      ADR1 => clk_divider_cnt(27),
      ADR2 => clk_divider_cnt(28),
      ADR3 => clk_divider_cnt(29),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_lut(3)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_3_Q : X_MUX2
    port map (
      IB => clk_divider_Mcompar_cnt_cmp_ge0000_cy(2),
      IA => N1,
      SEL => clk_divider_Mcompar_cnt_cmp_ge0000_lut(3),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_cy(3)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_4_Q : X_MUX2
    port map (
      IB => clk_divider_Mcompar_cnt_cmp_ge0000_cy(3),
      IA => N1,
      SEL => clk_divider_Mcompar_cnt_cmp_ge0000_lut(4),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_cy(4)
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_5_Q : X_MUX2
    port map (
      IB => clk_divider_Mcompar_cnt_cmp_ge0000_cy(4),
      IA => N0,
      SEL => clk_divider_Mcompar_cnt_cmp_ge0000_lut(5),
      O => clk_divider_cnt_cmp_ge0000
    );
  clk_divider_Mcount_cnt_cy_0_Q : X_MUX2
    port map (
      IB => N0,
      IA => N1,
      SEL => clk_divider_Mcount_cnt_lut(0),
      O => clk_divider_Mcount_cnt_cy(0)
    );
  clk_divider_Mcount_cnt_xor_0_Q : X_XOR2
    port map (
      I0 => N0,
      I1 => clk_divider_Mcount_cnt_lut(0),
      O => Result(0)
    );
  clk_divider_Mcount_cnt_cy_1_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(0),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_1_rt_212,
      O => clk_divider_Mcount_cnt_cy(1)
    );
  clk_divider_Mcount_cnt_xor_1_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(0),
      I1 => clk_divider_Mcount_cnt_cy_1_rt_212,
      O => Result(1)
    );
  clk_divider_Mcount_cnt_cy_2_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(1),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_2_rt_234,
      O => clk_divider_Mcount_cnt_cy(2)
    );
  clk_divider_Mcount_cnt_xor_2_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(1),
      I1 => clk_divider_Mcount_cnt_cy_2_rt_234,
      O => Result(2)
    );
  clk_divider_Mcount_cnt_cy_3_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(2),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_3_rt_238,
      O => clk_divider_Mcount_cnt_cy(3)
    );
  clk_divider_Mcount_cnt_xor_3_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(2),
      I1 => clk_divider_Mcount_cnt_cy_3_rt_238,
      O => Result(3)
    );
  clk_divider_Mcount_cnt_cy_4_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(3),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_4_rt_240,
      O => clk_divider_Mcount_cnt_cy(4)
    );
  clk_divider_Mcount_cnt_xor_4_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(3),
      I1 => clk_divider_Mcount_cnt_cy_4_rt_240,
      O => Result(4)
    );
  clk_divider_Mcount_cnt_cy_5_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(4),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_5_rt_242,
      O => clk_divider_Mcount_cnt_cy(5)
    );
  clk_divider_Mcount_cnt_xor_5_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(4),
      I1 => clk_divider_Mcount_cnt_cy_5_rt_242,
      O => Result(5)
    );
  clk_divider_Mcount_cnt_cy_6_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(5),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_6_rt_244,
      O => clk_divider_Mcount_cnt_cy(6)
    );
  clk_divider_Mcount_cnt_xor_6_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(5),
      I1 => clk_divider_Mcount_cnt_cy_6_rt_244,
      O => Result(6)
    );
  clk_divider_Mcount_cnt_cy_7_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(6),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_7_rt_246,
      O => clk_divider_Mcount_cnt_cy(7)
    );
  clk_divider_Mcount_cnt_xor_7_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(6),
      I1 => clk_divider_Mcount_cnt_cy_7_rt_246,
      O => Result(7)
    );
  clk_divider_Mcount_cnt_cy_8_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(7),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_8_rt_248,
      O => clk_divider_Mcount_cnt_cy(8)
    );
  clk_divider_Mcount_cnt_xor_8_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(7),
      I1 => clk_divider_Mcount_cnt_cy_8_rt_248,
      O => Result(8)
    );
  clk_divider_Mcount_cnt_cy_9_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(8),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_9_rt_250,
      O => clk_divider_Mcount_cnt_cy(9)
    );
  clk_divider_Mcount_cnt_xor_9_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(8),
      I1 => clk_divider_Mcount_cnt_cy_9_rt_250,
      O => Result(9)
    );
  clk_divider_Mcount_cnt_cy_10_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(9),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_10_rt_192,
      O => clk_divider_Mcount_cnt_cy(10)
    );
  clk_divider_Mcount_cnt_xor_10_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(9),
      I1 => clk_divider_Mcount_cnt_cy_10_rt_192,
      O => Result(10)
    );
  clk_divider_Mcount_cnt_cy_11_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(10),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_11_rt_194,
      O => clk_divider_Mcount_cnt_cy(11)
    );
  clk_divider_Mcount_cnt_xor_11_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(10),
      I1 => clk_divider_Mcount_cnt_cy_11_rt_194,
      O => Result(11)
    );
  clk_divider_Mcount_cnt_cy_12_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(11),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_12_rt_196,
      O => clk_divider_Mcount_cnt_cy(12)
    );
  clk_divider_Mcount_cnt_xor_12_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(11),
      I1 => clk_divider_Mcount_cnt_cy_12_rt_196,
      O => Result(12)
    );
  clk_divider_Mcount_cnt_cy_13_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(12),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_13_rt_198,
      O => clk_divider_Mcount_cnt_cy(13)
    );
  clk_divider_Mcount_cnt_xor_13_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(12),
      I1 => clk_divider_Mcount_cnt_cy_13_rt_198,
      O => Result(13)
    );
  clk_divider_Mcount_cnt_cy_14_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(13),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_14_rt_200,
      O => clk_divider_Mcount_cnt_cy(14)
    );
  clk_divider_Mcount_cnt_xor_14_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(13),
      I1 => clk_divider_Mcount_cnt_cy_14_rt_200,
      O => Result(14)
    );
  clk_divider_Mcount_cnt_cy_15_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(14),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_15_rt_202,
      O => clk_divider_Mcount_cnt_cy(15)
    );
  clk_divider_Mcount_cnt_xor_15_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(14),
      I1 => clk_divider_Mcount_cnt_cy_15_rt_202,
      O => Result(15)
    );
  clk_divider_Mcount_cnt_cy_16_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(15),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_16_rt_204,
      O => clk_divider_Mcount_cnt_cy(16)
    );
  clk_divider_Mcount_cnt_xor_16_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(15),
      I1 => clk_divider_Mcount_cnt_cy_16_rt_204,
      O => Result(16)
    );
  clk_divider_Mcount_cnt_cy_17_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(16),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_17_rt_206,
      O => clk_divider_Mcount_cnt_cy(17)
    );
  clk_divider_Mcount_cnt_xor_17_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(16),
      I1 => clk_divider_Mcount_cnt_cy_17_rt_206,
      O => Result(17)
    );
  clk_divider_Mcount_cnt_cy_18_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(17),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_18_rt_208,
      O => clk_divider_Mcount_cnt_cy(18)
    );
  clk_divider_Mcount_cnt_xor_18_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(17),
      I1 => clk_divider_Mcount_cnt_cy_18_rt_208,
      O => Result(18)
    );
  clk_divider_Mcount_cnt_cy_19_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(18),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_19_rt_210,
      O => clk_divider_Mcount_cnt_cy(19)
    );
  clk_divider_Mcount_cnt_xor_19_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(18),
      I1 => clk_divider_Mcount_cnt_cy_19_rt_210,
      O => Result(19)
    );
  clk_divider_Mcount_cnt_cy_20_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(19),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_20_rt_214,
      O => clk_divider_Mcount_cnt_cy(20)
    );
  clk_divider_Mcount_cnt_xor_20_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(19),
      I1 => clk_divider_Mcount_cnt_cy_20_rt_214,
      O => Result(20)
    );
  clk_divider_Mcount_cnt_cy_21_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(20),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_21_rt_216,
      O => clk_divider_Mcount_cnt_cy(21)
    );
  clk_divider_Mcount_cnt_xor_21_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(20),
      I1 => clk_divider_Mcount_cnt_cy_21_rt_216,
      O => Result(21)
    );
  clk_divider_Mcount_cnt_cy_22_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(21),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_22_rt_218,
      O => clk_divider_Mcount_cnt_cy(22)
    );
  clk_divider_Mcount_cnt_xor_22_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(21),
      I1 => clk_divider_Mcount_cnt_cy_22_rt_218,
      O => Result(22)
    );
  clk_divider_Mcount_cnt_cy_23_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(22),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_23_rt_220,
      O => clk_divider_Mcount_cnt_cy(23)
    );
  clk_divider_Mcount_cnt_xor_23_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(22),
      I1 => clk_divider_Mcount_cnt_cy_23_rt_220,
      O => Result(23)
    );
  clk_divider_Mcount_cnt_cy_24_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(23),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_24_rt_222,
      O => clk_divider_Mcount_cnt_cy(24)
    );
  clk_divider_Mcount_cnt_xor_24_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(23),
      I1 => clk_divider_Mcount_cnt_cy_24_rt_222,
      O => Result(24)
    );
  clk_divider_Mcount_cnt_cy_25_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(24),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_25_rt_224,
      O => clk_divider_Mcount_cnt_cy(25)
    );
  clk_divider_Mcount_cnt_xor_25_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(24),
      I1 => clk_divider_Mcount_cnt_cy_25_rt_224,
      O => Result(25)
    );
  clk_divider_Mcount_cnt_cy_26_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(25),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_26_rt_226,
      O => clk_divider_Mcount_cnt_cy(26)
    );
  clk_divider_Mcount_cnt_xor_26_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(25),
      I1 => clk_divider_Mcount_cnt_cy_26_rt_226,
      O => Result(26)
    );
  clk_divider_Mcount_cnt_cy_27_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(26),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_27_rt_228,
      O => clk_divider_Mcount_cnt_cy(27)
    );
  clk_divider_Mcount_cnt_xor_27_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(26),
      I1 => clk_divider_Mcount_cnt_cy_27_rt_228,
      O => Result(27)
    );
  clk_divider_Mcount_cnt_cy_28_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(27),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_28_rt_230,
      O => clk_divider_Mcount_cnt_cy(28)
    );
  clk_divider_Mcount_cnt_xor_28_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(27),
      I1 => clk_divider_Mcount_cnt_cy_28_rt_230,
      O => Result(28)
    );
  clk_divider_Mcount_cnt_cy_29_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(28),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_29_rt_232,
      O => clk_divider_Mcount_cnt_cy(29)
    );
  clk_divider_Mcount_cnt_xor_29_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(28),
      I1 => clk_divider_Mcount_cnt_cy_29_rt_232,
      O => Result(29)
    );
  clk_divider_Mcount_cnt_cy_30_Q : X_MUX2
    port map (
      IB => clk_divider_Mcount_cnt_cy(29),
      IA => N0,
      SEL => clk_divider_Mcount_cnt_cy_30_rt_236,
      O => clk_divider_Mcount_cnt_cy(30)
    );
  clk_divider_Mcount_cnt_xor_30_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(29),
      I1 => clk_divider_Mcount_cnt_cy_30_rt_236,
      O => Result(30)
    );
  clk_divider_Mcount_cnt_xor_31_Q : X_XOR2
    port map (
      I0 => clk_divider_Mcount_cnt_cy(30),
      I1 => clk_divider_Mcount_cnt_xor_31_rt_252,
      O => Result(31)
    );
  Inst_dcm1_DCM_SP_INST : X_DCM_SP
    generic map(
      CLKDV_DIVIDE => 2.000000,
      CLKFX_DIVIDE => 25,
      CLKFX_MULTIPLY => 13,
      CLKIN_DIVIDE_BY_2 => FALSE,
      CLKIN_PERIOD => 20.000000,
      CLKOUT_PHASE_SHIFT => "NONE",
      CLK_FEEDBACK => "1X",
      DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE => "LOW",
      DLL_FREQUENCY_MODE => "LOW",
      DSS_MODE => "NONE",
      DUTY_CYCLE_CORRECTION => TRUE,
      PHASE_SHIFT => 0,
      STARTUP_WAIT => TRUE,
      FACTORY_JF => X"C080"
    )
    port map (
      CLKIN => Inst_dcm1_CLKIN_IBUFG,
      CLKFB => Inst_dcm1_CLKFB_IN,
      RST => reset_IBUF_824,
      DSSEN => N0,
      PSINCDEC => N0,
      PSEN => N0,
      PSCLK => N0,
      CLK0 => Inst_dcm1_CLK0_BUF,
      CLK90 => NLW_Inst_dcm1_DCM_SP_INST_CLK90_UNCONNECTED,
      CLK180 => NLW_Inst_dcm1_DCM_SP_INST_CLK180_UNCONNECTED,
      CLK270 => NLW_Inst_dcm1_DCM_SP_INST_CLK270_UNCONNECTED,
      CLK2X => Inst_dcm1_CLK2X_BUF,
      CLK2X180 => NLW_Inst_dcm1_DCM_SP_INST_CLK2X180_UNCONNECTED,
      CLKDV => NLW_Inst_dcm1_DCM_SP_INST_CLKDV_UNCONNECTED,
      CLKFX => Inst_dcm1_CLKFX_BUF,
      CLKFX180 => NLW_Inst_dcm1_DCM_SP_INST_CLKFX180_UNCONNECTED,
      LOCKED => locked,
      PSDONE => NLW_Inst_dcm1_DCM_SP_INST_PSDONE_UNCONNECTED,
      STATUS(7) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_7_UNCONNECTED,
      STATUS(6) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_6_UNCONNECTED,
      STATUS(5) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_5_UNCONNECTED,
      STATUS(4) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_4_UNCONNECTED,
      STATUS(3) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_3_UNCONNECTED,
      STATUS(2) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_2_UNCONNECTED,
      STATUS(1) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_1_UNCONNECTED,
      STATUS(0) => NLW_Inst_dcm1_DCM_SP_INST_STATUS_0_UNCONNECTED
    );
  Inst_dcm1_CLK2X_BUFG_INST : X_CKBUF
    port map (
      I => Inst_dcm1_CLK2X_BUF,
      O => NLW_Inst_dcm1_CLK2X_BUFG_INST_O_UNCONNECTED
    );
  Inst_dcm1_CLK0_BUFG_INST : X_CKBUF
    port map (
      I => Inst_dcm1_CLK0_BUF,
      O => Inst_dcm1_CLKFB_IN
    );
  Inst_dcm1_CLKIN_IBUFG_INST : X_CKBUF
    port map (
      I => clk,
      O => Inst_dcm1_CLKIN_IBUFG
    );
  Inst_dcm1_CLKFX_BUFG_INST : X_CKBUF
    port map (
      I => Inst_dcm1_CLKFX_BUF,
      O => clk_26214k
    );
  lcd_cntrl_inst_state_FSM_FFd46 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_state_FSM_FFd46_In,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd46_756,
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd43 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_state_FSM_FFd43_In,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd43_752,
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_31_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(30),
      I1 => lcd_cntrl_inst_Mcount_count_temp_xor_31_rt_458,
      O => lcd_cntrl_inst_Result(31)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_30_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(29),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_30_rt_442,
      O => lcd_cntrl_inst_Result(30)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_30_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(29),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_30_rt_442,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(30)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_29_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(28),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_29_rt_438,
      O => lcd_cntrl_inst_Result(29)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_29_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(28),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_29_rt_438,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(29)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_28_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(27),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_28_rt_436,
      O => lcd_cntrl_inst_Result(28)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_28_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(27),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_28_rt_436,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(28)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_27_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(26),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_27_rt_434,
      O => lcd_cntrl_inst_Result(27)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_27_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(26),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_27_rt_434,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(27)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_26_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(25),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_26_rt_432,
      O => lcd_cntrl_inst_Result(26)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_26_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(25),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_26_rt_432,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(26)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_25_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(24),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_25_rt_430,
      O => lcd_cntrl_inst_Result(25)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_25_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(24),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_25_rt_430,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(25)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_24_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(23),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_24_rt_428,
      O => lcd_cntrl_inst_Result(24)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_24_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(23),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_24_rt_428,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(24)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_23_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(22),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_23_rt_426,
      O => lcd_cntrl_inst_Result(23)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_23_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(22),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_23_rt_426,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(23)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_22_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(21),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_22_rt_424,
      O => lcd_cntrl_inst_Result(22)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_22_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(21),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_22_rt_424,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(22)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_21_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(20),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_21_rt_422,
      O => lcd_cntrl_inst_Result(21)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_21_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(20),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_21_rt_422,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(21)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_20_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(19),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_20_rt_420,
      O => lcd_cntrl_inst_Result(20)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_20_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(19),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_20_rt_420,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(20)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_19_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(18),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_19_rt_416,
      O => lcd_cntrl_inst_Result(19)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_19_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(18),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_19_rt_416,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(19)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_18_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(17),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_18_rt_414,
      O => lcd_cntrl_inst_Result(18)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_18_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(17),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_18_rt_414,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(18)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_17_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(16),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_17_rt_412,
      O => lcd_cntrl_inst_Result(17)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_17_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(16),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_17_rt_412,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(17)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_16_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(15),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_16_rt_410,
      O => lcd_cntrl_inst_Result(16)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_16_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(15),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_16_rt_410,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(16)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_15_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(14),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_15_rt_408,
      O => lcd_cntrl_inst_Result(15)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_15_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(14),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_15_rt_408,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(15)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_14_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(13),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_14_rt_406,
      O => lcd_cntrl_inst_Result(14)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_14_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(13),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_14_rt_406,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(14)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_13_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(12),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_13_rt_404,
      O => lcd_cntrl_inst_Result(13)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_13_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(12),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_13_rt_404,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(13)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_12_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(11),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_12_rt_402,
      O => lcd_cntrl_inst_Result(12)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_12_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(11),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_12_rt_402,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(12)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_11_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(10),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_11_rt_400,
      O => lcd_cntrl_inst_Result(11)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_11_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(10),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_11_rt_400,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(11)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_10_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(9),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_10_rt_398,
      O => lcd_cntrl_inst_Result(10)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_10_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(9),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_10_rt_398,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(10)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_9_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(8),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_9_rt_456,
      O => lcd_cntrl_inst_Result(9)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_9_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(8),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_9_rt_456,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(9)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_8_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(7),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_8_rt_454,
      O => lcd_cntrl_inst_Result(8)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_8_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(7),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_8_rt_454,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(8)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_7_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(6),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_7_rt_452,
      O => lcd_cntrl_inst_Result(7)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_7_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(6),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_7_rt_452,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(7)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_6_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(5),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_6_rt_450,
      O => lcd_cntrl_inst_Result(6)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_6_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(5),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_6_rt_450,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(6)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_5_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(4),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_5_rt_448,
      O => lcd_cntrl_inst_Result(5)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_5_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(4),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_5_rt_448,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(5)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_4_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(3),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_4_rt_446,
      O => lcd_cntrl_inst_Result(4)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_4_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(3),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_4_rt_446,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(4)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_3_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(2),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_3_rt_444,
      O => lcd_cntrl_inst_Result(3)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_3_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(2),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_3_rt_444,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(3)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_2_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(1),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_2_rt_440,
      O => lcd_cntrl_inst_Result(2)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_2_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(1),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_2_rt_440,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(2)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_1_Q : X_XOR2
    port map (
      I0 => lcd_cntrl_inst_Mcount_count_temp_cy(0),
      I1 => lcd_cntrl_inst_Mcount_count_temp_cy_1_rt_418,
      O => lcd_cntrl_inst_Result(1)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_1_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcount_count_temp_cy(0),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcount_count_temp_cy_1_rt_418,
      O => lcd_cntrl_inst_Mcount_count_temp_cy(1)
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_0_Q : X_XOR2
    port map (
      I0 => N0,
      I1 => lcd_cntrl_inst_Mcount_count_temp_lut(0),
      O => lcd_cntrl_inst_Result(0)
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_0_Q : X_MUX2
    port map (
      IB => N0,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcount_count_temp_lut(0),
      O => lcd_cntrl_inst_Mcount_count_temp_cy(0)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_12_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(11),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_12_Q,
      O => lcd_cntrl_inst_state_cmp_ge0000
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_11_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(10),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_11_Q_386,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(11)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_11_Q : X_LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(28),
      ADR1 => lcd_cntrl_inst_count(29),
      ADR2 => lcd_cntrl_inst_count(30),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_11_Q_386
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_10_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(9),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_10_Q_385,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(10)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_10_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(24),
      ADR1 => lcd_cntrl_inst_count(25),
      ADR2 => lcd_cntrl_inst_count(26),
      ADR3 => lcd_cntrl_inst_count(27),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_10_Q_385
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_9_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(8),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_9_Q_395,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(9)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_9_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(20),
      ADR1 => lcd_cntrl_inst_count(21),
      ADR2 => lcd_cntrl_inst_count(22),
      ADR3 => lcd_cntrl_inst_count(23),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_9_Q_395
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_8_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(7),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_8_rt_382,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(8)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_7_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(6),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_7_Q,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(7)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_6_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(5),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_6_Q_393,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(6)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_6_Q : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(16),
      ADR1 => lcd_cntrl_inst_count(17),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_6_Q_393
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_5_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(4),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_5_Q,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(5)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_4_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(3),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_4_Q_391,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(4)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_4_Q : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(12),
      ADR1 => lcd_cntrl_inst_count(13),
      ADR2 => lcd_cntrl_inst_count(14),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_4_Q_391
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_3_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(2),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_3_Q_390,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(3)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_3_Q : X_LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(9),
      ADR1 => lcd_cntrl_inst_count(10),
      ADR2 => lcd_cntrl_inst_count(11),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_3_Q_390
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_2_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(1),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_2_Q_389,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(2)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_2_Q : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(7),
      ADR1 => lcd_cntrl_inst_count(8),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_2_Q_389
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_1_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(0),
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_1_Q,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(1)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_0_Q : X_MUX2
    port map (
      IB => N1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_0_Q_384,
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy(0)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_0_Q : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(4),
      ADR1 => lcd_cntrl_inst_count(5),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_0_Q_384
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_14_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_13_1,
      IA => lcd_cntrl_inst_count(31),
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_14_1,
      O => lcd_cntrl_inst_control_cmp_le0001
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_13_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_12_1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_13_1_348,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_13_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_13_1 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(27),
      ADR1 => lcd_cntrl_inst_count(28),
      ADR2 => lcd_cntrl_inst_count(29),
      ADR3 => lcd_cntrl_inst_count(30),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_13_1_348
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_12_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_11_1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_12_1_346,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_12_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_12_1 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(23),
      ADR1 => lcd_cntrl_inst_count(24),
      ADR2 => lcd_cntrl_inst_count(25),
      ADR3 => lcd_cntrl_inst_count(26),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_12_1_346
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_11_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_10_1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_11_1_344,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_11_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_11_1 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(19),
      ADR1 => lcd_cntrl_inst_count(20),
      ADR2 => lcd_cntrl_inst_count(21),
      ADR3 => lcd_cntrl_inst_count(22),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_11_1_344
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_10_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_9_1,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_10_1_342,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_10_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_10_1 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(17),
      ADR1 => lcd_cntrl_inst_count(18),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_10_1_342
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_9_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_2,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_9_1_370,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_9_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_9_1 : X_LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(15),
      ADR1 => lcd_cntrl_inst_count(16),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_9_1_370
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_2,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_2_368,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_2 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(13),
      ADR1 => lcd_cntrl_inst_count(14),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_2_368
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_2,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_2,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_2,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_rt_329,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_2,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_5_1_360,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_5_1 : X_LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(8),
      ADR1 => lcd_cntrl_inst_count(9),
      ADR2 => lcd_cntrl_inst_count(10),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_5_1_360
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_2,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_rt_321,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_2,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_3_1,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_2,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_rt_313,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_1 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_2,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_1_1,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_1 : X_MUX2
    port map (
      IB => N1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_2_340,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_2 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(0),
      ADR1 => lcd_cntrl_inst_count(1),
      ADR2 => lcd_cntrl_inst_count(2),
      ADR3 => lcd_cntrl_inst_count(3),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_2_340
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_14_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(13),
      IA => lcd_cntrl_inst_count(31),
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(14),
      O => lcd_cntrl_inst_control_cmp_le0000
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_13_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(12),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(13),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(13)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_12_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(11),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(12),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(12)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_12_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(26),
      ADR1 => lcd_cntrl_inst_count(27),
      ADR2 => lcd_cntrl_inst_count(28),
      ADR3 => lcd_cntrl_inst_count(29),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(12)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_11_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(10),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(11),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(11)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_11_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(22),
      ADR1 => lcd_cntrl_inst_count(23),
      ADR2 => lcd_cntrl_inst_count(24),
      ADR3 => lcd_cntrl_inst_count(25),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(11)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_10_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(9),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(10),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(10)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_10_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(18),
      ADR1 => lcd_cntrl_inst_count(19),
      ADR2 => lcd_cntrl_inst_count(20),
      ADR3 => lcd_cntrl_inst_count(21),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(10)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_9_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_1_334,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(9),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(9)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_9_Q : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(16),
      ADR1 => lcd_cntrl_inst_count(17),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(9)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_1_331,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_1_367,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_1_334
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_1 : X_LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(14),
      ADR1 => lcd_cntrl_inst_count(15),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_1_367
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_327,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_1_364,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_1_331
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_1 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(12),
      ADR1 => lcd_cntrl_inst_count(13),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_1_364
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_1_323,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_6_1,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_327
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_319,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_0_rt_325,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_1_323
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_1_315,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_4_1_358,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_319
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_4_1 : X_LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(7),
      ADR1 => lcd_cntrl_inst_count(8),
      ADR2 => lcd_cntrl_inst_count(9),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_4_1_358
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_311,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_0_rt_317,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_1_315
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_1_307,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_2_1,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_311
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_0 : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_1_296,
      IA => N1,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_0_rt_309,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_1_307
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_0 : X_MUX2
    port map (
      IB => N1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_1_339,
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_1_296
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_1 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(0),
      ADR1 => lcd_cntrl_inst_count(1),
      ADR2 => lcd_cntrl_inst_count(2),
      ADR3 => lcd_cntrl_inst_count(3),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_1_339
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_8_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(7),
      IA => lcd_cntrl_inst_count(31),
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(8),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(8)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_7_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(6),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(7),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(7)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_Q : X_LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(29),
      ADR1 => lcd_cntrl_inst_count(30),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(7)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(5),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(6),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(6)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_6_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(25),
      ADR1 => lcd_cntrl_inst_count(26),
      ADR2 => lcd_cntrl_inst_count(27),
      ADR3 => lcd_cntrl_inst_count(28),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(6)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(4),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(5),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(5)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_5_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(21),
      ADR1 => lcd_cntrl_inst_count(22),
      ADR2 => lcd_cntrl_inst_count(23),
      ADR3 => lcd_cntrl_inst_count(24),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(5)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(3),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(4),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(4)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_4_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(17),
      ADR1 => lcd_cntrl_inst_count(18),
      ADR2 => lcd_cntrl_inst_count(19),
      ADR3 => lcd_cntrl_inst_count(20),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(4)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(2),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(3),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(3)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_3_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(13),
      ADR1 => lcd_cntrl_inst_count(14),
      ADR2 => lcd_cntrl_inst_count(15),
      ADR3 => lcd_cntrl_inst_count(16),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(3)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(1),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(2),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(2)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_2_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(9),
      ADR1 => lcd_cntrl_inst_count(10),
      ADR2 => lcd_cntrl_inst_count(11),
      ADR3 => lcd_cntrl_inst_count(12),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(2)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(0),
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(1),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(1)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_1_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(5),
      ADR1 => lcd_cntrl_inst_count(6),
      ADR2 => lcd_cntrl_inst_count(7),
      ADR3 => lcd_cntrl_inst_count(8),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(1)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_0_Q : X_MUX2
    port map (
      IB => N1,
      IA => N0,
      SEL => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(0),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(0)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_0_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(1),
      ADR1 => lcd_cntrl_inst_count(2),
      ADR2 => lcd_cntrl_inst_count(3),
      ADR3 => lcd_cntrl_inst_count(4),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(0)
    );
  lcd_cntrl_inst_count_temp_31 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(31),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(31),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_30 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(30),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(30),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_29 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(29),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(29),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_28 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(28),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(28),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_27 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(27),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(27),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_26 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(26),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(26),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_25 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(25),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(25),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_24 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(24),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(24),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_23 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(23),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(23),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_22 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(22),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(22),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_21 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(21),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(21),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_20 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(20),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(20),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_19 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(19),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(19),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_18 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(18),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(18),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_17 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(17),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(17),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_16 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(16),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(16),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_15 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(15),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(15),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_14 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(14),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(14),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_13 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(13),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(13),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_12 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(12),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(12),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_11 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(11),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(11),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_10 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(10),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(10),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_9 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(9),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(9),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_8 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(8),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(8),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_7 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(7),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(7),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_6 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(6),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(6),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_5 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(5),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(5),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_4 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(4),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(4),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(3),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(3),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(2),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(2),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(1),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(1),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_temp_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_Result(0),
      SRST => lcd_cntrl_inst_count_temp_or0000_579,
      O => lcd_cntrl_inst_count_temp(0),
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_mode_state : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_next_mode_state_604,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_mode_state_603,
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_count_31 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(31),
      O => lcd_cntrl_inst_count(31),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_30 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(30),
      O => lcd_cntrl_inst_count(30),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_29 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(29),
      O => lcd_cntrl_inst_count(29),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_28 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(28),
      O => lcd_cntrl_inst_count(28),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_27 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(27),
      O => lcd_cntrl_inst_count(27),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_26 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(26),
      O => lcd_cntrl_inst_count(26),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_25 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(25),
      O => lcd_cntrl_inst_count(25),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_24 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(24),
      O => lcd_cntrl_inst_count(24),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_23 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(23),
      O => lcd_cntrl_inst_count(23),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_22 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(22),
      O => lcd_cntrl_inst_count(22),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_21 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(21),
      O => lcd_cntrl_inst_count(21),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_20 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(20),
      O => lcd_cntrl_inst_count(20),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_19 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(19),
      O => lcd_cntrl_inst_count(19),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_18 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(18),
      O => lcd_cntrl_inst_count(18),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_17 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(17),
      O => lcd_cntrl_inst_count(17),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_16 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(16),
      O => lcd_cntrl_inst_count(16),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_15 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(15),
      O => lcd_cntrl_inst_count(15),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_14 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(14),
      O => lcd_cntrl_inst_count(14),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_13 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(13),
      O => lcd_cntrl_inst_count(13),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_12 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(12),
      O => lcd_cntrl_inst_count(12),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_11 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(11),
      O => lcd_cntrl_inst_count(11),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_10 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(10),
      O => lcd_cntrl_inst_count(10),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_9 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(9),
      O => lcd_cntrl_inst_count(9),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_8 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(8),
      O => lcd_cntrl_inst_count(8),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_7 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(7),
      O => lcd_cntrl_inst_count(7),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_6 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(6),
      O => lcd_cntrl_inst_count(6),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_5 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(5),
      O => lcd_cntrl_inst_count(5),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_4 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(4),
      O => lcd_cntrl_inst_count(4),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(3),
      O => lcd_cntrl_inst_count(3),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(2),
      O => lcd_cntrl_inst_count(2),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(1),
      O => lcd_cntrl_inst_count(1),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_count_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_count_temp(0),
      O => lcd_cntrl_inst_count(0),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_timer_flag : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_timer_flag_not0001,
      I => lcd_cntrl_inst_timer_flag_mux0000,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_timer_flag_785,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_flag : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_lap_flag_not0001,
      I => N0,
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      SSET => timer_state_sreg1_FSM_FFd1_948,
      O => lcd_cntrl_inst_lap_flag_580,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_sf_d_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_3_Q,
      O => lcd_cntrl_inst_sf_d(3),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_sf_d_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_1_Q,
      O => lcd_cntrl_inst_sf_d(1),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  lcd_cntrl_inst_lap_min_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_mins_cnt(3),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_min(3),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_min_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_mins_cnt(2),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_min(2),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_min_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_mins_cnt(1),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_min(1),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_min_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_mins_cnt(0),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_min(0),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tenths_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tenths_cnt(3),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tenths(3),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tenths_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tenths_cnt(2),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tenths(2),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tenths_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tenths_cnt(1),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tenths(1),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tenths_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tenths_cnt(0),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tenths(0),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_hundredths_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_hundredths_cnt(3),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_hundredths(3),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_hundredths_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_hundredths_cnt(2),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_hundredths(2),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_hundredths_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_hundredths_cnt(1),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_hundredths(1),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_hundredths_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_hundredths_cnt(0),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_hundredths(0),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tens_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tens_cnt(3),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tens(3),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tens_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tens_cnt(2),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tens(2),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tens_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tens_cnt(1),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tens(1),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_tens_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_tens_cnt(0),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_tens(0),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_clock_flag : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      CE => mode_control,
      I => lcd_cntrl_inst_clock_flag_mux0000,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_clock_flag_503,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_next_mode_state : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      I => mode_control,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_next_mode_state_604,
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_ones_3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_ones_cnt(3),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_ones(3),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_ones_2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_ones_cnt(2),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_ones(2),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_ones_1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_ones_cnt(1),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_ones(1),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_lap_ones_0 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => timer_state_sreg1_FSM_FFd1_948,
      I => timer_inst_ones_cnt(0),
      SRST => lcd_cntrl_inst_lap_ones_or0000,
      O => lcd_cntrl_inst_lap_ones(0),
      SET => GND,
      RST => GND,
      SSET => GND
    );
  timer_inst_tenths_cnt_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tenths_cnt_mux0002(7),
      O => timer_inst_tenths_cnt(3),
      SET => GND
    );
  timer_inst_tenths_cnt_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tenths_cnt_mux0002(6),
      O => timer_inst_tenths_cnt(2),
      SET => GND
    );
  timer_inst_tenths_cnt_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tenths_cnt_mux0002(5),
      O => timer_inst_tenths_cnt(1),
      SET => GND
    );
  timer_inst_tenths_cnt_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tenths_cnt_mux0002(4),
      O => timer_inst_tenths_cnt(0),
      SET => GND
    );
  timer_inst_hundredths_cnt_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_hundredths_cnt_mux0003(3),
      O => timer_inst_hundredths_cnt(3),
      SET => GND
    );
  timer_inst_hundredths_cnt_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_hundredths_cnt_mux0003(2),
      O => timer_inst_hundredths_cnt(2),
      SET => GND
    );
  timer_inst_hundredths_cnt_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_hundredths_cnt_mux0003(1),
      O => timer_inst_hundredths_cnt(1),
      SET => GND
    );
  timer_inst_hundredths_cnt_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_hundredths_cnt_mux0003(0),
      O => timer_inst_hundredths_cnt(0),
      SET => GND
    );
  timer_inst_tens_cnt_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tens_cnt_mux0002(15),
      O => timer_inst_tens_cnt(3),
      SET => GND
    );
  timer_inst_tens_cnt_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tens_cnt_mux0002(14),
      O => timer_inst_tens_cnt(2),
      SET => GND
    );
  timer_inst_tens_cnt_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tens_cnt_mux0002(13),
      O => timer_inst_tens_cnt(1),
      SET => GND
    );
  timer_inst_tens_cnt_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_tens_cnt_mux0002(12),
      O => timer_inst_tens_cnt(0),
      SET => GND
    );
  timer_inst_ones_cnt_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_ones_cnt_mux0002(11),
      O => timer_inst_ones_cnt(3),
      SET => GND
    );
  timer_inst_ones_cnt_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_ones_cnt_mux0002(10),
      O => timer_inst_ones_cnt(2),
      SET => GND
    );
  timer_inst_ones_cnt_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_ones_cnt_mux0002(9),
      O => timer_inst_ones_cnt(1),
      SET => GND
    );
  timer_inst_ones_cnt_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_ones_cnt_mux0002(8),
      O => timer_inst_ones_cnt(0),
      SET => GND
    );
  timer_inst_mins_cnt_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_mins_cnt_mux0002(19),
      O => timer_inst_mins_cnt(3),
      SET => GND
    );
  timer_inst_mins_cnt_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_mins_cnt_mux0002(18),
      O => timer_inst_mins_cnt(2),
      SET => GND
    );
  timer_inst_mins_cnt_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_mins_cnt_mux0002(17),
      O => timer_inst_mins_cnt(1),
      SET => GND
    );
  timer_inst_mins_cnt_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      CE => timer_inst_ones_cnt_not0002,
      RST => timer_state_rst_947,
      I => timer_inst_mins_cnt_mux0002(16),
      O => timer_inst_mins_cnt(0),
      SET => GND
    );
  timer_state_sreg_FSM_FFd12 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd12_In_958,
      O => timer_state_sreg_FSM_FFd12_957,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd11 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd11_In,
      O => timer_state_sreg_FSM_FFd11_955,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd9 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd9_In_976,
      O => timer_state_sreg_FSM_FFd9_975,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd8 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd8_In,
      O => timer_state_sreg_FSM_FFd8_973,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd10 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd10_In,
      O => timer_state_sreg_FSM_FFd10_953,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd6 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd6_In,
      O => timer_state_sreg_FSM_FFd6_969,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd5 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd5_In,
      O => timer_state_sreg_FSM_FFd5_967,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd7 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd7_In,
      O => timer_state_sreg_FSM_FFd7_971,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd4 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd4_In,
      O => timer_state_sreg_FSM_FFd4_963,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd3_In,
      O => timer_state_sreg_FSM_FFd3_961,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd2_In,
      O => timer_state_sreg_FSM_FFd2_959,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg_FSM_FFd1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_sreg_FSM_FFd1_In,
      O => timer_state_sreg_FSM_FFd1_951,
      CE => VCC,
      SET => GND
    );
  timer_state_sreg1_FSM_FFd2 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      I => timer_state_sreg1_FSM_FFd2_In,
      SSET => timer_state_state_reset,
      O => timer_state_sreg1_FSM_FFd2_949,
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  timer_state_sreg1_FSM_FFd1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      I => timer_state_next_lap_trigger1,
      SRST => timer_state_state_reset,
      O => timer_state_sreg1_FSM_FFd1_948,
      CE => VCC,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  timer_state_clken : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      RST => timer_state_state_reset,
      I => timer_state_next_clken,
      O => timer_state_clken_943,
      CE => VCC,
      SET => GND
    );
  timer_state_rst : X_FF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_divider_div_262144_286,
      I => N0,
      SET => timer_state_state_reset,
      O => timer_state_rst_947,
      CE => VCC,
      RST => GND
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_0_Q : X_LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_min(3),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd13_711,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_0_Q_679
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_0_Q : X_MUX2
    port map (
      IB => N0,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_0_Q_679,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_cy_0_Q_673
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_1_Q : X_LUT4
    generic map(
      INIT => X"135F"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd9_763,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR2 => lcd_cntrl_inst_lap_tens(3),
      ADR3 => timer_inst_hundredths_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_1_Q_680
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_1_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_3_wg_cy_0_Q_673,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_1_Q_680,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_cy_1_Q_674
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_2_Q : X_LUT4
    generic map(
      INIT => X"135F"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd7_761,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR2 => lcd_cntrl_inst_lap_ones(3),
      ADR3 => timer_inst_tenths_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_2_Q_681
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_2_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_3_wg_cy_1_Q_674,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_2_Q_681,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_cy_2_Q_675
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_3_Q : X_LUT4
    generic map(
      INIT => X"135F"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd3_730,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd21_721,
      ADR2 => lcd_cntrl_inst_lap_tenths(3),
      ADR3 => timer_inst_ones_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_3_Q_682
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_3_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_3_wg_cy_2_Q_675,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_3_Q_682,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_cy_3_Q_676
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_4_Q : X_LUT4
    generic map(
      INIT => X"135F"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd27_727,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR2 => timer_inst_mins_cnt(3),
      ADR3 => timer_inst_tens_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_4_Q_683
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_4_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_3_wg_cy_3_Q_676,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_4_Q_683,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_cy_4_Q_677
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_5_Q : X_LUT4
    generic map(
      INIT => X"0111"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd31_732,
      ADR1 => lcd_cntrl_inst_N8,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd1_707,
      ADR3 => lcd_cntrl_inst_lap_hundredths(3),
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_5_Q_684
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_5_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_3_wg_cy_4_Q_677,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_5_Q_684,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_cy_5_Q_678
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_lut_6_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd37_742,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd38_743,
      ADR2 => lcd_cntrl_inst_N111,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd42_751,
      O => lcd_cntrl_inst_sf_d_temp_3_wg_lut_6_Q_685
    );
  lcd_cntrl_inst_sf_d_temp_3_wg_cy_6_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_3_wg_cy_5_Q_678,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_3_wg_lut_6_Q_685,
      O => lcd_cntrl_inst_sf_d_temp_3_Q
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_lut_0_Q : X_LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(27),
      ADR1 => lcd_cntrl_inst_count(23),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(0)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_cy_0_Q : X_MUX2
    port map (
      IB => N1,
      IA => N0,
      SEL => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(0),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(0)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_lut_1_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(24),
      ADR1 => lcd_cntrl_inst_count(25),
      ADR2 => lcd_cntrl_inst_count(28),
      ADR3 => lcd_cntrl_inst_count(22),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(1)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_cy_1_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(0),
      IA => N0,
      SEL => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(1),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(1)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_lut_2_Q : X_LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(21),
      ADR1 => lcd_cntrl_inst_count(17),
      ADR2 => lcd_cntrl_inst_count(26),
      ADR3 => lcd_cntrl_inst_count(20),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(2)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_cy_2_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(1),
      IA => N0,
      SEL => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(2),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(2)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_lut_3_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(19),
      ADR1 => lcd_cntrl_inst_count(9),
      ADR2 => lcd_cntrl_inst_count(30),
      ADR3 => lcd_cntrl_inst_count(15),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(3)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_cy_3_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(2),
      IA => N0,
      SEL => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(3),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(3)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_lut_4_Q : X_LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(3),
      ADR1 => lcd_cntrl_inst_count(13),
      ADR2 => lcd_cntrl_inst_count(31),
      ADR3 => lcd_cntrl_inst_count(8),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(4)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_cy_4_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(3),
      IA => N0,
      SEL => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(4),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(4)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_lut_5_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(2),
      ADR1 => lcd_cntrl_inst_count(1),
      ADR2 => lcd_cntrl_inst_count(29),
      ADR3 => lcd_cntrl_inst_count(0),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(5)
    );
  lcd_cntrl_inst_state_cmp_eq00001_wg_cy_5_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(4),
      IA => N0,
      SEL => lcd_cntrl_inst_state_cmp_eq00001_wg_lut(5),
      O => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(5)
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_0_Q : X_LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_tens(1),
      ADR1 => lcd_cntrl_inst_lap_tens(3),
      ADR2 => lcd_cntrl_inst_state_FSM_FFd9_763,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_0_Q_650
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_0_Q : X_MUX2
    port map (
      IB => N0,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_0_Q_650,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_0_Q_640
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_1_Q : X_LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_hundredths(1),
      ADR1 => lcd_cntrl_inst_lap_hundredths(3),
      ADR2 => lcd_cntrl_inst_state_FSM_FFd1_707,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_1_Q_652
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_1_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_0_Q_640,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_1_Q_652,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_1_Q_641
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_2_Q : X_LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(1),
      ADR1 => timer_inst_hundredths_cnt(3),
      ADR2 => lcd_cntrl_inst_state_FSM_FFd15_714,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_2_Q_653
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_2_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_1_Q_641,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_2_Q_653,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_2_Q_642
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_3_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_min(1),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd4_745,
      ADR2 => lcd_cntrl_inst_lap_min(3),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd13_711,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_3_Q_654
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_3_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_2_Q_642,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_3_Q_654,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_3_Q_643
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_4_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_ones(1),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd18_717,
      ADR2 => lcd_cntrl_inst_lap_ones(3),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd7_761,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_4_Q_655
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_4_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_3_Q_643,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_4_Q_655,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_4_Q_644
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_5_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_tenths(1),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd20_720,
      ADR2 => lcd_cntrl_inst_lap_tenths(3),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd3_730,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_5_Q_656
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_5_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_4_Q_644,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_5_Q_656,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_5_Q_645
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_6_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(1),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd14_712,
      ADR2 => timer_inst_mins_cnt(3),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd27_727,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_6_Q_657
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_6_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_5_Q_645,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_6_Q_657,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_6_Q_646
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_7_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd33_734,
      ADR2 => timer_inst_tenths_cnt(3),
      ADR3 => timer_inst_tenths_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_7_Q_658
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_7_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_6_Q_646,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_7_Q_658,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_7_Q_647
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_8_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(1),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd34_737,
      ADR2 => timer_inst_ones_cnt(3),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd21_721,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_8_Q_659
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_8_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_7_Q_647,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_8_Q_659,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_8_Q_648
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_9_Q : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd26_726,
      ADR2 => timer_inst_tens_cnt(3),
      ADR3 => timer_inst_tens_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_9_Q_660
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_9_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_8_Q_648,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_9_Q_660,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_cy_9_Q_649
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_lut_10_Q : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd12_710,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd6_760,
      ADR2 => lcd_cntrl_inst_N13,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd40_746,
      O => lcd_cntrl_inst_sf_d_temp_1_wg_lut_10_Q_651
    );
  lcd_cntrl_inst_sf_d_temp_1_wg_cy_10_Q : X_MUX2
    port map (
      IB => lcd_cntrl_inst_sf_d_temp_1_wg_cy_9_Q_649,
      IA => N1,
      SEL => lcd_cntrl_inst_sf_d_temp_1_wg_lut_10_Q_651,
      O => lcd_cntrl_inst_sf_d_temp_1_Q
    );
  lcd_cntrl_inst_lap_ones_or00001 : X_LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      ADR0 => mode_control,
      ADR1 => timer_state_rst_947,
      O => lcd_cntrl_inst_lap_ones_or0000
    );
  strtstop_debounce_sig_out1 : X_LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      ADR0 => strtstop_debounce_Q3_837,
      ADR1 => strtstop_debounce_Q2_836,
      ADR2 => strtstop_debounce_Q1_835,
      O => strtstop_debounced
    );
  mode_debounce_sig_out1 : X_LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      ADR0 => mode_debounce_Q3_801,
      ADR1 => mode_debounce_Q2_800,
      ADR2 => mode_debounce_Q1_799,
      O => mode_debounced
    );
  lap_load_debounce_sig_out1 : X_LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      ADR0 => lap_load_debounce_Q3_294,
      ADR1 => lap_load_debounce_Q2_293,
      ADR2 => lap_load_debounce_Q1_292,
      O => ll_debounced
    );
  Mcount_address_xor_1_11 : X_LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      ADR0 => address(0),
      ADR1 => address(1),
      O => Mcount_address1
    );
  timer_state_sreg_FSM_FFd8_In1 : X_LUT3
    generic map(
      INIT => X"C8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd7_971,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd8_973,
      O => timer_state_sreg_FSM_FFd8_In
    );
  timer_state_sreg_FSM_FFd7_In1 : X_LUT3
    generic map(
      INIT => X"32"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd7_971,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd10_953,
      O => timer_state_sreg_FSM_FFd7_In
    );
  timer_state_sreg_FSM_FFd6_In1 : X_LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => ll_debounced,
      ADR2 => timer_state_sreg_FSM_FFd6_969,
      O => timer_state_sreg_FSM_FFd6_In
    );
  timer_state_sreg_FSM_FFd2_In1 : X_LUT3
    generic map(
      INIT => X"C8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd1_951,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd2_959,
      O => timer_state_sreg_FSM_FFd2_In
    );
  timer_state_sreg_FSM_FFd10_In1 : X_LUT3
    generic map(
      INIT => X"C8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd9_975,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd10_953,
      O => timer_state_sreg_FSM_FFd10_In
    );
  timer_state_sreg_FSM_FFd1_In1 : X_LUT3
    generic map(
      INIT => X"32"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd1_951,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd3_961,
      O => timer_state_sreg_FSM_FFd1_In
    );
  Mcount_address_xor_2_11 : X_LUT3
    generic map(
      INIT => X"6C"
    )
    port map (
      ADR0 => address(1),
      ADR1 => address(2),
      ADR2 => address(0),
      O => Mcount_address2
    );
  timer_state_sreg_FSM_FFd3_In1 : X_LUT4
    generic map(
      INIT => X"88C8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd3_961,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd4_963,
      ADR3 => ll_debounced,
      O => timer_state_sreg_FSM_FFd3_In
    );
  timer_state_sreg_FSM_FFd11_In1 : X_LUT4
    generic map(
      INIT => X"88C8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd11_955,
      ADR1 => mode_debounced,
      ADR2 => timer_state_sreg_FSM_FFd9_975,
      ADR3 => strtstop_debounced,
      O => timer_state_sreg_FSM_FFd11_In
    );
  timer_state_state_reset_and00001 : X_LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      ADR0 => reset_IBUF_824,
      ADR1 => locked,
      O => timer_state_state_reset
    );
  lcd_cntrl_inst_sf_d_temp_0_51 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd12_710,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd36_741,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd37_742,
      O => lcd_cntrl_inst_N16
    );
  lcd_cntrl_inst_sf_d_temp_2_1 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd2_719,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd14_712,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd31_732,
      ADR3 => N8,
      O => lcd_cntrl_inst_N12
    );
  lcd_cntrl_inst_sf_d_temp_0_25 : X_LUT4
    generic map(
      INIT => X"FF80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd1_707,
      ADR1 => lcd_cntrl_inst_lap_hundredths(3),
      ADR2 => lcd_cntrl_inst_lap_hundredths(1),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd26_726,
      O => lcd_cntrl_inst_sf_d_temp_0_25_626
    );
  lcd_cntrl_inst_sf_d_temp_0_28 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR1 => timer_inst_tenths_cnt(3),
      ADR2 => timer_inst_tenths_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_0_28_630
    );
  lcd_cntrl_inst_sf_d_temp_0_220 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd21_721,
      ADR1 => timer_inst_ones_cnt(3),
      ADR2 => timer_inst_ones_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_0_220_622
    );
  lcd_cntrl_inst_sf_d_temp_0_239 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd3_730,
      ADR1 => lcd_cntrl_inst_lap_tenths(3),
      ADR2 => lcd_cntrl_inst_lap_tenths(1),
      O => lcd_cntrl_inst_sf_d_temp_0_239_624
    );
  lcd_cntrl_inst_sf_d_temp_0_242 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd9_763,
      ADR1 => lcd_cntrl_inst_lap_tens(3),
      ADR2 => lcd_cntrl_inst_lap_tens(1),
      O => lcd_cntrl_inst_sf_d_temp_0_242_625
    );
  lcd_cntrl_inst_sf_d_temp_0_250 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd7_761,
      ADR1 => lcd_cntrl_inst_lap_ones(3),
      ADR2 => lcd_cntrl_inst_lap_ones(1),
      O => lcd_cntrl_inst_sf_d_temp_0_250_627
    );
  lcd_cntrl_inst_sf_d_temp_0_253 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd13_711,
      ADR1 => lcd_cntrl_inst_lap_min(3),
      ADR2 => lcd_cntrl_inst_lap_min(1),
      O => lcd_cntrl_inst_sf_d_temp_0_253_628
    );
  lcd_cntrl_inst_sf_d_temp_0_261 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_0_239_624,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_242_625,
      ADR2 => lcd_cntrl_inst_sf_d_temp_0_250_627,
      ADR3 => lcd_cntrl_inst_sf_d_temp_0_253_628,
      O => lcd_cntrl_inst_sf_d_temp_0_261_629
    );
  lcd_cntrl_inst_sf_d_temp_0_272 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_0_25_626,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_28_630,
      ADR2 => lcd_cntrl_inst_sf_d_temp_0_221_623,
      ADR3 => lcd_cntrl_inst_sf_d_temp_0_261_629,
      O => lcd_cntrl_inst_N71
    );
  lcd_cntrl_inst_sf_d_temp_2_5 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N12,
      ADR1 => lcd_cntrl_inst_N6,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd33_734,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd40_746,
      O => lcd_cntrl_inst_sf_d_temp_2_5_666
    );
  lcd_cntrl_inst_sf_d_temp_2_12 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(2),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR2 => timer_inst_hundredths_cnt(1),
      ADR3 => timer_inst_hundredths_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_2_12_662
    );
  lcd_cntrl_inst_sf_d_temp_2_22 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR1 => timer_inst_tenths_cnt(2),
      ADR2 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR3 => timer_inst_tens_cnt(2),
      O => lcd_cntrl_inst_sf_d_temp_2_22_663
    );
  lcd_cntrl_inst_sf_d_temp_2_45 : X_LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd16_715,
      ADR1 => lcd_cntrl_inst_lap_hundredths(2),
      ADR2 => lcd_cntrl_inst_state_FSM_FFd1_707,
      O => lcd_cntrl_inst_sf_d_temp_2_45_665
    );
  lcd_cntrl_inst_sf_d_temp_2_50 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_ones(2),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd7_761,
      ADR2 => lcd_cntrl_inst_lap_min(2),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd13_711,
      O => lcd_cntrl_inst_sf_d_temp_2_50_667
    );
  lcd_cntrl_inst_sf_d_temp_2_61 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_tenths(2),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd3_730,
      ADR2 => lcd_cntrl_inst_lap_tens(2),
      ADR3 => lcd_cntrl_inst_state_FSM_FFd9_763,
      O => lcd_cntrl_inst_sf_d_temp_2_61_668
    );
  lcd_cntrl_inst_sf_d_temp_2_68 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(2),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd27_727,
      ADR2 => timer_inst_mins_cnt(1),
      ADR3 => timer_inst_mins_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_2_68_669
    );
  lcd_cntrl_inst_sf_d_temp_2_70 : X_LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_2_68_669,
      ADR1 => timer_inst_ones_cnt(2),
      ADR2 => lcd_cntrl_inst_state_FSM_FFd21_721,
      O => lcd_cntrl_inst_sf_d_temp_2_70_670
    );
  lcd_cntrl_inst_sf_d_temp_2_85 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_2_45_665,
      ADR1 => lcd_cntrl_inst_sf_d_temp_2_50_667,
      ADR2 => lcd_cntrl_inst_sf_d_temp_2_61_668,
      ADR3 => lcd_cntrl_inst_sf_d_temp_2_70_670,
      O => lcd_cntrl_inst_sf_d_temp_2_85_671
    );
  lcd_cntrl_inst_sf_d_temp_4_13 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR1 => timer_inst_tens_cnt(3),
      ADR2 => timer_inst_tens_cnt(2),
      ADR3 => timer_inst_tens_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_4_13_688
    );
  lcd_cntrl_inst_sf_d_temp_4_27 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd27_727,
      ADR1 => timer_inst_mins_cnt(3),
      ADR2 => timer_inst_mins_cnt(2),
      ADR3 => timer_inst_mins_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_4_27_696
    );
  lcd_cntrl_inst_sf_d_temp_4_52 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR1 => timer_inst_hundredths_cnt(3),
      ADR2 => timer_inst_hundredths_cnt(2),
      ADR3 => timer_inst_hundredths_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_4_52_697
    );
  lcd_cntrl_inst_sf_d_temp_4_66 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR1 => timer_inst_tenths_cnt(3),
      ADR2 => timer_inst_tenths_cnt(2),
      ADR3 => timer_inst_tenths_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_4_66_698
    );
  lcd_cntrl_inst_sf_d_temp_4_80 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_4_13_688,
      ADR1 => lcd_cntrl_inst_sf_d_temp_4_27_696,
      ADR2 => lcd_cntrl_inst_sf_d_temp_4_52_697,
      ADR3 => lcd_cntrl_inst_sf_d_temp_4_66_698,
      O => lcd_cntrl_inst_sf_d_temp_4_80_699
    );
  lcd_cntrl_inst_sf_d_temp_4_106 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd9_763,
      ADR1 => lcd_cntrl_inst_lap_tens(3),
      ADR2 => lcd_cntrl_inst_lap_tens(2),
      ADR3 => lcd_cntrl_inst_lap_tens(1),
      O => lcd_cntrl_inst_sf_d_temp_4_106_686
    );
  lcd_cntrl_inst_sf_d_temp_4_120 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd13_711,
      ADR1 => lcd_cntrl_inst_lap_min(3),
      ADR2 => lcd_cntrl_inst_lap_min(2),
      ADR3 => lcd_cntrl_inst_lap_min(1),
      O => lcd_cntrl_inst_sf_d_temp_4_120_687
    );
  lcd_cntrl_inst_sf_d_temp_4_147 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N111,
      ADR1 => lcd_cntrl_inst_sf_d_temp_4_80_699,
      ADR2 => lcd_cntrl_inst_sf_d_temp_4_106_686,
      ADR3 => lcd_cntrl_inst_sf_d_temp_4_120_687,
      O => lcd_cntrl_inst_sf_d_temp_4_147_689
    );
  lcd_cntrl_inst_sf_d_temp_4_172 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd21_721,
      ADR1 => timer_inst_ones_cnt(3),
      ADR2 => timer_inst_ones_cnt(2),
      ADR3 => timer_inst_ones_cnt(1),
      O => lcd_cntrl_inst_sf_d_temp_4_172_690
    );
  lcd_cntrl_inst_sf_d_temp_4_181 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd34_737,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd39_744,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd29_729,
      ADR3 => lcd_cntrl_inst_sf_d_temp_4_172_690,
      O => lcd_cntrl_inst_sf_d_temp_4_181_691
    );
  lcd_cntrl_inst_sf_d_temp_4_200 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd1_707,
      ADR1 => lcd_cntrl_inst_lap_hundredths(3),
      ADR2 => lcd_cntrl_inst_lap_hundredths(2),
      ADR3 => lcd_cntrl_inst_lap_hundredths(1),
      O => lcd_cntrl_inst_sf_d_temp_4_200_692
    );
  lcd_cntrl_inst_sf_d_temp_4_228 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd3_730,
      ADR1 => lcd_cntrl_inst_lap_tenths(3),
      ADR2 => lcd_cntrl_inst_lap_tenths(2),
      ADR3 => lcd_cntrl_inst_lap_tenths(1),
      O => lcd_cntrl_inst_sf_d_temp_4_228_693
    );
  lcd_cntrl_inst_sf_d_temp_4_242 : X_LUT4
    generic map(
      INIT => X"222A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd7_761,
      ADR1 => lcd_cntrl_inst_lap_ones(3),
      ADR2 => lcd_cntrl_inst_lap_ones(2),
      ADR3 => lcd_cntrl_inst_lap_ones(1),
      O => lcd_cntrl_inst_sf_d_temp_4_242_694
    );
  lcd_cntrl_inst_sf_d_temp_4_256 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_4_181_691,
      ADR1 => lcd_cntrl_inst_sf_d_temp_4_200_692,
      ADR2 => lcd_cntrl_inst_sf_d_temp_4_228_693,
      ADR3 => lcd_cntrl_inst_sf_d_temp_4_242_694,
      O => lcd_cntrl_inst_sf_d_temp_4_256_695
    );
  lcd_cntrl_inst_sf_d_temp_0_2 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N112,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd38_743,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd41_747,
      O => lcd_cntrl_inst_sf_d_temp_0_2_620
    );
  lcd_cntrl_inst_sf_d_temp_0_8 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_hundredths(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd1_707,
      ADR2 => lcd_cntrl_inst_lap_hundredths(2),
      ADR3 => lcd_cntrl_inst_lap_hundredths(3),
      O => lcd_cntrl_inst_sf_d_temp_0_8_637
    );
  lcd_cntrl_inst_sf_d_temp_0_201 : X_LUT4
    generic map(
      INIT => X"FFC8"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(1),
      ADR1 => timer_inst_mins_cnt(3),
      ADR2 => timer_inst_mins_cnt(2),
      ADR3 => timer_inst_mins_cnt(0),
      O => lcd_cntrl_inst_sf_d_temp_0_201_621
    );
  lcd_cntrl_inst_sf_d_temp_0_32 : X_LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N16,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd27_727,
      ADR2 => lcd_cntrl_inst_sf_d_temp_0_201_621,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd30_731,
      O => lcd_cntrl_inst_sf_d_temp_0_32_631
    );
  lcd_cntrl_inst_sf_d_temp_0_41 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd21_721,
      ADR2 => timer_inst_ones_cnt(2),
      ADR3 => timer_inst_ones_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_0_41_632
    );
  lcd_cntrl_inst_sf_d_temp_0_47 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR2 => timer_inst_tens_cnt(2),
      ADR3 => timer_inst_tens_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_0_47_633
    );
  lcd_cntrl_inst_sf_d_temp_0_57 : X_LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(0),
      ADR1 => timer_inst_tenths_cnt(2),
      ADR2 => timer_inst_tenths_cnt(3),
      O => lcd_cntrl_inst_sf_d_temp_0_57_634
    );
  lcd_cntrl_inst_sf_d_temp_0_66 : X_LUT4
    generic map(
      INIT => X"FFC8"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(1),
      ADR1 => timer_inst_hundredths_cnt(3),
      ADR2 => timer_inst_hundredths_cnt(2),
      ADR3 => timer_inst_hundredths_cnt(0),
      O => lcd_cntrl_inst_sf_d_temp_0_66_635
    );
  lcd_cntrl_inst_sf_d_temp_0_73 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_57_634,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR3 => lcd_cntrl_inst_sf_d_temp_0_66_635,
      O => lcd_cntrl_inst_sf_d_temp_0_73_636
    );
  lcd_cntrl_inst_sf_d_temp_0_80 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_0_41_632,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_47_633,
      ADR2 => lcd_cntrl_inst_sf_d_temp_0_73_636,
      O => lcd_cntrl_inst_sf_d_temp_0_80_638
    );
  lcd_cntrl_inst_sf_d_temp_0_103 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_0_2_620,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_8_637,
      ADR2 => lcd_cntrl_inst_sf_d_temp_0_32_631,
      ADR3 => lcd_cntrl_inst_sf_d_temp_0_80_638,
      O => lcd_cntrl_inst_sf_d_temp_0_103_613
    );
  lcd_cntrl_inst_sf_d_temp_0_117 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_tenths(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd3_730,
      ADR2 => lcd_cntrl_inst_lap_tenths(2),
      ADR3 => lcd_cntrl_inst_lap_tenths(3),
      O => lcd_cntrl_inst_sf_d_temp_0_117_614
    );
  lcd_cntrl_inst_sf_d_temp_0_123 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_ones(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd7_761,
      ADR2 => lcd_cntrl_inst_lap_ones(2),
      ADR3 => lcd_cntrl_inst_lap_ones(3),
      O => lcd_cntrl_inst_sf_d_temp_0_123_615
    );
  lcd_cntrl_inst_sf_d_temp_0_136 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_tens(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd9_763,
      ADR2 => lcd_cntrl_inst_lap_tens(2),
      ADR3 => lcd_cntrl_inst_lap_tens(3),
      O => lcd_cntrl_inst_sf_d_temp_0_136_616
    );
  lcd_cntrl_inst_sf_d_temp_0_142 : X_LUT4
    generic map(
      INIT => X"C888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_lap_min(0),
      ADR1 => lcd_cntrl_inst_state_FSM_FFd13_711,
      ADR2 => lcd_cntrl_inst_lap_min(2),
      ADR3 => lcd_cntrl_inst_lap_min(3),
      O => lcd_cntrl_inst_sf_d_temp_0_142_617
    );
  lcd_cntrl_inst_sf_d_temp_0_152 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_0_117_614,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_123_615,
      ADR2 => lcd_cntrl_inst_sf_d_temp_0_136_616,
      ADR3 => lcd_cntrl_inst_sf_d_temp_0_142_617,
      O => lcd_cntrl_inst_sf_d_temp_0_152_618
    );
  lcd_cntrl_inst_count_temp_or000021 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd45_755,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd46_756,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd44_754,
      O => lcd_cntrl_inst_N6
    );
  lcd_cntrl_inst_state_FSM_FFd46_In1 : X_LUT4
    generic map(
      INIT => X"88F8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd47_758,
      ADR1 => lcd_cntrl_inst_state_cmp_ge0000,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd46_756,
      ADR3 => lcd_cntrl_inst_state_cmp_eq0000,
      O => lcd_cntrl_inst_state_FSM_FFd46_In
    );
  lcd_cntrl_inst_state_FSM_FFd43_In1 : X_LUT4
    generic map(
      INIT => X"88F8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd44_754,
      ADR1 => lcd_cntrl_inst_state_cmp_eq0000,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd43_752,
      ADR3 => lcd_cntrl_inst_state_cmp_eq0001,
      O => lcd_cntrl_inst_state_FSM_FFd43_In
    );
  lcd_cntrl_inst_state_FSM_FFd33_In10 : X_LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR1 => mode_control,
      ADR2 => lcd_cntrl_inst_timer_flag_785,
      O => lcd_cntrl_inst_state_FSM_FFd33_In10_735
    );
  lcd_cntrl_inst_lap_flag_not00011 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_cmp_eq0001,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd1_707,
      O => lcd_cntrl_inst_lap_flag_not0001
    );
  Mcount_address_xor_3_111 : X_LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(1),
      ADR2 => address(0),
      O => N2
    );
  Mcount_address_xor_4_11 : X_LUT3
    generic map(
      INIT => X"9A"
    )
    port map (
      ADR0 => address(4),
      ADR1 => N2,
      ADR2 => address(3),
      O => Mcount_address4
    );
  Mcount_address_xor_5_11 : X_LUT4
    generic map(
      INIT => X"9AAA"
    )
    port map (
      ADR0 => address(5),
      ADR1 => N2,
      ADR2 => address(3),
      ADR3 => address(4),
      O => Mcount_address5
    );
  timer_state_sreg_FSM_FFd12_In_SW0 : X_LUT3
    generic map(
      INIT => X"FB"
    )
    port map (
      ADR0 => strtstop_debounced,
      ADR1 => timer_state_sreg_FSM_FFd4_963,
      ADR2 => ll_debounced,
      O => N15
    );
  timer_state_sreg_FSM_FFd12_In : X_LUT4
    generic map(
      INIT => X"FF8A"
    )
    port map (
      ADR0 => mode_debounced,
      ADR1 => timer_state_sreg_FSM_FFd12_957,
      ADR2 => N15,
      ADR3 => timer_state_rst_947,
      O => timer_state_sreg_FSM_FFd12_In_958
    );
  timer_state_sreg_FSM_FFd4_In10 : X_LUT4
    generic map(
      INIT => X"22F2"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd2_959,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd11_955,
      ADR3 => mode_debounced,
      O => timer_state_sreg_FSM_FFd4_In10_965
    );
  timer_state_sreg_FSM_FFd4_In21 : X_LUT4
    generic map(
      INIT => X"FF02"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd4_963,
      ADR1 => strtstop_debounced,
      ADR2 => mode_debounced,
      ADR3 => timer_state_sreg_FSM_FFd6_969,
      O => timer_state_sreg_FSM_FFd4_In21_966
    );
  timer_state_sreg_FSM_FFd4_In31 : X_LUT3
    generic map(
      INIT => X"AE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd4_In10_965,
      ADR1 => timer_state_sreg_FSM_FFd4_In21_966,
      ADR2 => ll_debounced,
      O => timer_state_sreg_FSM_FFd4_In
    );
  timer_state_next_clken0 : X_LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd3_961,
      ADR1 => timer_state_sreg_FSM_FFd10_953,
      O => timer_state_next_clken0_945
    );
  lcd_cntrl_inst_sf_d_temp_4_124 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N6,
      ADR1 => lcd_cntrl_inst_N13,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd43_752,
      O => lcd_cntrl_inst_N111
    );
  lcd_cntrl_inst_timer_flag_mux00002 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N4,
      ADR1 => lcd_cntrl_inst_N6,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd47_758,
      O => lcd_cntrl_inst_clock_flag_mux00002
    );
  lcd_cntrl_inst_count_temp_or0000_SW0 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd47_758,
      ADR1 => lcd_cntrl_inst_state_cmp_ge0000,
      ADR2 => lcd_cntrl_inst_N6,
      ADR3 => lcd_cntrl_inst_state_cmp_eq0000,
      O => N17
    );
  lcd_cntrl_inst_count_temp_or0000 : X_LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      ADR0 => timer_state_rst_947,
      ADR1 => N155,
      ADR2 => lcd_cntrl_inst_state_cmp_eq0001,
      ADR3 => N17,
      O => lcd_cntrl_inst_count_temp_or0000_579
    );
  lcd_cntrl_inst_clock_flag_mux00008 : X_LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR1 => lcd_cntrl_inst_state_cmp_eq0001,
      ADR2 => lcd_cntrl_inst_lap_flag_580,
      O => lcd_cntrl_inst_clock_flag_mux00008_510
    );
  lcd_cntrl_inst_clock_flag_mux000026 : X_LUT4
    generic map(
      INIT => X"AF8F"
    )
    port map (
      ADR0 => lcd_cntrl_inst_clock_flag_503,
      ADR1 => lcd_cntrl_inst_clock_flag_mux00008_510,
      ADR2 => lcd_cntrl_inst_mode_state_603,
      ADR3 => lcd_cntrl_inst_clock_flag_mux00002,
      O => lcd_cntrl_inst_clock_flag_mux0000
    );
  lcd_cntrl_inst_control_2_1 : X_LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N7,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      O => lcd_rs_OBUF_792
    );
  lcd_cntrl_inst_state_cmp_eq000110 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(14),
      ADR1 => lcd_cntrl_inst_count(12),
      ADR2 => lcd_cntrl_inst_count(6),
      ADR3 => lcd_cntrl_inst_count(7),
      O => lcd_cntrl_inst_state_cmp_eq000110_781
    );
  lcd_cntrl_inst_state_cmp_eq000113 : X_LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(10),
      ADR1 => lcd_cntrl_inst_count(11),
      O => lcd_cntrl_inst_state_cmp_eq000113_782
    );
  lcd_cntrl_inst_state_cmp_eq000124 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(18),
      ADR1 => lcd_cntrl_inst_count(16),
      ADR2 => lcd_cntrl_inst_count(4),
      ADR3 => lcd_cntrl_inst_count(5),
      O => lcd_cntrl_inst_state_cmp_eq000124_783
    );
  lcd_cntrl_inst_state_cmp_eq000149 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(5),
      ADR1 => lcd_cntrl_inst_state_cmp_eq000110_781,
      ADR2 => lcd_cntrl_inst_state_cmp_eq000113_782,
      ADR3 => lcd_cntrl_inst_state_cmp_eq000124_783,
      O => lcd_cntrl_inst_state_cmp_eq0001
    );
  lcd_cntrl_inst_state_cmp_eq000010 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(12),
      ADR1 => lcd_cntrl_inst_count(14),
      ADR2 => lcd_cntrl_inst_count(7),
      ADR3 => lcd_cntrl_inst_count(6),
      O => lcd_cntrl_inst_state_cmp_eq000010_765
    );
  lcd_cntrl_inst_state_cmp_eq000013 : X_LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(11),
      ADR1 => lcd_cntrl_inst_count(10),
      O => lcd_cntrl_inst_state_cmp_eq000013_766
    );
  lcd_cntrl_inst_state_cmp_eq000024 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(16),
      ADR1 => lcd_cntrl_inst_count(18),
      ADR2 => lcd_cntrl_inst_count(5),
      ADR3 => lcd_cntrl_inst_count(4),
      O => lcd_cntrl_inst_state_cmp_eq000024_779
    );
  lcd_cntrl_inst_state_cmp_eq000049 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_cmp_eq00001_wg_cy(5),
      ADR1 => lcd_cntrl_inst_state_cmp_eq000010_765,
      ADR2 => lcd_cntrl_inst_state_cmp_eq000013_766,
      ADR3 => lcd_cntrl_inst_state_cmp_eq000024_779,
      O => lcd_cntrl_inst_state_cmp_eq0000
    );
  timer_inst_tc_5_cmp_eq00001 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(1),
      ADR1 => timer_inst_mins_cnt(3),
      ADR2 => timer_inst_mins_cnt(0),
      ADR3 => timer_inst_mins_cnt(2),
      O => timer_inst_tc_5_cmp_eq0000
    );
  lcd_cntrl_inst_sf_d_temp_0_31 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd22_722,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd18_717,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd8_762,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd4_745,
      O => lcd_cntrl_inst_N112
    );
  lcd_cntrl_inst_sf_d_temp_1_1 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd5_759,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd11_709,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd28_728,
      ADR3 => N19,
      O => lcd_cntrl_inst_N13
    );
  lcd_cntrl_inst_clock_flag_mux0000111 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N9,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd31_732,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd37_742,
      ADR3 => N21,
      O => lcd_cntrl_inst_N7
    );
  lcd_cntrl_inst_sf_d_temp_3_3_SW0 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd16_715,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd20_720,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd24_724,
      ADR3 => lcd_cntrl_inst_N112,
      O => N23
    );
  lcd_cntrl_inst_sf_d_temp_3_3 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd2_719,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd6_760,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd10_708,
      ADR3 => N23,
      O => lcd_cntrl_inst_N8
    );
  lcd_cntrl_inst_clock_flag_mux0000114 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd43_752,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd42_751,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd40_746,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd32_733,
      O => lcd_cntrl_inst_clock_flag_mux0000114_507
    );
  lcd_cntrl_inst_clock_flag_mux0000119 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd33_734,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd26_726,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd14_712,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd12_710,
      O => lcd_cntrl_inst_clock_flag_mux0000119_508
    );
  lcd_cntrl_inst_clock_flag_mux00001127 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N7,
      ADR1 => lcd_cntrl_inst_N8,
      ADR2 => lcd_cntrl_inst_clock_flag_mux0000114_507,
      ADR3 => lcd_cntrl_inst_clock_flag_mux0000119_508,
      O => lcd_cntrl_inst_N4
    );
  lcd_cntrl_inst_clock_flag_mux0000111112 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd17_716,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd13_711,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd9_763,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd7_761,
      O => lcd_cntrl_inst_clock_flag_mux0000111112_505
    );
  lcd_cntrl_inst_clock_flag_mux0000111117 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd3_730,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd30_731,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd29_729,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd34_737,
      O => lcd_cntrl_inst_clock_flag_mux0000111117_506
    );
  lcd_cntrl_inst_control_0_26 : X_LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      ADR0 => lcd_cntrl_inst_control_0_12_511,
      ADR1 => lcd_cntrl_inst_control_0_25_512,
      O => lcd_e_OBUF_790
    );
  timer_inst_tc_4_cmp_eq00011 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(3),
      ADR1 => timer_inst_tens_cnt(2),
      ADR2 => timer_inst_tens_cnt(0),
      ADR3 => timer_inst_tens_cnt(1),
      O => timer_inst_tc_4_cmp_eq0001
    );
  timer_inst_hundredths_cnt_mux0003_1_0 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_15_Q,
      O => timer_inst_hundredths_cnt_mux0003_1_0_862
    );
  timer_inst_hundredths_cnt_mux0003_1_51 : X_LUT4
    generic map(
      INIT => X"FF60"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(0),
      ADR1 => timer_inst_Maddsub_hundredths_cnt_share0000_lut(1),
      ADR2 => N158,
      ADR3 => timer_inst_hundredths_cnt_mux0003_1_0_862,
      O => timer_inst_hundredths_cnt_mux0003(1)
    );
  timer_inst_tc_1_or00001 : X_LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      ADR0 => timer_inst_tc_1_and0001,
      ADR1 => N169,
      O => timer_inst_tc_1
    );
  timer_inst_Maddsub_hundredths_cnt_share0000_lut_2_1 : X_LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(2),
      ADR1 => mode_control,
      O => timer_inst_Maddsub_hundredths_cnt_share0000_lut(2)
    );
  timer_inst_hundredths_cnt_mux0003_2_0 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_15_Q,
      O => timer_inst_hundredths_cnt_mux0003_2_0_865
    );
  timer_inst_hundredths_cnt_mux0003_2_51 : X_LUT4
    generic map(
      INIT => X"FF60"
    )
    port map (
      ADR0 => timer_inst_Maddsub_hundredths_cnt_share0000_cy(1),
      ADR1 => timer_inst_Maddsub_hundredths_cnt_share0000_lut(2),
      ADR2 => timer_inst_hundredths_cnt_mux0003_1_31_863,
      ADR3 => timer_inst_hundredths_cnt_mux0003_2_0_865,
      O => timer_inst_hundredths_cnt_mux0003(2)
    );
  timer_inst_Maddsub_hundredths_cnt_share0000_lut_1_1 : X_LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(1),
      ADR1 => mode_control,
      O => timer_inst_Maddsub_hundredths_cnt_share0000_lut(1)
    );
  timer_inst_tc_1_cmp_eq00011 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(2),
      ADR1 => timer_inst_hundredths_cnt(0),
      ADR2 => timer_inst_hundredths_cnt(3),
      ADR3 => timer_inst_hundredths_cnt(1),
      O => timer_inst_tc_1_cmp_eq0001
    );
  timer_inst_tenths_cnt_mux0002_6_17 : X_LUT3
    generic map(
      INIT => X"60"
    )
    port map (
      ADR0 => timer_inst_Maddsub_tenths_cnt_share0000_cy(1),
      ADR1 => timer_inst_Maddsub_tenths_cnt_share0000_lut(2),
      ADR2 => N156,
      O => timer_inst_tenths_cnt_mux0002_6_17_938
    );
  timer_inst_hundredths_cnt_mux0003_3_32 : X_LUT4
    generic map(
      INIT => X"2882"
    )
    port map (
      ADR0 => timer_inst_N14,
      ADR1 => timer_inst_hundredths_cnt(3),
      ADR2 => mode_control,
      ADR3 => timer_inst_Maddsub_hundredths_cnt_share0000_cy(2),
      O => timer_inst_hundredths_cnt_mux0003_3_32_867
    );
  timer_inst_tc_2_cmp_eq00001 : X_LUT4
    generic map(
      INIT => X"0200"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(0),
      ADR1 => timer_inst_tenths_cnt(2),
      ADR2 => timer_inst_tenths_cnt(1),
      ADR3 => timer_inst_tenths_cnt(3),
      O => timer_inst_tc_2_cmp_eq0000
    );
  timer_inst_tc_1_cmp_eq00001 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(2),
      ADR1 => timer_inst_hundredths_cnt(0),
      ADR2 => timer_inst_hundredths_cnt(3),
      ADR3 => timer_inst_hundredths_cnt(1),
      O => timer_inst_tc_1_cmp_eq0000
    );
  timer_state_sreg_FSM_Out7 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd9_975,
      ADR1 => timer_state_sreg_FSM_FFd12_957,
      ADR2 => timer_state_sreg_FSM_FFd10_953,
      ADR3 => N162,
      O => mode_control
    );
  lap_load_IBUF : X_BUF
    port map (
      I => lap_load,
      O => lap_load_IBUF_291
    );
  strtstop_IBUF : X_BUF
    port map (
      I => strtstop,
      O => strtstop_IBUF_834
    );
  reset_IBUF : X_BUF
    port map (
      I => reset,
      O => reset_IBUF_824
    );
  mode_IBUF : X_BUF
    port map (
      I => mode,
      O => mode_IBUF_797
    );
  lcd_cntrl_inst_sf_d_7 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_7_1,
      SSET => lcd_cntrl_inst_N8,
      O => lcd_cntrl_inst_sf_d(7),
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_sf_d_6 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_6_18,
      SSET => lcd_cntrl_inst_N12,
      O => lcd_cntrl_inst_sf_d(6),
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_sf_d_5 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_5_1_700,
      SSET => lcd_cntrl_inst_N111,
      O => lcd_cntrl_inst_sf_d(5),
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_sf_d_4 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_4_256_695,
      SSET => lcd_cntrl_inst_sf_d_temp_4_147_689,
      O => lcd_cntrl_inst_sf_d(4),
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_sf_d_2 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_2_107,
      SSET => lcd_cntrl_inst_N71,
      O => lcd_cntrl_inst_sf_d(2),
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_sf_d_temp_2_1071 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_2_5_666,
      ADR1 => lcd_cntrl_inst_sf_d_temp_2_23_664,
      ADR2 => lcd_cntrl_inst_sf_d_temp_2_85_671,
      O => lcd_cntrl_inst_sf_d_temp_2_107
    );
  lcd_cntrl_inst_sf_d_0 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      I => lcd_cntrl_inst_sf_d_temp_0_173,
      SSET => lcd_cntrl_inst_N71,
      O => lcd_cntrl_inst_sf_d(0),
      CE => VCC,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_sf_d_temp_0_1731 : X_LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      ADR0 => lcd_cntrl_inst_sf_d_temp_0_103_613,
      ADR1 => lcd_cntrl_inst_sf_d_temp_0_152_618,
      O => lcd_cntrl_inst_sf_d_temp_0_173
    );
  lcd_cntrl_inst_state_FSM_FFd47 : X_SFF
    generic map(
      INIT => '1'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_ge0000,
      I => N0,
      SSET => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd47_758,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  lcd_cntrl_inst_state_FSM_FFd44 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0000,
      I => lcd_cntrl_inst_state_FSM_FFd45_755,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd44_754,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd45 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0000,
      I => lcd_cntrl_inst_state_FSM_FFd46_756,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd45_755,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd41 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd41_In22,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd41_747,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd40 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd41_747,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd40_746,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd42 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd43_752,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd42_751,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd38 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd39_744,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd38_743,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd37 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd38_743,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd37_742,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd39 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd40_746,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd39_744,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd35 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd35_In1,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd35_739,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd35_In11 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => mode_control,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd36_741,
      O => lcd_cntrl_inst_state_FSM_FFd35_In1
    );
  lcd_cntrl_inst_state_FSM_FFd34 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd34_In1,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd34_737,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd34_In11 : X_LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd36_741,
      ADR1 => mode_control,
      O => lcd_cntrl_inst_state_FSM_FFd34_In1
    );
  lcd_cntrl_inst_state_FSM_FFd36 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd37_742,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd36_741,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd32 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd35_739,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd32_733,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd31 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd32_733,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd31_732,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd33 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd33_In33,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd33_734,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd29 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd30_731,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd29_729,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd28 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd29_729,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd28_728,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd30 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd31_732,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd30_731,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd26 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd27_727,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd26_726,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd25 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd26_726,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd25_725,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd27 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd33_734,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd27_727,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd23 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd24_724,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd23_723,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd22 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd23_723,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd22_722,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd24 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd25_725,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd24_724,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd20 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd21_721,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd20_720,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd19 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd20_720,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd19_718,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd21 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd22_722,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd21_721,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd17 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd18_717,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd17_716,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd16 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd17_716,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd16_715,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd18 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd19_718,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd18_717,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd14 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd14_In1_713,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd14_712,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd13 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd14_712,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd13_711,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd15 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd16_715,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd15_714,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd11 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd12_710,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd11_709,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd10 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd11_709,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd10_708,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd12 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd13_711,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd12_710,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd8 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd9_763,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd8_762,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd7 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd8_762,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd7_761,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd9 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd10_708,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd9_763,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd5 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd6_760,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd5_759,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd4 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd5_759,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd4_745,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd6 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd7_761,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd6_760,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd2 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd3_730,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd2_719,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd1 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd2_719,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd1_707,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  lcd_cntrl_inst_state_FSM_FFd3 : X_SFF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_26214k,
      CE => lcd_cntrl_inst_state_cmp_eq0001,
      I => lcd_cntrl_inst_state_FSM_FFd4_745,
      SRST => timer_state_rst_947,
      O => lcd_cntrl_inst_state_FSM_FFd3_730,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_cy_0_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(17),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_cy_0_rt_180,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_1_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(1),
      O => clk_divider_Mcount_cnt_cy_1_rt_212,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_2_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(2),
      O => clk_divider_Mcount_cnt_cy_2_rt_234,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_3_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(3),
      O => clk_divider_Mcount_cnt_cy_3_rt_238,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_4_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(4),
      O => clk_divider_Mcount_cnt_cy_4_rt_240,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_5_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(5),
      O => clk_divider_Mcount_cnt_cy_5_rt_242,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_6_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(6),
      O => clk_divider_Mcount_cnt_cy_6_rt_244,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_7_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(7),
      O => clk_divider_Mcount_cnt_cy_7_rt_246,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_8_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(8),
      O => clk_divider_Mcount_cnt_cy_8_rt_248,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_9_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(9),
      O => clk_divider_Mcount_cnt_cy_9_rt_250,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_10_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(10),
      O => clk_divider_Mcount_cnt_cy_10_rt_192,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_11_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(11),
      O => clk_divider_Mcount_cnt_cy_11_rt_194,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_12_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(12),
      O => clk_divider_Mcount_cnt_cy_12_rt_196,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_13_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(13),
      O => clk_divider_Mcount_cnt_cy_13_rt_198,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_14_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(14),
      O => clk_divider_Mcount_cnt_cy_14_rt_200,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_15_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(15),
      O => clk_divider_Mcount_cnt_cy_15_rt_202,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_16_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(16),
      O => clk_divider_Mcount_cnt_cy_16_rt_204,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_17_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(17),
      O => clk_divider_Mcount_cnt_cy_17_rt_206,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_18_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(18),
      O => clk_divider_Mcount_cnt_cy_18_rt_208,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_19_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(19),
      O => clk_divider_Mcount_cnt_cy_19_rt_210,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_20_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(20),
      O => clk_divider_Mcount_cnt_cy_20_rt_214,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_21_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(21),
      O => clk_divider_Mcount_cnt_cy_21_rt_216,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_22_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(22),
      O => clk_divider_Mcount_cnt_cy_22_rt_218,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_23_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(23),
      O => clk_divider_Mcount_cnt_cy_23_rt_220,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_24_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(24),
      O => clk_divider_Mcount_cnt_cy_24_rt_222,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_25_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(25),
      O => clk_divider_Mcount_cnt_cy_25_rt_224,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_26_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(26),
      O => clk_divider_Mcount_cnt_cy_26_rt_226,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_27_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(27),
      O => clk_divider_Mcount_cnt_cy_27_rt_228,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_28_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(28),
      O => clk_divider_Mcount_cnt_cy_28_rt_230,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_29_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(29),
      O => clk_divider_Mcount_cnt_cy_29_rt_232,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_cy_30_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(30),
      O => clk_divider_Mcount_cnt_cy_30_rt_236,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_30_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(30),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_30_rt_442,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_29_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(29),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_29_rt_438,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_28_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(28),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_28_rt_436,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_27_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(27),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_27_rt_434,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_26_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(26),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_26_rt_432,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_25_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(25),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_25_rt_430,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_24_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(24),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_24_rt_428,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_23_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(23),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_23_rt_426,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_22_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(22),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_22_rt_424,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_21_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(21),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_21_rt_422,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_20_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(20),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_20_rt_420,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_19_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(19),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_19_rt_416,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_18_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(18),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_18_rt_414,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_17_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(17),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_17_rt_412,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_16_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(16),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_16_rt_410,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_15_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(15),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_15_rt_408,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_14_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(14),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_14_rt_406,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_13_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(13),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_13_rt_404,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_12_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(12),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_12_rt_402,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_11_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(11),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_11_rt_400,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_10_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(10),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_10_rt_398,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_9_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(9),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_9_rt_456,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_8_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(8),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_8_rt_454,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_7_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(7),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_7_rt_452,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_6_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(6),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_6_rt_450,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_5_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(5),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_5_rt_448,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_4_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(4),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_4_rt_446,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_3_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(3),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_3_rt_444,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_2_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(2),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_2_rt_440,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_cy_1_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(1),
      O => lcd_cntrl_inst_Mcount_count_temp_cy_1_rt_418,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_8_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(19),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_cy_8_rt_382,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(11),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_6_1_rt_329,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(7),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_4_1_rt_321,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(5),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_2_1_rt_313,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_0_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(10),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_5_0_rt_325,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_0_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(6),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_3_0_rt_317,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_0_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count(4),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy_1_0_rt_309,
      ADR1 => GND
    );
  clk_divider_Mcount_cnt_xor_31_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => clk_divider_cnt(31),
      O => clk_divider_Mcount_cnt_xor_31_rt_252,
      ADR1 => GND
    );
  lcd_cntrl_inst_Mcount_count_temp_xor_31_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_count_temp(31),
      O => lcd_cntrl_inst_Mcount_count_temp_xor_31_rt_458,
      ADR1 => GND
    );
  timer_inst_ones_cnt_mux0002_11_70 : X_LUT4
    generic map(
      INIT => X"F5E4"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => timer_inst_ones_cnt_mux0002_11_37_894,
      ADR2 => preset_time_11_Q,
      ADR3 => timer_inst_ones_cnt_mux0002_11_4_895,
      O => timer_inst_ones_cnt_mux0002(11)
    );
  timer_inst_mins_cnt_mux0002_19_70 : X_LUT4
    generic map(
      INIT => X"DDD8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_19_Q,
      ADR2 => timer_inst_mins_cnt_mux0002_19_4_883,
      ADR3 => timer_inst_mins_cnt_mux0002_19_37_882,
      O => timer_inst_mins_cnt_mux0002(19)
    );
  timer_inst_tenths_cnt_mux0002_6_45 : X_LUT4
    generic map(
      INIT => X"F5E4"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => timer_inst_tenths_cnt_mux0002_6_20_939,
      ADR2 => preset_time_6_Q,
      ADR3 => timer_inst_tenths_cnt_mux0002_6_17_938,
      O => timer_inst_tenths_cnt_mux0002(6)
    );
  timer_inst_tens_cnt_mux0002_13_41 : X_LUT4
    generic map(
      INIT => X"F5E4"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => timer_inst_tens_cnt_mux0002_13_18_923,
      ADR2 => preset_time_13_Q,
      ADR3 => timer_inst_tens_cnt_mux0002_13_1_922,
      O => timer_inst_tens_cnt_mux0002(13)
    );
  timer_inst_mins_cnt_mux0002_16_44 : X_LUT4
    generic map(
      INIT => X"DDD8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_16_Q,
      ADR2 => timer_inst_mins_cnt_mux0002_16_4_876,
      ADR3 => timer_inst_mins_cnt_mux0002_16_19_875,
      O => timer_inst_mins_cnt_mux0002(16)
    );
  timer_inst_tenths_cnt_mux0002_7_67 : X_LUT4
    generic map(
      INIT => X"DDD8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_0_Q,
      ADR2 => timer_inst_tenths_cnt_mux0002_7_8_942,
      ADR3 => timer_inst_tenths_cnt_mux0002_7_34_941,
      O => timer_inst_tenths_cnt_mux0002(7)
    );
  timer_inst_hundredths_cnt_mux0003_3_59 : X_LUT4
    generic map(
      INIT => X"DDD8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_0_Q,
      ADR2 => timer_inst_tc_1_and0001,
      ADR3 => timer_inst_hundredths_cnt_mux0003_3_32_867,
      O => timer_inst_hundredths_cnt_mux0003(3)
    );
  timer_inst_mins_cnt_mux0002_19_37 : X_LUT4
    generic map(
      INIT => X"0CD0"
    )
    port map (
      ADR0 => timer_inst_tc_5_cmp_eq0000,
      ADR1 => timer_inst_mins_cnt_and0001,
      ADR2 => N167,
      ADR3 => N30,
      O => timer_inst_mins_cnt_mux0002_19_37_882
    );
  timer_inst_mins_cnt_and00011 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => timer_inst_tc_3_cmp_eq0001,
      ADR1 => timer_inst_tc_4_cmp_eq0001,
      ADR2 => timer_inst_tc_2_cmp_eq0001,
      ADR3 => timer_inst_tc_1_and0001,
      O => timer_inst_mins_cnt_and0001
    );
  timer_inst_mins_cnt_mux0002_16_4 : X_LUT4
    generic map(
      INIT => X"C0CA"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(0),
      ADR1 => N154,
      ADR2 => timer_inst_mins_cnt_and0001,
      ADR3 => timer_inst_mins_cnt_and0000,
      O => timer_inst_mins_cnt_mux0002_16_4_876
    );
  timer_inst_mins_cnt_mux0002_19_4 : X_LUT4
    generic map(
      INIT => X"C0CA"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(3),
      ADR1 => timer_inst_tc_5_cmp_eq0001,
      ADR2 => timer_inst_mins_cnt_and0001,
      ADR3 => timer_inst_mins_cnt_and0000,
      O => timer_inst_mins_cnt_mux0002_19_4_883
    );
  timer_inst_tens_cnt_mux0002_13_18 : X_LUT4
    generic map(
      INIT => X"2733"
    )
    port map (
      ADR0 => timer_inst_tc_4_cmp_eq0001,
      ADR1 => N42,
      ADR2 => N43,
      ADR3 => N164,
      O => timer_inst_tens_cnt_mux0002_13_18_923
    );
  timer_inst_mins_cnt_mux0002_18_1 : X_LUT4
    generic map(
      INIT => X"020A"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(2),
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => timer_inst_mins_cnt_and0001,
      ADR3 => timer_inst_tens_cnt_and0000,
      O => timer_inst_mins_cnt_mux0002_18_1_880
    );
  timer_inst_mins_cnt_mux0002_17_1 : X_LUT4
    generic map(
      INIT => X"020A"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(1),
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => timer_inst_mins_cnt_and0001,
      ADR3 => timer_inst_tens_cnt_and0000,
      O => timer_inst_mins_cnt_mux0002_17_1_878
    );
  timer_inst_tens_cnt_mux0002_14_63 : X_LUT4
    generic map(
      INIT => X"D8DD"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_14_Q,
      ADR2 => timer_inst_tens_cnt_mux0002_14_5_926,
      ADR3 => N47,
      O => timer_inst_tens_cnt_mux0002(14)
    );
  timer_inst_mins_cnt_and00002_SW0 : X_LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      ADR0 => timer_inst_tc_5_cmp_eq0000,
      ADR1 => N160,
      O => N49
    );
  timer_inst_mins_cnt_mux0002_17_11 : X_LUT4
    generic map(
      INIT => X"7350"
    )
    port map (
      ADR0 => timer_inst_tc_5_cmp_eq0001,
      ADR1 => N49,
      ADR2 => timer_inst_mins_cnt_and0001,
      ADR3 => N163,
      O => timer_inst_N4
    );
  timer_inst_mins_cnt_mux0002_19_37_SW0_SW0 : X_LUT4
    generic map(
      INIT => X"AAA9"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(3),
      ADR1 => timer_inst_mins_cnt(2),
      ADR2 => timer_inst_mins_cnt(1),
      ADR3 => timer_inst_mins_cnt(0),
      O => N51
    );
  timer_inst_mins_cnt_mux0002_19_37_SW0_SW1 : X_LUT4
    generic map(
      INIT => X"9555"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(3),
      ADR1 => timer_inst_mins_cnt(2),
      ADR2 => timer_inst_mins_cnt(1),
      ADR3 => timer_inst_mins_cnt(0),
      O => N52
    );
  timer_inst_hundredths_cnt_mux0003_3_211_SW0 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd12_957,
      ADR1 => timer_state_sreg_FSM_FFd10_953,
      ADR2 => timer_state_sreg_FSM_FFd9_975,
      O => N60
    );
  timer_inst_hundredths_cnt_mux0003_3_211 : X_LUT4
    generic map(
      INIT => X"0100"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd7_971,
      ADR1 => timer_state_sreg_FSM_FFd8_973,
      ADR2 => N60,
      ADR3 => timer_inst_tc_1_cmp_eq0001,
      O => timer_inst_tc_1_and0001
    );
  timer_inst_tens_cnt_mux0002_15_62_SW0 : X_LUT4
    generic map(
      INIT => X"6900"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(3),
      ADR1 => timer_inst_Maddsub_tens_cnt_share0000_cy(2),
      ADR2 => timer_inst_tens_cnt_and0000,
      ADR3 => timer_inst_N3,
      O => N62
    );
  timer_inst_tens_cnt_mux0002_15_62 : X_LUT4
    generic map(
      INIT => X"F5E4"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => timer_inst_tens_cnt_mux0002_15_1_928,
      ADR2 => preset_time_15_Q,
      ADR3 => N62,
      O => timer_inst_tens_cnt_mux0002(15)
    );
  timer_inst_mins_cnt_mux0002_18_40 : X_LUT4
    generic map(
      INIT => X"E2F3"
    )
    port map (
      ADR0 => timer_inst_mins_cnt_mux0002_18_1_880,
      ADR1 => timer_state_sreg_FSM_FFd5_967,
      ADR2 => preset_time_18_Q,
      ADR3 => N67,
      O => timer_inst_mins_cnt_mux0002(18)
    );
  timer_inst_mins_cnt_mux0002_17_40 : X_LUT4
    generic map(
      INIT => X"E2F3"
    )
    port map (
      ADR0 => timer_inst_mins_cnt_mux0002_17_1_878,
      ADR1 => timer_state_sreg_FSM_FFd5_967,
      ADR2 => preset_time_17_Q,
      ADR3 => N69,
      O => timer_inst_mins_cnt_mux0002(17)
    );
  timer_inst_tens_cnt_mux0002_14_38_SW0 : X_LUT4
    generic map(
      INIT => X"B77B"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(2),
      ADR1 => timer_inst_tens_cnt_mux0002_14_11_925,
      ADR2 => timer_inst_Maddsub_tens_cnt_share0000_cy(1),
      ADR3 => timer_inst_tens_cnt_and0000,
      O => N47
    );
  timer_inst_ones_cnt_mux0002_8_44 : X_LUT4
    generic map(
      INIT => X"DDD8"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => preset_time_8_Q,
      ADR2 => timer_inst_ones_cnt_mux0002_8_5_898,
      ADR3 => timer_inst_ones_cnt_mux0002_8_19_897,
      O => timer_inst_ones_cnt_mux0002(8)
    );
  timer_inst_tenths_cnt_mux0002_5_45 : X_LUT4
    generic map(
      INIT => X"F5E4"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => timer_inst_tenths_cnt_mux0002_5_20_936,
      ADR2 => preset_time_15_Q,
      ADR3 => timer_inst_tenths_cnt_mux0002_5_17_935,
      O => timer_inst_tenths_cnt_mux0002(5)
    );
  timer_inst_tenths_cnt_mux0002_7_34_SW0 : X_LUT4
    generic map(
      INIT => X"C939"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(2),
      ADR1 => timer_inst_tenths_cnt(3),
      ADR2 => timer_inst_Maddsub_tenths_cnt_share0000_lut(2),
      ADR3 => N165,
      O => N71
    );
  timer_inst_ones_cnt_mux0002_8_19 : X_LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(0),
      ADR1 => N166,
      O => timer_inst_ones_cnt_mux0002_8_19_897
    );
  timer_inst_ones_cnt_mux0002_10_25_SW0 : X_LUT3
    generic map(
      INIT => X"7E"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(0),
      ADR1 => timer_inst_ones_cnt(1),
      ADR2 => N170,
      O => N73
    );
  timer_inst_Maddsub_ones_cnt_share0000_lut_1_1 : X_LUT4
    generic map(
      INIT => X"9555"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(1),
      ADR1 => timer_inst_tc_2_cmp_eq0000,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      ADR3 => mode_control,
      O => timer_inst_Maddsub_ones_cnt_share0000_lut(1)
    );
  timer_inst_tenths_cnt_mux0002_5_20 : X_LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      ADR0 => timer_inst_tc_1_and0001,
      ADR1 => timer_inst_N14,
      ADR2 => timer_inst_tenths_cnt(1),
      O => timer_inst_tenths_cnt_mux0002_5_20_936
    );
  timer_inst_ones_cnt_mux0002_8_5 : X_LUT4
    generic map(
      INIT => X"8D88"
    )
    port map (
      ADR0 => timer_inst_ones_cnt_and0002,
      ADR1 => timer_inst_tc_3_cmp_eq0001,
      ADR2 => timer_inst_ones_cnt_and0001,
      ADR3 => timer_inst_ones_cnt(0),
      O => timer_inst_ones_cnt_mux0002_8_5_898
    );
  timer_inst_tenths_cnt_mux0002_7_8 : X_LUT4
    generic map(
      INIT => X"EC20"
    )
    port map (
      ADR0 => timer_inst_N14,
      ADR1 => timer_inst_tc_1_and0001,
      ADR2 => timer_inst_tenths_cnt(3),
      ADR3 => timer_inst_tc_2_cmp_eq0001,
      O => timer_inst_tenths_cnt_mux0002_7_8_942
    );
  timer_inst_ones_cnt_not000289 : X_LUT4
    generic map(
      INIT => X"ECEE"
    )
    port map (
      ADR0 => timer_state_clken_943,
      ADR1 => timer_state_sreg_FSM_FFd5_967,
      ADR2 => N75,
      ADR3 => timer_inst_tc_1,
      O => timer_inst_ones_cnt_not0002
    );
  timer_inst_ones_cnt_mux0002_11_11_SW0 : X_LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => N161,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      O => N79
    );
  timer_inst_mins_cnt_and000011_SW0 : X_LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      O => N83
    );
  timer_inst_Maddsub_mins_cnt_share0000_lut_2_1 : X_LUT4
    generic map(
      INIT => X"5955"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(2),
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => N83,
      ADR3 => mode_control,
      O => timer_inst_Maddsub_mins_cnt_share0000_lut(2)
    );
  timer_inst_ones_cnt_mux0002_9_1 : X_LUT4
    generic map(
      INIT => X"020A"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(1),
      ADR1 => timer_inst_tc_2_cmp_eq0001,
      ADR2 => timer_inst_ones_cnt_and0001,
      ADR3 => timer_inst_tc_1_and0001,
      O => timer_inst_ones_cnt_mux0002_9_1_900
    );
  timer_inst_ones_cnt_mux0002_10_1 : X_LUT4
    generic map(
      INIT => X"020A"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(2),
      ADR1 => timer_inst_tc_2_cmp_eq0001,
      ADR2 => timer_inst_ones_cnt_and0001,
      ADR3 => timer_inst_tc_1_and0001,
      O => timer_inst_ones_cnt_mux0002_10_1_891
    );
  timer_inst_ones_cnt_mux0002_10_11_SW0 : X_LUT4
    generic map(
      INIT => X"DFFF"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      ADR3 => mode_control,
      O => N90
    );
  timer_inst_ones_cnt_mux0002_9_40 : X_LUT4
    generic map(
      INIT => X"E2F3"
    )
    port map (
      ADR0 => timer_inst_ones_cnt_mux0002_9_1_900,
      ADR1 => timer_state_sreg_FSM_FFd5_967,
      ADR2 => preset_time_9_Q,
      ADR3 => N92,
      O => timer_inst_ones_cnt_mux0002(9)
    );
  timer_inst_Maddsub_ones_cnt_share0000_cy_2_11_SW0 : X_LUT4
    generic map(
      INIT => X"AAA9"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(3),
      ADR1 => timer_inst_ones_cnt(2),
      ADR2 => timer_inst_ones_cnt(1),
      ADR3 => timer_inst_ones_cnt(0),
      O => N96
    );
  timer_inst_Maddsub_ones_cnt_share0000_cy_2_11_SW1 : X_LUT4
    generic map(
      INIT => X"9555"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(3),
      ADR1 => timer_inst_ones_cnt(2),
      ADR2 => timer_inst_ones_cnt(1),
      ADR3 => timer_inst_ones_cnt(0),
      O => N97
    );
  timer_inst_ones_cnt_mux0002_11_37 : X_LUT4
    generic map(
      INIT => X"3A00"
    )
    port map (
      ADR0 => N96,
      ADR1 => N97,
      ADR2 => timer_inst_ones_cnt_and0001,
      ADR3 => timer_inst_N6,
      O => timer_inst_ones_cnt_mux0002_11_37_894
    );
  timer_inst_mins_cnt_and000011_SW1 : X_LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_1_cmp_eq0000,
      O => N103
    );
  timer_inst_Maddsub_mins_cnt_share0000_lut_1_1 : X_LUT4
    generic map(
      INIT => X"5955"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(1),
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => N105,
      ADR3 => mode_control,
      O => timer_inst_Maddsub_mins_cnt_share0000_lut(1)
    );
  timer_inst_Maddsub_tens_cnt_share0000_cy_1_11 : X_LUT4
    generic map(
      INIT => X"8EEE"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(0),
      ADR1 => timer_inst_tens_cnt(1),
      ADR2 => N107,
      ADR3 => mode_control,
      O => timer_inst_Maddsub_tens_cnt_share0000_cy(1)
    );
  timer_inst_Maddsub_mins_cnt_share0000_cy_1_11 : X_LUT4
    generic map(
      INIT => X"8EEE"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(0),
      ADR1 => timer_inst_mins_cnt(1),
      ADR2 => N109,
      ADR3 => mode_control,
      O => timer_inst_Maddsub_mins_cnt_share0000_cy(1)
    );
  timer_inst_tens_cnt_mux0002_13_11_SW0 : X_LUT4
    generic map(
      INIT => X"FDFF"
    )
    port map (
      ADR0 => timer_inst_tc_3_cmp_eq0000,
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => N111,
      ADR3 => mode_control,
      O => N42
    );
  timer_inst_tens_cnt_mux0002_13_11_SW1 : X_LUT4
    generic map(
      INIT => X"6966"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(0),
      ADR1 => timer_inst_tens_cnt(1),
      ADR2 => N168,
      ADR3 => mode_control,
      O => N43
    );
  timer_inst_Maddsub_tens_cnt_share0000_cy_2_11 : X_LUT4
    generic map(
      INIT => X"EA2A"
    )
    port map (
      ADR0 => N115,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => mode_control,
      ADR3 => N116,
      O => timer_inst_Maddsub_tens_cnt_share0000_cy(2)
    );
  timer_inst_mins_cnt_mux0002_19_37_SW0 : X_LUT4
    generic map(
      INIT => X"EA2A"
    )
    port map (
      ADR0 => N51,
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => mode_control,
      ADR3 => N118,
      O => N30
    );
  timer_inst_ones_cnt_mux0002_10_40 : X_LUT4
    generic map(
      INIT => X"E2F3"
    )
    port map (
      ADR0 => timer_inst_ones_cnt_mux0002_10_1_891,
      ADR1 => timer_state_sreg_FSM_FFd5_967,
      ADR2 => preset_time_10_Q,
      ADR3 => N120,
      O => timer_inst_ones_cnt_mux0002(10)
    );
  timer_inst_ones_cnt_mux0002_10_11_SW1 : X_LUT3
    generic map(
      INIT => X"BF"
    )
    port map (
      ADR0 => N157,
      ADR1 => timer_inst_tc_2_cmp_eq0001,
      ADR2 => timer_inst_tc_1_and0001,
      O => N122
    );
  timer_inst_mins_cnt_and000011_SW10 : X_LUT4
    generic map(
      INIT => X"0800"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_4_cmp_eq0000,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => N126
    );
  timer_inst_Maddsub_tenths_cnt_share0000_lut_1_1_SW0 : X_LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(1),
      ADR1 => timer_inst_tenths_cnt(0),
      O => N128
    );
  timer_inst_hundredths_cnt_mux0003_3_110_SW0 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd7_971,
      ADR1 => timer_state_sreg_FSM_FFd12_957,
      ADR2 => timer_state_sreg_FSM_FFd10_953,
      O => N130
    );
  timer_inst_mins_cnt_and000111_SW0 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd8_973,
      ADR1 => timer_state_sreg_FSM_FFd7_971,
      ADR2 => timer_state_sreg_FSM_FFd12_957,
      ADR3 => timer_state_sreg_FSM_FFd10_953,
      O => N132
    );
  timer_inst_mins_cnt_and000111 : X_LUT4
    generic map(
      INIT => X"1000"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd9_975,
      ADR1 => N132,
      ADR2 => timer_inst_tc_1_cmp_eq0001,
      ADR3 => N159,
      O => timer_inst_ones_cnt_and0002
    );
  timer_inst_Maddsub_tenths_cnt_share0000_lut_2_1 : X_LUT4
    generic map(
      INIT => X"9A55"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(2),
      ADR1 => timer_state_sreg_FSM_FFd9_975,
      ADR2 => N136,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_Maddsub_tenths_cnt_share0000_lut(2)
    );
  timer_inst_mins_cnt_and000011_SW8 : X_MUX2
    port map (
      IA => N138,
      IB => N139,
      SEL => timer_inst_tc_2_cmp_eq0000,
      O => N116
    );
  timer_inst_mins_cnt_and000011_SW8_F : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(2),
      ADR1 => timer_inst_tens_cnt(1),
      ADR2 => timer_inst_tens_cnt(0),
      O => N138
    );
  timer_inst_mins_cnt_and000011_SW8_G : X_LUT4
    generic map(
      INIT => X"80FE"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(0),
      ADR1 => timer_inst_tens_cnt(2),
      ADR2 => timer_inst_tens_cnt(1),
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => N139
    );
  timer_inst_mins_cnt_and000011_SW9 : X_MUX2
    port map (
      IA => N51,
      IB => N141,
      SEL => timer_inst_tc_1_cmp_eq0000,
      O => N118
    );
  timer_inst_mins_cnt_and000011_SW9_G : X_LUT4
    generic map(
      INIT => X"ACCC"
    )
    port map (
      ADR0 => N52,
      ADR1 => N51,
      ADR2 => timer_inst_tc_2_cmp_eq0000,
      ADR3 => timer_inst_tc_3_cmp_eq0000,
      O => N141
    );
  lcd_cntrl_inst_sf_d_temp_2_23 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd42_751,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd43_752,
      ADR2 => lcd_cntrl_inst_sf_d_temp_2_12_662,
      ADR3 => lcd_cntrl_inst_sf_d_temp_2_22_663,
      O => lcd_cntrl_inst_sf_d_temp_2_23_664
    );
  lcd_cntrl_inst_state_FSM_FFd14_In1 : X_LUT4
    generic map(
      INIT => X"8088"
    )
    port map (
      ADR0 => mode_control,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR2 => lcd_cntrl_inst_lap_flag_580,
      ADR3 => lcd_cntrl_inst_clock_flag_503,
      O => lcd_cntrl_inst_state_FSM_FFd14_In1_713
    );
  lcd_cntrl_inst_sf_d_temp_0_221 : X_LUT4
    generic map(
      INIT => X"FF80"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR1 => timer_inst_tens_cnt(3),
      ADR2 => timer_inst_tens_cnt(1),
      ADR3 => lcd_cntrl_inst_sf_d_temp_0_220_622,
      O => lcd_cntrl_inst_sf_d_temp_0_221_623
    );
  lcd_cntrl_inst_state_FSM_FFd33_In331 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd1_707,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd28_728,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd34_737,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd33_In10_735,
      O => lcd_cntrl_inst_state_FSM_FFd33_In33
    );
  lcd_cntrl_inst_clock_flag_mux0000111127_SW0 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd21_721,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd23_723,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd27_727,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd36_741,
      O => N142
    );
  lcd_cntrl_inst_clock_flag_mux0000111127 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_clock_flag_mux0000111112_505,
      ADR1 => lcd_cntrl_inst_clock_flag_mux0000111117_506,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd1_707,
      ADR3 => N142,
      O => lcd_cntrl_inst_N9
    );
  lcd_cntrl_inst_sf_d_temp_5_1 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N9,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd37_742,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd38_743,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd15_714,
      O => lcd_cntrl_inst_sf_d_temp_5_1_700
    );
  lcd_cntrl_inst_sf_d_temp_2_1_SW0 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd39_744,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd12_710,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd36_741,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd37_742,
      O => N8
    );
  Mcount_address_xor_3_12 : X_LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      ADR0 => address(3),
      ADR1 => address(2),
      ADR2 => address(1),
      ADR3 => address(0),
      O => Mcount_address3
    );
  timer_state_sreg_FSM_FFd5_In1 : X_LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      ADR0 => lap_load_debounce_Q2_293,
      ADR1 => lap_load_debounce_Q3_294,
      ADR2 => lap_load_debounce_Q1_292,
      ADR3 => timer_state_sreg_FSM_FFd4_963,
      O => timer_state_sreg_FSM_FFd5_In
    );
  timer_state_sreg1_FSM_FFd1_In1 : X_LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      ADR0 => lap_load_debounce_Q2_293,
      ADR1 => lap_load_debounce_Q3_294,
      ADR2 => lap_load_debounce_Q1_292,
      ADR3 => timer_state_sreg1_FSM_FFd2_949,
      O => timer_state_next_lap_trigger1
    );
  timer_state_sreg1_FSM_FFd2_In1 : X_LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      ADR0 => lap_load_debounce_Q1_292,
      ADR1 => timer_state_sreg1_FSM_FFd1_948,
      ADR2 => lap_load_debounce_Q3_294,
      ADR3 => lap_load_debounce_Q2_293,
      O => timer_state_sreg1_FSM_FFd2_In
    );
  lcd_cntrl_inst_timer_flag_not00011 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd9_975,
      ADR1 => timer_state_sreg_FSM_FFd12_957,
      ADR2 => timer_state_sreg_FSM_FFd10_953,
      ADR3 => N25,
      O => lcd_cntrl_inst_timer_flag_not0001
    );
  lcd_cntrl_inst_control_0_12 : X_LUT4
    generic map(
      INIT => X"AA8A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N0,
      ADR1 => lcd_cntrl_inst_state_cmp_eq0001,
      ADR2 => lcd_cntrl_inst_control_cmp_le0000,
      ADR3 => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(8),
      O => lcd_cntrl_inst_control_0_12_511
    );
  lcd_cntrl_inst_control_0_25 : X_LUT4
    generic map(
      INIT => X"AA8A"
    )
    port map (
      ADR0 => lcd_cntrl_inst_N6,
      ADR1 => lcd_cntrl_inst_state_cmp_eq0000,
      ADR2 => lcd_cntrl_inst_control_cmp_le0001,
      ADR3 => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_cy(8),
      O => lcd_cntrl_inst_control_0_25_512
    );
  timer_inst_hundredths_cnt_mux0003_0_36 : X_LUT3
    generic map(
      INIT => X"B1"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd5_967,
      ADR1 => timer_inst_hundredths_cnt(0),
      ADR2 => preset_time_0_Q,
      O => timer_inst_hundredths_cnt_mux0003(0)
    );
  clk_divider_div_262144_BUFG : X_CKBUF
    port map (
      I => clk_divider_div_2621441,
      O => clk_divider_div_262144_286
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_lut_4_INV_0 : X_INV
    port map (
      I => clk_divider_cnt(30),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_lut(4)
    );
  clk_divider_Mcount_cnt_lut_0_INV_0 : X_INV
    port map (
      I => clk_divider_cnt(0),
      O => clk_divider_Mcount_cnt_lut(0)
    );
  lcd_cntrl_inst_Mcount_count_temp_lut_0_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count_temp(0),
      O => lcd_cntrl_inst_Mcount_count_temp_lut(0)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_7_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(18),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_7_Q
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_5_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(15),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_5_Q
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_1_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(6),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_1_Q
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_2_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(12),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_7_2
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_3_1_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(6),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_3_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_1_1_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(4),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_1_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_13_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(30),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(13)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_6_1_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(11),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_6_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_2_1_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(5),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_2_1
    );
  clk_divider_div_temp_not00011_INV_0 : X_INV
    port map (
      I => clk_divider_div_temp_288,
      O => clk_divider_div_temp_not0001
    );
  Mcount_address_xor_0_11_INV_0 : X_INV
    port map (
      I => address(0),
      O => Mcount_address
    );
  clk_divider_Mcompar_cnt_cmp_ge0000_lut_5_1_INV_0 : X_INV
    port map (
      I => clk_divider_cnt(31),
      O => clk_divider_Mcompar_cnt_cmp_ge0000_lut(5)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_14_2_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(31),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(14)
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_14_11_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(31),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_14_1
    );
  lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut_8_3_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(31),
      O => lcd_cntrl_inst_Mcompar_control_cmp_gt0000_lut(8)
    );
  lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_12_1_INV_0 : X_INV
    port map (
      I => lcd_cntrl_inst_count(31),
      O => lcd_cntrl_inst_Mcompar_state_cmp_ge0000_lut_12_Q
    );
  timer_state_sreg_FSM_FFd9_In : X_MUX2
    port map (
      IA => N146,
      IB => N147,
      SEL => timer_state_sreg_FSM_FFd12_957,
      O => timer_state_sreg_FSM_FFd9_In_976
    );
  timer_state_sreg_FSM_FFd9_In_F : X_LUT4
    generic map(
      INIT => X"2232"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd8_973,
      ADR1 => strtstop_debounced,
      ADR2 => timer_state_sreg_FSM_FFd9_975,
      ADR3 => mode_debounced,
      O => N146
    );
  timer_state_sreg_FSM_FFd9_In_G : X_LUT3
    generic map(
      INIT => X"4F"
    )
    port map (
      ADR0 => strtstop_debounced,
      ADR1 => timer_state_sreg_FSM_FFd8_973,
      ADR2 => mode_debounced,
      O => N147
    );
  timer_inst_ones_cnt_not000260_SW0 : X_MUX2
    port map (
      IA => N148,
      IB => N149,
      SEL => mode_control,
      O => N75
    );
  timer_inst_ones_cnt_not000260_SW0_F : X_LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0001,
      ADR1 => timer_inst_tc_3_cmp_eq0001,
      ADR2 => timer_inst_tc_4_cmp_eq0001,
      ADR3 => timer_inst_tc_5_cmp_eq0001,
      O => N148
    );
  timer_inst_ones_cnt_not000260_SW0_G : X_LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_4_cmp_eq0000,
      ADR3 => timer_inst_tc_5_cmp_eq0000,
      O => N149
    );
  timer_state_next_clken25 : X_MUX2
    port map (
      IA => N150,
      IB => N151,
      SEL => strtstop_debounced,
      O => timer_state_next_clken
    );
  timer_state_next_clken25_F : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd7_971,
      ADR1 => timer_state_sreg_FSM_FFd1_951,
      ADR2 => timer_state_next_clken0_945,
      O => N150
    );
  timer_state_next_clken25_G : X_LUT4
    generic map(
      INIT => X"FFAE"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd9_975,
      ADR1 => timer_state_sreg_FSM_FFd4_963,
      ADR2 => ll_debounced,
      ADR3 => timer_state_next_clken0_945,
      O => N151
    );
  timer_inst_tenths_cnt_mux0002_4_54 : X_MUX2
    port map (
      IA => N152,
      IB => N153,
      SEL => timer_inst_tc_1_and0001,
      O => timer_inst_tenths_cnt_mux0002(4)
    );
  timer_inst_tenths_cnt_mux0002_4_54_F : X_LUT4
    generic map(
      INIT => X"F909"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(0),
      ADR1 => timer_inst_N14,
      ADR2 => timer_state_sreg_FSM_FFd5_967,
      ADR3 => preset_time_4_Q,
      O => N152
    );
  timer_inst_tenths_cnt_mux0002_4_54_G : X_LUT4
    generic map(
      INIT => X"E2F3"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0001,
      ADR1 => timer_state_sreg_FSM_FFd5_967,
      ADR2 => preset_time_4_Q,
      ADR3 => timer_inst_tenths_cnt(0),
      O => N153
    );
  lcd_cntrl_inst_sf_d_temp_6_151 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd4_745,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd38_743,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd32_733,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd34_737,
      O => lcd_cntrl_inst_sf_d_temp_6_151_702
    );
  lcd_cntrl_inst_sf_d_temp_6_15_f5 : X_MUX2
    port map (
      IA => lcd_cntrl_inst_sf_d_temp_6_151_702,
      IB => N1,
      SEL => lcd_cntrl_inst_state_FSM_FFd6_760,
      O => lcd_cntrl_inst_sf_d_temp_6_15
    );
  lcd_cntrl_inst_state_FSM_FFd41_In221 : X_LUT4
    generic map(
      INIT => X"FF08"
    )
    port map (
      ADR0 => lcd_cntrl_inst_clock_flag_503,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR2 => lcd_cntrl_inst_lap_flag_580,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd42_751,
      O => lcd_cntrl_inst_state_FSM_FFd41_In221_749
    );
  lcd_cntrl_inst_state_FSM_FFd41_In222 : X_LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd42_751,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR2 => lcd_cntrl_inst_timer_flag_785,
      O => lcd_cntrl_inst_state_FSM_FFd41_In222_750
    );
  lcd_cntrl_inst_state_FSM_FFd41_In22_f5 : X_MUX2
    port map (
      IA => lcd_cntrl_inst_state_FSM_FFd41_In222_750,
      IB => lcd_cntrl_inst_state_FSM_FFd41_In221_749,
      SEL => mode_control,
      O => lcd_cntrl_inst_state_FSM_FFd41_In22
    );
  lcd_cntrl_inst_timer_flag_mux0000171 : X_LUT4
    generic map(
      INIT => X"FFAE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_mode_state_603,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR2 => lcd_cntrl_inst_state_cmp_eq0001,
      ADR3 => lcd_cntrl_inst_clock_flag_mux00002,
      O => lcd_cntrl_inst_timer_flag_mux000017
    );
  lcd_cntrl_inst_timer_flag_mux000017_f5 : X_MUX2
    port map (
      IA => lcd_cntrl_inst_mode_state_603,
      IB => lcd_cntrl_inst_timer_flag_mux000017,
      SEL => lcd_cntrl_inst_timer_flag_785,
      O => lcd_cntrl_inst_timer_flag_mux0000
    );
  lcd_cntrl_inst_sf_d_temp_7_11 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd12_710,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd14_712,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd26_726,
      ADR3 => lcd_cntrl_inst_state_FSM_FFd32_733,
      O => lcd_cntrl_inst_sf_d_temp_7_11_706
    );
  lcd_cntrl_inst_sf_d_temp_7_1_f5 : X_MUX2
    port map (
      IA => lcd_cntrl_inst_sf_d_temp_7_11_706,
      IB => N1,
      SEL => lcd_cntrl_inst_state_FSM_FFd33_734,
      O => lcd_cntrl_inst_sf_d_temp_7_1
    );
  lcd_cntrl_inst_sf_d_temp_6_1811 : X_LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd10_708,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd29_729,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd30_731,
      ADR3 => lcd_cntrl_inst_sf_d_temp_6_15,
      O => lcd_cntrl_inst_sf_d_temp_6_181
    );
  lcd_cntrl_inst_sf_d_temp_6_181_f5 : X_MUX2
    port map (
      IA => lcd_cntrl_inst_sf_d_temp_6_181,
      IB => N1,
      SEL => lcd_cntrl_inst_state_FSM_FFd8_762,
      O => lcd_cntrl_inst_sf_d_temp_6_18
    );
  timer_inst_tens_cnt_mux0002_12_621 : X_LUT3
    generic map(
      INIT => X"56"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(0),
      ADR1 => timer_inst_tens_cnt_and0000,
      ADR2 => timer_inst_ones_cnt_mux0002_11_2,
      O => timer_inst_tens_cnt_mux0002_12_62
    );
  timer_inst_tens_cnt_mux0002_12_62_f5 : X_MUX2
    port map (
      IA => timer_inst_tens_cnt_mux0002_12_62,
      IB => preset_time_12_Q,
      SEL => timer_state_sreg_FSM_FFd5_967,
      O => timer_inst_tens_cnt_mux0002(12)
    );
  timer_inst_tc_5_cmp_eq00011_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_tc_5_cmp_eq0001,
      O => N154
    );
  timer_inst_tc_5_cmp_eq00011 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(0),
      ADR1 => timer_inst_mins_cnt(1),
      ADR2 => timer_inst_mins_cnt(2),
      ADR3 => timer_inst_mins_cnt(3),
      O => timer_inst_tc_5_cmp_eq0001
    );
  lcd_cntrl_inst_count_temp_or000011_LUT3_D_BUF : X_BUF
    port map (
      I => lcd_cntrl_inst_N0,
      O => N155
    );
  lcd_cntrl_inst_count_temp_or000011 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd15_714,
      ADR1 => lcd_cntrl_inst_N4,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd41_747,
      O => lcd_cntrl_inst_N0
    );
  lcd_cntrl_inst_sf_d_temp_1_1_SW0_LUT3_L_BUF : X_BUF
    port map (
      I => lcd_cntrl_inst_sf_d_temp_1_1_SW0_O,
      O => N19
    );
  lcd_cntrl_inst_sf_d_temp_1_1_SW0 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd35_739,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd19_718,
      ADR2 => lcd_cntrl_inst_state_FSM_FFd25_725,
      O => lcd_cntrl_inst_sf_d_temp_1_1_SW0_O
    );
  lcd_cntrl_inst_clock_flag_mux0000111_SW0_LUT3_L_BUF : X_BUF
    port map (
      I => lcd_cntrl_inst_clock_flag_mux0000111_SW0_O,
      O => N21
    );
  lcd_cntrl_inst_clock_flag_mux0000111_SW0 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => lcd_cntrl_inst_state_FSM_FFd38_743,
      ADR1 => lcd_cntrl_inst_state_FSM_FFd39_744,
      ADR2 => lcd_cntrl_inst_N13,
      O => lcd_cntrl_inst_clock_flag_mux0000111_SW0_O
    );
  timer_inst_tenths_cnt_mux0002_5_11_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_N5,
      O => N156
    );
  timer_inst_tenths_cnt_mux0002_5_11 : X_LUT4
    generic map(
      INIT => X"222F"
    )
    port map (
      ADR0 => timer_inst_tc_1_and0001,
      ADR1 => timer_inst_tc_2_cmp_eq0001,
      ADR2 => timer_inst_tc_2_cmp_eq0000,
      ADR3 => timer_inst_N14,
      O => timer_inst_N5
    );
  timer_inst_tc_3_cmp_eq00011_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_tc_3_cmp_eq0001,
      O => N157
    );
  timer_inst_tc_3_cmp_eq00011 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(0),
      ADR1 => timer_inst_ones_cnt(1),
      ADR2 => timer_inst_ones_cnt(2),
      ADR3 => timer_inst_ones_cnt(3),
      O => timer_inst_tc_3_cmp_eq0001
    );
  timer_inst_hundredths_cnt_mux0003_1_31_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_hundredths_cnt_mux0003_1_31_863,
      O => N158
    );
  timer_inst_hundredths_cnt_mux0003_1_31 : X_LUT4
    generic map(
      INIT => X"010B"
    )
    port map (
      ADR0 => mode_control,
      ADR1 => timer_inst_tc_1_cmp_eq0001,
      ADR2 => timer_state_sreg_FSM_FFd5_967,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_hundredths_cnt_mux0003_1_31_863
    );
  timer_inst_tc_2_cmp_eq00011_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_tc_2_cmp_eq0001,
      O => N159
    );
  timer_inst_tc_2_cmp_eq00011 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_inst_tenths_cnt(0),
      ADR1 => timer_inst_tenths_cnt(1),
      ADR2 => timer_inst_tenths_cnt(2),
      ADR3 => timer_inst_tenths_cnt(3),
      O => timer_inst_tc_2_cmp_eq0001
    );
  timer_inst_tens_cnt_mux0002_13_11_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_tens_cnt_mux0002_13_11_O,
      O => timer_inst_N3
    );
  timer_inst_tens_cnt_mux0002_13_11 : X_LUT4
    generic map(
      INIT => X"7350"
    )
    port map (
      ADR0 => timer_inst_tc_4_cmp_eq0001,
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => timer_inst_ones_cnt_mux0002_11_2,
      ADR3 => timer_inst_tens_cnt_and0000,
      O => timer_inst_tens_cnt_mux0002_13_11_O
    );
  timer_inst_tc_4_cmp_eq00001_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_tc_4_cmp_eq0000,
      O => N160
    );
  timer_inst_tc_4_cmp_eq00001 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(3),
      ADR1 => timer_inst_tens_cnt(2),
      ADR2 => timer_inst_tens_cnt(0),
      ADR3 => timer_inst_tens_cnt(1),
      O => timer_inst_tc_4_cmp_eq0000
    );
  timer_inst_tc_3_cmp_eq00001_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_tc_3_cmp_eq0000,
      O => N161
    );
  timer_inst_tc_3_cmp_eq00001 : X_LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(2),
      ADR1 => timer_inst_ones_cnt(0),
      ADR2 => timer_inst_ones_cnt(3),
      ADR3 => timer_inst_ones_cnt(1),
      O => timer_inst_tc_3_cmp_eq0000
    );
  timer_state_sreg_FSM_Out7_SW0_LUT2_D_BUF : X_BUF
    port map (
      I => N25,
      O => N162
    );
  timer_state_sreg_FSM_Out7_SW0 : X_LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd7_971,
      ADR1 => timer_state_sreg_FSM_FFd8_973,
      O => N25
    );
  timer_inst_mins_cnt_and000011_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_tens_cnt_and0000,
      O => N163
    );
  timer_inst_mins_cnt_and000011 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      ADR3 => mode_control,
      O => timer_inst_tens_cnt_and0000
    );
  timer_inst_tens_cnt_mux0002_13_1_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_tens_cnt_mux0002_13_1_O,
      O => timer_inst_tens_cnt_mux0002_13_1_922
    );
  timer_inst_tens_cnt_mux0002_13_1 : X_LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(1),
      ADR1 => timer_inst_ones_cnt_mux0002_11_2,
      ADR2 => timer_inst_tens_cnt_and0000,
      O => timer_inst_tens_cnt_mux0002_13_1_O
    );
  timer_inst_tens_cnt_mux0002_15_1_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_tens_cnt_mux0002_15_1_O,
      O => timer_inst_tens_cnt_mux0002_15_1_928
    );
  timer_inst_tens_cnt_mux0002_15_1 : X_LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(3),
      ADR1 => timer_inst_ones_cnt_mux0002_11_2,
      ADR2 => timer_inst_tens_cnt_and0000,
      O => timer_inst_tens_cnt_mux0002_15_1_O
    );
  timer_inst_Maddsub_hundredths_cnt_share0000_cy_1_11_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_Maddsub_hundredths_cnt_share0000_cy_1_11_O,
      O => timer_inst_Maddsub_hundredths_cnt_share0000_cy(1)
    );
  timer_inst_Maddsub_hundredths_cnt_share0000_cy_1_11 : X_LUT3
    generic map(
      INIT => X"D4"
    )
    port map (
      ADR0 => mode_control,
      ADR1 => timer_inst_hundredths_cnt(1),
      ADR2 => timer_inst_hundredths_cnt(0),
      O => timer_inst_Maddsub_hundredths_cnt_share0000_cy_1_11_O
    );
  timer_inst_tens_cnt_and00011_LUT3_D_BUF : X_BUF
    port map (
      I => timer_inst_ones_cnt_mux0002_11_2,
      O => N164
    );
  timer_inst_tens_cnt_and00011 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => timer_inst_tc_3_cmp_eq0001,
      ADR1 => timer_inst_tc_2_cmp_eq0001,
      ADR2 => timer_inst_tc_1_and0001,
      O => timer_inst_ones_cnt_mux0002_11_2
    );
  timer_inst_Maddsub_tenths_cnt_share0000_cy_1_11_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_Maddsub_tenths_cnt_share0000_cy(1),
      O => N165
    );
  timer_inst_Maddsub_tenths_cnt_share0000_cy_1_11 : X_LUT4
    generic map(
      INIT => X"D4FC"
    )
    port map (
      ADR0 => timer_inst_tc_1_cmp_eq0000,
      ADR1 => timer_inst_tenths_cnt(0),
      ADR2 => timer_inst_tenths_cnt(1),
      ADR3 => mode_control,
      O => timer_inst_Maddsub_tenths_cnt_share0000_cy(1)
    );
  timer_inst_mins_cnt_mux0002_18_40_SW0_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_mux0002_18_40_SW0_O,
      O => N67
    );
  timer_inst_mins_cnt_mux0002_18_40_SW0 : X_LUT3
    generic map(
      INIT => X"9F"
    )
    port map (
      ADR0 => timer_inst_Maddsub_mins_cnt_share0000_lut(2),
      ADR1 => timer_inst_Maddsub_mins_cnt_share0000_cy(1),
      ADR2 => timer_inst_N4,
      O => timer_inst_mins_cnt_mux0002_18_40_SW0_O
    );
  timer_inst_mins_cnt_mux0002_17_40_SW0_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_mux0002_17_40_SW0_O,
      O => N69
    );
  timer_inst_mins_cnt_mux0002_17_40_SW0 : X_LUT3
    generic map(
      INIT => X"9F"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(0),
      ADR1 => timer_inst_Maddsub_mins_cnt_share0000_lut(1),
      ADR2 => timer_inst_N4,
      O => timer_inst_mins_cnt_mux0002_17_40_SW0_O
    );
  timer_inst_tenths_cnt_mux0002_7_34_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_tenths_cnt_mux0002_7_34_O,
      O => timer_inst_tenths_cnt_mux0002_7_34_941
    );
  timer_inst_tenths_cnt_mux0002_7_34 : X_LUT4
    generic map(
      INIT => X"C00D"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_1_and0001,
      ADR2 => timer_inst_N14,
      ADR3 => N71,
      O => timer_inst_tenths_cnt_mux0002_7_34_O
    );
  timer_inst_ones_cnt_mux0002_11_4_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_ones_cnt_mux0002_11_4_O,
      O => timer_inst_ones_cnt_mux0002_11_4_895
    );
  timer_inst_ones_cnt_mux0002_11_4 : X_LUT4
    generic map(
      INIT => X"FF02"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(3),
      ADR1 => timer_inst_ones_cnt_and0001,
      ADR2 => timer_inst_ones_cnt_and0002,
      ADR3 => timer_inst_ones_cnt_mux0002_11_2,
      O => timer_inst_ones_cnt_mux0002_11_4_O
    );
  timer_inst_tenths_cnt_mux0002_6_20_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_tenths_cnt_mux0002_6_20_O,
      O => timer_inst_tenths_cnt_mux0002_6_20_939
    );
  timer_inst_tenths_cnt_mux0002_6_20 : X_LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      ADR0 => timer_inst_tc_1_and0001,
      ADR1 => timer_inst_N14,
      ADR2 => timer_inst_tenths_cnt(2),
      O => timer_inst_tenths_cnt_mux0002_6_20_O
    );
  timer_inst_Maddsub_hundredths_cnt_share0000_cy_2_11_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_Maddsub_hundredths_cnt_share0000_cy_2_11_O,
      O => timer_inst_Maddsub_hundredths_cnt_share0000_cy(2)
    );
  timer_inst_Maddsub_hundredths_cnt_share0000_cy_2_11 : X_LUT4
    generic map(
      INIT => X"80FE"
    )
    port map (
      ADR0 => timer_inst_hundredths_cnt(0),
      ADR1 => timer_inst_hundredths_cnt(2),
      ADR2 => timer_inst_hundredths_cnt(1),
      ADR3 => mode_control,
      O => timer_inst_Maddsub_hundredths_cnt_share0000_cy_2_11_O
    );
  timer_inst_tens_cnt_mux0002_14_5_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_tens_cnt_mux0002_14_5_O,
      O => timer_inst_tens_cnt_mux0002_14_5_926
    );
  timer_inst_tens_cnt_mux0002_14_5 : X_LUT4
    generic map(
      INIT => X"C0CA"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(2),
      ADR1 => timer_inst_tc_4_cmp_eq0001,
      ADR2 => timer_inst_ones_cnt_mux0002_11_2,
      ADR3 => timer_inst_tens_cnt_and0000,
      O => timer_inst_tens_cnt_mux0002_14_5_O
    );
  timer_inst_ones_cnt_mux0002_11_11_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_N6,
      O => N166
    );
  timer_inst_ones_cnt_mux0002_11_11 : X_LUT4
    generic map(
      INIT => X"8F88"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0001,
      ADR1 => timer_inst_tc_1_and0001,
      ADR2 => N79,
      ADR3 => mode_control,
      O => timer_inst_N6
    );
  timer_inst_mins_cnt_mux0002_16_19_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_mux0002_16_19_O,
      O => timer_inst_mins_cnt_mux0002_16_19_875
    );
  timer_inst_mins_cnt_mux0002_16_19 : X_LUT4
    generic map(
      INIT => X"5150"
    )
    port map (
      ADR0 => timer_inst_mins_cnt(0),
      ADR1 => N49,
      ADR2 => timer_inst_mins_cnt_and0001,
      ADR3 => timer_inst_tens_cnt_and0000,
      O => timer_inst_mins_cnt_mux0002_16_19_O
    );
  timer_inst_mins_cnt_and00002_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_and0000,
      O => N167
    );
  timer_inst_mins_cnt_and00002 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => timer_inst_tc_3_cmp_eq0000,
      ADR1 => timer_inst_tc_4_cmp_eq0000,
      ADR2 => N103,
      ADR3 => mode_control,
      O => timer_inst_mins_cnt_and0000
    );
  timer_inst_mins_cnt_and000011_SW2_LUT3_D_BUF : X_BUF
    port map (
      I => N105,
      O => N168
    );
  timer_inst_mins_cnt_and000011_SW2 : X_LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      O => N105
    );
  timer_inst_mins_cnt_and000011_SW3_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_and000011_SW3_O,
      O => N107
    );
  timer_inst_mins_cnt_and000011_SW3 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_mins_cnt_and000011_SW3_O
    );
  timer_inst_mins_cnt_and000011_SW4_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_and000011_SW4_O,
      O => N109
    );
  timer_inst_mins_cnt_and000011_SW4 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => timer_inst_tc_2_cmp_eq0000,
      ADR1 => timer_inst_tc_3_cmp_eq0000,
      ADR2 => timer_inst_tc_4_cmp_eq0000,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_mins_cnt_and000011_SW4_O
    );
  timer_inst_mins_cnt_and000011_SW5_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_and000011_SW5_O,
      O => N111
    );
  timer_inst_mins_cnt_and000011_SW5 : X_LUT4
    generic map(
      INIT => X"9FFF"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(0),
      ADR1 => timer_inst_tens_cnt(1),
      ADR2 => timer_inst_tc_2_cmp_eq0000,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_mins_cnt_and000011_SW5_O
    );
  timer_inst_mins_cnt_and000011_SW7_LUT3_L_BUF : X_BUF
    port map (
      I => timer_inst_mins_cnt_and000011_SW7_O,
      O => N115
    );
  timer_inst_mins_cnt_and000011_SW7 : X_LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      ADR0 => timer_inst_tens_cnt(2),
      ADR1 => timer_inst_tens_cnt(1),
      ADR2 => timer_inst_tens_cnt(0),
      O => timer_inst_mins_cnt_and000011_SW7_O
    );
  timer_inst_ones_cnt_mux0002_9_40_SW0_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_ones_cnt_mux0002_9_40_SW0_O,
      O => N92
    );
  timer_inst_ones_cnt_mux0002_9_40_SW0 : X_LUT4
    generic map(
      INIT => X"EDA5"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(0),
      ADR1 => N122,
      ADR2 => timer_inst_Maddsub_ones_cnt_share0000_lut(1),
      ADR3 => N90,
      O => timer_inst_ones_cnt_mux0002_9_40_SW0_O
    );
  timer_inst_ones_cnt_mux0002_10_25_SW1_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_ones_cnt_mux0002_10_25_SW1_O,
      O => N120
    );
  timer_inst_ones_cnt_mux0002_10_25_SW1 : X_LUT4
    generic map(
      INIT => X"D5EA"
    )
    port map (
      ADR0 => timer_inst_ones_cnt(2),
      ADR1 => N122,
      ADR2 => N90,
      ADR3 => N73,
      O => timer_inst_ones_cnt_mux0002_10_25_SW1_O
    );
  timer_inst_tens_cnt_mux0002_14_11_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_tens_cnt_mux0002_14_11_O,
      O => timer_inst_tens_cnt_mux0002_14_11_925
    );
  timer_inst_tens_cnt_mux0002_14_11 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => timer_inst_tc_3_cmp_eq0001,
      ADR1 => timer_inst_ones_cnt_and0002,
      ADR2 => N126,
      ADR3 => mode_control,
      O => timer_inst_tens_cnt_mux0002_14_11_O
    );
  timer_inst_tenths_cnt_mux0002_5_17_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_tenths_cnt_mux0002_5_17_O,
      O => timer_inst_tenths_cnt_mux0002_5_17_935
    );
  timer_inst_tenths_cnt_mux0002_5_17 : X_LUT4
    generic map(
      INIT => X"9500"
    )
    port map (
      ADR0 => N128,
      ADR1 => timer_inst_tc_1_cmp_eq0000,
      ADR2 => mode_control,
      ADR3 => timer_inst_N5,
      O => timer_inst_tenths_cnt_mux0002_5_17_O
    );
  timer_inst_hundredths_cnt_mux0003_3_110_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_N14,
      O => N169
    );
  timer_inst_hundredths_cnt_mux0003_3_110 : X_LUT4
    generic map(
      INIT => X"01FF"
    )
    port map (
      ADR0 => N130,
      ADR1 => timer_state_sreg_FSM_FFd9_975,
      ADR2 => timer_state_sreg_FSM_FFd8_973,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_N14
    );
  timer_inst_ones_cnt_and00011_LUT4_D_BUF : X_BUF
    port map (
      I => timer_inst_ones_cnt_and0001,
      O => N170
    );
  timer_inst_ones_cnt_and00011 : X_LUT4
    generic map(
      INIT => X"E000"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd9_975,
      ADR1 => N132,
      ADR2 => timer_inst_tc_2_cmp_eq0000,
      ADR3 => timer_inst_tc_1_cmp_eq0000,
      O => timer_inst_ones_cnt_and0001
    );
  timer_inst_Maddsub_tenths_cnt_share0000_lut_2_1_SW0_LUT4_L_BUF : X_BUF
    port map (
      I => timer_inst_Maddsub_tenths_cnt_share0000_lut_2_1_SW0_O,
      O => N136
    );
  timer_inst_Maddsub_tenths_cnt_share0000_lut_2_1_SW0 : X_LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      ADR0 => timer_state_sreg_FSM_FFd8_973,
      ADR1 => timer_state_sreg_FSM_FFd7_971,
      ADR2 => timer_state_sreg_FSM_FFd12_957,
      ADR3 => timer_state_sreg_FSM_FFd10_953,
      O => timer_inst_Maddsub_tenths_cnt_share0000_lut_2_1_SW0_O
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712221_INV_0 : X_INV
    port map (
      I => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712221
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271222_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712222_52,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712221,
      SEL => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271222
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712222 : X_LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      ADR0 => address(1),
      ADR1 => address(2),
      ADR2 => address(4),
      ADR3 => address(3),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712222_52
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00008111_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00008111,
      IB => preset_time_15_Q,
      SEL => address(5),
      O => preset_time_6_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000081111 : X_LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      ADR0 => address(4),
      ADR1 => address(2),
      ADR2 => address(3),
      ADR3 => address(0),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00008111
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000241_INV_0 : X_INV
    port map (
      I => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000241
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f51 : X_LUT4
    generic map(
      INIT => X"80FF"
    )
    port map (
      ADR0 => address(4),
      ADR1 => address(2),
      ADR2 => address(3),
      ADR3 => address(0),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f51_29
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002712103 : X_LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      ADR0 => address(0),
      ADR1 => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271222,
      ADR2 => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271281_48,
      ADR3 => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271284_49,
      O => preset_time_13_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271284 : X_LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      ADR0 => address(3),
      ADR1 => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271284_49
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271281 : X_LUT4
    generic map(
      INIT => X"163C"
    )
    port map (
      ADR0 => address(0),
      ADR1 => address(2),
      ADR2 => address(4),
      ADR3 => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000271281_48
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002012 : X_LUT4
    generic map(
      INIT => X"3237"
    )
    port map (
      ADR0 => address(5),
      ADR1 => address(0),
      ADR2 => address(4),
      ADR3 => t_preset_N8,
      O => preset_time_10_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002012_SW0 : X_LUT4
    generic map(
      INIT => X"C9B5"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(0),
      ADR2 => address(1),
      ADR3 => address(3),
      O => t_preset_N8
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613_f6 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016132_46,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613_f5_45,
      SEL => address(1),
      O => preset_time_8_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016133 : X_LUT4
    generic map(
      INIT => X"5556"
    )
    port map (
      ADR0 => address(0),
      ADR1 => address(5),
      ADR2 => address(2),
      ADR3 => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016132_46
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016131,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613,
      SEL => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613_f5_45
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016132 : X_LUT3
    generic map(
      INIT => X"54"
    )
    port map (
      ADR0 => address(0),
      ADR1 => address(3),
      ADR2 => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000016131
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000023111 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => address(5),
      ADR1 => address(4),
      ADR2 => address(3),
      ADR3 => t_preset_N6,
      O => preset_time_0_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000023111_SW0 : X_LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(1),
      ADR2 => address(0),
      O => t_preset_N6
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00008112 : X_LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      ADR0 => preset_time_6_Q,
      ADR1 => preset_time_0_Q,
      O => preset_time_4_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002311 : X_LUT4
    generic map(
      INIT => X"FF01"
    )
    port map (
      ADR0 => address(5),
      ADR1 => address(4),
      ADR2 => t_preset_N2,
      ADR3 => preset_time_0_Q,
      O => preset_time_11_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00002311_SW0 : X_LUT4
    generic map(
      INIT => X"FFF7"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(1),
      ADR2 => address(3),
      ADR3 => address(0),
      O => t_preset_N2
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001811 : X_LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      ADR0 => address(5),
      ADR1 => address(4),
      ADR2 => t_preset_N01,
      O => preset_time_9_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001811_SW0 : X_LUT4
    generic map(
      INIT => X"EEE7"
    )
    port map (
      ADR0 => address(0),
      ADR1 => address(2),
      ADR2 => address(3),
      ADR3 => address(1),
      O => t_preset_N01
    );
  t_preset_spo_18_11 : X_LUT4
    generic map(
      INIT => X"1555"
    )
    port map (
      ADR0 => address(3),
      ADR1 => address(2),
      ADR2 => address(1),
      ADR3 => address(0),
      O => t_preset_spo_18_bdd0
    );
  t_preset_spo_18_2 : X_LUT3
    generic map(
      INIT => X"82"
    )
    port map (
      ADR0 => address(5),
      ADR1 => address(4),
      ADR2 => t_preset_spo_18_bdd0,
      O => preset_time_18_Q
    );
  t_preset_spo_19_1 : X_LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      ADR0 => address(4),
      ADR1 => t_preset_spo_18_bdd0,
      ADR2 => address(5),
      O => preset_time_19_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031_f6 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000312_39,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031_f5_38,
      SEL => address(3),
      O => preset_time_17_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000313 : X_LUT4
    generic map(
      INIT => X"2AAA"
    )
    port map (
      ADR0 => address(5),
      ADR1 => address(0),
      ADR2 => address(2),
      ADR3 => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000312_39
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000311_37,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031,
      SEL => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031_f5_38
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000312 : X_LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      ADR0 => address(1),
      ADR1 => address(0),
      ADR2 => address(2),
      ADR3 => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000311_37
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000311 : X_LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      ADR0 => address(1),
      ADR1 => address(0),
      ADR2 => address(2),
      ADR3 => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000031
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_f6 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000302_35,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_f5_34,
      SEL => address(4),
      O => preset_time_16_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000302 : X_LUT4
    generic map(
      INIT => X"60A0"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(0),
      ADR2 => address(5),
      ADR3 => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000302_35
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000301_33,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_32,
      SEL => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_f5_34
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000301 : X_LUT4
    generic map(
      INIT => X"7EEE"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(3),
      ADR2 => address(1),
      ADR3 => address(0),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000301_33
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030 : X_LUT4
    generic map(
      INIT => X"DAAA"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(3),
      ADR2 => address(1),
      ADR3 => address(0),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000030_32
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f6 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000282_31,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f5_30,
      SEL => address(1),
      O => preset_time_14_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000282 : X_LUT4
    generic map(
      INIT => X"0090"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(3),
      ADR2 => address(4),
      ADR3 => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000282_31
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000281_28,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f51_29,
      SEL => address(5),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028_f5_30
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000281 : X_LUT4
    generic map(
      INIT => X"4980"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(3),
      ADR2 => address(0),
      ADR3 => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000281_28
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000028 : X_LUT4
    generic map(
      INIT => X"80FF"
    )
    port map (
      ADR0 => address(4),
      ADR1 => address(2),
      ADR2 => address(3),
      ADR3 => address(0),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom00001613
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f6 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f51,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f5_23,
      SEL => address(5),
      O => preset_time_12_Q
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f5_0 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000243_25,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000242_24,
      SEL => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f51
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000243 : X_LUT4
    generic map(
      INIT => X"682A"
    )
    port map (
      ADR0 => address(3),
      ADR1 => address(0),
      ADR2 => address(1),
      ADR3 => address(2),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000243_25
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000242 : X_LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      ADR0 => address(0),
      ADR1 => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000242_24
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f5 : X_MUX2
    port map (
      IA => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom0000241,
      IB => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_21,
      SEL => address(4),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_f5_23
    );
  t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024 : X_LUT4
    generic map(
      INIT => X"80FF"
    )
    port map (
      ADR0 => address(2),
      ADR1 => address(0),
      ADR2 => address(3),
      ADR3 => address(1),
      O => t_preset_U0_xst_options_dist_mem_inst_gen_rom_rom_inst_Mrom_spo_int_rom000024_21
    );
  t_preset_XST_GND : X_ZERO
    port map (
      O => preset_time_15_Q
    );
  lcd_e_OBUF : X_OBUF
    port map (
      I => lcd_e_OBUF_790,
      O => lcd_e
    );
  lcd_rs_OBUF : X_OBUF
    port map (
      I => lcd_rs_OBUF_792,
      O => lcd_rs
    );
  lcd_rw_OBUF : X_OBUF
    port map (
      I => N0,
      O => lcd_rw
    );
  sf_d_0_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(0),
      O => sf_d(0)
    );
  sf_d_1_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(1),
      O => sf_d(1)
    );
  sf_d_2_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(2),
      O => sf_d(2)
    );
  sf_d_3_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(3),
      O => sf_d(3)
    );
  sf_d_4_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(4),
      O => sf_d(4)
    );
  sf_d_5_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(5),
      O => sf_d(5)
    );
  sf_d_6_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(6),
      O => sf_d(6)
    );
  sf_d_7_OBUF : X_OBUF
    port map (
      I => lcd_cntrl_inst_sf_d(7),
      O => sf_d(7)
    );
  NlwBlock_stopwatch_VCC : X_ONE
    port map (
      O => VCC
    );
  NlwBlock_stopwatch_GND : X_ZERO
    port map (
      O => GND
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

