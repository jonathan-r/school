#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>

#include "huffman.h"


uint8_t* huffman_encode(uint8_t str[], hcode hcodes[], int* encoded_bytes){ //changed it to return the amount of bytes
	//first compute the length
	int length = 0;
	uint8_t* c = str;
	
	do{
		length += hcodes[*c].len;
	} while(*c++); //include the null terminator
	
	int bytes = length / 8;
	if (length & 0x7) bytes++;
	*encoded_bytes = bytes;
	
	//allocate the buffer to hold the entire encoded string
	uint8_t* out = malloc(bytes);
	memset(out, 0, bytes);
	
	c = str;
	uint8_t* o = out; //output iterator
	int b = 0; //how many bits of o were filled
	do{
		unsigned data = hcodes[*c].data;
		int rem = hcodes[*c].len;
		
		while (rem > 0){
			if (b + rem >= 8){
				//will fill the current byte
				*o |= (data >> (rem - 8 + b)) & (0xFF >> b);//((1 << (8 - b)) - 1);
				rem -= 8 - b;
				
				o++;
				b = 0;
			}
			
			else{
				//will not fill the current byte, always the final fraction
				*o |= (data & (0xFF >> (8 - rem))) << (8 - (b + rem));
				b += rem;
				/*if (b == 8) {
					b = 0;
					o++;
				}*/
				rem = 0;
			}
		}
	} while(*c++);
	
	return out;
}
