

method findMax(s: seq<int>) returns (lo: int)
    requires |s| >= 0
{
    lo := 0;
    var hi := |s|-1;

    assert 0 <= hi - lo + 1;

    while (lo < hi)
        invariant 0 <= lo <= |s|
        invariant -1 <= hi < |s|
        decreases hi - lo + 1
    {
        if (s[lo] <= s[hi]) {
            lo := lo + 1;
        } else {
            hi := hi - 1;
        }
    }
}

