;r9 -> global int g = 256;
;r10/r26 - r15/r30 -> args
;r15/r31 -> link (return) address

init:
	add   r0, 256, r9
	ret   r31, 0

;r26 -> i
;r27 -> j
;r1 <- ((i + j) << 2) - 1
p:
	add   r26, r27, r16
	sll   r16, 2, r16
	ret   r25               ;delayed return
	sub   r16, 1, r1
	ret   r31, 0
	add   r0, r0, r0        ;no-op

;r26 -> i
;r1 <- p(g, -i)
q:
	subr  r26, 0, r10
	add   r9, 0, r11
	callr p, r15
	add   r0, r0, r0        ;no-op
	ret   r31, 0
	add   r0, r0, r0        ;no-op

;r26 -> x
;r27 -> y
;r1 <- x * y
mult:
	...

;r26 -> n
;r1 <- n!
f:
	add   r26, r0, r0 {c}
	jmp   ge, f_else
	add   r0, #1, r1        ;inoffensive on branch
	ret   r31, 0
	add   r0, r0, r0        ;no-op
f_else:
	sub   r26, #1, r10
	callr f, r15
	add   r0, r1, r11
	callr mult, r15
	add   r0, r0, r0        ;no-op

;r26 -> x
;r27 -> y
;r1 <- result
ackermann:
	add   r0, r27, r0 {c}
	jmp   ne, ackermann_else_if
	add   r27, #1, r1       ;will be rewritten on branch
	ret   r31, 0
ackermann_else_if:
	add   r0, r27, r0 {c}   ;inoffensive after ret
	jmp   ne, ackermann_else
	sub   r26, #1, r26       ;tail call
	add   r27, #1, r27
	jmp   alw, ackermann_tail

ackermann_else:
	add   r0, r10, r16      ;save x-1, inoffensive after callr
	add   r0, r26, r10      ;x
	sub   r27, #1, r11      ;y-1
	callr ackermann, r15
	add   r0, r1, r11       ;result of above as 2nd arg, inoffensive
	add   r0, r16, r10
	callr ackermann, r15
	add   r0, r0, r0        ;can't do anthing useful after window switch
	ret   r31, 0
