	.file	"sort.c"
	.intel_syntax noprefix
	.text
	.globl	sort
	.type	sort, @function
sort:
.LFB0:
	.cfi_startproc
	push	ebp                      #save clobbered register
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	push	edi                      #ditto
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	push	esi                      #same
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	push	ebx                      #again
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	sub	esp, 4                   #allocate four bytes on the stack, to keep a copy of (size - 1) and not have to recompute every iteration
	.cfi_def_cfa_offset 24
	mov	ebp, DWORD PTR [esp+24]  #load array pointer argument off the stack
	mov	eax, DWORD PTR [esp+28]  #load size argument off the stack
	sub	eax, 1                   #subtract, as per for condition
	mov	DWORD PTR [esp], eax     #place the value  (size-1) on the stack
	test	eax, eax                 #check(bitwise and) if (size-1) <= 0
	jle	.L1                      #success, there are one or no elements in the array, so nothing to do, return
	mov	edx, 0                   #loop counter i is optimized out, kept in edx; initialize to 0
	jmp	.L3                      #go to the while condition

.L7:
	mov	DWORD PTR [esi], ecx     #store value of (i+1)th element into the (i)th element
	mov	DWORD PTR [edi], ebx     #store value of the (i)th element in to the (i+1)th element
	sub	edx, 1                   #decrement i, line 7
	mov	edi, eax                 #put (i)th element into edi
	mov	ecx, DWORD PTR [eax]     #load value of the (i)th element  (silly?)
	lea	esi, [eax-4]             #compute pointer of the (i-1)th element
	mov	ebx, DWORD PTR [eax-4]   #load value of the (i-1)th element
	cmp	ecx, ebx                 #compare the value of (i)th and (i-1)th element (inline the while condition here)
	jle	.L4                      #fail, go to end of the loop
	mov	eax, esi                 #
	cmp	edx, -1                  #check if i is -1
	jne	.L7                      #if not (we're still in the valid range) do the replacement again
	add	edx, 1                   #otherwise, increment i (do the for loop increment)
	jmp	.L3                      #end of the loop

.L4: #for loop increment and condition check
	add	edx, 1                   #increment i
	cmp	edx, DWORD PTR [esp]     #compare i with (size-1)
	jge	.L1                      #return if the condition fails

.L3: #while condition
	lea	eax, [4+edx*4]           #compute offset of the (i+1)th element
	lea	edi, [ebp+0+eax]         #compute pointer of the (i+1)th element
	mov	ecx, DWORD PTR [edi]     #load value of the (i+1)th th element into ecx
	lea	esi, [ebp-4+eax]         #compute pointer of the (i)th element
	mov	ebx, DWORD PTR [esi]     #load value of the (i)th element into ebx
	cmp	ecx, ebx                 #compare the values of the (i+1)th and (i)th elements (1st part of the while condition)
	jle	.L4                      #on fail, go to ??
	test	edx, edx                 #see if i is 0
	js	.L4                      #when i is negative, go to ??
	mov	eax, esi                 #put (i)th element pointer into eax
	jmp	.L7                      #go to while body
.L1:
	add	esp, 4                   #free the stack allocation
	.cfi_def_cfa_offset 20
	pop	ebx                      #restore some registers
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	pop	esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	pop	edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	pop	ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret                              #return to caller
	.cfi_endproc
.LFE0:
	.size	sort, .-sort
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
