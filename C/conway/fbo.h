#ifndef FBO_H
#define FBO_H

#include <cassert>

#include <GL/glew.h>

#include "texture.h"
#include "gl_error.h"

class Framebuffer{
public:
	Framebuffer(int width, int height, Texture* colorOutput){
		assert(width == colorOutput->getWidth() and height == colorOutput->getHeight());
		
		depthWidth = width;
		depthHeight = height;
		color = colorOutput;
		
		glGenFramebuffers(1, &ob);
		bind();
		
		//attach a depth buffer
		glGenRenderbuffers(1, &depthRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, depthWidth, depthHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
		
		//attach the texture, to complete the framebuffer
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color->getId(), 0, 0); //glFramebufferTexture is null for some reason, in lg12
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color->getId(), 0);
		
		GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, draw_buffers);
		
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE){
			fprintf(stderr, "could not make the framebuffer\n");
			exit(1);
		}
		
		GLerror::list("framebuffer_constructor");
	}
	
	//texture must be resized too
	void resizeDepthBuffer(int width, int height){
		depthWidth = width;
		depthHeight = height;
		
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, depthWidth, depthHeight);
		GLerror::list("fbo_resize");
	}	
	
	void attachTexture(Texture* tex){
		color = tex;
		bind();
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color->getId(), 0, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color->getId(), 0);
		bindDefault();
		GLerror::list("fbo_attach_texture");
	}
	
	void bind(){
		//depth buffer and texture must be the same size, maybe, probably
		assert(depthWidth == color->getWidth() and depthHeight == color->getHeight());
		
		if (boundFramebuffer != ob){
			boundFramebuffer = ob;
			glBindFramebuffer(GL_FRAMEBUFFER, boundFramebuffer);
		}
	}
	
	static void bindDefault(){
		if (boundFramebuffer != 0){
			boundFramebuffer = 0;
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
	}	

private:
	static GLuint boundFramebuffer; //= 0

	GLuint ob;
	GLuint depthRenderBuffer;
	Texture* color;
	
	//size of the depth buffer
	int depthWidth;
	int depthHeight;
};

#endif //FBO_H
