#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

//letter to key translation
char to_digit[256];


int isletter(char c){
	return isalpha(c);
}

char case_insensitive(char c){
	if (c < 'a') return c + ('a' - 'A');
	else return c;
}

//O(m), where m is the average length a word
int word_cmp(const void* left, const void* right){ 
	const char* a = *((const char**)left);
	const char* b = *((const char**)right);
	
	int len_a = strlen(a);
	int len_b = strlen(b);
	
	//favor length over content
	if (len_a < len_b) return -1;
	else if (len_a > len_b) return 1;
	else{
		//compare the digit sequences needed to produce these words
		for (int i = 0; i < len_a; i++){
			int diff = to_digit[a[i]] - to_digit[b[i]];
			if (diff != 0) return diff;
		}
	}
	
	return 0; //they're completely the same
}

//convert a word in the sequence of keys needed to type it
void make_digits(char* to, char* word){
	while(*word) *to++ = to_digit[*word++];
	*to = '\0';
}

int main(int argc, char** argv){
	//initialize letter to key translation
	to_digit['a'] = to_digit['b'] = to_digit['c'] = '2';
	to_digit['d'] = to_digit['e'] = to_digit['f'] = '3';
	to_digit['g'] = to_digit['h'] = to_digit['i'] = '4';
	to_digit['j'] = to_digit['k'] = to_digit['l'] = '5';
	to_digit['m'] = to_digit['n'] = to_digit['o'] = '6';
	to_digit['p'] = to_digit['q'] = to_digit['r'] = to_digit['s'] = '7';
	to_digit['t'] = to_digit['u'] = to_digit['v'] = '8';
	to_digit['w'] = to_digit['x'] = to_digit['y'] = to_digit['z'] = '9';

	if (argc == 2){
		FILE* f = fopen(argv[1], "rb");

		if (f){
			fseek(f, 0, SEEK_END);
			long end = ftell(f);

			fseek(f, 0, SEEK_SET);
			long size = end - ftell(f);

			char* text = malloc(size + 1); //the entire file could be a single word, and it needs a NULL terminator
			fread(text, 1, size, f); //assumed to succeed
			fclose(f);
			
			//first count the number of words, since pointers are big
			int word_count = 0;
			int longest = 0;
			
			char* c = text;
			int wl = 0; //word length
			while (c != text + size + 1){
				if (isletter(*c)){
					*c = case_insensitive(*c); //make everything lowercase
					wl++;
				}
				else{
					if (wl > 0){ //word present
						if (wl > longest) longest = wl;

						*c = '\0'; //NULL terminate the word
						wl = 0; //look for the next word
						word_count++;
					}
				}
				
				c++;
			}
			
			if (word_count > 1){ //at least two words are needed
				//now store each word in a pointer array
				char** words = malloc(word_count * sizeof(char*));
				char** w = words;
			
				c = text;
				int word_added = 0;
				while (c != text + size){
					if (isletter(*c)){
						if (!word_added){
							*w++ = c;
							word_added = 1;
							//printf("found word %s\n", c);
						}
					}
					else word_added = 0;
					
					c++;
				}
			
				//now sort the words based on length and key sequence
				//O(n *log(n) * m), where n is the number of words, and m is the average length of a word
				qsort(words, word_count, sizeof(char*), word_cmp);
				
				//now go through every word and see if its sequence matches the previous one
				char* last_sequence = malloc(longest + 1);
				char* sequence = malloc(longest + 1);
			
				make_digits(last_sequence, words[0]);
				int last_len = strlen(words[0]);
			
				int first_printed = 0; //whether the first in a group of matching words was printed
			
				for (int i = 1; i < word_count; i++){
					make_digits(sequence, words[i]);
				
					if (strcmp(words[i], words[i - 1]) != 0){ //the words must be different
						if (strcmp(sequence, last_sequence) == 0){
							if (!first_printed){
								printf("----\n%s : %s\n", sequence, words[i - 1]);
								first_printed = 1;
							}
							printf("%s : %s\n", sequence, words[i]);
						}
				
						else{
							first_printed = 0;
						}
				
						//swap sequence and last_sequence
						char* tmp = sequence;
						sequence = last_sequence;
						last_sequence = tmp;
					}
				}
			
				printf("---\n");
				
				free(sequence);
				free(last_sequence);
				free(words);
			}
			
			free(text);
					
		}

		else{
			printf("could not open file\n");
			return -1;
		}

	}

	else{
		printf("usage: %s <file>\n", argv[0]);
	}

	return 0;
}
