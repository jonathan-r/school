
public class Symbols{
	//each packet coming from the sender contains a 5 byte header, followed by any amount of data
	//                  0     1                 2                3               4              5 
	//packet structure: [type][sequence id high][sequence id low][sender id high][sender id low][data][data]... each a byte long
	
	//numbers are transmitted as big endian

	//types
	public static final byte PACKET_SENDER_INIT = 0; //data: destination port(2 bytes), window size(2 bytes) and as many 0xFF bytes thereafter
	public static final byte PACKET_FILE_HEADER = 1;
	public static final byte PACKET_FILE_DATA = 2;
	
	public static final byte PACKET_ACK_INIT = 3;
	public static final byte PACKET_ACK_SEQ = 4;
	
	//the init ack contians the maximum mtu the link permits as data
	//the other ack has no extra data

	public static final int PACKET_HEADER_SIZE = 5;
	public static final int PACKET_SIZE = 1500;
	public static final int MAX_WINDOW_SIZE = 256; //since the sequence id is a single byte
	public static final int DEFAULT_RECEIVER_PORT = 1541;
	public static final int DEFAULT_WINDOWS_SIZE = 20;

	public static int bytesToShort(byte high, byte low){
		int highInt = ((int)high & 0xFF) << 8;
		int lowInt = (int)low & 0x00FF;
		return highInt | lowInt;
	}
	
	public static void writeShortToBytes(int value, byte[] bytes, int offset){
		bytes[offset] = (byte)(value >> 8);
		bytes[offset+1] = (byte)(value & 0xFF);
	}
}

