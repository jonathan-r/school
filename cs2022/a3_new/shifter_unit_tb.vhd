library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity shifter_unit_tb is
end shifter_unit_tb;
 
architecture behavior of shifter_unit_tb is 
	component shifter_unit is
		port ( input : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0) );
	end component;

	signal input : std_logic_vector (15 downto 0) := (others => '0');
	signal op :  std_logic_vector (1 downto 0) := (others => '0');
   signal output :  std_logic_vector (15 downto 0) := (others => '0');
	
begin
	uut: shifter_unit port map(input, op, output);

	stim_proc: process begin
		input <= x"abcd"; --1010 1011 1100 1101
	
		--pass through
		op <= "00";
		wait for 10ns;
		assert output = x"abcd";
		
		--shift right
		op <= "01";
		wait for 10ns;
		assert output = x"55e6";  --0101 0101 1110 0110
		
		--shift left
		op <= "10";
		wait for 10ns;
		assert output = x"579a";  --0101 0111 1001 1010
		
		wait;
	end process;
end;
