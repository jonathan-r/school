from matplotlib import pyplot
from random import random
import json, sys, math


#db = json.loads(open("meansd.json").read())
db = json.load(sys.stdin)
#xs = []
#ys = []
#cs = []

c1 = (0.6, 0.6, 1.0)

cols = []
md = 2.5 / len(db)
for i in range(len(db)):
  while True:
    r = 0.3 + random() * 0.7
    g = 0.3 + random() * 0.7
    b = 0.3 + random() * 0.7
    good = True
  
    for c2 in cols:
      d = math.sqrt((r - c2[0])**2 + (g - c2[1])**2 + (b - c2[2])**2)
      if d < md:
        good = False
        break
    
    if good:
      cols.append((r, g, b))
      break

print len(db)
print '\n'.join(map(lambda x : "%.2f %.2f %.2f" % (x[0], x[1], x[2]), cols))

plt = []

for i, im in enumerate(db):
  xs = []
  ys = []
  ms = []
  for slice in db[im]:
    xs.append(slice["sd"])
    ys.append(slice["mean"])
    ms.append('x' if slice["label"] else 'o')
  p = pyplot.scatter(xs, ys, c=cols[i], s=50)
  plt.append(p)

#p = pyplot.scatter(xs, ys, c=cs, s=50)
pyplot.xlabel("Standard Deviation")
pyplot.ylabel("Mean")
pyplot.legend(plt, list(db.keys()), scatterpoints=1)
pyplot.show()
