#include <stdio.h>

unsigned int str2int(char* str, int max_length, int* success){
	int s = 0;
	int valid = 0;
	int read = 0;
	const int max_read = sizeof(unsigned int) >> 4;

	unsigned int value = 0;

	while (*str == ' ' && s < max_length){
		str++; s++;
	}

	if (s < max_length){
		while (s < max_length){
			char c = *str;
			int v = 0;
			if (c >= '0' && c <= '9') v = c - '0';
			else if (c >= 'A' && c <= 'F') v = c - 'A';
			else if (c >= 'a' && c <= 'a') v = c - 'a';
			else break;

			valid = 1;
			value = (value << 4) | v;
			read++;

			if (read >= max_read) {
				valid = 0;
				break;
			}
		}
	}

	if (success) *success = valid;
	return value;
}

int main(int argc, char** argv){
	const int max = 256;
	char buffer[max];

	printf("type a number\n");
	fgets(buffer, max, stdin);

	int success;
	int val = str2int(buffer, max, &success);

	if (success) printf("valid:\n%d\n", val);
	else printf("invalid\n");

	return 0;
}
