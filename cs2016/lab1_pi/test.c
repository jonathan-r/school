#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <pthread.h>

void* entryPoint(void* data){
	sharedPrintf("hello from %X\n", (int)pthread_self());
	pthread_exit(NULL);
}

int main(){
	const int num = 10;
	pthread_t threads[num];

	for (int i = 0; i < num; i++){
		int err = pthread_create(&threads[i], NULL, entryPoint, NULL);
	}

	for (int i = 0; i < num; i++){
		pthread_join(threads[i], NULL);
	}

	pthread_exit(NULL);
}