#include "median_bg.h"

#include <stdlib.h> //rand
#include <math.h> //M_PI

#include <algorithm> //min, max

using namespace std;
using namespace cv;

int rectAreaIntersect(const Rect& r1, const Rect& r2) {
	//http://math.stackexchange.com/a/99576
	Point r1s = r1.tl(); Point r2s = r2.tl();
	Point r1e = r1.br(); Point r2e = r2.br();
	int xo = max(0, min(r1e.x, r2e.x) - max(r1s.x, r2s.x));
	int yo = max(0, min(r1e.y, r2e.y) - max(r1s.y, r2s.y));
	return xo * yo;
}

struct Chain {
	Point2f center;
	Rect bb; //bounding box

	int frameStarted; //frame index
	float timeStarted; //video time

	bool connected;
	bool confirmed;
	int obIdx;
	Scalar color; //debug only

	Chain(const Point2f& c, Rect b, int fr, float ti):
		center(c), bb(b), frameStarted(fr), timeStarted(ti) {
		connected = true; //prevent removal after the first frame
		confirmed = false;
		color = Scalar(rand() & 0xFF, rand() & 0xFF, rand() & 0xFF);
	}
};

struct ObRecord {
	int frameAbandoned;
	int framePickUp;
	Rect bounds;
	ObRecord() {
		frameAbandoned = framePickUp = -1;
	}
};

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("usage: objects <video_file>\n");
		return 0;
	}

	namedWindow("objects", WINDOW_AUTOSIZE);

	VideoCapture videoSource;
	videoSource.open(argv[1]);
	if (!videoSource.isOpened()) {
		fprintf(stderr, "could not open video file %s\n", argv[1]);
		return 1;
	}

	int frameCount = 0;
	double fps = videoSource.get(CV_CAP_PROP_FPS);
	double frameTime = 1.0/fps; //in seconds
	double elapsedTime = 0.0; //seconds
	double lastProcess = 0.0;

	Mat firstFrame, currentFrame;
	videoSource >> currentFrame;
	currentFrame.copyTo(firstFrame);

	MedianBackground medbg(firstFrame, 1.1f, 1);

	Mat bgSlowAverage;
	firstFrame.convertTo(bgSlowAverage, CV_32F);

	vector<vector<Point>> contours;
	vector<Chain> chains;

	float minSize = firstFrame.cols * 0.03f;
	float minArea = minSize * minSize;
	float minAreaFrac = 0.7f;
	float minTimeChained = 3.0f; //seconds

	int objectsFound = 0;
	vector<ObRecord> objects;

	while (elapsedTime < 2.0) {
		//use the first 2 seconds of video for background detection
		accumulateWeighted(currentFrame, bgSlowAverage, 0.01);
		videoSource >> currentFrame;
		elapsedTime += frameTime;
		frameCount++;
	}

	bgSlowAverage.convertTo(firstFrame, CV_8UC3);

	while (!currentFrame.empty()) {
		//process every 100ms video time
		double timeDiff = elapsedTime - lastProcess;
		if (timeDiff > 0.1) { //every 100ms
			medbg.UpdateBackground(currentFrame);

			Mat bgFrame = medbg.GetBackgroundImage();
			Mat bgAvgDiff, bgAvgDiffGray, diffThr;

			absdiff(firstFrame, bgFrame, bgAvgDiff);
			erode(bgAvgDiff, bgAvgDiff, Mat(), Point(-1, -1), 3); //remove high frequency noise
			dilate(bgAvgDiff, bgAvgDiff, Mat(), Point(-1, -1), 8); //join close sections
			cvtColor(bgAvgDiff, bgAvgDiffGray, CV_BGR2GRAY);
			threshold(bgAvgDiffGray, diffThr, 30, 255, THRESH_BINARY);

			Mat hierarchy; //throw-away
			contours.clear();
			findContours(diffThr, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

			//reset connectivity flag
			for (auto& chain : chains) {
				chain.connected = false;
			}

			//try to connect the contour with the previous contours
			for (auto& contour : contours) {
				Rect rect = boundingRect(contour);
				Point2f center = (Point2f(rect.tl()) + Point2f(rect.br())) / 2;
				float area = rect.area();

				if (area > minArea) {
					//find the closest one in the previous frame
					int bestIdx = -1;
					float bestArea = 0.0f;
					for (int j = 0; j < chains.size(); j++) {
						if (!chains[j].connected) {
							float areaOverlap = rectAreaIntersect(rect, chains[j].bb);
							if (areaOverlap > bestArea) {
								bestArea = areaOverlap;
								bestIdx = j;
							}
						}
					}

					float frac = bestArea / area;
					if (bestIdx != -1 and frac > minAreaFrac) {
						//chain together with the last one - update center and radius
						Chain& ch = chains[bestIdx];
						ch.center = center;
						ch.bb = rect;
						ch.connected = true;

						if (!ch.confirmed and (elapsedTime - ch.timeStarted) > minTimeChained) {
							ch.confirmed = true;
							ch.obIdx = objectsFound++;
							objects.emplace_back();
							objects.back().frameAbandoned = ch.frameStarted;
							objects.back().bounds = ch.bb;

							printf("abandoned object %d - frame %d, (%d, %d) to (%d, %d)\n",
								ch.obIdx, ch.frameStarted, rect.tl().x, rect.tl().y, rect.br().x, rect.br().y);

						}
					}
					else {
						//start new chain
						chains.emplace_back(center, rect, frameCount, elapsedTime);
						//printf("stared chain - frame %d, at (%.2f %2f)\n",
						//	frameCount, center.x, center.y);
					}
				}
			}

			//remove unconnected chains
			for (auto it = chains.begin(); it != chains.end();) {
				if (!it->connected) {
					if (it->confirmed) {
						printf("picked up object %d - frame %d\n", it->obIdx, frameCount);
						objects[it->obIdx].framePickUp = frameCount;
					}
					it = chains.erase(it);
				}
				else it++;
			}

			
			
			lastProcess = elapsedTime;
		}

		//pretty pictures
		for (auto& ch : chains) {
			if (ch.confirmed) {
				rectangle(currentFrame, ch.bb.tl(), ch.bb.br(), ch.color, 3);
				line(currentFrame, ch.bb.tl(), ch.bb.br(), ch.color, 3);
			}
			else {
				rectangle(currentFrame, ch.bb.tl(), ch.bb.br(), ch.color, 1);
			}
		}
		imshow("objects", currentFrame);

		char key = cvWaitKey(20);
		if (key == ' ') break;

		videoSource >> currentFrame;
		elapsedTime += frameTime;
		frameCount++;
	}

	if (objectsFound > 0) {
		ObRecord& ob = objects[0];
		if (ob.frameAbandoned >= 0 and ob.framePickUp >= 0) {
			int groundStart, groundEnd;
			Rect groundBound;

			if (strstr(argv[1], "ObjectAbandonmentAndRemoval1.avi")) {
				groundStart = 183;
				groundEnd = 509;
				groundBound = Rect(Point(356, 208), Point(390, 239));

			}
			else if (strstr(argv[1], "ObjectAbandonmentAndRemoval2.avi")) {
				groundStart = 215;
				groundEnd = 551;
				groundBound = Rect(Point(287, 261), Point(352, 323));
			}
			else return 0;

			double startDiff = (ob.frameAbandoned - groundStart) / fps;
			double endDiff = (ob.framePickUp - groundEnd) / fps;
			double dice = (2.0 * rectAreaIntersect(groundBound, ob.bounds)) / (groundBound.area() + ob.bounds.area());

			printf("start delay: %.2fs, end delay: %.2fs, dice coeff: %.2f\n", startDiff, endDiff, dice);
		}
	}

	return 0;
}