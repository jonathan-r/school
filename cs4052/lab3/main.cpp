#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <vector>
#include <iostream>
#include <fstream>

#include "gen_mesh.h"
#include "shader_program.h"
#include "vbo.h"
#include "vertex.h"

#include "maths/mat4.h"
#include "maths/vec.h"

#ifdef GLSL120
#define SHADER_ROOT_NAME "shaders/shader_120"
#else
#define SHADER_ROOT_NAME "shaders/shader_330"
#endif

int WW = 800;
int WH = 500;

using namespace std;


ShaderProgram* mainShader = NULL;
VertexArrayObject<vertex>* ropeModel = NULL;
VertexArrayObject<vertex>* arrowsModel = NULL;
VertexArrayObject<vertex>* gridModel = NULL;
vec3 translation;
vec3 scaling;
quat currentRot;

bool resetTransformation;
bool keysDown[256];
int lastDrawTime = 0;

const int ropePoints = 50;
const float ropeSize = 1.0f;
const float ropeWidth = 0.3f;
vertex ropeVerts[ropePoints];

void display() {
	int frameBeginTime = glutGet(GLUT_ELAPSED_TIME);
	lastDrawTime = frameBeginTime;
	float time = (float)frameBeginTime / 1000.0f;
	
	WW = glutGet(GLUT_WINDOW_WIDTH);
	WH = glutGet(GLUT_WINDOW_HEIGHT);
	//take keyboard input
	//update - TODO: move to idle function

	//translation
	const float trc = 0.3f;
	if (keysDown['w']) translation.y += trc;
	else if (keysDown['s']) translation.y -= trc;
	
	if (keysDown['a']) translation.x -= trc;
	else if (keysDown['d']) translation.x += trc;

	if (keysDown['q']) translation.z += trc;
	else if (keysDown['e']) translation.z -= trc;

	//scale input
	const float trs = 1.1f;
	if (keysDown['r']) scaling.x *= trs;
	else if (keysDown['f']) scaling.x /= trs;
	
	if (keysDown['t']) scaling.y *= trs;
	else if (keysDown['g']) scaling.y /= trs;
	
	if (keysDown['y']) scaling.z *= trs;
	else if (keysDown['h']) scaling.z /= trs;
	
	//rotation input
	vec3 rots(0.0f);
	const float change = 0.05f;
	if (keysDown['u']) rots.z = fmodf(rots.z - change, 2*M_PI);
	else if (keysDown['o']) rots.z = fmodf(rots.z + change, 2*M_PI);

	if (keysDown['i']) rots.x = fmodf(rots.x - change, 2*M_PI);
	else if (keysDown['k']) rots.x = fmodf(rots.x + change, 2*M_PI);

	if (keysDown['j']) rots.y = fmodf(rots.y - change, 2*M_PI);
	else if (keysDown['l']) rots.y = fmodf(rots.y + change, 2*M_PI);

	mat4 sca = mat4::scale(scaling);
	mat4 tra = mat4::translation(translation);
	mat4 model = tra * sca;

	//cout << matrix.represent() << endl;

	/*int stack[2];
	while (stack[0] < 2) {
		float comp = 8.0f;
		float phase = ri * M_PI + time * 0.1f;
		for (int vi = 0; vi < ropePoints; vi++) {
			float at = (float)vi / ropePoints;
			ropeVerts[vi].v.x = at * ropeSize;
			ropeVerts[vi].v.y = sinf(phase + at*comp) * ropeWidth;
			ropeVerts[vi].v.z = cosf(phase + at*comp) * ropeWidth;
		}
	}

	ropeModel->assign(ropePoints, ropeVerts); */

	GLuint prog = mainShader->getProgObj();
	glUseProgram(prog);
	
	GLuint matLoc = glGetUniformLocation(mainShader->getProgObj(), "matrix");
	GLuint lightFractionLoc = glGetUniformLocation(mainShader->getProgObj(), "lightFraction");
	GLuint timeLoc = glGetUniformLocation(mainShader->getProgObj(), "time");
	
	//actual display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUniform1f(timeLoc, time);
	
	float aspect = (float)WW / WH;
	for (int iy = 0; iy < 2; iy++) {
		int wy = iy * (WH/2);
		for (int ix = 0; ix < 2; ix++) {
			int wx = ix * (WW/2);
		
			glViewport(wx, wy, WW/2, WH/2);
			mat4 proj, view;

			view = mat4::viewLookAt(vec3(0.0f, 5.0f, 10.0f), vec3(0.0f), vec3(0.0f, 1.0f, 0.0f));

			switch (iy*2 + ix) {
				case 0: {
					//rotation
					vec3 p(10.0f * cosf(time), 5.0f, 10.0f * sinf(time));
					view = mat4::viewLookAt(p, vec3(0.0f), vec3(0.0f, 1.0f, 0.0f));
					proj = mat4::perspectiveProjection(45.0f, aspect, 0.1f, 1000.0f);
					break;
				}

				case 1: {
					proj = mat4::perspectiveProjection(70.0f, aspect, 0.1f, 1000.0f);
					break;
				}

				case 2: {
					//proj = mat4::orthographicProjection(-5.0f, 5.0f, -5.0f, 5.0f, -100.0f, 100.0f);
					proj = mat4::orthographicProjection(5.0f, (float)WW/WH, -100.0f, 100.0f);
					break;
				}
				
				case 3: {
					vec3 p(10.0f * cosf(time), 5.0f, 10.0f * sinf(time));
					view = mat4::viewLookAt(p, vec3(0.0f), vec3(0.0f, 1.0f, 0.0f));
					//proj = mat4::orthographicProjection(-5.0f, 5.0f, -5.0f, 5.0f, -100.0f, 100.0f);
					proj = mat4::orthographicProjection(5.0f, (float)WW/WH, -100.0f, 100.0f);
					break;
				}
			}

			//mat4 matrix = model;
			mat4 matrixScene = proj * view; //no model
			mat4 matrixModel = proj * view * model;

			//arrows, transformed
			glUniformMatrix4fv(matLoc, 1, GL_FALSE, matrixScene.data);
			arrowsModel->bind(prog);
			glUniform1f(lightFractionLoc, 0.0f);
			glDrawArrays(GL_LINES, 0, arrowsModel->getVerticesAssigned());
			
			//draw grid
			gridModel->bind(prog);
			glDrawArrays(GL_LINES, 0, gridModel->getVerticesAssigned());
			
			//full light affect, main model tranformed
			glUniformMatrix4fv(matLoc, 1, GL_FALSE, matrixModel.data);
			glUniform1f(lightFractionLoc, 1.0f);
			ropeModel->bind(prog);
			glDrawArrays(GL_TRIANGLES, 0, ropeModel->getVerticesAssigned());
		}
	}
	
	glutSwapBuffers();
	//int frameEndTime = glutGet(GLUT_ELAPSED_TIME);	
}

void idle() {	
	int time = glutGet(GLUT_ELAPSED_TIME);
	if (time - lastDrawTime > 16) glutPostRedisplay();
}

void keyboardDown(unsigned char key, int x, int y) {
	//cout << "key down: " << key << endl;
	keysDown[key] = true;
	if (key == ' ' and !resetTransformation) resetTransformation = true;
	else if (key == 0x1B) glutLeaveMainLoop();
}

void keyboardUp(unsigned char key, int x, int y) {
	//cout << "key up: " << key << endl;
	keysDown[key] = false;
}

void specDown(int key, int x, int y) {
	//keysDown[key] = true;
}

void specUp(int key, int x, int y) {
	//keysDown[key] = false;
}

void init()
{
	mainShader = new ShaderProgram(SHADER_ROOT_NAME ".vert", SHADER_ROOT_NAME ".frag");	
	ropeModel = new VertexArrayObject<vertex>();
	arrowsModel = new VertexArrayObject<vertex>();
	gridModel = new VertexArrayObject<vertex>();

	//generate cylinder
	vector<vertex> rope;
	generateCylinder(15, 20, vec3(10.0f, 2.0f, 2.0f), true, rope);
	ropeModel->assign(rope.size(), rope.data(), GL_STATIC_DRAW);

	//generate arrows - plain lines
	vertex arrows[6];
	arrows[0].v.set(0.0f, 0.0f, 0.0f); arrows[0].c.set(1.0f, 0.0f, 0.0f);
	arrows[1].v.set(1.0f, 0.0f, 0.0f); arrows[1].c.set(1.0f, 0.0f, 0.0f);
	arrows[2].v.set(0.0f, 0.0f, 0.0f); arrows[2].c.set(0.0f, 1.0f, 0.0f);
	arrows[3].v.set(0.0f, 1.0f, 0.0f); arrows[3].c.set(0.0f, 1.0f, 0.0f);
	arrows[4].v.set(0.0f, 0.0f, 0.0f); arrows[4].c.set(0.0f, 0.0f, 1.0f);
	arrows[5].v.set(0.0f, 0.0f, 1.0f); arrows[5].c.set(0.0f, 0.0f, 1.0f);
	arrowsModel->assign(6, arrows, GL_STATIC_DRAW);

	//generate grid
	const int gl = 10;
	vertex grid[4*gl];

	float gs = 5.0f;
	for (int i = 0; i < gl; i++) {
		float x = -gs + 2 * gs * (float)i / (gl-1);
		grid[i * 2 + 0].v.set(x, 0.0f, -gs);
		grid[i * 2 + 1].v.set(x, 0.0f,  gs);
		
		grid[2*gl + i * 2 + 0].v.set(-gs, 0.0f, x);
		grid[2*gl + i * 2 + 1].v.set( gs, 0.0f, x);
	}
	for (int i = 0; i < 4*gl; i++) {
		grid[i].c.set(1.0f, 1.0f, 1.0f);
		grid[i].n.set(0.0f, 1.0f, 0.0f);
	}
	gridModel->assign(4*gl, grid, GL_STATIC_DRAW);
	
	//reset state
	memset(keysDown, 0, 256);
	scaling.set(1.0f, 1.0f, 1.0f);
	translation.set(0.0f, 0.0f, 0.0f);

	glEnable(GL_CULL_FACE);
	glClearColor(0.3f, 0.4f, 0.2f, 1.0f);
	glClearDepth(1e5f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
}

int main(int argc, char** argv) {

	// Set up the window
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(WW, WH);
    glutCreateWindow("no more triangles");
	// Tell glut where the display function is
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboardDown);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(specDown);
	glutSpecialUpFunc(specUp);

	 // A call to glewInit() must be done after glut is initialized!
    GLenum res = glewInit();
	// Check for any errors
    if (res != GLEW_OK) {
      fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
      return 1;
    }
	// Set up your objects and shaders
	init();
	// Begin infinite event loop
	glutMainLoop();

	delete mainShader;
	delete ropeModel;
	delete arrowsModel;
	delete gridModel;

	cout << "graceful exit" << endl;
    return 0;
}

