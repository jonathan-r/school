import java.io.BufferedReader;
import java.io.InputStreamReader;

class BracketRemover {
	private static final int OP_ADD = 0;
	private static final int OP_SUB = 1;
	private static final int OP_DIV = 2;
	private static final int OP_MUL = 3;
	private static final int OP_EXP = 4;
	private static final int OP_BRACE = 5;
	private static final int OP_LIT = 6; //not an op, literal
	
	private static final char[] OP_SYMBOLS = {'+', '-', '/', '*', '^'};

	private static class TreeNode{
		private int op;
		private String lit;

		public TreeNode parent;
		public TreeNode left, right;

		public TreeNode(TreeNode parent){
			this.op = -1;
			this.lit = null;
			this.parent = parent;
			this.left = this.right = null;
		}

		public void setOp(int op){
			this.op = op;
		}

		private void setLit(String lit){
			this.op = OP_LIT;
			this.lit = lit;
		}

		public void addChild(TreeNode n){
			if (left == null) left = n;
			else right = n;
		}

		public TreeNode addChildBrace(){
			TreeNode n = new TreeNode(this); //it just passes through this node
			TreeNode m = new TreeNode(n);
			n.setOp(OP_BRACE);
			n.addChild(m);
			return m;
		}

		public void addChildLiteral(String lit){
			TreeNode n = new TreeNode(this);
			n.setLit(lit);
			if (left == null) left = n;
			else right = n;
		}
		
		public boolean isFull(){
			return (right != null);
		}

		public boolean isBrace(){
			return (op == OP_BRACE);
		}

		public void simplify(){
			//TODO
		}
		
		public String toString(){
			if (op == OP_BRACE){
				return "(" + left.toString() + ")";
			}
			
			else if (op == OP_LIT){
				return lit;
			}
		
			else {
				return left.toString() + OP_SYMBOLS[op] + right.toString();
			}
		}
	}

	public static String removeBrackets(String expr) {
		//tokenize and parse into a tree
		TreeNode node = new TreeNode(null);
		String literal = "";

		for (int i = 0; i < expr.length(); i++){
			char c = expr.charAt(i);

			if (literal.length() > 0 && !Character.isLetterOrDigit(c)){
				//end of a literal or number
				node.addChildLiteral(literal);
				literal = "";
			}

			else switch(c){
				case ' ': continue;
				case '(':
					node = node.addChildBrace();
					break;

				case ')':
					node = node.parent;
					if (node.isBrace()) node = node.parent;
					break;
				
				case '^':
					node.op = OP_EXP;
					break;
				
				case '+':
					node.op = OP_ADD;
					break;
				
				case '-':
					node.op = OP_SUB;
					break;
				
				case '*':
					node.op = OP_MUL;
					break;

				case '/':
					node.op = OP_DIV;
					break;

				default: //literal
					literal += c;
			}
			
			if (node.isFull()){
				TreeNode parent = node.parent;
				if (parent == null){
					parent = new TreeNode(null);
					parent.addChild(node);
				}
				
				node = parent;
			}
		}
		
		TreeNode parent = node.parent;
		while (parent != null){
			node = parent;
			parent = parent.parent;
		}
		
		node.simplify();
		return node.toString();
	}

	public static void main(String[] args) throws java.io.IOException{
		System.out.println("enter the expresison");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String expr = br.readLine();
		System.out.println(removeBrackets(expr));
	}
}
