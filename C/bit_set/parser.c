#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bitset.h"

bitset* parse_expression(char** tokens, int num_tokens, char** error_text){
	bitset* partial = NULL; //result so far
	bitset* read_to = NULL; //next set
	char op = 0; //last operator

	int error = 0;
	char* error_str = NULL;

	int read_set = 0; //state
	int has_op = 1; //the implied initial operator is an identity operator

	int i;
	for (i = 0; i < num_tokens && !error; i++){
		char* token = tokens[i];
		int len = strlen(token);
		int other = 0;

		if (len == 1){
			char c = *token;
			switch(c){
				case '[':
					if (has_op && !read_set){
						read_set = 1;
						has_op = 0;
						read_to = bitset_new();
					}
					else{
						error_str = "Expected operator, not set";
						error = 1;
					}
					break;

				case ']':
					if (read_set){
						//now do the operation
						if (op == 0){
							//this occurs for the first set
							partial = read_to;
							read_to = NULL;
						}

						else{
							//for subsequent ones
							bitset* result = bitset_new();
							if (op == 'u') bitset_union(result, partial, read_to);
							else if (op == 'n') bitset_intersect(result, partial, read_to);
							else if (op == '-') {
								/*int j, k, l, n;
								for (j = 0; j < BITSET_PAGES; j++){
									bitset_page* page = read_to->pages[j];
									if (page){
										for (k = 0; k < BITSET_PAGE_SIZE; k++){
											uint32_t data = page->data[k];
											for (l = 0; l < 32; l++){
												n = (j << BITSET_PAGE_INDEX_SHIFT) | (k << 5) | (1 << l);
												if (data & (1 << l)) bitset_remove(partial, read_to->array[j]);
											}
										}
									}
								}*/
							}

							bitset_destroy(read_to);
							read_to = NULL;

							if (op != '-'){
								bitset_destroy(partial);
								partial = result;
							}
						}

						has_op = 0;
						read_set = 0;
					}
					break;

				case 'u':
				case 'n':
				case '-':
					if (!read_set){
						if (!has_op){
							op = c;
							has_op = 1;
						}

						else {
							error_str = "Expected set, not operator";
							error = 1;
						}
					}
					else {
						error_str = "Operator inside set";
						error = 1;
					}
					break;

				default:
					other = 1;
					break;
			}
		}
		else other = 1;

		if (other){
			if (read_set){
				char* end;
				int elem = strtol(token, &end, 10);
				if (end != token + len){
					error_str = "Invalid expected number in set";
					error = 1;
				}

				else{
					bitset_add(read_to, elem);
				}
			}
		}
	}

	if (has_op && !error){ //if there is already an error, don't suppress it
		error_str = "Expected set after operator";
		error = 1;
	}

	if (error){
		//cleanup
		if (partial) bitset_destroy(partial);
		if (read_to) bitset_destroy(read_to);

		if (error_text) *error_text = error_str;
		return NULL;
	}

	return partial;
}

int main(int argc, char** argv){
	if (argc == 1){
		printf("A set is defined as \"[ 0 2 54 ... ]\"\n");
		printf("The operators '-', 'u' and 'n' mean difference union and intersect, respectively.\n");
		printf("Use brackets '(' and ')', in order to use more than one operator. The parser is dumb\n");
		printf("\nusage %s: <set expression>\n\n", argv[0]);
	}

	else{
		char* error = NULL;
		bitset* result = parse_expression(&argv[1], argc - 1, &error);
		if (result){
			printf("[ ");
			int i, k, b, n;
			for (i = 0; i < BITSET_PAGES; i++){
				bitset_page* page = result->pages[i];
				if (page){
					for (k = 0; k < BITSET_PAGE_SIZE; k++){
						uint32_t data = page->data[k];
						for (b = 0; b < 32; b++){
							if (data & (1 << b)){
								n = (((i << BITSET_PAGE_BITS) | k) << 5) + b;
								printf("%d ", n);
							}
						}
					}
				}
				
			}
			printf("]\n");
			
			bitset_destroy(result);
		}

		else if (error){
			printf("Error: %s\n", error);
		}
	}

	return 0;
}
