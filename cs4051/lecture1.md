# Human factors

## Lecture 1

Dumb phone to smartphone

- more featues
- unified app system
- started as prestice device / status symbol
- 'natural' interface
- discoverable, exploration
- wifi, mobile internet connectivity
- more feature complete browser (closer to desktop)
- touch/mouse interface for more complex UI
- first successful handheld computer (tried before, see IBM Simon Phone)

Reasons for studying/applying HCI:

- useful - accomplishes useful goal
- usable - safe to use
- used - chance for market share

Combines psychology with the design of technology
draws for many disciplines

### Usability
The system may work correctly but the interface is important too.
Human error may be caused by the interface.
Systems may be broken by design.
Users think of the interface *as the system*.

examples of bad user experiences:

- it's hard to tell how to accomplish a certain task.
- it's easy to make the same error repeatedly.
- easy to lose work, commit grave mistake.
- system does somting annoying automatically (defaults, etc).
- aesthetics.

UI/UX is an integral part of the system, not something bolted on at the end.

Goals:

- effective to use
- efficient to use
- safe to use
- easy to learn
- easy to retain knowledge of

### Interaction design
must take into account:

- target audience (knowledge, skills)
- activities carried out (how the system is used)
- what environment the interaction takes place (where the system is used)

interactions must be optimized to matc hthe users' activities and needs.

example: CLI may efficient to use but hard to learn and retain.

### Factors affecting design
Human:

- physical capabilities
- cognitive capabilities
- social, cultural influences
- cognitive load allowence (stressful environment)
- other users

Environmental factors:

- ligthing
- noise
- time pressure
- need for sterilized equipment

### Users
Skills, knowledge, education, role vary quite a lot.
Consider frequency of use.
Consider what task/reponsibilities to other peole (expert/admin vs client user)


### Understanding users' needs
(see chaos reports for failures of the '80s)

- consider what people and good and bad at, 
- the way they currently do things, 
- listen to opinions
- use tried and tested user-based methods.

users must be involved (thought of) during development, as systems are necessarily used in order to be successful.

needless features may be implemented.

Fitts' listed in 1951 the things humans are better at than machines (outdated somewhat). Of note, humans are better at flexible procedures, reason inductively and exercise judgement/ethics. Also listed are the opposite, of note precision, repetition, speed, cognitive load due to storage, lack of error (sans programming errors).

### Reading
(main one) Interaction Design 3rd/4th, Rogers, Sharp, Preece
HCI 3rd, Finlay, Abowd, Beale
Designing Interactive System 2nd, Benion
Sketching used experiences, Buxton


