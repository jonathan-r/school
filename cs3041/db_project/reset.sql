--drop everything
drop table VehicleClasses cascade constraints;
drop table OriginalCapacities cascade constraints;
drop table Vehicles cascade constraints;
drop table RefittedCapacities cascade constraints;
drop table Drivers cascade constraints;
drop table DriverQualifications cascade constraints;
drop table Stations cascade constraints;
drop table StationTerminals cascade constraints;
drop table ShipmentsWaiting cascade constraints;
drop table Shipments cascade constraints;
drop table ShipmentContents cascade constraints;
drop table ServiceRecord cascade constraints;

--create tables
create table VehicleClasses (
	id integer not null,
	type varchar(10) not null check(type in ('road', 'rail', 'air', 'sea')),
	make varchar(20) not null,
	model varchar(20) not null,
	date_introduced date not null,
	current_price numeric(20, 2),
	still_considered char(1) not null check(still_considered in ('y', 'n')),
	primary key(id)
);

create table OriginalCapacities (
	class_id integer not null,
	type varchar(20) not null check(type in ('passengers', 'boxed', 'liquid', 'powder', 'valuables')),
	quantity integer not null,
	primary key(class_id, type),
	foreign key(class_id) references VehicleClasses
);

--must be created before Vehicles table
create table Shipments (
	id integer not null,
	date_available date not null,
	date_picked_up date,
	date_delivered date,
	payment numeric(20, 2),
	primary key(id)
);

create table Vehicles (
	id integer not null,
	class_id integer not null,
	date_man date not null,
	date_acq date not null,
	acq_cost numeric(20, 2),
	state varchar(20) not null check(state in ('parked', 'picking up shipment', 'delivering shipment', 'transit to service', 'broken down')),
	km_travelled integer not null,
	profit numeric(20, 2),
	loc_lat float(32) not null,
	loc_lon float(32) not null,
	shipment_id integer,
	primary key(id),
	foreign key(class_id) references VehicleClasses,
	foreign key(shipment_id) references Shipments
);

create table RefittedCapacities (
	vehicle_id integer not null,
	type varchar(20) not null check(type in ('passengers', 'boxed', 'liquid', 'powder', 'valuables')),
	quantity integer,
	primary key(vehicle_id, type),
	foreign key(vehicle_id) references Vehicles
);

create table Drivers (
	id integer not null,
	vehicle_assigned integer,
	name varchar(50) not null,
	date_employed date not null,
	date_terminated date,
	salary numeric(20, 2),
	passenger_transport char(1) not null check(passenger_transport in ('y', 'n')),
	primary key(id),
	foreign key(vehicle_assigned) references Vehicles
);

create table DriverQualifications (
	driver_id integer not null,
	vehicle_type varchar(10) not null check(vehicle_type in ('road', 'rail', 'air', 'sea')),
	primary key(driver_id, vehicle_type),
	foreign key(driver_id) references Drivers
);

create table Stations (
	id integer not null,
	name varchar(50) not null,
	location_lat float not null,
	location_lon float not null,
	primary key(id)
);

create table StationTerminals (
	station_id integer not null,
	vehicle_type varchar(10) not null check(vehicle_type in ('road', 'rail', 'air', 'sea')),
	primary key(station_id, vehicle_type),
	foreign key(station_id) references Stations
);

create table ShipmentsWaiting (
	station_id integer not null,
	shipment_id integer not null, 
	primary key(station_id, shipment_id),
	foreign key(station_id) references Stations,
	foreign key(shipment_id) references Shipments
);

create table ShipmentContents (
	shipment_id integer not null,
	cargo_type varchar(20) not null check(cargo_type in ('passengers', 'boxed', 'liquid', 'powder', 'valuables')),
	quantity integer not null,
	primary key(shipment_id, cargo_type),
	foreign key(shipment_id) references Shipments
);

create table ServiceRecord (
	id integer not null,
	vehicle_id integer not null,
	date_started date not null,
	reason varchar2(300) not null,
	cost numeric(20, 2) not null,
	station_id integer, 
	primary key(id),
	foreign key(vehicle_id) references Vehicles,
	foreign key(station_id) references Stations
);

--create triggers
--http://stackoverflow.com/questions/10613846/create-table-with-sequence-nextval-in-oracle
drop sequence vehicle_class_id_seq;
create sequence vehicle_class_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_vehicle_class_id
	before insert on VehicleClasses
	for each row
declare
	v_id integer;
begin
	select vehicle_class_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence vehicle_id_seq;
create sequence vehicle_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_vehicle_id
	before insert on Vehicles
	for each row
declare
	v_id integer;
begin
	select vehicle_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence driver_id_seq;
create sequence driver_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_driver_id
	before insert on Drivers
	for each row
declare
	v_id integer;
begin
	select driver_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence shipment_id_seq;
create sequence shipment_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_shipment_id
	before insert on Shipments
	for each row
declare
	v_id integer;
begin
	select shipment_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence station_id_seq;
create sequence station_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_station_id
	before insert on Stations
	for each row
declare
	v_id integer;
begin
	select station_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;


--delete the refitted capacities when a vehicle is deleted
create or replace trigger delete_vehicle_cleanup
	before delete on Vehicles
	for each row
declare
	v_id integer;
begin
	v_id := :old.id;
	delete from RefittedCapacities where vehicle_id = v_id;
end;
.
run;

--make it possible to delete a shipment waiting in a station
--by deleting its ShipmentsWaiting entry beforehand
create or replace trigger delete_shipments_waiting
	before delete on Shipments
	for each row
declare
	v_id integer;
begin
	v_id := :old.id;
	delete from ShipmentsWaiting where shipment_id = v_id;
end;
.
run;

--view
create or replace view vehicles_at_station (station_id) as 
	select id from Vehicles where Vehicles.state = 'parked' and 
		(select type from VehicleClasses where VehicleClasses.id = Vehicles.class_id) in
			(select vehicle_type from StationTerminals where StationTerminals.station_id = station_id);
