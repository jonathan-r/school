library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity container_tb is
end container_tb;
 
architecture behavior of container_tb is 
	component container is
		port ( reg_write : in  std_logic;
         reg_dest : in  std_logic_vector(2 downto 0);
         reg_src_a : in  std_logic_vector(2 downto 0);
         reg_src_b : in  std_logic_vector(2 downto 0);
         constant_in : in  std_logic_vector(15 downto 0);
         mb_select : in  std_logic;
         func : in  std_logic_vector(4 downto 0);
         md_select : in  std_logic;
         data_in : in  std_logic_vector(15 downto 0);
         clk : in  std_logic;
         addr_out : out  std_logic_vector(15 downto 0);
         data_out : out  std_logic_vector(15 downto 0);
         v : out  std_logic;
         c : out  std_logic;
         n : out  std_logic;
         z : out  std_logic);
	end component;

	 --inputs
   signal reg_write : std_logic := '0';
   signal reg_dest : std_logic_vector(2 downto 0) := (others => '0');
   signal reg_src_a : std_logic_vector(2 downto 0) := (others => '0');
   signal reg_src_b : std_logic_vector(2 downto 0) := (others => '0');
   signal constant_in : std_logic_vector(15 downto 0) := (others => '0');
   signal mb_select : std_logic := '0';
   signal func : std_logic_vector(4 downto 0) := (others => '0');
   signal md_select : std_logic := '0';
   signal data_in : std_logic_vector(15 downto 0) := (others => '0');
   signal clk : std_logic := '0';

 	--outputs
   signal addr_out : std_logic_vector(15 downto 0);
   signal data_out : std_logic_vector(15 downto 0);
   signal v : std_logic;
   signal c : std_logic;
   signal n : std_logic;
   signal z : std_logic; 

	constant clk_period : time := 70ns; --50ns on the arithmetic unit + muxes and stuff around it
	
begin
	uut: container port map (reg_write, reg_dest, reg_src_a, reg_src_b, constant_in, mb_select, func, md_select, data_in, clk, addr_out, data_out, v, c, n, z);

	clk_process : process begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period * 3 / 2; --init
		
		report "contstant loading";
		reg_write <= '1';
		mb_select <= '1';
		func <= "10000"; --b passes through the shifter
		md_select <= '0'; --from the function unit
		
		for i in 0 to 7 loop
			report "write to r" & integer'image(i);
			reg_write <= '1';
			reg_dest <= std_logic_vector(to_unsigned(i, 3));
			constant_in <= std_logic_vector(to_unsigned(i, 16)) xor x"cdcd";
			wait for clk_period;
			
			report "disabled writing to r" & integer'image(i);
			reg_write <= '0';
			reg_dest <= std_logic_vector(to_unsigned(i, 3));
			data_in <= std_logic_vector(to_unsigned(i, 16)) xor x"eeee";
			wait for clk_period;
			
			report "read from r" & integer'image(i);
			reg_src_a <= std_logic_vector(to_unsigned(i, 3));
			wait for clk_period;
			assert addr_out = (std_logic_vector(to_unsigned(i, 16)) xor x"cdcd");
		end loop;
		
		report "data loading"; --load the value 0 to 7 in each register
		reg_write <= '1';
		mb_select <= '0';
		md_select <= '1';
		for i in 0 to 7 loop
			report "write to r" & integer'image(i);
			reg_dest <= std_logic_vector(to_unsigned(i, 3));
			data_in <= std_logic_vector(to_unsigned(i, 16));
			wait for clk_period;
			
			report "read from r" & integer'image(i);
			reg_src_a <= std_logic_vector(to_unsigned(i, 3));
			reg_src_b <= std_logic_vector(to_unsigned(i, 3));
			wait for clk_period;
			assert addr_out = std_logic_vector(to_unsigned(i, 16));
			assert data_out = std_logic_vector(to_unsigned(i, 16));
		end loop;
		
		
		report "sum of every register, into r0"; --sum the above values
		reg_src_a <= "000";
		reg_dest <= "000";
		reg_write <= '1';
		func <= "00010"; --sum
		md_select <= '0'; --reg input from function unit
		for i in 1 to 7 loop
			report "r0 += r" & integer'image(i);
			reg_src_b <= std_logic_vector(to_unsigned(i, 3));
			wait for clk_period;
		end loop;

		wait for 1ps; --assertion fails for some reason otherwise
		assert addr_out = x"001c"; -- 7 * (7 + 1) / 2 = 28 = 0000 0000 0001 1100
		assert c = '0';
		assert v = '0';
		assert n = '0';
		assert z = '0';
		wait for clk_period - 1ps;
		
		data_in <= x"0000";
		
		
		report "(destructive, unsigned) multiplication of 731(r2) by 375(r3), accumulate in r0, mask in r1";
		reg_write <= '1';
		md_select <= '1';
		
		reg_dest <= "000";
		data_in <= x"0000"; 
		wait for clk_period; --load 0 in r0
		
		reg_dest <= "010";
		data_in <= x"02db"; --
		wait for clk_period; --load 731 in r2
		
		reg_dest <= "011";
		data_in <= x"0177";
		wait for clk_period; --load 375 in r3
		
		reg_dest <= "100";
		data_in <= x"0000";
		wait for clk_period; --load 0 in r4
		md_select <= '0';
		
		for i in 0 to 15 loop --for every bit except the last one (cause x << 15 is always 0)
			report "multiplication round " & integer'image(i);
			report "isolate the first bit of r2";
			mb_select <= '1';
			constant_in <= x"0001"; --0x1
			func <= "01000"; -- bitwise and
			reg_src_a <= "010"; --r2
			reg_dest <= "001"; --r1
			wait for clk_period; --r1 = r2 & 0x1
			
			report "expand the first bit to every bit bits";
			mb_select <= '0';
			reg_src_a <= "100"; --r4, which stays 0 throughout
			reg_src_b <= "001"; --r1
			reg_dest <= "001"; --r1
			func <= "00101"; -- difference
			wait for clk_period; --r1 = 0 - r1
			assert (data_out = x"0000") or (data_out = x"0001");
			
			report "and r3 with the mask";
			reg_src_a <= "001"; --r1
			reg_src_b <= "011"; --r3
			reg_dest <= "001"; --r1
			func <= "01000"; -- bitwise and
			wait for clk_period; --r1 &= r3
			assert (addr_out = x"0000") or (addr_out = x"ffff");
			
			report "accumulate the masked r3";
			reg_src_a <= "000"; --r0
			reg_src_b <= "001"; --r1
			reg_dest <= "000"; --r0
			func <= "00010"; --sum
			wait for clk_period; --r0 += r1
			
			report "shift r3 left for next round";
			reg_src_b <= "011"; --r3
			reg_dest <= "011"; --r3
			func <= "11000"; --logical shift left
			wait for clk_period; --r3 <<= 1
			
			report "make the next bit of r2 the lsb";
			reg_src_b <= "010"; --r2
			reg_dest <= "010"; --r2
			func <= "10100"; --logical shift right
			wait for clk_period; --r2 >>= 1
		end loop;
		
		reg_write <= '0';
		reg_src_a <= "000";
		wait for clk_period;
		assert addr_out = std_logic_vector(to_unsigned(731 * 375, 16)); --x"2ecd"; --0010 1110 1100 1101
		
		wait;
	end process;
end;
