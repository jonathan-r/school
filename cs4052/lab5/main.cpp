#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <random>

#include "generic_shader.h"
#include "shader_program.h"
#include "vbo.h"
#include "vertex.h"
#include "object.h"
#include "texture.h"
#include "terrain.h"
#include "grabber.h"
#include "plane.h"
#include "noise.h"
#include "sunsky.h"

#include "maths/mat4.h"
#include "maths/vec.h"
#include "maths/util.h"

#ifdef GLSL120
#define SHADER_ROOT_NAME "shaders/shader_120"
#define UI_SHADER_ROOT_NAME "shaders/ui_120"
#else
#define SHADER_ROOT_NAME "shaders/shader_330"
#define UI_SHADER_ROOT_NAME "shaders/ui_330"
#endif

using namespace std;

//sunsky: https://nicoschertler.wordpress.com/2013/04/03/simulating-a-days-sky/

//Plane statics
Mesh* Plane::bodyMesh = nullptr;
Mesh* Plane::propMesh = nullptr;
Mesh* Plane::rudderMesh = nullptr;
Mesh* Plane::elevatorMesh = nullptr;
Mesh* Plane::wheelMesh = nullptr;
Texture* Plane::bodyTexture = nullptr;

//Grabber statics
Mesh* Grabber::segmentMesh = nullptr;
Mesh* Grabber::clawBaseMesh = nullptr;
Mesh* Grabber::clawLeftMesh = nullptr;
Mesh* Grabber::clawRightMesh = nullptr;
Texture* Grabber::segmentTexture = nullptr;
Texture* Grabber::clawTexture = nullptr;

//Texture statics
int Texture::numUnits = 0;
Texture** Texture::boundToSlot = nullptr;

//window dimensions
int WW = 800;
int WH = 600;

//input
bool keysDown[256];

//timing
int lastDrawTime = 0;
int lastIdleTime = -1;
int lastTerrainTime = -1;

GenericShader* mainShader = NULL;
UIShader* uiShader = NULL;
ShaderProgram* mainShaderProgram = NULL;
ShaderProgram* uiShaderProgram = NULL;
NoiseGen* noise = NULL;
Plane* plane = NULL;
Terrain* terrain = NULL;
UIMesh* clockMesh = NULL;

quat cameraRot;
vec2 headingAttitude(0.0f, 0.0f);
vec3 cameraPos(0.0f, 5.0f, 0.0f);
vec3 cameraDir(0.0f, 0.0f, 1.0f);
vec3 cameraUp(0.0f, 1.0f, 0.0f);
vec3 cameraSide(1.0f, 0.0f, 0.0f);

vec3 eye, target, up;

bool terrainWire = false;
bool terrainOverlay = false;
float fovAngle = 70.0f;
float dayCycle = 0.5f;

vec2 cameraDragStartHA;
vec2 cameraDragStartPos;
vec3 cameraDragTiltAxis;
bool cameraDrag;
bool freeCamera = false;

mt19937 rng;
vector<vec3> checkpoints;
vector<vertex> trailPoints;
AnglesObject trailOb;
vector<Plane*> ghosts;

mat4 viewMatrix;

bool pointAhead = false, pointRightOf = false;

void display() {
	int frameBeginTime = glutGet(GLUT_ELAPSED_TIME);
	lastDrawTime = frameBeginTime;
	float time = (float)frameBeginTime / 1000.0f;
	
	mainShader->use();
	mainShader->setTextureId(0);
	
	//3d scene display
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	mainShader->setTime(time);
	
	WW = glutGet(GLUT_WINDOW_WIDTH);
	WH = glutGet(GLUT_WINDOW_HEIGHT);
	glViewport(0, 0, WW, WH);
	float aspect = (float)WW / WH;

	if (freeCamera) {
		eye = cameraPos;
		target = cameraPos + cameraDir;
		up = cameraUp;
	}
	else {
		plane->cameraPoints(eye, target, up);
	}

	/*{
		//check whether points is above of right of
		vec3 point = checkpoints[0];
		vec3 diff = point - eye;
		vec3 forward = (target - eye).normal();
		vec3 right = (forward ^ up).normal();

		bool ah = (forward * diff) > 0.0f;
		bool rh = (right * diff) > 0.0f;

		if (ah != pointAhead) printf("point is now %s\n", ah? "ahead" : "behind");
		if (rh != pointRightOf) printf("point is now %s of\n", rh? "right" : "left");

		pointAhead = ah;
		pointRightOf = rh;
	}*/
	
	mainShader->setCameraPos(eye);
	viewMatrix = mat4::viewLookAt(eye, target, up);
	mat4 proj = mat4::perspectiveProjection(fovAngle, aspect, 0.1f, 5000.0f);
	//mat4 view = mat4::viewLookAt(cameraPos, , vec3(0.0f, 1.0f, 0.0f));
	//mat4 view = mat4::viewLookAt(a, b, c);

	vec3 sunDir, sunCol, sunSpe, sunAmb, skyCol;
	SunSky::sunSpecs(dayCycle, sunDir, sunCol, sunSpe, sunAmb, skyCol);
	mainShader->setLightParams(sunDir, sunCol, sunSpe, skyCol);//unAmb);
	glClearColor(skyCol.x, skyCol.y, skyCol.z, 1.0f);

	mat4 eyeMat = proj * viewMatrix; //no model
	mat4 worldMat = mat4::identity();
	
	//no transformation and no light on grid and arrows
	mainShader->setFullMatrix(eyeMat);
	mainShader->setWorldMatrix(worldMat);
	mainShader->setLightFraction(0.0f);
	mainShader->setTextureId(0);
	
	glLineWidth(5.0f);
	mainShader->setMatParams(vec3(1.0f), vec3(1.0f), vec3(1.0f), 0.4f);
	trailOb.draw(*mainShader, eyeMat);
	glLineWidth(1.0f);

	//full light affect
	mainShader->setLightFraction(1.0f);
	mainShader->setMatParams(vec3(0.5f, 0.5f, 0.5f), vec3(1.0f), vec3(1.0f), 0.4f);
	plane->getObj().draw(*mainShader, eyeMat);

	mainShader->setMatParams(vec3(0.5f, 0.5f, 0.5f), vec3(0.4f), vec3(0.4f), 0.4f);
	for (int i = 0; i < ghosts.size(); i++) {
		ghosts[i]->getObj().draw(*mainShader, eyeMat);
	}

	mainShader->setMatParams(vec3(0.6f, 0.8f, 0.7f), vec3(0.4f), vec3(1.0f), 0.4f);
	terrain->getOb().draw(*mainShader, eyeMat);

	//user interface
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	
	uiShader->use();
	mat3 uiMatrix = mat3::scale(vec2(1.0f, aspect));
	uiShader->setMatrix(uiMatrix);

	if (terrainOverlay) {
		terrain->overlay.bind(*uiShader);
		glLineWidth(3.0f);
		glDrawArrays(GL_TRIANGLES, 0, terrain->overlay.getVerticesAssigned());
	}

	clockMesh->bind(*uiShader);
	glDrawArrays(GL_LINES, 0, clockMesh->getVerticesAssigned());

	glutSwapBuffers();
	//int frameEndTime = glutGet(GLUT_ELAPSED_TIME);
}

void idle() {
	int dt, time = glutGet(GLUT_ELAPSED_TIME);
	if (lastIdleTime == -1) {
		dt = 10;
	}
	else {
		dt = time - lastIdleTime;
	}

	//fps independent animation
	if (dt > 0) {
		float dtf = dt / 1000.0f;
		float timef = time / 1000.0f;

		//translation
		float trc = 50.0f * dtf;

		if (freeCamera) {
			if (keysDown['w']) cameraPos += cameraDir * trc;
			else if (keysDown['s']) cameraPos -= cameraDir * trc;
			
			if (keysDown['a']) cameraPos -= cameraSide * trc;
			else if (keysDown['d']) cameraPos += cameraSide * trc;

			//plane frozen
		}
		else {
			plane->input(-keysDown['w'] * dtf * 10 + keysDown['s'] * dtf * 10, //elevator
					 -keysDown['a']	* dtf * 10 + keysDown['d'] * dtf * 10, //rudder
					 -keysDown['q'] * dtf * 10 + keysDown['e'] * dtf * 10, //aileron
					 -keysDown['z'] * dtf * 10 + keysDown['x'] * dtf * 10); //throttle
			plane->update(dtf);
		}

		if (keysDown['r']) dayCycle += dtf * 0.5f;
		else if (keysDown['t']) dayCycle -= dtf * 0.5f;
		else dayCycle += dtf * 0.01f; //normal progression

		if (dayCycle > 1.0f) dayCycle -= 1.0f;
		else if (dayCycle < 0.0f) dayCycle += 1.0f;

		//update clock
		vertex_ui clockVerts[2];
		vec2 cp(-0.7f, 0.0f);
		clockVerts[0].v = cp;
		float angle = -dayCycle * M_PI * 2;
		clockVerts[1].v = cp + vec2(cos(angle), sin(angle)) * 0.1f;
		clockVerts[0].c.set(1, 1, 1);
		clockVerts[1].c.set(1, 1, 1);

		clockMesh->assign(2, clockVerts, 0, nullptr, GL_DYNAMIC_DRAW);

		//check for collision
		vec3 pc = plane->getObj().stem;
		vec2 mp(pc.x, pc.z);
		vec3 mv(mp.x, terrain->sampleHeight(mp), mp.y);

		if ((pc - mv).length() < 3.0f) {
			//collided - spawn new plane

			if (ghosts.size() > 10) {
				delete ghosts[0];
				ghosts.erase(ghosts.begin());
			}

			ghosts.push_back(plane);
			pc = vec3(0, terrain->sampleHeight(vec2(0.0f)) + 150, 0);
			plane = new Plane(pc);
		}

		//update trail
		float dist = (pc - checkpoints[0]).length();
		if (dist < 100.0f) {
			checkpoints.erase(checkpoints.begin());
			trailPoints.erase(trailPoints.begin()+1);
		}

		if (checkpoints.size() == 0) {
			glutLeaveMainLoop();
		}

		trailPoints[0].v = pc;
		trailOb.model->assign(trailPoints.size(), trailPoints.data(), 0, nullptr, GL_DYNAMIC_DRAW);
	
		//if (keysDown['q']) targetPos.z += trc;
		//else if (keysDown['e']) targetPos.z -= trc;

		//update only counts if something was actually done
		lastIdleTime = time;
	}

	if (time - lastTerrainTime > 200) {
		lastTerrainTime = time;
		terrain->update(eye, target, up, terrainWire);
	}

	//limit ~60fps
	if (time - lastDrawTime > 16) {
		lastDrawTime = time;
		glutPostRedisplay();
	}
}

void mouseWheel(int wheel, int dir, int x, int y) {
	//printf("wheel %d, dir %d\n", wheel, dir);
	cameraPos += cameraUp * dir;
}

void mouseButton(int button, int state, int x, int y) {
	printf("mouse button %d %d\n", button, state);
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			//printf("starting drag at (%d %d)\n", x, y);
			cameraDrag = true;
			cameraDragStartPos.set(x, y);
			cameraDragStartHA = headingAttitude;
			//cameraDragTiltAxis = cameraRot.rotate(vec3(1.0f, 0.0f, 0.0f));//((cameraPos).normal() ^ vec3(0.0f, 1.0f, 0.0f)).normal();
		}
		else if (state == GLUT_UP) {
			cameraDrag = false;
		}
	}
	else if (button == 3 and state == GLUT_DOWN) mouseWheel(0,  1, x, y);
	else if (button == 4 and state == GLUT_DOWN) mouseWheel(0, -1, x, y);
}

//used for both dragging and hovering motion
void mouseMove(int x, int y) {
	if (cameraDrag) {
		vec2 drag = (vec2(x, y) - cameraDragStartPos) * 0.005f;
		drag.y = -drag.y;
		headingAttitude = cameraDragStartHA + drag;
		//printf("heading %+.2f attitude %+.2f\n", headingAttitude.x, headingAttitude.y);

		cameraDir = mat4::flightRotation(headingAttitude.x, headingAttitude.y, 0.0f).multVec3(vec3(0, 0, 1), 0.0f);
		cameraSide = (cameraDir ^ vec3(0, 1, 0)).normal();
	}
}

void keyboardDown(unsigned char key, int x, int y) {
	keysDown[key] = true;
	if (key == 0x1B) glutLeaveMainLoop(); //escape
	else if (key == 'n') terrainWire = !terrainWire;
	else if (key == 'm') terrainOverlay = !terrainOverlay;
	else if (key == ' ') freeCamera = !freeCamera;
	/*//else if (key == ' ') grabber->update(0.01f);
	else if (key == 'z') grabber->setTarget(&targetPos);
	else if (key == 'x') grabber->setTarget(NULL);
	else for (int c = '1'; c < '9'; c++) {
		if (key == c) {
			grabber->setTarget(&(flies[c-'1'].worldPos));
		}
	}*/
}

void keyboardUp(unsigned char key, int x, int y) {
	//cout << "key up: " << key << endl;
	keysDown[key] = false;
}

void specDown(int key, int x, int y) {
	//keysDown[key] = true;
}

void specUp(int key, int x, int y) {
	//keysDown[key] = false;
}

void init() {
	rng.seed(time(NULL));
	uniform_real_distribution<> dist(-1.0f, 1.0f);

	Texture::init();
	viewMatrix = mat4::viewLookAt(cameraPos, cameraPos + cameraDir, cameraUp);

	mainShaderProgram = new ShaderProgram(SHADER_ROOT_NAME ".vert", SHADER_ROOT_NAME ".frag");
	uiShaderProgram = new ShaderProgram(UI_SHADER_ROOT_NAME ".vert", UI_SHADER_ROOT_NAME ".frag");

	mainShader = new GenericShader(*mainShaderProgram);
	uiShader = new UIShader(*uiShaderProgram);

	noise = new NoiseGen(dist(rng) * 0xFFFFF);
	terrain = new Terrain(*noise, vec2(5000.0f), fovAngle, (float)WW / WH, 1000.0f);
	clockMesh = new UIMesh();

	plane = new Plane(vec3(0, terrain->sampleHeight(vec2(0.0f)) + 150, 0));

	//make checkpoints
	trailPoints.emplace_back(vec3(), vec3(1.0f), vec2(0.0f));
	for (int i = 0; i < 5; i++) {
		vec3 v;
		while (true) {
			vec2 p(dist(rng) * 1000, dist(rng) * 1000);
			v = vec3(p.x, terrain->sampleHeight(p) + 20 + (1.0f + dist(rng)) * 50, p.y);
			bool good = true;
			for (int j = 0; j < i; j++) {
				if ((checkpoints[j] - v).length() < 500) {
					good = false;
					break;
				}
			}
			if (good) break;
		}
		
		checkpoints.push_back(v);
		trailPoints.emplace_back(v, vec3(1.0f), vec2(0.0f));
	}

	trailOb.model = new Mesh();
	trailOb.drawMode = GL_LINE_STRIP;
	trailOb.bakeAll();
	
	//reset state
	memset(keysDown, 0, 256);

	glEnable(GL_CULL_FACE);
	glEnable(GL_MULTISAMPLE);
	glClearColor(0.3f, 0.4f, 0.2f, 1.0f);
	glClearDepth(1e5f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(PRIMITIVE_RESTART_INDEX); //OpenGL 3.1+ !!!
}

int main(int argc, char** argv) {
	//window
	glutInit(&argc, argv);
	glutInitContextVersion(3, 1);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(WW, WH);
    glutCreateWindow("scene");

    printf("driver: %s - %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION));
	
	//callbacks
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	
	glutKeyboardFunc(keyboardDown);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(specDown);
	glutSpecialUpFunc(specUp);
	
	glutMouseFunc(mouseButton);
	glutMouseWheelFunc(mouseWheel); //doesn't work on X11/gnome3 (workaround in mouseMove)
	glutMotionFunc(mouseMove);
	glutPassiveMotionFunc(mouseMove);

	//extensions
    GLenum res = glewInit();
    if (res != GLEW_OK) {
      fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
      return 1;
    }

    //code
	init();
	glutMainLoop();

	//after (TODO: doesn't seem to ever execute)
	delete mainShader;
	delete mainShaderProgram;

	cout << "graceful exit" << endl;
    return 0;
}

