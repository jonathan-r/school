module Main where
import qualified Graphics.HGL as G
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

type Point = (Float, Float)

next :: Point -> Point -> Point
next (u, v) (x, y) = (x*x - y*y + u, 2*x*y + v)

mandelbrot :: Point -> [Point]
mandelbrot p = iterate (next p) (0, 0)

fairlyClose :: Point -> Bool
fairlyClose (u, v) = (u*u + v*v) < 100

inMandelbrotSet :: Point -> Bool
inMandelbrotSet p = all fairlyClose (mandelbrot p)

approxTest :: Int -> Point -> Bool
approxTest n p = all fairlyClose (take n (mandelbrot p))

--type color = (Word8, Word8, Word8)

makeColor :: [color] -> [Point] -> color
makeColor palette = (palette!!) . length . take n . takeWhile fairlyClose
                    where n = length palette - 1

type Image color = Point -> color
fracImage :: (Point -> [Point]) -> [color] -> Image color
fracImage fractal palette = makeColor palette . fractal

type Symbol = Char
data Action =
    Sym Symbol | --advances the string, but does no direct action
    TurnLeft | TurnRight | --turn in place
    MoveForward | MoveBackward | --move in direction
    SaveState | RestoreState --operate on stack of properties
    deriving (Eq, Ord)

instance Show Action where
    show (Sym a) = "Symbol '" ++ [a] ++ "'"
    show TurnLeft = "Turn Left"
    show TurnRight = "Turn Left"
    show MoveForward = "Forwards"
    show MoveBackward = "Backwards"
    show SaveState = "Save"
    show RestoreState = "Restore"

type LString = [Action]
type RuleSet = Map.Map Symbol LString
type LSystem = (String, LString{-axiom-}, RuleSet)

substitute :: RuleSet -> Action -> LString
substitute rules (Sym c) =
    Maybe.fromMaybe (error $ "No substitution for symbol " ++ [c]) (Map.lookup c rules)
substitute _ act = [act] -- non-symbols don't change

--returns both starting sequence and all intermediates
evaluateAll :: LSystem -> Int -> [LString]
evaluateAll (_, start, rules) n = take n (iterate (map (substitute rules)) start)

--returns only final production
evaluateLast :: LSystem -> Int -> LString
evaluateLast (_, start, rules) n = iterate (map (substitute rules)) start !! n

insertRules :: [(Symbol, Action)] -> RuleSet -> RuleSet
insertRules [] from = from
insertRules ((sym, act):tail) from = insertRules tail (Map.insert sym act from)

makeRules :: [(Symbol, Action)] -> RuleSet
makeRules pairs = insertRules pairs Map.empty

plantSystem = ([Sym 'X'], makeRules [(Sym 'X', [])]) 

main :: IO ()
main = G.runGraphics $
       G.withWindow_ "Test Window" (600, 400) $ \ w -> do
       -- p <- createPen Solid 1 (color 255 255 0)
       G.drawInWindow w $ G.line (10, 10) (10, 10)
       G.getKey w

