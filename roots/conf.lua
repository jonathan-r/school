require("util")

--window
WW = 800
HH = 600

--map
MS = 100
CS = HH/(MS+1)

--roots
DL = 20
GC = 10 --growth cost
ER = 2 --extraction rate, per second
ML = 4 --max len

function getResCol(idx)
	--TODO: infinite colors (recursive
	local cols = {
		{r=255, g=0, b=0},
		{r=150, g=150, b=0},
		{r=0, g=255, b=0},
		{r=0, g=150, b=150},
		{r=0, g=0, b=255},
		{r=150, g=0, b=150},
	}
	return cols[idx]
end
