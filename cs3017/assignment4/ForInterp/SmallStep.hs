module SmallStep where

import Syntax
import State
import BigStep


----------------------------------------------------------
----------------------------------------------------------
--  Small-Step Interpreter for Arithmetic Expressions  ---
----------------------------------------------------------
----------------------------------------------------------

smallStepExp :: EArith -> Maybe EArith
smallStepExp (CNum n)                  = Nothing

smallStepExp (Var name)                = return (CNum 0)

smallStepExp (Add (CNum n1) (CNum n2)) = do let n3 = n1 + n2 
                                            return (CNum n3)

smallStepExp (Add (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Add (CNum n1) newE2)

smallStepExp (Add e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Add newE1 e2)

smallStepExp (Sub (CNum n1) (CNum n2)) = do let n3 = n1 - n2
                                            return (CNum n3)

smallStepExp (Sub (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Sub (CNum n1) newE2)

smallStepExp (Sub e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Sub newE1 e2)

smallStepExp (Mul (CNum n1) (CNum n2)) = do let n3 = n1 * n2
                                            return (CNum n3)

smallStepExp (Mul (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Mul (CNum n1) newE2)

smallStepExp (Mul e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Mul newE1 e2)

smallStepExp (Div (CNum n1) (CNum n2)) = do let n3 = n1 `div` n2
                                            return (CNum n3)

smallStepExp (Div (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Div (CNum n1) newE2)

smallStepExp (Div e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Div newE1 e2)

                    
-------------------------------------------------------
-------------------------------------------------------
--  Small-Step Interpreter for Boolean Expressions  ---
-------------------------------------------------------
-------------------------------------------------------
smallStepBool :: EBool -> Maybe EBool
smallStepBool (CBool v)                   = Nothing

smallStepBool (And (CBool v1) (CBool v2)) = do let v3 = (v1 && v2)
                                               return (CBool v3)

smallStepBool (And (CBool v1) b2)         = do newB2 <- smallStepBool b2
                                               return (And (CBool v1) newB2)

smallStepBool (And b1 b2)                 = do newB1 <- smallStepBool b1
                                               return (And newB1 b2)

smallStepBool (Or (CBool v1) (CBool v2))  = do let v3 = (v1 || v2)
                                               return (CBool v3)

smallStepBool (Or (CBool v1) b2)          = do newB2 <- smallStepBool b2
                                               return (Or (CBool v1) newB2)

smallStepBool (Or b1 b2)                  = do newB1 <- smallStepBool b1
                                               return (Or newB1 b2)

smallStepBool (Eq (CNum n1) (CNum n2))    = do let v = (n1 == n2)
                                               return (CBool v)

smallStepBool (Eq (CNum n1) e2)           = do newE2 <- smallStepExp e2
                                               return (Eq (CNum n1) newE2)

smallStepBool (Eq e1 e2)                  = do newE1 <- smallStepExp e1
                                               return (Eq newE1 e2)

smallStepBool (Lt (CNum n1) (CNum n2))    = do let v = (n1 < n2)
                                               return (CBool v)

smallStepBool (Lt (CNum n1) e2)           = do newE2 <- smallStepExp e2
                                               return (Lt (CNum n1) newE2)

smallStepBool (Lt e1 e2)                  = do newE1 <- smallStepExp e1
                                               return (Lt newE1 e2)

smallStepBool (Not (CBool v))             = do let notV = (not v)
                                               return (CBool notV)

smallStepBool (Not b)                     = do newB <- smallStepBool b
                                               return (Not newB)


--------------------------------------------
--------------------------------------------
--  Small-Step Interpreter for Commands  ---
--------------------------------------------
--------------------------------------------

smallStepCom :: (Com,  State) -> Maybe (Com, State)
smallStepCom (Skip, s)                 = Nothing

smallStepCom ((Asgn var exp),       s) = do let (VArith n) = bigStepExp exp s
                                            return (Skip, (stateUpdate s var n))

smallStepCom ((Seq Skip c2),        s) = do return (c2, s)

smallStepCom ((Seq c1 c2),          s) = do (c1', s') <- smallStepCom (c1, s)
                                            return ((Seq c1' c2), s')

smallStepCom ((IfThenElse b c1 c2), s) = do case (bigStepBool b s) of
                                              (VBool True)  -> return (c1, s)
                                              (VBool False) -> return (c2, s)
                        
smallStepCom ((While b c),          s) = do case (bigStepBool b s) of
                                              (VBool False) -> return (Skip,  s)
                                              (VBool True)  -> return ((Seq c (While b c)),  s)


smallStepCom ((For var n c),        s) = do let counter = stateLookup s var
                                            if counter <= n then
                                              return ((Seq (Seq c (Asgn var (Add (Var var) (CNum 1)))) (For var n c)), s)
                                            else
                                              return (Skip, s)

{-
Useful code fragments:

1. Lookup variable 'var' in state 's' and put the value in Haskell's variable 'vl'

  do let vl = stateLookup s var
     ...

2. Construct a for-language command that, when run, will increment 'var' by one

  ... (Asgn var (Add (Var var) (CNum 1))) ...
--}


