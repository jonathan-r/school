#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int word_possible_inner(char* grid, int size, int x, int y, char* letter){ //letter[0] is the letter at [x, y], letter[1] is the next one
	if (letter[1] == 0) return 1; //reached the end of the word
	
	//remove the letter
	grid[x + y * size] = 'a';//0;

	int possible = 0;
	
	for (int dx = -1; dx < 2; dx++){
		for (int dy = -1; dy < 2; dy++){
			int nx = x + dx;
			int ny = y + dy;
			
			if (nx >= 0 && nx < size && ny >= 0 && ny < size){
				if (grid[nx + ny * size] == letter[1]){
					if (word_possible_inner(grid, size, nx, ny, letter + 1)){
						grid[x + y * size] = letter[0]; //replace it here as well
						return 1;
					}
				}
			}
		}
	}
	
	//and replace it back
	grid[x + y * size] = letter[0];
	
	return 0;
}

int word_possible(char* grid, int size, char* word){
	//we can start from anywhere on the board
	for (int j = 0; j < size; j++){
		for (int i = 0; i < size; i++){
			if (grid[i + j * size] == word[0]){
				int possible = word_possible_inner(grid, size, i, j, word); //try to make a path from here
				if (possible) return 1;
			}
		}
	}
	
	return 0;
}

int main(int argc, char** argv){
	int size = 4;
	char* grid = malloc(size * size);
	
	memcpy(grid + size * 0, "CBRD", size);
	memcpy(grid + size * 1, "PCRF", size);
	memcpy(grid + size * 2, "STAA", size);
	memcpy(grid + size * 3, "IAEW", size);
	
	if (word_possible(grid, size, "TRAAW")){
		printf("word possible\n");
	}
	else printf("not possible\n");
	
	return 0;
}
