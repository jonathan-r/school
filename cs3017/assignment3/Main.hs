module Main( main ) where

import Parser
import SmallStep

import System.Environment( getArgs )
import System.Console.GetOpt

--import Text.Parsec( ParseError )

version :: Integer
version = 4

-- Main reads command-line arguments
main :: IO()
main = do 
  args <- getArgs
  case getOpt RequireOrder options args of
    (flags, [],      [])  -> run flags
    (_,     nonOpts, [])  -> error $ "unrecognized arguments: " 
                                     ++ unwords nonOpts ++ "\n" 
                                     ++ usageInfo header options
    (_,     _,       err) -> error $ concat err ++ usageInfo header options

-- the cases of the command-line arguments
data Flag = Help | Version | Input String | BigStep | SmallStep
  deriving (Eq, Show)
  
-- the syntax of the command-line arguments
options :: [OptDescr Flag]
options = [
  Option "?" [] (NoArg Help)                    "show help",
  Option "v" ["version"] (NoArg Version)        "show version number and exit",
  Option "f" ["file"]   (ReqArg Input "FILE")   "read source from file (if option\
                                                \is missing use standard input)" ]

header :: String
header = "Interpreter for the 'For' language given in CS3017\nVersion: " 
         ++ show version ++ "\nUsage: main [OPTION...]"


-- this is the function that reads the code and runs the appropriate interpreter
run :: [Flag] -> IO()
run flags | Help    `elem` flags = putStr $ usageInfo header options
          | Version `elem` flags = putStr $ "Version: " ++ show version  
          | otherwise            = do     -- run interpreter
    putStrLn "small-step interpreter for Exp programs"
    code <- if length filename > 0          -- read contents of file (or stdio) 
              then readFile filename
              else getLine
    case parseExp filename code of
      Left err    -> putStr $ "parse error at " ++ show err
      Right conf  -> do putStrLn $ showConfig conf
                        reduce smallStepExp conf
 where filename = foldr findfile "" flags
      

-- reduce a term using a small-step interpreter function
reduce :: (Configuration -> Maybe Configuration) -> Configuration -> IO ()
reduce smallStep conf = case smallStep conf of
                             Nothing    -> putStrLn "cannot reduce any more"
                             Just conf' -> do putStrLn $ "--> " ++ showConfig conf' 
                                              reduce smallStep conf'

-- find a filename in a flag or else return 2nd argument
findfile :: Flag -> String -> String
findfile flag fname = case flag of
                        Input file -> file
                        _ -> fname

