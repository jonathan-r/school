	.file	"fib.c"
	.intel_syntax noprefix
	.text
	.globl	fib
	.type	fib, @function
fib:
.LFB0:
	.cfi_startproc
	push	ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	mov	ebp, esp
	.cfi_def_cfa_register 5
	sub	esp, 16
	cmp	DWORD PTR [ebp+8], 1
	jg	.L2
	mov	eax, DWORD PTR [ebp+8]
	jmp	.L3
.L2:
	mov	DWORD PTR [ebp-4], 0
	mov	DWORD PTR [ebp-8], 1
	jmp	.L4
.L5:
	mov	eax, DWORD PTR [ebp-8]
	mov	DWORD PTR [ebp-12], eax
	mov	eax, DWORD PTR [ebp-4]
	add	DWORD PTR [ebp-8], eax
	mov	eax, DWORD PTR [ebp-12]
	mov	DWORD PTR [ebp-4], eax
	sub	DWORD PTR [ebp+8], 1
.L4:
	cmp	DWORD PTR [ebp+8], 1
	jg	.L5
	mov	eax, DWORD PTR [ebp-8]
.L3:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	fib, .-fib
	.ident	"GCC: (GNU) 4.9.1 20140903 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
