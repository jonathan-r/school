#!/usr/bin/python3

import socket
import urllib.parse as urllib
import sys

if len(sys.argv) in (3, 4):
	host = sys.argv[1]
	url = ''
	
	sl = host.find('/')
	if sl != -1:
		url = host[sl:]
		host = host[:sl]

	port = 80
	msg = ''

	if len(sys.argv) == 2:
		msg = sys.argv[2]
	else:
		port = int(sys.argv[2])
		msg = sys.argv[3]

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect((host, port))
	
	escaped = urllib.quote(msg)
	sock.send(bytes("GET %s?message=%s HTTP/1.1\r\n\r\n" % (url, escaped), "utf-8"))
	resp = b''
	while True:
		rec = sock.recv(1024)
		if rec == '': break
		resp += rec

	sock.close()
	
	st = resp.find('<pre>')
	nd = resp.find('</pre>')
	if st != nd: #neither are -1
		back = urllib.unquote(resp[st + len('<pre>'):nd])
		print(back)
	else:
		print('malformed response')

else:
	print("usage: %s <host> [port] <msg>" % sys.argv[0])
