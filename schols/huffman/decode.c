#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>

#include "huffman.h"


#define FRAGMENT_SIZE 512
typedef struct fragment_struct{
	int bytes;
	uint8_t data[FRAGMENT_SIZE];
	struct fragment_struct* next;
} fragment;

//this would run on a separate computer and so we can't just give it the symbol tree
//the amount of bytes to decompress must be known, since the last byte might contain extra characters
uint8_t* huffman_decode(uint8_t encoded[], int bytes_compressed, int bytes_decoded, hcode* codes){
	//recreate the symbol treee from the codes
	symbol* root = malloc(sizeof(symbol));
	root->lower[0] = root->lower[1] = NULL;
	root->next = NULL; //list functionality will not be used here
	root->c = 0;
	
	for (int character = 0; character < 256; character++){
		if (codes[character].len > 0){
			symbol** s = &root;
			int right = 0; //whether we went to the right last time
			for (int d = codes[character].len - 1; d >= 0; d--){
				if ((codes[character].data >> d) & 0x1) s = &((*s)->lower[1]); //branch right
				else s = &((*s)->lower[0]); //branch left
				
				if (!(*s)){
					*s = malloc(sizeof(symbol));
					(*s)->lower[0] = (*s)->lower[1] = NULL;
					(*s)->next = NULL;
					(*s)->c = 0;
				}
			}
			
			(*s)->c = character; //end of the path
		}
	}
	
	if (!root){
		fprintf(stderr, "there are no characters defined in the hcodes\n");
		return NULL;
	}
	
	//create the first page
	fragment* first = malloc(sizeof(fragment));
	first->bytes = 0;
	first->next = NULL;
	
	//iterators
	fragment* last = first;
	symbol* at = root;
	int decoded = 0;
	
	for (int inb = 0; inb < bytes_compressed; inb++){
		uint8_t byte = encoded[inb];
		for (int bit = 7; bit >= 0; bit--){
			if ((byte >> bit) & 0x1) at = at->lower[1];
			else at = at->lower[0];
			
			if (!at->lower[0]){
				//end of the branch
				last->data[last->bytes++] = at->c; //store the character
				decoded++;
				at = root; //reset the branch iterator
				
				if (decoded == bytes_decoded) {
					inb = bytes_compressed; //break out of the outer loop as well
					break;
				}
				
				if (last->bytes == FRAGMENT_SIZE){
					//make a new page
					last->next = malloc(sizeof(fragment));
					last = last->next;
					last->bytes = 0;
					last->next = NULL;
				}
			}
		}
	}
	
	assert(decoded == bytes_decoded);
	
	//gather all the fragments into a single buffer
	uint8_t* buf = malloc(decoded);
	uint8_t* pour = buf;
	for (fragment* f = first; f; f = f->next){
		memcpy(pour, f->data, f->bytes);
		pour += f->bytes;
		free(f);
	}
	
	return buf;
}


