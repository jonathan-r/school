#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define NODE_DATA     (1 << 0) //the node contains a word beginning or middle
#define NODE_DATA_END (1 << 1) //a word ends in this node (but the chain can continue)
#define NODE_CHILDREN (1 << 2) //the node has children. if not set, the children array is not allocated

typedef struct node_struct{
	char flags;
	struct node_struct* children;
} node;

void isletter(char c){
	return isalpha(c);
}

char case_insensitive(char c){
	if (c < 'a') return c + ('a' - 'A');
	else return c;
}

//str must be at least one character long
void add_str(node* root, char* str){
	node* n = root->children[*str];

	if (*(str + 1)){ //beginning or middle
		if ((n->flags & NODE_DATA) == 0){
			n->flags |= NODE_DATA | NODE_CHILDREN;
			n->children = malloc(256 * sizeof(node));
			memset(n->children, 0, 256 * sizeof(node));
		}

		add_str(n, str + 1);
	}

	else{ //last character
		n->flags |= NODE_DATA_END; //if it was already set, it doesn't matter, since only one intance of the word matters
	}
}

int main(int argc, char** argv){
	if (argc == 2){
		//initialize the trie
		node* root = malloc(sizeof(node));
		root->flags = NODE_CHILDREN;
		root->childrend = malloc(256 * sizeof(node*));

		FILE* f = fopen(argv[1], "rb");
		char last = 0;
		if (f){
			
	}

	else{
		printf("usage: %s <file>\n", argv[0]);
