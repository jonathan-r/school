#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 25
typedef struct node_struct{
	int data[N];
	int items;
	struct node_struct* next;
} node;

typedef struct list_struct{
	node* first;
} list;


list* new_list(){
	list* l = malloc(sizeof(list));
	l->first = NULL;
	return l;
}

void list_free(list* l){
	for (node* n = l->first; n;){
		node* next = n->next;
		free(n);
	}
	free(l);
}

void list_insert(list* l, int val){
	if (l->first){
		for (node* n = l->first; n; n = n->next){
			if (val >= n->data[0] && (!n->next || val < n->next->data[0])){
				//insert in this node
				
				if (n->items == N){
					//there's no space to fit everything
					
					if (n->next && n->next->items < N){
						//in the next node, shift every element to the right by one slot
						for (int i = n->next->items; i > 0; i--){
							n->next->data[i] = n->next->data[i - 1];
						}
						n->next->data[0] = n->data[N - 1]; //move the last element from this node over
					}
					
					else{
						//make a new node after this one
						node* next = malloc(sizeof(node));
						next->next = n->next;
						n->next = next;
						
						next->data[0] = n->data[N - 1]; //move the last element from this node over
						next->items = 1;
					}
				}
					
				//the last element of this node was taken care of; insert the new value
				int place = n->items;
				for (; place > 0; place--){
					if (n->data[place] <= val) break;
					n->data[place] = n->data[place - 1];
				}
				n->data[place] = val;
				return;
			}
		}
	}
	
	else{
		l->first = malloc(sizeof(node));
		l->first->next = NULL;
		l->first->items = 1;
		l->first->data[0] = val;
	}
}

void list_remove(list* l, int val){
	//in case more than one node contains instances of the value
	node* before = NULL; //node before first
	node* first = NULL;
	node* last = NULL;
	int first_id, last_id; //at which index in first and last does the instance first and last, repectively, of the value occur
	
	for (node* n = l->first; n; n = n->next){
		if (val <= n->data[n->items - 1]){
			if (val >= n->data[0]){
				for (int i = 0; i < n->items; i++){
					if (n->data[i] == val){
						if (!first){
							first = n;
							first_id = i;
						}
						
						last = n;
						last_id = i;
					}
				}
			}
			
			if (!first) before = n;
		}
		
		else break;
	}
	
	if (first){
		//there is at least one node that contains the value
		
		//delete all the nodes in between first and last, since they contain only the value to be removed
		for (node* n = first; n != last;){
			node* next = n->next;
			free(n);
			n = next;
		}
		first->next = last;
		
		int in_last = last->items - last_id - 1; //number or remaining items in last
		
		if (first_id + in_last <= N){
			//the remaining nodes of the two nodes fit in one node
			int dest = first_id;
			for (int i = last_id; i < last->items; i++){
				first->data[dest++] = last->data[i];
				first->items++;
			}
			
			first->next = last->next;
			free(last);
			last = first;
		}
		
		else{
			//just remove the elements from the two nodes
			if (first == last){
				int dest = first_id;
				for (int i = last_id + 1; i < first->items; i++){
					first->data[dest++] = first->data[i];
				}
			}
			
			else{
				//slice off the end of the first node
				first->items = first_id;
				
				//slice off the start of the last node
				int dest = 0;
				for (int i = last_id + 1; i < last->items; i++){
					last->data[dest++] = last->data[i];
				}
			}
		}
		
		//now remove and emtpy nodes
		if (first->items == 0){
			before->next = first->next;
			free(first);
			first = before; //just so that deleting the last node works
		}
		
		if (last != first){
			if (last->items == 0){
				first->next = last->next;
				free(last);
			}
		}
	}
}
	
void list_print(list* l){
	for (node* n = l->first; n; n = n->next){
		printf("& ");
		for (int i = 0; i < n->items; i++){
			printf("%d", n->data[i]);
			if (i < n->items - 1) printf(", ");
		}
	}
}
