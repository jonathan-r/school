'use strict';

var code_textarea = null;
var code_div = null;

var input_ok = false;

function MemoryRegion(start){
	this.start = start;
	this.end = start;
	this.content = []; //Instruction, Number as a 32 bit constant
}

function Word(value){
	this.value 
}

function Operand(type, value){
	this.type = type; //'reg', 'imm', 'label', 'cond'
	this.value = value; //Number or String
}

//always 32 bit large, must be aligned
function Instruction(op, type, a, b, c, scc){
	this.op = op; //'and', 'sub', ...
	this.type = type; //'reg-reg', 'load', ...
	this.operands = [a, b, c]; //Operand intances
	this.cond = scc; //Bool
}

//addressing modes:
//  32       25    24         19          14      13                  0
//1: | opcode | scc | dest_reg | src1_reg  | imm=0 | blank : src2_reg |
//2: | opcode | scc | dest_reg | src1_reg  | imm=1 | immediate offset |
//3: | opcode |     | data_reg | index_reg |       | immediate offset |
//4: | opcode |     | cond/off |         pc relative offset           |
//5: | opcode |     | dest_reg |           |       |                  |

var conditions = ['alw', 'e', 'ne', 'l', 'le', 'g', 'ge', 'b', 'be', 'a', 'ae'];

var op_categories = {
	'reg-reg' : ['add', 'addc', 'sub', 'rsub', 'subc', 'rsubc', 'and', 'or', 'xor', 'sla', 'sra', 'sll', 'srl'], //addressing modes 1, 2
	'mem' : ['ldl', 'ldlsu', 'ldss', 'ldbu', 'ldbs', 'stl', 'sts', 'stb'], //addressing mode 3
	'absolute' : ['jmp', 'call', 'ret'], //addressing mode 3
	'pc-relative' : ['jmpr', 'callr'], //addressing mode 4
	'misc' : ['gtlpc', 'gtin'] //addressing mode 5
};

Instruction.prototype.bits = function(){
	var op_id = ops.indexOf(this.op);
	var data = op_id << 24;

	switch(this.type){
		case 'reg-reg': //dest reg, src1 reg, src2/offset
			if (this.cond) data |= (1 << 24);
			data |= this.operands[0].value << 19;
			data |= this.operands[1].value << 14;
			if (this.operands[1].type == 'imm') data |= (1 << 13);
			data |= (this.operands[2].value) & 0x1FFF;
			break;

		case 'load': //data reg, index reg, offset
			data |= this.operands[0].value << 19;
			data |= this.operands[1].value << 14;
			data |= 1 << 13; //always uses immediate
			data |= (this.operands[2].value) & 0x1FFF;
			break;

		case 'absolute': //reg, offset, cond/nothing
			data |= conditions.indexOf(this.operands[2].value) << 19;
			data |= this.operands[0].value << 14;
			data |= 1 << 13; //always uses immediate
			data |= (this.operands[1].value) & 0x1FFF;
			break;

		case 'pc-relative': //reg/cond, offset
			if (this.op == 'jmpr') data |= conditions.indexOf(this.operands[0]) << 19;
			else data |= this.operands[0] << 19;
			data |= (this.operands[1]) & 0x7FFFF;
			break;

		case 'misc': //dest reg
			data |= this.operands[0] << 19;
			break;
	}

	return data;
}

var registerFile = function(){
	var globals = null;
	var data = null;
	var cwp = 0;

	return {
		init : function(num_banks){
			globals = new Array(10);
			for (var i = 0; i < 10; i++){
				globals[i] = 0;
			}

			var regs = num_banks * 16 + 6;
			data = new Array(num_banks * 22);
			for (var i = 0; i < num_banks * 22; i++){
				data[i] = 0;
			}

			cwp = (num_banks-1) * 22;
		}
	}
}();

//may change $lines
function parse_code(lines){
	var spanner = function(cl, str){
		return '<span class="' + cl + '">' + str + '</span>';
	}

	var extract_immediate = function(groups){
		if (groups[0]) return 0;
		else if (groups[1]) return parseInt(groups[1]);
		else if (groups[2]) return parseInt(groups[2], 8);
		else return parseInt(groups[3], 16);
	}

	//returns true on overflow
	var overflow_check = function(value, bits){
		var min = -(1 << (bits - 1));
		var max = (1 << (bits - 1)) - 1;
		return (value < min || value > max);
	}

	var register_index_check = function(index){
		return (index < 0 || index > 32);
	}

	var out_lines = [];

	for (var li in lines){
		var line = lines[li];

		//ignore anthing after ;, i.e. comments
		var inline_comment = '';
		var comment_index = line.indexOf(';');
		if (comment_index >= 0){
			inline_comment = line.substr(comment_index);
			line = line.substr(0, comment_index);
		}

		if (line.match(/^\s*$/)){
			//blank line
			continue;
		}

		//TODO long term: finite state accepter, error messages
		//TODO: group everything, to be able to tell the offset of every interesting part
		var immediate_hash_regex_str = "#([+-]?0)|#([+-]?[1-9][0-9]*)|#0([1-7][0-7]*)|#0x([0-9a-f]+)"; //immediate operand
		var immediate_regex_str = immediate_hash_regex_str.replace(/#/g, '');
		var register_regex_str = "[rR]([0-9]|[1-9][0-9]*)"; //register operand
		var conditions_regex = "alw|e|ne|l|le|g|ge|b|be|a|ae";
		var label_regex_str = "[a-f_][0-9a-f_-]*";

		var regs = {
			//ADD Ra, S, Rd - where S can be Rb or a 13bit #immediate
			arith_shift : new RegExp("^(\\s*)(ADD|ADDC|SUB|RSUB|SUBC|RSUBC|AND|OR|XOR|SLA|SRA|SLL|SRL)(\\s+)(" + 
				register_regex_str + ")(\\s*,\\s*)((" + register_regex_str + ")|(" + immediate_hash_regex_str + 
				"))(\\s*,\\s*)(" + register_regex_str + ")(\\s*\\{c\\})?(\\s*)$", 'i'),
			//              0   1    2     3    4     5        6   7 8    9         10 11   12      13     14       15  16    17       18     19
			//match groups: ( space, op, space, ( register ), sep, ( ( register ) , (  0, decimal, octal, hex ) ), sep, ( register ), carry, space )
			
			//LDL (Ra)S, Rd - where S must be a 15 bit immediate (no #)
			load : new RegExp("^(\\s*)(LDL|LDSU|LDSS|LDBU|LDBS)(\\s+)(\\(\\s*" + register_regex_str + "\\s*\\))(\\s*)(" + 
				immediate_regex_str + ")(\\s*,\\s*)(" + register_regex_str + ")(\\s*)$", 'i'),
			//              0   1     2    3    4     5         6    7 8    9        10    11     12  13    14        15
			//match groups: ( space, op, space, ( register ), space, ( 0, decimal, octal, hex ), sep, ( register ), space )

			//STL Rd, (Ra)S - where S must be a 15 bit immediate (no #)
			store : new RegExp("^(\\s*)(STL|STS|STB)(\\s+)(" + register_regex_str + ")(\\s*,\\s*)(\\(\\s*" + register_regex_str + 
				"\\s*\\))(\\s*)(" + immediate_regex_str + ")(\\s*)$", 'i'),
			//              0   1     2    3    4    5         6   7    8          9   10 11    12      13    14      15
			//match groups: ( space, op, space, ( register ), sep, ( register ), space, ( 0, decimal, octal, hex ), space )

			//JMP cond, S(Rd) - where S must be immediate (no #)
			jmp : new RegExp("^(\\s*)(JMP)(\\s+)(" + conditions_regex + ")(\\s*,\\s*)(" + immediate_regex_str + ")(\\(\\s*" +
				register_regex_str + "\\s*\\))(\\s*)$", 'i'),
			//              0   1     2    3     4     5   6 7     8       9    10    11    12        13
			//match groups: ( space, op, space, cond, sep, ( 0, decimal, octal, hex ), ( register ), space )

			//CALL Rd, S(Rn) - where S must be immediate (no #)
			call : new RegExp("^(\\s*)(CALL)(\\s+)(" + register_regex_str + ")(\\s*,\\s*)(" + immediate_regex_str + 
				")(\\s*\\(\\s*" + register_regex_str + "\\s*\\))(\\s*)$", 'i'),
			//              0   1    2     3    4    5         6   7 8     9      10     11   12    13         14
			//match groups: ( space, op, space, ( register ), sep, ( 0, decimal, octal, hex ), ( register ), space )

			//JMPR cond, T - where T is either #immediate (pc + offset) or label (resolved to an offset)
			jmpr : new RegExp("^(\\s*)(JMPR)(\\s+)(" + conditions_regex + ")(\\s*,\\s*)((" + immediate_hash_regex_str + 
				")|(" + label_regex_str + "))(\\s*)$", 'i'),
			//              0  1      2    3      4    5   6 7 8     9      10     11    12  13         14
			//match groups: ( space, op, space, cond, sep, ( ( 0, decimal, octal, hex ), ( label ) ), space )

			//CALLR Rd, T - where T is either #immediate (pc + offset) or label (resolved to an offset)
			callr_ret : new RegExp("^(\\s*)(CALLR|RET)(\\s+)(" + register_regex_str + ")(\\s*,\\s*)((" + immediate_hash_regex_str +
				")|(" + label_regex_str + "))(\\s*)$", 'i'),
			//              0   1     2    3    4     5        6   7 8 9    10      11     12    13  14        15
			//match groups: ( space, op, space, ( register ), sep, ( ( 0, decimal, octal, hex ), ( label ) ), space )

			//GTLPC Ra
			get : new RegExp("^(\\s*)(GTLPC|GTIN)(\\s+)(" + register_regex_str + ")(\\s*)$", 'i')
			//              0    1   2     3    4     5         6
			//match groups: ( space, op, space, ( register ), space )
		};

		var reg_matched = null;
		var gr = null;

		for (var ri in regs){
			gr = line.match(regs[ri]);
			if (gr){
				reg_matched = regs[ri];
				break;
			}
		}

		var arg1, arg2, arg3;
		var overflow = false;
		var bad_reg = false;

		var out;
		
		switch(reg_matched){
			case regs.arith_shift:
				arg1 = new Operand('reg', parseInt(gr[5]));
				bad_reg = register_index_check(arg1.value);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('reg', gr[4]) + gr[6];
				if (gr[8]){ //2nd arg is register
					arg2 = new Operand('reg', parseInt(gr[9]));
					bad_reg = bad_reg || register_index_check(arg2.value);
					out += spanner('reg', gr[8]);
				}
				else{ //2nd arg is #immediate
					arg2 = new Operand('imm', extract_immediate(gr.slice(11, 15)));
					overflow = overflow_check(arg2.value, 13);
					out += spanner('imm', gr[10]);
				}
				arg3 = new Operand('reg', parseInt(gr[17]));
				bad_reg = bad_reg || register_index_check(arg3.value);
				out += gr[15] + spanner('reg', gr[16]);
				if (gr[18]) {
					out += spanner('flag', gr[18]);
				}
				out += gr[19];
				break;

			case regs.load:
				arg1 = new Operand('reg', parseInt(gr[5]));
				bad_reg = register_index_check(arg1.value);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('reg', gr[4]) + gr[6];

				arg2 = new Operand('imm', extract_immediate(gr.slice(8, 12)));
				overflow = overflow_check(arg2.value, 15);
				out += spanner('imm', gr[7]);
	
				arg3 = new Operand('reg', parseInt(gr[14]));
				bad_reg = bad_reg || register_index_check(arg3.value);
				out += gr[12] + spanner('reg', gr[13]) + gr[15];
				break;

			case regs.store:
				arg1 = new Operand('reg', parseInt(gr[5]));
				bad_reg = register_index_check(arg1.value);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('reg', gr[4]) + gr[6];

				arg2 = new Operand('reg', parseInt(gr[8]));
				bad_reg = bad_reg || register_index_check(arg2.value);
				out += spanner('reg', gr[7]) + gr[9];

				arg3 = new Operand('imm', extract_immediate(gr.slice(11, 15)));
				overflow = overflow_check(arg3.value, 15);
				out += spanner('imm', gr[10]) + gr[15];
				break;

			case regs.jmp:
				arg1 = new Operand('cond', gr[4]);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('cond', gr[4]) + gr[5];

				arg2 = new Operand('imm', extract_immediate(gr.slice(7, 11)));
				overflow = overflow_check(arg2.value, 15);
				out += spanner('imm', gr[6]);

				arg3 = new Operand('reg', parseInt(gr[12]));
				bad_reg = bad_reg || register_index_check(arg3.value);
				out += spanner('reg', gr[11]) + gr[13];
				break;

			case regs.call:
				arg1 = new Operand('reg', parseInt(gr[5]));
				bad_reg = register_index_check(arg1.value);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('reg', gr[4]) + gr[6];

				arg2 = new Operand('imm', extract_immediate(gr.slice(8, 12)));
				overflow = overflow_check(arg2.value, 15);
				out += spanner('imm', gr[7]);

				arg3 = new Operand('reg', gr[13]);
				bad_reg = bad_reg || register_index_check(arg3.value);
				out += spanner('reg', gr[12]) + gr[14];
				break;

			case regs.jmpr:
				arg1 = new Operand('cond', gr[4]);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('cond', gr[4]) + gr[5];

				if (gr[7]){ //2nd operand is an immediate
					arg2 = new Operand('imm', extract_immediate(gr.slice(8, 12)));
					overflow = overflow_check(arg2.value, 15);
					out += spanner('imm', gr[7]);
				}
				else{ //2nd operand is a label
					arg2 = new Operand('label', gr[13]);
					out += spanner('label', gr[12]);
				}
				out += gr[14];
				break;

			case regs.callr_ret:
				arg1 = new Operand('reg', gr[5]);
				bad_reg = register_index_check(arg1.value);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('reg', gr[4]) + gr[6];

				if (gr[8]){ //2nd operand is an immediate
					arg2 = new Operand('imm', extract_immediate(gr.slice(9, 13)));
					overflow = overflow_check(arg2.value, 15);
					out += spanner('imm', gr[8]);
				}
				else{ //2nd operand is a label
					arg2 = new Operand('label', gr[14]);
					out += spanner('label', gr[13]);
				}
				out += gr[15];
				break;

			case regs.get:
				arg1 = new Operand('reg', parseInt(gr[5]));
				bad_reg = register_index_check(arg1.value);
				out = gr[1] + spanner('op', gr[2]) + gr[3] + spanner('reg', gr[4]) + gr[6];
				break;
		}

		//short term TODO: specific errors for evorflow
		if (!reg_matched){
			out = spanner('error', line) + "< badly formed expression";
		}
		else if (bad_reg || overflow){
			out = spanner('error', line);
			if (bad_reg){
				out += "< invalid register index. ";
			}
			if (overflow){
				out += "< immediate literal overflow. ";
			}
		}

		out_lines[li] = out + inline_comment;
	}

	return out_lines;
}

function init(){
	code_textarea = document.getElementById("code_textarea");
	code_div = document.getElementById("code_div");

	code_div.style.display = 'none';

	code_textarea.onblur = function(){
		var code = code_textarea.value;

		var lines = code.split(/\r?\n/);
		var div_lines = lines;// validate_code(lines);

		code_div.innerHTML = div_lines.join("<br/>");

		code_textarea.style.display = 'none';
		code_div.style.display = 'block';
	}

	code_div.onfocus = function(){
		code_div.style.display = 'none';
		code_textarea.style.display = 'block';
		code_textarea.focus();
	}
}

