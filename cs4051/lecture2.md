## nth lecture october 3

### Mouse design
see interviews by Moggridge


### Memory
encode, then retrieve
people are better at recognising than recalling
better at recalling images than words (hence UI button design)

encoding involves filtering unneeded information. 
Often recognised (rehearsed) information becomes remembered.

sensory memories for each stimuli

short term memory - rapic access and decay, small capacity
long term memory - slower(way slower) access and decay, large capacity

memory decays slowly.
information may be replaced and 
old facts forgotten (retroactive inhibition)
or vice versa (proactive inhibition)

recall may be aided somewhat by cue.

To reduce information overload, cues are used instead of the full information content. examples include icons, thumbnails, abbreviations

memories are made in relation to a context - so it is harder to recall in a different context.


Use colors and shapes are cues for the meaning of UI elements.

Personal information management causes problems of grouping, sorting and searching since computers nowadways have large storage spaces.
recall-directed and recognition-based searching.

added metadata stores relevant information for various types of files, allowing sorting/searching beyond file names and types (eg. music by a certain artist)

### Mental model
Users do not directly know how the system works, instead they form a simple model based on what the system does for them.

Knowledge is described as a mental model:

- How to use the system
- What to do in unfamiliar situations

Mental models may not be correct.

They may be unconscious processes that are difficult to explicitly recall.

deep vs shallow model (how to drive a car vs how it works)
