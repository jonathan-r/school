#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

//which letter is at the position of the crossing
int significantLetter;

int sortCmp(const void* left, const void* right){
	const char* a = *((const char**) left);
	const char* b = *((const char**) right);
	return a[significantLetter] - b[significantLetter];
}

//returns an array of tightly packet words that match the given length, with NULL terminators.
//returns NULL if no such words have been found
char** pickWords(char** dict, int dictWords, int length, int* amount){
	//first count the number of words
	int count = 0;
	for (int i = 0; i < dictWords; i++) if (strlen(dict[i]) == length) count++;
	
	*amount = count;
	if (count > 0){
		char** words = malloc(count * sizeof(char*));
		char** w = words; //iterator
		for (int i = 0; i < dictWords; i++){
			if (strlen(dict[i]) == length) *w++ = dict[i];
		}
		
		return words;
	}
	
	else return NULL;
}

//prints all n*m combinations of n vertical and m horizontal words
void printCombinations(char** vWords, char** hWords, int vStart, int hStart, int vEnd, int hEnd){
	for (int i = vStart; i < vEnd; i++){
		for (int j = hStart; j < hEnd; j++){
			printf("vertical \"%s\" and horizontal \"%s\"\n", vWords[i], hWords[j]);
		}
	}
}

void completeCrosswordFragment(int vertical, int verticalIntersection, int horizontal, int horizontalIntersection, char** dict, int dictLength){
	//pick out which words would work
	int horizontalWordsCount, verticalWordsCount;
	char** verticalWords   = pickWords(dict, dictLength, vertical, &verticalWordsCount);
	char** horizontalWords = pickWords(dict, dictLength, horizontal, &horizontalWordsCount);
	
	if (horizontalWordsCount > 0 && verticalWordsCount > 0){
		//sort both the vertical and horizontal words list by the respective significant letter
		significantLetter = verticalIntersection;
		qsort(verticalWords, verticalWordsCount, sizeof(char*), sortCmp);
		
		significantLetter = horizontalIntersection;
		qsort(horizontalWords, horizontalWordsCount, sizeof(char*), sortCmp);
	
		//merge-intersection, that finds all the groups of words that can be paired
		int h = 0;
		int v = 0;
		
		//the start of the region of matching significant letters
		int hRegionStart = -1;
		int vRegionStart = -1;
		int regionLength = 0;
		
		while (h < horizontalWordsCount && v < verticalWordsCount){
			//significant letter
			char sh = horizontalWords[h][horizontalIntersection];
			char sv = verticalWords[v][verticalIntersection];
			
			if (sh == sv){
				if (regionLength == 0){
					hRegionStart = h;
					vRegionStart = v;
					regionLength++;
				}
				else regionLength++;
				
				h++;
				v++;
			}
			
			else{
				if (regionLength > 0){
					//print all combinations
					printCombinations(verticalWords, horizontalWords, vRegionStart, hRegionStart, v, h);
					regionLength = 0;
				}
				
				if (sh < sv) h++;
				else v++;
			}
			
			if (regionLength > 0){
				//print the last region
				printCombinations(verticalWords, horizontalWords, vRegionStart, hRegionStart, v, h);
			}
		}
	}
	
	else{
		printf("there are no words that match the vertical or horizontal length in the dictionary (or both)\n");
	}
	
	if (horizontalWords) free(horizontalWords);
	if (verticalWords) free(verticalWords);
}

int main(int argc, char** argv){
	if (argc < 6){
		printf("usage: %s <vertical length> <vertical cross> <horizontal length> <horizontal cross> <dictionary words 1> [other words] [...]\n", argv[0]);
	}
	
	else{
		int vLength = atoi(argv[1]);
		int vCross = atoi(argv[2]);
		int hLength = atoi(argv[3]);
		int hCross = atoi(argv[4]);
		
		completeCrosswordFragment(vLength, vCross, hLength, hCross, &argv[5], argc - 5);
	}
	
	return 0;
}
		
