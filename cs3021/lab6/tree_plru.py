from base import BaseCache

class TreePlruCache(BaseCache):
	"""Cache with binary-tree pseudo-least-recently-used replacement policy."""
	def __init__(self, L, N, K, addr_bits):
		assert 2 ** (int.bit_length(K) - 1) == K #N must be a power of 2
		BaseCache.__init__(self, L, N, K, addr_bits)

	def on_reset(self):
		if self.K > 1:
			#one tree per cache line
			self.tree = [[False for i in range(self.K - 1)] for i in range(self.N)]

			#tree is encoded as follows:
			#        0
			#      /   \
			#     /     \
			#    1       4
			#   / \     / \
			#  2   3   5   6
			# x x x x x x x x
			# 0 1 2 3 4 5 6 7 <- way

		else:
			self.tree = None

	def update_tree(self, set_id, way_id):
		#update bits to point away from accessed node
		bit_id = 0
		offset = 1 << (int.bit_length(self.K) - 2)
		i = int.bit_length(self.K - 1) - 1
		while offset > 0:
			#if (way_id & offset) > 0: #any different?
			if way_id & (1 << i) > 0: #going right
				self.tree[set_id][bit_id] = False #point to the left
				bit_id += offset

			else: #going left
				self.tree[set_id][bit_id] = True #point to the right
				bit_id += 1

			offset = offset >> 1
			i -= 1

	def on_hit(self, tag_id, set_id, way_id):
		if self.tree:
			self.update_tree(set_id, way_id)

	#returns which way to place the data
	def on_miss(self, tag_id, set_id):
		if self.tree:
			#traverse the tree and compute the way index
			way_id = 0
			bit_id = 0
			offset = 1 << (int.bit_length(self.K) - 2)
			while offset > 0:
				if self.tree[set_id][bit_id]:
					way_id += offset
					bit_id += offset #skip the left subtree
				else:
					bit_id += 1 #move on to the left child
				offset = offset >> 1 #move down one level of the tree

			self.update_tree(set_id, way_id)
			return way_id

		else:
			return 0 #only one way

	def show_extra_info_column_name(self):
		return 'plru tree'

	def show_extra_info(self, set_id):
		if self.tree:
			#plru tree as a string of binary
			return ''.join(map(lambda x : '01'[int(x)], self.tree[set_id]))

		else:
			return ''