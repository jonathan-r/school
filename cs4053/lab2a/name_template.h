#pragma once

#include <string>

#include <string.h>
#include <stdlib.h>

struct NameTemplate {
	const char* name;
	int qstIdx;
	int digits;

	NameTemplate(const char* format) {
		name = format;
		const char* tplq = strchr(format, '?');
		if (tplq) {
			digits = 1;
			qstIdx = tplq - format;
			while (*(++tplq) == '?') digits++;
		}
		else {
			digits = 0;
			qstIdx = 0;
		}
	}

	bool isSequence() const {
		return digits > 0;
	}

	//"name?.jpg" and id=5 to "name5.jpg" (sequence)
	//"name???.jpg" and id=15 to "name015.jpg" (sequence)
	//"name.jpg" to "name.jpg" (single image)
	std::string makeImgName(int id) {
		char out[256];
		if(isSequence()) {
			memcpy(out, name, qstIdx);
			char* atPrefix = out + qstIdx;
			sprintf(atPrefix, "%0*d%s", digits, id, name + qstIdx + digits);
			return out;
		}
		else {
			return name;
		}
	}
};
