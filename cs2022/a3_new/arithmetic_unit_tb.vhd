library ieee;
use ieee.std_logic_1164.all;
 
entity arithmetic_unit_tb is
end arithmetic_unit_tb;
 
architecture behavior of arithmetic_unit_tb is 
    component arithmetic_unit
    port(a, b : in  std_logic_vector(15 downto 0);
         op : in  std_logic_vector(2 downto 0);
         result : out  std_logic_vector(15 downto 0);
         v, c : out  std_logic);
    end component;
    
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');
   signal op : std_logic_vector(2 downto 0) := (others => '0');
   signal result : std_logic_vector(15 downto 0);
   signal v : std_logic;
   signal c : std_logic;
	
	constant delay : time := 50ns;
	
begin
   uut: arithmetic_unit port map (a, b, op, result, v, c);

   stim_proc: process begin
		wait for delay;
		
		report "pass through";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010
		op <= "000";
		wait for delay;
		assert result = x"abcd";
		assert v = '0';
		assert c = '0';
		
		report "pass through";
		op <= "111";
		wait for delay;
		assert result = x"abcd";
		assert v = '0';
		assert c = '0';
		
		report "increment";
		a <= x"abcd"; --1010 1011 1100 1101
		op <= "001";
		wait for delay;
		report "increment";
		assert result = x"abce";
		assert v = '0';
		assert c = '0';
		
		report "increment with carry out";
		a <= x"ffff"; --1111 1111 1111 1111
		op <= "001";
		wait for delay;
		assert result = x"0000";
		assert v = '0';
		assert c = '1';
		
		report "sum with no overflow";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010
		op <= "010";
		wait for delay;
		assert result = x"e1a7"; --1110 0001 1010 0111
		assert v = '0';
		assert c = '0';
		
		report "sum with overflow and carry";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"b5da"; --1011 0101 1101 1010
		op <= "010";
		wait for delay;
		assert result = x"61a7"; --0110 0001 1010 0111
		assert v = '1';
		assert c = '1';
		
		report "sum + 1";
		a <= x"abcd"; --1010 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010
		op <= "011";
		wait for delay;
		assert result = x"e1a8"; --1110 0001 1010 1000
		assert v = '0';
		assert c = '0';
		
		report "difference - 1";
		a <= x"4bcd"; --0100 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010 
		--	  x"ca25"; --1100 1010 0010 0101 (~b)
		op <= "100";
		wait for delay;
		assert result = x"15f2"; --0001 0101 1111 0010
		assert v = '0'; --result should be positive
		assert c = '1';
		
		report "difference";
		a <= x"4bcd"; --0100 1011 1100 1101
		b <= x"35da"; --0011 0101 1101 1010 
		--	  x"ca26"; --1100 1010 0010 0110 (~b + 1)
		op <= "101";
		wait for delay;
		assert result = x"15f3"; --0001 0101 1111 0011
		assert v = '0'; --result should be positive
		assert c = '1';
		
		report "decrement with borrow";
		a <= x"abcd"; --1010 1011 1100 1101
		op <= "110";
		wait for delay;
		assert result = x"abcc";
		assert v = '0';
		assert c = '1'; --borrow
		
		report "decrement without borrow";
		a <= x"0000";
		op <= "110";
		wait for delay;
		assert result = x"FFFF";
		assert v = '0';
		assert c = '0'; --borrow
		
		wait;
   end process;
end;
