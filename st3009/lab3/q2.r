N = 10000

weights <- rnorm(n=N, mean=70, sd=12)
samples <- matrix(data=weights, ncol=8)
sums <- rowSums(x=samples)
over <- length(which(sums > 640))
print(over / N)
