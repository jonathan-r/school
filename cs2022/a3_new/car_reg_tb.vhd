--really works

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ca_reg_tb is
end ca_reg_tb;

architecture behavior of ca_reg_tb is 
	component ca_reg is port (
		addr : in std_logic_vector(7 downto 0);
		branch : in std_logic;
		clock, reset : in std_logic;
		output : inout std_logic_vector(7 downto 0) );
	end component;

	signal addr : std_logic_vector(7 downto 0) := (others => '0');
	signal branch : std_logic := '1';
	signal clock : std_logic := '1';
	signal reset : std_logic := '1';
	signal output : std_logic_vector(7 downto 0);

	constant clock_period : time := 20 ns;	

begin
	uut : ca_reg port map(addr, branch, clock, reset, output);
	
	clock_process : process begin
		clock <= '0';
		wait for clock_period/2;

		clock <= '1';
		wait for clock_period/2;
	end process;

	stim_proc: process begin
		wait for clock_period;
		
		--initial value
		reset <= '1';
		wait for clock_period;
		reset <= '0';
		assert output = x"00";
		
		--load value
		addr <= x"43"; --0100 0011
		branch <= '1';
		wait for clock_period;
		assert output = x"43";
		
		--increment a bunch of times
		branch <= '0';
		for i in 1 to 50 loop
			wait for clock_period;
			assert output = std_logic_vector(to_unsigned(16#43# + i, 16));
		end loop;
		
		--overflow
		addr <= x"ff";
		branch <= '1';
		wait for clock_period;
		
		--increment here
		branch <= '0';
		wait for clock_period;
		assert output = x"00"; --go back to 0
		
		wait;
	end process;
end;
