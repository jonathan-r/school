library ieee;
use ieee.std_logic_1164.all;

entity full_adder is
    port ( carry_in  : in   std_logic;
           a         : in   std_logic;
           b         : in   std_logic;
           sum       : out  std_logic;
           carry_out : out  std_logic);
end full_adder;

architecture behavioral of full_adder is
begin
	process(carry_in, a, b) begin
		sum <= (a xor b) xor carry_in after 2ns;
		carry_out <= ((a xor b) and carry_in) or (a and b) after 3ns;
	end process;
end behavioral;
