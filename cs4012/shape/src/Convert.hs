{-# LANGUAGE OverloadedStrings #-}
module Convert where

import Text.Blaze.Svg11 ((!))
import Text.Blaze (stringValue)
import qualified Text.Blaze.Svg11 as S
import qualified Text.Blaze.Svg11.Attributes as A
--import Text.Blaze.Svg.Renderer.Pretty (renderSvg)

import Style
import Shapes

makeSVG :: Drawing -> (Int, Int) -> S.Svg
makeSVG shapes (width, height) =
    S.svg ! A.version "1.1" ! 
        A.width (stringValue.show $ width) ! 
        A.height (stringValue.show $ height) $
        addShapes shapes (width, height)
            --S.rect ! A.transform (S.rotate 50) ! A.width "100" ! A.height "200" ! A.fill "#aabbccff"

addShapes :: Drawing -> (Int, Int) -> S.Svg
addShapes [] _ = mempty
addShapes ((_, Empty, _):ta) dims = addShapes ta dims
addShapes ((tr, Circle, st):ta) (w, h) = 
    svg >> addShapes ta (w, h)
    where
        svg = foldl (!) (S.circle ! cx ! cy ! r ! trans) styles :: S.Svg
        cx = A.cx (stringValue.show $ fromIntegral w / 2.0)
        cy = A.cy (stringValue.show $ fromIntegral h / 2.0)
        r = A.r (stringValue $ show $ (fromIntegral $ minimum [w, h]) / 2.0)
        trans = A.transform $ composeTransforms tr
        styles = flattenStyles st        

addShapes ((tr, Square, st):ta) (w, h) = 
    svg >> addShapes ta (w, h)
    where
        svg = foldl (!) (S.rect ! x ! y ! wi ! he ! trans) styles
        min = minimum [w, h]
        max = maximum [w, h]
        hh = (fromIntegral (max - min)) / 2.0
        x = A.x (stringValue.show $ if' (min == w) 0 hh)
        y = A.y (stringValue.show $ if' (min == w) hh 0)
        wi = A.width (stringValue.show $ min)
        he = A.height (stringValue.show $ min)
        trans = A.transform $ composeTransforms tr
        styles = flattenStyles st

composeTransforms :: Transform -> S.AttributeValue
composeTransforms Identity = ""
composeTransforms (Translate (Vector x y)) = S.translate x y
composeTransforms (Scale (Vector x y)) = S.scale x y
composeTransforms (Rotate mat) = makeMatrix mat
composeTransforms (Compose tr1 tr2) =
    mconcat [composeTransforms tr1, " ", composeTransforms tr2]

makeMatrix :: Matrix -> S.AttributeValue
makeMatrix (Matrix (Vector a b) (Vector c d)) = S.matrix a b c d 0 1

flattenStyles :: Style -> [S.Attribute]
flattenStyles Default = []
flattenStyles (Fill col) = [A.fill $ stringValue $ col2hex col]
flattenStyles (Stroke col) = [A.stroke $ stringValue $ col2hex col]
flattenStyles (EdgeWidth x) = [A.strokeWidth $ stringValue.show $ x]
flattenStyles (And st1 st2) = flattenStyles st1 ++ flattenStyles st2

if' :: Bool -> a -> a -> a
if' True a _ = a
if' False _ b = b