EXCLUDE="(Test)";

#all java files, one per line
ALL_FILES=$( ls | egrep .java$ );

#take all files, exclude whichever lines should not be included
NEEDED_FILES=$( echo "$ALL_FILES" | egrep -v "$EXCLUDE" );

#write the list to a file
TMP_FILE=$( mktemp );
echo "$NEEDED_FILES" > "$TMP_FILE";

#show the file names and archive them
cat "$TMP_FILE";
tar cf packed_part3.tar -T "$TMP_FILE";
