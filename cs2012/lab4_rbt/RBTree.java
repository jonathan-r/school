/*
 * Objects of this class represent Binary Tree nodes.
 */
public class RBTree<Key extends Comparable<Key>, Value> {
	private TreeNode<Key, Value> root;

	/**
	 * Constructor
	*/
	public RBTree(){
		root = null;
	}

	/**
	 * Inserts key $k and value $v into the tree.
	 * If a node with key $k already exists, its value is updated to $v
	 * Otherwise, a new node is created.
	 * @param k : new key
	 * @param v : new value
	*/
	public void insert(Key k, Value v){
		if (root == null) root = new TreeNode<Key, Value>(k, v);
		else root = root.insert(k, v);
		root.red = false;
	}

	/**
	 * @return the value of the node with key $k, or null if it doesn't exists.
	 * @param k : key to search for
	*/
	public Value valueOf(Key k){
		if (root == null) return null;
		else{
			TreeNode<Key, Value> n = root.find(k);
			if (n != null) return n.val;
			else return null;
		}
	}

	/**
	 * Computes the height of the tree.
	 * @return the number of steps in the longest possible top-down traversal of the tree.
	 * @time always O(n)
	*/
	public int height(){
		if (root == null) return 0;
		else return root.height();
	}

	/**
	 * @return the number nodes in the tree
	 * @time always O(1)
	*/
	public int size(){
		if (root == null) return 0;
		else return root.size;
	}

	/**
	 * The most nodes a tree with height h can have is 2^h
	 * @return the ratio of the actual number of nodes to the most possible nodes or 0.0 if the tree is empty
	 * @time O(n)
	*/
	public double utilisation(){
		int s = size();
		if (s == 0) return 0.0; //avoid division by zero
		else return (double)s / (Math.pow(2, height()) - 1);//(double)(1L << height());
	}

	/**
	 * @return the node with the lowest value, or null if the tree is empty
	 * @time O(height)
	 * @space none
	*/
	public TreeNode<Key, Value> getMinNode(){
		if (root == null) return null;
		else return root.getMin();
	}

	/**
	 * @return the node with the highest value, or null if the tree is empty
	 * @time O(height)
	 * @space none
	*/
	public TreeNode<Key, Value> getMaxNode(){
		if (root == null) return null;
		else return root.getMax();
	}

	/**
	 * @return the node with the median (tending left) value, or null if the tree is empty
	 * @time O(height)
	 * @space O(1)
	*/
	public TreeNode<Key, Value> getMedianNode(){
		if (root == null) return null;
		TreeNode<Key, Value> n = root;
		int at = 0;
		if (root.left != null) at = root.left.size;
		int target = (root.size + 1) / 2 - 1;
		
		while (at != target){
			if (at < target){
				n = n.right;
				at += 1;
				if (n.left != null) at += n.left.size;
			}
			else{
				n = n.left;
				at -= 1;
				if (n.right != null) at -= n.right.size;
			}
		}

		return n;
	}

	/**
	 * Removes the value associated with key $k from the tree if it is there
	 * @param k : the key of the node to delete
	 * @time O(height)
	 * @space O(1)
	*/
	public void delete(Key k){
		if (root != null){
			//find the node with key $k, and its parent
			TreeNode<Key, Value> parent = null;
			TreeNode<Key, Value> node = root;

			while (node != null){
				int cmp = k.compareTo(node.key);
				if (cmp == 0){
					deleteNode(parent, node);
					return;
				}
				else{
					parent = node;
					node.size--; //assume the node with key $k exists
					if (cmp < 0) node = node.left;
					else node = node.right;
				}
			}

			//if we reached this point, the node did not exist
			node = root;
			while (node != null){
				node.size++;

				int cmp = k.compareTo(node.key);
				if (cmp < 0) node = node.left;
				else node = node.right;
			}
		}
	}

	/**
	 * @return a String of space separated values associated with all the keys in the range low through high (inclusive)
	 * @param low : the lowest(inclusive) key to include in the string
	 * @param low : the highest(inclusive) key to include in the string
	 * @time O(n)
	 * @space O(n)
	*/
	public String rangeToString(Key low, Key high){
		if (root == null) return "";
		else{
			StringBuilder str = new StringBuilder();
			rangeToStringInner(low, high, root, str);
			if (str.length() > 0) str.deleteCharAt(str.length() - 1); //remove the last space
			return str.toString();
		}
	}

	

	/**
	 * @return a String containing the entire tree laid out horizontally
	 * @time O(n)
	 * @space O(n)
	*/
	public String prettyPrint(){
    	if (root == null) return "";
    	else{
    		boolean[] showBar = new boolean[height()];
    		StringBuilder str = new StringBuilder();
    		prettyPrintInner(0, root, str, showBar);
    		return str.toString();
    	}
	}
	
	/**
	 * @return whether the tree is a binary search tree (true if there are no nodes)
	 * @time O(n)
	 * @space O(1)
	 */
	public boolean isBST(){
		if (root == null) return true; //hmm
		else return root.isBST();
	}

	/**
	 * @return whether the tree is a binary search tree (true if there are no nodes)
	 * @time O(n)
	 * @space loads
	 */
	public boolean isRBT(){
		if (root == null) return true; //hmm
		else return root.isRBT();
	}
	
	//to be called exclusively from delete(Key)
	private void deleteNode(TreeNode<Key, Value> p, TreeNode<Key, Value> n){
		if (n.left == null && n.right == null){ //$n is childless
			//delete the reference to $n in $p
			//in this case, p.size was already decremented
			if (p == null){ //$n is the root
				root = null;
			}
			else{ //$n is somewhere below the root
				if (n == p.left) p.left = null;
				else p.right = null;
			}
		}

		else if (n.left != null && n.right != null){ //$n has two children
			TreeNode<Key, Value> pred = n.left; //predecessor of $n
			TreeNode<Key, Value> predParent = n; //parent of $pred
			n.size--; //it's losing its predecessor

			while (pred.right != null){
				pred.size--; //$pred will be deleted, so this subtree is losing a node
				predParent = pred;
				pred = pred.right;
			}

			//replace $n with $pred
			n.key = pred.key;
			n.val = pred.val;

			deleteNode(predParent, pred); //requisite recursion
		}

		else{ //$n has one child
			//replace $n with its child
			TreeNode<Key, Value> c = (n.left != null)? n.left : n.right;
			n.key = c.key;
			n.val = c.val;
			n.left = c.left;
			n.right = c.right;
			n.size--; //lost a child
		}
	}
	
	private void prettyPrintInner(int depth, TreeNode<Key, Value> node, StringBuilder str, boolean[] showBar){
		for (int i = 0; i < depth; i++){
			if (showBar[i]) str.append(" |");
			else str.append("  ");
		}

		if (node != null) {
			str.append((node.red)? '=' : '-');
			str.append(node.val.toString());
			str.append('\n');

			showBar[depth] = true;
			prettyPrintInner(depth + 1, node.left, str, showBar);

			showBar[depth] = false;
			prettyPrintInner(depth + 1, node.right, str, showBar);
		}

		else {
			str.append("-null");
			str.append('\n');
		}
	}
	
	private void rangeToStringInner(Key low, Key high, TreeNode<Key, Value> node, StringBuilder str){
		int cmpL = node.key.compareTo(low);
		int cmpR = node.key.compareTo(high);
		if (node.left != null && cmpL > 0) rangeToStringInner(low, high, node.left, str);
		if (cmpL >= 0 && cmpR <= 0){
			str.append(node.val);
			str.append(' ');
		}
		if (node.right != null && cmpR < 0) rangeToStringInner(low, high, node.right, str);
	}
}
