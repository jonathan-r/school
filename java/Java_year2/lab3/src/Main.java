
public class Main {
	private static boolean palindrome(int x, int length){
		if (x < (int)Math.pow(10, length-1)) return false; // must be 6 digits
		else{
			int r = x;
			int div = (int)Math.pow(10, length-1);
			
			for (int i = 0; i < length / 2; i++){
				int dr = r % 10;
				r /= 10;
				
				int dl = (x / div) % 10;
				div /= 10;
				
				if (dl != dr) return false;
			}
		}
		
		return true;
	}
	
	private static int maxPalindromeNaive(int factorLength, int palindromeLength){
		int start = (int)(Math.pow(10, factorLength) - 1);
		for (int l = start; l > 0; l--){
			for (int r = start; r > 0; r--){
				int x = l * r;
				
				if (palindrome(x, palindromeLength)) return x;
			}
		}
		
		return 0;
	}
	
	private static int maxPalindromeEvenMultiple11(int length){
		if (length % 2 == 1) return 0; //must be even
		
		int largest = (int)Math.pow(10, length) - 1;
		int multiple = (int)(Math.floor(largest / 11));
		
		for (int m = multiple; m > 0; m--){
			int p = m * 11;
			if (palindrome(p, length)) return p;
		}
		
		return 0;
	}
	
	public static void main(String[] args){
		int p = maxPalindromeNaive(3, 6);
		return;
	}
}
 