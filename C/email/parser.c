#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// Parses a token consisting of alphanumeric character
// start: begginning of the alphanum to parse
// returns a pointer to the character after the end of the alphanum
char* read_alphanum(char* start){
	char* c = start;
	while (isalpha(*c) || isdigit(*c) || *c == '-') c++;
	if (c == start) return NULL;
	else return c;
}

// Parses an identifier in the format <alpha>[alphanum[alphanum]...]
// start : beggining of the identifier to parse
// returns a pointer to the character after the end of the identifier
char* read_identifier(char* start){
	char* c = start;
	if (isalpha(*c)){
		c++;	
		char* a = read_alphanum(c);
		if (a) return a; //there is more than one character in the identifier
		else return c; //there is only the one character in the identifier
	}
	else return NULL;
}

// Parses a list of identifier as defined above, that are each separated by dots
// start: the beginning of the identifier list
// lastIdentifier: container of pointer that will be set to the start of the last (or only) identifier, or NULL
// returns the character after the end of the identifier list
char* read_identifier_list(char* start, char** lastIdentifier){
	if (lastIdentifier) *lastIdentifier = NULL; //in case there is no identifer here at all
	char* c = read_identifier(start); //read the first (and possibly only) identifier
	if (c){
		if (lastIdentifier) *lastIdentifier = start; //in case there's only one identifier
		while (*c == '.'){
			c++; //skip the .
			char* a = read_identifier(c);
			if (a){
				if (lastIdentifier) *lastIdentifier = c;
				c = a;
			}
			else{
				//there is a . but no identifier after it
				return NULL;
			}
		}
	}

	return c;
}

// Parses an email address in the format <identifier list>@<identifier_list>.<top level domain>
// start: beginning of the address to parse
// validDomains: array of pointers to null terminated strings containing the valid domains
// validDomainsCount: the amount of pointers in the above array
// domainValid: NULL or a pointer to an int that will be set to 1 if the address is well formed and has a valid top level domain
//              or 0 if it is not well formed or it does not have a valid top level domain
char* read_email(char* start, char** validDomains, int validDomainsCount, int* domainValid){
	//read user name
	char* nameEnd = read_identifier_list(start, NULL);

	if (nameEnd){
		if (*nameEnd == '@'){
			//read domain
			nameEnd++;
			char* topLevel = NULL;
			char* end = read_identifier_list(nameEnd, &topLevel);

			if (end && topLevel != nameEnd){ //there must be at least one identifier before the top level domain
				if (domainValid){
					//only check top level domain if the caller cares to know if it's valid
					int topLevelLength = (int)(end - topLevel);
					int i;

					*domainValid = 0;
					for (i = 0; i < validDomainsCount; i++){
						if (!strncasecmp(topLevel, validDomains[i], topLevelLength)){
							*domainValid = 1;
							break;
						}
					}
				}
	
				//this is avalid email address expression, whether its top level domain is valid or not
				return end;
			}
		}
	}

	//not a valid address
	if(domainValid) *domainValid = 0;
	return NULL;
}

void print_validity(char* address, char** terminators, int length){
	printf("The string %s is ", address);
	int valid = 0;
	char* end = read_email(address, terminators, length, &valid);
	if (!end || !valid){
		printf("not ");
	}
	printf("a valid email address\n");
}

int main(int argc, char** argv){
	if (argc >= 3){
		int names = argc - 2;
		print_validity(argv[1], &argv[2], names);
	}

	else{
		printf("usage: %s <email address> <terminator0> [terminator1] [ ... [terminatorN]]\n\n", argv[0]);
	}

	return 0;
}
