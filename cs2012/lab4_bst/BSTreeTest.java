import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

//-------------------------------------------------------------------------
/**
 *  Test class for Binary Search Tree
 *
 *  @author  
 *  @version 18/02/14 16:47:51
 */

@RunWith(JUnit4.class)
public class BSTreeTest{
	public BSTreeTest(){
		//constructor test
		new BSTree<Integer, String>();
	}

	@Test
	public void empty(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();

		//assertEquals( "Checking find non-existent node of empty tree", null, tree.find(24) );
		assertEquals( "Checking valueOf non-existent node of empty tree", null, tree.valueOf(24) );
		assertEquals( "Checking height of empty tree", 0, tree.height() );
		assertEquals( "Checking size of empty tree", 0, tree.size() );
		assertEquals( 0.0, tree.utilisation(), 0.0 );
		assertEquals( "Checking getMinNode of empty tree", null, tree.getMinNode() );
		assertEquals( "Checking getMaxNode of empty tree", null, tree.getMaxNode() );
		assertEquals( "Checking getMedianNode of empty tree", null, tree.getMedianNode() );
		assertEquals( "Checking rangeToString of empty tree", "", tree.rangeToString(0, 1) );
		assertEquals( "Checking prettyPrint of empty tree", "", tree.prettyPrint() );
		assertEquals( "Checking isBST of empty tree", true, tree.isBST());
		tree.delete(-1); //cover those lines
	}

	@Test
	public void oneAddition(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();

		tree.insert(91, "test");

		//assertEquals( "Checking find non-existent node of empty tree", "test", tree.find(91).val );
		assertEquals( "Checking valueOf the only node of the tree", "test", tree.valueOf(91) );
		assertEquals( "Checking height of tree", 1, tree.height() );
		assertEquals( "Checking size of tree", 1, tree.size() );
		assertEquals( 1.0, tree.utilisation(), 0.1 );
		assertEquals( "Checking getMinNode of tree", "test", tree.getMinNode().val );
		assertEquals( "Checking getMaxNode of tree", "test", tree.getMaxNode().val );
		assertEquals( "Checking getMedianNode of tree", "test", tree.getMedianNode().val );
		assertEquals( "Checking empty rangeToString of tree", "", tree.rangeToString(-10, 10) );
		assertEquals( "Checking non-empty rangeToString of tree", "test", tree.rangeToString(90, 100) );
		assertEquals( "Checking prettyPrint of empty tree", "-test\n |-null\n  -null\n", tree.prettyPrint() );
	}

	@Test
	public void fiveAdditions(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();

		tree.insert(0, "zero");
		tree.insert(1, "one");
		tree.insert(-1, "minus-one");
		tree.insert(9, "nine");
		tree.insert(3, "three");

		//should be
		//    0
		// -1   1
		//        9
		//       3

		//assertEquals( "Checking find some existing node", "minus-one", tree.find(-1).val );
		//assertEquals( "Checking find some existing node", "nine", tree.find(9).val );

		assertEquals( "Checking valueOf some existing node", "zero", tree.valueOf(0) );
		assertEquals( "Checking valueOf some existing node", "three", tree.valueOf(3) );

		assertEquals( "Checking height of tree", 4, tree.height() );
		assertEquals( "Checking size of tree", 5, tree.size() );
		assertEquals( 5.0 / ((1 << 4) - 1), tree.utilisation(), 0.1 );

		assertEquals( "Checking getMinNode of tree", "minus-one", tree.getMinNode().val );
		assertEquals( "Checking getMaxNode of tree", "nine", tree.getMaxNode().val );
		assertEquals( "Checking getMedianNode of tree", "one", tree.getMedianNode().val );

		assertEquals( "Checking partial rangeToString of tree", "zero one", tree.rangeToString(0, 2) );
		assertEquals( "Checking full rangeToString of tree", "minus-one zero one three nine", tree.rangeToString(-10, 100) );
		assertEquals( "Checking prettyPrint of empty tree", "-zero\n |-minus-one\n | |-null\n |  -null\n  -one\n   |-null\n    -nine\n     |-three\n     | |-null\n     |  -null\n      -null\n", tree.prettyPrint() );
	}
	
	@Test 
	public void prettyPrinting(){
		BSTree<Character, Character> tree = new BSTree<Character, Character>();
		for (char c : "SXEARCHM".toCharArray()) tree.insert(c, c);
		assertEquals( "Checking prettyPrint of empty tree", 			
			"-S\n" +
			" |-E\n" +
			" | |-A\n" +
			" | | |-null\n" +
			" | |  -C\n" +
			" | |   |-null\n" +
			" | |    -null\n" +
			" |  -R\n" +
			" |   |-H\n" +
			" |   | |-null\n" +
			" |   |  -M\n" +
			" |   |   |-null\n" +
			" |   |    -null\n" +
			" |    -null\n" +
			"  -X\n" +
			"   |-null\n" +
			"    -null\n" , tree.prettyPrint() );
	}

	@Test
	public void deletion(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();
		
		tree.insert(5, "five");
		tree.delete(5); //delete the root
		assertEquals( "1 - Checking getMinNode of tree", null, tree.getMinNode() );
		assertEquals( "1 - Checking getMaxNode of tree", null, tree.getMaxNode() );
		assertEquals( "1 - Checking getMedianNode of tree", null, tree.getMedianNode() );
		assertEquals( "1 - Checking height of tree", 0, tree.height() );
		assertEquals( "1 - Checking size of tree", 0, tree.size() );

		
		//as above
		tree.insert(0, "zero");
		tree.insert(1, "one");
		tree.insert(-1, "minus-one");
		tree.insert(9, "nine");
		tree.insert(3, "three");

		tree.delete(-2); //shouldn't do anything
		assertEquals( "1 - Checking getMinNode of tree", "minus-one", tree.getMinNode().val );
		assertEquals( "1 - Checking getMaxNode of tree", "nine", tree.getMaxNode().val );
		assertEquals( "1 - Checking getMedianNode of tree", "one", tree.getMedianNode().val );
		assertEquals( "1 - Checking height of tree", 4, tree.height() );
		assertEquals( "1 - Checking size of tree", 5, tree.size() );


		tree.delete(3); //trivial case

		//    0
		// -1   1
		//        9

		assertEquals( "2 - Checking getMinNode of tree", "minus-one", tree.getMinNode().val );
		assertEquals( "2 - Checking getMaxNode of tree", "nine", tree.getMaxNode().val );
		assertEquals( "2 - Checking getMedianNode of tree", "zero", tree.getMedianNode().val );
		assertEquals( "2 - Checking height of tree", 3, tree.height() );
		assertEquals( "2 - Checking size of tree", 4, tree.size() );

		
		tree.delete(1); //one child

		//    0
		// -1   9

		assertEquals( "3 - Checking getMinNode of tree", "minus-one", tree.getMinNode().val );
		assertEquals( "3 - Checking getMaxNode of tree", "nine", tree.getMaxNode().val );
		assertEquals( "3 - Checking getMedianNode of tree", "zero", tree.getMedianNode().val );
		assertEquals( "3 - Checking height of tree", 2, tree.height() );
		assertEquals( "3 - Checking size of tree", 3, tree.size() );


		tree.insert(8, "eight");
		tree.insert(10, "ten");
		tree.delete(9); //two children

		//    0
		// -1   8
		//        10		

		assertEquals( "4 - Checking partial rangeToString of tree", "minus-one zero eight ten", tree.rangeToString(-10, 10) );
		assertEquals( "4 - Checking getMinNode of tree", "minus-one", tree.getMinNode().val );
		assertEquals( "4 - Checking getMaxNode of tree", "ten", tree.getMaxNode().val );
		assertEquals( "4 - Checking getMedianNode of tree", "zero", tree.getMedianNode().val );
		assertEquals( "4 - Checking height of tree", 3, tree.height() );
		assertEquals( "4 - Checking size of tree", 4, tree.size() );
	}
	
	@Test
	public void isBSTValid(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();
		Random rand = new Random();
		final int insertions = 5000;
		
		for (int i = 0; i < insertions; i++){
			tree.insert(rand.nextInt(), randomStr(rand, 5));
		}
		
		assertEquals( "Checking isBST on a tree made by insertion", true, tree.isBST());
	}

	@Test
	public void isBSTInvalid(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();
		tree.insert(0, "");
		tree.insert(-5, "");
		tree.insert(50, "");

		tree.root.left.key = 105; //invalidates the rule leftchild.key < this.key
		
		assertEquals( "Checking isBST on a tree made by insertion", false, tree.isBST());
	}
	
	@Test
	public void sorting(){
		BSTree<Integer, Integer> tree = new BSTree<Integer, Integer>();
		Random rand = new Random();
		final int size = 50000;
		
		//populate the keys and vals
		int[] keys = new int[size];
		for (int i = 0; i < size; i++) keys[i] = i; //ascending order
		
		//shuffle
		for (int i = 0; i < size * 5; i++){
			int l = rand.nextInt(size);
			int r = rand.nextInt(size);
			int temp = keys[l];
			keys[l] = keys[r];
			keys[r] = temp;
		}
		
		//insert into the tree
		for (int i = 0; i < size; i++) tree.insert(keys[i], keys[i]);
		
		//build the sorted keys (which is just 0 .. size) into a string
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < size; i++){
			str.append(i);
			if (i < size - 1) str.append(' ');
		}
		
		assertEquals( "Checking that the tree can sort stuff", size, tree.size() );
		assertEquals( "Checking that the tree can sort stuff", new Integer(0), tree.getMinNode().val );
		assertEquals( "Checking that the tree can sort stuff", new Integer(size-1), tree.getMaxNode().val );
		assertEquals( "Checking that the tree can sort stuff", new Integer((size + 1) / 2 - 1), tree.getMedianNode().val );
		assertEquals( "Checking that the tree can sort stuff", str.toString(), tree.rangeToString(Integer.MIN_VALUE, Integer.MAX_VALUE) );
		assertEquals( "Checking isBST on a tree made by insertion", true, tree.isBST());
	}
	
	@Test
	public void randomUse(){
		BSTree<Integer, String> tree = new BSTree<Integer, String>();
		HashMap<Integer, String> map = new HashMap<Integer, String>(); //replicates $tree, with assured functionality
		Random rand = new Random();
		final int maxKey = 1000;
		
		for (int use = 0; use < 25000; use++){
			if (rand.nextInt(30) > 0){ //29 out of 30
				if (rand.nextInt(5) < 3){ //insert
					int key = rand.nextInt(maxKey);
					String val = randomStr(rand, 3 + rand.nextInt(7));

					tree.insert(key, val);
					map.put(key, val);
					
					String valInTree = tree.valueOf(key);
					assertEquals( "Checking that the insertion worked", val, valInTree);
				}
				
				else{ //delete
					int key = rand.nextInt(maxKey);
					String valBefore = tree.valueOf(key);
					String valInMap = map.remove(key);
					
					tree.delete(key);
					
					String valAfter = tree.valueOf(key);
					if (valInMap != null){
						assertEquals( "Checking that the right value exists in the map", valInMap, valBefore);
						assertEquals( "Checking that the deletion was successful", null, valAfter );
					}
					else{
						assertEquals( "Checking that the right value exists in the map", null, valBefore);
					}
				}
			}
			
			else{ //check that the tree and map are in sync
				if (map.size() > 0){
					int[] keys = new int[map.size()];
					int i = 0;
					for (Map.Entry<Integer, String> entry : map.entrySet()){
						int key = entry.getKey().intValue();
						keys[i++] = key;
						assertEquals( "Checking that a key from the map exists in the tree", entry.getValue(), tree.valueOf(key) );
					}
					
					Arrays.sort(keys); //is this needed?
					StringBuilder str = new StringBuilder();
					
					for (i = 0; i < map.size(); i++){
						str.append(map.get(keys[i]));
						if (i < map.size() - 1) str.append(' ');
					}
				
					assertEquals( "Checking that all the values in the map exist in the tree", str.toString(), tree.rangeToString(Integer.MIN_VALUE, Integer.MAX_VALUE) );
					assertEquals( "Checking getMinNode of tree", map.get(keys[0]), tree.getMinNode().val );
					assertEquals( "Checking getMaxNode of tree", map.get(keys[map.size()-1]), tree.getMaxNode().val );
					assertEquals( "Checking getMedianNode of tree", map.get(keys[(map.size() + 1) / 2 - 1]), tree.getMedianNode().val );
					assertEquals( "Checking size of tree", map.size(), tree.size() );
					assertEquals( "Checking isBST", true, tree.isBST());
				}
			}
		}
	}
	
	private static String randomStr(Random rand, int length){
		final int maxLen = 100;
		
		char[] word = new char[maxLen];
		if (length > maxLen) length = maxLen;
	
		final int range = 'z' - 'a';
		for (int l = 0; l < length; l++) word[l] = (char) ('a' + (char)rand.nextInt(range));
		return new String(word, 0, length);
	}
}



