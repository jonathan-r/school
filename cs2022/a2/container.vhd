library ieee;
use ieee.std_logic_1164.all;

entity container is
    port ( reg_write : in  std_logic;
           reg_dest : in  std_logic_vector (2 downto 0);
           reg_src_a : in  std_logic_vector (2 downto 0);
           reg_src_b : in  std_logic_vector (2 downto 0);
           constant_in : in  std_logic_vector (15 downto 0);
           mb_select : in  std_logic;
			  func : in std_logic_vector (4 downto 0);
           md_select : in  std_logic;
           data_in : in  std_logic_vector (15 downto 0);
			  clk : in std_logic;
           addr_out : out  std_logic_vector (15 downto 0);
           data_out : out  std_logic_vector (15 downto 0);
			  v : out std_logic;
			  c : out std_logic;
			  n : out std_logic;
			  z : out std_logic );
end container;

architecture behavioral of container is
	component register_file is
		port ( write_reg   : in std_logic;
		    dest_select : in   std_logic_vector (2 downto 0);
			 data_in     : in std_logic_vector (15 downto 0);
		    clk         : in   std_logic;
		    a_select    : in   std_logic_vector (2 downto 0);
			 b_select    : in   std_logic_vector (2 downto 0);
			 a_out       : out  std_logic_vector (15 downto 0);
			 b_out       : out  std_logic_vector (15 downto 0) );
	end component;
	
	component function_unit is
		port ( a : in  std_logic_vector (15 downto 0);
           b : in  std_logic_vector (15 downto 0);
           func : in  std_logic_vector (4 downto 0);
           v : out  std_logic;
           c : out  std_logic;
           n : out  std_logic;
           z : out  std_logic;
           f : out  std_logic_vector (15 downto 0) );
	end component;
	
	component mux2_16bit is
		port ( in0 : in  std_logic_vector (15 downto 0);
				 in1 : in  std_logic_vector (15 downto 0);
		       addr : in std_logic;
				 output : out  std_logic_vector (15 downto 0) );
	end component;
	
	signal reg_out_b : std_logic_vector (15 downto 0); --regfile b output
	signal bus_a : std_logic_vector (15 downto 0); --reg_file a to alu and addr_out
	signal bus_b : std_logic_vector (15 downto 0); --const_mux to alu and data_out
	signal alu_out : std_logic_vector(15 downto 0); --alu to md_mux
	signal md_out : std_logic_vector(15 downto 0); --md_mux to regfile data in

begin
	--wire directly
	addr_out <= bus_a;
	data_out <= bus_b;
	
	reg_file : register_file port map(reg_write, reg_dest, md_out, clk, reg_src_a, reg_src_b, bus_a, reg_out_b);
	const_mux : mux2_16bit port map(reg_out_b, constant_in, mb_select, bus_b);
	functional : function_unit port map(bus_a, bus_b, func, v, c, n, z, alu_out);
	md_mux : mux2_16bit port map(alu_out, data_in, md_select, md_out);
	
	
end behavioral;

