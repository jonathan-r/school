#include <stdlib.h>
#include <stdint.h>

#include "huffman.h" //definition of hcode and symbol

//a fragment of decoded text
#define FRAGMENT_SIZE 512
typedef struct fragment_struct{
	int bytes;
	uint8_t data[FRAGMENT_SIZE];
	struct fragment_struct* next;
} fragment;


//free a binary symbol tree
void tree_free(symbol* branch){
	symbol* left = branch->lower[0];
	symbol* right = branch->lower[1];
	
	free(branch);
	
	if (left){
		tree_free(left);
		tree_free(right);
	}
}

//uint8_t is synonymous with unsigned char, and comes from stdint.h
//encoded should be a tightly packed (a the bit level) huffman encoded NULL terminated string. the null terminator is also encoded
//codes should be an array of 256 hcodes, for every byte value
uint8_t* huffman_decode(uint8_t encoded[], hcode[] codes){
	//create a symbol tree from the codes, for efficient decoding
	symbol* root = malloc(sizeof(symbol));
	root->lower[0] = root->lower[1] = NULL;
	root->c = 0;
	
	for (int character = 0; character < 256; character++){
		if (codes[character].len > 0){
			symbol** s = &root;
			int right = 0; //whether we went to the right last time
			for (int d = codes[character].len - 1; d >= 0; d--){
				if ((codes[character].data >> d) & 0x1) s = &((*s)->lower[1]); //branch right
				else s = &((*s)->lower[0]); //branch left
				
				if (!(*s)){
					//extend the tree
					*s = malloc(sizeof(symbol));
					(*s)->lower[0] = (*s)->lower[1] = NULL;
					(*s)->c = 0;
				}
			}
			
			(*s)->c = character; //end of the path; this node stands for the current character
		}
	}
	
	//we don't know the length of the decoded string, so we'll keep adding fragments to fit it all.
	//create the first fragment
	fragment* first = malloc(sizeof(fragment));
	memset(first, 0, sizeof(fragment)); //sets every byte of first to 0
	
	//iterators
	fragment* last = first;
	symbol* at = root; //tree iterator
	int decoded = 0; //length of the decoded string, including the NULL terminator
	int end = 0; //whether the end was reached
	
	for (int inb = 0; inb < bytes_compressed; inb++){
		uint8_t byte = encoded[inb];
		for (int bit = 7; bit >= 0; bit--){
			//traverse the tree
			if ((byte >> bit) & 0x1) at = at->lower[1];
			else at = at->lower[0];
			
			if (!at->lower[0]){
				//end of the branch
				last->data[last->bytes++] = at->c; //store the character
				decoded++;
				at = root; //reset the branch iterator
				
				if (at->c == 0){
					//end of the string
					end = 1;
					break;
				}
				
				if (last->bytes == FRAGMENT_SIZE){
					//make a new page
					last->next = malloc(sizeof(fragment));
					last = last->next;
					last->bytes = 0;
					last->next = NULL;
				}
			}
		}
	}
	
	//free the symbol tree
	tree_free(root);
	
	//gather all the fragments into a single buffer, freeing the fragments along the way
	uint8_t* buf = malloc(decoded);
	uint8_t* pour = buf; //iterator
	for (fragment* f = first; f; f = f->next){
		memcpy(pour, f->data, f->bytes);
		pour += f->bytes;
		free(f);
	}
	
	return buf;
}


