library ieee;
use ieee.std_logic_1164.all;
use work.array_types.all;


entity register_file is
	port ( src      : in   std_logic_vector (2 downto 0);
		    des      : in   std_logic_vector (2 downto 0);
		    clk      : in   std_logic;
		    data_src : in   std_logic;
		    ext_data : in   std_logic_vector (15 downto 0);
		    reg_out  : out data_array );
end register_file;

architecture behavioral of register_file is
	component decoder_3to8 is
		port ( a : in  std_logic_vector (2 downto 0);
			    q : out std_logic_vector (7 downto 0) );
	end component;

	component register_16bit is
		port ( load : in  std_logic;
			    clk  : in  std_logic;
			    d    : in  std_logic_vector (15 downto 0);
			    q    : out std_logic_vector (15 downto 0) );
	end component;

	component mux8_16bit is
		port ( s      : in  std_logic_vector (2 downto 0);
			   in_data : in  data_array;
			   z       : out std_logic_vector (15 downto 0) );
	end component;

	component mux2_16bit is
		port (s   : in  std_logic;
			   in0 : in  std_logic_vector (15 downto 0);
			   in1 : in  std_logic_vector (15 downto 0);
			   z   : out std_logic_vector (15 downto 0) );
	end component;

	signal load : std_logic_vector (7 downto 0);
	signal reg_data : std_logic_vector (15 downto 0);
	signal reg_in_data : std_logic_vector(15 downto 0);
	signal reg_out_data : data_array;

begin
	dest_decoder : decoder_3to8 port map(des, load);
	src_mux : mux8_16bit port map(src, reg_out_data, reg_data);
	data_mux : mux2_16bit port map(data_src, ext_data, reg_data, reg_in_data);
	gen_reg : 
		for i in 0 to 7 generate
			regx : register_16bit port map(load(i), clk, reg_in_data, reg_out_data(i));
		end generate gen_reg;

	process(src, des, clk, data_src, ext_data, reg_out_data) begin
		for i in 0 to 7 loop
			reg_out(i) <= reg_out_data(i);
		end loop;
	end process;
end behavioral;

