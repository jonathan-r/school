
book: Logic in Computer Science: modelling and Reasoning, Huth, Ryan

- Symbolic logic
- Correctness of imperative programs (Floyd-Hoare)
- Correctness of functional programs

use [Dafny](research.microsoft.com/en-us/projects/dafny) or Why3

marks:
- 7% attendance
- 43% assignsments
- 50% exam (January)
