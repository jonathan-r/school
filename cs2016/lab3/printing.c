#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <ncurses.h>

#include "twister.h"

const int num_printers = 3;
int page_width; //in characters
int page_height; //in characters

int log_height; //from the bottom of the page
int log_at; //line on which the next log line will be printed

const char* bishop_translation = " o+=*BOX@&#/^";

//print job
typedef struct print_job_s{
	uint8_t hash[16];
	int ink_color;
	int page_color;
	int id;
} print_job;

//queue
typedef struct queue_node_s{
	struct queue_node_s* next;
	print_job job;
} queue_node;


//printer waking system
pthread_mutex_t printer_wait_lock;
pthread_cond_t printer_wait_cond;

//client waking system
pthread_mutex_t client_wait_lock;
pthread_cond_t client_wait_cond;

//semaphore
int sem_available; //number of available printers
int most_jobs_done; //set by the printer that did the most job
queue_node* queue_first; //highest priority job
queue_node* queue_last; //end
pthread_mutex_t queue_lock; //queue pointers and sem_available are protected by this


//this is only locked for single, atomic (screen) print operations, not part of the semaphore system
pthread_mutex_t screen_lock;
void log_message(const char* format, ...){
	va_list args;
	pthread_mutex_lock(&screen_lock);

	va_start(args, format);
	move(log_at, 0);
	vwprintw(stdscr, format, args); //ncurses
	va_end(args);
	
	refresh(); //ncurses, write changes to screen

	log_at++;
	if (log_at == LINES) log_at = LINES - log_height;

	pthread_mutex_unlock(&screen_lock);
}

//printer thread.
//one instance for each printer
typedef struct printer_data_s{
	int id;

	int jobs_done;
	char* page_buffer; //sized page_width * page_height

	//page offset on screen
	int off_rows;
	int off_cols;
} printer_data;

void* printer(void* data){
	printer_data* self = data;

	pthread_mutex_lock(&screen_lock);

	move(0, self->off_cols);
	wprintw(stdscr, "printer %d online", self->id);
		
	refresh(); //ncurses, write changes to screen
	pthread_mutex_unlock(&screen_lock);

	while (1){
		pthread_mutex_lock(&printer_wait_lock); //one by one the threads will acquire this lock
		pthread_cond_wait(&printer_wait_cond, &printer_wait_lock);
		//get the job description and see if it's for us
		if (!queue_first || self->jobs_done > most_jobs_done){
			//rest
			pthread_mutex_unlock(&printer_wait_lock);
			continue;
		}

		//get to work
		pthread_mutex_unlock(&printer_wait_lock);
		
		//pop the job queue
		pthread_mutex_lock(&queue_lock);
		queue_node* job_node = queue_first;
		queue_first = queue_first->next;
		if (!queue_first) queue_last = NULL;
		sem_available--;
		pthread_mutex_unlock(&queue_lock);

		log_message("printer %d: got print job 0x%x", self->id, job_node->job.id);

		//set ncurses color
		init_pair(self->id, job_node->job.ink_color, job_node->job.page_color);

		//slide down the new page
		for (int l = 0; l < page_height; l++){
			pthread_mutex_lock(&screen_lock);

			attron(COLOR_PAIR(self->id)); //turn color on
			move(self->off_rows + l, self->off_cols);
			for (int i = 0; i < page_width; i++) addch(' ');
			refresh();
			attroff(COLOR_PAIR(self->id)); //turn color off

			pthread_mutex_unlock(&screen_lock);

			usleep(50 * 1000);
		}

		//drunken bishop https://gist.github.com/malexmave/6262465
		memset(self->page_buffer, 0, page_width * page_height);

		int x = page_width / 2;
		int y = page_height / 2;

		for (int i = 0; i < 16; i++){
			for (int p = 0; p < 4; p++){
				int move = (job_node->job.hash[i] >> (p * 2)) & 0x3;
				x += (move & 0x1)? 1 : -1;
				y += (move & 0x2)? 1 : -1;

				if (x < 0) x = 0;
				if (y < 0) y = 0;
				if (x == page_width) x = page_width - 1;
				if (y == page_height) y = page_height - 1;

				int ch = ++self->page_buffer[x + y * page_width];
				if (ch > strlen(bishop_translation)) ch = 0;

				//show the character
				pthread_mutex_lock(&screen_lock);

				attron(COLOR_PAIR(self->id)); //turn color on
				move(self->off_rows + y, self->off_cols + x);
				addch(bishop_translation[ch] | A_BOLD);
				refresh();
				attroff(COLOR_PAIR(self->id)); //turn color off

				pthread_mutex_unlock(&screen_lock);

				usleep(20 * 1000);
			}
		}

		//convert the thing
		for (int r = 0; r < page_height; r++){
			for (int c = 0; c < page_width; c++){
				int ch = self->page_buffer[c + r * page_width];
				if (ch > strlen(bishop_translation)) ch = 0;
				self->page_buffer[c + r * page_width] = bishop_translation[ch];
			}
		}

		//slide out the page
		for (int i = 1; i < page_height + 2; i++){
			pthread_mutex_lock(&screen_lock);
			attron(COLOR_PAIR(self->id)); //turn color on

			for (int l = 0; l < page_height - i; l++){
				move(self->off_rows + l, self->off_cols);
				for (int c = 0; c < page_width; c++) addch((&(self->page_buffer[(i + l) * page_width]))[c] | A_BOLD);
				//wprintw(stdscr, &(self->page_buffer[(i + l) * page_width]));
			}

			attroff(COLOR_PAIR(self->id)); //turn color off

			//clear the next line
			move(self->off_rows + page_height - i + 1, self->off_cols);
			for (int i = 0; i < page_width; i++) addch(' ');//wprintw(stdscr, blank_page_line);
			refresh();

			pthread_mutex_unlock(&screen_lock);

			usleep(20 * 1000);
		}

		pthread_mutex_lock(&queue_lock);
		free(job_node);
		self->jobs_done++;
		if (self->jobs_done > most_jobs_done) most_jobs_done = self->jobs_done;
		sem_available++; //this printer is now free
		pthread_mutex_unlock(&queue_lock);
	
		//update counter at the top
		pthread_mutex_lock(&screen_lock);
		move(0, self->off_cols);
		wprintw(stdscr, "printer %d done %d jobs", self->id, self->jobs_done);
		pthread_mutex_unlock(&screen_lock);
		log_message("printer %d: finished printing", self->id);

		//job's done, tell the client in case they're waiting
		pthread_cond_broadcast(&client_wait_cond); //there could be multiple clients waiting, so broadcast
	}

	pthread_exit(NULL);
}


typedef struct client_data_s{
	int id;
	int seed;
	int jobs_sent;
} client_data;
void* client(void* data){
	client_data* self = data;

	twister rand;
	twister_init(&rand, self->seed);

	while(1){
		sleep((twister_uint(&rand) % 2) + 1); //rest awhile

		//design a page
		queue_node* node = malloc(sizeof(queue_node));
		memset(node, 0, sizeof(queue_node));
		node->next = NULL;

		node->job.ink_color = twister_uint(&rand) % 8;
		//while (node->job.page_color == node->job.ink_color) node->job.page_color = twister_uint(&rand) % 8; //make it readable
		node->job.page_color = 1 + twister_uint(&rand) % 7;

		for (int i = 0; i < 4; i++){
			uint32_t x = twister_uint(&rand);
			node->job.hash[0 + (i << 2)] = (x >>  0) & 0xFF;
			node->job.hash[1 + (i << 2)] = (x >>  8) & 0xFF;
			node->job.hash[2 + (i << 2)] = (x >> 16) & 0xFF;
			node->job.hash[3 + (i << 2)] = (x >> 24) & 0xFF;
		}

		node->job.id = twister_uint(&rand) % 100000;

		//send it to some printer
		pthread_mutex_lock(&queue_lock);
		int wait = sem_available <= 0;

		if (!queue_last) queue_first = queue_last = node;
		else{
			queue_last->next = node;
			queue_last = node;
		}

		self->jobs_sent++;
		log_message("client %d: sending job", self->id);
		pthread_mutex_unlock(&queue_lock);

		pthread_cond_broadcast(&printer_wait_cond);

		if (wait){
			//wait, in case the job is not done
			pthread_mutex_lock(&client_wait_lock);
			pthread_cond_wait(&client_wait_cond, &client_wait_lock);
			pthread_mutex_unlock(&client_wait_lock);
		}
	}
}

void terminal_resize(int sig){
	//just don't
	exit(1);
}

int main(int argc, char** argv){
	if (argc != 2){
		printf("Usage: printing <clients>\n");
	}

	else{
		int num_clients = strtol(argv[1], NULL, 10);

		if (num_clients > 0){
			srand(time(NULL));

			//ncurses sutff
			initscr(); 
			start_color(); 
			curs_set(0); //hide cursor
			nodelay(stdscr, 1); //make getch non-blocking

			//ncurses terminal resize handler
			/*struct sigaction sa;
			memset(&sa, 0, sizeof(struct sigaction));
			sa.sa_handler = terminal_resize;
			sigaction(SIGWINCH, &sa, NULL);*/

			//screen proportions
			page_width = COLS / num_printers - 2;
			page_height = LINES * 2 / 3 - 2;

			log_height = LINES - page_height - 3;
			log_at = page_height + 3;

			pthread_t* printer_handles = malloc(num_printers * sizeof(pthread_t));
			pthread_t* client_handles = malloc(num_printers * sizeof(pthread_t));

			printer_data* printers = malloc(num_printers * sizeof(printer_data));
			client_data* clients = malloc(num_clients * sizeof(client_data));

			sem_available = 0;
			queue_first = queue_last = NULL;

			pthread_mutex_init(&queue_lock, NULL);

			pthread_cond_init(&printer_wait_cond, NULL);
			pthread_mutex_init(&printer_wait_lock, NULL);

			pthread_cond_init(&client_wait_cond, NULL);
			pthread_mutex_init(&client_wait_lock, NULL);

			for (int i = 0; i < num_printers; i++){
				printer_data* p = &printers[i];
				p->id = i + 1;
				p->jobs_done = 0;
				p->page_buffer = malloc(page_width * page_height);
				p->off_rows = 1;
				p->off_cols = i * page_width + 1;

				pthread_create(&printer_handles[i], NULL, printer, p);
			}
			
			log_message("started the printers");
			
			//wait a bit for the printers to boot up
			usleep(200000);

			for (int i = 0; i < num_clients; i++){
				client_data* c = &clients[i];
				c->id = i;
				c->seed = rand();
				c->jobs_sent = 0;

				pthread_create(&client_handles[i], NULL, client, c);
			}

			log_message("started the clients");

			while (1){ //busywork
				pthread_mutex_lock(&screen_lock);
				int key = getch(); //ncurses, non blocking here
				pthread_mutex_unlock(&screen_lock);
				if (key == 27 || key == 'q' || key == 'Q') break; //escape, q, Q
				usleep(2000);
			}

			//TODO: kill all threads here

			//deinitialize printers
			for (int i = 0; i < num_printers; i++){
				free(printers[i].page_buffer);
			}

			free(clients);
			free(printers);

			free(printer_handles);
			free(client_handles);

			pthread_cond_destroy(&printer_wait_cond);
			pthread_mutex_destroy(&printer_wait_lock);

			pthread_cond_destroy(&client_wait_cond);
			pthread_mutex_destroy(&client_wait_lock);

			endwin(); //ncurses dies, but does not deallocate everything
		}
	}

	return 0;
}
