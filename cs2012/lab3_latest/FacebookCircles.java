/**
 * Class FacebookCircles: calculates the statistics about the friendship circles in facebook data.
 *
 * @author
 *
 * @version 04/02/14 20:01:16
 */
public class FacebookCircles {
	private int[] member;
	private int[] sizes;
	private int[] depths;

	private boolean circlesSizesFresh; //whether the values below are correct
	private int circlesCount;
	private int largestCircle;
	private int averageCircle;
	private int smallestCircle;

	/**
	 * Constructor
	 * @param numberOfFacebookUsers : the number of users in the sample data.
	 * Each user will be represented with an integer id from 0 to numberOfFacebookUsers-1.
	 * time: O(N) for N users
	 */
	public FacebookCircles(int numberOfFacebookUsers) {
		member = new int[numberOfFacebookUsers];
		depths = new int[numberOfFacebookUsers];
		sizes = new int[numberOfFacebookUsers];
		circlesSizesFresh = false;

		for (int i = 0; i < numberOfFacebookUsers; i++){
			member[i] = i; //each user initially friendless
			depths[i] = 0;
			sizes[i] = 1;
		}
	}

	/** 
	 * finds the index of the set the vertex id belongs to, shortening the tree as it goes.
	 * @param id : index of the vertex being queried
	 * time: O(ln*N) for N members
	 */
	private int find(int id){
		while (id != member[id]){
			member[id] = member[member[id]];
			id = member[id];
		}

		return id;
	}

	/**
	 * creates a friendship connection between two users, represented by their corresponding integer ids.
	 * @param user1 : int id of first user
	 * @param user2 : int id of second  user
	 * time: O(ln*N) for N members
	 */
	public void friends(int user1, int user2){
		//System.out.println("friend " + user1 + " and " + user2);

		if (user1 != user2){
			int s1 = find(user1);
			int s2 = find(user2);

			if (s1 != s2){
				/*if (sizes[s1] <= sizes[s2]){
				  member[user1] = s2; //link user1 directly to the root of user2
				  member[user2] = s2;
				  member[s1] = s2; //link the root of user1 to the root of user2
				  sizes[s2] += sizes[s1];
				//sizes[s1] -= sizes[user1];
				}

				else{//if (sizes[s1] > sizes[s2]){
				member[user2] = s1;
				member[user1] = s1;
				member[s2] = s1;
				sizes[s1] += sizes[s2];
				//sizes[s2] -= sizes[user2];
				}*/

				if (depths[s1] < depths[s2]){
					member[s1] = s2;
					sizes[s2] += sizes[s1];
				}
				else if (depths[s1] > depths[s2]){
					member[s2] = s1;
					sizes[s1] += sizes[s2];
				}
				else{
					member[s2] = s1;
					depths[s1]++;
					sizes[s1] += sizes[s2];
				}
			}
		}
	}

	//compute the size of each set
	//O(N * ln*(N)) for N members
	private void computeCircleSizes(){
		if (!circlesSizesFresh){
			if (member.length > 0){
				smallestCircle = Integer.MAX_VALUE;
				largestCircle = 0;
				circlesCount = 0;

				long sum = 0;

				for (int vert = 0; vert < member.length; vert++){
					int root = member[vert];
					if (root == vert){
						circlesCount++;
						int size = sizes[vert];
						sum += size;
						if (size < smallestCircle) smallestCircle = size;
						if (size > largestCircle) largestCircle = size;
					}
				}
				averageCircle = member.length / circlesCount;
			}

			else{
				//trivial case
				largestCircle = averageCircle = smallestCircle = 0;
			}

			circlesSizesFresh = true;
		}
	}

	/**
	 * @return the number of friend circles in the data already loaded.
	 */
	public int numberOfCircles() {
		computeCircleSizes();
		return circlesCount;
	}

	/**
	 * @return the size of the largest circle in the data already loaded.
	 */
	public int sizeOfLargestCircle() {
		computeCircleSizes();
		return largestCircle;
	}

	/**
	 * @return the size of the average circle in the data already loaded.
	 */
	public int sizeOfAverageCircle() {
		computeCircleSizes();
		return averageCircle;
	}

	/**
	 * @return the size of the smallest circle in the data already loaded.
	 */
	public int sizeOfSmallestCircle() {
		computeCircleSizes();
		return smallestCircle;
	}
}
