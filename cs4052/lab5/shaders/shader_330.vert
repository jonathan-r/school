#version 330

uniform mat4 fullMatrix; //proj * view * model
uniform mat4 worldMatrix; //model
uniform float time; //seconds

in vec3 vPosition; //object
in vec3 vNormal; //object
//in vec3 vTangent; //d tex / d surface
in vec2 vTexCoord;

out vec3 worldVec; //world
out vec3 interpNormal; //world
out vec2 tex;

void main() {
	gl_Position = fullMatrix * vec4(vPosition, 1.0); //eye space
	worldVec = (worldMatrix * vec4(vPosition, 1.0)).xyz; //world space
	interpNormal = (worldMatrix * vec4(vNormal, 0.0)).xyz; //world space - normalization needed for terrain

	tex = vTexCoord;
	//tangent = vTangent; //world
	//biTangent = cross(vNormal, biTangent); //world
}
