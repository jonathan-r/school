require("conf")

Map = {data=nil, roots=nil, img=nil, text=nil}
function Map:new()
	local height = love.image.newImageData(MS, MS)
 	local deriv = love.image.newImageData(MS, MS)
  local color = love.image.newImageData(MS, MS)

	local v, div, mul
	for y = 0,MS-1 do
		for x = 0,MS-1 do
			--v = octaveNoise(x, y, 100, 0.01, 2, 0.75)
			v =     distoredNoise(x * 0.01, y * 0.01, 2) * 200
			v = v + distoredNoise(x * 0.03, y * 0.03, 3) * 50
			v = v + distoredNoise(x * 0.08, y * 0.08, 5) * 10
			v = clamp(math.floor(v), 0, 255)  
      height:setPixel(x, y, v, v, v, 255)
		end
	end
  
  for y = 0,MS-2 do
		for x = 0,MS-2 do
      local a00 = height:getPixel(x, y)
      local a10 = height:getPixel(x+1, y)
      local a01 = height:getPixel(x, y+1)
      local a11 = height:getPixel(x+1, y+1)
			
      local d1 = a10 - a00
      local d2 = a01 - a00
      local d3 = a11 - a00
      local d = 255 - (math.abs(d1) + math.abs(d2) + math.abs(d3))
      
			deriv:setPixel(x, y, d, d, d, 255)
		end
	end
  
  for y = 0,MS-1 do
		for x = 0,MS-1 do
      local h = height:getPixel(x, y)
      local d = deriv:getPixel(x, y)
      local r, g, b
      
      if i < 80 then
        r =  20; g =  20; b = 100
      elseif i < 100 then
        r = 100; g =  80; b =  30
      elseif i < 220 then
        r = 130; g = 200; b = 100
      else
        r = 200; g = 200; b = 200
      end
      
      color:setPixel(x, y, rm[v], gm[v], bm[v], 255)
		end
	end
	
	local himg = love.graphics.newImage(height)
  local dimg = love.graphics.newImage(deriv)
  local cimg = love.graphics.newImage(color)
	himg:setFilter("nearest", "nearest")
  dimg:setFilter("nearest", "nearest")
  cimg:setFilter("nearest", "nearest")
	
	local ob = {
		height = height,
    deriv = deriv,
    color = color,
		himg = himg,
    dimg = dimg,
    cimg = cimg,
    drawim = 0,
		text = love.graphics.newText(love.graphics.getFont(), "")
	}
	
	setmetatable(ob, self)
	self.__index = self
	return ob
end

-- Root = {rx, ry, rootNode = Node}
-- Node = {dx, dy, len, ch = [Node]}

function Map:cellFloor(mx, my)
	local cx, cy = math.floor(mx/CS), math.floor(my/CS)
	if cx >= 0 and cx < MS and cy >= 0 and cy < MS then
		return true, cx, cy
	else
		return false, 0, 0
	end
end

function Map:query(cx, cy)
	local val,occ,_,_ = self.height:getPixel(cx, cy)
	return val, occ
end

function Map:setCell(cx, cy, val, occ)
  self.data:setPixel(cx, cy, val, occ, 0, 255)
end

function Map:click(mx, my)
  local on, cx, cy = self:cellFloor(mx, my)
  if on then
    self.drawim = (self.drawim + 1) % 3
  end
end

function Map:draw()
  love.graphics.setColor(255, 255, 255, 255)
  if drawim == 0 then
    love.graphics.draw(self.cimg, 0, 0, 0, CS, CS)
    love.graphics.print("color", 0, 20, 0, 20, 20)
  elseif drawim == 1 then
    love.graphics.draw(self.dimg, 0, 0, 0, CS, CS)
    love.graphics.print("deriv", 0, 20, 0, 20, 20)
  else
    love.graphics.draw(self.himg, 0, 0, 0, CS, CS)
    love.graphics.print("height", 0, 20, 0, 20, 20)
	end
  
  local mx, my = love.mouse.getPosition()
  local g, cx, cy = self:cellFloor(mx, my)
	
	if g then
		local val, occ = self:query(cx, cy)
		love.graphics.setColor(255, 255, 255, 255)
		self.text:set(string.format("%d (%d)", val, occ))
		love.graphics.draw(self.text, mx, my - self.text:getHeight())
	end
end
