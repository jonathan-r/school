#include <assert.h>
#include <stdlib.h>

#include "ring.h"

void ring_init(ring_buffer* ring, int size){
	ring->data_start = 0;
	ring->data_end = 0;
	ring->stuff = 0;
	ring->size = size;
	ring->data = malloc(size);
}

void ring_destroy(ring_buffer* ring){
	if (ring->data) free(ring->data);
}

int ring_space_left(ring_buffer* ring){
	return ring->size - ring->stuff;
}

int ring_data_left(ring_buffer* ring){
	return ring->stuff;
}

int ring_put(ring_buffer* ring, char* input, int max){
	int space = ring->size - ring->stuff;
	int fill = (max > space)? space : max; //minimum of the two

	for (int i = 0; i < fill; i++){
		ring->data[ring->data_end++] = input[i];
		if (ring->data_end == ring->size) ring->data_end = 0;
	}

	ring->stuff += fill;
	assert(ring->stuff <= ring->size);

	return fill;
}

int ring_get(ring_buffer* ring, char* output, int max){
	int got = (max > ring->stuff)? ring->stuff : max; //minimum of the two

	for (int i = 0; i < got; i++){
		output[i] = ring->data[ring->data_start++];
		if (ring->data_start == ring->size) ring->data_start = 0;
	}

	ring->stuff -= got;
	assert(ring->stuff >= 0);

	return got;
}