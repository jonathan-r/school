#pragma once

#include <fstream>
#include <iostream>

#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

using namespace std;

class ShaderProgram {
public:
	ShaderProgram(const char* vsfn, const char* fsfn) {
		progObj = glCreateProgram();

	    if (progObj == 0) {
	        fprintf(stderr, "Error creating shader program\n");
	        exit(1);
	    }
		
		GLuint vsObj = compileShader(vsfn, GL_VERTEX_SHADER);
		GLuint fsObj = compileShader(fsfn, GL_FRAGMENT_SHADER);

		// Attach the compiled shader objects to the program object
	    glAttachShader(progObj, vsObj);
	    glAttachShader(progObj, fsObj);
		
	    GLint Success = 0;
	    GLchar ErrorLog[1024] = { 0 };
	
		// After compiling all shader objects and attaching them to the program, we can finally link it
	    glLinkProgram(progObj);
		// check for program related errors using glGetProgramiv
	    glGetProgramiv(progObj, GL_LINK_STATUS, &Success);
		if (Success == 0) {
			glGetProgramInfoLog(progObj, sizeof(ErrorLog), NULL, ErrorLog);
			fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
	        exit(1);
		}
	
		// program has been successfully linked but needs to be validated to check whether the program can execute given the current pipeline state
	    glValidateProgram(progObj);
		// check for program related errors using glGetProgramiv
	    glGetProgramiv(progObj, GL_VALIDATE_STATUS, &Success);
	    if (!Success) {
	        glGetProgramInfoLog(progObj, sizeof(ErrorLog), NULL, ErrorLog);
	        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
	        exit(1);
	    }

		// mark shader objects for deletion (will be deleted on program deletion)
		glDeleteShader(vsObj);
		glDeleteShader(fsObj);
	}

	~ShaderProgram() {
		glDeleteProgram(progObj);
	}

	GLuint getProgObj() {
		return progObj;
	}

	/*GLuint getAttribLocation(const char* name) {
		glGetAttribLocation(progObj, "vPosition");
	}*/

private:
	GLuint progObj;
	bool valid;

	GLuint compileShader(const char* fn, GLuint type) {
		ifstream t(fn);
		t.seekg(0, ios::end);
		size_t fileSize = t.tellg();
		t.seekg(0);
		char* buf = (char*)malloc(fileSize + 1);
		t.read(buf, fileSize);
		buf[fileSize] = 0;
		t.close();

		cout << "loading shader " << fn << endl;

		GLuint shaderObj = glCreateShader(type);
	    if (shaderObj == 0) {
	        fprintf(stderr, "Error creating shader type %d\n", type);
	        exit(0);
	    }
		// Bind the source code to the shader, this happens before compilation
		glShaderSource(shaderObj, 1, (const GLchar**)&buf, NULL);
		// compile the shader and check for errors
	    glCompileShader(shaderObj);
	    GLint success;
		// check for shader related errors using glGetShaderiv
	    glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &success);
	    if (!success) {
	        GLchar InfoLog[1024];
	        glGetShaderInfoLog(shaderObj, 1023, NULL, InfoLog);
	        fprintf(stderr, "Error compiling shader type %d: '%s'\n", type, InfoLog);
	        exit(1);
	    }
	
		free(buf);
		return shaderObj;
	}
};
