#version 330

uniform mat4 matrix;
uniform float time;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vColor;

out vec3 normal;
out vec3 color;

void main() {
	vec3 vec = vec3(matrix * vec4(vPosition, 1.0));
    gl_Position = vec4(vec, 1.0);
	normal = vNormal;
	color = vColor;
}
