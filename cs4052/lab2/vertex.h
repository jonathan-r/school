#pragma once

#include "maths/mat4.h"
#include "maths/vec.h"

struct vertex {
	vec3 v;
	vec3 n;
	vec3 c;

	vertex(const vec3& vv, const vec3& nn, const vec3& cc) : v(vv), n(nn), c(cc) {}
	vertex() {}

	static void setAttribPointers(GLuint prog) {
		//vertex shader must use these names for its inputs
		GLuint posLoc = glGetAttribLocation(prog, "vPosition");
		GLuint norLoc = glGetAttribLocation(prog, "vNormal");
		GLuint colLoc = glGetAttribLocation(prog, "vColor");
	
		glEnableVertexAttribArray(posLoc);
		glEnableVertexAttribArray(norLoc);
		glEnableVertexAttribArray(colLoc);
	
		glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, v)));
		glVertexAttribPointer(norLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, n)));
		glVertexAttribPointer(colLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, c)));
	}
};

