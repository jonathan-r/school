#include <stdlib.h>
#include <assert.h>

#include "list.h"

/* create a new, empty linked list set */
struct listset* listset_new(){
	struct listset* set = malloc(sizeof(struct listset));
	set->start = set->end = NULL;
	return set;
}

/* safely destroy any kind of set. also frees the listset memory */
void listset_destroy(struct listset* this){
	struct listnode* node;
	for(node = this->start; node;){
		struct listnode* next = node->next;
		free(node);
		node = next;
	}
	
	free(this);
}

/* check to see if an item is in the set returns 1 if in the set, 0 if not */
int listset_lookup(struct listset* this, int item){
	if (this->start){
		if (item < this->start->data) return 0;
		if (item > this->end->data) return 0;

		struct listnode* node;
		for(node = this->start; node; node = node->next){
			if (node->data == item) return 1;
			else if (node->data > item) return 0;
		}
	}

	return 0;
}

/* add an item, with number 'item' to the set has no effect if the item is already in the set */
void listset_add(struct listset* this, int item){
	struct listnode* node, * at, * last = NULL;
	
	if (this->start){
		if (item > this->end->data){
			node = malloc(sizeof(struct listnode));
			node->data = item;
			node->next = NULL;
			this->end->next = node;
			this->end = node;
		}
		else{
			for (at = this->start; at; at = at->next){
				if (item == at->data) return;
				else if (item < at->data){
					node = malloc(sizeof(struct listnode));
					node->data = item;
					node->next = at;
					if (last) last->next = node;
					else{
						node->next = this->start;
						this->start = node;
					}
					return;
				}

				last = at;
			}
		}
	}
	else{
		node = malloc(sizeof(struct listnode));
		this->start = this->end = node;
		node->next = NULL;
		node->data = item;
	}
}

/* remove an item with number 'item' from the set */
void listset_remove(struct listset* this, int item){
	if (this->start){
		if (item < this->start->data || item > this->end->data) return; //it's not here
		else{
			struct listnode* at, * last = NULL;
			for (at = this->start; at; at = at->next){
				if (item == at->data){
					if (!last) this->start = at->next;
					else last->next = at->next;
					if (at == this->end) this->end = last;
					free(at);

					return;
				}
				else if (item < at->data) return; //it's not here
			}
		}
	}
}

/* place the union of src1 and src2 into dest. dest must be a new set */
void listset_union(struct listset* dest, struct listset* src1, struct listset* src2){
	assert(!dest->start);

	struct listnode* a = src1->start;
	struct listnode* b = src2->start;
	struct listnode* last = NULL, * node = NULL;

	while (a && b){
		node = malloc(sizeof(struct listnode));
		if (a->data < b->data) {
			node->data = a->data;
			a = a->next;
		}
		else if(a->data > b->data){
			node->data = b->data;
			b = b->next;
		}
		else{ //a->data == b->data
			node->data = a->data;
			a = a->next;
			b = b->next;
		}
			
		if (last) last->next = node;
		else dest->start = node;

		last = node;
	}

	//add any remaining items
	int i;
	for (i = 0; i < 2; i++){
		struct listnode* from = (i == 0)? a : b;
		while (from){
			node = malloc(sizeof(struct listnode));
			node->data = from->data;
			if (last) last->next = node;
			else dest->start = node;
	
			last = node;
			from = from->next;
		}
	}

	if (node){
		//at least one was added
		dest->end = node;
		node->next = NULL;
	}
}


/* place the intersection of src1 and src2 into dest. dest must be a new set */
void listset_intersect(struct listset* dest, struct listset* src1, struct listset* src2){
	assert(!dest->start);

	struct listnode* a = src1->start;
	struct listnode* b = src2->start;
	struct listnode* last = NULL, * node = NULL;

	while (a && b){
		if (a->data < b->data) a = a->next;
		else if (a->data > b->data) b = b->next;
		else{
			node = malloc(sizeof(struct listnode));
			node->data = a->data;

			if (last) last->next = node;
			else dest->start = node;
			last = node;
			
			a = a->next;
			b = b->next;
		}
	}

	if (node){
		//at least one element was added
		dest->end = node;
		node->next = NULL;
	}
}

