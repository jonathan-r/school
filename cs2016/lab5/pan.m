#define rand	pan_rand
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* CLAIM not_starved */
	case 3: /* STATE 1 - _spin_nvr.tmp:3 - [(!((!(timeout)||((crashed+done)==2))))] (6:0:0 - 1) */
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[2][1] = 1;
		if (!( !(( !(((trpt->tau)&1))||((now.crashed+now.done)==2)))))
			continue;
		/* merge: assert(!(!((!(timeout)||((crashed+done)==2)))))(0, 2, 6) */
		reached[2][2] = 1;
		spin_assert( !( !(( !(((trpt->tau)&1))||((now.crashed+now.done)==2)))), " !( !(( !(((trpt->tau)&1))||((crashed+done)==2))))", II, tt, t);
		/* merge: .(goto)(0, 7, 6) */
		reached[2][7] = 1;
		;
		_m = 1; goto P999; /* 2 */
	case 4: /* STATE 10 - _spin_nvr.tmp:8 - [-end-] (0:0:0 - 1) */
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[2][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC :init: */
	case 5: /* STATE 1 - ./attempt4.pml:63 - [(run thread(0))] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][1] = 1;
		if (!(addproc(II, 0, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 6: /* STATE 2 - ./attempt4.pml:63 - [(run thread(1))] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][2] = 1;
		if (!(addproc(II, 0, 1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 7: /* STATE 3 - ./attempt4.pml:64 - [-end-] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][3] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC thread */
	case 8: /* STATE 1 - ./attempt4.pml:23 - [i = 1] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][1] = 1;
		(trpt+1)->bup.oval = ((P0 *)this)->i;
		((P0 *)this)->i = 1;
#ifdef VAR_RANGES
		logval("thread:i", ((P0 *)this)->i);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 9: /* STATE 2 - ./attempt4.pml:23 - [((i<=2))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][2] = 1;
		if (!((((P0 *)this)->i<=2)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 10: /* STATE 3 - ./attempt4.pml:33 - [want[self] = 1] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][3] = 1;
		(trpt+1)->bup.oval = ((int)now.want[ Index(((P0 *)this)->self, 2) ]);
		now.want[ Index(((P0 *)this)->self, 2) ] = 1;
#ifdef VAR_RANGES
		logval("want[thread:self]", ((int)now.want[ Index(((P0 *)this)->self, 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 11: /* STATE 4 - ./attempt4.pml:35 - [(want[other])] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][4] = 1;
		if (!(((int)now.want[ Index(((P0 *)this)->other, 2) ])))
			continue;
		_m = 3; goto P999; /* 0 */
	case 12: /* STATE 5 - ./attempt4.pml:36 - [want[self] = 0] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][5] = 1;
		(trpt+1)->bup.oval = ((int)now.want[ Index(((P0 *)this)->self, 2) ]);
		now.want[ Index(((P0 *)this)->self, 2) ] = 0;
#ifdef VAR_RANGES
		logval("want[thread:self]", ((int)now.want[ Index(((P0 *)this)->self, 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 13: /* STATE 6 - ./attempt4.pml:37 - [want[self] = 1] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][6] = 1;
		(trpt+1)->bup.oval = ((int)now.want[ Index(((P0 *)this)->self, 2) ]);
		now.want[ Index(((P0 *)this)->self, 2) ] = 1;
#ifdef VAR_RANGES
		logval("want[thread:self]", ((int)now.want[ Index(((P0 *)this)->self, 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 14: /* STATE 12 - ./attempt4.pml:43 - [critical = (critical+1)] (0:0:1 - 3) */
		IfNotBlocked
		reached[0][12] = 1;
		(trpt+1)->bup.oval = now.critical;
		now.critical = (now.critical+1);
#ifdef VAR_RANGES
		logval("critical", now.critical);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 15: /* STATE 13 - ./attempt4.pml:44 - [assert((critical==1))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][13] = 1;
		spin_assert((now.critical==1), "(critical==1)", II, tt, t);
		_m = 3; goto P999; /* 0 */
	case 16: /* STATE 14 - ./attempt4.pml:45 - [temp = n] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][14] = 1;
		(trpt+1)->bup.oval = ((P0 *)this)->temp;
		((P0 *)this)->temp = now.n;
#ifdef VAR_RANGES
		logval("thread:temp", ((P0 *)this)->temp);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 17: /* STATE 15 - ./attempt4.pml:46 - [n = (temp+1)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][15] = 1;
		(trpt+1)->bup.oval = now.n;
		now.n = (((P0 *)this)->temp+1);
#ifdef VAR_RANGES
		logval("n", now.n);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 18: /* STATE 16 - ./attempt4.pml:49 - [critical = (critical-1)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][16] = 1;
		(trpt+1)->bup.oval = now.critical;
		now.critical = (now.critical-1);
#ifdef VAR_RANGES
		logval("critical", now.critical);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 19: /* STATE 17 - ./attempt4.pml:50 - [want[self] = 0] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][17] = 1;
		(trpt+1)->bup.oval = ((int)now.want[ Index(((P0 *)this)->self, 2) ]);
		now.want[ Index(((P0 *)this)->self, 2) ] = 0;
#ifdef VAR_RANGES
		logval("want[thread:self]", ((int)now.want[ Index(((P0 *)this)->self, 2) ]));
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 20: /* STATE 18 - ./attempt4.pml:23 - [i = (i+1)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][18] = 1;
		(trpt+1)->bup.oval = ((P0 *)this)->i;
		((P0 *)this)->i = (((P0 *)this)->i+1);
#ifdef VAR_RANGES
		logval("thread:i", ((P0 *)this)->i);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 21: /* STATE 24 - ./attempt4.pml:53 - [done = (done+1)] (0:0:1 - 3) */
		IfNotBlocked
		reached[0][24] = 1;
		(trpt+1)->bup.oval = now.done;
		now.done = (now.done+1);
#ifdef VAR_RANGES
		logval("done", now.done);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 22: /* STATE 28 - ./attempt4.pml:60 - [-end-] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][28] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

