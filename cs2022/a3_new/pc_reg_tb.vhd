library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc_reg_tb is
end pc_reg_tb;

architecture behavior of pc_reg_tb is 
	component pc_reg is port (
		load, increment : in std_logic;
		offset : in std_logic_vector(15 downto 0);
		clock, reset : in std_logic;
		output : inout std_logic_vector(15 downto 0) );
	end component;

	signal load : std_logic := '0';
	signal increment : std_logic := '0';
	signal offset : std_logic_vector(15 downto 0) := (others => '0');
	signal clk : std_logic := '0';
	signal reset : std_logic := '0';
	signal output : std_logic_vector(15 downto 0);	

	constant clk_period : time := 20 ns;	

begin
	uut : pc_reg port map(load, increment, offset, clk, reset, output);
	
	clk_process : process begin
		clk <= '0';
		wait for clk_period/2;

		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period;
		
		--async reset
		reset <= '1';
		wait for clk_period;
		reset <= '0';
		assert output = x"0000";
		
		--add offset
		offset <= x"4343"; --0100 0011 0100 0011
		load <= '1';
		wait for clk_period;
		assert output = x"4343";
		
		--preserve the value
		load <= '0';
		wait for clk_period;
		assert output = x"4343";
		
		--increment a bunch of times
		increment <= '1';
		for i in 1 to 50 loop
			wait for clk_period;
			assert output = std_logic_vector(to_unsigned(16#4343# + i, 16));
		end loop;
		
		--reset again
		reset <= '1';
		load <= '0';
		increment <= '0';
		wait for clk_period;
		reset <= '0';
		
		--load and increment at once
		load <= '1';
		increment <= '1';
		offset <= x"f151";
		wait for clk_period;
		assert output = x"f152";
		
		increment <= '0'; --so it doesn't keep incrementing for the rest of the simulation
		wait;
	end process;
end;
