library(plyr)
library(boot)

data <- read.csv(file = "lab6data.csv", header = T)
names(data) [names(data)=="probelm"] <- "problem"

sysA <- subset(split(data, data$group)$SysA, select = c(times, problem))
sysB <- subset(split(data, data$group)$SysB, select = c(times, problem))

meanboot <- function(x, indices) {
  mean(x$times[indices]);
}

diffwmeans <- function(data, indices) {
  sa <- subset(data[indices,], group=="SysA")$times
  sb <- subset(data[indices,], group=="SysB")$times
  sameans <- mean(sa)
  sbmeans <- mean(sb)
  return(sameans - sbmeans)
}

#bootres <- boot(data = data, statistic = diffwmeans, R = 2000)
bootresA <- boot(data = sysA, statistic = meanboot, R = 2000)
bootresB <- boot(data = sysB, statistic = meanboot, R = 2000)
boxplot(bootresA$t, bootresB$t)

boot.ci(bootresA, type = "perc")
boot.ci(bootresB, type = "perc")
