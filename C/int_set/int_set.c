#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "int_set.h"

//helper
static int int_set_max_set_alloc(int size){
	int over = size; 
	if (over < 16) over = 16;

	return size + over;
}

struct int_set* int_set_new(){
	struct int_set* n = malloc(sizeof(struct int_set));

	n->current = 0;
	n->max = int_set_max_set_alloc(n->current);
	n->array = malloc(n->max * sizeof(int));

	return n;
}

void int_set_destroy(struct int_set* this){
	assert(this);

	if (this->array) free(this->array);
	free(this);
}


//binary search
//if the elements sought for is not found, it returns the place it should be inserted it
static int int_set_inner_search(struct int_set* this, int item, int* found){
	int place = 0;
	int present = 0;

	if (this->current > 0){
		place = -1;

		int* const array = this->array;
	
		int left = 0;
		int right = this->current;
	
		if (array[left] == item) place = left;
		if (array[right - 1] == item) place = right - 1;
	
		if (place == -1){
			while (left < right){
				int mid = left + ((right - left) >> 1);
	
				if (array[mid] < item) left = mid + 1;
				else if (array[mid] > item) right = mid;
				else{
					place = mid;
					present = 1;
					break;
				}
			}
	
			if (!present) place = left; //where it would go if it were present
		}
		else present = 1;
	}
	
	if (found) *found = present;
	return place;
	
}


int int_set_lookup(struct int_set* this, int item){
	int found;
	int_set_inner_search(this, item, &found);
	return found;
}


int int_set_add(struct int_set* this, int item){
	//search for the element first
	int exists;
	int place = int_set_inner_search(this, item, &exists);

	if (!exists){
		this->current++;

		if (this->current <= this->max){
			//insert the new item in it's right place
			int last = this->array[place];
			this->array[place] = item;

			//shift every higher item to the right
			for (int i = place + 1; i < this->current; i++){
				int temp = this->array[i];
				this->array[i] = last;
				last = temp;
			}
		}

		else{	
			//make a new array to fit everything
			this->max = int_set_max_set_alloc(this->current - 1);
			int* newPool = malloc(this->max * sizeof(int));

			memcpy(newPool + 0, this->array, place * sizeof(int)); //up to place
			newPool[place] = item; //the item to add
			memcpy(newPool + place + 1, this->array + place, (this->current - place - 1) * sizeof(int)); //from there on

			free(this->array);
			this->array = newPool;
		}

		return 1;
	}

	else return 0;
}

int int_set_remove(struct int_set* this, int item){
	int exists;
	int place = int_set_inner_search(this, item, &exists);

	if (exists){
		//shift every element from the item element on, to the left, removing the item itself
		for (int i = place; i < this->current - 1; i++){
			this->array[i] = this->array[i + 1];
		}
		this->current--;

		//reduce the array if it is too large
		int maxSize = int_set_max_set_alloc(this->current);
		if (maxSize < this->max){
			this->max = int_set_max_set_alloc(this->current);
			this->array = realloc(this->array, this->max * sizeof(int));
		}

		return 1;
	}

	else return 0;
}


//another helper, for union and intersect
//this copies a
static void int_set_internal_replace(struct int_set* dest, int* array, int elements, int size){
	dest->current = elements;
	int maxSize = int_set_max_set_alloc(elements);

	//not copying is better than copying
	if (size <= maxSize){
		//the newly allocted array satisfies the set size constraint
		dest->max = size;
		free(dest->array);
		dest->array = array;
	}

	else{
		if (elements > dest->max || dest->max > maxSize){
			//if dest->array is too small or too large
			free(dest->array);
			dest->max = maxSize;
			dest->array = malloc(dest->max * sizeof(int));
		}
	
		memcpy(dest->array, array, dest->current * sizeof(int));
		free(array);
	}
}

void int_set_union(struct int_set* dest, struct int_set* src, struct int_set* src2){
	//there are at most |src| + |src2| items in the union
	int maxSetSize = src->current + src2->current;
	int* array = malloc(maxSetSize * sizeof(int));

	int* a = src->array;
	int* b = src2->array;
	int* d = array;

	int* endA = src->array + src->current;
	int* endB = src2->array + src2->current;

	//merge and check for equality
	while (a < endA && b < endB){
		if (*a < *b){
			*d++ = *a++;
		}
		else if (*a == *b){
			*d++ = *a++;
			b++;
		}
		else{
			*d++ = *b++;
		}
	}	
	
	while (a < endA) *d++ = *a++;
	while (b < endB) *d++ = *b++;

	int elements = (int)(d - array);
	int_set_internal_replace(dest, array, elements, maxSetSize);
}

void int_set_intersect(struct int_set* dest, struct int_set* src, struct int_set* src2){
	//there are at most |src| + |src2| items in the union
	int maxSetSize = src->current + src2->current;
	int* array = malloc(maxSetSize * sizeof(int));

	int* a = src->array;
	int* b = src2->array;
	int* d = array;

	int* endA = src->array + src->current;
	int* endB = src2->array + src2->current;

	//merge and check for equality
	while (a < endA && b < endB){
		if (*a < *b) a++;
		else if (*a == *b){
			*d++ = *a++;
			b++;
		}
		else b++;
	}	

	int elements = (int)(d - array);
	int_set_internal_replace(dest, array, elements, maxSetSize);
}

