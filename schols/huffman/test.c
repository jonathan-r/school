#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>

#include "huffman.h"

int main(int argc, char** argv){
	if (argc == 2){
		char* filename = argv[1];
		FILE* f = fopen(filename, "rb"); //binary so that fseek gives the correct size
		
		if (f){
			fseek(f, 0, SEEK_END);
			long size = ftell(f);
			rewind(f);
			
			uint8_t* original = malloc(size + 1);
			fread(original, size, 1, f);
			fclose(f);
			original[size] = 0; //NULL terminator
			printf("original: %s\n", (char*) original);
			
			hcode codes[256];
			analyze_text(original, codes);
			
			int encoded_bytes;
			uint8_t* encoded = huffman_encode(original, codes, &encoded_bytes);
			
			uint8_t* decoded = huffman_decode(encoded, encoded_bytes, size + 1, codes);
			printf("decoded: %s\n", (char*) decoded);
		}
		
		else{
			fprintf(stderr, "could not open file\n");
			return 1;
		}
	}
	
	else printf("usage: %s <file to read>\n", argv[0]);
	
	return 0;
}


