#include <stdio.h>
#include <stdlib.h>

//this wasn't part of any schols paper (that I know of), but it was an example of a tricky question from the normal exams, and it's an interesting problem nonetheless

typedef struct node_struct{
	struct node_struct* next;
	void* data;	
} node;

typedef struct list_struct{
	node* head;
} list;


//O(n^2) time, O(n) aux storage; pretty much worthless
int check_simple(list* l){
	//keep a list of already visited nodes
	node* visited_start = NULL;
	node* visited_end = NULL;
	
	node* head = l->head; //current node
	int loop = 0; //whether a loop was found
	while (head->next){
		//see if it's already in the visited list
		for (node* v = visited_start; v; v = v->next){
			if (v->data == head->next){
				loop = 1;
				break;
			}
		}
		
		if (loop) break;
		
		else{
			//add this node to the loop
			node* v = malloc(sizeof(node));
			if (visited_end) visited_end->next = v;
			v->data = head->next;
			v->next = NULL;
			visited_end = v;
			
			if (!visited_start) visited_start = visited_end;
			
			head = head->next;
		}
	}
	
	//free the visited list
	for (node* v = visited_start; v;){
		node* next = v->next;
		free(v);
		v = next;
	}
	
	return loop;
}

//O(n) time, O(n) aux storage, but assumes that data in the list can never be a pointer to a stack value; still not very good
int check_replacing(list* l){
	//keep a list of the original contents of visited nodes
	node* stored_start = NULL;
	node* stored_end = NULL;
	
	int visited_value = 0; //the address of this is assumed to not be in the original list
	
	node* head = l->head; //current node
	int loop = 0; //whether there is a loop
	while(head){
		if (head->data == &visited_value){
			//this node was already visited
			loop = 1;
			break;
		}
		
		if (loop) break;
		
		else{
			//add the contents of this to the storage list
			node* s = malloc(sizeof(node));
			if (stored_end) stored_end->next = s;
			
			s->data = head->data;
			s->next = NULL;
			stored_end = s;
			
			if (!stored_start) stored_start = s;
			
			//mark the node as visited
			head->data = &visited_value;
			head = head->next;
		}
	}
	
	//restore the replaced data values, and delete the storage list
	for (node* s = stored_start, * h = l->head; s;){
		node* next = s->next;
		
		h->data = s->data;
		free(s);
		s = next;
		h = h->next;
	}
	
	return loop;
}

//Floyd’s Cycle detection algorithm, O(n + something, but still linear) time, no auxilliary storage, amazing; use this
int check_best(list* l){
	//in a list without a loop, this is the only time these two pointers are equal
	node* slow = l->head;
	node* fast = l->head;
	
	while (slow && fast && fast->next){
		slow = slow->next; //go to the next
		fast = fast->next->next; //go the the one after the next
		
		if (slow == fast){
			//there is a loop; the fast pointer caught up to the slow one
			return 1;
		}
	}
	
	return 0;
}

//better yet, use the first method, but store the visited nodes in a hashmap for quicker access. It requires even more storage, but this is a time/storage tradeoff

list* new_list(){
	list* l = malloc(sizeof(list));
	l->head = NULL;
	return l;
}

void free_list(list* l){
	for (node* n = l->head; n;){
		node* next = n->next;
		free(n);
		n = next;
	}
	free(l);
}
	
void front_insert(list* l, void* data){
	node* n = malloc(sizeof(node));
	n->data = data;
	n->next = l->head;
	l->head = n;
}

void print_list_to(list* l, node* to){
	printf("{ ");
	node* n = l->head;
	node* prev = NULL;
	do{
		printf("%d ", *(int*)(n->data));
		prev = n;
		n = n->next;
	} while(prev != to);
	printf("}\n");
}

int main(){
	//this is used to make sure the loop checking function don't alter the data
	int* data = malloc(500 * sizeof(int));
	for (int i = 0; i < 500; i++) data[i] = i + 1;
	
	list* straight = new_list();
	for (int i = 0; i < 100; i++) front_insert(straight, &data[rand() % 500]);
	
	//worst case - the last element points back
	list* looping = new_list();
	front_insert(looping, &data[0]);
	node* looping_end = looping->head;
	for (int i = 1; i < 100; i++) front_insert(looping, &data[rand() % 500]);
	looping_end->next = looping->head; //form the loop
	
	print_list_to(looping, looping_end);

	if (check_simple(straight)){
		printf("loop in straight list\n");
	}
	
	if (check_replacing(looping)){
		printf("loop in looping list\n");
	}
	
	print_list_to(looping, looping_end);

	free_list(straight);
	//free_list(looping); //doing that would cause an endless loop
	free(data);

	return 0;
}


