#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "int_set.h"

struct int_set* parse_expression(char** tokens, int num_tokens, char** error_text){
	struct int_set* partial = NULL; //result so far
	struct int_set* read_to = NULL; //next set
	char op = 0; //last operator

	int error = 0;
	char* error_str = NULL;

	int read_set = 0; //state
	int has_op = 1; //the implied initial operator is an identity operator

	int i;
	for (i = 0; i < num_tokens && !error; i++){
		char* token = tokens[i];
		int len = strlen(token);
		int other = 0;

		if (len == 1){
			char c = *token;
			switch(c){
				case '[':
					if (has_op && !read_set){
						read_set = 1;
						has_op = 0;
						read_to = int_set_new();
					}
					else{
						error_str = "Expected operator, not set";
						error = 1;
					}
					break;

				case ']':
					if (read_set){
						//now do the operation
						if (op == 0){
							//this occurs for the first set
							partial = read_to;
							read_to = NULL;
						}

						else{
							//for subsequent ones
							struct int_set* result = int_set_new();
							if (op == 'u') int_set_union(result, partial, read_to);
							else if (op == 'n') int_set_intersect(result, partial, read_to);
							else if (op == '-') {
								int j;
								for (j = 0; j < read_to->current; j++) int_set_remove(partial, read_to->array[j]);
							}

							int_set_destroy(read_to);
							read_to = NULL;

							if (op != '-'){
								int_set_destroy(partial);
								partial = result;
							}
						}

						has_op = 0;
						read_set = 0;
					}
					break;

				case 'u':
				case 'n':
				case '-':
					if (!read_set){
						if (!has_op){
							op = c;
							has_op = 1;
						}

						else {
							error_str = "Expected set, not operator";
							error = 1;
						}
					}
					else {
						error_str = "Operator inside set";
						error = 1;
					}
					break;

				default:
					other = 1;
					break;
			}
		}
		else other = 1;

		if (other){
			if (read_set){
				char* end;
				int elem = strtol(token, &end, 10);
				if (end != token + len){
					error_str = "Invalid expected number in set";
					error = 1;
				}

				else{
					int_set_add(read_to, elem);
				}
			}
		}
	}

	if (has_op && !error){ //if there is already an error, don't suppress it
		error_str = "Expected set after operator";
		error = 1;
	}

	if (error){
		//cleanup
		if (partial) int_set_destroy(partial);
		if (read_to) int_set_destroy(read_to);

		if (error_text) *error_text = error_str;
		return NULL;
	}

	return partial;
}

int main(int argc, char** argv){
	if (argc == 1){
		printf("A set is defined as \"[ 0 2 54 ... ]\"\n");
		printf("The operators '-', 'u' and 'n' mean difference union and intersect, respectively.\n");
		printf("Use brackets '(' and ')', in order to use more than one operator. The parser is dumb\n");
		printf("\nusage %s: <set expression>\n\n", argv[0]);
	}

	else{
		char* error = NULL;
		struct int_set* result = parse_expression(&argv[1], argc - 1, &error);
		if (result){
			printf("[ ");
			int i;
			for (i = 0; i < result->current; i++){
				printf("%d ", result->array[i]);
			}
			printf("]\n");
		}

		else if (error){
			printf("Error: %s\n", error);
		}

		else{
			printf("Nothingness\n");
		}
	}

	return 0;
}
