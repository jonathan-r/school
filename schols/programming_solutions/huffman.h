
//entry for a character, used for encoding
typedef struct hcode_struct{
	//number of bits at the (logical) end of data that make up the encoding.
	//If 0, this character is not used at all, so it should not be included in the tree (cause it would screw up the tree)
	int len; 

	unsigned data;
} hcode;

//binary symbol tree, used for decoding
typedef struct symbol_struct{
	uint8_t c; //character, relevant only when this node is a leaf
	struct symbol_struct* lower[2]; //child nodes, if they are NULL this is a leaf
} symbol;

