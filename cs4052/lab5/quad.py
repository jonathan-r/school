from math import floor, ceil
import matplotlib as mpl
mpl.use('Agg') #doesn't need X
from matplotlib import pyplot as plt
from matplotlib import patches as pch
#import matplotlib.image as mpimg
import sys

exs = [0, 0, 100, 100]
eys = [0, 100, 0, 100]
#plt.scatter(exs, eys)
lines = open(sys.argv[1], "r").readlines()

fg = plt.figure()
ax = fg.add_subplot(111, aspect='equal')

for line in lines:
	st, et, dt = line.split('|')
	sx, sy = map(lambda x : float(x) / 100, st.split(' '))
	ex, ey = map(lambda x : float(x) / 100, et.split(' '))
	w = abs(ex - sx)
	h = abs(ey - sy)
	d = int(dt)
	#print(sx, sy, w, h, d)
	ax.add_patch(pch.Rectangle((sx, sy), w, h, alpha=1.0/(d+1)))

fg.savefig(sys.argv[1] + '.png')
