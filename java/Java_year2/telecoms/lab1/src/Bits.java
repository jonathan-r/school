
public class Bits {
	static class BitStringException extends Exception {
		public BitStringException() {
			super("Incorrect bit string");
		}
	}

	// Takes a long and returns a binary representation of it as a String,
	// with as many leading zeros as needed to make it of length length
	// length = 0 ensures there will be no leading zeros
	static String toBitString(byte x, int length) {
		//arithmetic right shift refuses to work
		boolean msb = (x & 0x80) != 0;
		x &= 0x7F; //unset the most significant bit
		
		String s = "";
		if (x != 0) {
			while (x != 0) {
				s = Integer.toString((int) x & 1) + s;
				x = (byte) (x >>> 1);
			}
		} 
		
		else s = "0";

		if (s.length() < length) {
			String leading = "";
			for (int i = 0; i < length - s.length(); i++) leading += '0';
			s = leading + s;
		}
		
		if (msb) s = "1" + s.substring(1);

		return s;
	}

	//takes a bit string composed exclusively of ascii '0' and '1' and returns
	//a byte array containing the data as binary
	//throws BitStringException if the string contains other characters
	static byte[] toBytes(String bitString) throws BitStringException {
		int size = bitString.length() / 8;
		boolean extraByte = bitString.length() % 8 > 0;

		byte[] data = new byte[size + ((extraByte) ? 1 : 0)];
		for (int i = 0; i < size; i++) {
			for (int b = 0; b < 8; b++) {
				int c = bitString.charAt(i * 8 + b) - 0x30;
				if (c != 0 && c != 1) throw new BitStringException();
				data[i] |= c << (7 - b);
			}
		}

		if (extraByte) {
			for (int b = 0; b < bitString.length() - size * 8; b++) {
				int c = bitString.charAt(size * 8 + b) - 0x30;
				if (c != 0 && c != 1) throw new BitStringException();
				data[size] |= c << (7 - b);
			}
		}

		return data;
	}
}
