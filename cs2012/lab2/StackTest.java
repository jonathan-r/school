import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

//-------------------------------------------------------------------------
/**
 *  Test class for Stack
 *
 *  @author  Jonathan Roșca
 *  @version 2014.01.19
 */
@RunWith(JUnit4.class)
public class StackTest
{
    //~ Constructor ........................................................
    @Test
    public void testConstructor()
    {
      new Stack();
    }

    //~ Public Methods ........................................................

    /**
    *
    */
    @Test
    public void testEmpty(){
        Stack<Integer> testStack = new Stack<Integer>();
        assertEquals( "Checking empty stack", "", testStack.toString() );
        assertEquals( "Checking empty stack", testStack.toString(), testStack.toStringAlt() );
        assertEquals( "Checking isEmpty", true, testStack.isEmpty() );
    }

    // ----------------------------------------------------------
    /**
     * Check if insert works at the beginning
     */
    @Test
    public void testPush()
    {
        Stack<Integer> testStack = new Stack<Integer>();

        testStack.push(1);
        assertEquals( "Checking pushing", "1", testStack.toString() );
        assertEquals( "Checking pushing", testStack.toString(), testStack.toStringAlt() );

        testStack.insertFirst(2);
        assertEquals( "Checking pushing", "2,1", testStack.toString() );
        assertEquals( "Checking pushing", testStack.toString(), testStack.toStringAlt() );

        testStack.insertFirst(3);        
        assertEquals( "Checking pushing", "3,2,1", testStack.toString() );
        assertEquals( "Checking pushing", testStack.toString(), testStack.toStringAlt() );
    }

    // ----------------------------------------------------------
    /**
     * Check if the insert works at the end
     */
    @Test
    public void testPop()
    {
        Stack<Integer> testStack = new Stack<Integer>();
        testStack.push(1);
        testStack.push(2);
        testStack.push(3);

        assertEquals( "Checking popping", 3, (int)testStack.pop() );
        assertEquals( "Checking state after 1st pop", "2,1", testStack.toString() );
        assertEquals( "Checking state after 1st pop", testStack.toString(), testStack.toStringAlt() );

        assertEquals( "Checking popping", 2, (int)testStack.pop() );
        assertEquals( "Checking state after 1st pop", "1", testStack.toString() );
        assertEquals( "Checking state after 1st pop", testStack.toString(), testStack.toStringAlt() );

        assertEquals( "Checking popping", 1, (int)testStack.pop() );
        assertEquals( "Checking insertion at end", "", testStack.toString() );
        assertEquals( "Checking insertion at end", testStack.toString(), testStack.toStringAlt() );

        assertEquals( "Checking popping an empty list", null, testStack.pop() );
    }

    // ----------------------------------------------------------
    /**
     * Check if the insert works in the middle
     */
    @Test
    public void testIsEmpty()
    {
        // test non-empty list
        Stack<Integer> testStack = new Stack<Integer>();
        assertEquals( "Checking empty list", true, testStack.isEmpty() );

        testStack.push(1);
        assertEquals( "Checking empty list", false, testStack.isEmpty() );

        testStack.pop();
        assertEquals( "Checking empty list", true, testStack.isEmpty() );
     }
}

