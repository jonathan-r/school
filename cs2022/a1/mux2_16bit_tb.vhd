library ieee;
use ieee.std_logic_1164.all;
 
entity mux2_16bit_tb is
end mux2_16bit_tb;
 
architecture behavior of mux2_16bit_tb is 
	component mux2_16bit
	port(s   : in  std_logic;
	     in0 : in  std_logic_vector(15 downto 0);
	     in1 : in  std_logic_vector(15 downto 0);
	     z   : out std_logic_vector(15 downto 0) );
	end component;

	signal s_in : std_logic := '0';
	signal in0_in : std_logic_vector(15 downto 0) := (others => '0');
	signal in1_in : std_logic_vector(15 downto 0) := (others => '0');
	signal z_out : std_logic_vector(15 downto 0);

begin
	uut: mux2_16bit port map (s_in, in0_in, in1_in, z_out);
	
	stim_proc: process begin		
		wait for 1 ns;
		--set inputs to different values
		in0_in <= x"ABAB";
		in1_in <= x"CFCF";
		
		s_in <= '0';
		wait for 1 ns;
		assert (z_out = x"ABAB");

		s_in <= '1';
		wait for 1 ns;
		assert (z_out = x"CFCF");	
	end process;
end;
