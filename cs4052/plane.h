#pragma once

#include "asset.h"

#include "vertex.h"
#include "object.h"
#include "maths/mat4.h"
#include "maths/quat.h"

#include <algorithm>

class Plane {
public:
	Plane(const vec3& spawnPos) {
		init();

		bodyOb.stem = spawnPos;

		bodyOb.model = bodyMesh;
		bodyOb.texture = bodyTexture;

		rudderOb.model = rudderMesh;
		rudderOb.texture = bodyTexture;
		rudderOb.stem.set(0, -0.29f, -7.69f); //offset
		bodyOb.shoots.push_back(&rudderOb);

		elevatorOb.model = elevatorMesh;
		elevatorOb.texture = bodyTexture;
		elevatorOb.stem.set(0, 0.15f, -7.39f); //offset
		bodyOb.shoots.push_back(&elevatorOb);

		wheelLOb.model = wheelMesh;
		wheelLOb.texture = bodyTexture;
		wheelLOb.stem.set(0.92f, -1.89f, 0); //offset
		bodyOb.shoots.push_back(&wheelLOb);

		wheelROb.model = wheelMesh;
		wheelROb.texture = bodyTexture;
		wheelROb.stem.set(-0.92f, -1.89f, 0); //offset
		bodyOb.shoots.push_back(&wheelROb);

		propOb.model = propMesh;
		propOb.texture = bodyTexture;
		propOb.stem.set(0, 0, 1); //offset
		bodyOb.shoots.push_back(&propOb);

		throttle = 0.0f;
		propSpin = 0.0f;
	}

	~Plane() {

	}

	Object& getObj() {
		return *(dynamic_cast<Object*>(&bodyOb));
	}

	//http://www.movesinstitute.org/~zyda/pubs/Presence.1.4.pdf
	//http://gamepipe.usc.edu/~zyda/resources/pubs/Cooke-Zyda-Presence-Vol1-No4.pdf

	void cameraPoints(vec3& pos, vec3& target, vec3& up) {
		float dist = 2.0f + (vel.length() * 0.3f);
		pos = bodyOb.global.multVec3(vec3(0, 5, -10) * dist);
		target = bodyOb.global.multVec3(vec3(0, 3, 10) * dist);
		up = bodyOb.global.multVec3(vec3(0, 1, 0), 0);
	}

	void input(float DE, float DR, float DA, float DT) {
		const float limit = 0.5f;
		controlSurf.x = std::max(-limit, std::min(limit, controlSurf.x + DE));
		controlSurf.y = std::max(-limit, std::min(limit, controlSurf.y + DR));
		controlSurf.z = std::max(-limit, std::min(limit, controlSurf.z + DA));
		throttle = std::max(0.0f, std::min(1.0f, throttle + DT));
	}

	void update(float dt) {
		propSpin += exp(5 * throttle - 3) * 5 * dt;
		propSpin -= (5 + propSpin * propSpin) * 0.05f * dt;
		propSpin = std::max(0.0f, std::min(200.0f, propSpin));
		//TODO: if propSpin*dt > 15/180 PI then blur or draw multiple copies with additive blending

		thrust = propSpin * propSpin;

		//body frame of reference
		float vel = velAxial.length();
		vec3 forces(0.0f);

		//body frame of reference
		
		//move control surfaces back to neutral
		for (int i = 0; i < 3; i++) {
			const float eps = 0.01f;
			float& val = controlSurf.data[i];
			if (fabs(val) < eps) val = 0.0f;
			else val -= val * 0.1f * dt;
		}

		float alpha = atan(W / U);
		float beta = atan(V / U);
		float alphaDeriv = ((du / U) - (W * dU / (U * U))) / ((W*W) / (U*U) + 1.0f);

		float velMagsq = U*U + V*V + W*W;
		float velMag = sqrt(velMagsq);

		float rho = 0.15f; //TODO: air pressure
		float Q = velMagsq * rho * 0.5f; //dynamic pressure

		float delWindVel = windVel - lastWindVel;
		float q = (velMag + delWindVel.length()) / velMag;

		float Ldash = (CL0 + CLa * alpha + (CLQ * Q + CLad * alphaDeriv) * (chord / (2*velMag)) + CLde * controlSurf.x * q*q) * Q * S;
		float D = (CD0 + CDa * alpha + CDde * controlSurf.x * q*q) * Q * S;
		float SF = (CYb * beta + CYdr * controlSurf.y) * Q * S;

		//aerodynamic forces
		float Faz = -Ldash * cos(alpha) - D * sin(alpha);
		float Fax = Ldash * alpha - D * cos(alpha) - SF * sin(beta);
		float Fay = SF * cos(beta);

		//aerodynamic moments
		float La = (CLb * beta + (CLP * P + CLR * R) * (b / (2 * velMag)) + CLda * controlSurf.z + CLdr * controlSurf.y) * Q * S * b;
		float Ma = (CM0 + CMa * alpha + (CMQ * Q + CMad * alphaDeriv) * (c / (2 * velMag)) + CMde * controlSurf.x * q*q) * Q * S * c;
		float Na = (CNb * beta + (CNP * P + CNR * R) * (b / (2 * velMag)) + CNda * controlSurf.z + CNdr * controlSurf.y) * Q * S * b;

		float Fx = Fax + thrust;
		float Fy = Fay;
		float Fz = Faz;

		/*L = La;
		M = Ma; // + M_thrust + M_gyro
		N = Na; // + N_thrust + N_gyro*/

		//total force
		const float g = 9.8f; //gravitational constant?
		dU = V*R - W*Q - g * sin(theta) + Fx / w;
		dV = W*P - U*R - g * sin(phi) * cos(theta) + Fy / w;
		dW = U*Q - V*P + g * cos(phi) * cos(theta) + Fz / w;

		//total moment


		//update presentation
		elevatorOb.eu.x = controlSurf.x; //elevatorOb.qt = quat::axisRotation(vec3(1, 0, 0), controlSurf.x);
		rudderOb.eu.y = controlSurf.y; //rudderOb.qt = quat::axisRotation(vec3(0, 1, 0), controlSurf.y);
		//aileronOb.eu.z = controlSurf.z; //aileronOb.qt = quat::axisRotation(vec3(1, 0, 0), controlSurf.z);
		propOb.eu.z = fmod(propOb.eu.z + propSpin * dt, M_PI); //propOb.qt *= quat::axisRotation(vec3(0, 0, 1), propSpin * dt); //propOb.qt.normalize();

		bodyOb.bakeAll();

		//return control surfaces to neutral
		for (int i = 0; i < 3; i++) {
			const float eps = 0.01f;
			float& val = controlSurf.data[i];
			if (fabs(val) < eps) val = 0.0f;
			else if (val < 0.0f) val += 1.0f * dt;
			else val -= 1.0f * dt;
		}
	}

	static void init() {
		if (!bodyMesh) {
			bodyMesh = loadMesh("plane_body.obj");
			rudderMesh = loadMesh("rudder.obj");
			elevatorMesh = loadMesh("elevator.obj");
			wheelMesh = loadMesh("wheel.obj");
			propMesh = loadMesh("prop.obj");
			bodyTexture = loadTexture("HSVS255.jpg"); //placeholder
		}
	}

	static void finalize() {
		delete bodyMesh;
		delete rudderMesh;
		delete elevatorMesh;
		delete wheelMesh;
		delete propMesh;
		delete bodyTexture;
	}

private:
	struct PlaneConstants {
		//constants, for each aircraft
		float CL0; //reference lift at zero angle of attack
		float CD0; //reference drag at zero angle of attack
		float CLa; //lift curve slope
		float CDa; //drag curve slope
		float CM0; //pitch moment (reference?)
		float CMa; //pitch moment due to angle of attach
		float CLQ; //lift due to pitch rate
		float CMQ; //pitch moment due to pitch rate
		float CLad; //lift due to angle of attach deriv
		float CMad; //pitch moment due to angle of attach deriv

		float CYb; //side force due to sideslip
		float CLb; //dihedral effect
		float CLP; //roll damping
		float CLR; //roll due to yaw rate
		float CNb; //weather cocking stability
		float CNP; //rudder adverse yaw
		float CNR; //yaw damping

		float CLde; //lift due to elevator
		float CDde; //drag due to elevator
		float CMde; //pitch due to elevator
		float CLda; //roll due to aileron

		float Ix, Iy, Iz, Ixz; //inertia on axes

		float chord; //on the airfoil, the geomethic distance between the leading and trailing edge
		float S; //wing area
		float b; //wing span
		float c; //airfoil chord length
		float w; //weight

		PlaneConstants() {
			//example data given on the paper
			CD0 = 0.03f; CDa = 0.3f
			CL0 = 0.28; CLa = 3.45; CLQ = 0.0f; CLda = 0.72f; CLde = 0.36f;
			CM0 = 0.0f; CMQ = -3.6f; CMa = -0.38f; CMda = -1.1f; CMde = -0.5f;
			CYb = -0.98f; CYdr = 0.17f;
			CLb = -0.12f; CLP = -0.26f; CLR = 0.14f; CLda = 0.08f; CLdr = -0.105f;
			CNb = 0.25f; CNP = 0.022f; CNR = -0.35f; CNda = 0.06f; CNdr = 0.032f;

			b = 27.5f;
			S = 260.0f;
			c = 10.8f;
			w = 546.0f;
			Ix = 8090.0f;
			Iy = 25900.0f;
			Iz = 29200.0f;
			Ixz = 1300.0f;
		}
	};

	const PlaneConstants* pc;

	AnglesObject bodyOb;
	AnglesObject rudderOb;
	AnglesObject elevatorOb;
	AnglesObject wheelLOb;
	AnglesObject wheelROb;
	AnglesObject propOb;

	// inputs
	vec3 controlSurf; //elevator, rudder, aileron absolute rotation
	float throttle; //[0, 1]
	float propSpin; //lags behind throttle
	vec3 windVel;
	vec3 lastWindVel;

	//flight
	float U, V, W; //linear velocity
	float P, Q, R; //angular velocity
	float L, M, N; //moments
	float dU, dV, dW; //linear acceleration
	float thrust;

	//assets
	static Mesh* bodyMesh;
	static Mesh* propMesh;
	static Mesh* rudderMesh;
	static Mesh* elevatorMesh;
	static Mesh* wheelMesh;
	static Texture* bodyTexture;
};
