#version 120

uniform vec2 input_tex_size;
uniform sampler2D input_tex;

void main(){
	int cx = int(gl_FragCoord.x);
	int cy = int(gl_FragCoord.y);
	
	int s = 0;
	for (int i = -1; i < 2; i++){
		for (int j = -1; j < 2; j++){
			vec3 cc = texture2D(input_tex, vec2(cx + i, cy + j) / input_tex_size).xyz;
			float c = cc.x + cc.y + cc.z;
			s += (c > 1.5)? 1 : 0;
		}
	}
	
	float c = (s == 3 || s == 4)? 1.0 : 0.0;
	
	gl_FragColor = vec4(vec3(c), 1.0);
}
