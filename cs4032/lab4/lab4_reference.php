<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

if (sizeof($argv) != 4) {
	echo "usage: echo_stanalone.php hostname port studentid";
	exit;
}
else {
	date_default_timezone_set('Europe/Dublin');
	echo "STARTING SERVER: " . date("Y/m/d h:i:sa") . "";
}

$host = $argv[1];
$port = $argv[2];
$studentid = $argv[3];
set_time_limit(10000);
class Server {
	public function __construct($host, $port, $studentid, &$roomdata) {
		$this->host = $host;
		$this->port = $port;
		$this->studentid = $studentid;
		$this->clients = array();
		$this->doloop = true;
		$this->roomdata = $roomdata;
		$this->socket = socket_create(AF_INET, SOCK_STREAM, 0);
		if (!$this->socket) {
			$error = socket_strerror(socket_last_error());
			echo "ERROR: Could not create socket: $error ";
			exit;
		}
		else {
			echo "Server socket created.";
		}

		$result = socket_bind($this->socket, $this->host, $this->port);
		if ($result) {
			echo "Socket binding successful for host $this->host on port $this->port. ";
		}
		else {
			echo "ERROR: Could not bind to address $this->host:$this->port ";
			exit;
		}
	}

	public function run() {
		socket_listen($this->socket);
		echo "Server listening for Connections.";
		socket_set_nonblock($this->socket);
		while ($this->doloop) {
			$client = socket_accept($this->socket);
			if ($client !== false) {
				if (socket_getpeername($client, $clientaddress, $clientport)) {
					echo "Client $clientaddress : $clientport is now connected.";
				}

				array_push($this->clients, new Client($client, $this->host, $this->port, $this->studentid, $this->roomdata, $this));
			}

			$closed = [];
			foreach($this->clients as $client) {
				if ($client->processSocket() == false) $closed[] = $client;
			}
		}
	}

	function killService() {
		echo "Shutting down service.";
		if (!$this->doloop) return;
		$this->doloop = false;
		echo "Closing all client connections";
		foreach($this->clients as $client) {
			$client->stop();
		}

		sleep(2);
		echo "Server Ending";
		socket_shutdown($this->socket, STREAM_SHUT_WR);
		socket_close($this->socket);
		exit;
	}
}

$roomdata = new RoomData();
$server = new Server($host, $port, $studentid, $roomdata);
$server->run();
class RoomData {
	public function __construct() {
		$this->next_id = 1;
		$this->client_names = [];
		$this->chat_rooms = [];
		$this->next_room_ref = 1;
		$this->client_sockets = [];
	}

	public function run() {
	}

	public function getClientID($client_name) {
		if (isset($this->client_names[$client_name])) {
			return $this->client_names[$client_name];
		}
		else {
			$client_id = $this->next_id++;
			$this->client_names[$client_name] = $client_id;
			return $client_id;
		}
	}

	public function getClientName($client_id) {
		foreach($this->client_names as $key => $value) {
			if ($value == $client_id) return $key;
		}

		return null;
	}

	public function addClientToChatroom($chatroom, $client_id) {
		$found = false;
		$room_ref = null;
		foreach($this->chat_rooms as $rr => $value) {
			if ($value["name"] == $chatroom) {
				$found = true;
				$room_ref = $rr;
				array_push($this->chat_rooms[$rr]["values"], $client_id);
				break;
			}
		}

		if (!$found) {
			$room_ref = $this->next_room_ref++;
			$this->chat_rooms[$room_ref] = array(
				"name" => $chatroom,
				"values" => array(
					$client_id
				)
			);
		}

		return $room_ref;
	}

	public function print_chatrooms() {
		return;
		echo "RoomDATA:";
		print_r($this->chat_rooms);
		echo "";
	}

	public function removeClientFromChatroom($client_id, $room_ref) {
		foreach($this->chat_rooms as $key => $value) {
			if ($key == $room_ref) {
				if (($k = array_search($client_id, $value["values"])) !== false) {
					unset($this->chat_rooms[$key]["values"][$k]);
				}

				return;
			}
		}
	}

	public function getClientChatrooms($client_id) {
		echo "function getClientChatrooms($client_id)";
		$rooms = array();
		print_r($this->chat_rooms);
		foreach($this->chat_rooms as $key => $value) {
			if (($k = array_search($client_id, $value["values"])) !== false) {
				array_push($rooms, $key);
			}
		}

		print_r($rooms);
		return $rooms;
	}

	public function sendMessageToChatroom($room_ref, $client_name, $message) {
		echo "sending message to chatroom $room_ref from $client_name....";
		if (isset($this->chat_rooms[$room_ref])) {
			$client_ids = $this->chat_rooms[$room_ref]["values"];
			$message_mar = "CHAT:" . $room_ref . "CLIENT_NAME:" . $client_name . "MESSAGE:" . trim($message) . "";
			echo "--------Sending Message--------" . $message_mar . "---------End Message-----------";
			print_r($client_ids);
			foreach($client_ids as $client_id) {
				$this->sendMesageToClient($client_id, $message_mar);
			}
		}
		else {
			echo "ERROR: chat room $room_ref not found in database";
			print_r($this->chat_rooms);
		}
	}

	public function sendMesageToClient($client_id, $message) {
		$socket = $this->getClientSocket($client_id);
		$write = socket_write($socket, $message, strlen($message));
		if (!$write) {
			$error = socket_strerror(socket_last_error());
			echo "ERROR: Could not send message to client $client_id ($socket): $error.";
		}
		else {
			echo "sent message to client $client_id (socket: $socket).";
		}
	}

	public function storeClientSocket($client_id, $socket) {
		$this->client_sockets[$client_id] = $socket;
	}

	public function deleteClientSocket($client_id) {
		if (in_array($client_id, $this->client_sockets)) unset($this->client_sockets[$client_id]);
	}

	public function getClientSocket($client_id) {
		if (isset($this->client_sockets[$client_id])) return $this->client_sockets[$client_id];
		else return null;
	}
}

class Client {
	public function __construct($socket, $host, $port, $studentid, $roomdata, &$server) {
		$this->client = $socket;
		$this->host = $host;
		$this->port = $port;
		$this->studentid = $studentid;
		$this->roomdata = $roomdata;
		$this->server = $server;
	}

	public function stop() {
		$this->doloop = false;
	}

	public function processSocket() {
		if ($this->client) {
			$read = array(
				$this->client
			);
			$write = NULL;
			$except = NULL;
			$num_changed_sockets = socket_select($read, $write, $except, 0);
			if ($num_changed_sockets === false) return $this->closeSocket("There was a socket error when selecting socket for read: terminating processing of this client.");
			else
			if ($num_changed_sockets == 0) return true;
			if (($sle = socket_last_error($this->client))) {
				return $this->closeSocket("Socket closed by Peer: " . socket_strerror($sle));
			}

			$result = @socket_read($this->client, 1024, PHP_NORMAL_READ);
			if ($result === false) return $this->closeSocket("client disconnected :" . socket_strerror(socket_last_error($this->client)) . "");
			else
			if ($result === "") {
				$error = socket_strerror(socket_last_error($this->client));
				return $this->closeSocket("ERROR: Could not read server response: $error ");
			}
			else
			if (empty($result)) {
				$error = socket_strerror(socket_last_error($this->client));
				return $this->closeSocket("ERROR: Server response is empty: $error ");
			}
			else {
				echo "------- Recieved message -------$result--------- End Message ----------";
				if (substr($result, 0, 5) === "HELO ") {
					$message = $result . "IP:" . $this->host . "" . "Port:" . $this->port . "" . "StudentID:TESTSERVER" . $this->studentid;
					$write = socket_write($this->client, $message, strlen($message));
					if (!$write) {
						$error = socket_strerror(socket_last_error($this->client));
						return $this->closeSocket("ERROR: Could not send data to server: $error <br /> ");
					}
					else {
						echo "Sent message to client:" . $message . "";
						return true;
					}
				}
				else
				if (substr($result, 0, 12) === "KILL_SERVICE") {
					echo "Recieved KILL_SERVICE. Shutting down.";
					$this->server->killService();
					return false;
				}
				else {
					$res = $this->handleClient($result, $this->client, $this->host, $this->port);
					if ($res == false) return $this->closeSocket("");
					else return $res;
				}
			}
		}
	}

	private function closeSocket($str) {
		if (strlen($str) > 0) echo $str;
		socket_set_block($this->client);
		socket_close($this->client);
		$this->client = null;
		return false;
	}

	public function handleClient($result, &$socket, $hostip, $port) {
		return $this->handleJoinChatroom($result, $socket, $hostip, $port) || $this->handleLeaveChatroom($result, $socket) || $this->handleDisconnect($result, $socket) || $this->handleChat($result, $socket) || $this->defaultHandler($result, $socket);
	}

	public function defaultHandler($result, $socket) {
		echo "Warning: Unknown Message: $result\nUnable to process, so terminating this client processing.";
		return false;
	}

	public function handleChat($result, $socket) {
		$command_string = "CHAT:";
		if (substr($result, 0, strlen($command_string)) === $command_string) {
			echo "Handling CHAT message";
			$line = explode(":", $result);
			$room_ref = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			echo "room ref: $room_ref";
			$command_string = "JOIN_ID:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$join_id = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			$command_string = "CLIENT_NAME:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$client_name = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			echo "client_name: $client_name";
			$command_string = "MESSAGE:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				echo "RESULT: $result";
				$line = explode(":", $result);
				$message = trim(preg_replace('/\s\s+/', ' ', $line[1]));
				$inmessage = true;
				while ($inmessage) {
					$result = socket_read($socket, 1024, PHP_NORMAL_READ);
					if (trim($result) == '') {
						$inmessage = false;
						echo "-----------message recieved----------------" . $message . "---------------end message------------";
					}
					else if (empty($result)) {
						$error = socket_strerror(socket_last_error());
						echo "ERROR: Server response is empty: $error";
						return $true;
					}
					else $message.= $result . "";
				}
			}

			$this->roomdata->sendMessageToChatroom($room_ref, $client_name, $message);
			return true;
		}
		else return false;
	}

	public function handleDisconnect($result, &$socket) {
		$command_string = "DISCONNECT:";
		if (substr($result, 0, strlen($command_string)) === $command_string) {
			echo "Handling DISCONNECT message";
			$line = explode(":", $result);
			$ip = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			$command_string = "PORT:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$port = trim(preg_replace('/\s\s+/', ' ', $line[1]));
				echo "PORT is $port";
			}

			$command_string = "CLIENT_NAME:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$client_name = trim(preg_replace('/\s\s+/', ' ', $line[1]));
				echo "Client name: $client_name";
			}

			$client_id = $this->roomdata->getClientID($client_name);
			$message = "$client_name has left this chatroom.";
			$room_refs = $this->roomdata->getClientChatrooms($client_id);
			for ($i = 0; $i < sizeof($room_refs); $i++) {
				$this->roomdata->sendMessageToChatroom($room_refs[$i], $client_name, $message);
				echo "removing from room $room_refs[$i]";
				$this->roomdata->removeClientFromChatroom($client_id, $room_refs[$i]);
			}

			socket_close($socket);
			$this->roomdata->deleteClientSocket($client_id);
			$socket = null;
			echo "HANDLING DISCONNECT COMPLETED";
			return true;
		}
		else return false;
	}

	public function handleLeaveChatroom($result, $socket) {
		$command_string = "LEAVE_CHATROOM:";
		if (substr($result, 0, strlen($command_string)) === $command_string) {
			echo "Handling LEAVE_CHATROOM message...";
			$line = explode(":", $result);
			$room_ref = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			$command_string = "JOIN_ID:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$client_id = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			$command_string = "CLIENT_NAME:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$client_name = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			$message = "LEFT_CHATROOM: $room_ref JOIN_ID: $client_id";
			$write = socket_write($socket, $message, strlen($message));
			if (!$write) {
				$error = socket_strerror(socket_last_error());
				echo "ERROR: Could not send data to client: $error ";
			}
			else {
				echo "sent message to client:" . $message . "";
			}

			echo "Sending leaving message to chatroom members.";
			$message = "$client_name has left this chatroom.";
			$this->roomdata->sendMessageToChatroom($room_ref, $client_name, $message);
			$this->roomdata->removeClientFromChatroom($client_id, $room_ref);
			echo "LeaveChatroom Handler Complete.";
			return true;
		}
		else return false;
	}

	public function handleJoinChatroom($result, $socket, $hostip, $port) {
		$command_string = "JOIN_CHATROOM:";
		if (substr($result, 0, strlen($command_string)) === $command_string) {
			echo "Handling JOIN_CHATROOM message";
			$line = explode(":", $result);
			$chatroom = trim(preg_replace('/\s\s+/', '', $line[1]));
			$command_string = "CLIENT_IP:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$client_ip = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			$command_string = "PORT:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$ipport = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			$command_string = "CLIENT_NAME:";
			$result = socket_read($socket, 1024, PHP_NORMAL_READ);
			if (substr($result, 0, strlen($command_string)) === $command_string) {
				$line = explode(":", $result);
				$client_name = trim(preg_replace('/\s\s+/', ' ', $line[1]));
			}

			$client_id = $this->roomdata->getClientID($client_name);
			$room_ref = $this->roomdata->addClientToChatroom($chatroom, $client_id);
			$this->roomdata->storeClientSocket($client_id, $socket);
			$message = "JOINED_CHATROOM:" . $chatroom . "SERVER_IP:" . $hostip . "PORT:" . $port . "ROOM_REF:" . $room_ref . "JOIN_ID: " . $client_id . "";
			$write = socket_write($socket, $message, strlen($message));
			if (!$write) {
				$error = socket_strerror(socket_last_error());
				echo "ERROR: Could not send data to client: $error ";
			}
			else {
				echo "sent message to client:";
				echo "-----------Sent Message--------" . $message . "---------End Message-----------";
			}

			$message = "$client_name has joined this chatroom.";
			$this->roomdata->sendMessageToChatroom($room_ref, $client_name, $message);
			echo "Handler for JOIN_CHATROOM completed successfully.";
			return true;
		}
		else return false;
	}
}
?>
