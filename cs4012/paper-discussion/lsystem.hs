module LSystem where
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

type Symbol = Char
type Rule = (Symbol, String)
type RuleSet = Map.Map Symbol String
type LSystem = (String, String, String, RuleSet) --(variables, constants, axiom, rules)

substitute :: RuleSet -> Symbol -> String
substitute rules c =
    --Maybe.fromMaybe (error $ "No substitution for symbol " ++ [c]) (Map.lookup c rules)
    Maybe.fromMaybe [c] (Map.lookup c rules)

advanceString :: RuleSet -> String -> String
advanceString _ [] = ""
advanceString rules (x:xs) = substitute rules x ++ advanceString rules xs

--returns both starting sequence and all intermediates
evaluateAll :: LSystem -> Int -> [String]
evaluateAll (_, _, axiom, rules) n = take n (iterate (advanceString rules) axiom)

--returns only final production
evaluateLast :: LSystem -> Int -> String
evaluateLast (_, _, axiom, rules) n = iterate (advanceString rules) axiom !! n

insertRules :: [Rule] -> RuleSet -> RuleSet
insertRules [] from = from
insertRules ((sym, act):tail) from = insertRules tail (Map.insert sym act from)

makeRules :: [Rule] -> RuleSet
makeRules pairs = insertRules pairs Map.empty
