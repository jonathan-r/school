/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x6dd86d03 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//tholos.scss.tcd.ie/ugrad/roscaj/cs2022/a1_final/decoder_3to8_tb.vhd";
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_180853171_1035706684(char *, char *, int , int );
char *ieee_p_1242562249_sub_2540846514_1035706684(char *, char *, char *, char *, int );


static void work_a_1343320774_2372691052_p_0(char *t0)
{
    char t7[16];
    char t15[16];
    char *t1;
    char *t2;
    int64 t3;
    char *t4;
    int t5;
    int t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned int t19;
    int t20;

LAB0:    t1 = (t0 + 1344U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(22, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1244);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(23, ng0);
    t2 = (t0 + 2963);
    *((int *)t2) = 0;
    t4 = (t0 + 2967);
    *((int *)t4) = 7;
    t5 = 0;
    t6 = 7;

LAB8:    if (t5 <= t6)
        goto LAB9;

LAB11:    goto LAB2;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(24, ng0);
    t8 = (t0 + 2963);
    t9 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t7, *((int *)t8), 3);
    t10 = (t0 + 1576);
    t11 = (t10 + 32U);
    t12 = *((char **)t11);
    t13 = (t12 + 40U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 3U);
    xsi_driver_first_trans_fast(t10);
    xsi_set_current_line(25, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1244);
    xsi_process_wait(t2, t3);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 2963);
    t5 = *((int *)t2);
    t4 = (t0 + 2967);
    t6 = *((int *)t4);
    if (t5 == t6)
        goto LAB11;

LAB24:    t20 = (t5 + 1);
    t5 = t20;
    t8 = (t0 + 2963);
    *((int *)t8) = t5;
    goto LAB8;

LAB12:    xsi_set_current_line(26, ng0);
    t2 = (t0 + 684U);
    t4 = *((char **)t2);
    t2 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t15, 1, 8);
    t8 = (t0 + 2963);
    t9 = ieee_p_1242562249_sub_2540846514_1035706684(IEEE_P_1242562249, t7, t2, t15, *((int *)t8));
    t10 = (t7 + 12U);
    t16 = *((unsigned int *)t10);
    t17 = (1U * t16);
    t18 = 1;
    if (8U == t17)
        goto LAB18;

LAB19:    t18 = 0;

LAB20:    if (t18 == 0)
        goto LAB16;

LAB17:    goto LAB10;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    t13 = (t0 + 2971);
    xsi_report(t13, 19U, 2);
    goto LAB17;

LAB18:    t19 = 0;

LAB21:    if (t19 < 8U)
        goto LAB22;
    else
        goto LAB20;

LAB22:    t11 = (t4 + t19);
    t12 = (t9 + t19);
    if (*((unsigned char *)t11) != *((unsigned char *)t12))
        goto LAB19;

LAB23:    t19 = (t19 + 1);
    goto LAB21;

}


extern void work_a_1343320774_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1343320774_2372691052_p_0};
	xsi_register_didat("work_a_1343320774_2372691052", "isim/decoder_3to8_tb_isim_beh.exe.sim/work/a_1343320774_2372691052.didat");
	xsi_register_executes(pe);
}
