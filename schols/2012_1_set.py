import sys, re

#http://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf
def cyclic(num_verts, edges):
	verts = range(num_verts) #each node points to itself
	depths = [0 for i in xrange(num_verts)] #depth of the tree

	def find_root(i):
		while i != verts[i]:
			verts[i] = verts[verts[i]] #shorten the path
			i = verts[i] #get its parent
		
		return i

	for edge in edges:
		ai = int(edge[0])
		bi = int(edge[1])

		ar = find_root(ai)
		br = find_root(bi)

		if ar == br: #they're already in the same set
			return True

		else: #merge a and b
			ad = depths[ar]
			bd = depths[br]

			if ad < bd: #a's set is shorter 
				verts[ar] = br #link a's set to b's set
				verts[ai] = br #link this element directly to b's set
				
				depths[bd] += ad

			else: 
				verts[br] = ar
				verts[bi] = ar

				depths[ad] += bd

	return False

verts = input()
line = sys.stdin.readline()

pattern = re.compile('\((\d+) (\d+)\)')
ar = pattern.findall(line)
print ar

print cyclic(verts, ar)
