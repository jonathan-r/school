library ieee;
use ieee.std_logic_1164.all;

entity istruction_register_tb is
end istruction_register_tb;
 
architecture behavior of istruction_register_tb is 
    component instruction_register port(
         input : in  std_logic_vector(15 downto 0);
         il : in  std_logic;
         clock : in  std_logic;
         reset : in  std_logic;
         opcode : out  std_logic_vector(6 downto 0);
         dr : out  std_logic_vector(2 downto 0);
         sa : out  std_logic_vector(2 downto 0);
         sb : out  std_logic_vector(2 downto 0) );
    end component;
    
   signal memory_bus : std_logic_vector(15 downto 0) := (others => '0');
   signal il : std_logic := '0';
   signal clock : std_logic := '0';
   signal reset : std_logic := '0';

   signal opcode : std_logic_vector(6 downto 0);
   signal dr : std_logic_vector(2 downto 0);
   signal sa : std_logic_vector(2 downto 0);
   signal sb : std_logic_vector(2 downto 0);

   -- clock period definitions
   constant clock_period : time := 10 ns;
 
begin
   uut: instruction_register port map (memory_bus, il, clock, reset, opcode, dr, sa, sb);

   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 
   stim_proc: process
   begin		
      wait for clock_period;
		
		--reset
		reset <= '1';
		wait for clock_period;
		reset <= '0';
		assert opcode = "0000000";
		assert dr = "000";
		assert sa = "000";
		assert sb = "000";
		
		--load new instruction
		il <= '1';
		memory_bus <= x"abcd"; --1010 1011 1100 1101
		wait for clock_period;
		assert opcode = "1010101";
		assert dr = "111";
		assert sa = "001";
		assert sb = "101";
		
		--change memory bus but do not load
		il <= '0';
		memory_bus <= x"3f3f";
		wait for clock_period;
		assert opcode = "1010101";
		assert dr = "111";
		assert sa = "001";
		assert sb = "101";
		
      wait;
   end process;
end;
