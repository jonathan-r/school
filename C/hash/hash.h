#ifndef HASH_TABLE_H
#define HASH_TABLE_H

struct hashtable_node {
	char* data;
	struct hashtable_node* next;
};

struct hashtable_table {
	int size; //amount of items, fixed for the table's lifetime
	int* length; //of each of the lists below
	struct hashtable_node** table; //data linked lists
};

// construct a new hash table with size elements
struct hashtable_table * hashtable_new(int size);

// free the memory for all parts of the hashtable_table
void hashtable_free(struct hashtable_table* this);

// insert string into the hash table, no effect if it's already there
void hashtable_insert(struct hashtable_table* this, char * item);

// remove string from the hash table, no effect if not in table
void hashtable_remove(struct hashtable_table* this, char * item);

// return 1 if string is already in table, 0 otherwise
int hashtable_lookup(struct hashtable_table* this, char * item);

// print out each entry in the hash table and the values
// stored at that entry
void hashtable_print(struct hashtable_table* this);

// print a histogram of the usage of the table
void hashtable_print_stats(struct hashtable_table* this);

#endif
