.file "assembly.s"

	#export a global int "g", assign it the value 256
	.globl	g
	.data
	.align	4
	.type	g, @object
	.size	g, 4
g:
	.long	256

	#use intel syntax rather than the default at&t
	.intel_syntax noprefix
	.text
	
	#export the functions defined in this file,
	#specifying the type of symbol they are
	.globl	p_asm
	.type	p_asm, @function
	
	.globl	q_asm
	.type	q_asm, @function
	
	.globl	f_asm
	.type	f_asm, @function

#int p(int i, int j);
#computes (i + j) * 4 - 1
p_asm:
	push	ebp             #save previous stack frame
	                        #no need for a new stack frame

	mov	eax, [esp + 8]  #load i; notice esp, not ebp
	mov     ecx, [esp + 12] #load j; ditto
	add	eax, ecx        #compute i+j
	shl	eax, 2          #compute (i+j) << 2
	dec	eax             #and finally ((i+j) << 2) - 1

	pop	ebp             #restore previous stack frame
	ret                     #esp is at the return address already

#int q(int i);
#computes p(g, -i)
q_asm:
	push	ebp             #save previous stack frame
	mov	ebp, esp        #create a new stack frame

	mov	eax, [ebp + 8]  #load i arg
	neg	eax             #compute -i
	push	eax             #prepare -i as second argument
	mov	eax, g          #load g; the address will be resolved by the linker
	push	eax             #prepare g as the first argument
	call	p_asm           #execute p(g, -i)
	add	esp, 8          #remove args off the stack

	mov	esp, ebp        #clear current stack frame, bringing esp to the return addr
	pop	ebp             #restore previous stack frame
	ret                     #jump to address pointed to by esp

#int f(int n);
#computes factorial(n), for n > 0
#returns 1 for n <= 0
f_asm:
	push	ebp            #save previous stack frame
	mov	ebp, esp       #new stack frame

	mov	eax, [ebp + 8] #load n
	test	eax, eax       #see if it's <= 0
	jle	f_else         #if so, return 1

	dec	eax            #prepare n-1

	push	eax            #push argument for call of f(n-1)
	call	f_asm          #execute f(n-1)
	add	esp, 4         #clean argument off the stack

	mov	ecx, [ebp + 8] #reload n
	imul	ecx            #compute f(n-1) * n, with the result of f(n-1) being in eax

	jmp	f_ret          #now f(n-1) * n is in eax

f_else: #when n <= 0
	xor	eax, eax       #set to 0
	inc	eax            #return 1

f_ret: #common exit
	mov	esp, ebp
	pop	ebp
	ret

