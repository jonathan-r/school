\documentclass[12pt,article]{article}
\usepackage[hmargin=1cm,vmargin=1.5cm]{geometry}
\usepackage[romanian]{babel}
\usepackage{combelow}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{fancyvrb}
\usepackage{tabularx}

\raggedbottom

\begin{document}
\begin{center}
{\large \bf CS3071}\\
{\bf  Final Report}\\ 
\vspace{0.25 cm}
\small{\textbf{Jonathan Ro\cb{s}ca}}\\
\small{\textbf{12306261}}\\
\end{center}

\section*{Language summary}
This is a statically and strongly typed, imperative, garbage collected programming language running in a virtual machine. 
It supports (new features in \textbf{bold}):
\begin{itemize}
\setlength\itemsep{0em}
\item Constants
\item Strings
\item \textbf{Non-void procedures} %done
\item \textbf{Arguments to procedures} %done
\item \textbf{Dynamic allocation system} %done mostly
\item \textbf{Records} %done
\item \textbf{Multi-dimensional Arrays} %done
\item \textbf{Switch statements} %done, not tested
\item \textbf{Standard procedures} %done, not tested
\end{itemize}

\subsection*{Typing}
The language does not support any kind of type conversion. In fact, all terms in an expression must be of the same type. All type checking is done at compile time. There are two kinds of variable types, plain and reference, instances both of which occupy a single word.

For simplicity, instances of all types occupy a single word (16 bits).

Plain types (`int' and `bool'), and variables in this type contain their value directly, requiring no indirection for access.

Reference types (`string', arrays of any kind, records of any kind) are indirect, requiring (at the very least) a memory lookup for access. Direct assignment to a reference type variable causes the reference to change and does not affect the contents.

The `null' value is a special reference that can be assigned to any reference type and signifies that the variable does not point to a valid object. All local reference type variables effectively start out assigned to null at the beginning of the execution of their enclosing procedure. Access to a null reference causes a runtime error.

A reference that is not null is guaranteed to be valid.

\section*{Procedure arguments and return value}
Procedures can now take a fixed number (zero or more) of positional arguments. The procedure declaration must include the types and names of the arguments, and inside the body of the function, arguments are effectively local variables, having the same lifetime as local variables.
The function declaration thus becomes:
\begin{lstlisting}
proc <ident> (<type> <arg1 ident>, <type> <arg2 ident>, ...) : <type> {
	<body>
}
\end{lstlisting}
where the $<type>$ after the colon becomes the return type of the procedure itself.

Arguments are passed by value, which is straightforward for plain types, but passing in a reference variable does not copy the actual contents, only the reference.

Procedures can be either of void type, or of any type that a variable can be. Non-void procedures must contain, as the final line of their body,
\begin{lstlisting}
return <Expr>;
\end{lstlisting}
where $<Expr>$ is an expression in the same type as that of the procedure. Conversely, void procedures mustn't contain such a line. Furthermore, the ``return'' keyword cannot be used to exit a procedure early, like in C-family languages.

Non-void procedures can be used as part of an expression, for example, in assignment to a variable. They can also be called without being part of an expression, discarding their return value. Void procedures can only be called outside of expressions (as a single statement).

Procedure overloading is not supported, that is, a unique procedure identifier can refer to one procedure only.

\subsection*{Implementation}
The calling convention was changed (for simplicity) for both void and non-void procedures. One word of stack space is reserved (0 is pushed to the stack) before the functions call. This word of memory is referred to as the return slot.

Next, the arguments are pushed (Pascal-style, in order of declaration) onto the stack. In the procedure body, the arguments can be accessed as if they were stack local variables. They can be overwritten, and, like local variables, will cease to exist when the procedure exits.

Upon procedure exit (right before Leave and Ret), for non-void procedures, the return value is found at the top of the stack due to the return expression. The compiler generates a Sto instruction to pass the return value in the return slot.

The calling code then pops the arguments off the stack.

What then remains on the stack is the return value. For void functions, this will be a dummy value and so the calling code must pop it off the stack (using the new Pop instruction). This must also be done if the procedure is called on its own, outside of an expression.



\section*{Dynamic Allocation System}
In order to make use of strings, arrays and records, instances must be created. For ease of programming, a reference counting garbage collection system was implemented. To allocate a new object and set a reference variable to it, the special ``new'' value is assigned (type inferred):
\begin{lstlisting}
<reference container> := new;
\end{lstlisting}
where $<reference\ container>$ can be either a variable or some array and/or record access expression, eventually resolving to a reference variable. The only limitation is that a string cannot be allocated this way, since the length is not known.

\subsection*{Implementation}
At the start of execution, the HeapStart instruction will tell the virtual machine where the heap section of the data memory starts.

The list of free and used blocks is kept track of by the machine. The above statement causes an AllocS instruction to be generated, which takes a buffer size from the stack and tries to find a free block large enough to hold that contiguous buffer. The buffer is then pushed to the stack and can be assigned to some reference variable.

When an object instance is assigned to a variable, its reference count is incremented with the Ref instruction and the old reference's reference count is decremented with the UnRef instruction.

The Collect instruction causes the virtual machine to try to join unused blocks of free memory.

\section*{Records}
The language now supports a simple C struct like record type, templates of which must be defined in the global scope, as follows:
\begin{lstlisting}
record <ident> {
	<type> <member 1>, <member 2>, ...;
	...
	<type> <member n>;
}
\end{lstlisting}
where the members declared can be any of the existing types, (including reference the record being defined, but not records that have not yet been defined). A record cannot be empty.
Thereafter, the record template can be used as a type name. 

To refer to member variable of a record instance using a reference variable, the dot operator is used:
\begin{lstlisting}
<reference>.member
\end{lstlisting}
where `reference' is an access chain expression that ends in a reference type and `member' is a member of the record template of that type. This can be used for both reading a member variable as part of an expression, as well as for assigning to that member. In either case, the reference must not be null.

\subsection*{Implementation}
Record instances are stored only in the data memory. To access data, a version of the Sto and Load instruction have been added, which take the address from the stack, then load or store to that computed address in data memory, also to/from the stack. They are called StoS and LoadS respectively, the S standing for `Stack address'.



\section*{Arrays}
Any of the types can be made into a (possibly multi-dimensional) array, changing the type part of the variable and procedure argument declaration syntax to:
\begin{lstlisting}
<type>[size 1][size 2][size 3]...[size n]
\end{lstlisting}
where $<type>$ cannot be void, or any other array type (due to syntax ambiguity). Each size declaration must be either an integer literal or constant, with a value greater than 0.

The types of two arrays are equivalent only if, along with their basic type, they both have the same number of dimensions and the sizes of the individual are equal. An array reference variable can only be assigned to an array reference of the same type.

The syntax to access a member of an array, given a reference variable or procedure argument is:
\begin{lstlisting}
<ident>[<index 1 expr>][<index 2 expr>]...[<index n expr>]
\end{lstlisting}
where every index expression must be of type integer. Indexing is 0 based.

This is used for both read accesses (as in, as part of an expression in the basic type of the array), as well as assignment operations. Assignment operations will cause the actual contents of the array to change.

\subsection*{Implementation}
Because the array sizes are known at compile time, they are not stored at runtime and stricter type safety is also possible than otherwise.

Array contents are always word sized and are laid out linearly in a single buffer regardless of the number of dimensions, in row-major order. Elements of the least significant(rightmost) dimension are next to each other and full `slices' of one dimension are next to each other from the perspective of the next, more significant, dimension. Hence, given an $n$-dimensional array of sizes (most to least significant) $(s_1, s_2, ..., s_n)$, to access an element by indexes $(i_1, i_2, ..., i_n)$, the memory address to be computed is
\begin{align*}
base + \sum\limits_{d = 1}^{n - 1}{\left(i_d * \prod_{k = d + 1}^{n}{s_k}\right)} + i_n,
\end{align*}
where $base$ is the address of the start of the array contents storage. This index is computed at every array access at runtime, using the Add and Mult instructions.

For every array access, the compiler uses the StoS or LoadS instruction after the final index is computed on the stack.



\section*{Switch statements}
In addition to `if', `for' and `while' control flow schemes, the langauge now supports C like switch statements. They are limited to integer values. The syntax is as follows:
\begin{lstlisting}
switch (<integer expr>) {
	case (<integer literal/constant>) {
		<body>
		//fall-through
	}
	case (<integer literal/constant>) {
		<body>
		break;
	}
	case (<integer literal/constant>) {
		<body>
		break;
	}
	...
}
\end{lstlisting}
When a switch statement is encountered, case that corresponds to the value computed in the parantheses of the first line will be executed (if any).
If a case is executed, after the body executes, the optional ``break'' statement will cause execution to exit to switch altogether. The lack of a ``break'' line will cause the execution to continue to the next case statement, even if the check fails, until a ``break'' statement is encountered, or until there are no more cases.



\section*{Standard procedures}
The `read' and `write' I/O operations were replaced with the following `standard library' procedures, along with new ones:
\begin{itemize}
\item proc PeekInput() : bool; \\
Return true if there is at least one more item in the standard input queue.

\item proc ReadInteger () : int; \\
Reads the next item from standard input as an integer;\\
Causes a run-time error if there is no more input or the next item is not an integer.

\item proc ReadString () : string; \\
Reads the next item from standard input as a string, allocating memory for it and returning the reference to the string;\\
Causes a run-time error if there is no more input.

\item proc WriteString (string str) : void; \\
Writes str to standard output. str must not be null.

\item proc WriteInteger (int val) : void; \\
Writes val to standard output.

These procedures are statically added to the assembly generated by the compiler, as long as they are used in the program, on an individual basis.
\end{itemize}


\section*{Other changes}
\subsection*{Symbol system}
The Symbol tuple in the compiler was expanded into a class and the TastierKind enum was expanded into one class per kind, each extending the Symbol class. The kind of a symbol is now determined using RTTI. The TastierType enum was renamed to BasicType and a new class called FullType now contains complete type information, including array sizes and record template (if any).

\subsection*{External support}
All semblance of support for multiple compilation units has been removed, including the keyword ``external''. Also removed is the linker and assembly output symbol table.

\subsection*{Line numbers}
The assembly generated contains line start indicators before the first instruction that was generated from that line. Not all lines generate instructions, so not all lines get such an indicator in the generated assembly. This would help debugging.

\section*{Sample programs}
A handful of test programs were written to test and demonstrate the new features, in the test/Programs directory:
\begin{itemize}
\item Fibonacci.TAS: Recursive fibonacci number generator. Reads one number from standard input, computes the fibonacci number with that index, then writes it to standard output. 
\item Records.TAS: Use of records.
\item Alloc.TAS: Use of memory allocation.
\end{itemize}

\end{document}
