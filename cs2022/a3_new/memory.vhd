library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity memory is port( 
	address, write_data : in std_logic_vector (15 downto 0);
	memwrite : in std_logic; 
	read_data : out std_logic_vector (15 downto 0) );
end memory;

architecture behavioral of memory is
	type mem_array is array(0 to 511) of std_logic_vector(15 downto 0);
	
begin 
	mem_process: process(address, write_data)
	variable data_mem : mem_array := (
	 --opcode      dest    src_a   src_b
		"0010010" & "000" & "000" & "101",   --mov r0, #5
		"0010111" & "001" & "001" & "100",   --adi r1, r1, #4  <--
		"0010100" & "000" & "000" & "000",   --dec r0, r0        |
		"0001101" & "111" & "000" & "101",   --bnz 0x0001   ------
		--now r1 is 20
		
		"0010010" & "010" & "000" & "100",   --mov r2, #4
		"0100000" & "000" & "010" & "001",   --st [r2], r1
		--now mem[4] should be 20
		
		"0000010" & "000" & "000" & "001",   --b 0x0003  ----------------
		--gibberish below                                               |
		"0010111" & "111" & "000" & "110",   --adi r7, r0, #6; skipped  |
		"0010111" & "001" & "001" & "100",   --adi r1, r1, #4           |
		"0010011" & "010" & "001" & "000",   --inc r2, r1               |
		"0011100" & "001" & "001" & "000",   --not r1, r1               |
		                                     --                         |
		"0011100" & "001" & "001" & "000",   --not r1, r1   <------------
		"0011100" & "001" & "001" & "000",   --not r1, r1
		
	
		others => x"0000");
	variable addr : integer;
	
	begin
		-- the following type conversion function is std_logic_arith
		addr := conv_integer(unsigned(address(2 downto 0)));
		
		if memwrite = '1' then
			data_mem(addr):= write_data;
			
		else
			read_data <= data_mem(addr) after 10 ns;
			
		end if;
	end process;
end behavioral;
