#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "huffman.h" //definition of hcode and symbol

//uint8_t is synonymous with unsigned char, and comes from stdint.h
//str should be a NULL terminated string
//codes should be an array of 256 hcodes, for every byte value
uint8_t* huffman_encode(uint8_t str[], hcode hcodes[]){
	//first compute the amount of bytes required to hold the encoded string
	int length = 0; //in bits
	uint8_t* c = str; //iterator
	
	do{
		length += hcodes[*c].len;
	} while(*c++); //include the null terminator
	
	int bytes = length / 8;
	if (length & 0x7) bytes++;
	
	//allocate the buffer to hold the encoded string
	uint8_t* out = malloc(bytes);
	memset(out, 0, bytes);
	
	//go through the string again, this time creating the encoding
	c = str;
	uint8_t* current_byte = out; //output iterator
	int b = 0; //how many bits of *current_byte are filled
	do{
		unsigned data = hcodes[*c].data;
		int rem = hcodes[*c].len;
		
		//pack the codes at the bit level
		while (rem > 0){
			if (b + rem >= 8){
				//will fill the current byte
				*current_byte |= (data >> (rem - 8 + b)) & (0xFF >> b);//((1 << (8 - b)) - 1);
				rem -= 8 - b;
				
				current_byte++;
				b = 0;
			}
			
			else{
				//will not fill the current byte, always the final fraction
				*current_byte |= (data & (0xFF >> (8 - rem))) << (8 - (b + rem));
				b += rem;
				rem = 0;
			}
		}
	} while(*c++);
	
	return out;
}
