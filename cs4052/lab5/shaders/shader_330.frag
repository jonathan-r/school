#version 330

uniform sampler2D texture;

//camera
uniform vec3 eyeWorld; //world

//point light
uniform vec3 lightDir;
uniform vec3 lightDiffuse;
uniform vec3 lightSpecular;
uniform vec3 lightAmbient;

//material
uniform float lightFraction;
uniform vec3 matDiffuse;
uniform vec3 matSpecular;
uniform vec3 matAmbient;
uniform float matShine;

//from vertex shader
in vec3 worldVec; //world
in vec3 interpNormal; //world
in vec2 tex;

out vec4 FragColor;

void main() {
	//float ndcDepth = (2.0 * gl_FragCoord.z - gl_DepthRange.near - gl_DepthRange.far) / (gl_DepthRange.far - gl_DepthRange.near);
	//float cd = ndcDepth / gl_FragCoord.w;

	//sum of irradience
	vec3 Ir = lightAmbient * matAmbient;
	vec3 normal = interpNormal; //normalize(interpNormal);

	float ndl = dot(normal, lightDir);

	if (ndl > 0.0) {
		vec3 Id = lightDiffuse * matDiffuse * max(ndl, 0.0);
		Ir += Id;

		vec3 eyeDirNeg = normalize(worldVec - eyeWorld);
		vec3 lightOutDir = reflect(lightDir, normal);

		float specDot = dot(lightOutDir, eyeDirNeg);
		if (specDot > 0.0) {
			vec3 Is = lightSpecular * matSpecular * pow(specDot, matShine);
			Ir += Is;
		}
	}

	vec4 sample = texture2D(texture, tex);
	//vec3 final = mix(sample.xyz, Ir * sample.xyz, lightFraction); //lerp
	//vec3 final = mix(vec3(0.1), Ir, lightFraction); //lerp
	vec3 final = Ir * sample.xyz; //normal;//sample.xyz;
	//vec3 final = sample.xyz * normal;
	FragColor = vec4(final, 1.0);
}
