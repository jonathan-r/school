import java.lang.Thread;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;

import java.util.ArrayList;

//A thread that received and stores packets without pause, so no incoming packets will be missed
//Optionally, only packets coming from a specified ip address are stored
public class Listener extends Thread{
	private DatagramSocket socket;
	private ArrayList<DatagramPacket> packets;
	private InetAddress source;
	private boolean running;
	
	//bindPort - port of the socket
	//expectedsource - packets will only be stored if they come from this source. if null, every packet received is stored, regardless of source
	public Listener(int bindPort, InetAddress expectedSource){
		try{
			socket = new DatagramSocket(bindPort);
			packets = new ArrayList<DatagramPacket>();
			source = expectedSource;
			running = false;
			
			//since there is no way to interrupt Datagramocket.re
			socket.setSoTimeout(1000);
		}
		
		catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}	
	
	public void run(){
		running = true;
		while (running){
			try{
				//make a buffer large enough to hold any packet
				byte[] data = new byte[Symbols.PACKET_SIZE];
				DatagramPacket packet = new DatagramPacket(data, data.length);
				
				//blocking receive
				socket.receive(packet);
				
				synchronized (packets){
					//System.out.println("received packet from " + packet.getAddress().toString() + ":" + packet.getPort());
					
					if (source == null || packet.getAddress().equals(source)){
						packets.add(packet);
					}
				}
			}
			catch (SocketTimeoutException e){
				//do nothing
			}
			
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void kill(){
		running = false;
	}

	public synchronized DatagramPacket getPacket(){
		if (packets.size() > 0) return packets.remove(0);
		else return null;
	}
}
