
public class CRC {
	public static final byte defaultPolynomial = 0xD; //1101

	//remainder is expected to be on the last byte
	public static byte computeCRC(byte[] data, int bytes, byte poly) {
		String divisor = Bits.toBitString(poly, 0);
		int polyLength = divisor.length();
		String divisorZero = Bits.toBitString((byte)0, polyLength);
		
		int lengthBits = bytes * 8;

		//print "<data><remainderIn> / 1101"
		for (int i = 0; i < bytes; i++) System.out.print(Bits.toBitString(data[i], 8));
		System.out.println(" / " + divisor);
		
		//initial data to divide:
		//the polynomial is at most a byte long, so only the first byte of the data is needed
		byte mask = (byte)((1 << polyLength) - 1);
		int next = 8 - polyLength;
		byte d = (byte) ((data[0] >> (8 - polyLength)) & mask);
		
		int leading = 0; //amount of leading spaces, used for printing the calculation
		byte remainder = 0; //result

		while (next <= lengthBits) {
			// print leading space for the divisor
			for (int i = 0; i < leading; i++) System.out.print(' ');

			// pick the divisor:
			// 0 if the leading bit of the top line is zero
			// poly otherwise
			byte p;
			if (((d >> (polyLength - 1)) & 1) == 0) {
				p = 0;
				System.out.println(divisorZero);
			} 
			
			else {
				p = poly;
				System.out.println(divisor);
			}

			// compute the xor of the current line and the divisor as chosen
			// above
			byte x = (byte) (d ^ p);

			if (next < lengthBits) {
				//drop the next bit from the data (or remainder when the data ends)
				int n = (data[next / 8] >> (7 - next % 8)) & 1;
				x = (byte) ((x << 1) | n);
				d = x;
			}
			else remainder = x;

			String xored = Bits.toBitString(x, polyLength - ((next == lengthBits)? 1 : 0));

			// print the leading space for the next line
			leading++;
			for (int i = 0; i < leading; i++) System.out.print(' ');
			System.out.println(xored);

			next++;
		}

		return remainder;
	}
}
