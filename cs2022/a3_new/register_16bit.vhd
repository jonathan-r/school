library ieee;
use ieee.std_logic_1164.all;

entity register_16bit is port ( 
	load, clk, reset : in std_logic;
	d    : in std_logic_vector (15 downto 0);
	q    : out std_logic_vector (15 downto 0) );
end register_16bit;

architecture behavioral of register_16bit is begin 
	process(reset, clk) begin
		if reset = '1' then
			q <= x"0000";
		
		elsif clk'event and clk = '1' then
			if load = '1' then
				q <= d;
			end if;
			
		end if;
	end process;
end behavioral;
