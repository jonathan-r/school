library(shiny)
library(plyr)
library(ggplot2)

#http://www.andrews.edu/~calkins/math/edrm611/edrm11.htm
#http://www.cookbook-r.com/Graphs/Plotting_distributions_%28ggplot2%29/

#Takes the input object from the server function
#Returns the standard deviation value based on user input
#Valid results are > 0
#If the sample csv option is taken, -1 is returned when there is no file uploaded or there was an error
get_sd_val <- function(input) {
  if (input$source_type == 'csv') {
    in_file <- input$file_input
    col <- input$csv_header_input
    
    if (is.null(in_file)) {
      return (-1)
    }
    
    data <- read.csv(in_file$datapath, header = input$csv_input_header, sep = input$csv_input_sep, 
      quote = input$csv_input_quote)
    
    if (col != "") {
      if (!(col %in% colnames(data))) {
        return (-2)
      }
    }
    else {
      col <- colnames(data)[1]
    }
    
    return (sd(data$col))
  }
  else if (input$source_type == 'sd_lin') {
    return (input$sd_input_lin)
  }
  else {
    return (10 ^ input$sd_input_log)
  }
  
  return (-1)
}

#Takes the input object from the server function
#Returns the significant difference value based on user input
get_diff_val <- function(input) {
  if (input$diff_input_type == 'diff_lin') {
    val <- input$diff_input_lin
  }
  else {
    val <- 10 ^ input$diff_input_log
  }
  
  return (val)
}

#Takes the input object from the server function
#Returns the critical z value based on user input
get_crit_z <- function(input) {
  alpha_val <- input$alpha_input
  beta_val <- input$beta_input  
  return (qnorm(1.0 - alpha_val) - qnorm(beta_val))
}

#limit upload size to 2MB
options(shiny.maxRequestSize = 2*1024^2)

shinyServer(function(input, output) {
  #relay standard deviation value back to frontend
  output$sd_val <- renderText({
    sd_val = get_sd_val(input)
    paste("SD: ", toString(sd_val))
  })
  
  #relay significant difference value back to frontend
  output$diff_val <- renderText({
    diff_val = get_diff_val(input)
    paste("diff: ", toString(diff_val))
  })
  
  #relay mimimum sample size value to frontend
  output$sample_size <- renderText({
    sd_val <- get_sd_val(input)
    diff_val <- get_diff_val(input)
    crit_z <- get_crit_z(input)
    
    #z = (upper - mean) / (sd / sqrt(n))
    #sqrt(n) = (z * sd) / (upper - mean)
    #n = (z^2 * sd^2) / (upper - mean)^2
    
    #upper = z * (sd / sqrt(n)) + mean
    #here, mean is 0
    
    n <- ceiling((crit_z * crit_z * sd_val * sd_val) / (diff_val * diff_val))
    paste("", toString(n))
  })
  
  #relay percentage of area of the alternative hypothesis distribution > upper percentile to frontend
  output$percentage_area <- renderText({
    sd_val <- get_sd_val(input)
    diff_val <- get_diff_val(input)
    crit_z <- get_crit_z(input)
    upper <- crit_z * sd_val # + mean, but that's always 0
    
    alt_z <- (upper - diff_val) / sd_val
    area <- (1.0 - pnorm(alt_z)) * 100
    
    paste("", toString(area))
  })
  
  #render the distributions plot
  output$plot <- renderPlot({
    sd_val <- get_sd_val(input)
    diff_val <- get_diff_val(input)
    crit_z <- get_crit_z(input)
    divs <- 4 #standard deviations either side of the mean to plot
    
    if (sd_val >= 0) {
      #we have a standard deviation and an alpha value
      #compute the critical z, which will allow us to find the upper limit
      upper = crit_z * sd_val # + mean, but that's always 0
      
      x_null <- seq(-sd_val * divs, sd_val * divs, length = 100)
      x_alt <- seq(diff_val - sd_val * divs, diff_val + sd_val * divs, length = 100)
      
      y_null <- dnorm(x_null, mean = 0, sd = sd_val)
      y_alt <- dnorm(x_alt, mean = diff_val, sd = sd_val)
      
      df_null <- data.frame(x = x_null, y = y_null)
      df_alt <- data.frame(x = x_alt, y = y_alt)
      
      #start with the alternative hypothesis, so that the geom_ribbon applies to it
      #the environment argument allows predicates in the geom_* calls to access variables from this scope
      ggplot(df_alt, aes(x=x, y=y), environment = environment()) + 
        xlim(-sd_val * divs, diff_val + sd_val * divs) + #set the x bounds
        geom_ribbon(data = subset(df_alt, x > upper), #shade the area under the alternate hypothesis
          aes(x = x, ymin = 0, ymax = y), alpha=0.3) + 
        geom_line(colour = "red") + #draw the alternate hypothesis distribution
        geom_line(data=df_null, aes(x=x, y=y), colour = "blue") + #now the null hypothesis distribution
        ylab("") + theme(axis.ticks.y=element_blank()) #the y axis has no real meaning so remove the label
    }
  })
})