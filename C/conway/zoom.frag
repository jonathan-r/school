//sphere
void main(void){
	vec2 uv = gl_FragCoord.xy; /// iResolution.xy;
	vec2 mouse = iMouse.xy;
	
	vec2 diff = uv - mouse;
	
	const float R = 100.0;
	const float h = 80.0;
	float hr = R * sqrt(1.0 - ((R - h) / R) * ((R - h) / R));
 
	float r = sqrt(dot(diff, diff));
	vec2 offset = (r < hr) ? diff * (R - h) / sqrt(R * R - r * r) : diff;
	
	gl_FragColor = texture2D(iChannel0, (mouse + offset) / iResolution.xy);
}


//2nd version
void main(void){
	vec2 uv = gl_FragCoord.xy; /// iResolution.xy;
	vec2 mouse = iMouse.xy;
	
	vec2 diff = uv - mouse;
	
	//x^3 - x^2 + 4/27, stop at x = 2/3
	const float end = 2.0/3.0;
	
	float r = sqrt(dot(diff, diff)) / 100.0;
	vec2 off;
	
	if (r < end){
		float o = (r*r*r - r*r + 4.0/27.0) * 50.0;
		off = diff / o;
	}
	else{
		off = diff;
	}
	
	
	//vec2 offset = (r < hr) ? diff * (R - h) / sqrt(R * R - r * r) : diff;
	
	gl_FragColor = texture2D(iChannel0, (mouse + off) / iResolution.xy);
}
