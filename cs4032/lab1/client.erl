#!/usr/bin/env escript

%-module(client).
%-export([send_message/3]).

recv_message(error, Sock) ->
	''.

recv_message(ok, Sock) ->
	{Status, Data} = gen_tcp:recv(Sock, 16),
	Follow = recv_message(Status, Sock),
	concat(Data, Follow).

send_message(Host, Port, Msg) ->
	{ok, Sock} = gen_tcp:connect(Host, Port, raw),
	ok = gen_tcp:send(Sock, concat(concat("GET /echo.php?message=", Msg), " HTTP/1.1\r\n\r\n")),
	RecMsg = recv_message(ok, Sock),
	io:format(RecMsg),
	ok = gen_tcp:close(Sock).

main([Host, Port, Msg]) ->
	send_message(Host, Port, Msg).

