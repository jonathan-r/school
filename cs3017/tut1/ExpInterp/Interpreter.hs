module Interpreter where

import Syntax


--
-- Big-Step Interpreter for arithmetic expressions
--
bigStepExp :: Exp -> Val
bigStepExp (CNum n)    = VNum n

bigStepExp (Add e1 e2) = let (VNum n1) = bigStepExp e1 in
                         let (VNum n2) = bigStepExp e2 in
                         let n3        = (n1 + n2) in
                         (VNum n3)

bigStepExp (Sub e1 e2) = let (VNum n1) = bigStepExp e1 in
                         let (VNum n2) = bigStepExp e2 in
                         let n3        = (n1 - n2) in
                         (VNum n3)

bigStepExp (Mul e1 e2) = let (VNum n1) = bigStepExp e1 in
                         let (VNum n2) = bigStepExp e2 in
                         let n3        = (n1 * n2) in
                         (VNum n3)

bigStepExp (Div e1 e2) = let (VNum n1) = bigStepExp e1
                             (VNum n2) = bigStepExp e2
                             n3        = quot n1 n2 in
					     (VNum n3)

--
-- Small-step Interpreter for Arithmetic Expressions
--
smallStepExp :: Exp -> Maybe Exp
smallStepExp (CNum n)                  = Nothing

smallStepExp (Add (CNum n1) (CNum n2)) = do let n3 = n1 + n2 
                                            return (CNum n3)

smallStepExp (Add (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Add (CNum n1) newE2)

smallStepExp (Add e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Add newE1 e2)

smallStepExp (Sub (CNum n1) (CNum n2)) = do let n3 = n1 - n2
                                            return (CNum n3)

smallStepExp (Sub (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Sub (CNum n1) newE2)

smallStepExp (Sub e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Sub newE1 e2)

smallStepExp (Mul (CNum n1) (CNum n2)) = do let n3 = n1 * n2
                                            return (CNum n3)

smallStepExp (Mul (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Mul (CNum n1) newE2)

smallStepExp (Mul e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Mul newE1 e2)

smallStepExp (Div (CNum n1) (CNum n2)) = do let n3 = quot n1 n2
                                            return (CNum n3)

smallStepExp (Div (CNum n1) e2)        = do newE2 <- smallStepExp e2
                                            return (Div (CNum n1) newE2)

smallStepExp (Div e1 e2)               = do newE1 <- smallStepExp e1
                                            return (Div newE1 e2)


