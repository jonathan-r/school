#include <stdio.h>
#include <string.h>

#include "list.h"

void print_chars_in_set(struct listset* set){
	printf("[");
	struct listnode* node;
	for (node = set->start; node; node = node->next){
		printf(" %c", node->data);
	}
	printf(" ]");
}

int main(){
	const int max = 2048;
	char buffer[max];
	struct listset* sets[2];

	printf("Write two lines lines of text representing two sets. Up to %d characters. The newline character is not included in the set.\n", max);

	int i;
	for (i = 0; i < 2; i++){
		sets[i] = listset_new();
	
		char* c = fgets(buffer, max, stdin);

		if (c){ //c might be NULL in case of a an i/o error
			while (*c) {
				if (*c != '\n') listset_add(sets[i], *c);
				c++;
			}
		}
	}
	
	for(i = 0; i < 2; i++){
		printf("set %d contains: ", i);
		print_chars_in_set(sets[i]);
		printf(".\n");
	}

	struct listset* out = listset_new();

	listset_union(out, sets[0], sets[1]);
	printf("union: ");
	print_chars_in_set(out);
	listset_destroy(out);
	
	out = listset_new();
	listset_intersect(out, sets[0], sets[1]);
	printf("\nintersection: ");
	print_chars_in_set(out);
	printf("\n");

	listset_destroy(sets[0]);
	listset_destroy(sets[1]);
	listset_destroy(out);

	return 0;
}
