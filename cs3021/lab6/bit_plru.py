from base import BaseCache

class BitPlruCache(BaseCache):
	"""Cache with simple single-bit pseudo-least-recently-used replacement policy."""
	def __init__(self, L, N, K, addr_bits):
		BaseCache.__init__(self, L, N, K, addr_bits)

	def on_reset(self):
		#K by K bit fields for every line
		self.mru_bits = [[False for i in range(self.K)] for j in range(self.N)]

	def update_set(self, set_id, way_id):
		if self.K > 1:
			self.mru_bits[set_id][way_id] = True
			if self.mru_bits[set_id].count(False) == 0:
				#all lines are marked as "recently used"
				for q in range(self.K):
					if q != way_id:
						#so unmark all other lines
						self.mru_bits[set_id][q] = False

	def on_hit(self, tag_id, set_id, way_id):
		self.update_set(set_id, way_id)

	#returns which way to place the data
	def on_miss(self, tag_id, set_id):
		#find the first line with a 0 bit (there always has to be at least one)
		way_id = self.mru_bits[set_id].index(False)
		self.update_set(set_id, way_id)
		return way_id

	def show_extra_info_column_name(self):
		return 'mru bits'

	def show_extra_info(self, set_id):
		#table; sadly must be flattened to a line
		return ''.join(map(lambda x : "01"[int(x)], self.mru_bits[set_id]))