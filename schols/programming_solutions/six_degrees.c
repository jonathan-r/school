#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <math.h>

//The question was to determine, given a social network (see the person struct), whether (to use fancy terms from Wilkins' notes) for any two distinct vertices in the network, their shortest path contains fewer that or equal to k edges.

//my solution is that great. I couldn't think of a better algorithm than a naive one, so it's got an atrocious running time (both in term of algorithm analysis and actual running time on my core i7)


typedef struct person_s{
	char* name;
	int knowns; //slight deviation from the question, where the list of known people was (presumably, it's not stated) NULL terminated
	struct person_s** known;
} person;


//altogether, O(n^2 * (len(V) + len(E))); n is the number of population(people), len(V) is n, len(E) is number of links between the population of the graph between n and n^2 (around n * average degree of a vertex)
char* bf_mark; //array of booleans for every person, used in searching
person* bf_base; //value of people[], so that the index of a person can be derived from its pointer

//determines whether there is a path (at least one) within (bf_)k hops.
//since the is no heuristic for how close we're getting to the target, the best we can do is blindly go on any route possible
//returns -1 if there is no path or it would take > k hops
//returns the length of the path otherwise
int bfs_distance_within_k(person* at, person* to, int hops){
	//mark self
	int self_id = (int)(at - bf_base);
	bf_mark[self_id] = 1;
	
	//breadth first
	for (int n = 0; n < at->knowns; n++){
		if (at->known[n] == to){
			bf_mark[self_id] = 0;
			return 1; //one hop
		}
	}
	
	//otherwise recurse
	int min = -1; //shortest path from here to the destination
	
	if (hops < bf_k){ //there's no point if the path would be longer than k
		for (int n = 0; n < at->knowns; n++){
			person* next = at->known[n];
			int id = (int)(next - bf_base);
			if (!bf_mark[id]){
				int len = bfs_distance_within_k(next, to, hops + 1);
				if (len != -1 && (len < min || min == -1)) min = len;
			}
		}
	}
	
	//remove the mark
	bf_mark[self_id] = 0;
	return (min == -1)? -1 : min + 1; //+1 if there is a path because of the one hop from this node to the shortest path
}

int within_k_degrees_bruteforce(person* people, int num_people, int k){
	bf_mark = malloc(num_people);
	memset(bf_mark, 0, num_people);
	bf_base = people;
	bf_k = k;
	
	//for every pair of vertices, n(n-1)/2 in total, avoiding backward edges
	for (int i = 0; i < num_people; i++){
		for(int j = i + 1; j < num_people; j++){
			int dist = bfs_distance_within_k(&people[i], &people[j], 1);
			
			if (dist == -1){
				return 0;
			}
		}
	}
	
	free(bf_mark);
	return 1;
}


//***everything below is not part of the answer, just a testbed

//O(1)
void link(person* a, person* b){
	a->known[a->knowns++] = b;
	b->known[b->knowns++] = a;
}

//O(k), for max k degree vertices
void safe_link(person* a, person* b, int max_degree){
	if (a->knowns == max_degree || b->knowns == max_degree) return;
	for (int i = 0; i < a->knowns; i++) if (a->known[i] == b) return;
	for (int i = 0; i < b->knowns; i++) if (b->known[i] == a){
		assert(!"unidirectional link");
		return;
	}
	link(a, b);
}

void generate_network(int population, int max_degree, person* people){
	//Efficient generation of large random networks, Batagelj, Brandes
	//algorithm 1, O(population + edges)
	int v = 1;
	int w = -1;
	
	float p = (float)max_degree / (population);
	float log_1p = log(1 - p);
	
	while (v < population){
		//r is in [0, 1)
		int ri = rand();
		if (ri == RAND_MAX) ri--;
		float r = (float)ri / RAND_MAX; 
		
		w += 1 + (int) (log(1 - r) / log_1p);
		while (w >= v && v < population){
			w -= v;
			v++;
		}
		if (v < population) safe_link(&people[v], &people[w], max_degree);
	}
}

int main(int argc, char** argv){
	srand(time(NULL));
	
	if (argc == 3){
		int population = atoi(argv[1]);
		int max_degree = atoi(argv[2]);
		
		if (population <= 0 || max_degree <= 0){
			printf("population and max degree must both be greater than 0");
			return 1;
		}
		
		//allocate enough to allow each vertex to be of degree max_degree
		person** link_storage = malloc(population * max_degree * sizeof(person*)); //note that it's an array of pointers
		memset(link_storage, 0, population * max_degree * sizeof(person*)); //and set every byte to 0
	
		person* people = malloc(population * sizeof(person)); //actual storage of people
		for (int i = 0; i < population; i++){
			char* name = malloc(11);
			sprintf(name, "%d", i);
			people[i].name = name;
			people[i].knowns = 0;
			people[i].known = link_storage + i * max_degree;
		}

		generate_network(population, max_degree, people);
		
		
		
		for (int i = 0; i < population; i++) free(people[i].name);
		free(people);
		free(link_storage);
	}
	
	else printf("usage: %s <population> <max degree> <k>\n", argv[0]);
	
	return 0;
}	
	

