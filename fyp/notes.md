# Final Year Project

## VR: "natural interaction with objects in virtual environments"
Attempt to create a natural interface for 3d VR enviroments.
Using head tracking for camera and body positioning as well as hand tracking, investigate the feasibility of a system that enables interaction with the enviroment without the need for controllers or keyboards. This would imply a class system for the kinds of objects in the world, where interaction with each is context sensitive and limited. 

For example, a small tennis ball would be picked up when hovering one's hand over it. Then if the user makes a throwing motion, the ball is thrown as if it was held. Another example would be a keypad. The user's virtual hand would become a close fist with the index finger sticking out, in a button-pressing pose. The button to press would be selected by moving the hand around and the selected button would be pressed when the user moves their hand slightly forward.

This system would rely on the precision of the hand trackers. It would be very jarring for a user's hand tracking not to sync up with their head tracking.

Another problem is the lack of physical feedback; how should the VR enviroment behave if the user moves freely in the physical world but there's a wall or object in the virtual world? Related is the lack of tactile feedback.

Will use head mounted tracking and display system such as the Oculus Rift or the HTC Vive.

to research:
* hand tracking systems 
* object affordance
* natural user interface

## AR: "3d projection onto arbitrary surfaces"
Investigate the feasibility of a system composed of a projector and static projection destination such as a the background of a dining hall.

First, scan the destination so as to make the system aware of the geometry. Based on this information, as well as the projector position/orientation, project an image (may have some motion) of an environment so as to give the illusion of the presence of a different enviroment, such as a beach scene in the dining hall.

One consideration is ambient sound.

to research:
* 3d scanning of environments (static, concave and from within, as opposed to small objects that can be moved/rotated)
* warping of images to suit projection
* where to acquire/how to create the images to project

## Supervisor
Carol O'Sullivan, office 007, top floor, Stack B, Custom House Quay
