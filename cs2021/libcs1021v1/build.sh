#compile library to object code
arm-none-eabi-as LPC2400.s -mcpu=arm7tdmi -o lpc2400.o
arm-none-eabi-gcc -c Retarget.c Serial.c main.c

#compile user code to object code
arm-none-eabi-as start.s -mcpu=arm7tdmi -o start.o

#statically link object code
#arm-none-eabi-gcc --specs=rdimon.specs -Wl,--start-group -lgcc -lc -lm -lrdimon -Wl,--end-group -o test.elf *.o
#arm-none-eabi-gcc --specs=nosys.specs -o test.elf *.o
arm-none-eabi-gcc -o test.elf *.o

#generate hex file that the isp programmer can load
arm-none-eabi-objcopy -O ihex test.elf test.hex

#is an archive needed here?
#ar rcs liblpc.a lpc2400.o Retarget.o Serial.o main.o test_start.o
#rm *.o

#http://stackoverflow.com/questions/13235748/linker-error-on-a-c-project-using-eclipse

