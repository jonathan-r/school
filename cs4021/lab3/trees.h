#pragma once

#include <stdint.h>
#include <assert.h>

#include <atomic>

#include "helper.h"


#define BST_ALLOC 1023

class BST {
public:
	BST() {
		pool = nullptr;
		poolIdx = 0;
	}

private:
	struct alloc {
		node load[BST_ALLOC];
		alloc* prev;

		alloc(alloc* prev) {
			this->prev = prev;
			memset(load, 0, BST_ALLOC * sizeof(node));
		}
	};

	struct node {
		node *l, *r;
		uint64_t val;

		bool free() const {
			return (val >> 63) == 0;
		}
	};

	volatile node* volatile root;
	thread_local alloc* pool;
	thread_local int poolIdx;

	//per-thread
	void threadInit() {
		pool = new alloc(nullptr);
		poolIdx = 0;
	}

	//thread-local
	node* newNode() {
		if (poolIdx == BST_ALLOC) {
			pool = new alloc(pool);
			poolIdx = 0;
		}

		return pool->load[poolIdx++];
	}

	//lock thread safe
	void insert(uint64_t val) {
		val |= 1 << 63; //force msb

		if (!root) {
		}
		node* at = root;
		node* prev = nullptr;
	
	}

	//not thread safe
	void check() {
	}
};
