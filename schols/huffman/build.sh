#!/bin/bash

CC=clang
OUT=huffy

$CC analyze.c decode.c encode.c latex_tree.c test.c -o $OUT -g3 -O0
