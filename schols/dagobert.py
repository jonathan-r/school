

def bridge(b, planks):
	body = 0
	left = 0
	right = 1
	
	#move right 0
	#move left  1
	#move body  2
	state = 0 
	
	moves = 0
	
	while body + b < len(planks) - 1:
		if state == 0:
			for i in range(body + b - 1, right, -1):
				if planks[i] == 'p':
					right = i
					state = 1
					break
		
		elif state == 1:
			for i in range(right, body - 1, -1):
				if planks[i] == 'p':
					left = i
					state = 2
					break
					
					
		elif state == 2:
			if body < left:
				body += 1
				
			else:
				state = 0
				
		moves += 1
		
	return moves 
	
print bridge(5, "pp pp   ppp")

