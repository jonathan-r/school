module HaskellGame.Interaction where

import HaskellGame.Datatypes

{-
  Check if the player's new position would collide with something.
  Return True if there would be a collision.
  Return False if there would be no collision.

  Lab 2: Implement this function for 20 marks
-}

detectCollision :: Scene -> Point -> Bool
detectCollision (Scene (Map w h tiles) (Player (px, py)) obs monsters) (nx, ny) =
  (nx >= w) || (nx < 0) || (ny < 0) || (ny >= h) || 
  not (walkable (tiles!!ny!!nx)) || 
  detectCollisionLocated obs (nx, ny) ||
  detectCollisionLocated monsters (nx, ny)

walkable :: Tile -> Bool
walkable Grass = True
walkable _ = False

detectCollisionLocated :: Located a => [a] -> Point -> Bool
detectCollisionLocated [] _ = False
detectCollisionLocated (loc : ls) (px, py) = 
  (lx == px) && (ly == py) ||
  detectCollisionLocated ls (px, py)
  where
    (lx, ly) = position loc

{- Handle a key press from the player -}

handleInput :: Char -> Scene -> Scene
handleInput c theScene@(Scene map player objects monsters) =
  let newPosition = move player c
  in
    if detectCollision theScene newPosition then
      -- if there would be a collision, we'll just give back the old scene
      theScene
    else
      -- if there would be NO collision, we move the player
      (Scene map (Player newPosition) objects monsters)
  where
    move (Player (x, y)) chr =
      case c of
        'i' -> (x, (y-1))
        'j' -> ((x-1), y)
        'k' -> (x, (y+1))
        'l' -> ((x+1), y)
        _   -> (x, y)
