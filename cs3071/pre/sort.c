void sort(int* array, int size){
	for(int i = 0; i < size-1; i++){
		while(array[i+1] > array[i] && i >= 0){
			int tmp = array[i];
			array[i] = array[i+1];
			array[i+1] = tmp;
			i--;
		}
	}
}

