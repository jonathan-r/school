#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <stdio.h>
#include <limits>
#include <string>
#include <algorithm>

using namespace std;
using namespace cv;

#define BG_THRESH 0.2
#define IMAGE_SLICES 5

struct NameTemplate {
	const char* name;
	int qstIdx;

	NameTemplate(const char* format) {
		name = format;
		const char* tplq = strchr(format, '?');
		qstIdx = (tplq)? (tplq - format) : -1;
	}

	bool isSequence() const {
		return qstIdx != -1;
	}

	//"name?.jpg" and id=5 to "name5.jpg" (sequence)
	//"name.jpg" to "name.jpg" (single image)
	string makeImgName(int id) {
		char out[256];
		if(isSequence()) {
			memcpy(out, name, qstIdx);
			char* atPrefix = out + qstIdx;
			sprintf(atPrefix, "%d%s", id, name + qstIdx + 1);
			return string(out);
		}
		else {
			return string(name);
		}
	}
};

struct Bound {
	double min, max;
	Bound() {
		max = 0.0;
		min = numeric_limits<double>::max();
	}

	void update(double x) {
		min = std::min(x, min);
		max = std::max(x, max);
	}
};

double bottleSd(const Mat& singleBottleBGR, double bgThresh) {
	//convert to Hue-Light-Saturation
	Mat hls;
	cvtColor(singleBottleBGR, hls, CV_BGR2HLS);

	//split into channels - ignore Hue
	vector<Mat> hlsChannels;
	split(hls, hlsChannels);	
	Mat& light = hlsChannels[1];
	Mat& saturation = hlsChannels[2];

	//threshold out background
	Mat noBg;
	threshold(light, noBg, bgThresh * 255, 255, THRESH_TOZERO);// | THRESH_OTSU);
	
	//compute standard deviation, ignore background pixels
	Scalar mean, stddev;
	meanStdDev(saturation, mean, stddev, noBg);

	return stddev[0];
}

int main(int argc, char** argv) {
	if (argc != 4) {
		printf("usage: label_hist <train_blank> <train_label> <test>\n"
				"Where train_blank and train_label should each be image(s) of a single bottle,\n"
				"while test image(s) should have 5 bottles each equally spaced apart (horizontally).\n"
				"Any of the arguments may be a single image or may contain a '?' character,\n"
				"in which case it is assumed there's a sequence of images,\n"
				"with the '?' being replaced by sequence number starting with 1.\n");
		return 0;
	}

	//prcess train and test arguments
	NameTemplate blankNames(argv[1]);
	NameTemplate labelNames(argv[2]);
	NameTemplate testNames(argv[3]);

	NameTemplate* imgTemplate[] = {&blankNames, &labelNames};

	//assume about the image sequences
	const int seqStartIdx = 1;

	//text drawing
	Scalar textCol(255, 255, 255);
	Scalar textBgCol(0, 0, 0);
	char text[256];

	//histogram constants
	int bins = 200;
	float range[] = {0, 256};
	const float* hr = {range};

	Bound bounds[2];

	//look at blank and labeled images first
	for (int kind = 0; kind < 2; kind++) {
		int seq = seqStartIdx;
		string imgName = imgTemplate[kind]->makeImgName(seq);
		do {
			Mat img = imread(imgName);
			Mat crop = img(Rect(0, img.rows/2, img.cols, img.rows/2));
			if (!img.data) {
				if (seq == seqStartIdx) {
					//not even one image
					fprintf(stderr, "no %s training image(s) found", (kind == 0)? "blank" : "label");
					return 0;
				}
				else {
					//no more in the sequence
					break;
				}
			}

			//process image
			double sd = bottleSd(crop, BG_THRESH);
			bounds[kind].update(sd);
			fprintf(stderr, "loaded image %s, sd = %.3f\n", imgName.c_str(), sd);

			//next image
			seq++;
			imgName = imgTemplate[kind]->makeImgName(seq);
		} while (imgTemplate[kind]->isSequence());
	}

	double boundary = (bounds[0].max + bounds[1].min) / 2;
	fprintf(stderr, "blank sd range: %.2f to %.2f\n", bounds[0].min, bounds[0].max);
	fprintf(stderr, "label sd range: %.2f to %.2f\n", bounds[1].min, bounds[1].max);
	fprintf(stderr, "boundary %.2f\n", boundary);

	printf("{\n"); //json start
	string imgName = testNames.makeImgName(seqStartIdx);
	string nextName;
	Mat next = imread(imgName);
	for (int seq = seqStartIdx+1; testNames.isSequence() && next.data; seq++, imgName = nextName) {
		Mat current = next;
		printf("\t\"%s\" : [\n", imgName.c_str()); //json file start

		//try to load the *next* image
		nextName = testNames.makeImgName(seq);
		next = imread(imgName);

		int sliceW = current.cols / IMAGE_SLICES;
		int sliceH = current.rows / 2;
	
		for (int si = 0; si < IMAGE_SLICES; si++) {
			Rect crop(si * sliceW, sliceH, sliceW, sliceH);
			Mat slice = current(crop); //crop slice

			//convert to HLS, split into channels
			Mat hls;
			cvtColor(slice, hls, CV_BGR2HLS);
			vector<Mat> hlsChannels;
			split(hls, hlsChannels);
			
			Mat& light = hlsChannels[1];
			Mat& saturation = hlsChannels[2];

			//threshold out background
			Mat noBg;
			threshold(light, noBg, BG_THRESH * 255, 255, THRESH_TOZERO);// | THRESH_OTSU);
			
			Mat hist;
			calcHist(&saturation, 1, NULL, noBg, hist, 1, &bins, &hr, true, false);
			normalize(hist, hist, 0, sliceH, NORM_MINMAX);
	
			float bw = (float)sliceW / bins;
			
			Scalar mean, stddev;
			meanStdDev(saturation, mean, stddev, noBg); //ignore background pixels
			bool hasLabel = stddev[0] > boundary;

			//draw histogram over image	
			for (int i = 0; i < bins; i++) {
				Scalar col = (double)i / bins < BG_THRESH ? Scalar(255, 100, 100) : Scalar(100, 255, 255);
				line(slice, Point((int)(bw * (i-1)), sliceW - cvRound(hist.at<float>(i-1))),
							Point((int)(bw * (i)), sliceW - cvRound(hist.at<float>(i))),
							col, 1, 8, 0);
			}
		
			//draw text over image
			sprintf(text, "sd = %.3f", stddev[0]);
			putText(slice, text, Point(0, sliceH/2), FONT_HERSHEY_PLAIN, 1.0, textBgCol, 3, CV_AA, false);
			putText(slice, text, Point(0, sliceH/2), FONT_HERSHEY_PLAIN, 1.0, textCol, 1, CV_AA, false);
	
			sprintf(text, "avg = %.3f", mean[0]);
			putText(slice, text, Point(0, sliceH/2 + 20), FONT_HERSHEY_PLAIN, 1.0, textBgCol, 3, CV_AA, false);
			putText(slice, text, Point(0, sliceH/2 + 20), FONT_HERSHEY_PLAIN, 1.0, textCol, 1, CV_AA, false);
		
			if (!hasLabel) {
				sprintf(text, "no label", mean[0]);
				putText(slice, text, Point(0, sliceH/2 + 40), FONT_HERSHEY_PLAIN, 1.0, textBgCol, 3, CV_AA, false);
				putText(slice, text, Point(0, sliceH/2 + 40), FONT_HERSHEY_PLAIN, 1.0, textCol, 1, CV_AA, false);
			}
	
			char windowName[256];
			sprintf(windowName, "hist-%s-%d", imgName.c_str(), si);
			namedWindow(windowName, WINDOW_AUTOSIZE);
			imshow(windowName, slice);
		
			printf("\t\t{\"mean\": %.3f, \"sd\": %.3f, \"label\": %s}%s\n", 
				mean[0], stddev[0], hasLabel? "true" : "false", si < IMAGE_SLICES-1? "," : ""); //json slice data
		}

		waitKey(0);
		destroyAllWindows();
		printf("\t]%s\n", next.data? "," : ""); //json image end
	}

	printf("}\n"); //json end
	return 0;
}
