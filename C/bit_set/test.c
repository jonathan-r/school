#include <stdio.h>
#include <string.h>

#include "bitset.h"

void print_chars_in_set(bitset* this){	
	printf("[");
	int j;
	for (j = 0; j < 0xFF; j++){
		if (bitset_lookup(this, j)){
			printf(" %c", j);
		}
	}
	printf(" ]");
}

int main(){
	const int max = 2048;
	char buffer[max];
	bitset* sets[2];

	printf("Write two lines lines of text representing two sets. Up to %d characters\n", max);

	int i;
	for (i = 0; i < 2; i++){
		sets[i] = bitset_new();
	
		char* c = fgets(buffer, max, stdin);

		if (c){ //c might be NULL in case of a an i/o error
			while (*c) {
				if (*c != '\n') bitset_add(sets[i], *c);
				c++;
			}
		}

		printf("set %d contains: ", i);
		print_chars_in_set(sets[i]);
		printf(".\n");
	}

	bitset* out = bitset_new();

	bitset_union(out, sets[0], sets[1]);
	printf("union: ");
	print_chars_in_set(out);
	printf("\nintersection: ");
	bitset_intersect(out, sets[0], sets[1]);
	print_chars_in_set(out);
	printf("\n");

	bitset_destroy(sets[0]);
	bitset_destroy(sets[1]);
	bitset_destroy(out);

	return 0;
}
