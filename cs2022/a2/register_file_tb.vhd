library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity register_file_tb is
end register_file_tb;
 
architecture behavior of register_file_tb is 
	component register_file is
	port ( write_reg   : in std_logic;
			dest_select : in   std_logic_vector (2 downto 0);
			data_in     : in std_logic_vector (15 downto 0);
			clk         : in   std_logic;
			a_select    : in   std_logic_vector (2 downto 0);
			b_select    : in   std_logic_vector (2 downto 0);
			a_out       : out  std_logic_vector (15 downto 0);
			b_out       : out  std_logic_vector (15 downto 0));
	end component;

	signal write_reg   : std_logic := '0';
	signal dest_select : std_logic_vector(2 downto 0) := (others => '0');
	signal data_in     : std_logic_vector (15 downto 0) := (others => '0');
	signal clk         : std_logic := '0';
	signal a_select    : std_logic_vector (2 downto 0) := (others => '0');
	signal b_select    : std_logic_vector (2 downto 0) := (others => '0');
	signal a_out       : std_logic_vector (15 downto 0) := (others => '0');
	signal b_out       : std_logic_vector (15 downto 0) := (others => '0');

	constant clk_period : time := 50 ns;
	
begin
	uut: register_file port map (write_reg, dest_select, data_in, clk, a_select, b_select, a_out, b_out);

	clk_process : process begin
		clk <= '0';
		wait for clk_period/2;

		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period;

		-- register writing, bus reading and data preservation
		for i in 0 to 7 loop
			data_in <= x"1ba4" xor std_logic_vector(to_unsigned(i, 16));
			dest_select <= std_logic_vector(to_unsigned(i, 3));
			a_select <= std_logic_vector(to_unsigned(i, 3));
			b_select <= std_logic_vector(to_unsigned(i, 3));
			write_reg <= '1'; --write

			wait for clk_period; --transfer

			assert a_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --after the transfer
			assert b_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --after the transfer

			write_reg <= '0'; --don't write
			data_in <= x"cbac";

			wait for clk_period; --transfer

			assert a_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --data unchanged
			assert b_out = (x"1ba4" xor std_logic_vector(to_unsigned(i, 16))); --data unchange;
		end loop;
		
		wait;
	end process;
end;
