#pragma once

#include <vector>

#include "maths/vec.h"
#include "vertex.h"

void generateIcoSphere(int iter, std::vector<vertex>& out);
void generateCylinder(int rad, int longi, vec3 bounds, bool closed, std::vector<vertex>& out);
