from base import BaseCache

class PerfectLruCache(BaseCache):
	"""Cache with perfect least-recently-used replacement policy."""
	def __init__(self, L, N, K, addr_bits):
		BaseCache.__init__(self, L, N, K, addr_bits)

	def on_reset(self):
		self.use_hist = [list(reversed(range(self.K))) for j in range(self.N)]

	def on_hit(self, tag_id, set_id, way_id):
		line = self.use_hist[set_id]
		#bring way_id to the back of the queue
		line.insert(0, line.pop(line.index(way_id)))

	#returns which way to place the data
	def on_miss(self, tag_id, set_id):
		way_id = self.use_hist[set_id].pop()
		self.use_hist[set_id].insert(0, way_id)
		return way_id

	def show_extra_info_column_name(self):
		return 'access history'

	def show_extra_info(self, set_id):
		#usage history for that line
		return ', '.join(map(str, self.use_hist[set_id]))