module Style (Color(..), Style(..), col2hex, testStyle) where

import Text.Printf (printf)
import Data.Bits (shiftR)

data Color = RGBA (Int, Int, Int, Int)
           | RGB (Int, Int, Int)
           | Black
           | White
           | Red
           | Green
           | Blue
             deriving (Show, Read)

byteRange x = (x >= 0) && (x < 256)

col2hex :: Color -> String
col2hex Black = "#000000"
col2hex White = "#FFFFFF"
col2hex Red = "#FF0000"
col2hex Green = "#00FF00"
col2hex Blue = "#0000FF"
col2hex (RGBA (r, g, b, a)) = 
    if all byteRange [r, g, b, a]
        then
            printf "#%02x%02x%02x%02x" r g b a
        else
            error "bad color description"

col2hex (RGB (r, g, b)) =
    if all byteRange [r, g, b]
        then
            printf "#%02x%02x%02xFF" r g b
        else
            error "bad color description"

data Style = Default
           | Fill Color
           | Stroke Color
           | EdgeWidth Float
           | And Style Style
             deriving (Show, Read)

l <+> r = And l r

testStyle = Fill Black <+> Stroke Green