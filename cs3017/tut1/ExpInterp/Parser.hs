module Parser ( parseExp, parseBool ) where

import Syntax

-- parser-specific modules
import Control.Monad.Identity
import Text.Parsec
import Text.Parsec.Text
import Text.Parsec.Expr
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)


-- The lexer
lexer       = P.makeTokenParser haskellDef    
parens      = P.parens lexer
identifier  = P.identifier lexer
integer     = P.integer lexer
reservedOp  = P.reservedOp lexer
true        = P.reserved lexer "true"
false       = P.reserved lexer "false"

-- Build the arithmetic expression parser using Parsec's automation
expSyntax :: Parsec String () Exp
expSyntax = buildExpressionParser expOpTable expTerm
        <?> "expression"

expTerm :: Parsec String () Exp
expTerm = parens expSyntax
      <|> do{ i <- integer; return (CNum i) }
      <?> "simple expression"

expOpTable :: OperatorTable String () Identity Exp
expOpTable = [ [binary "*" Mul AssocLeft, binary "/" Div AssocLeft ]
             , [binary "+" Add AssocLeft, binary "-" Sub AssocLeft ] ]

-- Build the boolean expression parser using Parsec's automation
bexpSyntax :: Parsec String () BExp
bexpSyntax = buildExpressionParser bexpOpTable bexpTerm
        <?> "expression"

bexpTerm :: Parsec String () BExp
bexpTerm = parens bexpSyntax
      <|> do{ i <- true; return CTrue}
      <|> do{ i <- false; return CFalse}
      <?> "simple expression"

bexpOpTable :: OperatorTable String () Identity BExp
bexpOpTable = [[prefix "!" Not] 
              ,[binary "&&" And AssocLeft]
              ,[binary "||" Or AssocLeft] ]

-- Helper functions
binary :: String -> (a -> a -> a) -> Assoc -> Operator String u Identity a
binary name fun assoc = Infix (do{ reservedOp name; return fun }) assoc

prefix :: String -> (a -> a) ->  Operator String u Identity a
prefix name fun       = Prefix (do{ reservedOp name; return fun })


-- Functions to run the parser
parseExp = parse expSyntax 
parseBool = parse bexpSyntax

-- for debugging
run :: Show a => Parsec String () a -> String -> IO ()
run p input = case (parse p "" input) of
                Left err -> do{ putStr "parse error at "
                              ; print err }
                Right x  -> print x


