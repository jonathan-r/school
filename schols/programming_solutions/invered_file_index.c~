#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//I don't remember which year this is from, but the question was, given a text file, to create an index of the words present in the text, like the one you find in books (only in this case, for eve word, not just the important ones), along with every occurence(index) in the text.

//this is a linked list with backward pointers, that's really a stack since the only available operation is append to the end, in O(1) time
#define LOC_NODE_ARRAY_LENGTH 10
typedef struct loc_node_struct{
	struct loc_node_struct* prev;
	int data[LOC_NODE_ARRAY_LENGTH];
} loc_node;

//this is a linked list with forward pointers
typedef struct word_node_struct{
	struct word_node_struct* next;
	char* data;
	int len;
	loc_node* location;
} word_node;


void list_insert(word_node** start, char* word, int len, int location){
	word_node* last = NULL;
	word_node* place = NULL;
	
	if (start){
		for (place = *start; place; place = place->next){
			int cmp = strncasecmp(place->data, word, (len < place->len)? len : place->len);
			if (cmp == 0 && len == place->len){
				//this is the node, insert the new location
				if (place->location){
					for (int i = 0; i < LOC_NODE_ARRAY_LENGTH; i++){
						if (place->location->data[i] == -1){
							place->location->data[i] = location;
							if (i < LOC_NODE_ARRAY_LENGTH - 1) place->location->data[i + 1] = -1; //cap the array
							return;
						}
					}
				}
				
				//either the location list is not allocated at all, or all the slots in the last loc node are full, so insert a new one
				loc_node* ln = malloc(sizeof(loc_node));
				ln->prev = place->location;
				ln->data[0] = location;
				ln->data[1] = -1; //cap it
				place->location = ln;
				return;
			}
			
			else if(cmp > 0){
				//the word is not in the list; this is the place it should go
				break;
			}
			
			last = place;
		}
	}
	
	//insert the new word node in the right place
	word_node* wn = malloc(sizeof(word_node));
	wn->next = place;
	if (last) last->next = wn;
	else *start = wn;
	
	wn->data = word;
	wn->len = len;
	wn->location = malloc(sizeof(loc_node));
	wn->location->prev = NULL;
	wn->location->data[0] = location;
	wn->location->data[1] = -1;
}


void list_delete(word_node* start){
	for (word_node* wn = start; wn;){
		//free all the location data
		for (loc_node* ln = wn->location; ln;){
			loc_node* prev = ln->prev;
			free(ln);
			ln = prev;
		}
	
		word_node* next = wn->next;
		free(wn);
		wn = next;
	}
}

void write_locations(loc_node* node, FILE* to){
	if (node->prev) write_locations(node->prev, to); //recurse to the first node
	for (int i = 0; i < LOC_NODE_ARRAY_LENGTH; i++){
		if (node->data[i] != -1) fprintf(to, "%d, ", node->data[i]);
		else break;
	}
}

void invert_file(char str[], FILE* file){
	char* start = NULL; //of the most recent word, stays NULL when not going over a word
	char* cur = str; //character iterator
	word_node* list = NULL; //linked list of words, always lexicographically sorted
	
	do{
		if (isalpha(*cur)){
			if (!start) start = cur; //this is the first letter after one or more non-letters
		}
		
		else if(start){ //this is a non-letter, and there have been letters before, forming a word
			//insert to list
			int len = (int)(cur - start);
			int location = (int)(start - str);
			list_insert(&list, start, len, location);
			start = NULL;
		}
	} while(*cur++); //include the NULL terminator, so that the last word can be processed
	
	for (word_node* wn = list; wn; wn = wn->next){
		fprintf(file, "\"");
		for (int i = 0; i < wn->len; i++) fprintf(file, "%c", tolower(wn->data[i])); //print it lowercase
		fprintf(file, "\" ");
		
		write_locations(wn->location, file);
		fprintf(file, ";\n");
		
	}
	
	list_delete(list);
}

//**testbed below, not part of the answer

int main(int argc, char** argv){
	if (argc == 2){
		char* filename = argv[1];
		FILE* f = fopen(filename, "rb"); //binary so that fseek gives the correct size
		
		if (f){
			fseek(f, 0, SEEK_END);
			long size = ftell(f);
			rewind(f);
			
			char* str = malloc(size + 1);
			fread(str, size, 1, f);
			fclose(f);
			str[size] = 0; //NULL terminator
			
			invert_file(str, stdout);
			free(str);
		}
		
		else{
			fprintf(stderr, "could not open file\n");
			return 1;
		}
	}
	
	else printf("usage: %s <file to read>\n", argv[0]);
	
	return 0;
}

	
