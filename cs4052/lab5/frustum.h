#pragma once

#include "maths/vec.h"

//http://www.lighthouse3d.com/tutorials/view-frustum-culling/radar-approach-implementation/

template <typename T>
class FrustumT {
public:
	FrustumT(T fov, T aspect, T n, T f) {
		updateCam(fov, aspect, n, f);
	}
	~FrustumT() {}

	void updateCam(T fov, T aspect, T nn, T ff) {
		n = nn;
		f = ff;
		asp = aspect;
		T rads = (fov * (T)(3.141)) / (2 * 180);
		t = tan(rads);
		
		sphY = T(1) / cos(rads);
		sphX = T(1) / cos(atan(t * asp));
	}

	void update(const vec3T<T>& e, const vec3T<T>& t, const vec3T<T>& u) {
		eye = e;
		z = (t - e).normal();
		x = (z ^ u).normal();
		y = x ^ z;
	}

	bool test(const vec3T<T>& p, T& pcz) {
		vec3T<T> v = p - eye;
		pcz = v * z; //distance along view direction

		if (pcz > f or pcz < n) 
			return false;
		
		T pcy = v * y;
		T aux = pcz * t;
		if (pcy > aux or pcy < -aux)
			return false;

		T pcx = v * x;
		aux *= asp;
		if (pcx > aux or pcx < -aux)
			return false;

		return true;
	}
	
	bool testSphere(const vec3T<T>& p, T rad) {
		vec3T<T> v = p - eye;
		//bool ret = true;
		
		T pcz = v * z;
		if (pcz > f + rad or pcz < n - rad) return false;
		//if (pcz > f - rad or pcz < n + rad) ret = true;
		
		T pcy = v * y;
		T d = sphY * rad;
		pcz *= t;
		if (pcy > pcz + d or pcy < -pcz - d) return false;
		//if (pcy > pcz - d or pcy < -pcz + d) ret = true;

		T pcx = v * x;
		pcz *= asp;
		d = sphX * rad;
		if (pcx > pcz + d or pcx < -pcz - d) return false;

		return true;
	}

private:
	vec3T<T> eye;
	vec3T<T> x, y, z;
	T asp, t, n, f;
	T sphX, sphY;
};

typedef FrustumT<float> Frustum;