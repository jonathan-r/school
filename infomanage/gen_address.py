from random import randint

f = open('/usr/share/dict/web2', 'r')
names = [x[:len(x)-1] for x in f.readlines() if len(x) > 2 and x[0].isupper()] #exclude newline

def name():
	return names.pop(randint(0, len(names) -1))

print str(randint(1, 340)) + ' ' + name() + ' ' + ('Avenue', 'Road', 'Street')[randint(0, 2)] + ', ' + name()

