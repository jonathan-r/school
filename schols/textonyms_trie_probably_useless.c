#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#define NODE_DATA     (1 << 0) //the node contains a word beginning or middle, and so it has children
#define NODE_WORD_END (1 << 1) //a word ends in this node (but it can also have children)

typedef struct node_struct{
	char flags;
	struct node_struct* children;
} node;

int isletter(char c){
	return isalpha(c);
}

char case_insensitive(char c){
	if (c < 'a') return c + ('a' - 'A');
	else return c;
}

//str must be at least one character long
//returns 1 if it didn't exist already, 0 if it did
int add_str(node* root, char* str){
	node* n = &(root->children[*str]);

	if (*(str + 1)){ //beginning or middle
		if ((n->flags & NODE_DATA) == 0){
			assert(!n->children); //not sure about this one

			n->flags |= NODE_DATA;
			n->children = malloc(256 * sizeof(node));
			memset(n->children, 0, 256 * sizeof(node));
		}

		return add_str(n, str + 1);
	}

	else{ //last character
		if (n->flags & NODE_WORD_END) return 0; //the word is there already
		else{
			n->flags |= NODE_WORD_END;
			return 1;
		}
	}
	
	return 0; //shouldn't happen
}

void tree_free(node* root){
	for (int i = 0; i < 256; i++){
		node* n = &(root->children[i]);
		if (n->flags & NODE_DATA){
			tree_free(n);
		}
	}

	free(root->children);
}

void print_textonyms(node* root, char* digits, char* last_digits, char* word, int depth){
	const int lengths[] = {3, 3, 3, 3, 3, 4, 3, 4}; //how many letters are on each button, starting with the 2 key

	if (root->flags & NODE_DATA){
		char letter = 'a';

		for (int k = 0; k < 8; k++){ //for every letter bearing key
			digits[depth] = '2' + k; //mark the key in the string

			for (int l = 0; l < lengths[k]; l++){
				node* n = root->children[letter + l];
				if (n->flags & NODE_WORD_END){
					words++; //a word ends on this digit
				}
			}

			letter += lengths[k]; //this will eventually become 'w'+1
				
		for (int c = 'a'; c <= 'z'; c++){
			if (root->children[c]->flags & NODE_WORD_END) words++;



void print_words(node* root, char* word, int depth){
	if (root->flags & NODE_DATA){
		for (char i = 'a'; i <= 'z'; i++){
			node* n = &(root->children[i]);
			if (n->flags & (NODE_DATA | NODE_WORD_END)){
				word[depth] = i;
				if (n->flags & NODE_WORD_END){
					word[depth + 1] = '\0';
					printf("%s\n", word);
				}

				print_words(n, word, depth + 1);
			}
		}
	}
}


int main(int argc, char** argv){
	if (argc == 2){
		//initialize the trie
		node* root = malloc(sizeof(node));
		root->flags = NODE_DATA; //this node must have children, and no words can end in it
		root->children = malloc(256 * sizeof(node));
		memset(root->children, 0, 256 * sizeof(node));

		FILE* f = fopen(argv[1], "rb");

		if (f){
			fseek(f, 0, SEEK_END);
			long end = ftell(f);

			fseek(f, 0, SEEK_SET);
			long size = end - ftell(f);

			char* wordBuf = malloc(size + 1); //the entire could be a single word, and it needs a NULL terminator
			char* w = wordBuf; //iterator

			int longest = 0;
			char c;
			do {
				c = fgetc(f);
				if (isletter(c)){
					*w++ = case_insensitive(c);
				}

				else{
					if (w != wordBuf){
						//there is at least one character in the buffer

						int length = (int)(w - wordBuf);
						if (length > longest) longest = length;

						*w = '\0'; //NULL terminate the string
						//printf("found word '%s'\n", wordBuf); 
						add_str(root, wordBuf);

						//reset the iterator
						w = wordBuf;
					}
				}
			} while(c != EOF);

			fclose(f);
			free(wordBuf);

			if (longest > 0){ //that is, if there is at least one word
				char* digit_str = malloc(longest + 1);
				char* last_digit_str = malloc(longest + 1);
				print_textonyms(root, digit_str, last_digit_str, wordBuf); //wordbuf is reused
				//print_words(root, digit_str, 0);

				free(digit_str);
			}

			tree_free(root);
			free(root);
		}

		else{
			printf("could not open file\n");
			return -1;
		}

	}

	else{
		printf("usage: %s <file>\n", argv[0]);
	}

	return 0;
}
