#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "literal_lexer.h"


//correspond 
enum symbol {
	INPUT_SIGN = 0, //+, -
	INPUT_LOW_DIGIT, // 0 - 7
	INPUT_HIGH_DIGIT, // 8 - 9
	INPUT_HEX_EXCEPT_B, // a, c - f, A, C - F
	INPUT_B, //b, B
	INPUT_H, //h, H
	INPUT_END //any other character, including the null terminator
};

//maps semantically idnetical characters to a single symbollic value
int transliterate(char c){
	switch(c){
		case '+': case '-': return INPUT_SIGN;
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': return INPUT_LOW_DIGIT;
		case '8': case '9': return INPUT_HIGH_DIGIT;
		case 'a': case 'A': case 'c': case 'C': case 'd': case 'D': case 'e': case 'E': case 'f': case 'F': return INPUT_HEX_EXCEPT_B;
		case 'b': case 'B': return INPUT_B;
		case 'h': case 'H': return INPUT_H;
		default: return INPUT_END; //any character not belonging to the input alphabet
	}
}

//maps characters [0-9] and [a-fA-F] to a corresponding numerical binary value
//returns -1 on other input
int character_value(char c){
	if (c >= '0' && c <= '9') return (int)(c - '0');
	c &= ~(0x20); //make uppercase
	if (c >= 'A' && c <= 'F') return (int)(c - 'A') + 10;
	return -1; //undefined
}

//constants in the transition table
enum transition_type {
	//un-named states 0 through 2
	SIGN_STATE = 3,
	//un-named states 4 through 7

	//valid exit transitions
	DEC_VALID_EXIT = 8,
	OCT_VALID_EXIT,
	HEX_VALID_EXIT,

	//many transition errors
	TR_ERR_NO_DIGITS,
	TR_ERR_MISSING_H,
	TR_ERR_SIGNED_HEX,
	TR_ERR_EXTRA_SIGN,
	TR_ERR_EXTRA_B,
	TR_ERR_IN_AFTER_END,

	//parser error
	ERROR_OVERFLOW
};

//columns correspond to the symbol enum
int transition_table[8][7] = {
//     sign                0-7                  8,9                 [a-f]\b               bB                   hH                   end
/*0*/  SIGN_STATE,         1,                   2,                   6,                   6,                   TR_ERR_NO_DIGITS,    TR_ERR_NO_DIGITS,
/*1*/  TR_ERR_EXTRA_SIGN,  1,                   2,                   6,                   5,                   7,                   DEC_VALID_EXIT,
/*2*/  TR_ERR_EXTRA_SIGN,  2,                   2,                   6,                   6,                   7,                   DEC_VALID_EXIT,
/*3*/  TR_ERR_EXTRA_SIGN,  4,                   4,                   TR_ERR_SIGNED_HEX,   TR_ERR_EXTRA_B,      TR_ERR_SIGNED_HEX,   TR_ERR_NO_DIGITS,
/*4*/  TR_ERR_EXTRA_SIGN,  4,                   4,                   TR_ERR_SIGNED_HEX,   TR_ERR_EXTRA_B,      TR_ERR_SIGNED_HEX,   DEC_VALID_EXIT,
/*5*/  TR_ERR_EXTRA_SIGN,  6,                   6,                   6,                   6,                   7,                   OCT_VALID_EXIT,
/*6*/  TR_ERR_EXTRA_SIGN,  6,                   6,                   6,                   6,                   7,                   TR_ERR_MISSING_H,
/*7*/  TR_ERR_EXTRA_SIGN,  TR_ERR_IN_AFTER_END, TR_ERR_IN_AFTER_END, TR_ERR_IN_AFTER_END, TR_ERR_IN_AFTER_END, TR_ERR_IN_AFTER_END, HEX_VALID_EXIT
};

typedef struct{
	const char* highest;
	int max_digits;
	int radix;
} base_info;

//index corresponding with the numeral_type enum
const base_info bases[3] = {
	{"2147483648", 10, 10},
	{"37777777777", 11, 8},
	{"FFFFFFFF", 8, 16}
};

char* error_list[] = {
	[ERROR_OVERFLOW] = "Numerical overflow",

	//transition errors
	[TR_ERR_NO_DIGITS] = "Digit expected",
	[TR_ERR_MISSING_H] = "Hexadecimal end marker expected after hexadecimal digits",
	[TR_ERR_SIGNED_HEX] = "Hexadecimal literal cannot be signed",
	[TR_ERR_EXTRA_SIGN] = "Extra sign",
	[TR_ERR_EXTRA_B] = "Extra octal end marker",
	[TR_ERR_IN_AFTER_END] = "Input after end marker"
};

//see literal_lexer.h for description
int parse(char* literal, uint32_t* value, const char** error_str, int* error_position){
	int state = 0;
	int decimal_sign = 1; //positive; implied if sign absent
	int num_digits = 0; //of any kind
	char* first_digit = literal; //first non-sign, non-zero character

	//default values
	*value = 0;
	if (error_str) *error_str = NULL;
	if (error_position) *error_position = 0;

	for (char* cp = literal; state < DEC_VALID_EXIT; cp++){
		int input = transliterate(*cp);

		//transition
		state = transition_table[state][input];

		if (state >= TR_ERR_NO_DIGITS){
			//reached an error state
			if (error_position){
				*error_position = (int)(cp - literal);
			}
		}

		else if (state == SIGN_STATE){ //sign
			if (*cp == '-'){
				decimal_sign = -1;
			}
			first_digit++; //skip the sign
		}

		else if (input == INPUT_END){
			//finished parsing, ending with a valid exit transition
			if (state > DEC_VALID_EXIT){
				//B and H terminators are not a digit
				num_digits--;
			}
		}

		else if ((*cp == '0') && (num_digits == 0)){
			//skip leading zeros
			first_digit++;
		}

		else {
			//generic non-error transition to a digit
			num_digits++;
		}
	}

	if ((state >= DEC_VALID_EXIT) && (state <= HEX_VALID_EXIT)){
		int base_id = state - DEC_VALID_EXIT;
		
		const base_info* base = &bases[base_id];
		if (num_digits > base->max_digits){
			state = ERROR_OVERFLOW;
		}

		//don't do the checking for hexadecimal, since there is no way to overflow if there are fewer than 9 digits
		else if ((num_digits == base->max_digits) && (state != HEX_VALID_EXIT)){
			int cmp = strncmp(first_digit, base->highest, num_digits);

			//in the case decimal, the negative range is 1 larger than the positive range
			if ((cmp > 0) || ((cmp == 0) && (decimal_sign == 1) && (state == DEC_VALID_EXIT))){
				state = ERROR_OVERFLOW;
			}
		}
			
		if (state != ERROR_OVERFLOW){
			//finally parse the number
			for (char* cp = first_digit; num_digits > 0; num_digits--, cp++){
				*value = (*value) * base->radix + (decimal_sign) * character_value(*cp);
			}

			return base_id;
		}
	}

	//reaching this point means an error occurred
	if (error_str){
		*error_str = error_list[state];
	}

	return NUMERAL_ERROR;
}
