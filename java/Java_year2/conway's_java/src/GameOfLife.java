import java.lang.Thread;

public class GameOfLife extends Thread{
	private byte[] live;
	private byte[] neighbours;
	private byte[] lastNeighbours;

	private int w;
	private int h;
	private int delay;
	
	private boolean running;

	public GameOfLife(int width, int height, int delayMilli, byte[] initialState) {
		w = width;
		h = height;
		delay = delayMilli;
		
		live = new byte[w * h];
		lastNeighbours = new byte[w * h];
		System.arraycopy(initialState, 0, live, 0, w * h); //copy the initial state
	
		//initialize neighbour count values
		for (int y = 0; y < h; y++) {
			int up = (y == 0) ? h - 1 : y - 1;
			int down = (y == h - 1) ? 0 : y + 1;

			for (int x = 0; x < w; x++) {
				int left = (x == 0) ? w - 1 : x - 1;
				int right = (x == w - 1) ? 0 : x + 1;

				if (live[x + y * w] == 1) {
					lastNeighbours[left + up * w]++;
					lastNeighbours[left + y * w]++;
					lastNeighbours[left + down * w]++;

					lastNeighbours[x + up * w]++;
					//center
					lastNeighbours[x + down * w]++;

					lastNeighbours[right + up * w]++;
					lastNeighbours[right + y * w]++;
					lastNeighbours[right + down * w]++;
				}
			}
		}
		
		neighbours = new byte[w * h];
		System.arraycopy(lastNeighbours, 0, neighbours, 0, w * h);
	}

	//updates the board one step
	private void update() {
		for (int y = 0; y < h; y++) {
			int up = (y == 0) ? h - 1 : y - 1;
			int down = (y == h - 1) ? 0 : y + 1;

			for (int x = 0; x < w; x++) {
				int left = (x == 0) ? w - 1 : x - 1;
				int right = (x == w - 1) ? 0 : x + 1;

				byte around = lastNeighbours[x + y * w]; //amount of living neighbours around this cell

				int add = 0;
				if (live[x + y * w] == 1 && (around < 2 || around > 3)){
					//wither
					add = -1;
					live[x + y * w] = 0;
				}
				
				else if (live[x + y * w] == 0 && around == 3){
					//grow
					add = 1;
					live[x + y * w] = 1;
				}

				if (add != 0) {
					neighbours[left + up * w] += add;
					neighbours[left + y * w] += add;
					neighbours[left + down * w] += add;

					neighbours[x + up * w] += add;
					//center
					neighbours[x + down * w] += add;

					neighbours[right + up * w] += add;
					neighbours[right + y * w] += add;
					neighbours[right + down * w] += add;
				}
			}
		}
		
		System.arraycopy(neighbours, 0, lastNeighbours, 0, w * h);
	}

	//clears the screen and display the board
	private void print() {
		System.out.println("\033[2J"); //clear
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				System.out.print((live[x + y * w] == 1)? 'X' : ' ');
			}
			
			System.out.println();
		}
		
	}
	
	//update the and display the board again and again, until stopped
	public void run(){
		running = true;
		while (running) {
			print();
			update();
			try {
				Thread.sleep(delay);
			}

			catch (InterruptedException e) {
				//interrupted by another thread
				//running = false;
			}
		}
	}
	
	public synchronized void die(){
		running = false;
	}

	public static void main(String[] args) {
		GameOfLife game;
		
		//default values
		int width = 10;
		int height = 10;
		int delay = 1000;
		
		if (args.length == 1){
			//delay only
			delay = Integer.parseInt(args[0]);
		}

		else if (args.length == 2 || args.length == 3) {
			//width and height and maybe delay
			width = Integer.parseInt(args[0]);
			height = Integer.parseInt(args[1]);
			if (args.length == 3) delay = Integer.parseInt(args[2]);
		}
		
		else if (args.length != 0) {
			//confused
			width = -1;
			System.out.println("usage: java GameOfLife [width <height>] [update delay]");
		}

		if (width > 5 && height > 5 && delay > 0) {
			byte[] initial = new byte[width * height];

			//make a glider
			System.arraycopy(new byte[] { 0, 1, 0 }, 0, initial, width * 0, 3);
			System.arraycopy(new byte[] { 0, 0, 1 }, 0, initial, width * 1, 3);
			System.arraycopy(new byte[] { 1, 1, 1 }, 0, initial, width * 2, 3);

			game = new GameOfLife(width, height, delay, initial);
			game.start();
			
			//pause this thread until a key is pressed
			try{
				System.in.read();
			}
			catch(java.io.IOException e){}
			
			//stop the simulation
			game.die();
		}
	}
}
