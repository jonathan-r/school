#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "maths/vec.h"
#include "maths/util.h"

#include "texture.h"

//public domain implementation
//https://github.com/josephg/noisejs/blob/master/perlin.js

//perlin noise improvements:
//https://www.cs.utah.edu/~aek/research/noise.pdf

//to build the permutation tables, and bruteforce seraching for a good rank - one that causes high differences between neighbouring values:
//http://webhome.cs.uvic.ca/~ruskey/Publications/RankPerm/MyrvoldRuskey.pdf

//template <class T> //TODO: generalize to vec2/3T<T>
class NoiseGen {
public:
	NoiseGen(uint32_t seed) {
		init(seed);
	}

	float noise2Distorted(const vec2& X, float dist) {
		//blender source source/blender/blenlib/intern/noise.c end of mg_VLNoise; distorted-domain noise
		const float q = 13.5f;
		float x = noise2(vec2(X.x + q, X.y + q)) * dist;
		float y = noise2(X) * dist;
		return noise2(vec2(X.x + x, X.y + y));
	}

	float noise2(const vec2& X) {
		ivec2 I = X.floor(); //correct handling of negative cases
		vec2 F = X - I;

		float ns[4];
		ns[0] = G[Px[(I.x + 0) & 0xFF] ^ Py[(I.y + 0) & 0xFF]].xy * vec2(F.x - 0, F.y - 0);
		ns[1] = G[Px[(I.x + 0) & 0xFF] ^ Py[(I.y + 1) & 0xFF]].xy * vec2(F.x - 0, F.y - 1);
		ns[2] = G[Px[(I.x + 1) & 0xFF] ^ Py[(I.y + 0) & 0xFF]].xy * vec2(F.x - 1, F.y - 0);
		ns[3] = G[Px[(I.x + 1) & 0xFF] ^ Py[(I.y + 1) & 0xFF]].xy * vec2(F.x - 1, F.y - 1);

		float u = fade(F.x);
		float v = fade(F.y);

		return lerp(
			lerp(ns[0], ns[2], u),
			lerp(ns[1], ns[3], u),
		v);
	}

	float noise3(const vec3& X) {
		ivec3 I = X.floor(); //correct handling of negative cases
		vec3 F = X - I;

		float ns[8];
		for (int k = 0; k < 2; k++) {
			for (int j = 0; j < 2; j++) {
				for (int i = 0; i < 2; i++) {
					int hash =  Px[(I.x + i) & 0xFF] ^ //modulo table length
								Py[(I.y + j) & 0xFF] ^ 
								Pz[(I.z + k) & 0xFF];
					ns[(i << 2) | (j << 1) | k] = G[hash] * vec3(F.x - i, F.y - j, F.z - k);
				}
			}
		}

		float u = fade(F.x);
		float v = fade(F.y);
		float w = fade(F.z);

		return lerp(
			lerp(
				lerp(ns[0], ns[4], u),
				lerp(ns[1], ns[5], u), w),
			lerp(
				lerp(ns[2], ns[6], u),
				lerp(ns[3], ns[7], u), w),
		v);
	}

	void init(uint32_t seed) {
		//TODO: better random number generation, say mersenne twister

		// Park and Miller's psuedo-random number generator.
		// http://farside.ph.utexas.edu/teaching/329/lectures/node107.html
		lastRand = seed;

		for (int i = 0; i < 256; i++) {
			vec3 v;
			do {
				v = randVec3();
			} while (v.sqrLength() < 0.01f);
			G[i] = v.normal();
		}
	}

	uint32_t rand() {
		const uint32_t A = 16807;
		const uint32_t M = 0xFFFFFFFF;
		const uint32_t Q = M / A;
		const uint32_t R = M % A;

		lastRand = A * (lastRand % Q) - R * (lastRand / Q);
		//if (lastRand < 0) lastRand += M;
		return lastRand;
	}

	float randUnit() {
		int32_t r = rand();
		return ((float)r / 0x7FFFFFFF);
	}

	vec3 randVec3() {
		return vec3(randUnit(), randUnit(), randUnit());
	}

	Texture* makeNoiseTexture(int w, int h, int octaves, float persistence, vec2 size = vec2(1.0f, 1.0f), int chan = 3, uint8_t** saveData = nullptr) {
		uint8_t* data = new uint8_t[w * h * chan];
		
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				for (int ci = 0; ci < chan; ci++) {
					/*float s = 0.0f;
					float d = persistence;

					for (int oi = 0; oi < octaves; oi++) {
						int mul = 1 << oi;
						vec3 p((i * size.x * mul) / w, (j * size.y * mul) / h, (float)ci / chan);
						
						s += noise3(p) / d;
						d *= d;
					}*/

					float s = noise2Distorted(vec2((i * size.x) / w, (j * size.y) / h), 5.0f);

					uint8_t col = (int)(s * 127) + 127; //bring to [0, 1]^3 range
					data[(j * w + i) * chan + ci] = col;
				}
			}
		}

		Texture* tex = new Texture(chan, GL_UNSIGNED_BYTE, w, h, data);
		if (saveData) *saveData = data;
		else delete [] data;
		return tex;
	}

	void test() {
		int buckets[256];
		for (int i = 0; i < 256; i++) buckets[i] = 0;
		for (int i = 0; i < 1000000; i++) {
			float f = randUnit() * 127;
			int b = 127 + f;
			assert(b >= 0 and b < 256);
			buckets[b]++;
		}

		printf("buckets: [ ");
		for (int i = 0; i < 256; i++) printf("%d ", buckets[i]);
		printf("]\n");
	}

private:
	uint32_t lastRand;
	vec3 G[256];

	//made using unrank1 from [Myrvold, Ruskey 2001]
	static const int Px[256];
	static const int Py[256];
	static const int Pz[256];
};