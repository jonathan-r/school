#version 330

uniform float lightFraction;

in vec3 normal;
in vec3 color;
out vec4 FragColor;

void main() {
	float lum = dot(normal, vec3(0.5, 0.5, 0.5));
	vec3 final = mix(color, lum * color, lightFraction); //lerp
	FragColor = vec4(final, 1.0);
}
