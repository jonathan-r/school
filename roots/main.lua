require("conf")
require("map")

function love.conf(c)
	c.window.title = "Experimental"
	c.window.resizable = false
	c.window.width = WW
	c.window.height = HH
	c.window.msaa = 4
end

local map = nil

function love.load(arg)
	if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
	love.graphics.setBackgroundColor(0, 0, 0)
	love.graphics.setLineStyle("smooth")
	
	map = Map:new()
end

function love.keyreleased(key)
	
end

function love.mousepressed(x, y, button, touch)
  map:click(x, y)
end

function love.update(dt)
  
end

function love.draw()
	map:draw(love.mouse.isDown(1))
end

