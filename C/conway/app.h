#if !defined(_WIN32) && (defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__)))
#define UNIX_LIKE
#endif 

#ifdef UNIX_LIKE
#include <sys/time.h>
#include <unistd.h>
#else
#include <windows.h>
#endif

#include "texture.h"
#include "fbo.h"
#include "shader.h"
#include "text_render.h"

class App{
public:
	App(){
	}
	
	~App(){
	}
	
	void run(){
		init();
		glutReshapeFunc(reshapeCallback);
		glutDisplayFunc(displayCallback);
		glutKeyboardFunc(keyboardCallback);
		glutMouseFunc(mouseButtonCallback);
		glutMotionFunc(mouseMotionCallback);
		glutPassiveMotionFunc(mouseMotionCallback);
		glutSpecialFunc(specialCallback);
		glutIdleFunc(idleCallback);
		
		glutMainLoop();
	}
	
	static App app;
	
private:
	int width;
	int height;
	
	Framebuffer* fbo;
	Texture* renderTex[2];
	Texture* grid;
	Shader* defaultShader;
	Shader* simShader;
	Shader* magShader;
	
	uint8_t* blankBuffer;

	int inputTexId; //which texture index (0 or 1) is the input (the other being the output)

	int resetDelayAfterResize; //in ms
	int simDelay; //in ms
	int simDelayAfterInput; //in ms
	
	//absolute time in ms
	int steppedAt;
	int resizedAt;
	int drawingAt;
	
	bool simRunning;
	bool drawSizeLabel;
	bool magnifierMode;
	bool paused;
	
	//user input
	bool mousePressed[3];
	bool mousePressedEaten[3];
	int mouse[2]; //position
	bool keyControl;
	bool keyAlt;
	bool keyShift;
	
	void init(){
		width = 800;
		height = 600;
		
		//initialize window and make an OpenGL context (includes system specific stuff that would take thousands of lines of code otherwise)
		glutInitWindowSize(width, height);
		glutInitDisplayMode(GLUT_RGB);
		glutCreateWindow("Conway's shaded game of life");
		printf("created a glut window\n");

		//Let GLEW chase those extension function pointers
		GLenum err = glewInit(); 
		if (err != GLEW_OK){
			fprintf(stderr, "could not init glew\n");
			exit(1);
		}
		printf("initialized glew\n");
		
		//load shaders
		defaultShader = new Shader("default");
		simShader = new Shader("simulation");
		magShader = new Shader("magnifier");
		
		//make two blank textures and bind the first one as the output of the framebuffer object
		blankBuffer = (uint8_t*) malloc(width * height * 3);
		memset(blankBuffer, 0, width * height);
		renderTex[0] = new Texture(GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, width, height, blankBuffer);
		renderTex[1] = new Texture(GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, width, height, NULL);
		fbo = new Framebuffer(width, height, renderTex[1]);
		inputTexId = 0; //this is the texture that is being read in the simulation fragment buffer

		simRunning = false;		
		drawSizeLabel = true;
		magnifierMode = false;
		paused = false;
		
		
		
		resetDelayAfterResize = 1500;
		simDelay = 1500;
		simDelayAfterInput = 1500;
		
		steppedAt = resizedAt = drawingAt = 0;
		
		//create grid texture
		const int s = 256;
		uint8_t* gridData = (uint8_t*) malloc(256 * 256 * 3);
		for (int y = 0; y < s; y++){
			for (int x = 0; x < s; x++){
				uint8_t r = (((x ^ y) & 0x9) == 0)? 255 : 0;
				uint8_t g = (((x ^ y) & 0x11) == 0)? 255 : 0;
				uint8_t b = (((x ^ y) & 0x13) == 0)? 255 : 0;
				gridData[(x + y * s) * 3 + 0] = r;
				gridData[(x + y * s) * 3 + 1] = g;
				gridData[(x + y * s) * 3 + 2] = b;
			}
		}
		
		grid = new Texture(GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, s, s, gridData);
	
		//load font texture
		TextRender::init();
		printf("created font texture\n");
		
		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClearDepth(0.0f);
		
		mousePressed[0] = mousePressed[1] = mousePressed[2] = false;
		mousePressedEaten[0] = mousePressedEaten[1] = mousePressedEaten[2] = false;
		keyControl = keyAlt = keyShift = false;
		
		mouse[0] = mouse[1] = 0;
	}
	
	//texture and renderbuffer must be resized
	void resetSim(){
		free(blankBuffer);
		blankBuffer = (uint8_t*) malloc(width * height * 3);
		memset(blankBuffer, 0, width * height * 3);
		for (int i = 0; i < std::min(width, height); i++){
			blankBuffer[(i + i * width) * 3] = 255;
			blankBuffer[(width - i - 1 + i * width) * 3] = 255;
		}
			
		renderTex[inputTexId]->updateData(GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, width, height, blankBuffer);
		renderTex[inputTexId ^ 1]->updateData(GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, width, height, NULL);
		fbo->resizeDepthBuffer(width, height);
	}

	void reshape(int w, int h){
		width = w;
		height = h;
		glViewport(0, 0, w, h);

		//set up orthographic perspective
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, w, h, 0, -1.0, 1.0);

		glMatrixMode(GL_MODELVIEW);
		
		//show the size label and disable the simulation for a second until the user has finished resizing
		drawSizeLabel = true;
		simRunning = false;
		resizedAt = timeMilli();
		
		printf("reshaped\n");
	}

	void display(){
		//logic
		int time = timeMilli();
		
		//process input
		if (mousePressed[2] && !mousePressedEaten[2]){
			//switch viewing mode
			magnifierMode = !magnifierMode;
			mousePressedEaten[2] = true;
		}
		
		if (mousePressed[0]){
			//drawing
			simRunning = false;
			printf("TODO: drawing\n");
			
			if (!keyControl){
				//add
			}
			else{
				//remove
			}
			
			drawingAt = time;
		}
		
		if (drawSizeLabel and time - resizedAt > resetDelayAfterResize){
			//enough time has passed since the window has been resized
			drawSizeLabel = false;
			resetSim();
			simRunning = true;
		}
		
		if (!drawSizeLabel and time - drawingAt > simDelayAfterInput){
			//enough time has passed since cell drawing
			simRunning = true;
		}
	
	
		Framebuffer::bindDefault();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		
		if (simRunning and !paused){
			//simulation step
			if (time - steppedAt >= simDelay){
				simShader->use();
				glUniform1i(simShader->getUniformLocation("tex"), 0);
				glUniform2f(simShader->getUniformLocation("tex_size"), width, height);
				
				//bind the texture that was being drawn up until now
				fbo->bind();
				inputTexId ^= 1;
				fbo->attachTexture(renderTex[inputTexId]);
				
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				
				//bind input texture
				glActiveTexture(GL_TEXTURE0);
				renderTex[inputTexId]->bind();
	
				//draw it
				glBegin(GL_QUADS);
				glTexCoord2f(0.0f, 0.0f); glVertex2i(0, 0);          
				glTexCoord2f(1.0f, 0.0f); glVertex2i(width, 0);      
				glTexCoord2f(1.0f, 1.0f); glVertex2i(width, height); 
				glTexCoord2f(0.0f, 1.0f); glVertex2i(0, height);   
				glEnd();
	
				

				Framebuffer::bindDefault();
				steppedAt = time;
			}
		}
		
		//draw cells
		if (magnifierMode){
			magShader->use();
			glUniform1i(magShader->getUniformLocation("tex"), 0);
			glUniform2f(magShader->getUniformLocation("tex_size"), width, height);
			glUniform2f(magShader->getUniformLocation("mouse"), mouse[0], height - mouse[1]);
		}
		
		else{
			defaultShader->use();
			glUniform1i(defaultShader->getUniformLocation("tex"), 0);
		}
		
		glActiveTexture(GL_TEXTURE0);
		renderTex[inputTexId ^ 1]->bind();
	
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f, 1.0f); glVertex2i(0, 0);
			glTexCoord2f(1.0f, 1.0f); glVertex2i(width, 0);
			glTexCoord2f(1.0f, 0.0f); glVertex2i(width, height);
			glTexCoord2f(0.0f, 0.0f); glVertex2i(0, height);
		glEnd();
		
		char label[80];

		if (drawSizeLabel){
			glPushMatrix();
			glClearColor(0.2f, 0.1f, 0.5f, 1.0f);
			
			glColor4f(1.0f, 1.0f, 0.5f, 1.0f);
			glTranslatef(width / 2.0f, height / 2.0f, 0.0f);
			
			sprintf(label, "%d x %d", width, height);
			TextRender::render(label);
			glPopMatrix();
		}
		
		else{
			glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		}
		
		glColor4f(1.0f, 1.0f, 0.5f, 1.0f);
		
		//mouse position or mode
		glTranslatef(10, 5, 0);
		if (simRunning) sprintf(label, "running");
		else sprintf(label, "{%d, %d}", mouse[0], mouse[1]);
		TextRender::render(label);
		
		//speed
		glTranslatef(150, 0, 0);
		sprintf(label, "delay: %d ms", simDelay);
		TextRender::render(label);
		
		//view mode
		glTranslatef(150, 0, 0);
		sprintf(label, (magnifierMode)? "magnify" : "normal");
		
		TextRender::render(label);

		glutSwapBuffers();
		GLerror::list("app_display_update");
	}

	void keyboard(uint8_t key, int x, int y){
		int keyMods = glutGetModifiers();
		keyControl = keyMods & GLUT_ACTIVE_CTRL;
		keyAlt = keyMods & GLUT_ACTIVE_ALT;
		keyShift = keyMods & GLUT_ACTIVE_SHIFT;
	
		switch(key){
			case 27: //escape
			case 'q': 
				glutLeaveMainLoop();
				break;
	
			case ' ':
				paused = !paused;
				break;
		}
	}

	void mouseButton(int key, int state, int x, int y){
		switch(key){
			case GLUT_LEFT_BUTTON: 
				mousePressed[0] = (state == GLUT_DOWN); 
				mousePressedEaten[0] = false;
				break;
				
			case GLUT_MIDDLE_BUTTON: 
				mousePressed[1] = (state == GLUT_DOWN);
				mousePressedEaten[1] = false;
				break;
				
			case GLUT_RIGHT_BUTTON: 
				mousePressed[2] = (state == GLUT_DOWN);
				mousePressedEaten[2] = false;
				break;
		}
	}
	
	//this is used for both active (with button held) and passive mouse motion
	void mouseMotion(int x, int y){
		mouse[0] = x;
		mouse[1] = y;
		
		//if (
	}

	void special(int key, int x, int y){
	}

	void idle(){
		//cap framerate to 60
		unsigned int start = timeMilli();
		glutPostRedisplay();
		unsigned int time = timeMilli() - start;
		
		if (time < 17){
			#ifdef UNIX_LIKE
			usleep
			#else
			Sleep
			#endif
			((17 - time) * 1000);
		}
			
		
	}
	
	static unsigned int timeMilli() {
		#ifdef UNIX_LIKE
		struct timeval t;
		gettimeofday(&t, NULL);
		return (int) (t.tv_sec * 1000 + t.tv_usec / 1000);
		
		#else //windoze
		return (int) GetTickCount();
		#endif
	}
	
	
	//sadly freeglut requires plain function pointers (with no class context) for callbacks, so it's a bit messier in c++
	static void reshapeCallback(int w, int h){
		app.reshape(w, h);
	}

	static void displayCallback(){
		app.display();
	}

	static void keyboardCallback(uint8_t key, int x, int y){
		app.keyboard(key, x, y);
	}

	static void mouseButtonCallback(int key, int state, int x, int y){
		app.mouseButton(key, state, x, y);
	}
	
	static void mouseMotionCallback(int x, int y){
		app.mouseMotion(x, y);
	}

	static void specialCallback(int key, int x, int y){
		app.special(key, x, y);
	}

	static void idleCallback(){
		app.idle();
	}
};

