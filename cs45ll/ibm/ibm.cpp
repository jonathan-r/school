#include<vector>
#include<string>
#include<iostream>
#include<iomanip>
#include<cmath>
#include<algorithm>
#include<cstdint>

using namespace std;

// want to represents vocab items by integers because then various tables 
// need by the IBM model and EM training can just be represented as 2-dim 
// tables indexed by integers

// the following #defines, defs of VS, VO, S, O, and create_vocab_and_data()
// are set up to deal with the specific case of the two pair corpus
// (la maison/the house)
// (la fleur/the flower)

// S VOCAB
#define LA 0
#define MAISON 1
#define FLEUR 2

// O VOCAB
#define THE 0
#define HOUSE 1
#define FLOWER 2

#define VS_SIZE 3
#define VO_SIZE 3
#define D_SIZE 2

vector<string> VS(VS_SIZE); // S vocab: VS[x] gives Src word coded by x 
vector<string> VO(VO_SIZE); // O vocab: VO[x] gives Obs word coded by x

vector<vector<int> > S(D_SIZE); // all S sequences; in this case 2
vector<vector<int> > O(D_SIZE); // all O sequences; in this case 2

// sets S[0] and S[1] to be the int vecs representing the S sequences
// sets O[0] and O[1] to be the int vecs representing the O sequences
void create_vocab_and_data(); 

// functions which use VS and VO to 'decode' the int vecs representing the 
// Src and Obs sequences
void show_pair(int d);
void show_O(int d); 
void show_S(int d);

void print_table(const char* name, vector<vector<double> >& data);
int64_t hcf(int64_t a, int64_t b);
pair<int, int> make_fract(double x, int decimals);
string make_fract_str(double x, int decimals);

int main() {
	create_vocab_and_data();

	// (o_j, s_i) -> #(o, s)
	vector<vector<double> > counts(VO_SIZE);

	// tr(o_i | s_j)
	vector<vector<double> > tr(VO_SIZE);

	//initialize tr and counts
	for (int j = 0; j < VO_SIZE; j++) {
		counts[j].resize(VS_SIZE);
		tr[j].resize(VS_SIZE);
		for (int i = 0; i < VS_SIZE; i++) {
			tr[j][i] = 1.0 / (VS_SIZE);
		}
	}

	int iterations = 0;
	for (; iterations < 1000000; iterations++) { //hard limit
		//expectation step

		//set #(o, s) to 0 for all o and s
		for (int j = 0; j < VO_SIZE; j++) {
			for (int i = 0; i < VS_SIZE; i++) {
				counts[j][i] = 0.0;
			}
		}

		//for each pair (o, s) in d
		for (int di = 0; di < D_SIZE; di++) {
			vector<int>& o = O[di];
			vector<int>& s = S[di];

			//for each j in 0 to lo-1
			for (int j = 0; j < o.size(); j++) {
				int ow = o[j]; //value of o_j

				//compute (sum over i' of tr(o_j | s_i')) once
				double den = 0.0;
				for (int i = 0; i < s.size(); i++) {
					int sw = s[i]; //value of s_i
					den += tr[ow][sw];
				}

				//#(o_j, s_i) += P((j, i) | o, s)
				for (int i = 0; i < s.size(); i++) {
					int sw = s[i]; //value of s_i

					//#(o_j, s_i) += tr(o_j, s_i) / (sum over i' of tr(o_j | s_i'))
					counts[ow][sw] += tr[ow][sw] / den;
				}
			}
		}

		//maximization step
		double sumDiff = 0.0; //sum of absolute differences across tr
		for (int i = 0; i < VS_SIZE; i++) {
			//compute (sum over o' of #(o', s)) once
			double den = 0.0;
			for (int j = 0; j < VO_SIZE; j++) {
				den += counts[j][i];
			}

			//compute next values of tr as #(o,s) / (sum over o' of #(o', s))
			for (int j = 0; j < VO_SIZE; j++) {
				double old = tr[j][i];
				tr[j][i] = counts[j][i] / den;
				sumDiff += fabs(old - tr[j][i]);
			}
		}

		cout << "iteration " << iterations << ", tr changes sum to " << sumDiff << endl;
		print_table("#(o,s)", counts);
		cout << endl;
		print_table("tr(o|s)", tr);
		cout << endl;

		if (sumDiff < 0.01) break;
	}
	
	cout << "converged in " << iterations << " iterations" << endl;
	//print_table("tr(o|s)", tr);

	return 0;
}

void print_table(const char* name, vector<vector<double> >& data) {
	const int fieldWidth = 8;
	const int contentWidth = fieldWidth - 1;

	//header
	cout << setw(fieldWidth) << right << name;
	for (int i = 0; i < VS_SIZE; i++) {
		cout << setw(fieldWidth) << right << VS[i].substr(0, contentWidth);
	}
	cout << '\n';

	for (int j = 0; j < VS_SIZE; j++) {
		cout << setw(fieldWidth) << right << VO[j].substr(0, contentWidth);
		for (int i = 0; i < VO_SIZE; i++) {
			cout << setw(fieldWidth) << right << fixed << setprecision(4) << data[j][i];
		}
		cout << '\n';
	}
}

void create_vocab_and_data() {

	VS[LA] = "la";
	VS[MAISON] = "maison";
	VS[FLEUR] = "fleur";

	VO[THE] = "the";
	VO[HOUSE] = "house";
	VO[FLOWER] = "flower";

	cout << "source vocab\n";
	for(int vi=0; vi < VS.size(); vi++) {
		cout << VS[vi] << " ";
	}
	cout << endl;
	cout << "observed vocab\n";
	for(int vj=0; vj < VO.size(); vj++) {
		cout << VO[vj] << " ";
	}
	cout << endl;

	// make S[0] be {LA,MAISON}
	//		O[0] be {THE,HOUSE}
	S[0].resize(2);	 O[0].resize(2);
	S[0] = {LA,MAISON};
	O[0] = {THE,HOUSE};

	// make S[1] be {LA,FLEUR}
	//		O[1] be {THE,FLOWER}
	S[1].resize(2);	 O[1].resize(2);
	S[1] = {LA,FLEUR};
	O[1] = {THE,FLOWER};

	for(int d = 0; d < S.size(); d++) {
		show_pair(d);
	}
}

void show_O(int d) {
	for(int i=0; i < O[d].size(); i++) {
		cout << VO[O[d][i]] << " ";
	}
}

void show_S(int d) {
	for(int i=0; i < S[d].size(); i++) {
		cout << VS[S[d][i]] << " ";
	}
}

void show_pair(int d) {
	cout << "S" << d << ": ";
	show_S(d);
	cout << endl;
	cout << "O" << d << ": ";
	show_O(d);
	cout << endl;
}

