module Parser ( parseExp, parseBool, parseCom ) where

import Syntax

-- parser-specific modules
import Control.Monad.Identity
import Text.Parsec
import Text.Parsec.Text
import Text.Parsec.Expr
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (javaStyle)


-- The lexer
langLex = javaStyle{ P.opStart = oneOf ":=*+-/|&!;{}<>"
                    , P.opLetter = oneOf "=><"
                    , P.reservedOpNames = ["+", "-", "*", "/", "&", "|", "!", "=", ":=", ";", "{", "}", "<", ">", ">=", "<="]
                    , P.reservedNames = ["true", "false", "skip", "if", "then", "else", "for", "while", "do" ]
                    }

lexer       = P.makeTokenParser langLex
parens      = P.parens lexer
identifier  = P.identifier lexer
integer     = P.integer lexer
reservedOp  = P.reservedOp lexer
whiteSpace  = P.whiteSpace lexer
true        = P.reserved lexer "true"
false       = P.reserved lexer "false"
eq          = P.reserved lexer "="
--gt          = P.reserved lexer ">"
lt          = P.reserved lexer "<"
--ge          = P.reserved lexer ">="
le          = P.reserved lexer "<="
whilestart  = P.reserved lexer "while"
forstart    = P.reserved lexer "for"
asgn        = P.reserved lexer ":="
ifstart     = P.reserved lexer "if"
ifthen      = P.reserved lexer "then"
ifelse      = P.reserved lexer "else"
skip        = P.reserved lexer "skip"
whiledo     = P.reserved lexer "do"
fordo       = P.reserved lexer "do"
braceOpen   = P.reserved lexer "{"
braceClose  = P.reserved lexer "}"
semicolon   = P.reserved lexer ";"

-- Build the arithmetic expression parser using Parsec's automation
expSyntax :: Parsec String () EArith
expSyntax = buildExpressionParser expOpTable expTerm
        <?> "arithmetic expression"

expTerm :: Parsec String () EArith
expTerm = parens expSyntax
      <|> do{ i <- integer; return (CNum i) }
      <|> do{ i <- identifier; return (Var i)}
      <?> "simple arithmetic expression"

expOpTable :: OperatorTable String () Identity EArith
expOpTable = [ [binary "*" Mul AssocLeft, binary "/" Div AssocLeft ]
             , [binary "+" Add AssocLeft, binary "-" Sub AssocLeft ] ]

-- Build the boolean expression parser using Parsec's automation
bexpSyntax :: Parsec String () EBool
bexpSyntax = buildExpressionParser bexpOpTable bexpTerm
        <?> "boolean expression"

bexpTerm :: Parsec String () EBool
bexpTerm = parens bexpSyntax
      <|> (true  >> return (CBool True))
      <|> (false >> return (CBool False))
      <|> do e1 <- expSyntax
             op <- comparisonOp
             e2 <- expSyntax
             if (op == "=")
               then return (Eq e1 e2) 
               else return (Lt e1 e2) 
      <?> "simple boolean expression"

bexpOpTable :: OperatorTable String () Identity EBool
bexpOpTable = [[prefix "!" Not] 
              ,[binary "&" And AssocLeft]
              ,[binary "|" Or AssocLeft] ]

comparisonOp :: Parsec String () String
comparisonOp = do eq
                  return "="
           <|> do lt
                  return "<"


-- Build the command parser
comSyntax :: Parsec String () Com
comSyntax = do whiteSpace
               c <- comSequence
               eof
               return c
        
comSequence :: Parsec String () Com
comSequence = do lst <- P.semiSep1 lexer comBasic
                 return (foldr1 Seq lst)
       <?> "command sequence"

comSingle :: Parsec String () Com
comSingle = comBasic
        <|> do braceOpen 
               c <- comSequence
               braceClose
               return c
       <?> "single command"

comBasic :: Parsec String () Com
comBasic = do l <- identifier
              asgn
              e <- expSyntax
              return (Asgn l e)
       <|> do skip
              return Skip
       <|> do ifstart
              b <- bexpSyntax
              ifthen
              c1 <- comSingle
              ifelse
              c2 <- comSingle
              return (IfThenElse b c1 c2)
       <|> do forstart
              l <- identifier
              le
              n <- integer
              fordo
              c <- comSingle
              return (For l n c)
       <|> do whilestart
              b <- bexpSyntax
              whiledo
              c <- comSingle
              return (While b c)
       <?> "basic command"

-- Helper functions
binary :: String -> (a -> a -> a) -> Assoc -> Operator String u Identity a
binary name fun assoc = Infix (do{ reservedOp name; return fun }) assoc

prefix :: String -> (a -> a) ->  Operator String u Identity a
prefix name fun       = Prefix (do{ reservedOp name; return fun })


-- Functions to run the parser
parseExp = parse expSyntax 
parseBool = parse bexpSyntax
parseCom = parse comSyntax

-- for debugging
run :: Show a => Parsec String () a -> String -> IO ()
run p input = case (parse p "" input) of
                Left err -> do{ putStr "parse error at "
                              ; print err }
                Right x  -> print x


