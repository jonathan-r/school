import java.awt.Graphics;
import java.awt.FontMetrics;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import java.lang.Thread;

import java.util.concurrent.Semaphore;
import java.util.HashMap;
import java.util.Iterator;


public class Visualizer implements MazeChangeListener{
	private Semaphore semaphore = null;
	private PaintWindow window = null;

	public void cellStateChangeEvent(MazeCell cell){
		try { semaphore.acquire(); } //block until the window paints the change
		catch (InterruptedException e) { e.printStackTrace(); }
		
		assert (window.changedCell == null) : "cell change that wasn't seen by the visualizer";
		window.changedCell = (SquareCell)cell;
	}

	public void stateChangeEvent(){
		try { semaphore.acquire(); } //block until the window paints the change
		catch (InterruptedException e) { e.printStackTrace(); }

		assert (window.changedCell == null) : "cell change that wasn't seen by the visualizer";
		assert (window.mazeChanged == false) : "maze change that wasn't seen by the visualizer";
		window.mazeChanged = true;
	}

	private class PaintWindow extends JFrame implements Runnable{
		//set by cellStateChangeEvent and stateChangeEvent
		public SquareCell changedCell = null;
		public boolean mazeChanged = false;

		//target maze
		private SquareCellMaze maze = null;
		private SquareCell lastSolutionCell = null;

		//logic
		private long updateDelay;
		private boolean running = true;
		private boolean firstDraw = true;
		private boolean solving = true; //whether the solver is running
		private int ticks = 0; //number of updates since something happened
		private String endStatus = null; //resolution

		//colors
		private Color mazeBgCol = new Color(50, 50, 50);
		private Color mazeContourCol = new Color(50, 40, 40);
		private Color mazeLinkCol = new Color(50, 170, 50);
		private Color mazeSolutionPathCol = new Color(170, 50, 50);
		private HashMap<MazeCell.CellState, Color> stateToCol = new HashMap<MazeCell.CellState, Color>();

		//polygons
		private int[][] trianglesX = new int[][]{
			new int[]{0, 1, 0}, new int[]{1, 2, 2}, new int[]{2, 2, 1}, new int[]{0, 1, 0}, //outer
			new int[]{1, 1, 0}, new int[]{1, 2, 1}, new int[]{1, 2, 1}, new int[]{0, 1, 1}  //inner
		};
		private int[][] trianglesY = new int[][]{
			new int[]{0, 0, 1}, new int[]{0, 0, 1}, new int[]{1, 2, 2}, new int[]{1, 2, 2}, //outer
			new int[]{0, 1, 1}, new int[]{0, 1, 1}, new int[]{1, 1, 2}, new int[]{1, 1, 2}  //inner
		};
		private boolean[] drawTriangle = new boolean[8];

		private int[] polygonX = new int[4];
		private int[] polygonY = new int[4];

		//proportions
		private final int gridMargin = 20;
		private final int quartSize = 10;
		private final int halfSize = quartSize * 2;
		private final int cellSize = halfSize * 2;
		private final int diamondSize = (quartSize * 2) / 3;
		private final int donutSize = (quartSize * 5) / 2;

		public PaintWindow(Semaphore sem, String solverType, SquareCellMaze target, String mazeName, long delay){
			super(solverType + " solving " + mazeName);
			updateDelay = delay;
			maze = target;
			semaphore = sem;

			setResizable(false);
			setSize(gridMargin * 2 + cellSize * target.getWidth(), gridMargin * 2 + cellSize * target.getHeight() + 1);
			
			setVisible(true);
			toFront();

			stateToCol.put(MazeCell.CellState.notVisited, new Color(0x009999));//Color(150, 150, 250));
			stateToCol.put(MazeCell.CellState.visited, new Color(0xFF7400));//Color(150, 250, 150));
			stateToCol.put(MazeCell.CellState.visitInProgress, new Color(0x008500));//Color(250, 250, 50));
			stateToCol.put(MazeCell.CellState.solutionPath, new Color(0xFF4040));//Color(250, 150, 150));
			
			addWindowListener(new WindowAdapter() {
		        @Override
		        public void windowClosing(WindowEvent e) {
		            System.exit(-1);
		        }
		    });
		}

		private void paintWallSubcell(int x, int y, byte type, Graphics g){
			final boolean t = true;
			final boolean f = false;

			final boolean[] innerNW = new boolean[]{f,  f, f, f, f,  f,  f, t, f, t,  f,  t, t, t, t,  f};
			final boolean[] innerNE = new boolean[]{f,  f, f, f, f,  f,  t, t, f, f,  f,  t, t, t, t,  f};
			final boolean[] innerSE = new boolean[]{f,  f, f, t, f,  f,  t, t, f, f,  f,  t, f, t, t,  f};
			final boolean[] innerSW = new boolean[]{f,  f, f, t, f,  f,  f, t, f, t,  f,  t, f, t, t,  f};

			drawTriangle[0] = (type & 0x8) != 0;
			drawTriangle[1] = (type & 0x4) != 0;
			drawTriangle[2] = (type & 0x2) != 0;
			drawTriangle[3] = (type & 0x1) != 0;
			drawTriangle[4] = innerNW[type];
			drawTriangle[5] = innerNE[type];
			drawTriangle[6] = innerSE[type];
			drawTriangle[7] = innerSW[type];

			if (type == 0x5){ //backslash
				paintWallSubcell(x, y, (byte)0x4, g);
				paintWallSubcell(x, y, (byte)0x1, g);
			}

			else if (type == 0xA){ //slash
				paintWallSubcell(x, y, (byte)0x8, g);
				paintWallSubcell(x, y, (byte)0x2, g);
			}

			else if (type != 0x0 && type != 0xF){
				for (int i = 0; i < 8; i++){
					if (drawTriangle[i]){
						polygonX[0] = x + trianglesX[i][0] * quartSize;
						polygonY[0] = y + trianglesY[i][0] * quartSize;

						polygonX[1] = x + trianglesX[i][1] * quartSize;
						polygonY[1] = y + trianglesY[i][1] * quartSize;

						polygonX[2] = x + trianglesX[i][2] * quartSize;
						polygonY[2] = y + trianglesY[i][2] * quartSize;

						g.fillPolygon(polygonX, polygonY, 3);
					}
				}
			}

			//else don't fill the contour
		}

		private void paintCell(SquareCell cell, Graphics g){
			int x = gridMargin + cell.getCol() * cellSize + halfSize;
			int y = gridMargin + cell.getRow() * cellSize + halfSize;
			
			g.setColor(stateToCol.get(cell.getState()));
			
			if (cell.isDonut()){
				g.fillArc(x - donutSize / 2, y - donutSize / 2, donutSize, donutSize, 0, 360);
			}
			
			else{
				if (cell.isStart()){
					//draw a star
					g.drawLine(x - diamondSize, y - diamondSize, x + diamondSize, y + diamondSize);
					g.drawLine(x + diamondSize, y - diamondSize, x - diamondSize, y + diamondSize);
				}
				
				polygonX[0] = x;
				polygonX[1] = x + diamondSize;
				polygonX[2] = x;
				polygonX[3] = x - diamondSize;
	
				polygonY[0] = y - diamondSize;
				polygonY[1] = y;
				polygonY[2] = y + diamondSize;
				polygonY[3] = y;
	
				g.fillPolygon(polygonX, polygonY, 4);
			}
		}

		private void paintLink(SquareCell a, SquareCell b, int width, Color c, Graphics g){
			int ax = gridMargin + a.getCol() * cellSize + halfSize;
			int ay = gridMargin + a.getRow() * cellSize + halfSize;
			int bx = gridMargin + b.getCol() * cellSize + halfSize;
			int by = gridMargin + b.getRow() * cellSize + halfSize;
					
			g.setColor(c);
			for (int i = 0; i < width; i++){
				g.drawLine(ax - i, ay, bx - i, by);
				g.drawLine(ax + i, ay, bx + i, by);
				g.drawLine(ax, ay - i, bx, by - i);
				g.drawLine(ax, ay + i, bx, by + i);
			}
		}

		@Override
		public synchronized void paint(Graphics g){
			if (solving){
				if (firstDraw){
					g.setColor(mazeBgCol);
					g.clearRect(gridMargin, gridMargin, cellSize * maze.getWidth(), cellSize * maze.getHeight());
	
					//draw the entire maze once
					for (int r = 0; r < maze.getHeight(); r++){
						int cy = gridMargin + r * cellSize;
	
						for (int c = 0; c < maze.getWidth(); c++){
							SquareCell cell = maze.getCell(r, c);
							int cx = gridMargin + c * cellSize;
	
							//marching squares
							//Split the cell in a 2x2 subgrid of 3x3 points, where the center point is the cell itself
							//Infer whether each of the 9 points is a wall
							//(the center is never a wall, so exclude it)
							byte nw, n, ne, e, se, s, sw, w; //1 for wall, 0 for space
	
							//sides
							n = cell.isWall(Direction.north)? (byte)1 : (byte)0;
							e = cell.isWall(Direction.east)? (byte)1 : (byte)0;
							s = cell.isWall(Direction.south)? (byte)1 : (byte)0;
							w = cell.isWall(Direction.west)? (byte)1 : (byte)0;
	
							//corners 
							nw = ne = se = sw = 0; //reset
							if (r > 0 && c > 0){
								SquareCell nwCell = maze.getCell(r - 1, c - 1);
								if (nwCell.isWall(Direction.south) || nwCell.isWall(Direction.east)) nw = 1;
							}
							else nw = 1; //edge of the maze
	
							if (r > 0 && c < maze.getWidth() - 1){
								SquareCell neCell = maze.getCell(r - 1, c + 1);
								if (neCell.isWall(Direction.south) || neCell.isWall(Direction.west)) ne = 1;
							}
							else ne = 1; //edge of the maze
	
							if (r < maze.getHeight() - 1 && c < maze.getWidth() - 1){
								SquareCell seCell = maze.getCell(r + 1, c + 1);
								if (seCell.isWall(Direction.north) || seCell.isWall(Direction.west)) se = 1;
							}
							else se = 1; //edge of the maze
	
							if (r < maze.getHeight() - 1 && c > 0){
								SquareCell swCell = maze.getCell(r + 1, c - 1);
								if (swCell.isWall(Direction.east) || swCell.isWall(Direction.north)) sw = 1;
							}
							else sw = 1; //edge of the maze
	
							nw |= (byte)(w | n);
							ne |= (byte)(n | e);
							se |= (byte)(e | s);
							sw |= (byte)(s | w);
	
							//paint each of the 4 subcells according to the 9 points
							g.setColor(mazeContourCol);
							paintWallSubcell(cx,            cy,            (byte)((nw << 3) | (n  << 2) | (0  << 1) | w), g);
							paintWallSubcell(cx + halfSize, cy,            (byte)((n  << 3) | (ne << 2) | (e  << 1) | 0), g);
							paintWallSubcell(cx + halfSize, cy + halfSize, (byte)((0  << 3) | (e  << 2) | (se << 1) | s), g);
							paintWallSubcell(cx,            cy + halfSize, (byte)((w  << 3) | (0  << 2) | (s  << 1) | sw), g);
							
							paintCell(cell, g);
						}
					}
	
					firstDraw = false;
				}
	
				if (mazeChanged){
					System.out.println("maze changed");
					mazeChanged = false;
				}
	
				else if (changedCell != null){
					if (changedCell.getState() == MazeCell.CellState.visitInProgress){
						Iterator<SquareCell> it = maze.getNeighbors(changedCell);
						while (it.hasNext()){
							SquareCell n = it.next();
							paintLink(changedCell, n, 1, stateToCol.get(changedCell.getState()), g);
							paintCell(n, g); //drawing the link messed it up
						}
					}
	
					else if (changedCell.getState() == MazeCell.CellState.solutionPath){
						if (lastSolutionCell != null){
							paintLink(changedCell, lastSolutionCell, diamondSize, mazeSolutionPathCol, g);
							paintCell(lastSolutionCell, g); //drawing the link messed it up

						}
						lastSolutionCell = changedCell;
						
						/* There really needs to be a way for a maze runner to tell a listener if/when it solved the maze
						 if((maze.getDonut().getState() == MazeCell.CellState.solutionPath) && (maze.getStart().getState() == MazeCell.CellState.solutionPath)){
							endStatus = "Solved";
							solving = false;
							ticks = 0;
						}*/
					}
	
					paintCell(changedCell, g);
					changedCell = null;
				}
				
				else{
					ticks++;
					if (ticks == 1000 / updateDelay + 1){
						endStatus = "Solver is finished (or stuck)";
						solving = false;
						ticks = 0;
					}
				}
			}
			
			else{
				FontMetrics m = g.getFontMetrics();
				g.drawString(endStatus, getWidth() / 2 - m.stringWidth(endStatus) / 2, gridMargin * 2 / 3);
				
				/*ticks++;
				if (ticks == 5000 / updateDelay + 1){
					running = false;
				}*/
			}

			semaphore.release(); //allow cellStateChangeEvent and stateChangeEvent to do one more step
		}

		@Override
		public void run(){
			try {Thread.sleep(100);} //let the window manager bring up the window
			catch (InterruptedException e) { e.printStackTrace(); }

			while (running) {
				try {
					Thread.sleep(updateDelay);
					repaint();
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			try {Thread.sleep(2000);} //let the user see the end string
			catch (InterruptedException e) { e.printStackTrace(); }
			System.exit(0);
		}
	}

	public Visualizer(String solverType, SquareCellMaze targetMaze, String targetName, long delay) {
		semaphore = new Semaphore(1, true);
		window = new PaintWindow(semaphore, solverType, targetMaze, targetName, delay);
		Thread t = new Thread(window);
		t.start();
		//t.join();
	}
}
