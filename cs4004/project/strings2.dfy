predicate isPrefixPred(pre:string, str:string)
{
	(|pre| <= |str|) && pre == str[..|pre|]
}

predicate isNotPrefixPred(pre:string, str:string)
{
    (|pre| > |str|) || pre != str[..|pre|]
}

// Sanity check: Dafny should be able to automatically prove the following lemma
lemma PrefixNegationLemma(pre:string, str:string)
	ensures  isPrefixPred(pre,str) <==> !isNotPrefixPred(pre,str)
	ensures !isPrefixPred(pre,str) <==>  isNotPrefixPred(pre,str)
{}

predicate isSubstringPred(sub:string, str:string)
{
    exists i, j :: i >= 0 && j == i+|sub| && j < |str| && sub == str[i..j]
}

predicate isNotSubstringPred(sub:string, str:string)
{
	forall i, j :: (i >= 0 && j == i+|sub| && j < |str|) ==> (sub != str[i .. j])
}

// Sanity check: Dafny should be able to automatically prove the following lemma
lemma SubstringNegationLemma(sub:string, str:string)
	ensures  isSubstringPred(sub,str) <==> !isNotSubstringPred(sub,str)
	ensures !isSubstringPred(sub,str) <==>  isNotSubstringPred(sub,str)
{}

predicate haveCommonKSubstringPred(k:nat, str1:string, str2:string)
{
	exists i0, j0, i1, j1 :: 
		i0 >= 0 && i1 == i0 + k && i1 < |str1| &&
		j0 >= 0 && j1 == j0 + k && j1 < |str2| &&
		str1[i0 .. i1] == str2[j0 .. j1]
}

predicate haveNotCommonKSubstringPred(k:nat, str1:string, str2:string)
{
	forall i0, j0, i1, j1 :: 
		i0 >= 0 && i1 == i0 + k && i1 < |str1| &&
		j0 >= 0 && j1 == j0 + k && j1 < |str2| 
		==>
		str1[i0 .. i1] != str2[j0 .. j1]
}

// Sanity check: Dafny should be able to automatically prove the following lemma
lemma commonKSubstringLemma(k:nat, str1:string, str2:string)
	ensures  haveCommonKSubstringPred(k,str1,str2) <==> !haveNotCommonKSubstringPred(k,str1,str2)
	ensures !haveCommonKSubstringPred(k,str1,str2) <==> haveNotCommonKSubstringPred(k,str1,str2)
{}
