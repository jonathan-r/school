// -------------------------------------------------------------------------
/**
 *	This class contains the methods of Doubly Linked Stack.
 *
 *	@author  Jonathan Roșca
 *	@version 2014.01.19
 */


/**
 * Class Stack: implements a *generic* Doubly Linked Stack.
 * @param <T> This is a type parameter. T is used as a class name in the
 * definition of this class.
 *
 * When creating a new DoublyLinkedList, T should be instantiated with an
 * actual class name that extends the class Comparable.
 * Such classes include String and Integer.
 *
 * For example to create a new DoublyLinkedList class containing String data: 
 *		DoublyLinkedList<String> myStringList = new DoublyLinkedList<String>();
 */
class Stack<T extends Comparable<T>> extends DoublyLinkedList<T> {
	/**
	 * Constructor
	 * @return DoublyLinkedList
	 */
	public Stack(){
		super();
	}

	
	/**
	 * Tests if the stack is empty
	 * @return true if list is empty, and false otherwise
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	DoublyLinkedList.isEmpty() has a worst-case asymptotic runtime cost of O(1).
	 */
	@Override
	public boolean isEmpty(){
		return super.isEmpty();
	}

	/**
	 * Inserts an element at the top of the stack
	 * @param data : The new data of class T that needs to be added to the list
	 * @return none
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	DoublyLinkedList.push() has a worst-case asymptotic runtime cost of O(1).
	 */
	public void push(T data){
		super.insertFirst(data);
	}

	/**
	 * Delete the the top of the stack if it exists.
	 * @return the data of the delete element, or null if the stack was empty.
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	Both DoublyLinkedList.push() and DoublyLinkedList.deleteFirst have a worst-case asymptotic runtime cost of O(1).
	 */
	public T pop(){
		T data = super.getFirst();
		super.deleteFirst();
		return data;
	}
}