#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>

int main(int argc, char** argv) {
	if (argc != 3) {
		printf("usage: %s <server> <msg>\n", argv[0]);
		return 0;
	}

	int port = 80;

	//TODO: explicit port extraction
	char *afterProto = NULL;
	char *hostAddr = NULL;
	char *hostUrl = NULL;
	
	//try to parse protocol
	const char* httpsProto = "https://";
	const char* httpProto = "http://";
	if (!strncasecmp(argv[1], httpsProto, strlen(httpsProto))) {
		port = 443;
		afterProto = argv[1] + strlen(httpsProto);
	}
	else if (!strncasecmp(argv[1], httpProto, strlen(httpProto))) {
		port = 80;
		afterProto = argv[1] + strlen(httpProto);
	}
	else afterProto = argv[1];
	
	char *firstSlash = strchr(afterProto, '/');
	if (firstSlash) {
		//there's a base url
		int len = (int)(firstSlash - afterProto);
		hostAddr = malloc(len + 1);
		hostAddr[len] = 0;
		memcpy(hostAddr, afterProto, len);
		hostUrl = firstSlash;
	}
	else {
		hostAddr = afterProto;
		hostUrl = NULL;
	}

	printf("host %s, url %s, port: %d\n", hostAddr, hostUrl, port);
	
	int sock = socket(PF_INET, SOCK_STREAM, 0);
	struct sockaddr_in servAddr;
	struct hostent *hostEnd;
	
	if (!(hostEnd = gethostbyname(hostAddr))) {
		fprintf(stderr, "could not resolve server address; perror: %s\n", strerror(errno));
		return 1;
	}

	if (!hostEnd->h_addr_list[0]) {
		fprintf(stderr, "could not resolve server address; perror: %s\n", strerror(errno));
		return 1;
	}	

	memset(&servAddr, 0, sizeof(struct sockaddr_in));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port);
	memcpy(&servAddr.sin_addr, hostEnd->h_addr_list[0], sizeof(struct in_addr));

	if (connect(sock, (const struct sockaddr*) &servAddr, sizeof(struct sockaddr_in)) < 0) {
		fprintf(stderr, "connection failed\n");
		return 1;
	}

	int msgLen = strlen(argv[2]);
	int escLen = msgLen * 3; //TODO: better way would be to count the number of special chars
	char* escaped = malloc(escLen+1);
	
	//escape msg
	char *o = escaped;
	for (char *c = argv[2]; *c; c++) {
		switch(*c) {
			case ' ': *o++ = '%'; *o++ = '2'; *o++ = '0'; break;
			case '!': *o++ = '%'; *o++ = '2'; *o++ = '1'; break;
			case '"': *o++ = '%'; *o++ = '2'; *o++ = '2'; break;
			case '#': *o++ = '%'; *o++ = '2'; *o++ = '3'; break;
			case '$': *o++ = '%'; *o++ = '2'; *o++ = '4'; break;
			case '%': *o++ = '%'; *o++ = '2'; *o++ = '5'; break;
			case '&': *o++ = '%'; *o++ = '2'; *o++ = '6'; break;
			case '\'': *o++ = '%'; *o++ = '2'; *o++ = '7'; break;
			case '(': *o++ = '%'; *o++ = '2'; *o++ = '8'; break;
			case ')': *o++ = '%'; *o++ = '2'; *o++ = '9'; break;
			case '*': *o++ = '%'; *o++ = '2'; *o++ = 'A'; break;
			case '+': *o++ = '%'; *o++ = '2'; *o++ = 'B'; break;
			case ',': *o++ = '%'; *o++ = '2'; *o++ = 'C'; break;
			case '-': *o++ = '%'; *o++ = '2'; *o++ = 'D'; break;
			case '.': *o++ = '%'; *o++ = '2'; *o++ = 'E'; break;
			case '/': *o++ = '%'; *o++ = '2'; *o++ = 'F'; break;
			case ':': *o++ = '%'; *o++ = '3'; *o++ = 'A'; break;
			case ';': *o++ = '%'; *o++ = '3'; *o++ = 'B'; break;
			case '<': *o++ = '%'; *o++ = '3'; *o++ = 'C'; break;
			case '=': *o++ = '%'; *o++ = '3'; *o++ = 'D'; break;
			case '>': *o++ = '%'; *o++ = '3'; *o++ = 'E'; break;
			case '?': *o++ = '%'; *o++ = '3'; *o++ = 'F'; break;
			case '@': *o++ = '%'; *o++ = '4'; *o++ = '0'; break;
			case '[': *o++ = '%'; *o++ = '5'; *o++ = 'B'; break;
			case '\\': *o++ = '%'; *o++ = '5'; *o++ = 'C'; break;
			case ']': *o++ = '%'; *o++ = '5'; *o++ = 'D'; break;
			case '^': *o++ = '%'; *o++ = '5'; *o++ = 'E'; break;
			case '_': *o++ = '%'; *o++ = '5'; *o++ = 'F'; break;
			case '`': *o++ = '%'; *o++ = '6'; *o++ = '0'; break;
			case '{': *o++ = '%'; *o++ = '7'; *o++ = 'B'; break;
			case '|': *o++ = '%'; *o++ = '7'; *o++ = 'C'; break;
			case '}': *o++ = '%'; *o++ = '7'; *o++ = 'D'; break;
			case '~': *o++ = '%'; *o++ = '7'; *o++ = 'E'; break;
			default: *o++ = *c; break;
		}
	}
	//*o++ = 0; //null terminate

	const char* preMsg = "GET ";
	const char* urlEnd = "?message=";
	const char* postMsg = " HTTP/1.1\r\n"; //TODO: omit host?
	const char* hostMsg = "Host: ";
	const char* endMsg = "\r\n\r\n";

	int sz, wrote, got;
	#define WR(s, buf) { sz = s; \
		if ((wrote = write(sock, buf, sz)) != sz) \
			fprintf(stderr, "could not write data, only wrote %d bytes; perror: %s\n", wrote, strerror(errno)); }

	WR(strlen(preMsg), preMsg);
	if (hostUrl) WR(strlen(hostUrl), hostUrl)
	else WR(1, "/")
	WR(strlen(urlEnd), urlEnd)
	WR((int)(o - escaped), escaped);
	WR(strlen(postMsg), postMsg);
	WR(strlen(hostMsg), hostMsg);
	WR(strlen(hostAddr), hostAddr);
	WR(strlen(endMsg), endMsg);

	printf("sent mesage\n");

	const int recvSize = 127;
	char recvBuf[recvSize+1]; recvBuf[recvSize] = 0;
	while ((got = read(sock, recvBuf, recvSize))) {
		if (got < 0) {
			fprintf(stderr, "could not read data, perror: %s\n", strerror(errno));
		}
		else {
			recvBuf[got] = 0;
			fputs(recvBuf, stdout);
			fflush(stdout);
		}
	}
	fputc('\n', stdout);
	close(sock);
	
	return 0;
}
