#libraries
from socketserver import ThreadingTCPServer

#internal
from base import ServerHandlerBase, Connection
import config

dirAddress = ('localhost', config.DIR_PORT)

#one of these is spawned for each client connection
class GatewayHandler(ServerHandlerBase):
	def setup(self):
		super(GatewayHandler, self).setup()
		self.cmdMap["OPEN_FILE"] = self.openRequest

		#connect to all the services
		dirSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		dirSock.connect(dirAddress) #can raise timeout or interrupt

		self.dirConn = Connection(dirSock)

	def openRequest(self, firstarg):
		pass

if __name__ == '__main__':
	import sys

	port = config.GATEWAY_PORT

	if len(sys.argv) == 2:
		port = int(sys.argv[1])

	elif len(sys.argv) != 1:
		print("usage: python3 %s [own port]" % sys.argv[0])
		sys.exit(0)

	#starts an endless loop blocking on socket.accept
	#spawns a thread running GatewayHandler for each connection
	ThreadingTCPServer(('0.0.0.0', port), GatewayHandler).serve_forever()
	