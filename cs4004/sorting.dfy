
predicate sorted(a: seq<int>) {
    forall i:nat, j:nat :: (0 <= i < j < |a| && (j == i+1)) ==> a[i] <= a[j]
}

function count (a: seq<int>, val: int): int {
    if |a| == 0 then 0 else
        if a[0] == val then 1 + count (a[1..], val)
        else 0 + count (a[1..], val)
}

predicate permutation(input: seq<int>, out: seq<int>) {
    |input| == |out| &&
    forall v:int :: count (input, v) == count (out, v) 
}

predicate sortingMethod(input: seq<int>, out: seq<int>) {
    permutation(input, out) && sorted(out)
}

method sort(input: seq<int>) returns (out: seq<int>)
    ensures sorted (out)
    ensures permutation (input, out)
    decreases |input|
{
    if |input| == 0 || |input| == 1 {
        out := input;
    }
    else if |input| == 2 {
        if input[0] > input[1] {
            out := [input[1], input[0]];
        }
        else {
            out := input;
        }
    }
    else {
        var mi := |input| / 2;
        var mid := input[mi];
        assert 0 <= mi < |input|;
        var low:seq<int> := [];
        var high:seq<int> := [];

        var i := 0;
        while i < |input|
        invariant |low| < |input|
        invariant |high| < |input|
        {
            if i != mi {
                if input[i] <= mid {
                    low := low + [input[i]];
                }
                else {
                    high := high + [input[i]];
                }
            }
            i := i + 1;
        }

        low := sort(low);
        high := sort(high);
        out := low + [mid] + high;
    }
}

predicate allEqual(a : seq<int>) {
    forall i, j :: (0 <= i < |a| && 0 <= j < |a|) ==> a[i] == a[j]
}

predicate noneFromS1inS2 (s1 : seq<int>, s2 : seq<int>) {
    forall i :: (0 <= i < |s1|) ==> !(exists j :: 0 <= j < |s2| && s1[i] == s2[j])
}


