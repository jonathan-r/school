#pragma once

#include <vertex>

#include "maths/mat4.h"
#include "maths/quat.h"

enum {
	GRABBER_TOO_FAR = 0,
	GRABBER_REACHING,
	GRABBER_REACED
};

class Grabber {
public:
	Grabber(int segs, float segLen) {
		len = segLen;
		for (int i = 0; i < segs; i++) {
			mats.emplace_back(mat4::identity());
			rots.emplace_back();
		}
	}

	void update(float dt);
	void setTarget(const vec3& at);

	const mat4& getMat (int id) const {
		return mats[id];
	}

private:
	float len;
	std::vertex<mat4> mats;
	std::vertex<quat> rots;
	vec3 end; //current location
};
