{-# LANGUAGE ScopedTypeVariables #-}
-- Language extension needed for line 57's type signature on a binding

module StateMonad where
import Control.Monad(liftM)
import System.Random

-- A type to represent state transformations

newtype State s a = State {
    runState :: s -> (a, s)
 }

--
-- We need a Functor instance, but because we will eventually
-- be making this type an instance of Monad the standard overloaded
-- implementation of liftM will do the job for us.
-- 
instance Functor (State s) where
  fmap = liftM

--
-- To be an instance of Monad we need to make this an instance of Applicative.
-- In practice the monad instance will just rely on "return = pure", so we can
-- supply our "return" implementation here. 
--
instance Applicative (State s) where
  pure a = State (\s -> (a,s))
  
--
-- Finally, to make State an instance of Monad we need to supply a bind operation.
-- The default implementations of >>, return, and error will work fine.
--
instance Monad (State s) where
    m >>= k = State (\s -> let (a,s') = runState m s
                           in runState (k a) s')

-- Next we provide the primitive operations on states.
-- Monad actions that extract copies of,or replace the state value.
get :: State s s
get = State $ \s -> (s,s)

put :: s -> State s ()
put s = State $ \_ -> ((),s)

--
-- An example of using this
--

-- Here is a data type. We will make some random values of this type in a moment.
data Val = Val Int Bool Char Int
           deriving Show

-- Here is how to make a random value using a standard generator.
makeRandomValue :: StdGen -> (Val, StdGen)
makeRandomValue g = let (n,g1) = randomR (1,100) g
                        (b::Bool,g2) = random g1
                        (c,g3) = randomR ('a', 'z') g2
                        (m,g4) = randomR (-n, n) g3
                     in (Val n b c m, g4 )

-- We can also make one using the State Monad to manage the generator:

-- First we need to make the actions over the generators into State
-- Monad values. As it happens they are already of the perfect
-- form. they just need to be wrapped up in the State constructor!

randomRState :: (Random a, RandomGen s) => (a, a) -> State s a
randomRState bounds = State $ randomR bounds

randomState :: (RandomGen g, Random a) => State g a
randomState = State random

makeRandomValueST :: State StdGen Val
makeRandomValueST = do
  n <- randomRState (1,100)
  b <- randomState
  c <- randomRState ('a','z')
  m <- randomRState (-n,n)
  return $ Val n b c m

main = do
         g <- getStdGen
         print $ fst $ makeRandomValue g
         print $ fst $ runState makeRandomValueST g
