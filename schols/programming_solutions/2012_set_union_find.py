import sys, re #ignore this

#this one is not in c, but it should be straightforward enough.
#what it does it, given a set of pairs of numbers, it 

#http://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf
def cyclic(num_verts, edges):
	#range returns an array of number [0, 1, 2, 3, ..., n]
	verts = range(num_verts) #each node points to itself
	
	#and this is a array made of zero, repeated num_verts times
	depths = [0] * num_verts #depth of the tree

	def find_root(i): #function in a function
		while i != verts[i]:
			verts[i] = verts[verts[i]] #shorten the path
			i = verts[i] #get its parent
		
		return i

	for edge in edges:
		ai = int(edge[0]) #this converts the string edge[0] to a number
		bi = int(edge[1]) #ditto

		ar = find_root(ai)
		br = find_root(bi)

		if ar == br: #they're already in the same set, so there's a cycle
			return True

		else: #merge a and b
			ad = depths[ar]
			bd = depths[br]

			if ad < bd: #a's set is shorter 
				verts[ar] = br #link a's set to b's set
				verts[ai] = br #link this element directly to b's set
				
				depths[bd] += ad

			else: 
				verts[br] = ar
				verts[bi] = ar

				depths[ad] += bd

	return False

verts = input()
line = sys.stdin.readline()

#don't worry about the regex. it's a bit of a cheat to extract all numbers quickly
pattern = re.compile('\((\d+) (\d+)\)')
ar = pattern.findall(line)

print cyclic(verts, ar)
