#pragma once

#include <cstdlib>
#include <cassert>

#include <GL/glew.h>

class Texture {
public:
	Texture() {
		glGenTextures(1, &id);
		channels = type = min = mag = w = h = 0;
		boundTo = -1;
	}

	Texture(int channels, GLenum sourceType, int width, int height, void* data = nullptr, GLenum minMode = GL_NEAREST, GLenum magMode = GL_NEAREST, float anisotropyLevel = 1.0f) {
		glGenTextures(1, &id);
		boundTo = -1;
		updateData(channels, sourceType, width, height, data);
		updateAccessMode(minMode, magMode, anisotropyLevel);
	}
	
	~Texture() {
		glDeleteTextures(1, &id);
	}
	
	void updateAccessMode(GLenum minMode, GLenum magMode, float anisotropyLevel) {
		min = minMode;
		mag = magMode;

		Texture* oldBound = boundToSlot[0];

		bind(0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropyLevel);

		if (oldBound) oldBound->bind(0);
	}
	
	void updateData(int channels, GLenum sourceType, int width, int height, void* data) {
		assert(width >= 64 and height >= 64); //gl spec requires this
		assert(channels >= 1 and channels <= 4);
	
		GLenum format = channelsToFormat(channels);
		type = sourceType;
		
		w = width;
		h = height;
	
		Texture* oldBound = boundToSlot[0];

		bind(0);
		glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, type, data);
		//gluBuild2DMipmaps(GL_TEXTURE_2D, internalFormat, w, h, sourceFormat, sourceType, data);

		if (oldBound) oldBound->bind(0);
	}
	
	void updateSubData(int dataWidth, int dataHeight, int offsetX, int offsetY, void* data) {
		assert (data);
		assert (offsetX >= 0 and offsetY >= 0);
		assert (offsetX + dataWidth <= w and offsetY + dataHeight <= h);
		
		bind(0);
		GLenum format = channelsToFormat(channels);
		glTexSubImage2D(GL_TEXTURE_2D, 0, offsetX, offsetY, dataWidth, dataHeight, format, type, data);
	}
	
	void bind(int slot) {
		if (boundTo != slot) {
			if (slot >= numUnits) {
				fprintf(stderr, "warning: trying to bind texture to slot %d; there are only %d slots available\n", slot, numUnits);
			}
			else {
				Texture* oldBound = boundToSlot[slot];
				if (oldBound) {
					oldBound->boundTo = -1;
				}
			}

			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_2D, id);

			boundToSlot[slot] = this;
			boundTo = slot;
		}
	}
	
	void unBind() {
		if (boundTo != -1) {
			assert(boundToSlot[boundTo] == this);
			glActiveTexture(GL_TEXTURE0 + boundTo);
			glBindTexture(GL_TEXTURE_2D, 0);
			boundToSlot[boundTo] = nullptr;
			boundTo = -1;
		}
	}
	
	GLuint getId() {
		return id;
	}
	
	int getWidth() {
		return w;
	}
	
	int getHeight() {
		return h;
	}
	
	static void init() {
		glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &numUnits);
		boundToSlot = new Texture*[numUnits];
		for (int i = 0; i < numUnits; i++) boundToSlot[i] = nullptr;
	}

	static void finalize() {
		delete [] boundToSlot;
	}
	
private:
	static GLenum channelsToFormat(int channels) {
		assert(channels >= 1 and channels <= 4);
		const GLenum channelsToFormatTable[] = {
			0, 
			GL_RED, //1
			GL_RG,  //2
			GL_RGB, //3
			GL_RGBA //4
		};
		return channelsToFormatTable[channels];
	}

	static int numUnits;
	static Texture** boundToSlot;

	//TODO: need 1 for each texture unit
	//static GLuint boundTexture; //= 0 for no texture

	GLuint id;
	int channels;
	GLenum type; //source type
	
	//access modes
	GLenum min;
	GLenum mag;
	
	int w;
	int h;

	int boundTo;
};
