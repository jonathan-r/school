gcc -std=c99 -O2 -fopenmp -march=native -o lab1_speedup harness.c matmul.c
gcc -std=c99 -O2 -fopenmp -march=native -o lab1_team_only harness-team-only.c matmul.c
#gcc -S -masm=intel -fverbose-asm -std=c99 -O1 -march=native -o lab1.asm matmul.c
