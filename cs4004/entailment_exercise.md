
p -> (q or r) entails ~r -> s

-> has 3 ways to be true, 1 way to be false
so focus on the false part;

make ~r -> s be false:
r = 0, s = 0, so ~r -> s is false

p -> (q or r), q -> r, p
0  1  0 0  0   0 1  0  1

contradiction in p




~p -> q, q -> p, p -> ~q entails p and ~q
   1       1       1                0
1  X  0          1    11         0     10
                 1 X  01         1     01
         1 X  0  0    01         0     01

find all the way (p and ~q) is 0




p -> q -> r   entails  q -> p -> r


p -> (q -> r)  entails  q -> (p -> r)
  1                       0     1    
0     1 1  1            1     0    1
1     1 1  1            1     1    1
0 X   1 0  0            1     0    0

to prove the entailment wrong, r must have a different value on left and right,
hence the entailment is correct


