
r = [0] * 8
func = 0
constant_in = 0
data_in = 0
reg_write = 0
reg_src_a = 0
reg_src_b = 0
reg_dest = 0

mb_select = 0
md_select = 0

addr_out = 0
data_out = 0

def clock():
	global r, func, constant

	a = r[reg_src_a]
	b = r[reg_src_b]
	f = 0

	if mb_select == 1:
		b = constant_in

	addr_out = a
	data_out = b

	if md_select == 0:
		if func == 0b00000: f = (a) & 0xffff
		elif func == 0b00001: f = (a + 1) & 0xffff
		elif func == 0b00010: f = (a + b) & 0xffff
		elif func == 0b00011: f = (a + b + 1) & 0xffff
		elif func == 0b00100: f = (a + ~b) & 0xffff
		elif func == 0b00101: f = (a + ~b + 1) & 0xffff
		elif func == 0b00110: f = (a - 1) & 0xffff
		elif func == 0b00111: f = (a) & 0xffff

		elif func == 0b01000: f = a & b
		elif func == 0b01001: f = a | b
		elif func == 0b01100: f = a ^ b
		elif func == 0b01110: f = a

		elif func == 0b10000: f = b
		elif func == 0b10100: f = ((b >> 1) ^ ((b >> 1) & 0x8000)) & 0xffff
		elif func == 0b11000: f = (b << 1) & 0xffff

	else:
		f = data_in

	print 'func', bin(func), 'a:', hex(a), 'b:', hex(b), 'f:', hex(f),
	if reg_write == 1: 
		print 'into', reg_dest
		r[reg_dest] = f

	else: print ''

	print r


reg_write = 1
md_select = 1 #load data in
		
reg_dest = 0b0
data_in = 0x0000 
clock()

reg_dest = 0b010
data_in = 0x02db
clock() #load 731 in r2

reg_dest = 0b011
data_in = 0x0177
clock() #load 375 in r3

reg_dest = 0b100
data_in = 0x0000
clock() #load 0 in r4
md_select = 0

for i in range(14): #for every bit except the last one (cause x << 15 is always 0)
	print 'round', i
	mb_select = 1
	constant_in = 0x0001 #0x1
	func = 0b01000 # bitwise and
	reg_src_a = 0b010 #r2
	reg_dest = 0b001 #r1
	clock() #r1 = r2 & 0x1
	print r[1] == 0 or r[1] == 1
	
	print "expand the first bit to every bit bits"
	mb_select = 0
	reg_src_a = 0b100 #r4, which stays 0 throughout
	reg_src_b = 0b001 #r1
	reg_dest = 0b001 #r1
	func = 0b00101 # difference
	clock() #r1 = 0 - r1
	print r[1] == 0 or r[1] == 0xffff
	
	print "and r3 with the mask"
	reg_src_a = 0b001 #r1
	reg_src_b = 0b011 #r3
	reg_dest = 0b001 #r1
	func = 0b01000 # bitwise and
	clock() #r1 &= r3
	
	print "accumulate the masked r3"
	reg_src_a = 0b000 #r0
	reg_src_b = 0b001 #r1
	reg_dest = 0b000 #r0
	func = 0b00010 #sum
	clock() #r0 += r1
	
	print "shift r3 left for next round"
	reg_src_b = 0b011 #r3
	reg_dest = 0b011 #r3
	func = 0b11000 #logical shift left
	clock() #r3 <= 1
	
	print "make the next bit of r2 the lsb"
	reg_src_b = 0b010 #r2
	reg_dest = 0b010 #r2
	func = 0b10100 #logical shift right
	clock() #r2 >>= 1

	print ''

print r[0]