library ieee;
use ieee.std_logic_1164.all;


entity mux2_8bit is
	port (in0 : in  std_logic_vector (7 downto 0);
			in1 : in  std_logic_vector (7 downto 0);
		   addr : in std_logic;
			output : out  std_logic_vector (7 downto 0) );
end mux2_8bit;

architecture behavioral of mux2_8bit is begin
	process(in0, in1, addr)
		begin case addr is
			when '0' => output <= in0 after 1ns;
			when '1' => output <= in1 after 1ns;
			when others => output <= "UUUUUUUU";
		end case;
	end process;
end behavioral;
