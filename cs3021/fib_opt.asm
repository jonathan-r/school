	.file	"fib.c"
	.intel_syntax noprefix
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4,,15
	.globl	fib
	.type	fib, @function
fib:
.LFB0:
	.cfi_startproc
	push	ebx
	.cfi_def_cfa_offset 8
	.cfi_offset 3, -8
	mov	edx, DWORD PTR [esp+8]
	cmp	edx, 1
	jle	.L4
	mov	ecx, 1
	xor	ebx, ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	mov	ecx, eax
.L3:
	sub	edx, 1
	lea	eax, [ebx+ecx]
	mov	ebx, ecx
	cmp	edx, 1
	jne	.L5
	pop	ebx
	.cfi_remember_state
	.cfi_restore 3
	.cfi_def_cfa_offset 4
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	mov	eax, edx
	pop	ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
.LFE0:
	.size	fib, .-fib
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.ident	"GCC: (GNU) 4.9.1 20140903 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
