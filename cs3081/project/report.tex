\documentclass[12pt,article]{article}
\usepackage[hmargin=1cm,vmargin=1.5cm]{geometry}
\usepackage[romanian]{babel}
\usepackage{combelow}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{fancyvrb}
\usepackage{tabularx}
\usepackage{graphicx}

\raggedbottom

\begin{document}
\begin{center}
{\large \bf CS3081 Project}\\
{\bf  Trilateration on a 2D plane}\\ 
\vspace{0.25 cm}
\small{\textbf{Jonathan Ro\cb{s}ca}}\\
\small{\textbf{12306261}}\\
\end{center}

\section*{Problem summary}
On a cartesian 2D plane, given a handful of stationary transmitters at known positions $(a_i, b_i)$,
find the position of a receiver at $(px, py)$ at a given point in time.
\\
The receiver is able to tell its distance to each transmitter $(d_i)$, within a certain margin of error.
\\
This naturally translates to an overdetermined system of non-linear equations in the form
\[f_i(x, y) = (x - a_i)^2 + (y - b_i)^2 = d_i^2, \]
one for each transmitter.

\section*{Solving the system}
This problem lends itself to being solved with the Gauss-Newton algorithm, which minimizes the sum of the squares of the residuals. This is an iterative technique, where each iteration should refine the previous iteration's result, starting with an initial guess $[x_0\ y_0]^T$.

At each iteration $n$, the method requires the computation of the Jacobian matrix, which contains the partial derivatives of the equations, applied the previous solution $[x_{n-1}\ y_{n-1}]^T$:
\begin{equation*}
J_n = 
\begin{bmatrix}
\frac{\partial f_1(x_{n-1}, y_{n-1})}{\partial x} & \frac{\partial f_1(x_{n-1}, y_{n-1})}{\partial y}\\
\frac{\partial f_2(x_{n-1}, y_{n-1})}{\partial x} & \frac{\partial f_2(x_{n-1}, y_{n-1})}{\partial y}\\
\frac{\partial f_3(x_{n-1}, y_{n-1})}{\partial x} & \frac{\partial f_3(x_{n-1}, y_{n-1})}{\partial y}\\
\vdots & \vdots \\
\frac{\partial f_i(x_{n-1}, y_{n-1})}{\partial x} & \frac{\partial f_i(x_{n-1}, y_{n-1})}{\partial y}\\
\end{bmatrix}
=
\begin{bmatrix}
2(x_{n-1} - a_1) & 2(y_{n-1} - b_1) \\
2(x_{n-1} - a_2) & 2(y_{n-1} - b_2) \\
2(x_{n-1} - a_3) & 2(y_{n-1} - b_3) \\
\vdots & \vdots \\
2(x_{n-1} - a_i) & 2(y_{n-1} - b_i) \\
\end{bmatrix},
\end{equation*}

as well as a residuals vector, which is the difference between the observations taken and the result of each function applied to $[x_{n-1}\  y_{n-1}]^T$:
\begin{equation*}
b_n = 
\begin{bmatrix}
f_1(x_{n-1}, y_{n-1}) - d_1 \\
f_2(x_{n-1}, y_{n-1}) - d_2 \\
f_3(x_{n-1}, y_{n-1}) - d_3 \\
\vdots \\
f_i(x_{n-1}, y_{n-1}) - d_i \\
\end{bmatrix}
=
\begin{bmatrix}
((x_{n-1} - a_1)^2 + (y_{n-1} - b_2)^2) - d_1^2 \\
((x_{n-1} - a_2)^2 + (y_{n-1} - b_2)^2) - d_2^2 \\
((x_{n-1} - a_3)^2 + (y_{n-1} - b_2)^2) - d_3^2 \\
\vdots \\
((x_{n-1} - a_i)^2 + (y_{n-1} - b_i)^2) - d_i^2 \\
\end{bmatrix}
.
\end{equation*}

Now solving the equation
\begin{equation*}
J_n
\begin{bmatrix}
\Delta x \\
\Delta y \\
\end{bmatrix}
 = b_n,
\end{equation*}

leads to the solution of the current iteration
\begin{equation*}
x_n = x_{n-1} -
\begin{bmatrix}
\Delta x \\
\Delta y \\
\end{bmatrix}
.
\end{equation*}

\subsection*{Termination}
The iteration process can stop when the values of $\Delta x$ and $\Delta y$ get insignificantly small. This is called convergence. If the values do not become smaller, then either the initial guess is incorrect or there is no solution to the system of equations.

\section*{Implementation}
The method described above was implemented in the Python programming language, using the {\tt numpy} library. It should work in both Python 2 and Python 3.

\subsection*{{\tt trilat.py}}
This contains a library of functions, implementing the solver described above.
The functions {\tt jacobian} and {\tt residuals} compute the jacobian matrix and the residuals vector, respectively. The function {\tt solve\_system} returns a generator that does one iteration of the solution on each {\tt .next()} call (such as in a for loop). All three functions take the positions of the transmitters and the measurements (distances), which should be lists of the same length. The function {\tt solve\_system} also takes an initial guess, and optionally a tolerance or epsilon value for convergence and a hard iteration limit. It raises an exception if convergence is not achieved within the hard limit of iterations.

\subsection*{{\tt test.py}}
This file contains a testing script for the method implemented in {\tt trilat.py}. It uses the {\tt matplotlib} library to display the data and the solving process.

The function {\tt generate\_data} randomly generates data with evenly distributed errors in the distance measurements. The number of transmitters and the magnitude of the error (as a ratio of the measurement) are specified as parameters.

The function {\tt plot\_residual\_map} takes the generated data and plots the sum of the squares of the residuals at every point in space. It is computationally intensive.

The function {\tt plot\_initial\_guess\_error\_map} takes the generated data and runs the solver with every point as the starting guess, plotting the distance of the final solution to the actual position of the receiver. It is even more computationally intensive.

These two functions return a handle of their subplot, which should be passed to the functions {\tt plot\_input\_data} and {\tt plot\_single\_run}, which display the positions of the transmitters and plot the solution of each iteration (for a single starting point), respectively. The latter function also calls {\tt scipy.optimize.leastsq} to compare the final answer it got from the solver, and plots this point.

\section*{Testing}
Here is a very informal and by no means complete analysis of the behavior of the solver, with a few edge cases highlighted. Most of the time, the solution given by the solver is very similar to that returned by the scipy function.

\subsection*{Initial guess}
The initial guess does not seem to matter much. So long as there is convergence, there is a very small difference in the error of the final solution, based on the value of the initial guess. In the background is a map generated by the {\tt plot\_initial\_guess\_error\_map} function:

\includegraphics[scale=0.5]{solution_error}

\subsection*{Number of transmitters}
When only two transmitters are present, the receiver is unlikely to be exactly in between them, so there are usually two points that both solve the system of equations, at the intersections of the circles centered on the transmitters. Hence, in general, more transmitters are needed for the system to be reliable.
Here is a run without a solution attempt:

\includegraphics[scale=0.5]{two_transmitters}

\subsection*{Position of receiver relative to transmitters}
When the receiver is away from all the transmitters, the map of the residuals looks like a kind of `moat' around the transmitters and neither the solution described here nor the scipy solver give an accurate solution. Here's a run with the initial guess as the mean of the position of the transmitters, with the residuals in the background:

\includegraphics[scale=0.5]{position_of_receiver}

When the receiver is flanked by the transmitters, the map of the residuals is much simpler, resembling a pit with very shallow edges, at the bottom of which the receiver lies. This suggests that the initial guess can start have any value, since the solver will always tend to `flow' down to the pit.

\includegraphics[scale=0.5]{receiver_in_the_middle}

\end{document}

