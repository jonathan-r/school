#version 120

uniform vec2 tex_size;
uniform sampler2D tex;

void main(){
	int cx = int(gl_FragCoord.x);
	int cy = int(gl_FragCoord.y);
	
	int n = 0, a;
	n += int(texture2D(tex, vec2(cx - 1, cy - 1) / tex_size).x);
	n += int(texture2D(tex, vec2(cx,     cy - 1) / tex_size).x);
	n += int(texture2D(tex, vec2(cx + 1, cy - 1) / tex_size).x);
	
	n += int(texture2D(tex, vec2(cx - 1, cy    ) / tex_size).x);
	a  = int(texture2D(tex, vec2(cx,     cy    ) / tex_size).x);
	n += int(texture2D(tex, vec2(cx + 1, cy    ) / tex_size).x);
	
	n += int(texture2D(tex, vec2(cx - 1, cy + 1) / tex_size).x);
	n += int(texture2D(tex, vec2(cx,     cy + 1) / tex_size).x);
	n += int(texture2D(tex, vec2(cx + 1, cy + 1) / tex_size).x);
	
	float c = 0.0;
	if (a == 1){
		if (n < 2 || n > 3) c = 0.0;
		else c = 1.0;
	}
	else if (n == 3) c = 1.0;
	gl_FragColor = vec4(vec3(c), 1.0);
}
