	.file	"sort.c"
	.intel_syntax noprefix
	.text
	.globl	sort
	.type	sort, @function
sort:
.LFB0:
	.cfi_startproc
	push	ebp                     #keep the old base pointer safe
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	mov	ebp, esp                #start a new stack frame
	.cfi_def_cfa_register 5
	sub	esp, 16                 #allocate 16 bytes of stack space, even though only 8 are needed (must keep 16 byte alignment)
	mov	DWORD PTR [ebp-8], 0    #assign 0 to the i variable, on the stack
	jmp	.L2                     #go to the for condition check

.L6: #for condition success
	jmp	.L3                     #continue on to while condition check

.L5: #while condition success
	mov	eax, DWORD PTR [ebp-8]  #load i variable from the stack
	lea	edx, [0+eax*4]          #compute (i)th element offset
	mov	eax, DWORD PTR [ebp+8]  #load array pointer from the stack
	add	eax, edx                #compute pointer of the (i)th element
	mov	eax, DWORD PTR [eax]    #load value of (i)th element
	mov	DWORD PTR [ebp-4], eax  #assign the value (i)th element to the tmp variable and store the variable on to the stack
	mov	eax, DWORD PTR [ebp-8]  #load i variable from the stack
	lea	edx, [0+eax*4]          #compute offset of the (i)th element
	mov	eax, DWORD PTR [ebp+8]  #load array pointer from the stack
	add	edx, eax                #compute (i)th element pointer
	mov	eax, DWORD PTR [ebp-8]  #load i variable from the stack
	add	eax, 1                  #add 1, to get the next index, as per line 5
	lea	ecx, [0+eax*4]          #load the offset of the (i+1)th element
	mov	eax, DWORD PTR [ebp+8]  #load the pointer of the array
	add	eax, ecx                #compute the pointer of the (i+1)th element
	mov	eax, DWORD PTR [eax]    #load the value of the (i+1)th element
	mov	DWORD PTR [edx], eax    #assign the (i+t)th element's value to the (i)th element
	mov	eax, DWORD PTR [ebp-8]  #load the i variable again
	add	eax, 1                  #add 1 for line 6
	lea	edx, [0+eax*4]          #offset
	mov	eax, DWORD PTR [ebp+8]  #base
	add	edx, eax                #pointer of (i+1)th element
	mov	eax, DWORD PTR [ebp-4]  #load the tmp variable off the stack
	mov	DWORD PTR [edx], eax    #assign the tmp value to the (i+1)th element
	sub	DWORD PTR [ebp-8], 1    #decrement i directly on the stack

.L3: #while condition check
	mov	eax, DWORD PTR [ebp-8]  #load i variable from the stack
	add	eax, 1                  #add 1, to get the next array index
	lea	edx, [0+eax*4]          #compute the offset of the (i+1)th element
	mov	eax, DWORD PTR [ebp+8]  #load the array pointer
	add	eax, edx                #add the offset to the base, getting the actual pointer of the (i+1)th element
	mov	edx, DWORD PTR [eax]    #load the value of the (i+1)th element
	mov	eax, DWORD PTR [ebp-8]  #load the i variable from the stack again
	lea	ecx, [0+eax*4]          #compute the offset of the (i)th element
	mov	eax, DWORD PTR [ebp+8]  #load the array pointer
	add	eax, ecx                #add the offset to the base for the (i)th element
	mov	eax, DWORD PTR [eax]    #load the value of the (i)th element
	cmp	edx, eax                #compare (i+1)th and (i)th elements' values
	jle	.L4                     #if (i+1)th is smaller or equal, jump away (else case)
	cmp	DWORD PTR [ebp-8], 0    #logical-and with (i >= 0)
	jns	.L5                     #(jump not sign) go to the if body if the last cmp succeeded (i >= 0)

.L4: #while condition fail
	add	DWORD PTR [ebp-8], 1    #execute the i++ part of the for loop, then continue on to a new iteration (before condiition checking)

.L2: #for condition check
	mov	eax, DWORD PTR [ebp+12] #load size argument from the stack
	sub	eax, 1                  #subtract one, as per the for loop condition
	cmp	eax, DWORD PTR [ebp-8]  #compare with the i variable stored on the stack
	jg	.L6                     #take the branch if i > size - 1
	leave                           #otherwise, undo this stack frame (copy esp into ebp and pop ebp)
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret                             #branch back to caller
	.cfi_endproc
.LFE0:
	.size	sort, .-sort
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
