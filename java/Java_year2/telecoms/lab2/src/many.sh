if [ "$#" == "0" ]; then
	echo "usage: $0 <host> <file1> [file2] [file3] ...";
	exit
fi


DEST=$1
PORT=1567

shift
for file in "$@"; do
	echo $file
	java Sender $DEST $PORT $file &
	PORT=$((PORT+1))
done
