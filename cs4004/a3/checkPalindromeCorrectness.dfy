predicate isPalindrome(s : seq<int>) {
    forall i, j :: 0 <= i < |s|/2 && j == |s| - i - 1 ==> s[i] == s[j]
}

predicate isPalindromeBounds(s : seq<int>, bound: int) { 
    0 <= bound < |s| && forall i, j :: 0 <= i < bound && j == |s| - i - 1 ==> s[i] == s[j]
}

method checkPalindrome(s : seq<int>) returns (res: int)
    requires |s| > 0 
    ensures (res == 1) ==> isPalindrome(s)
{
    res := 1;
    var i := 0;
    var j := |s|-1;

    while (i < j && res == 1)
        invariant 0 <= i < |s|
        invariant 0 <= j < |s|
        invariant j == |s| - i - 1
        invariant (res == 1) ==> isPalindromeBounds(s, i)

        decreases j - i + res
    {
        if (s[i] != s[j]) {
            res := 0;
        }
        else {
            i := i + 1;
            j := j - 1;

        }
        assert (res == 1) ==> isPalindromeBounds(s, i);
    }

    assert (res == 0 || res == 1);
    
    assert (res == 1) ==> isPalindromeBounds(s, i);
    assert (res == 1) ==> isPalindromeBounds(s, |s|/2);
    assert isPalindromeBounds(s, |s|/2) ==> isPalindrome(s);

    assert (res == 0) ==> !isPalindromeBounds(s, |s|);
    assert !isPalindromeBounds(s, |s|/2) ==> !isPalindrome(s);
    assert (res == 0) ==> !isPalindrome(s);
}
