 'use strict';

//http://stackoverflow.com/questions/122102/what-is-the-most-efficient-way-to-clone-a-javascript-object 4th/5th answer
function deepClone(ob){
	var out = (ob instanceof Array) ? [] : {};
	for (var item in ob){
		if (ob[item] && (typeof ob[item]) == 'object')
			out[item] = objectDeepClone(ob[item]);
		else 
			out[item] = ob[item];
	}

	return out;
}

function shallowClone(ob){
	var out = (ob instanceof Array) ? [] : {};
	for (var item in ob)
		out[item] = ob[item];

	return out;
}

function setify(array){
	array.sort();
	var last = array[0];
	for (var i = 1; i < array.length; i++){
		while(array[i] == last) array.splice(i, 1);
		last = array[i];
	}
}

