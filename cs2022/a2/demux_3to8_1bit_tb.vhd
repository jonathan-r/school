library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity demux_3to8_1bit_tb is
end demux_3to8_1bit_tb;
 
architecture behavior of demux_3to8_1bit_tb is 
	component demux_3to8_1bit is
	 port ( data : in std_logic;
			  addr : in  std_logic_vector (2 downto 0);
           output : out std_logic_vector (7 downto 0) );
	end component;

	signal addr_in : std_logic_vector(2 downto 0) := (others => '0');
	signal data_in : std_logic_vector(0 downto 0) := "0"; --to get around the "Cannot index the result of a type conversion" silliness
	signal output : std_logic_vector(7 downto 0);

begin
	uut: demux_3to8_1bit port map (data_in(0), addr_in, output);

	stim_proc: process begin		
		wait for 1 ns;
		for d in 0 to 1 loop --data
			data_in <= std_logic_vector(to_unsigned(d, 1));
			for a in 0 to 7 loop --addr
				addr_in <= std_logic_vector(to_unsigned(a, 3));
				wait for 1 ns;
				assert output = std_logic_vector(to_unsigned(d, 8) sll a);
			end loop;
		end loop;
		
		wait;
	end process;
end;
