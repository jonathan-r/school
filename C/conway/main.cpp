#include <cstdlib>
#include <cstdio>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/glu.h>

#include "app.h"

//statics
GLuint Texture::boundTexture = 0;
GLuint Shader::boundProgram = 0;
GLuint Framebuffer::boundFramebuffer;
Texture* TextRender::tex = NULL;
Shader* TextRender::shader = NULL;
App App::app;

int main(int argc, char** argv){
	glutInit(&argc, argv);
	//glutInitContextVersion (1, 1);
	//glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	//glutInitContextProfile(GLUT_CORE_PROFILE);
	
	App::app.run();

	return 0;
}
