\documentclass[12pt,article]{article}
\usepackage[hmargin=1cm,vmargin=1.5cm]{geometry}
\usepackage[romanian]{babel}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{textcomp}
\usepackage{graphicx}

\definecolor{greencomments}{rgb}{0,0.5,0}
\lstset{
	language=C++,
	breaklines=true,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{greencomments}\ttfamily,
	morecomment=[l][\color{magenta}]{\#}
}

\newcommand*{\addheight}[2][.5ex]{%
  \raisebox{0pt}[\dimexpr\height+(#1)\relax]{#2}%
}

\begin{document}
\begin{center}
{\large \bf CS4053}\\
{\bf  Computer Vision\\Page detection based on known templates}\\
\vspace{0.25 cm}
\small{\textbf{Jonathan Rosca}}\\
\small{\textbf{12306261}}\\
\end{center}

\section*{Introduction}
Presented is a computer vision system that, given a number of sample page images (referred to as `templates'), attempts to identify whether a given photo (referred to as a `test image') contains one of the pages from the templates and to find the best matching template.

\section*{Constraints}
The object in the test image must have a an outline of blue dots around the page. If the system identifies that the test image contains a page as described above, it cannot determine whether the contents of the page actually matches any of the templates. It can only give the best match. In the test image, the page must be evenly lit and must be contained almost entirely in the image. The blue outline of dots must not be obscured on more than one corner. Ideally, the camera must point directly at the page. The system will fail if the angle of the page relative to the line of sight is too acute. The test image need not be aligned, but if a flipped version of a template image is considered to be distinct from the actual template, the system is incorrectly match it.

\section*{Methodology}
The system is made of three components:
\subsection*{Preparation}
Each of the template images is loaded, cropped to remove its outline, scaled into a square (to enable $90^{\circ}$ rotated comparisons), converted to grayscale, then run through the Canny edge detector. The edge detection is done to lessen the effect of noise and lighting and color balancing variations in the test images.

Also loaded is a sample image of the expected blue dots ({\tt blue.png}). An {\tt RGB} histogram of this image is computed, with $4$ bins.

\subsection*{Page isolation}
First the histogram of the blue dots image is back-projected into the test image to produce a blue dot probability map, which is then normalized. This map is dilated so as to connect very close components that are actually the same feature. A binary threshold of the map is taken - the best value seems to be $50\%$. Connected-components is run on the threshold image to find all the possible blue dots in the test image. The centers of all the contours is stored, these will be referred to as `points'.

Segments are formed between each closest points, starting with any of the points and continuing to the closest one, until all points lead to another point. This usually forms a perfect loop around the outline of the page in the test image, but there may be false positives among the points, which will be called `outliers'.

To remove these outliers, the median ($M$) and standard deviation ($\sigma$) of the lengths of each of the segments is computed. These values vary depending on the number of outliers in the image. Provided that the median lands on a legitimate segment length, it is possible to exclude the outliers.

The displacement between the end of each segment is computed. It is negated in certain conditions, so that regardless of the ordering of the segment loop, there will be roughly two clusters of slopes. The more outliers are removes, the more clear these clusters will be. In a linear fashion, a standard deviation multiplier threshold $m$ must be fine tuned, starting from a very inclusive value of $3.0$, down to $0.1$, in $0.2$ increments. Any segment whose length is outside the range $M \pm m\sigma$ is considered an outlier and is excluded. For each value of $m$, k-means clustering with $K=2$ is run on the remaining segments, in the segment-displacement space. A successful clustering yields two clusters that represent two direction vectors, one for the horizontal segments, one for the vertical segments (it is arbitrary which is which). These vectors are ideally orthogonal and their relative length is equal. When these two properties are within an acceptance threshold, the $m$ fine tuning is ceased. If the process does not yield two correct direction vectors, the test image is concluded to lack a page with a blue dot outline.

After the final (successful) $m$ fine tuning iteration, it is expected that all remaining segments are part of the outline. The direction vectors are normalized and their tangents (also normalized) are computed. Each point is projected onto the tangents of each side vector, to establish the position of each point along the tangent vector. The minimum and maximum values these stored, as well as the mid-points.

The k-means clustering has partitioned the segments based on their slope. The two clusters of segments are each divided in two, based on whether the projection of the tangent direction is less than the mid-point of the extremes on that direction.

Ideally, there are now 4 clusters of segments, but there can be cases where one or even two sides are missing. For all sides that have clusters, the points available are fit into a line using {\tt fitLine}, yielding a (straight) line definition for that side of the outline. For the sides of the outline where there are no segments available, the tangent extremes are used to substitute in a line based on a point derived from the extreme multiplied by the tangent vector and with the direction vector from the cluster. This usually fails to match the actual line, but is reasonably close.

Finally, the four lines are intersected against each other, yielding four corner points. From these, an inverse projection transformation matrix is derived. The projection reverse also crops the image, into a square image, just slightly larger than the template images. This is referred to as the comparison image. Because of the ordering of the lines, the comparison image may be flipped.

\subsection*{Content matching}
Provided the page extraction was successful, the system moves on to attempt to match the comparison image to one of the template images. First, the comparison image is converted to grayscale and the Canny edge detector is applied to it.

As there is no way to tell which way the comparison image is rotated. For each $90^{\circ}$ rotation, the original and transpose thereof, for each template image, template matching is run. The index of the image with the best matching metric is recorded.

There is always a match, even if it is erroneous. This index is returned.
\pagebreak
\section*{Improvements}
The point segment generation method could be improved by making it reverse direction when it detects that it has started in the wrong direction.

The cluster detection is very vulnerable to noise, hence the need for outlier removal. A more robust method such as DBSCAN could be used to directly find the clusters and also eliminate outliers at the same time. The system is fine tuned to the set of test images provided. It would likely fail on any other conditions.

When the page in the test image is bent, it would be possible (provided that all outline points are detected perfectly) to draw lines between opposing points, so as to create a mesh of tiles. Each tile could be used to generate an inverse perspective matrix only for that area, which would enable the final comparison image to have the bending removed.

\section*{Program}
The system is implemented as two C++ programs based on OpenCV 3. They both take the path of the template images and that of the test images. The {\tt pages\_gui} program has a graphical interface and displays some of the working of the program. It has a trackbar at the top, for the index of the test image to process. Whenever the trackbar changes, a new image is loaded, and the program displays its best template match, along with whether the match was accurate (the ground truth is hard-coded). The second program, {\tt pages\_batch} does not have graphical user interface. Instead, it processes all the test images one after another and prints its best match.

\section*{Testing and Metrics}

The {\tt pages\_batch} program yields the following output:
\begin{lstlisting}
ound 13 reference images
image test/BookView01.JPG matched with template 1 - correct
image test/BookView02.JPG matched with template 2 - correct
image test/BookView03.JPG matched with template 3 - correct
image test/BookView04.JPG matched with template 4 - correct
image test/BookView05.JPG matched with template 5 - correct
image test/BookView06.JPG matched with template 6 - correct
image test/BookView07.JPG matched with template 7 - correct
image test/BookView08.JPG matched with template 8 - correct
image test/BookView09.JPG matched with template 9 - correct
image test/BookView10.JPG matched with template 10 - correct
image test/BookView11.JPG matched with template 11 - correct
image test/BookView12.JPG matched with template 12 - correct
image test/BookView13.JPG matched with template 13 - correct
image test/BookView14.JPG matched with template 2 - correct
image test/BookView15.JPG matched with template 3 - correct
image test/BookView16.JPG matched with template 5 - correct
image test/BookView17.JPG matched with template 4 - correct
image test/BookView18.JPG matched with template 7 - correct
image test/BookView19.JPG matched with template 9 - correct
image test/BookView20.JPG matched with template 8 - correct
image test/BookView21.JPG matched with template 7 - correct
image test/BookView22.JPG matched with template 11 - correct
image test/BookView23.JPG matched with template 13 - correct
image test/BookView24.JPG matched with template 12 - correct
image test/BookView25.JPG matched with template 2 - correct
image test/BookView26.JPG matched with template 1 - correct
image test/BookView27.JPG matched with template 3 - correct
image test/BookView28.JPG matched with template 3 - correct
image test/BookView29.JPG matched with template 4 - correct
image test/BookView30.JPG matched with template 9 - correct
image test/BookView31.JPG matched with template 8 - wrong
image test/BookView32.JPG matched with template 10 - correct
image test/BookView33.JPG matched with template 12 - correct
image test/BookView34.JPG matched with template 13 - correct
image test/BookView35.JPG matched with template 1 - correct
image test/BookView36.JPG not matched
image test/BookView37.JPG matched with template 6 - correct
image test/BookView38.JPG not matched
image test/BookView39.JPG matched with template 8 - correct
image test/BookView40.JPG not matched
image test/BookView41.JPG matched with template 7 - correct
image test/BookView42.JPG matched with template 6 - correct
image test/BookView43.JPG matched with template 9 - correct
image test/BookView44.JPG matched with template 8 - correct
image test/BookView45.JPG matched with template 3 - wrong
image test/BookView46.JPG matched with template 11 - correct
image test/BookView47.JPG matched with template 12 - wrong
image test/BookView48.JPG not matched
image test/BookView49.JPG not matched
image test/BookView50.JPG matched with template 3 - wrong
\end{lstlisting}
\vspace{1cm}
The {\tt pages\_gui program appears as follow, in two situations:
\begin{center}
\begin{tabular}{c|c}
\addheight{\includegraphics[width=9cm]{page33.png}} &
\addheight{\includegraphics[width=9cm]{page48.png}} \\
one reconstructed line & page not detected \\
\end{tabular}
\end{center}

\subsection*{Metrics}
\begin{itemize}
\item a true positive is a page visible and recognised correctly.
\item a false positive is an incorrectly recognised page (there are no test images without a page).
\item a true negative is a lack of a page visible and correctly being identified as such (no such test images).
\item a false negative is a page being visible but not recognized.
\end{itemize}
\vspace{1cm}
Based on the provided template and test images, the system performs as follows for the first 25 images:
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Total & True Positive & False Positive & True Negative & False Negative \\
\hline
25 & 25 & 0 & 0 & 0 \\
\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Precision & Recall & Accuracy & Specificity & Sensitivity & F1 \\
\hline
$1.0$ & $1.0$ & $1.0$ & $0/0$ & $1.0$ & $1.0$ \\
\hline
\end{tabular}
\end{center}

\vspace{1cm}
And for all 50 images:
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Total & True Positive & False Positive & True Negative & False Negative \\
\hline
50 & 41 & 4 & 0 & 5 \\
\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Precision & Recall & Accuracy & Specificity & Sensitivity & F1 \\
\hline
$0.911$ & $0.891$ & $0.82$ & $0/0$ & $0.911$ & $0.9$ \\
\hline
\end{tabular}
\end{center}

\end{document}
