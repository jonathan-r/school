for class in `find . -type f | grep Test\.class$`
do
	file=`basename $class`;
	stem="${file%.*}";
	echo "running $stem";
	java -cp ../lib/hamcrest-core-1.3.jar:../lib/junit-4.11.jar:. "$stem";
done
