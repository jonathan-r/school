#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>

#include "huffman.h"


//generates valid latex code for any ascii character. Characters outside the range ' ' .. '~' are represented by "0x__"
char* escape_latex(uint8_t c){
	static char buf[16];
	switch(c){
		//symbols that can be drawn but must be escaped
		case ' ' : return "{ }";
		case '#' : return "\\#";
		case '%' : return "\\%";
		case '&' : return "\\&";
		case '~' : return "\\~{}";
		case '_' : return "\\_";
		case '^' : return "\\^{}";
		case '\\': return "\\textbackslash";
		case '{' : return "\\{";
		case '}' : return "\\}";
		case '[' : return "{[}";
		case ']' : return "{]}";
	}
	
	char* format;
	if (c >= ' ' && c <= '~') format = "%c"; //alpha-numeric and symbols that passed the switch do not have to be escaped
	else if (c < 16) format = "0x0%X"; //unprintable characters below 0x10
	else format = "0x%X"; //and above 0xF
	
	sprintf(buf, format, c);
	return buf;
}


//generates valid latex syntax for a tree diagram, neatly laid out to be as tight as possible, but very much readable;
//requires the qtree package
void print_latex(symbol* branch, int depth, char* code){
	if (depth == 0) printf("\\Tree");
	
	printf("[.%d ", branch->freq);

	if (branch->lower[0]){
		code[depth] = '0';
		print_latex(branch->lower[0], depth + 1, code);
		
		code[depth] = '1';
		print_latex(branch->lower[1], depth + 1, code);
	}
	
	else{
		code[depth] = '\0';
		printf("%s", escape_latex(branch->c));
	}
	
	if (depth == 0) printf("]\n");
	else printf(" ]");
}

