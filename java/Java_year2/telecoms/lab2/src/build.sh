build(){
	javac $1 2>&1 | egrep -A 10000 -B 100000 --color "\:[[:digit:]]+\:" 
}

build "Receiver.java Symbols.java Listener.java"
build "Sender.java Symbols.java Listener.java"

