#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>

#include "huffman.h"


void list_insert(symbol** list, symbol* ns){
	symbol* prev = NULL;
	for (symbol* s = *list; s; s = s->next){
		if (s->freq > ns->freq) break;
		prev = s;
	}
	
	if (prev){
		ns->next = prev->next;
		prev->next = ns;
	}
	else{
		ns->next = *list;
		*list = ns;
	}
}


void make_definition(symbol* branch, hcode* codes, int depth, unsigned code){
	if (branch->lower[0]){
		make_definition(branch->lower[0], codes, depth + 1, (code << 1) | 0);
		make_definition(branch->lower[1], codes, depth + 1, (code << 1) | 1);
	}
	
	else{
		codes[branch->c].len = depth;
		codes[branch->c].data = code;
	}
}


void tree_free(symbol* branch){
	symbol* b0 = branch->lower[0];
	symbol* b1 = branch->lower[1];
	
	free(branch);
	if (b0){
		tree_free(b0);
		tree_free(b1);
	}
}

//constructs a huffman tree given a text
//codes must be an array to hold the 256 hcodes, for each character
//in case the character is never encountered, the corresponding hcode will have .len set to 0
void analyze_text(uint8_t* str, hcode* codes){
	int frequency[256]; //of each character;
	memset(frequency, 0, 256 * sizeof(int));
		
	frequency[0] = 1; //there's only one NULL terminator
	for (uint8_t* c = str; *c; c++) frequency[*c]++; //count every instance of every character
	
	//make a node for each character
	symbol* list = NULL;
	for (int character = 0; character < 256; character++){
		if (frequency[character] > 0){
			symbol* s = malloc(sizeof(symbol));
			s->c = character;
			s->freq = frequency[character];
		
			s->lower[0] = s->lower[1] = NULL;
			list_insert(&list, s);
		}
	}
	
	//do the huffman thing until there's only one element
	while(list->next){
		//make a pair of the two least represented nodes
		symbol* pair = malloc(sizeof(symbol));
		pair->lower[0] = list;
		pair->lower[1] = list->next;
		pair->freq = pair->lower[0]->freq + pair->lower[1]->freq;
		if (pair->freq == 0) pair->freq = 1; //hackish fix for degenerate branches caused by nodes with 0 frequency
		pair->c = 0; //doesn't matter
		
		//remove those two nodes
		list = list->next->next;
		
		//and insert the pair
		list_insert(&list, pair);
	}
	
	//now list only contains one element, the root of the tree
	
	//make the huffman codes from the implicit definitions in the tree structure
	memset(codes, 0, 256 * sizeof(hcode));
	make_definition(list, codes, 0, 0);
	
	char buf[1024];
	print_latex(list, 0, buf);
	
	//then remove the tree
	tree_free(list);
}
