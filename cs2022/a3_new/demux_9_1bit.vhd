library ieee;
use ieee.std_logic_1164.all;


entity demux_9_1bit is
   port ( data : in std_logic;
			  addr : in std_logic_vector (3 downto 0);
           output : out std_logic_vector (8 downto 0) );
end demux_9_1bit;

architecture behavioral of demux_9_1bit is 
begin
	process(data, addr) begin
		if data = '1' then
			if addr(3) = '0' then
				case addr(2 downto 0) is
					when "000" => output <= "000000001";
					when "001" => output <= "000000010";
					when "010" => output <= "000000100";
					when "011" => output <= "000001000";
					when "100" => output <= "000010000";
					when "101" => output <= "000100000";
					when "110" => output <= "001000000";
					when "111" => output <= "010000000";
					when others => output <= "UUUUUUUUU";
				end case;
			
			elsif addr(3) = '1' then
				output <= "100000000";
			
			else
				output <= "UUUUUUUUU";
			
			end if;
		else
			output <= "000000000";
		end if;
	end process;
end behavioral;

