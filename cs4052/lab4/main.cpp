#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <vector>
#include <iostream>
#include <fstream>

#include "gen_mesh.h"
#include "shader_program.h"
#include "vbo.h"
#include "vertex.h"

#include "maths/mat4.h"
#include "maths/vec.h"

#ifdef GLSL120
#define SHADER_ROOT_NAME "shaders/shader_120"
#else
#define SHADER_ROOT_NAME "shaders/shader_330"
#endif

using namespace std;

//window dimensions
int WW = 800;
int WH = 600;

ShaderProgram* mainShader = NULL;
VertexArrayObject<vertex>* cylModel = NULL;
VertexArrayObject<vertex>* arrowsModel = NULL;
VertexArrayObject<vertex>* gridModel = NULL;

const float cylWidth = 1.0f;
const float cylHeight = 1.0f;

quat cameraRot;
vec3 cameraPos;

vec3 translation;

struct Object {
	vec3 stem; //local space
	quat rot; //local space
	mat4 global; //global space
	vector<Object*> shoots;

	void bakeAll(const mat4& parent = mat4::identity()) {
		mat4 tr = mat4::translation(stem);
		mat4 rt = mat4::quaternionRotation(rot);
		global = parent * tr * rt;
		for (auto ob : shoots) {
			ob->bakeAll(global);
		}
	}
};

Object rootObj;

bool keysDown[256];
int lastDrawTime = 0;
int lastIdleTime = -1;

void drawChain(Object& obj, mat4 proj, GLuint matLoc, int verts) {
	mat4 m = proj * obj.global;
	glUniformMatrix4fv(matLoc, 1, GL_FALSE, m.data);
	glDrawArrays(GL_TRIANGLES, 0, verts);

	for (auto ob : obj.shoots) {
		drawChain(*ob, proj, matLoc, verts);
	}
}

void display() {
	int frameBeginTime = glutGet(GLUT_ELAPSED_TIME);
	lastDrawTime = frameBeginTime;
	float time = (float)frameBeginTime / 1000.0f;
	
	WW = glutGet(GLUT_WINDOW_WIDTH);
	WH = glutGet(GLUT_WINDOW_HEIGHT);
	//take keyboard input
	//update - TODO: move to idle function
	mat4 model = mat4::translation(translation);

	GLuint prog = mainShader->getProgObj();
	glUseProgram(prog);
	
	GLuint matLoc = glGetUniformLocation(mainShader->getProgObj(), "matrix");
	GLuint lightFractionLoc = glGetUniformLocation(mainShader->getProgObj(), "lightFraction");
	GLuint timeLoc = glGetUniformLocation(mainShader->getProgObj(), "time");
	
	//actual display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUniform1f(timeLoc, time);
	
	glViewport(0, 0, WW, WH);
	float aspect = (float)WW / WH;

	mat4 proj = mat4::perspectiveProjection(45.0f, aspect, 0.1f, 1000.0f);
	mat4 view = mat4::viewLookAt(vec3(0.0f, 5.0f, 10.0f), vec3(0.0f), vec3(0.0f, 1.0f, 0.0f));
	
	vec3 p(10.0f * cosf(time), 5.0f, 10.0f * sinf(time));
	view = mat4::viewLookAt(p, vec3(0.0f), vec3(0.0f, 1.0f, 0.0f));

	//mat4 matrix = model;
	mat4 matrixScene = proj * view; //no model
	mat4 matrixModel = proj * view * model;

	//arrows, transformed
	glUniformMatrix4fv(matLoc, 1, GL_FALSE, matrixScene.data);
	arrowsModel->bind(prog);
	glUniform1f(lightFractionLoc, 0.0f);
	glDrawArrays(GL_LINES, 0, arrowsModel->getVerticesAssigned());
	
	//draw grid
	gridModel->bind(prog);
	glDrawArrays(GL_LINES, 0, gridModel->getVerticesAssigned());
	
	//full light affect, main model tranformed
	mat4 t = mat4::translation(translation);
	rootObj.bakeAll(t);
	glUniform1f(lightFractionLoc, 1.0f);
	cylModel->bind(prog);
	drawChain(rootObj, matrixScene, matLoc, cylModel->getVerticesAssigned());
	
	glutSwapBuffers();
	//int frameEndTime = glutGet(GLUT_ELAPSED_TIME);	
}

void idle() {
	int dt;
	int time = glutGet(GLUT_ELAPSED_TIME);
	if (lastIdleTime == -1) {
		dt = 1;
	}
	else {
		dt = time - lastIdleTime;
	}

	//fps independent animation
	if (dt > 0) {
		float dtf = dt / 1000.0f;
		float timef = time / 1000.0f;
		//translation
		float trc = 0.003f * dtf;
		if (keysDown['w']) translation.y += trc;
		else if (keysDown['s']) translation.y -= trc;
		
		if (keysDown['a']) translation.x -= trc;
		else if (keysDown['d']) translation.x += trc;
	
		if (keysDown['q']) translation.z += trc;
		else if (keysDown['e']) translation.z -= trc;

		Object* ob = &rootObj;
		int depth = 0;
		while (ob) {
			ob->rot = quat::flightRotation(sinf(timef * 1.3f) * 0.4f, cosf(timef * 0.3f) * 0.8f, sinf(timef * 0.2f) * 0.2f);
			if (ob->shoots.size() > 0) ob = ob->shoots[0];
			else break;
			depth++;
		}

		lastIdleTime = time;
	}

	//limit ~60fps
	if (time - lastDrawTime > 16) glutPostRedisplay();
}

void keyboardDown(unsigned char key, int x, int y) {
	//cout << "key down: " << key << endl;
	keysDown[key] = true;
	if (key == 0x1B) glutLeaveMainLoop(); //escape
}

void keyboardUp(unsigned char key, int x, int y) {
	//cout << "key up: " << key << endl;
	keysDown[key] = false;
}

void specDown(int key, int x, int y) {
	//keysDown[key] = true;
}

void specUp(int key, int x, int y) {
	//keysDown[key] = false;
}

void init()
{
	mainShader = new ShaderProgram(SHADER_ROOT_NAME ".vert", SHADER_ROOT_NAME ".frag");	
	cylModel = new VertexArrayObject<vertex>();
	arrowsModel = new VertexArrayObject<vertex>();
	gridModel = new VertexArrayObject<vertex>();

	//generate rope - along y axis
	vector<vertex> rope;
	generateCylinder(8, 2, vec3(cylWidth, cylHeight, cylWidth), true, rope);
	cylModel->assign(rope.size(), rope.data(), GL_STATIC_DRAW);

	//generate arrows - plain lines
	vertex arrows[6];
	arrows[0].v.set(0.0f, 0.0f, 0.0f); arrows[0].c.set(1.0f, 0.0f, 0.0f);
	arrows[1].v.set(1.0f, 0.0f, 0.0f); arrows[1].c.set(1.0f, 0.0f, 0.0f);
	arrows[2].v.set(0.0f, 0.0f, 0.0f); arrows[2].c.set(0.0f, 1.0f, 0.0f);
	arrows[3].v.set(0.0f, 1.0f, 0.0f); arrows[3].c.set(0.0f, 1.0f, 0.0f);
	arrows[4].v.set(0.0f, 0.0f, 0.0f); arrows[4].c.set(0.0f, 0.0f, 1.0f);
	arrows[5].v.set(0.0f, 0.0f, 1.0f); arrows[5].c.set(0.0f, 0.0f, 1.0f);
	arrowsModel->assign(6, arrows, GL_STATIC_DRAW);


	//hierarchy
	Object* at = &rootObj;
	for (int i = 0; i < 10; i++) {
		Object* ob = new Object();
		ob->stem.y = cylHeight;
		ob->rot = quat::flightRotation(0.2f, 0.5f, 0.1f);
		at->shoots.push_back(ob);
	
		at = ob;
	}

	//generate grid
	const int gl = 11;
	vertex grid[4*gl];

	float gs = 5.0f;
	for (int i = 0; i < gl; i++) {
		float x = -gs + 2 * gs * (float)i / (gl-1);
		grid[i * 2 + 0].v.set(x, 0.0f, -gs);
		grid[i * 2 + 1].v.set(x, 0.0f,  gs);
		
		grid[2*gl + i * 2 + 0].v.set(-gs, 0.0f, x);
		grid[2*gl + i * 2 + 1].v.set( gs, 0.0f, x);
	}
	for (int i = 0; i < 4*gl; i++) {
		grid[i].c.set(1.0f, 1.0f, 1.0f);
		grid[i].n.set(0.0f, 1.0f, 0.0f);
	}
	gridModel->assign(4*gl, grid, GL_STATIC_DRAW);
	
	//reset state
	memset(keysDown, 0, 256);
	translation.set(0.0f, 0.0f, 0.0f);

	glEnable(GL_CULL_FACE);
	glClearColor(0.3f, 0.4f, 0.2f, 1.0f);
	glClearDepth(1e5f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
}

int main(int argc, char** argv) {

	// Set up the window
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(WW, WH);
    glutCreateWindow("no more triangles");
	// Tell glut where the display function is
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboardDown);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(specDown);
	glutSpecialUpFunc(specUp);

	 // A call to glewInit() must be done after glut is initialized!
    GLenum res = glewInit();
	// Check for any errors
    if (res != GLEW_OK) {
      fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
      return 1;
    }
	// Set up your objects and shaders
	init();
	// Begin infinite event loop
	glutMainLoop();

	delete mainShader;
	delete cylModel;
	delete arrowsModel;
	delete gridModel;

	cout << "graceful exit" << endl;
    return 0;
}

