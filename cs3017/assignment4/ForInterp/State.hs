module State where

import Data.Map (Map)
import qualified Data.Map as Map

-- Program states are implemented using Haskell's Map datatype
type State = Map String Integer

-- Make and empty state
emptyState = Map.empty

-- Update the value of an existing identifier (or add a new identifier) in the state
stateUpdate :: State -> String -> Integer -> State
stateUpdate st ident val = Map.insert ident val st

-- Lookup the value of an identifier in a state
stateLookup :: State -> String -> Integer 
stateLookup st ident = case Map.lookup ident st of
                        Just i  -> i
                        Nothing -> error $ "looked up variable '" ++ ident ++ "' which was not in the state" 

showState :: State -> String
showState st = "[" ++ (Map.foldrWithKey 
                        (\key val str ->
                            key ++ "->" ++ (show val) ++
                            (if str=="]" then str else ", " ++ str))
                        "]" st)
