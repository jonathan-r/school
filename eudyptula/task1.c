#include <linux/module.h>
#include <linux/kernel.h>

static int __init hello_init(void) {
	printk(KERN_DEBUG "Hello World!\n");
	return 0;
}

static void __exit hello_cleanup(void) {
	printk(KERN_DEBUG "Bye World!\n");
}

module_init(hello_init);
module_exit(hello_cleanup);