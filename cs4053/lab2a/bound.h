#pragma once

#include <algorithm>
#include <limits>

template <typename T>
struct BoundT {
 	T min, max;
	BoundT() {
		max = 0.0;
		min = std::numeric_limits<T>::max();
	}

	void update(T x) {
		min = std::min(x, min);
		max = std::max(x, max);
	}

	T range() {
		return max - min;
	}

	T midPoint() {
		return min + range() / 2;
	}
};

typedef BoundT<double> Bound;