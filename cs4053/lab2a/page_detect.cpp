#include "pages.h"

using namespace std;
using namespace cv;

//D1, D2 need not be normalized
bool raysIntersect(const Point2f& p1, const Point2f& p2, const Point2f& D1, const Point2f& D2, Point2f& out) {
	//find intersections between any 2 non-parallel lines:
	//for every two rays (p + d), where p and d are 2d vectors and d is unit length
	//find some t1 and t2 such that p1 + d1*t1 = p2 + d2*t2
	//t2x = (p1x - p2x + t1*d1x) / d2x
	//t2y = (p1y - p2y + t1*d1y) / d2y
	//hence t1 = (d2x * (p1y - p2y) - d2y * (p1x - p2x)) / (dx1 * d2y - d1y * d2x)
	//and t2 = (p1 - p2 + t1*d1) / d2 (either dimension)

	const float eps = 0.001f;

	float d1LenSqr = D1.x * D1.x + D1.y * D1.y;
	float d2LenSqr = D2.x * D2.x + D2.y * D2.y;

	if (d1LenSqr < eps) return false; //d1 too short
	if (d2LenSqr < eps) return false; //d2 too short

	Point2f d1 = D1 / sqrtf(d1LenSqr);
	Point2f d2 = D2 / sqrtf(d2LenSqr);

	float divisor = (d1.x * d2.y - d1.y * d2.x);
	if (fabs(divisor) < eps) return false; //lines parallel

	Point2f dp = p1 - p2;
	float t1 = (d2.x * dp.y - d2.y * dp.x) / divisor;
	out = p1 + d1 * t1;
	return true;
}

void orderPointsNearest(const vector<Point2f>& points, vector<Point2f>& ordered, vector<float>& dists) {
	ordered.clear();
	dists.clear();
	vector<int> pointingToIdx(points.size(), -1);

	int chainStart = 0;
	int at = 0;
	for (int proc = 0; proc < points.size(); proc++) {
		const Point2f& p1 = points[at];

		//find the closest other point
		float shortest = numeric_limits<float>::max();
		int si = -1;
		for (int idx2 = 0; idx2 < points.size(); idx2++) {
			if (idx2 != at and ((pointingToIdx[idx2] == -1) or proc == points.size()-1)) {
				const Point2f& p2 = points[idx2];
				Point2f diff = (p2 - p1);

				float dist = sqrtf(diff.x * diff.x + diff.y * diff.y);
				if (dist < shortest) {
					shortest = dist;
					si = idx2;
				}
			}
		}

		assert(si >= 0 and si < points.size());
		const Point2f& p2 = points[si];
		Point2f diff = (p2 - p1);
		float dist = sqrtf(diff.x * diff.x + diff.y * diff.y);

		//if the start of the chain is closer to the closest point we found, reverse the order
		/*if (proc > 0) {
			const Point2f& cs = points[chainStart];
			Point2f csd = p2 - cs;
			float csdist = sqrtf(csd.x * csd.x + csd.y * csd.y);
			if (csdist < dist) {
				printf("reverse order\n");
				//reverse order
				int follow = at;
				int next = pointingToIdx[at];

				pointingToIdx[at] = -1;
				while (follow != chainStart) {
					int after = pointingToIdx[next];
					pointingToIdx[next] = follow;
					follow = next;
					next = after;
				}

				chainStart = at;
				at = follow;
				continue;
			}
		}*/

		pointingToIdx[si] = at;

		ordered.emplace_back(p1);
		dists.push_back(dist);

		at = si; //follow
	}
}

bool extractPage(const Mat& bluePixels, double thresh, double normalizeVal, int dilations, int histBins, const Mat& testImg, float outSize, Mat& isolatedSquarePage, Mat* debugImg) {
	Mat sourceImg = testImg.clone();

	int bins[] = {histBins, histBins, histBins};
	float histRange[] = {0, 255};
	const int channels[] = {0, 1, 2};
	const float* hr[] = {histRange, histRange, histRange};

	//printf("3d blue histogram compute\n");
	MatND blueHist;
	calcHist(&bluePixels, 1, channels, Mat(), blueHist, 3, bins, hr, true, false);
	
	//printf("3d blue histogram normalize\n");
	normalize(blueHist, blueHist, normalizeVal);

	//printf("back projection\n");
	Mat backproj;
	calcBackProject(&sourceImg, 1, channels, blueHist, backproj, hr, 255.0, true);

	//printf("dilation\n");
	dilate(backproj, backproj, Mat(), Point(-1, -1), dilations);
	//morphologyEx(backproj, backproj, MORPH_CLOSE, Mat::ones(5, 5, CV_32F), Point(2, 2), 3);

	//imshow(windowName, backproj);

	//printf("threshold %.2f\n", thresh);
	Mat backprojThresh;
	threshold(backproj, backprojThresh, thresh, 255, THRESH_BINARY);// | THRESH_OTSU);

	//imshow(windowName, backprojThresh);

	//printf("contours\n");
	Mat hierarchy;
	vector<vector<Point>> contours;
	findContours(backprojThresh, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	//printf("contour sizes and histogram\n");
	vector<float> contourRads(contours.size());
	vector<Point2f> contourCenters(contours.size());
	
	Bound radBound;
	for (int i = 0; i < contours.size(); i++) {
		Point2f& center = contourCenters[i];
		auto& points = contours[i];
		float& rad = contourRads[i];

		//area = contourArea(contours[i]);
		minEnclosingCircle(contours[i], center, rad);
		radBound.update(rad);
	}

	Scalar mean, sd;
	meanStdDev(contourRads, mean, sd);
	//printf("mean %.2f, stddev %.2f\n", mean[0], sd[0]);

	float range = radBound.range();
	int buckets = 2 + radBound.max/10;
	vector<int> contourRadsHist(buckets+1, 0); //zero-filled
	//printf("%d contours, area min %.2f, max %.2f, %d buckets\n", contours.size(), radBound.min, radBound.max, buckets);

	for (int i = 0; i < contours.size(); i++) {
		int idx = (int)((contourRads[i] * buckets) / radBound.max);
		contourRadsHist[idx]++;
	}

	//find spike
	int spikeAt = 0;
	int spikeSize = contourRadsHist[0];
	for (int i = 0; i < buckets; i++) {
		//printf("%d ", contourRadsHist[i]);
		if (contourRadsHist[i] > spikeSize) {
			spikeSize = contourRadsHist[i];
			spikeAt = i;
		}
	}

	float modeRad = (((float)spikeAt + 0.5f) / buckets) * radBound.max;
	//printf("fullest bucket rad: %.2f, %d items\n", modeRad, spikeSize);

	//direction overlay
	Scalar crossc = Scalar(50, 50, 55);
	Scalar color = Scalar(0, 0, 255);

	float cs = 200.0f;
	Point2f c(200, 200);
	if (debugImg) {
		line(*debugImg, Point(c + Point2f(0, -cs)), Point(c + Point2f(0, cs)), crossc, 1, CV_AA, 0);
		line(*debugImg, Point(c + Point2f(-cs, 0)), Point(c + Point2f(cs, 0)), crossc, 1, CV_AA, 0);
	}

	//vector<pair<Point2f, Point2f>> segs[2]; //left, right
	vector<Point2f> orderedPoints;
	vector<float> distPoints;

	orderPointsNearest(contourCenters, orderedPoints, distPoints);

	//find segment length median, mean and standard deviation
	vector<int> distOrder(orderedPoints.size()); //order segment distances
	for (int i = 0; i < distOrder.size(); i++) distOrder[i] = i; //generate indices

	sort(distOrder.begin(), distOrder.end(), [&](const int& i, const int& j) ->  bool {
		return (distPoints[i] > distPoints[j]);
	});

	float segLenMed = distPoints[distOrder[distOrder.size()/2]];
	//printf("segment length median: %.2f\n", segLenMed);

	Scalar segLenMean, segLenSD;
	meanStdDev(distPoints, segLenMean, segLenSD);
	//printf("segment length mean %.2f, stddev %.2f\n", segLenMean[0], segLenSD[0]);

	//remove outliers, try a few standard deviation coefficients, high to low, until the kmeans clusters have similar length and are about 90 degrees apart
	Point2f diffCenters[2]; //(dx, dy)
	Point2f diffCentersPolar[2]; //(arg, len)
	vector<Point2f> alignedDiffPoints;
	vector<Point2f> diffOriginPoints;
	Point2f lastPoint;
	Mat diffLabels, diffCentersMat;

	bool accept = false;
	for (float sdThresh = 3.0f; sdThresh >= 0.1f; sdThresh -= 0.2f) {
		alignedDiffPoints.clear();
		diffOriginPoints.clear();

		for (int i = 0; i < distPoints.size()-1; i++) {
			Point2f& p1 = orderedPoints[i];
			Point2f& p2 = orderedPoints[i+1];

			if (fabs(distPoints[i] - segLenMed) > segLenSD[0] * sdThresh) {
				//outlier
				Point2f& p1 = orderedPoints[i];
				Point2f& p2 = orderedPoints[i+1];
				//if (debugImg) line(*debugImg, Point(p1), Point(p2), Scalar(0, 80, 255), 5, CV_AA, 0);
			}

			else {
				Point2f diff = p2 - p1;

				if (fabs(diff.y) < 0.001f) {
					//horizontal, ensure points to the right
					if (diff.x < 0.0f) {
						diff = -diff;
						diffOriginPoints.emplace_back(p2);
					}
					else {
						diffOriginPoints.emplace_back(p1);
					}
				}
				else if (diff.y > 0) {
					//not horizontal, ensure points up-ish
					diff = -diff;
					diffOriginPoints.emplace_back(p2);

				}
				else {
					diffOriginPoints.emplace_back(p1);
				}

				alignedDiffPoints.emplace_back(diff);
				lastPoint = p2;

				//if (debugImg) line(*debugImg, Point(p1), Point(p2), Scalar(0, 127, 127), 3, CV_AA, 0);
				//if (debugImg) circle(*debugImg, Point(p1), 2.0f, Scalar(255, 80, 0), 2);
			}
		}

		diffOriginPoints.emplace_back(lastPoint);
		//if (debugImg) circle(*debugImg, Point(lastPoint), 2.0f, Scalar(255, 80, 0), 2);

		kmeans(alignedDiffPoints, 2, diffLabels, TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 100, 0.001f), 20, KMEANS_PP_CENTERS, diffCentersMat);

		for (int i = 0; i < 2; i++) {
			Point2f& p = diffCenters[i];
			p = Point2f(diffCentersMat.at<float>(i, 0), diffCentersMat.at<float>(i, 1));
			diffCentersPolar[i] = Point2f(atan2(p.y, p.x), sqrtf(p.x * p.x + p.y * p.y));
		}

		//termination check - see if the diffCenters are similar in length and are roughly orthogonal to each other
		float argOrthoDiff = fabs(M_PI/2 - fabs(diffCentersPolar[0].x - diffCentersPolar[1].x));
		float lenRatioDiff = fabs(1.0f - fabs(diffCentersPolar[0].y / diffCentersPolar[1].y));
		//printf("sd thresh = %.2f, arg diff: %.2f, len diff: %.2f\n", sdThresh, argOrthoDiff, lenRatioDiff);

		if (lenRatioDiff < 0.2f and argOrthoDiff < 0.2f) {
			accept = true;
			break;
		}
	}

	if (accept) {
		BoundT<float> extremesSide[2];
		Point2f sideDirs[2];
		Point2f sideTans[2];
		float sideTansSelfDot[2];
		vector<Point2f> linePoints[4];
		vector<pair<Point2f, Point2f>> fitted(4); //point, dir

		for (int i = 0; i < 2; i++) {
			//printf("cluster %d, (%.2f, %.2f)\n", i, diffCenters[i].x, diffCenters[i].y);
			sideDirs[i] = Point2f(cos(diffCentersPolar[i].x), sin(diffCentersPolar[i].x));
			sideTans[i] = Point2f(-sideDirs[i].y, sideDirs[i].x);
			sideTansSelfDot[i] = sideTans[i].x * sideTans[i].x + sideTans[i].y * sideTans[i].y;
			if (debugImg) circle(*debugImg, Point(c + diffCenters[i]), 10.0f, Scalar(0, 255, 0), 2);
		}

		//find the tangent axis extremes
		for (int si = 0; si < 2; si++) {
			Point2f& sideTan = sideTans[si];
			for (int i = 0; i < diffOriginPoints.size(); i++) {
				Point2f& p = diffOriginPoints[i];
				float along = (p.x * sideTan.x + p.y * sideTan.y) / sideTansSelfDot[si]; //project on tangent axis
				if (debugImg) drawText(*debugImg, Point(p + Point2f(20, -10 + si*20)), "%.2f", along); //overlay at 1/3 of the way along the segment
				extremesSide[si].update(along);
			}
		}

		float midSide[2];
		midSide[0] = extremesSide[0].midPoint();
		midSide[1] = extremesSide[1].midPoint();

		Scalar colors[4];
		colors[0] = Scalar(0, 0, 255);
		colors[1] = Scalar(0, 200, 220);
		colors[2] = Scalar(255, 0, 0);
		colors[3] = Scalar(180, 200, 0);

		for (int i = 0; i < alignedDiffPoints.size(); i++) {
			Point2f& p = diffOriginPoints[i];
			Point2f& diff = alignedDiffPoints[i];
			int label = diffLabels.at<int>(i);
			assert(label == 0 or label == 1);

			Point2f& sideTan = sideTans[label];
			float along = (p.x * sideTan.x + p.y * sideTan.y) / sideTansSelfDot[label]; //project on tangent axis

			int lineId = (along < midSide[label]) * 2 + label;
			linePoints[lineId].emplace_back(p);

			if (debugImg) circle(*debugImg, Point(p), 8.0f, colors[lineId], 2);
		}

		//try to fit as many lines as possible
		for (int i = 0; i < 4; i++) {
			Point2f& p = fitted[i].first;
			Point2f& d = fitted[i].second;

			if (linePoints[i].size() >= 2) {
				//best case - use actual points
				Vec4f vp;
				fitLine(linePoints[i], vp, CV_DIST_L2, 0.0, 0.01, 0.01);
				
				p = Point2f(vp[2], vp[3]);
				d = Point2f(vp[0], vp[1]);

				if (debugImg) line(*debugImg, Point(p), Point(p + d * 1000), Scalar(255, 255, 255), 2, CV_AA, 0);
				if (debugImg) line(*debugImg, Point(p), Point(p - d * 1000), Scalar(255, 255, 255), 2, CV_AA, 0);
				if (debugImg) drawText(*debugImg, Point(p), "%d", i); //overlay at 1/3 of the way along the segment
			}
			else {
				//fallback - use extremes on tangents
				int lower = i >> 2;
				int label = i & 1;

				p = sideTans[label] * ((lower)? extremesSide[label].max : extremesSide[label].min);
				d = sideDirs[label];

				if (debugImg) line(*debugImg, Point(p), Point(p + d * 1000), Scalar(0, 255, 255), 2, CV_AA, 0);
				if (debugImg) line(*debugImg, Point(p), Point(p - d * 1000), Scalar(0, 255, 255), 2, CV_AA, 0);

				//printf("missing side %d, lower %d, label %d, p = (%.2f %.2f), d = (%.2f %.2f)\n", i, lower, label, p.x, p.y, d.x, d.y);
			}
		}

		//intersect lines
		vector<Point2f> corners;
		for (int i = 0; i < 4; i++) {
			int j = (i + 1) % 4;

			Point2f intersect;
			bool hasIntersection = raysIntersect(fitted[i].first, fitted[j].first, fitted[i].second, fitted[j].second, intersect);
			assert(hasIntersection);

			if (debugImg) circle(*debugImg, Point(intersect), 13.0f, Scalar(255, 0, 0), 2);
			if (debugImg) drawText(*debugImg, Point(intersect + Point2f(-50, 0)), "%d", i); //overlay at 1/3 of the way along the segment
			corners.emplace_back(intersect);
		}

		assert(corners.size() == 4);
		//now we have 4 corners, do a geometric transformation
		vector<Point2f> cmpCorners;
		cmpCorners.emplace_back(0.0f, 0.0f);
		cmpCorners.emplace_back(outSize, 0.0f);
		cmpCorners.emplace_back(outSize, outSize);
		cmpCorners.emplace_back(0.0f, outSize);

		Mat perspectiveTransform = getPerspectiveTransform(corners.data(), cmpCorners.data());
		warpPerspective(sourceImg, isolatedSquarePage, perspectiveTransform, Size(outSize, outSize), INTER_LANCZOS4);

		return true;
	}

	return false;
}