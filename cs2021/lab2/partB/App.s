	AREA	AsmTemplate, CODE, READONLY
	IMPORT	main

; sample pressing one of the switches makes the "next" of the 4 LEDs P1.16, P1.17, P1.18, P1.19
; go on and off in sequence
; (c) Mike Brady, 2012.

; this program sample is only partically complete:
; it does not wait for the end of the switch press

	EXPORT	start
start

IO1DIR	EQU	0xE0028018
IO1SET	EQU	0xE0028014
IO1CLR	EQU	0xE002801C
IO1PIN	EQU	0xE0028010

		ldr	r1,=IO1DIR
		ldr	r2,=0x000f0000	;select P1.19--P1.16
		str	r2,[r1]			;make them outputs
		ldr	r1,=IO1SET
		str	r2,[r1]			;set them to turn the LEDs off
		ldr	r2,=IO1CLR

; r1 points to the SET register
; r2 points to the CLEAR register

		ldr	r5,=0x00100000	; end when the mask reaches this value
wloop	ldr	r3,=0x00010000	; start with P1.16.
floop	str	r3,[r2]	   		; clear the bit -> turn on the LED

		ldr	r0,=IO1PIN
repeek
		ldr	r6,[r0]
		ldr	r7,=0x00F00000	; mask for switch bits
		and	r6,r6,r7
		cmp	r6,r7			; if any switch is pressed, at least one of the remaining bits in r6 will be zero
		beq	repeek

repeek2
		ldr	r6,[r0]
		ldr	r7,=0x00F00000	; mask for switch bits
		and	r6,r6,r7
		cmp	r6,r7			; if any switch is pressed, at least one of the remaining bits in r6 will be zero
		bne	repeek2	


;delay for about a half second
		ldr	r4,=6000000
dloop	subs	r4,r4,#1
		bne	dloop

		str	r3,[r1]			;set the bit -> turn off the LED
		mov	r3,r3,lsl #1	;shift up to next bit. P1.16 -> P1.17 etc.
		cmp	r3,r5
		bne	floop
		b	wloop

stop	B	stop

	END