#!/usr/bin/python3
import argparse

#returns (resulting value, proc calls, maximum window depth, overflows, underflows)
def ackermann(x, y, num_sets, show_state):
	reg_windows = [None for i in range(num_sets)] #fixed on-die register file; contains frame depth
	offloaded = [] #memory stack of offloaded num_sets
	cwp = num_sets - 1
	swp = num_sets - 1

	overflows = 0
	underflows = 0
	calls = 0

	def enter_frame(depth):
		nonlocal cwp, swp, calls, offloaded, reg_windows, overflows, show_state

		if (cwp - 2) % num_sets == swp:
			offloaded.append(reg_windows[swp])
			reg_windows[swp] = None
			swp = (swp - 1) % num_sets
			overflows += 1

		cwp = (cwp - 1) % num_sets
		reg_windows[cwp] = depth
		calls += 1

		if show_state:
			print('call, depth %d, cwp=%d, swp=%d, on_die: %s, stack: %s' % (depth, cwp, swp, reg_windows, offloaded))


	def return_frame(depth):
		nonlocal cwp, swp, calls, offloaded, reg_windows, underflows, show_state

		if (cwp + 2) % num_sets == swp and len(offloaded) > 0:
			swp = (swp + 1) % num_sets
			reg_windows[swp] = offloaded.pop()
			underflows += 1

		reg_windows[cwp] = None #mark as free
		cwp = (cwp + 1) % num_sets

		if show_state:
			print(' ret, depth %d, cwp=%d, swp=%d, on_die: %s, stack: %s' % (depth, cwp, swp, reg_windows, offloaded))


	#returns (result, deepest frame reached)
	def f(x, y, depth):
		#;r26 -> x
		#;r27 -> y
		#;r1 <- result

		#ackermann:
			#add   r0, r27, r0 {c}
			#jmp   ne, ackermann_else_if
			#add   r27, #1, r1       ;will be rewritten on branch
			#ret   r31, 0

		if x == 0:
			return_frame(depth)
			return (y+1, depth) #one frame
		

		#ackermann_else_if:
			#add   r0, r27, r0 {c}   ;inoffensive after ret
			#jmp   ne, ackermann_else
			#sub  r26, #1, r26       ;tail call
			#add  r27, #1, r27
			#jmp  alw, ackermann_tail

		elif y == 0:
			####recursive call optimized to tail call (don't enter_frame and don't return_frame)
			enter_frame(depth+1)
			(v, deepest) = f(x-1, 1, depth+1)
			return_frame(depth)
			return (v, deepest)

		#ackermann_else:
			#add   r0, r10, r16      ;save x-1, inoffensive after callr
			#add   r0, r26, r10      ;x
			#sub   r27, #1, r11      ;y-1
			#callr ackermann, r15
			#add   r0, r1, r11       ;result of above as 2nd arg, inoffensive
			#add   r0, r16, r10
			#callr ackermann, r15
			#add   r0, r0, r0        ;can't do anthing useful after window switch
			#ret   r31, 0
			#add   r0, r0, r0        ;nop safeguard

		else:
			#ackermann(x, y-1) call
			enter_frame(depth+1)
			(v1, deepest1) = f(x, y-1, depth+1)

			#ackermann(x-1, ackermann(x, y-1)) call
			enter_frame(depth+1)
			(v2, deepest2) = f(x-1, v1, depth+1)

			return_frame(depth)
			return (v2, max([deepest1, deepest2]))

	reg_windows[-1] = 0 #calling function has depth 0
	enter_frame(1)
	(val, deepest) = f(x, y, 1)
	print(offloaded)
	return (val, calls, deepest, overflows, underflows)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Simulate running the ackermann function on a RISC-I machine.')
	parser.add_argument('x', metavar='x', type=int, nargs=1, help='1st argument to the ackermann function call')
	parser.add_argument('y', metavar='y', type=int, nargs=1, help='2nd argument to the ackermann function call')
	parser.add_argument('sets', metavar='num_sets', type=int, nargs=1, help='number of register sets on die')
	parser.add_argument('--show-windows', dest='show', action="store_true", 
		help='print state of the register file and window stack after each call and ret')

	args = parser.parse_args()
	(result, calls, most_frames, overflows, underflows) = ackermann(args.x[0], args.y[0], args.sets[0], args.show)
	print('ackermann(%d, %d) = %d' % (args.x[0], args.y[0], result))
	print('recursive calls: %d' % calls)
	print('deepest frame: %d' % most_frames)
	print('overflows: %d, underflows: %d' % (overflows, underflows))

