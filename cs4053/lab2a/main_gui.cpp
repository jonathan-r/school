#include "pages.h"

using namespace std;
using namespace cv;

//global data
Mat bluePixels, testImg;
char windowName[256];

int lastImgId = -1;
int trackImgId = 1;
const int seqStartIdx = 1; //minimum

NameTemplate* testNames;
vector<Mat> referenceImgs;

int main(int argc, char** argv) {
	if (argc != 3) {
		printf("usage: pages <page_img> <test_img>\n"
				"Where page_img should be a path pattern for all the known pages to test against,\n"
				"while test_img should be a path pattern for pictures of the pages to test.\n"
				"Any of the arguments may be a single image or may contain a sequence of '?' characters,\n"
				"which represents a sequence of images,\n"
				"with the ?s being replaced by a sequence number starting with 1.\n"
				"The number of ?s represents the number of digits in the squence number,\n"
				"so that leading 0s are added when needed.\n");
		return 0;
	}

	//prcess train and test arguments
	NameTemplate pageNames(argv[1]);
	testNames = new NameTemplate(argv[2]);

	sprintf(windowName, "pages");
	namedWindow(windowName, WINDOW_AUTOSIZE);

	loadTemplates(pageNames, referenceImgs);
	printf("found %d reference images\n", referenceImgs.size());
	bluePixels = imread("blue.png");

	createTrackbar(UI_IMG_ID, windowName, &trackImgId, 50, trackbarChange, NULL);
	trackbarChange(0, NULL);

	waitKey(0);
	//destroyAllWindows();

	return 0;
}

void trackbarChange(int _, void* userData) { //don't use the position arg
	if (trackImgId == 0) trackImgId = 1;
	if (trackImgId != lastImgId) {
		string testName = testNames->makeImgName(trackImgId);
		printf("loading test image%s\n", testName.c_str());
		testImg = imread(testName);
		lastImgId = trackImgId;
	}

	Mat debugImg = testImg.clone();

	int idx;
	bool matched = matchPage(testImg, referenceImgs, bluePixels, idx, &debugImg);
	resize(debugImg, debugImg, Size(1024, 1024 * ((float)testImg.rows / testImg.cols)));

	if (matched) {
		idx++; //1 based;
		drawText(debugImg, Point(500, 50), "matched with page %d (%s)", idx, (idx == getGroundTruth(trackImgId))? "right" : "wrong");
	}
	else {
		drawText(debugImg, Point(500, 50), "found no page");
	}

	imshow(windowName, debugImg);
}
