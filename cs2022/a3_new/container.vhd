library ieee;
use ieee.std_logic_1164.all;

entity container is port( 
	clock, reset : in std_logic );
end entity;

architecture behavioral of container is
	component register_file is port ( 
		write_reg   : in std_logic;
		dest_select : in std_logic_vector (3 downto 0);
		data_in     : in std_logic_vector (15 downto 0);
		clk, reset  : in std_logic;
		a_select, b_select : in std_logic_vector (3 downto 0);
		a_out, b_out : out std_logic_vector (15 downto 0) );
	end component;
	
	component function_unit is port ( 
		a, b : in  std_logic_vector (15 downto 0);
		func : in  std_logic_vector (4 downto 0);
		v, c, n, z : out std_logic;
		f : out  std_logic_vector (15 downto 0) );
	end component;
	
	component mux2_16bit is port ( 
		in0, in1 : in std_logic_vector(15 downto 0);
		addr : in std_logic;
		output : out std_logic_vector(15 downto 0) );
	end component;
	
	component control_memory is port ( 
		car : in std_logic_vector(7 downto 0);
		mw, mm, rw, md : out std_logic;
		func : out std_logic_vector(4 downto 0);
		mb, tb, ta, td, pl, pi, il, mc, sw : out std_logic;
		ms : out std_logic_vector(2 downto 0);
		na : out std_logic_vector(7 downto 0) );
	end component;
	
	component memory is port( 
		address, write_data : in std_logic_vector (15 downto 0);
		memwrite : in std_logic; 
		read_data : out std_logic_vector (15 downto 0) );
	end component;
	
	component register_16bit is port ( 
		load, clk, reset : in std_logic;
		d    : in std_logic_vector (15 downto 0);
		q    : out std_logic_vector (15 downto 0) );
	end component;
	
	component pc_reg is port (
		load, increment : in std_logic;
		offset : in std_logic_vector(15 downto 0);
		clock, reset : in std_logic;
		output : inout std_logic_vector(15 downto 0) );
	end component;
	
	component ca_reg is port (
		addr : in std_logic_vector(7 downto 0);
		branch : in std_logic;
		clock, reset : in std_logic;
		output : inout std_logic_vector(7 downto 0) );
	end component;
	
	component instruction_register is port(
		input : in std_logic_vector(15 downto 0);
		il : in std_logic;
		clock, reset : in std_logic;
		opcode : out std_logic_vector(6 downto 0);
		dr, sa, sb : out std_logic_vector(2 downto 0) );
	end component;
	
	component mux2_8bit is port(
		in0 : in  std_logic_vector(7 downto 0);
		in1 : in  std_logic_vector(7 downto 0);
		addr : in  std_logic;
		output : out  std_logic_vector(7 downto 0) );
	end component;
	
	component mux8_1bit port(
		input : in std_logic_vector(7 downto 0);
		selection : in std_logic_vector(2 downto 0);
		output : out std_logic );
	end component;
	
	--left side
	signal pc_offset : std_logic_vector(15 downto 0); --ir to pc
	signal pc_out : std_logic_vector(15 downto 0); --pc to mux_m
	
	signal opcode : std_logic_vector(6 downto 0); --ir and constant '0' to mux_c
	
	signal mc_in1 : std_logic_vector(7 downto 0); --ir and constant '0' to mux_c
	
	signal ms_in : std_logic_vector(7 downto 0); --status(v, c, n, z) and contstants to mux_s
	signal ms_out : std_logic; --mux_s to car
	signal mc_out : std_logic_vector(7 downto 0); --mux_c to car
	
	signal car_out : std_logic_vector(7 downto 0); --car to control memory
	
	--control memory outputs
	signal na : std_logic_vector (7 downto 0);
	signal ms : std_logic_vector (2 downto 0);
	signal sw, mc, il, pi, pl, mb, md, rw, mm, mw : std_logic;
	signal func : std_logic_vector(4 downto 0);
	
	--right side
	signal reg_out_a : std_logic_vector (15 downto 0); --regfile a to function unit
	signal reg_out_b : std_logic_vector (15 downto 0); --regfile b mux_b(0)
	
	signal mb_out : std_logic_vector (15 downto 0); --mux_b to alu and data_out
	signal md_out : std_logic_vector(15 downto 0); --mux_d to regfile data in
	signal mm_out : std_logic_vector(15 downto 0); --mux_m to memory
	signal mem_out : std_logic_vector(15 downto 0); --memory to mux_d and ir
	
	signal f_out : std_logic_vector(15 downto 0); --function unit to mux_d
	signal v, c, n, z : std_logic; --function unit to mux_s
	signal status_in, status_out : std_logic_vector(15 downto 0);
	
	signal tddr, tasa, tbsb : std_logic_vector(3 downto 0); --control mem and ir to register file
	signal const : std_logic_vector(15 downto 0); --ir to mux_b

begin
	--extend opcode to 8 bits, as it goes into mc_mux
	mc_in1 <= '0' & opcode;
	
	--wire each bit of mux_s
	ms_in(5 downto 0) <= status_out(3 downto 0) & '1' & '0';
	ms_in(6) <= not status_out(0) after 1ns; --not c
	ms_in(7) <= not status_out(2) after 1ns; --not z
	
	--wire status register input
	status_in <= (0 to 11 => '0') & n & z & v & c;
	
	--zero fill sb into the immediate value
	const <= (0 to 12 => '0') & tbsb(2 downto 0);
	
	--combine and sign extend dr and sb into pc branch offset
	pc_offset <= (0 to 9 => tddr(2)) & tddr(2 downto 0) & tbsb(2 downto 0);
	
	--left side
	pc : pc_reg port map(pl, pi, pc_offset, clock, reset, pc_out);
	ir : instruction_register port map(mem_out, il, clock, reset, opcode, tddr(2 downto 0), tasa(2 downto 0), tbsb(2 downto 0));
	
	mux_s : mux8_1bit port map(ms_in, ms, ms_out);
	mux_c : mux2_8bit port map(na, mc_in1, mc, mc_out);
	car : ca_reg port map(mc_out, ms_out, clock, reset, car_out);
	control_mem : control_memory port map(car_out, mw, mm, rw, md, func, mb, tddr(3), tasa(3), tbsb(3), pl, pi, il, mc, sw, ms, na);
	
	--right side
	reg_file : register_file port map(rw, tddr, md_out, clock, reset, tasa, tbsb, reg_out_a, reg_out_b);
	mux_b : mux2_16bit port map(reg_out_b, const, mb, mb_out);
	mux_m : mux2_16bit port map(reg_out_a, pc_out, mm, mm_out);
	
	status_reg : register_16bit port map(sw, clock, reset, status_in, status_out);
	
	functional : function_unit port map(reg_out_a, mb_out, func, v, c, n, z, f_out);
	mem : memory port map(mm_out, mb_out, mw, mem_out);
	mux_d : mux2_16bit port map(f_out, mem_out, md, md_out);
	
end behavioral;

