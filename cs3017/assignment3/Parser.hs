module Parser ( parseExp ) where

import Syntax
import State

-- parser-specific modules
import Control.Monad.Identity
import Text.Parsec hiding ( try, State )
import Text.Parsec.Expr
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (javaStyle)

import qualified Data.Map as Map
import Prelude hiding ( catch )

-- The lexer
langLex :: P.GenLanguageDef String () Identity
langLex = javaStyle{  P.opStart         = oneOf "+-/"
                    , P.opLetter        = oneOf "+-/>"
                    , P.reservedOpNames = ["+", "-", "/"]
                    , P.reservedNames   = ["throw", "try", "catch", "with", "->"]
                    }

lexer :: P.GenTokenParser String () Identity
lexer       = P.makeTokenParser langLex

parens :: ParsecT String () Identity a -> ParsecT String () Identity a 
parens      = P.parens lexer

angles :: ParsecT String () Identity a -> ParsecT String () Identity a
angles = P.angles lexer

brackets :: ParsecT String () Identity a -> ParsecT String () Identity a
brackets = P.brackets lexer

identifier :: ParsecT String () Identity String
identifier  = P.identifier lexer

integer :: ParsecT String () Identity Integer
integer     = P.integer lexer

comma :: ParsecT String () Identity String
comma = P.comma lexer

commaSep :: ParsecT String () Identity a -> ParsecT String () Identity [a]
commaSep = P.commaSep lexer

reservedOp :: String -> ParsecT String () Identity ()
reservedOp  = P.reservedOp lexer

throw :: ParsecT String () Identity ()
throw       = P.reserved lexer "throw"

try :: ParsecT String () Identity ()
try         = P.reserved lexer "try"

catch :: ParsecT String () Identity ()
catch       = P.reserved lexer "catch"

with :: ParsecT String () Identity ()
with        = P.reserved lexer "with"

arrow :: ParsecT String () Identity ()
arrow       = P.reserved lexer "->"


-- Build the arithmetic expression parser using Parsec's automation
expSyntax :: Parsec String () Exp
expSyntax = buildExpressionParser expOpTable expTerm
        <?> "expression"

expTerm :: Parsec String () Exp
expTerm = parens expSyntax
      <|> do i <- integer
             return ( CNum i )
      <|> do i <- identifier
             return ( Var i )
      <|> do throw
             n <- integer
             return ( Throw n )
      <|> do try
             e1 <- expTerm
             catch
             l  <- identifier
             with
             e2 <- expTerm
             return ( TryCatch e1 l e2 )
      <?> "simple arithmetic expression"

stateTerm :: ParsecT String () Identity [(String, Integer)]
stateTerm = commaSep $ do l <- identifier
                          arrow
                          n <- integer
                          return (l, n)


expOpTable :: OperatorTable String () Identity Exp
expOpTable = [ [binary "/" Div AssocLeft ]
             , [binary "+" Add AssocLeft, binary "-" Sub AssocLeft ] ]

-- Helper functions
binary :: String -> (a -> a -> a) -> Assoc -> Operator String () Identity a
binary name fun  = Infix (do{ reservedOp name; return fun }) 


-- Functions to run the parser
parseExp :: SourceName -> String -> Either ParseError (Exp, State)
parseExp = parse parseTerm 

parseTerm :: ParsecT String () Identity (Exp, State)
parseTerm = angles $ do e <- expSyntax
                        _ <- comma
                        s <- brackets stateTerm
                        return ( e, Map.fromList s )
  
-- for debugging
--run :: Show a => Parsec String () a -> String -> IO ()
--run p input = case parse p "" input of
--                Left err -> do{ putStr "parse error at "
--                              ; print err }
--                Right x  -> print x