/******************************************************************************/
/* RETARGET.C: 'Retarget' layer for target-dependent low level functions      */
/******************************************************************************/
/* This file is part of the uVision/ARM development tools.                    */
/* Copyright (c) 2005-2007 Keil Software. All rights reserved.                */
/* This software may only be used under the terms of a valid, current,        */
/* end user licence from KEIL for a compatible version of KEIL software       */
/* development tools. Nothing else gives you the right to use this software.  */
/******************************************************************************/

#include <stdio.h>
//#include <rt_misc.h> //no external references in this file
#pragma import(__use_no_semihosting_swi)

extern int sendchar(int ch);  /* in serial.c */

struct __FILE { int handle; /* Add whatever you need here */ };
FILE __stdout;

int fputc(int ch, FILE *f) {
  return (sendchar(ch));
}

//int ferror(FILE *f) {
  /* Your implementation of ferror */
//  return EOF;
//}


void _ttywrch(int ch) {
  sendchar(ch);
}

//used to be _sys_exit
void _exit(int return_code) {
label:  goto label;  /* endless loop */
}


//https://sites.google.com/site/stm32discovery/open-source-development-with-the-stm32-discovery/getting-newlib-to-work-with-stm32-and-code-sourcery-lite-eabi
#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>


#undef errno
extern int errno;

int _close(int file) {
	return -1;
}

/*
 lseek
 Set position in a file. Minimal implementation:
 */
int _lseek(int file, int ptr, int dir) {
	return 0;
}

/*
 isatty
 Query whether output stream is a terminal. For consistency with the other minimal implementations,
 */
int _isatty(int file) {
	switch (file){
	case STDOUT_FILENO:
	case STDERR_FILENO:
	case STDIN_FILENO:
		return 1;
	default:
		//errno = ENOTTY;
		errno = EBADF;
		return 0;
	}
}

/*
 sbrk
 Increase program data space.
 Malloc and related functions depend on this
 */
caddr_t _sbrk(int incr) {
	static char* Heap_Mem; //defined in LPC2400.s
	return (caddr_t) Heap_Mem;
	/*
	char *prev_heap_end;

	if (heap_end == 0) {
		heap_end = &_ebss;
	}
	prev_heap_end = heap_end;

	char * stack = (char*) __get_MSP();
	 if (heap_end + incr >  stack)
	 {
		 _write (STDERR_FILENO, "Heap and stack collision\n", 25);
		 errno = ENOMEM;
		 return  (caddr_t) -1;
		 //abort ();
	 }

	heap_end += incr;
	*/
}


/*
 read
 Read a character to a file. `libc' subroutines will use this system routine for input from all files, including stdin
 Returns -1 on error or blocks until the number of characters have been read.
 */

int _read(int file, char *ptr, int len) {
	int n;
	int num = 0;
	switch (file) {
		case STDIN_FILENO:
			for (n = 0; n < len; n++) {
				*ptr++ = getkey();
				num++;
			}
			break;

		default:
			errno = EBADF;
			return -1;
	}
	return num;
}


/*
 write
 Write a character to a file. `libc' subroutines will use this system routine for output to all files, including stdout
 Returns -1 on error or number of bytes sent
 */
int _write(int file, char *ptr, int len) {
	int n;
	switch (file) {
		case STDOUT_FILENO: /*stdout*/
		case STDERR_FILENO: /* stderr */
			for (n = 0; n < len; n++) {
				sendchar(*ptr++);
			}
			break;
		default:
			errno = EBADF;
			return -1;
	}

	return len;
}


/*
 fstat
 Status of an open file. For consistency with other minimal implementations in these examples,
 all files are regarded as character special devices.
 The `sys/stat.h' header file required is distributed in the `include' subdirectory for this C library.
 */
int _fstat(int file, struct stat *st) {
    st->st_mode = S_IFCHR;
    return 0;
}