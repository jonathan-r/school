library ieee;
use ieee.std_logic_1164.all;


entity mux2_16bit is
    port ( s   : in  std_logic;
           in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
			  z   : out std_logic_vector (15 downto 0) );
end mux2_16bit;

architecture behavioral of mux2_16bit is begin
	process(s, in0, in1)
		begin case s is
			when '0' => z <= in0;
			when '1' => z <= in1;
			when others => z <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

