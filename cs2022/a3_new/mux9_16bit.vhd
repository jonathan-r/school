library ieee;
use ieee.std_logic_1164.all;
use work.array_types.all;

entity mux9_16bit is
    port ( s       : in  std_logic_vector (3 downto 0);
           in_data : in  data_array;
           z       : out std_logic_vector (15 downto 0) );
end mux9_16bit;

architecture behavioral of mux9_16bit is begin
	process(s, in_data) begin 
		if s(3) = '0' then
			case s(2 downto 0) is
				when "000" => z <= in_data(0);
				when "001" => z <= in_data(1);
				when "010" => z <= in_data(2);
				when "011" => z <= in_data(3);
				when "100" => z <= in_data(4);
				when "101" => z <= in_data(5);
				when "110" => z <= in_data(6);
				when "111" => z <= in_data(7);
				
				when others => z <= "UUUUUUUUUUUUUUUU";
			end case;
		
		elsif s(3) = '1' then
			z <= in_data(8);
		
		else
			z <= "UUUUUUUUUUUUUUUU";
		
		end if;
	end process;
end behavioral;