#pragma once

#include <vector>
#include <GL/glew.h>

#include "vbo.h"
#include "texture.h"
#include "vertex.h"
#include "maths/mat4.h"
#include "maths/vec.h"

class Object {
public:
	vec3 stem; //local space
	vec3 scale;
	mat4 global; //global space
	std::vector<Object*> shoots;
	GLenum drawMode;
	Mesh* model;
	Texture* texture;

	Object() : scale(1.0f) {
		model = nullptr;
		texture = nullptr;
		drawMode = GL_TRIANGLES;
	}

	virtual void bakeSelf(const mat4& parent) = 0;
	void bakeAll(const mat4& parent = mat4::identity()) {
		bakeSelf(parent);
		for (auto ob : shoots) {
			ob->bakeAll(global);
		}
	}
	
	void draw(GenericShader& shader, const mat4& projView) {
		assert(model);
		
		mat4 m = projView * global;
		shader.setFullMatrix(m);
		shader.setWorldMatrix(global);
	
		model->bind(shader);
		if (texture) {
			texture->bind(0);
		}
		if (model->getIndicesAssigned() > 0) {
			//using indices
			glDrawElements(drawMode, model->getIndicesAssigned(), GL_UNSIGNED_INT, NULL);
		}
		else {
			//not using indices
			glDrawArrays(drawMode, 0, model->getVerticesAssigned());
		}
		if (texture) {
			texture->unBind();
		}
	
		//draw descendents
		for (auto ob : shoots) {
			ob->draw(shader, projView);
		}
	}
};

class AnglesObject : public Object {
public:
	vec3 eu;
	
	AnglesObject() {
		eu.set(0.0f, 0.0f, 0.0f);
	}

	void bakeSelf(const mat4& parent) {
		mat4 tr = mat4::translation(stem);
		mat4 rt = mat4::flightRotation(eu.y, eu.x, eu.z);
		mat4 sc = mat4::scale(scale);
		global = parent * tr * rt * sc;
	}
};

class QuatObject : public Object {
public:
	quat qt; //euler angles
	void bakeSelf(const mat4& parent) {
		mat4 tr = mat4::translation(stem);
		mat4 rt = mat4::quaternionRotation(qt);
		mat4 sc = mat4::scale(scale);
		global = parent * (tr * rt) * sc;
	}
};
