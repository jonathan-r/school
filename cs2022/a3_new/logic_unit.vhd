library ieee;
use ieee.std_logic_1164.all;

entity logic_unit is
    port ( a  : in  std_logic_vector (15 downto 0);
           b  : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector  (1 downto 0);
           f  : out  std_logic_vector (15 downto 0) );
end logic_unit;

architecture behavioral of logic_unit is
	component mux4_16bit is
		port ( in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
           in2 : in  std_logic_vector (15 downto 0);
           in3 : in  std_logic_vector (15 downto 0);
           addr : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
	end component;
	
	signal conjunction : std_logic_vector (15 downto 0);
	signal disjunction : std_logic_vector (15 downto 0);
	signal exclusive : std_logic_vector (15 downto 0);
	signal inverted : std_logic_vector (15 downto 0);

begin
	mux : mux4_16bit port map(conjunction, disjunction, exclusive, inverted, op, f);

	conjunction <= a and b after 1ns;
	disjunction <= a or b after 1ns;
	exclusive <= a xor b after 1ns;
	inverted <= not a after 1ns;
end behavioral;

