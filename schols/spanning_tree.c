#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct nodeStruct{
	int number;
	int neighbourCount;
	struct nodeStruct** neighbours;
} node;

typedef struct linkStruct{
	int a, b;
} link;

const int maxNodes = 20;
node nodes[maxNodes];
node* links[maxNodes * maxNodes];
int nodeCount = 0;

#define MARK 0x80000000
#define MASK 0x000000FF


void cleanup_marking(node* from, node* n){
	//unmark it
	n->number &= MASK;

	for (int i = 0; i < n->neighbourCount; i++){
		node* to = n->neighbours[i];
		if (to != from)
			if (to->number & MARK) cleanup_marking(n, to);
	}
}

bool spanning_tree_marking_inner(node* from, node* n, link** keeping, link** deleting){
	bool cycle = false;

	//mark it once
	n->number |= MARK;

	for (int i = 0; i < n->neighbourCount; i++){
		node* to = n->neighbours[i];
		if (to != from){ //don't go directly back
			int self = n->number & MASK;
			int other = to->number & MASK;

			bool remove = (to->number & MARK) != 0;
			link** storeIn = (remove)? deleting : keeping;

			if (self > other){ //prevent redundant ((a, b), (b, a)) links
				(*storeIn)->a = self;
				(*storeIn)->b = other;
				(*storeIn)++; //increment end pointer
				//return true; //TODO: this needed?
			}

			if (!remove) spanning_tree_marking_inner(n, to, keeping, deleting);
		}
	}

	return false;
}


//up to O(N) for a an N-sized graph
void spanning_tree(node* start){
	link keep[20];
	link delete[20];

	link* keep_end = keep;
	link* delete_end = delete;

	spanning_tree_marking_inner(NULL, start, &keep_end, &delete_end);
	cleanup_marking(NULL, start);

	if (keep_end != keep){
		printf("keep");
		for (link* k = keep; k < keep_end; k++)
			printf(" (%d %d)", k->a, k->b);
		
		printf(".\n");
	}

	if (delete_end != delete){
		printf("remove");
		for (link* d = delete; d < delete_end; d++)
			printf(" (%d %d)", d->a, d->b);

		printf(".\n");
	}
}

/*
//up to O(N*N) for an N-sized graph,
//since the stack must be searched every for every node
bool spanning_tree_stack_inner(node* n, node** start, int si){
	bool cycle = false;
*/

void connect(node* a, node* b){
	a->neighbours[a->neighbourCount++] = b;
	b->neighbours[b->neighbourCount++] = a;
}

int main(){
	int i;
	for (i = 0; i < maxNodes; i++){
		nodes[i].number = i;
		nodes[i].neighbourCount = 0;
		nodes[i].neighbours = &links[i * maxNodes];
	}

	memset(links, 0, maxNodes * maxNodes * sizeof(node*));

	/*nodeCount = 9;
	connect(&nodes[0], &nodes[1]);
	connect(&nodes[0], &nodes[2]);
	connect(&nodes[2], &nodes[3]);
	connect(&nodes[2], &nodes[5]);
	connect(&nodes[3], &nodes[6]);
	//connect(&nodes[3], &nodes[4]);
	connect(&nodes[4], &nodes[5]);
	connect(&nodes[4], &nodes[7]);
	connect(&nodes[7], &nodes[8]);*/

	connect(&nodes[0], &nodes[3]);
	connect(&nodes[0], &nodes[6]);
	connect(&nodes[3], &nodes[2]);
	connect(&nodes[2], &nodes[1]);
	connect(&nodes[1], &nodes[6]);
	connect(&nodes[5], &nodes[6]);
	connect(&nodes[5], &nodes[4]);
	connect(&nodes[4], &nodes[6]);

	node* start = &nodes[0];
	spanning_tree(start);

	return 0;
}
