#atoms
LITERAL = "literal"
BRACKETS_POSITIVE = "brackets"
BRACKETS_NEGATIVE = "neg-brackets"
GROUP = "group"

#times
ONCE = "once"
OPTIONAL = "optional"
ONCE_OR_MORE = "once or more"
ANY_AMOUNT = "any amount"
times_dict = {'?' : OPTIONAL, '+' : ONCE_OR_MORE, '*' : ANY_AMOUNT}

#[abc] or [^abc] and so on
def parse_brackets(expr, i):
	positive = True
	if expr[i] == '^':
		positive = False
		i += 1

	things = []
	escape = False
	rng = False

	while i < len(expr):
		#escaped literal
		if expr[i] == '\\' and not escape:
			escape = True
			i += 1
			#things.append( (LITERAL, expr[i+1]) )
			#i += 2

		#']' loses its special meaning and represents itself unless it's the last in the brackets, but it's a pain to do,
		#so just assume this is the last one
		if expr[i] == ']' and not escape:
			i += 1 # eat ]
			times = ONCE
			if expr[i] in times_dict:
				times = times_dict[expr[i]]
				i += 1

			return things, times, positive, i

		elif expr[i] == '[' and not escape:
			raise Exception('recursive brackets')

		elif expr[i] == '(' and not escape:
			print("group at %d" % i)
			(sc, st, i) = parse_expression(expr, i+1, True)
			assert st == ONCE
			things.append( (GROUP, sc, st) )
			print("group ends at %d" % i)

		elif expr[i] == ')' and not escape:
			raise Exception('unescaped, non-group-terminating ) in brackets')

		else:
			things.append( (LITERAL, expr[i]) )

			i += 1
			escape = False

	raise Exception('expected ]')

def parse_expression(expr, i, expect_close_paren):
	things = []

	while i < len(expr):
		if expr[i] == '^' or expr[i] == '$':
			#ignore
			i += 1

		elif expr[i] == ' ':
			#ignore
			i += 1

		elif expr[i] == '\\':
			things.append( (LITERAL, expr[i+1]) )
			i += 2

		elif expr[i] == '[':
			print("brackets at %d" % i)
			(bc, bt, bp, i) = parse_brackets(expr, i+1)
			if bp:
				things.append( (BRACKETS_POSITIVE, bc, bt) )
			else:
				things.append( (BRACKETS_NEGATIVE, bc, bt) )
			print("brackets ends at %d" % i)

		elif expr[i] == '(':
			print("group at %d" % i)
			(sc, st, i) = parse_expression(expr, i+1, True)
			things.append( (GROUP, sc, st) )
			print("group ends at %d" % i)

		#sub-expression done
		elif expr[i] == ')':
			if expect_close_paren:
				i += 1 #eat )
				times = ONCE
				if expr[i] in times_dict:
					times = times_dict[expr[i]]
					print("times of expression/group is %s" % times)
					i += 1


				return things, times, i

			else:
				raise Exception('unexpected )')

		elif expr[i] == '\n':
			break

		else:
			things.append( (LITERAL, expr[i]) )
			i += 1

	if not expect_close_paren:
		return things, i

	else:
		raise Exception('expected )')
		
#very limited subset of BRE
def parse_regex(expr):
	(things, i) = parse_expression(expr + '\n', 0, False)
	assert i == len(expr)
	return things