#libraries
from socketserver import BaseRequestHandler

class Connection:
	class BadLineException(Exception):
		def __init__(self, msg):
			super(BadLineException, self).__init__(msg)

	def __init__(self, socket):
		self.sock = socket
		self.readFile = socket.makefile('rb', buffering=4096) #this precludes socket timout
		self.writeFile = socket.makefile('wb', buffering=4096) #this precludes socket timout

	def end(self):
		self.writeFile.flush()

		self.readFile.close()
		self.writeFile.close()

	def writeLine(self, data):
		self.writeFile.write(data + '\n')

	def getCommand(self):
		line = self.readFile.readline()
		at = line.find(':')
			if at == -1:
				raise BadLineException("expected command, got: " + line)

			else:
				cmd = line[:at]
				arg = line[at+1:]
				return cmd, arg

	def getLine(self, expectKey):
		line = self.readFile.readline()
		keyAt = line.find(expectKey)
		if keyAt == 0:
			return line[len(expectKey)+1:]
		else:
			raise BadLineException("expected line '%s:...'" % expectKey)


class ServerHandlerBase(BaseRequestHandler):
	def setup(self):
		self.cmdMap = {} #first_key => method in the form handle*(firstarg)
		self.conn = Connection(self.request)

	def handle(self): #entry point, comes with self.request (a socket) and self.client_address
		print("connected to " + str(self.client_address))

		while True:
			cmd, arg = self.conn.getCommand()

			if cmd in self.cmdMap:
				print("got command " + cmd)
				self.cmdMap[cmd](arg)

			else:
				raise BadLineException("unknown command '%s'" % cmd)


	def finish(self):
		#close files
		self.conn.end()

		#close socket itself
		self.request.close()

