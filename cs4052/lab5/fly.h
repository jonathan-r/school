#pragma once

//unused for now

//fly that flies on randam path around a point
//seamless path progression (bicubic spline interpolation for continuous angular velocity)
struct fly {
	float t; //position between [0, 1)
	vec3 ps[4]; //position (phi, rho, mag)
	vec3 hs[5]; //handles of position interpolant
	vec3 worldPos; //baked position (x, y, z)
	vec3 cd, cs; //diffuse, specular colors
	vec3 dir; //direction of flight
	float speed; //distance per unit time
	float segLen;
	
	fly() {
		ps[1] = genDest();
		ps[2] = genDest();
		ps[3] = genDest();
		hs[0] = vec3(0); //initial, cannot compute
		t = 1.0f; //force update
		
		update(0.0f);
		dir = vec3(1.0f, 0, 0);
		cd = (vec3(0.1f) + vec3((rand() % 1000) / 1000.0f, (rand() % 1000) / 1000.0f, (rand() % 1000) / 1000.0f)) * 2;
		cs = (vec3(0.1f) + vec3((rand() % 1000) / 1000.0f, (rand() % 1000) / 1000.0f, (rand() % 1000) / 1000.0f)) * 2;
		speed = 0.9f + (rand() % 1000) / 1000.0f;
	}
	
	vec3 genDest() {
		vec3 dst;
		dst.x = M_PI * ((rand() % 1000) / 1000.0f - 0.5f);
		dst.y = 2 * M_PI * (rand() % 1000) / 1000.0f;
		dst.z = 5 + 5 * (rand() % 1000) / 1000.0f - 2.5f;
		return dst;
	}

	vec3 atTime(float t) { //shadows this->t
		vec3& p0 = ps[0];
		vec3& p3 = ps[1];
		vec3& p1 = hs[0];
		vec3& p2 = hs[1];
		vec3 c = bicub(p0, p1, p2, p3, t);
		return mat4::flightRotation(c.y, c.x, 0.0f).multVec3(vec3(c.z, 0, 0));
	}

	void update(float dt) {
		if (t >= 1.0f) {
			ps[0] = ps[1];
			ps[1] = ps[2];
			ps[2] = genDest();
			hs[0] = hs[2]; //transfer over
			t = 0.0f;
			bicubHandlesVec3(ps[0], ps[1], ps[2], hs[1], hs[2]);
			bicubHandlesVec3(ps[1], ps[2], ps[3], hs[3], hs[4]);

			//just approximate the length in the converted carthesian space for a few points;
			//this will almost always be an underestimate
			segLen = 0.0f;
			vec3 l = atTime(0.0f);
			for (int i = 1; i <= 500; i++) {
				float tt = (float)i / 500;
				vec3 p = atTime(tt);
				segLen += (p - l).length();
				l = p;
			}
		}
		
		t += dt * speed / segLen;
		vec3 np = atTime(t);
		vec3 lastPos = worldPos;
		worldPos = np;
		dir = (worldPos - lastPos).normal();
	}
};