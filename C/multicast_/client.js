'use strict';

var dgram = require('dgram');
var client = dgram.createSocket('udp4');

if (process.argv.length != 2 + 3){
	console.log("usage: " + process.argv[0] + " " + process.argv[1] + " <multicast group> <inbound ip> <port>");
	process.exit();
}

var group = process.argv[2];
var host = process.argv[3];
var port = new Number(process.argv[4]);

client.on('listening', function () {
	var address = client.address();
	console.log('UDP Client listening on ' + address.address + ":" + address.port);
	client.setBroadcast(true)
	client.setMulticastTTL(128); 
	client.addMembership(group, host);
});

client.on('message', function (message, remote) {   
	console.log('B: From: ' + remote.address + ':' + remote.port +' - ' + message);
});

client.bind(port);
