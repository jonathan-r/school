#include <stdio.h>

//assembly.s
extern "C" int p_asm(int i, int j);
extern "C" int q_asm(int i);
extern "C" int f_asm(int n);
extern "C" int g;

//reference implementations
int p_reference(int i, int j){
	int k = i + j;
	return (k << 2) - 1;
}

int q_reference(int i){
	return p_reference(g, -i);
}

int f_reference(int n){
	if (n > 0){
		return n * f_reference(n - 1);
	}
	else{
		return 1;
	}
}

int leading(int n){
	int space = 0;
	if (n < 0){
		space++;
		n = -(n / 10);
		if (n != 0) space++;
	}
	
	if (n == 0) {
		space++;
	}
	
	while (n > 0){
		space++;
		n /= 10;
	}

	return space;
}

int main(){
	printf("testing p:\n");
	for (int i = -1; i <= 20; i += 4){
		for (int j = -1; j <= 20; j += 4){
			int sample = p_asm(i, j);
			int control = p_reference(i, j);

			printf("i=%*s%d, j=%*s%d: expected %*s%d, got %*s%d; %s\n", 2-leading(i), "", i, 2-leading(j), "", j, 4-leading(control), "", control, 4-leading(sample), "", sample, (sample == control)? "match" : "wrong");
		}
	}

	printf("\ntesting q:\n");
	for (int i = -1; i <= 20; i++){
		int sample = q_asm(i);
		int control = q_reference(i);
		printf("i=%*s%d: expected %*s%d, got %*s%d; %s\n", 2-leading(i), "", i, 11-leading(control), "", control, 11-leading(sample), "", sample, (sample == control)? "match" : "wrong");
	}

	printf("\ntesting f:\n");
	for (int i = -1; i <= 20; i++){
		int sample = f_asm(i);
		int control = f_reference(i);
		printf("n=%*s%d: expected %*s%d, got %*s%d; %s\n", 2-leading(i), "", i, 11-leading(control), "", control, 11-leading(sample), "", sample, (sample == control)? "match" : "wrong");
	}
	printf("done\n");
	return 0;
}
