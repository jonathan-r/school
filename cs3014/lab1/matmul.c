#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <stddef.h>
#include <xmmintrin.h>
#include <pmmintrin.h> //sse3
#include <smmintrin.h> //sse4.1
#include <omp.h>

struct complex {
  float real;
  float imag;
};

//very good ref https://software.intel.com/sites/landingpage/IntrinsicsGuide/

/* the fast version of matmul written by the team */
void team_matmul(struct complex ** A, struct complex ** B_src, struct complex ** C, int a_rows, int a_cols, int b_cols)
{
  //a_cols is rows of B
  //C has a_rows and b_cols

  //transposed B

  //(memory) row length must be a multiple of 2 for unaligned access to work
  int alingned_a_cols = a_cols + (a_cols & 1);

  //allocate row header, alignment wiggle room, then data block
  struct complex ** B = malloc(b_cols * sizeof(struct complex *) + 15 + (alingned_a_cols * b_cols) * sizeof(struct complex));
  struct complex * B_data = (struct complex *) (B + b_cols);

  //offset the data buffer to make it 16-byte aligned
  if ((uintptr_t) B_data & 0xF) B_data = (struct complex *) ((uintptr_t) B_data + (16 - ((uintptr_t) B_data & 0xF)));

  //generate row header
  for (int r = 0; r < b_cols; r++) {
    B[r] = B_data + (r * alingned_a_cols);
  }

  //copy transposed data
  for (int r = 0; r < a_cols; r++) {
    for (int c = 0; c < b_cols; c++) {
      B[c][r] = B_src[r][c];
    }
  }

  //sanity check
  assert( sizeof(float) == 4 );
  assert( sizeof(struct complex) == 2 * sizeof(float) );
  assert( offsetof(struct complex, real) == 0 );
  assert( offsetof(struct complex, imag) - offsetof(struct complex, real) == sizeof(float) );

  //simple sse multiplication
  float arr[4];

  #pragma omp parallel for schedule(static) num_threads(32) private(arr)
  for (int r = 0; r < a_rows; r++) {
    for (int c = 0; c < b_cols; c++) {
      __m128 real_sum = _mm_set_ps1(0.0f); //[r4, r3, r2, r1]
      __m128 imag_sum = _mm_set_ps1(0.0f); //[i4, i3, i2, i1]

      int k = 0;
      for (; k < (a_cols & ~3); k += 4) {
        //printf("sse r=%d, c=%d, k=%d\n", r, c, k);

        __m128 a1, a2, b1, b2, a1b1, a2b2, end;
        a1 = _mm_loadu_ps(&A[r][k].real);   //[ai2, ar2, ai1, ar1]
        a2 = _mm_loadu_ps(&A[r][k+2].real); //[ai4, ar4, ai3, ar3]
        b1 = _mm_load_ps(&B[c][k].real);    //[bi2, br2, bi1, br1]
        b2 = _mm_load_ps(&B[c][k+2].real);  //[bi4, br4, bi3, br3]

        a1b1 = _mm_mul_ps(a1, b1);      //[ai2*bi2, ar2*br2, ai1*bi1, ar1*br1]
        a2b2 = _mm_mul_ps(a2, b2);      //[ai4*bi4, ar4*br4, ai3*bi3, ar3*br3]
        end = _mm_hsub_ps(a1b1, a2b2);  //[ar4*br4 - ai4*bi4, ar3*br3 - ai3*bi3, ar2*br2 - ai2*bi2, ar1*br1 - ai1*bi1] //sse3
        real_sum = _mm_add_ps(real_sum, end);

        b1 = _mm_shuffle_ps(b1, b1, _MM_SHUFFLE(2, 3, 0, 1)); //[br2, bi2, br1, bi1]
        b2 = _mm_shuffle_ps(b2, b2, _MM_SHUFFLE(2, 3, 0, 1)); //[br4, bi4, br3, bi3]
        a1b1 = _mm_mul_ps(a1, b1);      //[ai2*br2, ar2*bi2, ai1*br1, ar1*bi1]
        a2b2 = _mm_mul_ps(a2, b2);      //[bi4*br4, ar4*bi4, ai3*br3, ar3*bi3]
        end = _mm_hadd_ps(a1b1, a2b2);  //[bi4*br4 + ar4*bi4, ai3*br3 + ar3*bi3, ai2*br2 + ar2*bi2, ai1*br1 + ar1*bi1]
        imag_sum = _mm_add_ps(imag_sum, end);
      }

      //now reduce real_sum and imag_sum to a single value each
      real_sum = _mm_hadd_ps(real_sum, real_sum); // [<duplicate>, real2, real1]
      imag_sum = _mm_hadd_ps(imag_sum, imag_sum); // [<duplicate>, imag2, imag1]
      real_sum = _mm_hadd_ps(real_sum, imag_sum); // [imag, imag, real, real]

      _mm_store_ps(arr, real_sum);
      float real, imag;
      real = arr[0];
      imag = arr[2];

      //runoff
      for (; k < a_cols; k++) {
        //printf("plain r=%d, c=%d, k=%d\n", r, c, k);
        real += A[r][k].real * B[c][k].real - A[r][k].imag * B[c][k].imag;
        imag += A[r][k].real * B[c][k].imag + A[r][k].imag * B[c][k].real;
      }

      //C[r][c] = sum;
      C[r][c].real = real;
      C[r][c].imag = imag;
    }
  }

  free(B);
}
