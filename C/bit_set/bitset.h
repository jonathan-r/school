#ifndef INT_SET
#define INT_SET

#include <stdint.h>

//covers entire 32 bit range
#define BITSET_DATA_BITS 5 //covers [0, 4 * 8]
#define BITSET_PAGE_BITS 16 //covers [0, 

//computed
#define BITSET_PAGES (1 << (32 - BITSET_DATA_BITS - BITSET_PAGE_BITS))
#define BITSET_PAGE_SIZE (1 << BITSET_PAGE_BITS)
#define BITSET_PAGE_MASK (BITSET_PAGE_SIZE - 1)
#define BITSET_BIT_MASK ((1 << BITSET_DATA_BITS) - 1)

typedef struct bitset_page_struct{
	uint32_t data[BITSET_PAGE_SIZE];
	uint32_t elements;
} bitset_page;

typedef struct bitset_struct{
	bitset_page* pages[BITSET_PAGES];
} bitset;

// create a new, empty set
bitset* bitset_new();

// safely destroy any kind of set
void bitset_destroy(bitset* this);

// check to see if an item is in the set;
// returns 1 if in the set, 0 if not;
int bitset_lookup(bitset* this, uint32_t item);

// add an item, with number 'item' to the set;
// has no effect if the item is already in the set;
// returns 1 if the item did not already exist, 0 otherwise
int bitset_add(bitset* this, uint32_t item);

// remove an item with number 'item' from the set;
// has no effect if the items is not in the set;
// returns 1 if the item did exist, 0 otherwise
int bitset_remove(bitset* this, uint32_t item);

// place the union of src1 and src2 into dest;
// dest must be an empty set (but can be allocated);
// n steps
void bitset_union(bitset* dest, bitset* src, bitset* src2);

// place the intersection of src1 and src2 into dest;
// dest must be an empty set (but can be allocated);
// n steps
void bitset_intersect(bitset* dest, bitset* src, bitset* src2);

#endif
