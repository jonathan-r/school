module Main where
import qualified Graphics.HGL as G
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import System.Environment
import System.IO.Unsafe

--internal
import LSystem
import LDraw

plantSystem :: LSystem
plantSystem = ("XF", "+-[]", "X", makeRules [('X', "F-[[X]+X]+F[+FX]-X"), ('F', "FF")])

main :: IO ()
main =
    G.runGraphics $
    G.withWindow_ "Test Window" (width, height) $ \ w -> do
        putStrLn (args !! 0)
        -- p <- createPen Solid 1 (color 255 255 0)
        --G.drawInWindow w $ G.line (10, 10) (10, 10)
        drawLines plantLines w
        G.getKey w

        where
            (width, height) = (800, 900) :: (Int, Int)
            args = unsafePerformIO getArgs
            iter = read (args !! 0) :: Int
            size = (read (args !! 1) :: Float) / (fromIntegral iter)
            plantString = evaluateLast plantSystem iter
            plantLines = stackDraw plantString size [((fromIntegral (width `div` 2), fromIntegral height), -pi/2)]

