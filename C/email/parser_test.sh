#!/bin/bash

TERMS="com exe bat ie net org"

test(){
	./parser $1 $TERMS
}

echo
test right@good.place.net
test rg iht@good.place.not.here
test I.am@fish
test tes	t..test@aol.ie
test test.test.ie
test df@sd@test.comm
test abs@ie
test 3number@45.com
test not@3number.org
echo
