#pragma once

#include "maths/vec.h"
#include "maths/util.h"
#include "texture.h"

class SunSky {
//TODO proper sun and sky https://nicoschertler.wordpress.com/2013/04/03/simulating-a-days-sky/

public:
	static void sunSpecs(float cycle, vec3& dir, vec3& col, vec3& spe, vec3& amb, vec3& sky) {
		float dayStart = (6 / 24.0f);
		float dayMid = (14 / 24.0f);
		float dayEnd = (20 / 24.0f);

		vec3 colStart(0.5f, 0.4f, 0.4f);
		vec3 colMid(0.8f, 0.8f, 0.2f);
		vec3 colEnd(0.9f, 0.7f, 0.2f);

		vec3 speStart(0.5f, 0.4f, 0.4f);
		vec3 speMid(0.8f, 0.8f, 0.2f);
		vec3 speEnd(0.9f, 0.7f, 0.2f);

		vec3 dirStart = vec3(1.0f, 0.0f, 0.5f).normal();
		vec3 dirMid = vec3(0.0f, 1.0f, 0.0f).normal();
		vec3 dirEnd = vec3(0.5f, 0.0f, 0.7f).normal();

		vec3 skyStart = vec3(0.4, 0.5, 0.7f);
		vec3 skyMid = vec3(0.6, 0.6, 0.7f);
		vec3 skyEnd = vec3(0.6, 0.4, 0.4f);

		if (cycle < dayStart or cycle > dayEnd) {
			dir = dirStart;
			col = colStart;
			spe = speStart;
			amb.set(0.2f, 0.2f, 0.2f);
			sky.set(0.2, 0.2f, 0.2f);
		}
		else if (cycle < dayMid) {
			float t = (cycle - dayStart) / (dayMid - dayStart);
			dir = lerp(dirStart, dirMid, t).normal();
			col = lerp(colStart, colMid, t);
			spe = lerp(speStart, speMid, t);
			amb.set(0.2f, 0.2f, 0.2f);
			sky = lerp(skyStart, skyMid, t);
		}
		else if (cycle < dayEnd) {
			float t = (cycle - dayMid) / (dayEnd - dayMid);
			dir = lerp(dirMid, dirEnd, t).normal();
			col = lerp(colMid, colEnd, t);
			spe = lerp(speMid, speEnd, t);
			amb.set(0.2f, 0.2f, 0.2f);
			sky = lerp(skyMid, skyEnd, t);
		}
	}
};

