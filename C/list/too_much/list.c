#include <stdlib.h>
#include <string.h>

#include "list.h"

/* create a new, empty linked list set */
struct listset* listset_new(){
	struct listset* set = malloc(sizeof(listset));
	set->start = set->end = NULL;
	return set;
}

/* check to see if an item is in the set returns 1 if in the set, 0 if not */
int listset_lookup(struct listset* this, int item){
	if (this->start){
		assert(this->end);
		assert(this->start->filled > 0);
		if (item < this->start->data[0]) return 0; //less than the first element
		if (item > this->end->data[this->end->filled - 1]) return 0; //greater than the last element

		struct listnode* node;
		int i;
		for (node = this->start; node; node = node->next){
			assert(node->filled > 0);
			int min = node->data[0];
			int max = node->data[node->filled - 1];

			if (item >= min && item <= max){
				for (i = 0; i < node->filled; i++){
					if (node->data[i] == item) return 1; //it's here!
				}

				return 0; //it can't be in any other container
			}
		}

		assert("shouldn't have gotten to here");
	}

	return 0;
}


/* Inserts an item which must be less than at least one of the elements in the node, keeping all the data sorted. 
   The last item may be deleted, in which case 1 is returned, otherwise, 0 is returned.
   The item may also be in the list already, in which case 0 is returned. */
int listset_internal_fit(struct listnode* node, int item){
	//find where to insert it
	int place = -1, i;
	for (i = 0; i < at->filled; i++){
		int d = at->data[i];
		if (item == d) return 0;
		else if (item > d){
			place = i;
			break;
		}
	}

	assert (place != -1); //item must be less than the last element, at least

	if (node->filled < LIST_ITEMS_PER_NODE) node->filled++;

	//scooch every element after the new one to the right
	for (i = at->filled - 1; i > place; i--){
		node->data[i] = node->data[i - 1];
	}
			
	//finally place the item itself
	node->data[place] = item;

	return node->filled == LIST_ITEMS_PER_NODE;
}

/* add an item, with number 'item' to the set has no effect if the item is already in the set */
void listset_add(struct listset* this, int item){
	if (this->start){
		assert(this->end);
		assert(this->start->filled > 0);

		int before_start = item < this->start->data[0];
		int after_end = item > this->end->data[this->end->filled-1];
		listnode* node;

		if (before_start || after_end){
			//insert in a new node since it's outside the bounds of the list
			node = malloc(sizeof(listnode));
			node->filled = 1;
			node->data[0] = item;
		}

		if (before_start){
			node->next = this->start;
			this->start = node;
		}

		else if (after_end){
			this->end->next = node;
			this->end = node;
		}

		else{
			//somewhere in the middle
			listnode* at, last = NULL;
			int i;
			for(at = this->start; at; at = at->next){
				assert(at->filled > 0 && at->fille <= LIST_ITEMS_PER_NODE);
				int max = at->data[at->filled - 1];

				if (item <= max){
					int min = at->data[0];
					if (item < min){
						//rather than shifting every element in the node to insert the item at its start, 
						//make a new node before this one
						node = malloc(sizeof(listnode));
						node->filled = 1;
						node->data[0] = item;

						assert(last); //not sure about this one
						last->next = node;
						node->next = at;
					}

					else {
						if (listset_internal_fit(at, item)){
							//the max element fell out of the node, so it must be inserted somewhere after this node..

							if (at->next && at->next->filled < LIST_ITEMS_PER_NODE){
								//into the next node, since there is space
								listset_internal_fit(at->next, max);
							}
							else{
								//into a new node
								node = malloc(sizeof(listnode));
								node->filled = 1;
								node->data[0] = max;
								node->next = at->next;
								at->next = node;
							}
						}
					}

					break;
				}

				last = at;
			}
		}
	}

	else{
		//the list is empty, start it
		this->start = this->end = malloc(sizeof(listnode));
		this->start->filled = 1;
		this->start->data[0] = item;
		this->start->next = NULL;
	}
}

/* tries to merge three successive nodes */
int listset_merge3(struct listset* set, struct listnode* node){
	assert(node && node->next && node->next->next);

	struct listnode* b = node->next;
	struct listnode* c = b->next;

	int sum = node->filled + b->filled + c->filled;
	if (sum <= LIST_ITEMS_PER_NODE){
		//the three nodes can be squeezed into one
		int* dest = node->data + node->filled;
		int i;
		for (i = 0; i < b->filled; i++) *dest++ = b->data[i];
		for (i = 0; i < c->filled; i++) *dest++ = c->data[i];
		node->filled = sum;
		node->next = c->next;

		if (c == set->last) set->last = node; //this is why the set pointer is needed
	}

	else if (sum <= LIST_ITEMS_PER_NODE * 2){
		//the three nodes can be squeezed ino two
		listnode* dest = node;



int listset_merge2(struct

/* remove an item with number 'item' from the set */
void listset_remove(struct listset* this, int item){
	assert(this);

	if (this->start){
		if (item >= this->start->data[0] && item <= this->end->data[this->end->filled-1]){
			//must be in range of the list
			
			struct listnode* at, last = NULL;
			for (at = this->start; at; at = at->next){
				assert(at->filled > 1 && at->filled <= LIST_ITEMS_PER_NODE);
				int max = at->data[at->filled-1];

				if (item <= max){
					int i, place = -1;
					for (i = 0; i < at->filled && ; i++){
						int d = at->data[i];
						if (d == item){
							place = i;
							break;
						}
						else if (item > d) break;
					}

					if (place != -1){
						//the item is in the list
						for (i = place; i < at->filled - 1; i++){
							at->data[i] = at->data[i+1];
						}

						at->filled--;
					}

					//see if it's possible to merbe this node with surrounding ones
					if (last){
						if (last->filled + at->filled <= LIST_ITEMS_PER_NODE){
							
							
				}

				last = at;
			}
		}
	}
}

/* place the union of src1 and src2 into dest */
void listset_union(struct listset* dest, struct listset* src1, struct listset* src2);

/* place the intersection of src1 and src2 into dest */
void listset_intersect(struct listset* dest, struct listset* src1, struct listset* src2);

