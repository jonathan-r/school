library ieee;
use ieee.std_logic_1164.all;

package array_types is
	type data_array is array (0 to 8) of std_logic_vector(15 downto 0);
end package;
