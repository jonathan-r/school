library ieee;
use ieee.std_logic_1164.all;
 
entity mux4_16bit_tb is
end mux4_16bit_tb;
 
architecture behavior of mux4_16bit_tb is 
	component mux4_16bit
		port(in0 : in  std_logic_vector(15 downto 0);
         in1 : in  std_logic_vector(15 downto 0);
         in2 : in  std_logic_vector(15 downto 0);
         in3 : in  std_logic_vector(15 downto 0);
         addr : in  std_logic_vector(1 downto 0);
         output : out  std_logic_vector(15 downto 0) );
	end component;

   signal in0 : std_logic_vector(15 downto 0) := (others => '0');
   signal in1 : std_logic_vector(15 downto 0) := (others => '0');
   signal in2 : std_logic_vector(15 downto 0) := (others => '0');
   signal in3 : std_logic_vector(15 downto 0) := (others => '0');
   signal addr : std_logic_vector(1 downto 0) := (others => '0');
   signal output : std_logic_vector(15 downto 0);
 
begin
   uut: mux4_16bit port map (in0, in1, in2, in3, addr, output);

   stim_proc: process
   begin
      wait for 10 ns;
		
		--same value on all
		in0 <= "0101011001011010";
		in1 <= "0101011001011010";
		in2 <= "0101011001011010";
		in3 <= "0101011001011010";
		wait for 10ns;
		
		for i in 0 to 3 loop
			addr <= std_logic_vector(to_unsigned(i, 2));
			wait for 1 ns;
			assert output = "0101011001011010";
		end loop;

		--unique value on each one
		in0 <= x"0123";
		in1 <= x"f12a";
		in2 <= x"e12b";
		in3 <= x"d12c";

		s_in <= "000";
		wait for 1 ns;
		assert z_out = x"0123";

		s_in <= "001";
		wait for 1 ns;
		assert z_out = x"f12a";

		s_in <= "010";
		wait for 1 ns;
		assert z_out = x"e12b";

		s_in <= "011";
		wait for 1 ns;
		assert z_out = x"d12c";

      wait;
   end process;

end;
