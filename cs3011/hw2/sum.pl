mkList(0, []).
mkList(X, [X | TL]) :- 
	X > 0, succ(Y, X), mkList(Y, TL). %X > 0 to cover all cases

s(X) --> [X].
s(X) --> [Y], {mkList(X, X_list)}, {member(Y, X_list)},
              {member(Z, X_list)}, {plus(Z, Y, X)}, s(Z).
