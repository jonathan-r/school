library ieee;
use ieee.std_logic_1164.all;

entity function_unit is
    port ( a : in  std_logic_vector (15 downto 0);
           b : in  std_logic_vector (15 downto 0);
           func : in  std_logic_vector (4 downto 0);
           v : out  std_logic;
           c : out  std_logic;
           n : out  std_logic;
           z : out  std_logic;
           f : out  std_logic_vector (15 downto 0));
end function_unit;

architecture behavioral of function_unit is
	component logic_unit is
		port ( a  : in  std_logic_vector (15 downto 0);
				 b  : in  std_logic_vector (15 downto 0);
             op : in  std_logic_vector  (1 downto 0);
             f  : out  std_logic_vector (15 downto 0) );
	end component;
	
	component arithmetic_unit is
		 port ( a : in  std_logic_vector (15 downto 0);
				  b : in  std_logic_vector (15 downto 0);
              op : in  std_logic_vector (2 downto 0);
              result : out  std_logic_vector (15 downto 0);
              v : out  std_logic;
              c : out  std_logic );
	end component;
	
	component shifter_unit is
		port ( input : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
	end component;

	component mux4_16bit is
		port ( in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
           in2 : in  std_logic_vector (15 downto 0);
           in3 : in  std_logic_vector (15 downto 0);
           addr : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
	end component;
	
	signal arith_out : std_logic_vector (15 downto 0);
	signal logic_out : std_logic_vector (15 downto 0);
	signal shift_out : std_logic_vector (15 downto 0);
	signal mux_out : std_logic_vector (15 downto 0);

begin
	arith : arithmetic_unit port map(a, b, func(2 downto 0), arith_out, v, c);
	logic : logic_unit port map(a, b, func(2 downto 1), logic_out);
	shift : shifter_unit port map(b, func(3 downto 2), shift_out);
	mux : mux4_16bit port map(arith_out, logic_out, shift_out, shift_out, func(4 downto 3), mux_out);
	
	--wire directly
	f <= mux_out; 
	n <= mux_out(15);
	
	z <= ((mux_out(0) nor mux_out(1)) and (mux_out(2) nor mux_out(3))) and
	     ((mux_out(4) nor mux_out(5)) and (mux_out(6) nor mux_out(7))) and
	     ((mux_out(8) nor mux_out(9)) and (mux_out(10) nor mux_out(11))) and
	     ((mux_out(12) nor mux_out(13)) and (mux_out(14) nor mux_out(15))) after 4ns; --log2(16)
end behavioral;

