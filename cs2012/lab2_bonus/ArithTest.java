import java.io.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

//-------------------------------------------------------------------------
/**
 *  Test class for Arith class
 *
 *  @author  
 *
 *  @version 04/02/14 20:01:43
 */

@RunWith(JUnit4.class)
public class ArithTest
{
  public ArithTest(){
    //constructor test
    new Arith();
  }

  //~ Unit tests ..........................................................
  @Test
  public void validateGoodPrefix(){
    String[] prefix = new String[]{"+", "7", "*", "-", "1", "2", "3"};
    assertEquals ( "validatePrefix good expression", true, Arith.validatePrefixOrder(prefix));
  }

  @Test
  public void validateGoodPostfix(){
    String[] postfix = new String[]{"7", "1", "2", "-", "3", "*", "+"};
    assertEquals ( "validatePostfix good expression", true, Arith.validatePostfixOrder(postfix));
  }

  @Test
  public void validateBadPrefix(){
    String[] prefix = new String[]{"+", "7", "1", "2", "3"};
    String[] prefix2 = new String[]{"-", "/", "-", "1", "2"};
    assertEquals ( "validatePrefix overfull operators", false, Arith.validatePrefixOrder(prefix));
    assertEquals ( "validatePrefix starved operators", false, Arith.validatePrefixOrder(prefix2));
  }

  @Test
  public void validateBadPostfix(){
    String[] postfix = new String[]{"7", "1", "-", "6", "9", "/"};
    String[] postfix2 = new String[]{"7", "1", "-", "+"};
    assertEquals ( "validatePostfix overfull operators", false, Arith.validatePostfixOrder(postfix));
    assertEquals ( "validatePostfix starved operators", false, Arith.validatePostfixOrder(postfix2));
  }



  @Test
  public void evaluatePrefix(){
    String[] prefix = new String[]{"+", "7", "*", "-", "1", "2", "3"};
    assertEquals ( "evaluatePrefix", 4, Arith.evaluatePrefixOrder(prefix));

    String[] prefix2 = new String[]{"/", "+", "5", "7", "3"};
    assertEquals ( "evaluatePrefix", 4, Arith.evaluatePrefixOrder(prefix2));
  }

  @Test
  public void evaluatePostfix(){
    String[] postfix = new String[]{"7", "1", "2", "-", "3", "*", "+"};
    assertEquals ( "evaluatePostfix", 4, Arith.evaluatePostfixOrder(postfix));

    String[] postfix2 = new String[]{"5", "7", "+", "3", "/"};
    assertEquals ( "evaluatePostfix", 4, Arith.evaluatePostfixOrder(postfix2));
  }



  @Test
  public void prefixToPostfix()
  {
    String[] prefix = new String[]{"+", "7", "*", "-", "1", "2", "3"};
    assertArrayEquals( new String[]{"7", "1", "2", "-", "3", "*", "+"}, Arith.convertPrefixToPostfix(prefix) );
  }

  @Test
  public void postfixToPrefix()
  {
    String[] postfix = new String[]{"7", "1", "2", "-", "3", "*", "+"};
    assertArrayEquals( new String[]{"+", "7", "*", "-", "1", "2", "3"}, Arith.convertPostfixToPrefix(postfix) );
  }



  @Test
  public void prefixToInfix()
  {
    String[] prefix = new String[]{"+", "7", "*", "-", "1", "2", "3"};
    assertArrayEquals( new String[]{"(", "7", "+", "(", "(", "1", "-", "2", ")", "*", "3", ")", ")"}, Arith.convertPrefixToInfix(prefix) );
  
    String[] prefix2 = new String[]{"+", "*", "-", "1", "2", "3", "-", "10", "+", "3", "/", "6", "3"};
    assertArrayEquals( new String[]{"(", "(", "(", "1", "-", "2", ")", "*", "3", ")", "+", "(", "10", "-", "(", "3", "+", "(", "6", "/", "3", ")", ")", ")", ")"}, Arith.convertPrefixToInfix(prefix2) );
  }

  @Test
  public void postfixToInfix()
  {
    String[] postfix = new String[]{"7", "1", "2", "-", "3", "*", "+"};
    assertArrayEquals( new String[]{"(", "7", "+", "(", "(", "1", "-", "2", ")", "*", "3", ")", ")"}, Arith.convertPostfixToInfix(postfix) );
  
    String[] prefix2 = new String[]{"1", "2", "-", "3", "*", "10", "3", "6", "3", "/", "+", "-", "+"};
    assertArrayEquals( new String[]{"(", "(", "(", "1", "-", "2", ")", "*", "3", ")", "+", "(", "10", "-", "(", "3", "+", "(", "6", "/", "3", ")", ")", ")", ")"}, Arith.convertPostfixToInfix(prefix2) );
  
    String[] prefix3 = new String[]{"a", "b", "c", "-", "+", "d", "e", "-", "f", "g", "-", "h", "+", "/", "*"};
    assertArrayEquals( new String[]{"(", "(", "a", "+", "(", "b", "-", "c", ")", ")", "*", "(", "(", "d", "-", "e", ")", "/", "(", "(", "f", "-", "g", ")", "+", "h", ")", ")", ")"}, Arith.convertPostfixToInfix(prefix3) );  
  }

  @Test
  public void codeCoverage(){
    Arith.StackArray<Integer> stack = new Arith.StackArray<Integer>(2);
    assertEquals( "popping empty stack", null, stack.pop() );
    assertEquals( "pushing empty stack", true, stack.push(0) );
    assertEquals( "pushing non-full stack", true, stack.push(5) );
    assertEquals( "pushing full stack", false, stack.push(3) );
  }
}




