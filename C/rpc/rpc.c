#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "stack.h"

int main(int argc, char** argv){
	stack* st = stack_create();

	const char* error_str = NULL;
	int error_at = 0;
	
	for (int i = 1; i < argc && !error_str; i++){
		char* token = argv[i];
		int len = strlen(token);
		if (len == 1 && !isdigit(*token)){
			if (stack_contains(st) == 2){
				double l, r;
				stack_pop(st, &r);
				stack_pop(st, &l);
				
				switch (*token){
					case '+':
						stack_push(st, l + r);
						break;

					case '-':
						stack_push(st, l - r);
						break;

					case 'x': 
					case 'X':
						stack_push(st, l * r);
						break;

					case '/':
						stack_push(st, l / r);
						break;

					default:
						error_at = i;
						error_str = "Unknown operator.";
						break;
				}
			}
			
			else{
				error_at = i;
				error_str = "Stack underflow. Too many operators.";
			}
		}

		else{
			int negative = 0;
			int length = strlen(token);
			if (*token == '-'){
				negative = 1;
				token++;
				length--;
			}
			
			char* end;
			double val = strtod(token, &end);
			
			if (end == token + length){
				if (negative) val = -val;
				stack_push(st, val);
			}
			
			else{
				error_at = i;
				error_str = "Illegal token.";
			}
		}
	}

	if (stack_contains(st) == 1){
		//there is only one element left, the final result
		double result;
		stack_pop(st, &result);
		
		char buf[512];
		int len = snprintf(buf, 512, "%f", result);
		printf("len at %d\n", len);
		
		//remove trailing zeros
		char* end = buf + (len - 1);
		while (*end == '0'){
			*end-- = '\0';
		}
		
		if (*end == '.'){
			//also remove the dot if there's nothing after it
			*end = '\0';
		}
		
		printf("%s\n", buf);
	}
	
	else{
		//there are operands remaining, that haven't been consumed by operations
		error_at = argc;
		error_str = "Stack not empty. Too many operands.";
	}
	
	stack_delete(st);
	
	if (error_str){
		int space = 0;
		for (int i = 1; i < argc; i++){
			fprintf(stderr, "%s ", argv[i]);
			if (i < error_at) space += strlen(argv[i]) + 1;
		}
		putc('\n', stderr);
		
		for (int i = 0; i < space; i++) putc(' ', stderr);
		fprintf(stderr, "^\nError: %s\n", error_str);
		
		return 1;
	}
	
	else return 0;
}

