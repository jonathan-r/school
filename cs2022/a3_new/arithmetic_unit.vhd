library ieee;
use ieee.std_logic_1164.all;

entity arithmetic_unit is
    port ( a : in  std_logic_vector (15 downto 0);
           b : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (2 downto 0);
           result : out  std_logic_vector (15 downto 0);
           v : out  std_logic;
           c : out  std_logic );
end arithmetic_unit;

architecture behavioral of arithmetic_unit is
	component full_adder is
		port ( carry_in  : in   std_logic;
				  a         : in   std_logic;
				  b         : in   std_logic;
				  sum       : out  std_logic;
				  carry_out : out  std_logic);
	end component;
	
	--second adder input
	signal adder_b : std_logic_vector (15 downto 0);
	
	--adder outputs
	signal adder_sum : std_logic_vector (15 downto 0);
	signal adder_carry : std_logic_vector (16 downto 0); --[i] is input, [i+1] is output

begin
	--ripple adder
	gen_adder : 
		for i in 0 to 15 generate
			adder : full_adder port map(adder_carry(i), a(i), adder_b(i), adder_sum(i), adder_carry(i + 1));
		end generate gen_adder;
		
	--directly connect adder_sum to the unit output
	result <= adder_sum;
	
	--carry in
	adder_carry(0) <= op(0) and (op(2) nand op(1)) after 2ns;
	
	--overflow and carry out, according to https://www.scss.tcd.ie/Michael.Manzke/CS2022/08-CS2022_p2_eighth_lecture.pdf
	v <= adder_carry(16) xor adder_carry(15) after 1ns;
	c <= adder_carry(16);
	
	process(a, b, op) begin
		--select b adder input
		case op is
			when "000" => adder_b <= x"0000" after 1ns; --result = a
			when "001" => adder_b <= x"0000" after 1ns; --result = a + 1
			when "010" => adder_b <= b after 1ns; --result = a + b  
			when "011" => adder_b <= b after 1ns; --result = a + b + 1
			when "100" => adder_b <= not b after 2ns; --result = a + b'
			when "101" => adder_b <= not b after 2ns; --result = a + b' + 1
			when "110" => adder_b <= x"FFFF" after 1ns; --result = a - 1
			when "111" => adder_b <= x"0000" after 1ns; --result = a
			when others => adder_b <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

