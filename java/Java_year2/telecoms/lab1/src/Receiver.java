import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;

public class Receiver {
	static final int DEFAULT_PORT = 50001;
	
	private DatagramSocket socket;
	private InetSocketAddress address;
	
	private byte[] data = new byte[1024];
	private int dataLength;
	
	private boolean good = false;

	public Receiver() {
		this(DEFAULT_PORT);
	}
	
	public Receiver(int port) {
		try {
			socket = new DatagramSocket(port);
			good = true;
		}
		
		catch(java.lang.Exception e) {
			e.printStackTrace();
		}
	}

	private void printContent() {
		System.out.println("Packet length:" + (dataLength - 1) + " + 1 byte for the CRC remainder");
		
		byte crcOut = CRC.computeCRC(data, dataLength, CRC.defaultPolynomial);
		String strCRC = Bits.toBitString(crcOut, 0);
		
		System.out.print(strCRC);
		if (crcOut == 0) System.out.println(" -> data OK");
		else System.out.println(" -> Something went wrong");
		
		System.out.println();
	}
	
	public boolean isGood(){
		return good;
	}

	public void start() {
		DatagramPacket packet;
		
		try {
			packet = new DatagramPacket(data, data.length);
			System.out.println("Waiting for packet...");
			
			socket.receive(packet);
			dataLength = packet.getLength();
				
			System.out.println("Received packet");
			
			printContent();
		}
		catch(java.lang.Exception e) {
			e.printStackTrace();
		}		
	}


	public static void main(String[] args) {
		Receiver r;
	
		if (args.length == 1){
			int port= Integer.parseInt(args[0]);
			r = new Receiver(port);	
			
		}
		
		else if (args.length == 0){
			r = new Receiver();
		}
		
		else{
			System.out.println("Usage: receiver <port>");
			return;
		}
		
		if (r.isGood()) r.start();
		System.out.println("Program completed");
	}
}
