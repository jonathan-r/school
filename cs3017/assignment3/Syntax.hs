module Syntax where

import Text.PrettyPrint.HughesPJ ( integer, text, (<>), Doc, render, parens )

-- Arithmetic Expressions
data Exp = CNum Integer                   -- n
            | Var String                  -- L (locations)
            | Add Exp Exp                 -- e + e'
            | Sub Exp Exp                 -- e - e'
            | Div Exp Exp                 -- e / e'
            | Throw Integer               -- throw n
            | TryCatch Exp String Exp     -- try e1 catch l with e2  


--
-- Pretty-print functions of the above datatypes
--

instance Show Exp where
  show = render . pp 0
  
pp :: Int -> Exp -> Doc
pp _ (CNum n)           = integer n
pp _ (Var  s)           = text s
pp i (Add e1 e2)        = parens $ pp i e1 <> text " + " <> pp i e2
pp i (Sub e1 e2)        = parens $ pp i e1 <> text " - " <> pp i e2
pp i (Div e1 e2)        = parens $ pp i e1 <> text " / " <> pp i e2
pp _ (Throw n)          = text "throw " <> integer n
pp i (TryCatch e1 l e2) = (  text "try "     <> parens (pp (i+1) e1)) 
                          <> text " catch " <> text l 
                          <> text " with "  <> parens (pp (i+1) e2)