library ieee;
use ieee.std_logic_1164.all;
use work.array_types.all;


entity register_file is
	port ( write_reg   : in std_logic;
		    dest_select : in   std_logic_vector (2 downto 0);
			 data_in     : in std_logic_vector (15 downto 0);
		    clk         : in   std_logic;
		    a_select    : in   std_logic_vector (2 downto 0);
			 b_select    : in   std_logic_vector (2 downto 0);
			 a_out       : out  std_logic_vector (15 downto 0);
			 b_out       : out  std_logic_vector (15 downto 0) );
end register_file;

architecture behavioral of register_file is
	component demux_3to8_1bit is
		port ( data : in std_logic;
			  addr : in  std_logic_vector (2 downto 0);
           output : out std_logic_vector (7 downto 0) );
	end component;

	component register_16bit is
		port ( load : in  std_logic;
			    clk  : in  std_logic;
			    d    : in  std_logic_vector (15 downto 0);
			    q    : out std_logic_vector (15 downto 0) );
	end component;

	component mux8_16bit is
		port ( s      : in  std_logic_vector (2 downto 0);
			   in_data : in  data_array;
			   z       : out std_logic_vector (15 downto 0) );
	end component;

	signal load : std_logic_vector (7 downto 0); --load demux to load of each register
	signal reg_out_data : data_array; --register output to bus muxes

begin
	write_demux : demux_3to8_1bit port map(write_reg, dest_select, load);
	gen_reg : for i in 0 to 7 generate
		regx : register_16bit port map(load(i), clk, data_in, reg_out_data(i));
	end generate gen_reg;
	bus_a_mux : mux8_16bit port map(a_select, reg_out_data, a_out);
	bus_b_mux : mux8_16bit port map(b_select, reg_out_data, b_out);
end behavioral;

