#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <vector>
#include <iostream>
#include <fstream>

#include "shader_program.h"
#include "vbo.h"
#include "vertex.h"

#include "maths/mat4.h"
#include "maths/vec.h"

#ifdef GLSL120
#define SHADER_ROOT_NAME "shaders/shader_120"
#else
#define SHADER_ROOT_NAME "shaders/shader_330"
#endif

using namespace std;

//ico_sphere.cpp
void generateIcoSphere(int iter, vector<vertex>& sink);

ShaderProgram* mainShader = NULL;
VertexArrayObject<vertex>* mainModel = NULL;
VertexArrayObject<vertex>* arrowsModel = NULL;
vec3 translation;
vec3 scaling;
quat currentRot;

bool resetTransformation;
bool keysDown[256];
int lastDrawTime = 0;

void display() {
	int frameBeginTime = glutGet(GLUT_ELAPSED_TIME);
	lastDrawTime = frameBeginTime;

	if (resetTransformation) {
		quat ident; //defaults to no rotation
		float rotSize = vec3(currentRot.x, currentRot.y, currentRot.z).length();
	
		//detect if the transformation is close enough to the origin to snap seamlessly
		if (rotSize > 0.005f or fabs(translation * vec3(1.0f)) > 0.01f or fabs(scaling * vec3(1.0f) - 3.0f) > 0.005f) {
			translation *= 0.80f;
			currentRot = quat::lerpRot(currentRot, ident, std::min(1.0f, 0.05f * (1.0f + 1.0f / rotSize)));
			scaling -= (scaling - vec3(1.0)) * 0.1;
		}
		else {
			//snap to default
			resetTransformation = false;
			currentRot = ident;
			scaling.set(1.0f, 1.0f, 1.0f);
			translation.set(0.0f, 0.0f, 0.0f);
		}
	}
	else {
		//take keyboard input
		//update - TODO: move to idle function

		//translation
		const float trc = 0.3f;
		if (keysDown['w']) translation.y += trc;
		else if (keysDown['s']) translation.y -= trc;
		
		if (keysDown['a']) translation.x -= trc;
		else if (keysDown['d']) translation.x += trc;
	
		if (keysDown['q']) translation.z += trc;
		else if (keysDown['e']) translation.z -= trc;

		//scale input
		const float trs = 1.1f;
		if (keysDown['r']) scaling.x *= trs;
		else if (keysDown['f']) scaling.x /= trs;
		
		if (keysDown['t']) scaling.y *= trs;
		else if (keysDown['g']) scaling.y /= trs;
		
		if (keysDown['y']) scaling.z *= trs;
		else if (keysDown['h']) scaling.z /= trs;
		
		//rotation input
		vec3 rots(0.0f);
		const float change = 0.05f;
		if (keysDown['u']) rots.z = fmodf(rots.z - change, 2*M_PI);
		else if (keysDown['o']) rots.z = fmodf(rots.z + change, 2*M_PI);
	
		if (keysDown['i']) rots.x = fmodf(rots.x - change, 2*M_PI);
		else if (keysDown['k']) rots.x = fmodf(rots.x + change, 2*M_PI);
	
		if (keysDown['j']) rots.y = fmodf(rots.y - change, 2*M_PI);
		else if (keysDown['l']) rots.y = fmodf(rots.y + change, 2*M_PI);
	
		quat applyRot = quat::flightRotation(rots.y, rots.x, rots.z);
		//applyRot.normalize();
	
		currentRot = applyRot * currentRot; // quat::chainRotation(currentRot, applyRot);
		currentRot.normalize();
	}
	mat4 sca = mat4::scale(scaling);
	mat4 rot = mat4::quaternionRotation(currentRot);
	mat4 tra = mat4::translation(translation);
	mat4 matrix = tra * rot * sca;

	//cout << matrix.represent() << endl;

	GLuint prog = mainShader->getProgObj();
	glUseProgram(prog);
	
	GLuint matLoc = glGetUniformLocation(mainShader->getProgObj(), "matrix");
	GLuint lightFractionLoc = glGetUniformLocation(mainShader->getProgObj(), "lightFraction");
	GLuint timeLoc = glGetUniformLocation(mainShader->getProgObj(), "time");
	
	glUniform1f(timeLoc, (float)frameBeginTime / 1000.0f);
	
	//actual display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//no transformation but scaling
	mat4 mat1 = mat4::scale(vec3(0.7f));
	glUniformMatrix4fv(matLoc, 1, GL_FALSE, mat1.data);

	//draw arrows and model, scaled
	arrowsModel->bind(prog);
	glUniform1f(lightFractionLoc, 0.0f);
	glDrawArrays(GL_LINES, 0, arrowsModel->getVerticesAssigned());
	
	mainModel->bind(prog);
	glUniform1f(lightFractionLoc, 1.0f);
	glDrawArrays(GL_TRIANGLES, 0, mainModel->getVerticesAssigned());

	//arrows, transformed
	arrowsModel->bind(prog);
	glUniform1f(lightFractionLoc, 0.0f);
	glDrawArrays(GL_LINES, 0, arrowsModel->getVerticesAssigned());
	
	//full light affect, main model tranformed
	glUniform1f(lightFractionLoc, 1.0f);
	glUniformMatrix4fv(matLoc, 1, GL_FALSE, matrix.data);
	mainModel->bind(prog);
	glDrawArrays(GL_TRIANGLES, 0, mainModel->getVerticesAssigned());

    glutSwapBuffers();
	int frameEndTime = glutGet(GLUT_ELAPSED_TIME);	
}

void idle() {	
	int time = glutGet(GLUT_ELAPSED_TIME);
	if (time - lastDrawTime > 16) glutPostRedisplay();
}

void keyboardDown(unsigned char key, int x, int y) {
	//cout << "key down: " << key << endl;
	keysDown[key] = true;
	if (key == ' ' and !resetTransformation) resetTransformation = true;
	else if (key == 0x1B) glutLeaveMainLoop();
}

void keyboardUp(unsigned char key, int x, int y) {
	//cout << "key up: " << key << endl;
	keysDown[key] = false;
}

void specDown(int key, int x, int y) {
	//keysDown[key] = true;
}

void specUp(int key, int x, int y) {
	//keysDown[key] = false;
}

void init()
{
	mainShader = new ShaderProgram(SHADER_ROOT_NAME ".vert", SHADER_ROOT_NAME ".frag");	
	mainModel = new VertexArrayObject<vertex>();
	arrowsModel = new VertexArrayObject<vertex>();

	vector<vertex> iso;
	generateIcoSphere(4, iso);
	mainModel->assign(iso.size(), iso.data(), GL_STATIC_DRAW);

	//generate arrows - plain lines
	vertex arrows[6];
	arrows[0].v.set(0.0f, 0.0f, 0.0f); arrows[0].c.set(1.0f, 0.0f, 0.0f);
	arrows[1].v.set(1.0f, 0.0f, 0.0f); arrows[1].c.set(1.0f, 0.0f, 0.0f);
	arrows[2].v.set(0.0f, 0.0f, 0.0f); arrows[2].c.set(0.0f, 1.0f, 0.0f);
	arrows[3].v.set(0.0f, 1.0f, 0.0f); arrows[3].c.set(0.0f, 1.0f, 0.0f);
	arrows[4].v.set(0.0f, 0.0f, 0.0f); arrows[4].c.set(0.0f, 0.0f, 1.0f);
	arrows[5].v.set(0.0f, 0.0f, 1.0f); arrows[5].c.set(0.0f, 0.0f, 1.0f);
	arrowsModel->assign(6, arrows, GL_STATIC_DRAW);

	memset(keysDown, 0, 256);
	scaling.set(1.0f, 1.0f, 1.0f);
	translation.set(0.0f, 0.0f, 0.0f);

	glEnable(GL_CULL_FACE);
	glClearColor(0.3f, 0.4f, 0.2f, 1.0f);
	glClearDepth(0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_GREATER);
}

int main(int argc, char** argv) {

	// Set up the window
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("no more triangles");
	// Tell glut where the display function is
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboardDown);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(specDown);
	glutSpecialUpFunc(specUp);

	 // A call to glewInit() must be done after glut is initialized!
    GLenum res = glewInit();
	// Check for any errors
    if (res != GLEW_OK) {
      fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
      return 1;
    }
	// Set up your objects and shaders
	init();
	// Begin infinite event loop
	glutMainLoop();

	delete mainShader;
	delete mainModel;
	delete arrowsModel;

	cout << "graceful exit" << endl;
    return 0;
}

