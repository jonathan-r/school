library ieee;
use ieee.std_logic_1164.all;


entity shifter_unit is
    port ( input : in  std_logic_vector (15 downto 0);
           op : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
end shifter_unit;

architecture behavioral of shifter_unit is

begin
	process(input, op) begin
		--end bits
		case op is
			when "00" => output(0) <= input(0) after 1ns;
						 output(15) <= input(15) after 1ns;

			when "01" => output(0) <= input(1) after 1ns;
						 output(15) <= '0' after 1ns;

			when "10" => output(0) <= '0' after 1ns;
						 output(15) <= input(14) after 1ns;

			when others => output(0) <= 'U';
						   output(15) <= 'U';
		end case;
		
		--middle bits, assumed to be the same as above
		for i in 1 to 14 loop
			case op is
				when "00" => output(i) <= input(i) after 1ns;
				when "01" => output(i) <= input(i+1) after 1ns;
				when "10" => output(i) <= input(i-1) after 1ns;
				when others => output(i) <= 'U';
			end case;
		end loop;
	end process;
end behavioral;

