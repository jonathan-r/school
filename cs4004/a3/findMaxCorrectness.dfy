predicate isMax(s: seq<int>, lo: int) {
    0 <= lo < |s| && forall i :: 0 <= i < |s| ==> s[lo] >= s[i]
}

predicate isMaxBound(s: seq<int>, lo: int, hi: int) {
    0 <= lo <= hi < |s| && forall i ::0 <= i <= lo || hi <= i < |s| ==> (s[lo] >= s[i] || s[hi] >= s[i])
}

method findMax(s: seq<int>) returns (lo: int)
    requires |s| > 0
    ensures isMax(s, lo)
{
    lo := 0;
    var hi := |s|-1;
    assert isMaxBound(s, lo, hi);

    while (lo < hi)
        invariant 0 <= lo <= hi < |s|
        decreases hi - lo
        invariant isMaxBound(s, lo, hi)
    {
        if (s[lo] <= s[hi]) {
            assert isMaxBound(s, lo, hi) && s[lo] <= s[hi] ==> isMaxBound(s, lo+1, hi);
            assert isMaxBound(s, lo+1, hi);
            lo := lo + 1;
            assert 0 <= lo <= hi < |s| && forall i :: 0 <= i <= lo || hi <= i < |s| ==> (s[lo] >= s[i] || s[hi] >= s[i]);
        } else {
            assert isMaxBound(s, lo, hi) && s[lo] > s[hi] ==> isMaxBound(s, lo, hi-1);
            assert isMaxBound(s, lo, hi-1);
            hi := hi - 1;
            assert 0 <= lo <= hi < |s| && forall i :: 0 <= i <= lo || hi <= i < |s| ==> (s[lo] >= s[i] || s[hi] >= s[i]);
        }
    }

    assert lo == hi;
    assert forall i :: 0 <= i <= lo || hi <= i < |s| ==> (s[lo] >= s[i] || s[hi] >= s[i]);
    assert forall i :: 0 <= i <= lo || hi <= i < |s| ==> (s[lo] >= s[i]);
    assert forall i :: 0 <= i < |s| ==> s[lo] >= s[i];
    assert isMax(s, lo);
}

