#include <stdio.h>
#include <time.h>

int ackermann(int x, int y){
	if (x == 0){
		return y + 1;
	}
	else if (y == 0){
		return ackermann(x-1, 1);
	}
	else{
		return ackermann(x-1, ackermann(x, y-1));
	}
}

//http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
struct timespec diff(struct timespec start, struct timespec end){
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} 
	else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

int main(int argc, char** argv){
	if (argc == 3){
		int x = strtol(argv[1], NULL, 10);
		int y = strtol(argv[2], NULL, 10);

		int i;
		int r = 0;
		struct timespec time1, time2;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);

		const int times = 20001;
		for (i = 0; i < times; i++){
			r = r ^ ackermann(x, y);
		}

		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
		struct timespec elapsed = diff(time1, time2);
		double sec = (double) elapsed.tv_sec / times + ((double) elapsed.tv_nsec / times) / 1e9;

		printf("%d; took %f seconds\n", r, sec);
	}

	else{
		printf("usage: %s x y\n", argv[0]);
	}

	return 0;
}
 
