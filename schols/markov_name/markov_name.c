#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "markov_name.h"

void markov_init(markov_state* state){
	memset(state, 0, sizeof(markov_state));
}

int letter_to_base(char c){
	if (c == 0) return MARKOV_LETTERS;
	else if (isalpha(c)) return tolower(c) - 'a';
	else return -1;
}

char base_to_letter(int b){
	if (b >= 0 && b < MARKOV_LETTERS) return b + 'a';
	else return 0;
}

void markov_train(markov_state* state, char* str, int len){
	int first = letter_to_base(tolower(str[0]));
	if (first != -1){
		int last = first;
		state->opener[first]++;
		state->opener_sum++;
	
		for (int i = 1; i <= len; i++){
			int base = (i == len)? MARKOV_LETTERS : letter_to_base(tolower(str[i]));
			if (base != -1){
				state->transition[last][base]++;
				state->transition_sum[last]++;
			}
			else return;
			last = base;
		}
	}
}

void markov_train_file(markov_state* state, const char* filename){
	FILE* f = fopen(filename, "rb"); //binary so that fseek gives the correct size

	if (f){
		fseek(f, 0, SEEK_END);
		long size = ftell(f);
		rewind(f);
		
		char* text = malloc(size + 1);
		fread(text, size, 1, f);
		fclose(f);
		text[size] = 0; //NULL terminator
		
		char* start = NULL;
		char* c = text;
		while (c != text + size + 1){
			if (isalpha(*c)){
				if (!start) start = c;
			}
			else{
				if (start){
					markov_train(state, start, (int)(c - start));
					start = NULL; //look for the next word
				}
			}
			
			c++;
		}
		
		free(text);
	}
	
	else{
		perror("could not open file");
	}
}

int markov_transition(markov_state* state, int from){
	int value = rand() % state->transition_sum[from];
	int id = 0;
	for (id = 0; id <= MARKOV_LETTERS && value > 0; id++){
		value -= state->transition[from][id]; //find the right one
	}
	if (value < 0) id--; //overshot
	return id;
}
	
char* markov_word(markov_state* state, int min_length, int max_length){
	for (int i = 0; i < MARKOV_LETTERS; i++){
		if (state->transition_sum[i] == 0){
			fprintf(stderr, "markov transition row is empty for %c\n", base_to_letter(i));
			return NULL;
		}
	}
	
	char* buf = malloc(max_length + 1);
	buf[max_length] = 0;
	
	int opener_value = rand() % state->opener_sum;
	int opener_id = 0;
	while (opener_value > 0) opener_value -= state->opener[opener_id++]; //find the right one
	if (opener_value < 0) opener_id--; //overshot
	
	buf[0] = base_to_letter(opener_id);
	int last = opener_id;
	
	for (int i = 1; i < max_length; i++){
		int base = markov_transition(state, last);
		buf[i] = base_to_letter(base);
		if (base == MARKOV_LETTERS){
			if (i < min_length) i--; //redo
			else break;
		}
		
		else last = base;
	}
	
	return buf;
}

char* markov_name(markov_state* state, int min_length, int max_length){
	const char* vowels = MARKOV_VOWELS;
	
	while (1){
		char* name = markov_word(state, min_length, max_length);
		if (!name) return NULL; //can't generate any names
		else{
			int bad = 0;
			int len = strlen(name);
			
			//no double letter at the begining
			if (name[0] == name[1]) continue;
			
			//no triple consonants
			if (len >= 3){
				char a = name[0];
				char b = name[1];
				
				int ca = !strchr(vowels, name[0]);
				int cb = !strchr(vowels, name[1]);
				for (int i = 2; i < len; i++){
					char c = name[i];
					int cc = !strchr(vowels, name[i]);
					
					if ((cc && cb && ca) || (c == b == a)){
						bad = 1;
						break;
					}
					
					a = b; ca = cb;
					b = c; cb = cc;
				}
			}
			
			if (bad) continue;
			
			//must have at least one vowels
			bad = 1;
			for (int i = 0; i < len; i++){
				if (strchr(vowels, name[i])) bad = 0;
			}
			
			if (bad) continue;
			
			return name;
		}
	}
}

