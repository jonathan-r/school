#!/bin/bash

USAGE="usage: flash.sh hex_file serial_tty";
[[ -n "$1" ]] || { echo "$USAGE"; exit 1; }
[[ -n "$2" ]] || { echo "$USAGE"; exit 1; }

lpc21isp -hex -debug3 -control -verify $1 $2 230400 14746
