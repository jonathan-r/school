library ieee;
use ieee.std_logic_1164.all;
 
entity reg_tb is
end reg_tb;
 
architecture behavior of reg_tb is 
	component register_16bit
	port( load : in  std_logic;
	      clk : in  std_logic;
	      d : in  std_logic_vector(15 downto 0);
	      q : out  std_logic_vector(15 downto 0) );
	end component;

	signal load_in : std_logic := '0';
	signal clk_in : std_logic := '0';
	signal d_in : std_logic_vector(15 downto 0) := (others => '0');
	signal q_out : std_logic_vector(15 downto 0);

	constant clk_period : time := 1 ns;
	
begin
	uut: register_16bit port map (load_in, clk_in, d_in, q_out);

	clk_process :process begin
		clk_in <= '0';
		wait for clk_period/2;
		clk_in <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period;

		--load a value
		d_in <= x"abab";
		load_in <= '1';
		wait for clk_period;
		assert q_out = x"abab";

		--make sure the value stays past a clock even if the input data changes
		d_in <= x"0123";
		load_in <= '0';
		wait for clk_period;
		assert q_out = x"abab";

		--change the value
		d_in <= x"fafc";
		load_in <= '1';
		wait for clk_period;
		assert q_out = x"fafc";
		
		wait;
	end process;
end;
