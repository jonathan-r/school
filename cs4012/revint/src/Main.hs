module Main where

import System.Environment

main :: IO ()
main = do
	args <- getArgs
	case args of
		[srcFileName] -> do
			script <- readFile srcFileName
			runStep lines script

		_ ->
			putStrLn "Usage: revint <script>"

