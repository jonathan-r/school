#ifndef INTERNAL_H
#define INTERNAL_H

#define DEBUGGING(_x)

struct complex {
  float real;
  float imag;
};

struct matrix_size {
  int rows;
  int cols;
};

//used for all communication
#define TAG_VALUE 0
#define MASTER_RANK 0

/* write matrix to stdout */
void write_out(struct complex ** a, int dim1, int dim2);

/* create new empty matrix */
struct complex ** new_empty_matrix(int dim1, int dim2);

/* create a matrix and fill it with random numbers */
struct complex ** gen_random_matrix(int dim1, int dim2);

/* take a copy of the matrix and return in a newly allocated matrix */
struct complex ** copy_matrix(struct complex ** source_matrix, int dim1, int dim2);

/* free any matrix */
void free_matrix(struct complex ** matrix);

/* check the sum of absolute differences is within reasonable epsilon */
void check_result(struct complex ** result, struct complex ** control, int dim1, int dim2);

/* multiply matrix A times matrix B and put result in matrix C */
void matmul(struct complex ** A, struct complex ** B, struct complex ** C, int a_dim1, int a_dim2, int b_dim2);

#endif