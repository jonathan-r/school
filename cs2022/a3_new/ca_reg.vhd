library ieee;
use ieee.std_logic_1164.all;

entity ca_reg is port(
	addr : in std_logic_vector(7 downto 0);
	branch : in std_logic;
	clock, reset : in std_logic;
	output : inout std_logic_vector(7 downto 0) );
end ca_reg;

architecture behavioral of ca_reg is
	component half_adder is port(
		a, b       : in std_logic;
		sum, carry : out std_logic );
	end component;

	signal adder_sum : std_logic_vector(7 downto 0);
	signal adder_carry : std_logic_vector(7 downto 0); --last bit is not needed
	
begin
	--incrementer, computes (output + 1) into adder_sum:
	--the first adder is a half adder whose first operand is the first bit of output 
	--and whose second operand is always 1(hence the increment)
	adder_sum(0) <= not output(0) after 1ns;
	adder_carry(0) <= output(0);
	
	--the remaining half adders operate one the corresponding bit of the output and the carry out of the previous adder
	gen_adders : for i in 1 to 7 generate
		--adder_sum(i) <= output(i) xor adder_carry(i - 1) after 1 ns;
		--adder_carry(i) <= output(i) and adder_carry(i - 1) after 1 ns;
		bit_adder : half_adder port map(output(i), adder_carry(i - 1), adder_sum(i), adder_carry(i));
	end generate gen_adders;
	
	--set the register on the clock edge
	process(reset, clock) begin
		if reset = '1' then
			output <= x"00" after 1ns;
		
		elsif clock'event and clock = '1' then
			--select between the incremented internal address or the external address input, based on the branch input
			if branch = '1' then
				output <= addr after 2ns;
			else
				output <= adder_sum after 2ns;
			end if;
		end if;
	end process;
end behavioral;

