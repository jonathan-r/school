
//intersection points and vertical length, for the comparison function.
//this makes the function thread unsafe, in that there can only be one instance of the sorting part at once, 
//otherwise these values will be clobbered.
int hPoint, vPoint, vLen;

int sortCmp(const void* left, const void* right){
	const char* a = (const char*) left;
	const char* b = (const char*) right;
	
	int lenA = strlen(a);
	int lenB = strlen(b);
	
	if (lenA < lenB) return -1;
	else if (lenA > lenB) return 1;
	else {
		char cA, cB;
		if (lenA == vLen){
			cA = a[vPoint];
			cB = b[vPoint];
		}
		else{
			cA = a[hPoint];
			cB = b[hPoint];
		}
		
		return cA - cB;
	}
}

void completeCrosswordFragment(int vertical, int verticalIntersection, int horizontal, int horizontalIntersection, char** dict, int dictLength){
	hPoint = horizontalIntersection;
	vPoint = verticalIntersection;
	vLen = vertical;
	
	//sort the words such that the shorter words are first.
	//then, between the words that are of the same length, sort each word by the significant letter, 
	//that is, the letter that is being crossed with the other axis word
	qsort(dict, dictLength, sizeof(char*), sortCmp);
	
	
	if (vertical != horizontal){
		//cut the dictionary between the horizontal and vertical words
		int shorter = (vertical < horizontal) vertical : horizontal
		char** first = dict;
		char** second = dict;
		while(strlen(*second) == shorter) second++;
	
		//merge-intersection, that finds all the
	
	
