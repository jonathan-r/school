#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <time.h>

typedef struct point_struct{
	int x, y;
} point;

typedef struct line_struct{
	point* a;
	point* b;
	int dx, dy; //offset from each other
} line;

//slope, then line length
int line_cmp(const void* left, const void* right){
	line* l = (line*)left;
	line* r = (line*)right;
	
	/*if (l->slope == r->slope){
		if (l->length < r->length) return -1;
		else if (r->length == r->length) return 0;
		else return 1;
	}
	
	else{
		return l->slope - r->slope;
	}*/
	
	int ddx = l->dx - r->dx;
	if (ddx == 0){
		return l->dy - r->dy;
	}
	
	else return ddx;
}

int point_cmp(const void* left, const void* right){
	point* l = *((point**)left);
	point* r = *((point**)right);
	
	int dx = l->x - r->x;
	if (dx == 0){
		return l->y - r->y;
	}
	else return dx;
}


#define FILL_BUCKET do{ bucket[0]=a; bucket[1]=b; bucket[2]=c; bucket[3]=d; } while(0)
//a b
//c d
void print_para(point** bucket, int sort){
	if (sort) qsort(bucket, 4, sizeof(point*), point_cmp);
	printf("found ((%d %d), (%d %d), (%d %d), (%d %d))\n", bucket[0]->x, bucket[0]->y, bucket[1]->x, bucket[1]->y, bucket[2]->x, bucket[2]->y, bucket[3]->x, bucket[3]->y);
}

//O(n^4)
void find_naive(point* cities, int N){
	point* bucket[4];
	
	for (int i = 0; i < N; i++){
		point* a = cities + i;
		for (int j = i + 1; j < N; j++){
			point* b = cities + j;
			for (int k = j + 1; k < N; k++){
				point* c = cities + k;
				for (int l = k + 1; l < N; l++){
					point* d = cities + l;
					
					if (a->x - b->x == c->x - d->x){
						if (a->y - c->y == b->y - d->y){
							FILL_BUCKET;
							print_para(bucket, 1);
						}
					}
					
					if (b->x - a->x == c->x - d->x){
						if (b->y - c->y == a->y - d->y){
							FILL_BUCKET;
							print_para(bucket, 1);
						}
					}
				}
			}
		}
	}
}


int bucket_cmp(const void* left, const void* right){
	point** l = *((point***)left); //getting a bit ridiculous now
	point** r = *((point***)right);
	
	for (int i = 0; i < 4; i++){
		int cmp = point_cmp(&l[i], &r[i]);
		if (cmp != 0) return cmp;
	}
	
	return 0;
}


void find_parallelograms(point* cities, int N){
	int L = N * (N / 2); //from (n * n / 4) to (n * n / 2)
	line* lines = malloc(L * sizeof(line));
	
	int li = 0;
	
	//generate one line between every two points
	//O(n^2)
	for (int i = 0; i < N; i++){
		point* a = &cities[i];
		
		for (int j = i + 1; j < N; j++){
			point* b = &cities[j];
		
			int dx = a->x - b->x;
			int dy = a->y - b->y;
			
			assert(dx != 0 || dy != 0);
			
			//reverse it so it always points to the right
			if (dx < 0){
				dx = -dx;
				dy = -dy;
			}
			
			int adx = abs(dx);
			int ady = abs(dy);
			
			//the sign is the same       or  either one is 0    or   it is a 45 degree rising slope
			if (dx == 0 || (dx > 0 && dy > 0) || dx == dy){
				line* l = &lines[li];
				l->a = a;
				l->b = b;
							
				l->dx = dx;
				l->dy = dy;
				li++;
				
				printf("added %d %d (%d %d) (%d %d)\n", dx, dy, a->x, a->y, b->x, b->y);
			}
		}
	}
	
	printf("made %d lines\n", li);
	
	//sort the lines according to offset
	//up to O(n^2), but likely O(n*log2(n))
	qsort(lines, li, sizeof(line), line_cmp);
	
	point* bucket[4];
	line* last = NULL;
	for (int i = 0; i < li; i++){
		line* l = &lines[i];
	
		if (last){
			if (l->dx == last->dx && l->dy == last->dy){
				if ((l->a != last->a && l->a != last->b) && (l->b != last->a && l->b != last->b)){
					bucket[0] = l->a;
					bucket[1] = l->b;
					bucket[2] = last->a;
					bucket[3] = last->b;
					print_para(bucket, 1);
				}
			}
		}
		
		last = l;
	}
	
	free(lines);
}

int main(int argc, char** argv){
	srand(time(NULL));

	int N = 10;
	point* cities = malloc(N * sizeof(point));
	for (int i = 0; i < N; i++){
		cities[i].x = rand() % 20;
		cities[i].y = rand() % 20;
		
		//no duplicates
		for (int j = 0; j < i; j++){
			if (cities[i].x == cities[j].x && cities[i].y == cities[j].y){
				i--;
				break;
			}
		}
	}
	
	printf("naive:\n");
	find_naive(cities, N);
	
	printf("better:\n");
	find_parallelograms(cities, N);
	
	free(cities);
	
	return 0;
}
