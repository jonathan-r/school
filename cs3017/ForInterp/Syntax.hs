module Syntax where

-- Values (this is what the Big-Step interpreter for arithmetic and boolean expressions returns)

data Val = VArith Integer  -- n
         | VBool Bool      -- True/False

-- Arithmetic Expressions
data EArith = CNum Integer       -- n
            | Var String         -- L (arithmetic variables)
            | Add EArith EArith  -- e + e'
            | Sub EArith EArith  -- e - e'
            | Mul EArith EArith  -- e * e'
            | Div EArith EArith  -- e / e'

-- Boolean Expressions
data EBool = CBool Bool          -- true/false
          | Eq EArith EArith     -- e = e'
          | Lt EArith EArith     -- e = e'
          | And EBool EBool      -- b & b'
          | Or EBool EBool       -- b | b'
          | Not EBool            -- !b

-- Commands
data Com = Asgn String EArith          -- l := e
         | IfThenElse EBool Com Com    -- if b then c else c'
         | Seq Com Com                 -- c; c'
         | While EBool Com             -- while b do c
         | For String Integer Com      -- for l <= n do c
         | Skip                        -- skip



--
-- Pretty-print functions of the above datatypes
--

instance Show Val where
  show (VArith i)   = show i
  show (VBool b)    = if b then "true" else "false"

instance Show EArith where
  show (CNum i)     = show i
  show (Var  s)     = s
  show (Add e1 e2)  = "(" ++ show e1 ++ " + " ++ show e2 ++ ")"
  show (Sub e1 e2)  = "(" ++ show e1 ++ " - " ++ show e2 ++ ")"
  show (Mul e1 e2)  = "(" ++ show e1 ++ " * " ++ show e2 ++ ")"
  show (Div e1 e2)  = "(" ++ show e1 ++ " / " ++ show e2 ++ ")"

instance Show EBool where
  show (CBool b)    = if b then "true" else "false"
  show (Eq e1 e2)   = "(" ++ show e1 ++ " = " ++ show e2 ++ ")"
  show (Lt e1 e2)   = "(" ++ show e1 ++ " < " ++ show e2 ++ ")"
  show (And b1 b2)  = "(" ++ show b1 ++ " & " ++ show b2 ++ ")"
  show (Or b1 b2)   = "(" ++ show b1 ++ " | " ++ show b2 ++ ")"
  show (Not b)      = "!" ++ show b

instance Show Com where
  show c            = showJustified c 4


showJustified :: Com -> Int -> String
showJustified Skip                 i = (replicate i ' ') ++ "skip"
showJustified (Asgn s e)           i = (replicate i ' ') ++ s ++ " := " ++ show e
showJustified (IfThenElse b cThen cElse) i = (replicate i ' ') ++ "if " ++ show b ++ " then\n" ++
                                               (showJustified cThen (i+2)) ++ "\n" ++
                                               (replicate i ' ') ++ "else\n" ++
                                               (showJustified cElse (i+2))
showJustified (Seq c1 c2)          i = (showJustified c1 i) ++ ";\n" ++ (showJustified c2 i)
showJustified (While b c)          i = (replicate i ' ') ++ "while " ++ show b ++ " do\n" ++
                                         (showJustified c (i+2))
showJustified (For var n c)        i = (replicate i ' ') ++ "for " ++ var ++ " <= " ++ show n ++ " do\n" ++
                                         (showJustified c (i+2))

  



