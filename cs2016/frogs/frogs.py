
def run(state, stack):
	stack.append(state)

	if state[0] == '<' and state[1] == '<' and state[3] == '>' and state[4] == '>':
		print 'solved:'
		for sf in stack:
			print '\t' + sf[0] + sf[1] + sf[2] + sf[3] + sf[4]
		
		print ''

	for i in range(5):
		c = state[i]
		n = state[:]

		if c == '>':
			if i < 4 and state[i + 1] == ' ':
				n[i] = ' '
				n[i + 1] = '>'
				run(n, stack)

			if i < 3 and state[i + 2] == ' ':
				n[i] = ' '
				n[i + 2] = '>'
				run(n, stack)

		if c == '<':
			if i > 0 and state[i - 1] == ' ':
				n[i] = ' '
				n[i - 1] = '<'
				run(n, stack)
			
			if i > 1 and state[i - 2] == ' ':
				n[i] = ' '
				n[i - 2] = '<'
				run(n, stack)

	stack.pop()

run(['>', '>', ' ', '<', '<'], [])
