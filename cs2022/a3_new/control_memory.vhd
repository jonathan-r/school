library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity control_memory is port ( 
	car : in std_logic_vector(7 downto 0);
	mw, mm, rw, md : out std_logic;
	func : out std_logic_vector(4 downto 0);
	mb, tb, ta, td, pl, pi, il, mc, sw: out std_logic;
	ms : out std_logic_vector(2 downto 0);
	na : out std_logic_vector(7 downto 0) );
end control_memory;

architecture behavioral of control_memory is
	type mem_array is array (0 to 255) of std_logic_vector (28 downto 0);
	
begin
	process(car)
		variable control_mem : mem_array := (
		--"jump" is used here to refer to a microcode branch, while "branch" refers to an instruction branch
		-- na       ms     mc    il    pi    pl    td    ta    tb    mb    func      md    rw    mm    mw    sw
			x"00" & "000" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --00  after reset or branch: set ir from memory
			x"00" & "001" & '1' & '0' & '1' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --01  set car from opcode, jump to instruction
			
			x"00" & "001" & '0' & '0' & '0' & '1' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --02  B always
		
			x"02" & "010" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --03  BC, jump to B if taken
			x"01" & "001" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --04  if not taken
			
			x"02" & "011" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --05  BV, jump to B if taken
			x"01" & "001" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --06  if not taken
			
			x"02" & "100" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --07  BZ, jump to B if taken
			x"01" & "001" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --08  if not taken
			
			x"02" & "101" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --09  BN, jump to B if taken
			x"01" & "001" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --0a  if not taken
			
			x"02" & "110" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --0b  BNC, jump to B if taken
			x"01" & "001" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --0c  if not taken
			
			x"02" & "111" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --0d  BNZ, jump to B if taken
			x"01" & "001" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & "00000" & '0' & '0' & '1' & '0' & '0', --0e  if not taken
			
			x"0000000" & '0', x"0000000" & '0', --words wasted, a mistake too late to fix
			
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "10000" & '0' & '1' & '1' & '0' & '1', --11 MOVREG
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '1' & "10000" & '0' & '1' & '1' & '0' & '1', --12 MOVIMM
			
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "00001" & '0' & '1' & '1' & '0' & '1', --13 INC
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "00110" & '0' & '1' & '1' & '0' & '1', --14 DEC
			
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "00010" & '0' & '1' & '1' & '0' & '1', --15 SUMREG
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "00101" & '0' & '1' & '1' & '0' & '1', --16 DIFFREG
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '1' & "00010" & '0' & '1' & '1' & '0' & '1', --17 SUMIMM
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '1' & "00110" & '0' & '1' & '1' & '0' & '1', --18 DIFFIMM
			
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "01000" & '0' & '1' & '1' & '0' & '1', --19 AND
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "01010" & '0' & '1' & '1' & '0' & '1', --1a  OR
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "01100" & '0' & '1' & '1' & '0' & '1', --1b XOR
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "01110" & '0' & '1' & '1' & '0' & '1', --1c NOT
			
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "10100" & '0' & '1' & '1' & '0' & '1', --1d SRL
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "11000" & '0' & '1' & '1' & '0' & '1', --1e SLL
			
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "11000" & '1' & '1' & '0' & '0' & '1', --1f LD
			x"01" & "001" & '0' & '1' & '0' & '0' & '0' & '0' & '0' & '0' & "11000" & '0' & '1' & '0' & '1' & '1', --20 ST
			
			others => x"0000000" & '0'
		);
		variable addr : integer;
		variable control_out : std_logic_vector(28 downto 0);
			
	begin
		addr := conv_integer(car);
		control_out := control_mem(addr);
		
		sw <= control_out(0);
		mw <= control_out(1);
		mm <= control_out(2);
		rw <= control_out(3);
		md <= control_out(4);
		func <= control_out(9 downto 5);
		mb <= control_out(10);
		tb <= control_out(11);
		ta <= control_out(12);
		td <= control_out(13);
		pl <= control_out(14);
		pi <= control_out(15);
		il <= control_out(16);
		mc <= control_out(17);
		ms <= control_out(20 downto 18);
		na <= control_out(28 downto 21);
		
	end process;
end behavioral;
