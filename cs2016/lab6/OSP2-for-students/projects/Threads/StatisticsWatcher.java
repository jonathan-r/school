package osp.Threads;

import java.util.LinkedList;
import java.util.Iterator;

import java.awt.*;
import java.awt.image.*;

import javax.swing.*;

import javax.imageio.ImageIO; //for saving images

//This thread listens for threadEnded or update calls, until they stop coming, at which point it decides the simulation ended and runs some code.
//For it to stay in the idle state, the delay until the next call must be at most five times the averate delay of previous calls.

//Its raison d'etre is that there's no way to reach the osp.Utilities.Simulation class, 
//access its simConfig.getIntegerParam and find the simulation length in order to determine when to run the statistics code
public class StatisticsWatcher extends JFrame implements Runnable {
	//idling
	private long lastUpdateAt;
	private int updateCount = 0;

	//ui elements
	private JPanel panels[];
	private Label meanLabels[];
	private Label varianceLabels[];
	private JLabel historgramContainers[];

	private BufferedImage histograms[];

	//statistics
	private static final int maxBuckets = 80;

	//container for a thread lifecycle
	private static class ThreadStats{
		public long startTime;
		public double executionRatio;
		public long turnaroundTime;

		public ThreadStats(long startTime, long runningTime, long turnaroundTime){
			this.startTime = startTime;
			this.executionRatio = (double)runningTime / turnaroundTime;
			this.turnaroundTime = turnaroundTime;
		}

		public double getVal(int i){
			if (i == 0) return startTime;
			else if (i == 1) return executionRatio;
			else return turnaroundTime;
		}
	}

	//each thread that finished its lifecycle gets an entry
	private LinkedList<ThreadStats> stats = new LinkedList<ThreadStats>();

	//histogram drawing
	private final int leftMargin = 50; //ruler on the left
	private final int bottomMargin = 60; //ruler on the bottom
	private final int barWidth = 10;
	private final int maxBarHeight = 400;

	private final Color[] cols = new Color[]{ new Color(0xAB2A56), new Color(0x4C2982) };
	private final Color ruleCol = new Color(0x22222222);

	public StatisticsWatcher(){
		super("Thread Scheduling Statistics");
		lastUpdateAt = System.currentTimeMillis();

		setResizable(false);
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

		panels = new JPanel[3];
		meanLabels = new Label[3];
		varianceLabels = new Label[3];
		Label fieldName[] = new Label[]{ new Label("start time"), new Label("execution time ratio"), new Label("turnaround time") };
		historgramContainers = new JLabel[3];
		histograms = new BufferedImage[3];

		for (int i = 0; i < 3; i++){
			panels[i] = new JPanel();
			panels[i].setLayout(new BoxLayout(panels[i], BoxLayout.Y_AXIS));

			meanLabels[i] = new Label("");
			varianceLabels[i] = new Label("");
			historgramContainers[i] = new JLabel();

			panels[i].add(fieldName[i]);
			panels[i].add(meanLabels[i]);
			panels[i].add(varianceLabels[i]);
			panels[i].add(historgramContainers[i]);

			add(panels[i]);
		}
	}

	private void gotUpdate(){
		updateCount++;
		lastUpdateAt = System.currentTimeMillis();
	}

	public synchronized void threadEnded(long startTime, long runningTime, long turnaroundTime, int executions){
		stats.add(new ThreadStats(startTime, runningTime, turnaroundTime));
		gotUpdate();
	}

	public synchronized void update(){
		gotUpdate();
	}

	@Override
	public void run(){
		while (true) {
			try {
				Thread.sleep(100);
				if (System.currentTimeMillis() - lastUpdateAt > 500) break;
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		setVisible(true);
		toFront();

		if (stats.size() > 0){
			double means[] = new double[3];
			double variances[] = new double[3];

			for (int i = 0; i < 3; i++){
				//compute mean and variance
				double sum = 0.0;

				ThreadStats head = stats.getFirst();
				double min = head.getVal(i);
				double max = head.getVal(i);

				Iterator<ThreadStats> it = stats.descendingIterator();
				while(it.hasNext()){
					ThreadStats ts = it.next();
					double thing = ts.getVal(i);

					sum += thing;
					if (thing < min) min = thing;
					if (thing > max) max = thing;
				}

				means[i] = sum / stats.size();
				meanLabels[i].setText(String.format("\u03BC: %.7f", means[i]));

				//reuse the sum variable and compute the variance
				sum = 0.0;

				it = stats.descendingIterator();
				while(it.hasNext()){
					ThreadStats ts = it.next();
					double s = ts.getVal(i) - means[i];
					sum += s * s;
				}

				variances[i] = sum / stats.size();
				varianceLabels[i].setText(String.format("\u03C3: %.7f", variances[i]));

				double range = max - min;
				double bucketSize;
				int buckets;

				//discrete
				if (i == 0 || i == 2){
					bucketSize = (int)(range / Math.min(stats.size(), maxBuckets));
					buckets = (int)Math.ceil(range / bucketSize);
				}

				//analogue
				else{
					buckets = Math.min(stats.size(), maxBuckets);
					bucketSize = range / buckets;
				}

				int[] bars = new int[buckets + 1];
				int most = 0;

				it = stats.descendingIterator();
				while(it.hasNext()){
					ThreadStats ts = it.next();
					double thing = ts.getVal(i);

					int fallsIn = (int)((thing - min) / bucketSize);
					bars[fallsIn]++;
					if (bars[fallsIn] > most) most = bars[fallsIn];
				}


				System.out.println("range: " + range + ", bucketSize: " + bucketSize + ", buckets: " + buckets + ", most: " + most);
				histograms[i] = new BufferedImage(leftMargin + barWidth * bars.length, maxBarHeight + bottomMargin, BufferedImage.TYPE_3BYTE_BGR);
				Graphics g = histograms[i].getGraphics();

				int width = barWidth * bars.length;

				//rule
				g.setColor(ruleCol);
				for (int b = 0; b < most; b++){
					int v = (b * maxBarHeight) / most;
					g.drawLine(0, maxBarHeight - v, leftMargin, maxBarHeight - v);
				}

				for (int b = 0; b < bars.length; b++){
					g.setColor(cols[b % 2]);

					System.out.println("data: " + bars[b]);
					int h = (bars[b] * maxBarHeight) / most;
					int l = leftMargin + b * barWidth;
					int r = leftMargin + (b + 1) * barWidth - 2;
					g.fillRect(l, maxBarHeight - h, r, maxBarHeight);

					//rule
					g.setColor(ruleCol);
					int rh = (b % 2 == 0)? bottomMargin / 2 : bottomMargin / 4;
					g.drawLine(l, maxBarHeight, l, maxBarHeight + rh);
				}

				historgramContainers[i].setIcon(new ImageIcon(histograms[i]));
			}

			pack(); //resize windows to fit contents
		}
	}
}