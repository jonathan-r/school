#version 120

uniform mat3 matrix;

attribute vec2 vPosition; //object
attribute vec2 vTexCoord;
attribute vec3 vColor; //d tex / d surface

varying vec2 tex;
varying vec3 col;

void main() {
	gl_Position = vec4(matrix * vec3(vPosition, 1.0), 1.0f); //NDC

	tex = vTexCoord;
	col = vColor;
}
