#version 120

uniform sampler2D texture;

//from vertex shader
in vec2 tex;
in vec3 col;

out vec4 FragColor;

void main() {
	vec3 sample = texture2D(texture, tex).xyz;
	vec3 final = sample * col;
	FragColor = vec4(final, 1.0);
}
