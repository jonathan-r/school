	.file	"sort.c"
	.intel_syntax noprefix
	.text
	.p2align 4,,15
	.globl	sort
	.type	sort, @function
sort:
.LFB0:
	.cfi_startproc
	push	ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	push	edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	push	esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	push	ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	sub	esp, 4
	.cfi_def_cfa_offset 24
	mov	eax, DWORD PTR [esp+28]
	mov	ebp, DWORD PTR [esp+24]
	sub	eax, 1
	test	eax, eax
	mov	DWORD PTR [esp], eax
	jle	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	xor	edx, edx
.L3:
	lea	edi, [edx+1]
	lea	eax, [0+edi*4]
	lea	esi, [ebp+0+eax]
	lea	eax, [ebp-4+eax]
	mov	ecx, DWORD PTR [esi]
	mov	ebx, DWORD PTR [eax]
	cmp	ecx, ebx
	jle	.L4
	test	edx, edx
	js	.L4
	mov	edi, eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	cmp	edx, -1
	mov	edi, eax
	je	.L12
.L7:
	mov	DWORD PTR [eax], ecx
	sub	edx, 1
	mov	DWORD PTR [esi], ebx
	mov	esi, eax
	sub	eax, 4
	mov	ecx, DWORD PTR [eax+4]
	mov	ebx, DWORD PTR [edi-4]
	cmp	ecx, ebx
	jg	.L9
	lea	edi, [edx+1]
.L4:
	cmp	edi, DWORD PTR [esp]
	mov	edx, edi
	jl	.L3
.L10:
	add	esp, 4
	.cfi_def_cfa_offset 20
	pop	ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	pop	esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	pop	edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	pop	ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
.LFE0:
	.size	sort, .-sort
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
