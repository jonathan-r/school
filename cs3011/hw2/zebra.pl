intersect([], _, []).

intersect([X | T], L2, [X | TI]) :-
	member(X, L2),
	intersect(T, L2, TI).

intersect([X | T], L2, TI) :-
	not(member(X, L2)),  %remove overlap with previous rule
	intersect(T, L2, TI).

s                 --> house(A), house(B), house(C), 
                      {intersect(A, B, [])}, {intersect(B, C, [])}, {intersect(A, C, [])}. 
                      % non-intersection is not transitive

house([X,Y,Z|[]]) --> color(X), nationality(Y), pet(Z).

color(X)          --> [X], {member(X, [red, green, blue])}.
nationality(X)    --> [X], {member(X, [english, japanese, spanish])}.
pet(X)            --> [X], {member(X, [snail, jaguar, zebra])}.
