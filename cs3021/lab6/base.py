class BaseCache:
	"""Base cache class. Should not be instantiated on its own."""
	def __init__(self, L, N, K, addr_bits):
		assert L > 0 and N > 0 and K > 0 and addr_bits > 0 #sanity check
		self.L = L
		self.N = N
		self.K = K
		self.addr_bits = addr_bits
		self.reset()

	def reset(self):
		#actual cache functionality
		self.tags = [[-1 for wi in range(self.K)] for si in range(self.N)]

		#statistics
		self.tags_free = self.N * self.K
		self.compulsory_misses = 0
		self.capacity_misses = 0
		self.conflict_misses = 0
		self.hits = 0

		#convenience
		self.offset_bits = int.bit_length(self.L) - 1
		self.set_bits = int.bit_length(self.N) - 1
		self.tag_bits = self.addr_bits - self.offset_bits - self.set_bits
		assert self.tag_bits > 0

		#these are only used to discern the type of a miss
		self.seen_set = set() #all addresses seen so far
		self.on_reset() #deriver's reset method

	def size(self):
		#in bytes
		return self.L * self.N * self.K

	def access(self, addr): #addr = tag | set | offset
		tag_id = addr >> (self.offset_bits + self.set_bits)
		set_id = (addr >> self.offset_bits) & ((1 << self.set_bits) - 1)
		out = "access addr %04x: tag %03x, set %x => " % (addr, tag_id, set_id)

		for way_id in range(self.K):
			if self.tags[set_id][way_id] == tag_id:
				#hit
				self.on_hit(tag_id, set_id, way_id)
				self.hits += 1
				out += "hit way " + str(way_id)
				return True, out

		#miss
		way_id = self.on_miss(tag_id, set_id)
		old_tag = self.tags[set_id][way_id]
		self.tags[set_id][way_id] = tag_id

		if old_tag == -1:
			self.tags_free -= 1

		#miss reason
		#TODO: thought long overdue, but maybe the conflict/capacity dicothomy 
		#should be applied for every set individually, the whole cache

		no_offset_addr = addr >> self.offset_bits
		if not no_offset_addr in self.seen_set:
			out += "compulsory miss"
			self.seen_set.add(no_offset_addr)
			self.compulsory_misses += 1

		elif self.tags_free > 0:
			out += "conflict miss"
			self.conflict_misses += 1

		else:
			out += "capacity miss"
			self.capacity_misses += 1

		out += " (into way %d)" % way_id
		return False, out

	def show_header(self):
		out = 'set'.rjust(len(str((1 << self.set_bits) - 1))) + ' | '
		out += ' | '.join(map(lambda x : 'way ' + 
			str(x).rjust(len(str(self.K - 1))), range(self.K)))
		out += ' | ' + self.show_extra_info_column_name() + '\n'
		return out

	def show_body(self):
		max_tag_len = max([len(str((1 << self.tag_bits) - 1)), len('way ') + 
			len(str(self.K - 1))])

		out = ''
		for set_id in range(self.N):
			out += str(set_id).rjust(max([len('set'), 
				len(str((1 << self.set_bits) - 1))])) + ' | '
			for way_id in range(self.K):
				tag_id = '' if self.tags[set_id][way_id] == -1 else \
					hex(self.tags[set_id][way_id])[2:]
				out += tag_id.zfill(max_tag_len) + ' | '

			out += self.show_extra_info(set_id) + '\n'

		return out

	def __repr__(self):
		return self.show_header() + self.show_body()

	#derivers must override these methods
	def on_reset(self):
		raise NotImplementedError

	def on_hit(self, tag_id, set_id, way_id):
		raise NotImplementedError
	
	#should return which way to place the data
	def on_miss(self, tag_id, set_id):
		raise NotImplementedError

	#should return a string of replacement-policy specific data for a given cache line
	def show_extra_info(self, set_id):
		raise NotImplementedError

	#should return a string of the last column name
	def show_extra_info_column_name(self):
		raise NotImplementedError