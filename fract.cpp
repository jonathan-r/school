#include <string>
#include <cstdint>

int64_t hcf(int64_t a, int64_t b) {
	if (b == 0) {
		return a;
	}
	else if (a > b) {
		return hcf(b, a%b);
	}
	else {
		return hcf(a, b%a);
	}
}

pair<int, int> make_fract(double x, int decimals) {
	int64_t m = 1;
	for (int di = 0; di < decimals; di++) m *= 10;
	
	double q = x * m;
	q += 0.5 - 1 * (x < 0); //move 0.5 away from 0 for better rounding

	int64_t i = (int64_t)q;
	if (i == 0) {
		return pair<int, int>(0, 1);
	}
	else {
		int64_t h = hcf(i, m);
		return pair<int, int>(i/h, m/h);
	}
}

string make_fract_str(double x, int decimals) {
	pair<int, int> fr = make_fract(x, decimals);
	string ret;
	if (fr.first == 0) ret = "0";
	else if (fr.second == 1) ret = to_string(fr.first);
	else ret = to_string(fr.first) + "/" + to_string(fr.second);
	return ret;
}
