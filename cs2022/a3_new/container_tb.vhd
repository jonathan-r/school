library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity container_tb is
end container_tb;
 
architecture behavior of container_tb is 
	component container is port ( 
		clock, reset : in std_logic );
	end component;

   signal clock : std_logic := '0';
	signal reset : std_logic := '0';
	constant clk_period : time := 70ns; --50ns on the arithmetic unit + muxes and stuff around it
	
begin
	uut: container port map (clock, reset);

	clock_process : process begin
		clock <= '0';
		wait for clk_period/2;
		clock <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		report "reset";
		reset <= '1';
		wait for clk_period;
		reset <= '0';
		
		wait;
	end process;
end;
