module Main( main ) where

import Parser
import Syntax
import Interpreter

import System.Environment( getArgs )
import System.Console.GetOpt

--import Text.Parsec( ParseError )

version = 1

-- Main reads command-line arguments
main :: IO()
main = do 
  args <- getArgs
  case getOpt RequireOrder options args of
    (flags, [],      [])  -> run flags
    (_,     nonOpts, [])  -> error $ "unrecognized arguments: " ++ unwords nonOpts ++ "\n" ++ usageInfo header options
    (_,     _,       err) -> error $ concat err ++ usageInfo header options

-- the cases of the command-line arguments
data Flag = Help | Version | Input String | BigStep | SmallStep
  deriving (Eq, Show)
  
-- the syntax of the command-line arguments
options :: [OptDescr Flag]
options = [
  Option ['?'] [] (NoArg Help)                    "show help",
  Option ['v'] ["version"] (NoArg Version)        "show version number and exit",
  Option ['f'] ["file"]   (ReqArg Input "FILE")   "read source from file (if option is missing use standard input)",
  Option ['b'] ["bigstep"]     (NoArg BigStep)    "big-step interpreter (default)",
  Option ['s'] ["smallstep"]    (NoArg SmallStep) "small-step interpreter" ]

header = "Usage: main [OPTION...]"


-- this is the function that reads the code and runs the appropriate interpreter
run :: [Flag] -> IO()
run flags =
  if elem Help flags
  then
      putStr $ usageInfo header options
  else if elem Version flags 
  then
      putStr $ "Version: " ++ show version    -- just print version and exit
  else                                        -- run the interpreter
    let filename = foldr findfile "" flags in   -- find file name with some functional magic
    do{ putStrLn (if elem SmallStep flags
                then "small-step interpreter for arithmetic expressions"
                else "big-step interpreter for arithmetic expressions")
      ; code <- (if (length filename) > 0    -- read contents of file (or stdio) 
                         then readFile filename
                         else getContents)
      ; case (parseExp filename code) of                        -- parse
          Left err  -> putStr $ "parse error at " ++ (show err) -- parse error
          Right term ->                                          -- parse OK
            if elem SmallStep flags
              then                                                                -- small-step
                do{ putStr $ "    " ++ (show term)
                  ; reduce smallStepExp term}
              else putStr $ (show term) ++ " ==> " ++ (show (bigStepExp term)) ++ "\n"  -- big-step
      }

-- reduce a term using a small-step interpreter function
reduce smallStep term = case smallStep term of
                          Nothing -> putStrLn "\ncannot reduce any more"
                          Just e  -> do{ putStr $ "\n--> " ++ (show e)
                                       ; reduce smallStep e}

-- find a filename in a flag or else return 2nd argument
findfile :: Flag -> String -> String
findfile flag fname = case flag of
                        Input file -> file
                        _ -> fname

