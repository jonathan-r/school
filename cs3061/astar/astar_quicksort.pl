arc(N, M, Seed, Cost) :- M is N * Seed, Cost = 1.
arc(N, M, Seed, Cost) :- M is N * Seed + 1, Cost = 2.

goal(N, Target) :- 0 is N mod Target.

h(N, Hvalue, Target) :-
	goal(N, Target), !, Hvalue is 0;
	Hvalue is 1/N.

arc_add(N, M, Seed, NCost, MCost) :-
	arc(N, M, Seed, XCost),
	MCost is NCost + XCost.

search([[N, _] | _], _, Target, N) :- goal(N, Target).
search([[N, NCost] | FRest], Seed, Target, Found) :-
	setof([M, MCost], arc_add(N, M, Seed, NCost, MCost), FNodes),
	add_to_frontier(FNodes, FRest, FNew, Target),
	search(FNew, Seed, Target, Found).

%quick_sort
%partition(+List, +MidCost, +CurrentLeft, +CurrentRight, +Target, ?LeftPartition, ?RightPartition)
partition([], _, L, R, _, L, R).
partition([[N, NCost] | Arr], MidCost, LIn, RIn, Target, LOut, ROut) :-
	( h(N, HVal, Target), (NCost + HVal) =< MidCost, !, % head < mid ?
	append([[N, NCost]], LIn, LPart), RPart = RIn;      % put head in left list
	LPart = LIn, append([[N, NCost]], RIn, RPart) ),    % put head in right list
	partition(Arr, MidCost, LPart, RPart, Target, LOut, ROut).

f_sort([], [], _).
f_sort([[MidN, MidC] | Arr], Out, Target) :-
	h(MidN, MidH, Target), MidCost is MidC + MidH,
	partition(Arr, MidCost, [], [], Target, L, R),
	f_sort(L, LSorted, Target), f_sort(R, RSorted, Target),
	append([[MidN, MidC]], RSorted, RSortedFinal),
	append(LSorted, RSortedFinal, Out).

add_to_frontier(FNodes, FRest, FNewSorted, Target) :-
	append(FNodes, FRest, FNewUnsorted),
	f_sort(FNewUnsorted, FNewSorted, Target).

%a_star(+Start, +Seed, +Target, ?Found)
a-star(Start, Seed, Target, Found):-
	search([[Start, 0]], Seed, Target, Found).
