#ifndef INT_SET
#define INT_SET

struct int_set{
	int* array;
	int max;
	int current;
};

// create a new, empty set
// initially has 16 spaces
struct int_set* int_set_new();

// safely destroy any kind of set
void int_set_destroy(struct int_set* this);

// check to see if an item is in the set;
// returns 1 if in the set, 0 if not;
// log2(n) steps
int int_set_lookup(struct int_set* this, int item);

// add an item, with number 'item' to the set;
// has no effect if the item is already in the set;
// returns 1 if the item did not already exist, 0 otherwise
// log2(n) + n steps
int int_set_add(struct int_set* this, int item);

// remove an item with number 'item' from the set;
// has no effect if the items is not in the set;
// returns 1 if the item did exist, 0 otherwise
// log2(n) + n steps
int int_set_remove(struct int_set* this, int item);

// place the union of src1 and src2 into dest;
// dest must be an empty set (but can be allocated);
// n steps
void int_set_union(struct int_set* dest, struct int_set* src, struct int_set* src2);

// place the intersection of src1 and src2 into dest;
// dest must be an empty set (but can be allocated);
// n steps
void int_set_intersect(struct int_set* dest, struct int_set* src, struct int_set* src2);

#endif
