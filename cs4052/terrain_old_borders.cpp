
#include "maths/util.h"

#include "terrain.h"

void Terrain::allocate(int triangleCount) {
	if (triangleCount > vertStorage * 2) {
		vertStorage = triangleCount / 2; //3 verts per triangle, but each vert shared between 6 triangles
		indStorage = triangleCount * 3; //3 vert references per triangle
		points = (vertex*) realloc(points, vertStorage * sizeof(vertex));
		indices = (uint32_t*) realloc(indices, indStorage * sizeof(uint32_t));
	}
}

float Terrain::sampleHeight(const vec2& p) {
	const int octaves = 3;
	const float persistence = 1.9f;

	/*float s = 0.0f;
	float div = persistence;

	for (int oi = 0; oi < octaves; oi++) {
		s += noise.noise2(p * (1 << oi) * 0.1f) / div;
		div *= div;
	}*/

	float s = noise.noise2Distorted(p * 0.05f, 5.0f);
	return s * 10.0f;
}

bool Terrain::testPoint(const vec2& p) {
	vec3 p3(p.x, sampleHeight(p), p.y);
	float dist;
	if (clip.test(p3, dist)) {
		return true;
	}
	return false;
}

void Terrain::splitTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se) {
	//compute cross
	bool nwv = testPoint(nw);
	bool nev = testPoint(ne);
	bool swv = testPoint(sw);
	bool sev = testPoint(se);
	vec2 c = (nw + se) / 2;
	bool cv = testPoint(c);

	float diag = (ne - sw).length();
	float dist = (vec3(c.x, sampleHeight(c), c.y) - eye).length();
	float factor = diag / dist;

	if (at.isLeaf() and at.level < TERRAIN_MAX_DEPTH) {
		if ((at.level < 2) or (nwv or nev or swv or sev or cv)) {
			//visible, consider for split

			if (factor > 0.9f) {
				at.split(); leafCount += 3; //lose current, gain 4 new
			}
		}
	}

	if (factor < 0.5f) {
		leafCount -= at.flatten() - 1; //after flattening, there is one leaf gained
	}

	if (!at.isLeaf()) {
		//traverse down
		vec2 n = (nw + ne) / 2;
		vec2 e = (ne + se) / 2;
		vec2 s = (sw + se) / 2;
		vec2 w = (nw + sw) / 2;

		splitTraverse(*(at.ch[0]), nw, n, w, c);
		splitTraverse(*(at.ch[1]), n, ne, c, e);
		splitTraverse(*(at.ch[2]), w, c, sw, s);
		splitTraverse(*(at.ch[3]), c, e, s, se);
	}
}

void Terrain::stitchTraverse(node& at, node* nn, node* ee, node* ss, node* ww, int maxDepth, int pos) {
	/*if (nn) assert(nn->level == at.level or nn->level == at.level-1);
	if (ee) assert(ee->level == at.level or ee->level == at.level-1);
	if (ss) assert(ss->level == at.level or ss->level == at.level-1);
	if (ww) assert(ww->level == at.level or ww->level == at.level-1);*/

	//must also split any neighbours that are more than 1 level away
	if (nn and nn->isLeaf() and (nn->level < at.level-1)) {
		//assert(nn->level == at.level-1);
		assert(pos == 0 or pos == 1); //we're a northern cell
		nn->split(); leafCount += 3;
		nn = (pos == 2)? nn->ch[2] : nn->ch[3];
	}

	if (ee and ee->isLeaf() and (ee->level < at.level-1)) {
		//assert(ee->level == at.level-1);
		assert(pos == 1 or pos == 3); //we're an eastern cell
		ee->split(); leafCount += 3;
		ee = (pos == 1)? ee->ch[0] : ee->ch[2];
	}

	if (ss and ss->isLeaf() and (ss->level < at.level-1)) {
		//assert(ss->level == at.level-1);
		assert(pos == 2 or pos == 3); //we're a southern cell
		ss->split(); leafCount += 3;
		ss = (pos == 2)? ss->ch[0] : ss->ch[1];
	}

	if (ww and ww->isLeaf() and (ww->level < at.level-1)) {
		//assert(ww->level == at.level-1);
		assert(pos == 0 or pos == 2); //we're a western cell
		ww->split(); leafCount += 3;
		ww = (pos == 0)? ww->ch[1] : ww->ch[3];
	}

	if (!at.isLeaf() and at.level < maxDepth) {
		//traverse down
		node *nnn=nn, *eee=ee, *sss=ss, *www=ww;

		if (nn) if (!nn->isLeaf()) nnn = nn->ch[2];
		if (ww) if (!ww->isLeaf()) www = ww->ch[1];
		stitchTraverse(*(at.ch[0]), nnn, at.ch[1], at.ch[2], www, maxDepth, 0);

		if (nn) if (!nn->isLeaf()) nnn = nn->ch[3];
		if (ee) if (!ee->isLeaf()) eee = ee->ch[0];
		stitchTraverse(*(at.ch[1]), nnn, eee, at.ch[3], at.ch[0], maxDepth, 1);

		if (ww) if (!ww->isLeaf()) www = ww->ch[3];
		if (ss) if (!ss->isLeaf()) sss = ss->ch[0];
		stitchTraverse(*(at.ch[2]), at.ch[0], at.ch[3], sss, www, maxDepth, 2);

		if (ee) if (!ee->isLeaf()) eee = ee->ch[2];
		if (ss) if (!ss->isLeaf()) sss = ss->ch[1];
		stitchTraverse(*(at.ch[3]), at.ch[1], eee, sss, at.ch[2], maxDepth, 3);
	}
}

int Terrain::addVert(const vec2& p) {
	if (vertUsed == vertStorage) {
		vertStorage *= 2;
		points = (vertex*)realloc(points, vertStorage * sizeof(vertex));
	}
	int id = vertUsed++;
	vec3& p3 = points[id].v;
	p3 = vec3(p.x, sampleHeight(p), p.y);

	//FIXME: really expensive normal
	vec3 p3a(p.x + 0.1f, sampleHeight(p + vec2(0.1f, 0.0f)), p.y + 0.0f);
	vec3 p3b(p.x + 0.0f, sampleHeight(p + vec2(0.0f, 0.1f)), p.y + 0.1f);
	points[id].n = ((p3 - p3a).normal() ^ (p3 - p3b).normal()).normal();

	points[id].t.set(p.x / samplingSize.x + 0.5f, p.y / samplingSize.y + 0.5f);
	return id;
}

void Terrain::configTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se,
							 node* nn, node* ee, node* ss, node* ww/*, 
							 int nwi, int nei, int swi, int sei*/) {
	if (nn) assert(nn->level == at.level or nn->level == at.level-1);
	if (ee) assert(ee->level == at.level or ee->level == at.level-1);
	if (ss) assert(ss->level == at.level or ss->level == at.level-1);
	if (ww) assert(ww->level == at.level or ww->level == at.level-1);

	//TODO: figure out vertex indexing

	vec2 n = (nw + ne) / 2;// int ni = addVert(n);
	vec2 e = (ne + se) / 2;// int ei = addVert(e);
	vec2 s = (sw + se) / 2;// int si = addVert(s);
	vec2 w = (nw + sw) / 2;// int wi = addVert(w);

	vec2 c = (nw + se) / 2;
	//int ci = addVert(c);

	if (at.isLeaf()) {
		int dn = 0, de = 0, ds = 0, dw = 0; //lack of neighbour treated like same level
		if (nn) dn = at.level - nn->level;
		if (ee) de = at.level - ee->level;
		if (ss) ds = at.level - ss->level;
		if (ww) dw = at.level - ww->level;

		//TODO: many triangles per patch
		//TODO: triangle indexing

		//borders
		float top = lerp(nw.y, sw.y, 1.0f / TERRAIN_LEAF_ACROSS);
		float top2 = lerp(nw.y, sw.y, 2.0f / TERRAIN_LEAF_ACROSS);
		float bot = lerp(nw.y, sw.y, (TERRAIN_LEAF_ACROSS-1.0f) / TERRAIN_LEAF_ACROSS);
		float bot2 = lerp(nw.y, sw.y, (TERRAIN_LEAF_ACROSS-2.0f) / TERRAIN_LEAF_ACROSS);

		float lef = lerp(nw.x, ne.x, 1.0f / TERRAIN_LEAF_ACROSS);
		float lef2 = lerp(nw.x, ne.x, 2.0f / TERRAIN_LEAF_ACROSS);
		float rig = lerp(nw.x, ne.x, (TERRAIN_LEAF_ACROSS-1.0f) / TERRAIN_LEAF_ACROSS);
		float rig2 = lerp(nw.x, ne.x, (TERRAIN_LEAF_ACROSS-2.0f) / TERRAIN_LEAF_ACROSS);

		for (int i = 1; i < TERRAIN_LEAF_ACROSS; i += 2) {
			float x0 = lerp(nw.x, ne.x, (float)(i-1) / TERRAIN_LEAF_ACROSS);
			float x1 = lerp(nw.x, ne.x, (float)(i  ) / TERRAIN_LEAF_ACROSS);
			float x2 = lerp(nw.x, ne.x, (float)(i+1) / TERRAIN_LEAF_ACROSS);

			{
				//northern side
				vec2 nw0(x0, nw.y);
				vec2 n0(x1, nw.y);
				vec2 ne0(x2, nw.y);
				vec2 e0(x2, top);
				vec2 se0(x2, top2);
				vec2 cn(x1, top);

				addVert(cn);
				addVert(nw0);
				if (!dn) {
					addVert(n0);
					addVert(cn);
					addVert(n0);
				}
				addVert(ne0);
				
				addVert(cn);
				addVert(ne0);
				addVert(e0);
				
				addVert(cn);
				addVert(e0);
				addVert(se0);
			}

			{
				//soutern side
				vec2 sw0(x0, sw.y);
				vec2 s0(x1, sw.y);
				vec2 se0(x2, sw.y);
				vec2 
				vec2 cs(x1, bot);

				addVert(cs);
				addVert(se0);
				if(!ds) {
					addVert(s0);
					addVert(cs);
					addVert(s0);
				}
				addVert(sw0);
			}

			float y0 = lerp(nw.y, sw.y, (float)(i-1) / TERRAIN_LEAF_ACROSS);
			float y1 = lerp(nw.y, sw.y, (float)(i  ) / TERRAIN_LEAF_ACROSS);
			float y2 = lerp(nw.y, sw.y, (float)(i+1) / TERRAIN_LEAF_ACROSS);

			{
				//eastern side
				vec2 ne0(se.x, y0);
				vec2 e0(se.x, y1);
				vec2 se0(se.x, y2);
				vec2 ce(rig, y1);

				addVert(ce);
				addVert(ne0);
				if(!de) {
					addVert(e0);
					addVert(ce);
					addVert(e0);
					
				}
				addVert(se0);
			}

			{
				//western side
				vec2 nw0(sw.x, y0);
				vec2 w0(sw.x, y1);
				vec2 sw0(sw.x, y2);
				vec2 cw(lef, y1);

				addVert(cw);
				addVert(sw0);
				if(!dw) {
					addVert(w0);
					addVert(cw);
					addVert(w0);
				}
				addVert(nw0);
			}
		}

		for (int j = 1; j < TERRAIN_LEAF_ACROSS; j += 2) {
			float y0 = lerp(nw.y, sw.y, (float)(j-1) / TERRAIN_LEAF_ACROSS);
			float y1 = lerp(nw.y, sw.y, (float)(j  ) / TERRAIN_LEAF_ACROSS);
			float y2 = lerp(nw.y, sw.y, (float)(j+1) / TERRAIN_LEAF_ACROSS);

			for (int i = 1; i < TERRAIN_LEAF_ACROSS; i += 2) {
				float x0 = lerp(nw.x, ne.x, (float)(i-1) / TERRAIN_LEAF_ACROSS);
				float x1 = lerp(nw.x, ne.x, (float)(i  ) / TERRAIN_LEAF_ACROSS);
				float x2 = lerp(nw.x, ne.x, (float)(i+1) / TERRAIN_LEAF_ACROSS);

				vec2 c0(x1, y1);

				vec2 nw0(x0, y0);
				vec2 n0(x1, y0);
				vec2 ne0(x2, y0);
				
				vec2 e0(x2, y1);

				vec2 se0(x2, y2);
				vec2 s0(x1, y2);
				vec2 sw0(x0, y2);

				vec2 w0(x0, y1);
				
				//northern side
				addVert(c0);
				addVert(nw0);
				if (!(dn and j == 1)) {
					addVert(n0);
					addVert(c0);
					addVert(n0);
				}
				
				addVert(ne0);

				//eastern side
				addVert(c0);
				addVert(ne0);
				if (!(de and i == TERRAIN_LEAF_ACROSS-1)) {
					addVert(e0);
					addVert(c0);
					addVert(e0);
				}
				addVert(se0);

				//soutern side
				addVert(c0);
				addVert(se0);
				if (!(ds and j == TERRAIN_LEAF_ACROSS-1)) {
					addVert(s0);
					addVert(c0);
					addVert(s0);
				}
				addVert(sw0);
				
				//western side
				addVert(c0);
				addVert(sw0);
				if (!(dw and i == 0)) {
					addVert(w0);
					addVert(c0);
					addVert(w0);
				}
				addVert(nw0);
			}
		}
	}
	else {
		//traverse down
		/*int ni = addVert(n);
		int ei = addVert(e);
		int si = addVert(s);
		int wi = addVert(w);*/

		node *nnn=nn, *eee=ee, *sss=ss, *www=ww;

		if (nn) if (!nn->isLeaf()) nnn = nn->ch[2];
		if (ww) if (!ww->isLeaf()) www = ww->ch[1];
		configTraverse(*(at.ch[0]), nw, n, w, c, nnn, at.ch[1], at.ch[2], www/*, nwi, ni, wi, ci*/);

		if (nn) if (!nn->isLeaf()) nnn = nn->ch[3];
		if (ee) if (!ee->isLeaf()) eee = ee->ch[0];
		configTraverse(*(at.ch[1]), n, ne, c, e, nnn, eee, at.ch[3], at.ch[0]/*, ni, nei, ci, ei*/);

		if (ww) if (!ww->isLeaf()) www = ww->ch[3];
		if (ss) if (!ss->isLeaf()) sss = ss->ch[0];
		configTraverse(*(at.ch[2]), w, c, sw, s, at.ch[0], at.ch[3], sss, www/*, wi, ci, swi, si*/);

		if (ee) if (!ee->isLeaf()) eee = ee->ch[2];
		if (ss) if (!ss->isLeaf()) sss = ss->ch[1];
		configTraverse(*(at.ch[3]), c, e, s, se, at.ch[1], eee, sss, at.ch[2]/*, ci, ei, si, sei*/);
	}
}

void Terrain::reprTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se) {
	if (at.isLeaf()) {
		//still a leaf
		vec2 x(nw.x / samplingSize.x, nw.y / samplingSize.y);
		vec2 y(ne.x / samplingSize.x, ne.y / samplingSize.y);
		vec2 z(sw.x / samplingSize.x, sw.y / samplingSize.y);
		vec2 w(se.x / samplingSize.x, se.y / samplingSize.y);

		float cr = ((at.level * 7)  % 31) / 31.0f;
		float cg = ((at.level * 11) % 19) / 19.0f;
		float cb = ((at.level * 5)  % 13) / 13.0f;

		vertex_ui a(x, x, vec3(cr, cg, cb));
		vertex_ui b(y, y, vec3(cr, cg, cb));
		vertex_ui c(z, z, vec3(cr, cg, cb));
		vertex_ui d(w, w, vec3(cr, cg, cb));
		//vertex_ui& e = debugVerts[uiVertsUsed++];
		//vertex_ui& f = debugVerts[uiVertsUsed++];


		/*a.v = x; a.t = x; a.c.set(cr, cg, cb);
		b.v = y; b.t = y; b.c.set(cr, cg, cb);
		c.v = w; c.t = w; c.c.set(cr, cg, cb);
		d.v = x; d.t = x; d.c.set(cr, cg, cb);
		e.v = w; e.t = w; e.c.set(cr, cg, cb);
		f.v = z; f.t = z; f.c.set(cr, cg, cb);*/

		debugVerts[uiVertsUsed++] = a;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = c;

		debugVerts[uiVertsUsed++] = a;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = c;
		debugVerts[uiVertsUsed++] = c;
		debugVerts[uiVertsUsed++] = a;
	}
	else {
		//traverse down
		vec2 n = (nw + ne) / 2;
		vec2 e = (ne + se) / 2;
		vec2 s = (sw + se) / 2;
		vec2 w = (nw + sw) / 2;
		vec2 c = (nw + se) / 2;

		reprTraverse(*(at.ch[0]), nw, n, w, c);
		reprTraverse(*(at.ch[1]), n, ne, c, e);
		reprTraverse(*(at.ch[2]), w, c, sw, s);
		reprTraverse(*(at.ch[3]), c, e, s, se);
	}
}

void Terrain::update(const vec3& eye3, const vec3& target3, const vec3& up3, bool drawLines) {
	eye = eye3;
	clip.update(eye3, target3, up3);

	/*//project frustum onto XZ plane
	eye.set(eye3.x, eye3.z);
	vec2 viewDir = (vec2(target3.x, target3.z) - eye).normal();
	float heading = atan2(viewDir.z, viewDir.y);
	float pitch = atan2(viewDir.y, viewDir.z);
	dfl = vec2(cos(heading-fovx/2), sin(heading-fovx/2));
	dfr = vec2(cos(heading+fovx/2), sin(heading+fovx/2));*/

	//TODO: traverse quadtree
	root.markAllInvisible();

	vec2 nw(-samplingSize.x/2,  samplingSize.y/2);
	vec2 ne( samplingSize.x/2,  samplingSize.y/2);
	vec2 sw(-samplingSize.x/2, -samplingSize.y/2);
	vec2 se( samplingSize.x/2, -samplingSize.y/2);

	splitTraverse(root, nw, ne, sw, se);
	printf("terrain: %d leaves after split\n", leafCount);
	
	for (int i = 1; i <= TERRAIN_MAX_DEPTH; i++) {
		stitchTraverse(root,  nullptr, nullptr, nullptr, nullptr, i, 0);
	}

	printf("terrain: %d leaves after stitch\n", leafCount);

	//DEBUG: just print out the quads
	debugVerts = (vertex_ui*) realloc(debugVerts, leafCount * 12 * sizeof(vertex_ui));
	uiVertsUsed = 0;
	reprTraverse(root, nw, ne, sw, se);
	assert(uiVertsUsed == leafCount * 12);
	overlay.assign(uiVertsUsed, debugVerts, 0, nullptr, GL_DYNAMIC_DRAW);

	//generate actual mesh
	vertUsed = 0;
	indUsed = 0;
	configTraverse(root, nw, ne, sw, se, nullptr, nullptr, nullptr, nullptr);
	printf("generated %d verts, %d elements\n", vertUsed, indUsed);
	
	//dummy generate mesh
	/*const int s = 100;
	vertUsed = 0;
	indUsed = 0;
	const float sizeOut = 10.0f;

	//verts
	for (int j = 0; j < s; j++) {
		for (int i = 0; i < s; i++) {
			vec2 po((float)i / s, (float)j / s);
			vec2 p(po.x / samplingSize.x, po.y / samplingSize.y);

			vertex& v = points[vertUsed++];
			v.v.set(po.x * sizeOut - sizeOut/2, sampleHeight(p) * sizeOut, po.y * sizeOut - sizeOut/2);
			v.n.set(0, 0, 0);
			v.ta.set(0, 0, 0); //not used yet
			v.t.set(po.x, po.y);
		}
	}

	//elements
	for (int j = 0; j < s-1; j++) {
		for (int i = 0; i < s-1; i++) {
			// a b
			// c d

			int ai = (j+0) * s + (i+0);  vertex& a = points[ai];
			int bi = (j+0) * s + (i+1);  vertex& b = points[bi];
			int ci = (j+1) * s + (i+0);  vertex& c = points[ci];
			int di = (j+1) * s + (i+1);  vertex& d = points[di];

			//compute normals
			vec3 n1 = ((a.v - b.v).normal() ^ (a.v - c.v).normal()).normal();
			vec3 n2 = ((d.v - b.v).normal() ^ (d.v - c.v).normal()).normal();

			a.n += n1; 
			b.n += n1; b.n += n2;
			c.n += n1; c.n += n2;
			d.n += n2;

			//generate indices
			if (drawLines) {
				indices[indUsed++] = bi;
				indices[indUsed++] = ai;

				indices[indUsed++] = ai;
				indices[indUsed++] = ci;

				indices[indUsed++] = ci;
				indices[indUsed++] = bi;

				indices[indUsed++] = ci;
				indices[indUsed++] = di;

				indices[indUsed++] = di;
				indices[indUsed++] = bi;
			}
			else {
				indices[indUsed++] = ci;
				indices[indUsed++] = bi;
				indices[indUsed++] = ai;

				indices[indUsed++] = ci;
				indices[indUsed++] = di;
				indices[indUsed++] = bi;
			}
		}
	}

	//normalize/smooth normals
	for (int i = 0; i < vertUsed; i++) {
		points[i].n.normalize();
	}*/

	vbo.assign(vertUsed, points, indUsed, indices, GL_DYNAMIC_DRAW);
	object.drawMode = (drawLines)? GL_LINES : GL_TRIANGLES;
	object.bakeAll();
}

