declare -a test_cases=(
	"239854" 
	"99"
	"-2399"
	"-9932"
	"12B"
	"19fdbb92h"
	"7bfH"
	"4dH"
	"9aH"
	"1h"
	"8H"
	"eh"
	"bH"
	"2147483647"
	"2147483648"
	"-2147483648"
	"-000000002147483648"
	"37777777777b"
	"FFFFFFFFh"
	"0h"
	"0b"
);

for test_case in "${test_cases[@]}"
do
	echo "trying $test_case";
	./test "$test_case";
	echo "";
done

