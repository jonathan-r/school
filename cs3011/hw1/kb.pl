pterm(null).
pterm(f0(X)) :- pterm(X).
pterm(f1(X)) :- pterm(X).

incr(null, f1(null)).
incr(f1(X), f0(Y)) :- incr(X, Y), pterm(X).
incr(f0(X), f1(X)) :- pterm(X).

legal(f0(null)).
legal(X) :- legal(P), incr(P, X).

incrR(P, R) :- legal(P), incr(P, R).

add(P1, null, P1) :- pterm(P1). %x + 0 = x
add(null, P2, P2) :- pterm(P2). %0 + x = x
add(f0(P1), f0(P2), f0(P3)) :- add(P1, P2, P3).
add(f0(P1), f1(P2), f1(P3)) :- add(P1, P2, P3).
add(f1(P1), f0(P2), f1(P3)) :- add(P1, P2, P3).
add(f1(P1), f1(P2), f0(P3)) :- add(P1, P2, X), incr(X, P3).

mult(null, P, f0(null)) :- pterm(P). %x * 0 = 0
mult(f1(A), B, X) :- pterm(A), pterm(B), mult(A, f0(B), R), add(R, B, X).
mult(f0(A), B, R) :- pterm(A), pterm(B), mult(A, f0(B), R).

revers_stack(null, X, X).
revers_stack(f0(P), S, R) :- revers_stack(P, f0(S), R).
revers_stack(f1(P), S, R) :- revers_stack(P, f1(S), R).

revers(P, RP) :- revers_stack(P, null, RP).

normalize_rev(f1(RP), X) :- revers(f1(RP), X).
normalize_rev(f0(RP), X) :- normalize_rev(RP, X).

normalize(null, f0(null)).
normalize(P, Y) :- revers(P, RP), normalize_rev(RP, Y).

%
%test code below
%

% test add inputting numbers N1 and N2
testAdd(N1,N2,T1,T2,Sum,SumT) :- numb2pterm(N1,T1), numb2pterm(N2,T2), 
                                 add(T1,T2,SumT), pterm2numb(SumT,Sum).

% test mult inputting numbers N1 and N2  
testMult(N1,N2,T1,T2,N1N2,T1T2) :- numb2pterm(N1,T1), numb2pterm(N2,T2), 
                                   mult(T1,T2,T1T2), pterm2numb(T1T2,N1N2).

% test revers inputting list L
testRev(L,Lr,T,Tr) :- ptermlist(T,L), revers(T,Tr), ptermlist(Tr,Lr).

% test normalize inputting list L
testNorm(L,T,Tn,Ln) :- ptermlist(T,L), normalize(T,Tn), ptermlist(Tn,Ln).

% make a pterm T from a number N    numb2term(+N,?T)
numb2pterm(0,f0(null)).
numb2pterm(N,T) :- N>0, M is N-1, numb2pterm(M,Temp), incr(Temp,T).

% make a number N from a pterm T  pterm2numb(+T,?N)
pterm2numb(null,0).
pterm2numb(f0(X),N) :- pterm2numb(X,M), N is 2*M.
pterm2numb(f1(X),N) :- pterm2numb(X,M), N is 2*M +1.

% reversible  ptermlist(T,L)
ptermlist(null,[]).
ptermlist(f0(X),[0|L]) :- ptermlist(X,L).
ptermlist(f1(X),[1|L]) :- ptermlist(X,L).
