module TastierMachine.Instructions where
import Data.Int (Int8, Int16)
import Data.List (elem)

data Instruction = Add --0
                 | Sub --0
                 | Mul --0
                 | Div --0
                 | Equ --0
                 | Lss --0
                 | Gtr --0
                 | Neg --0
                 | Load --2
                 | Sto  --2
                 | Call --2
                 | LoadG --1
                 | StoG  --1
                 | Const --1
                 | Enter --1
                 | Jmp  --1
                 | FJmp --1
                 | Ret --0
                 | Leave --0
                 | Halt --0
                 | Dup --0
                 | Nop --0
                 --new ones
                 | NotEq --0
                 | LssEq --0
                 | GtrEq --0
                 | LoadBufG --0 new, pops length, pops reference, copies and pushes contents
                 | StoBufG --0 changed, pops reference, pops length, pops and stores contents
                 | ReadInt --0 pushes value
                 | WriteInt --0 pops value
                 | ReadStr --0 pushes contents, pushes length
                 | WriteStr --0 pops reference
                 --new ones this last time
                 | Pop   --pops value to nowhere
                 | StoS  --pops data value, pops dmem reference, and writes value
                 | LoadS --pops dmem reference, reads and pushes data value
                 | UnRef --pops dmem reference
                 | Ref   --pops dmem reference
                 | AllocS --pops size off the stack, pushes new reference
                 | NullCheck --peeks at the top of the stack for reference
                 | Collect --force garbage collection round
                 | HeapStart --takes address paramenter
                 | InputLeft --pushes number of tokens left input

                 deriving (Eq, Ord, Show, Enum)

data InstructionWord = Nullary Instruction
                     | Unary Instruction Int16
                     | Binary Instruction Int16 Int16
                     deriving (Eq, Show)

arguments :: Instruction -> Int
arguments i =
  if i `elem` [Load, Sto, Call] then 2
  else if i `elem` [LoadG, StoG, Const, Enter, Jmp, FJmp, HeapStart] then 1
  else 0
