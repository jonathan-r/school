#ifndef HUFFMAN_H
#define HUFFMAN_H


typedef struct hcode_struct{
	int len; //number of bits at the (logical) end of data that consist of the huffman code for this character
	unsigned data;
} hcode;

typedef struct symbol_struct{
	uint8_t c;
	int freq; 
	struct symbol_struct* lower[2]; //vertical (tree)
	struct symbol_struct* next; //lateral (linked list)
} symbol;


//analyze.c
void analyze_text(uint8_t* str, hcode* codes);

//latex_tree.c
void print_latex(symbol* branch, int depth, char* code);

//decode.c
uint8_t* huffman_decode(uint8_t* encoded, int bytes_compressed, int bytes_decoded, hcode* codes);

//encode.c
uint8_t* huffman_encode(uint8_t* str, hcode* hcodes, int* encoded_bytes);


#endif
