#version 120

uniform mat3 matrix;

in vec2 vPosition; //object
in vec2 vTexCoord;
in vec3 vColor; //d tex / d surface

out vec2 tex;
out vec3 col;

void main() {
	gl_Position = vec4(matrix * (vPosition, 1.0), 1.0f); //NDC

	tex = vTexCoord;
	col = vColor;
}
