#ifndef TWISTER_H
#define TWISTER_H

#include <stdint.h>
#define TWISTER_STATE_SIZE 624
#define TWISTER_M 397

typedef struct twister_s{
	uint32_t state[TWISTER_STATE_SIZE + 1]; //+1 to avoid an (i + 1) mod N
	uint32_t index;
} twister;

void twister_init(twister* t, uint32_t seed);
uint32_t twister_uint(twister* t);
double twister_unit(twister* t);

#endif