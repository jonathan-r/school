#include "pages.h"

using namespace std;
using namespace cv;

//global data
const int seqStartIdx = 1; //minimum

int main(int argc, char** argv) {
	if (argc != 3) {
		printf("usage: pages <page_img> <test_img>\n"
				"Where page_img should be a path pattern for all the known pages to test against,\n"
				"while test_img should be a path pattern for pictures of the pages to test.\n"
				"Any of the arguments may be a single image or may contain a sequence of '?' characters,\n"
				"which represents a sequence of images,\n"
				"with the ?s being replaced by a sequence number starting with 1.\n"
				"The number of ?s represents the number of digits in the squence number,\n"
				"so that leading 0s are added when needed.\n");
		return 0;
	}

	//prcess train and test arguments
	NameTemplate pageNames(argv[1]);
	NameTemplate testNames(argv[2]);

	vector<Mat> referenceImgs;
	loadTemplates(pageNames, referenceImgs);
	printf("found %d reference images\n", referenceImgs.size());
	Mat bluePixels = imread("blue.png");

	//load all the test images, scaled
	int seq = 1;
	string testName = testNames.makeImgName(seq);
	do {
		Mat testImg = imread(testName);
		if (!testImg.data) break;

		int idx;
		bool matched = matchPage(testImg, referenceImgs, bluePixels, idx);

		if (matched) {
			idx++; //1 based;
			printf("image %s matched with template %d - %s\n", testName.c_str(), idx, (idx == getGroundTruth(seq))? "correct" : "wrong");
		}
		else {
			printf("image %s not matched\n", testName.c_str());
		}

		testName = testNames.makeImgName(++seq);
	} while(testNames.isSequence());

	return 0;
}
