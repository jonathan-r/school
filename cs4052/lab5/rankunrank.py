
import math, random

def rank(l):
	inv = [0 for i in range(len(l))]
	for i, k in enumerate(l):
		inv[k] = i

	def rank1(n, pi, ipi):
		if n == 1: return 0
		s = pi[n-1]
		pi[n-1], pi[ipi[n-1]] = pi[ipi[n-1]], pi[n-1]
		ipi[s], ipi[n-1] = ipi[n-1], ipi[s]
		return s + n * rank1(n-1, pi, ipi)

	return rank1(len(l), list(l), inv)

def unrank1(n, r):
	l = [i for i in range(n)]
	for i in reversed(range(1, n+1)):
		rn = r % i
		l[i-1], l[rn] = l[rn], l[i-1]
		r = r // i

	return l

def unrank2(n, r):
	l = [i for i in range(n)]
	for i in reversed(range(1, n+1)):
		f = math.factorial(i-1)
		s = r // f
		l[i-1], l[s] = l[s], l[i-1]
		r = r % f

	return l


def good_perm(n, tries):
	best = None
	bestVal = 0
	bestRank = 0
	m = math.factorial(n)

	for i in range(tries):
		r = random.randint(0, m)
		p = unrank1(n, r)

		v = 0
		for j in range(n-1):
			v += abs(p[j] - p[j+1])

		if v > bestVal:
			best = p
			bestVal = v
			bestRank = r

	return best, bestVal, bestRank
