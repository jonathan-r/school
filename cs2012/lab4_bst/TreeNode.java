 /*
 * Objects of this class represent Binary Tree nodes.
 */
public class TreeNode<Key extends Comparable<Key>, Value> {
	public Key key;
	public Value val;
	public int size; //the number of decendents + 1
	public TreeNode<Key, Value> left, right;

	public TreeNode(Key k, Value v){
		key = k;
		val = v;
		size = 1; //itself
		left = right = null;
	}

	//returns -1 if the node already exists in this subtree
	//returns the depth(from this node, inclusive) at which the insertion was made, otherwise
	public boolean insert(Key k, Value v){
		int cmp = k.compareTo(key);
		if (cmp == 0){
			val = v; //update the value
			return false; //no new node
		}
		else{
			TreeNode<Key, Value> branch = (cmp < 0)? left : right;

			if (branch != null){
				//insert in subtree
				if (branch.insert(k, v)){
					size++; //this subtree gained a new node	
					return true;
				}
				else return false; //no insertion
			} 

			else{
				//insert in this node
				if (cmp < 0) left = new TreeNode<Key, Value>(k, v);
				else right = new TreeNode<Key, Value>(k, v);
				size++;	
				return true;
			}
		}
	}

	//time : O(height)
	//space : O(height)
	public TreeNode<Key, Value> find(Key k){
		int cmp = k.compareTo(key);
		if (cmp == 0) return this;
		else if (cmp < 0 && left != null) return left.find(k);
		else if (cmp > 0 && right != null) return right.find(k);
		else return null;
	}

	//time : O(height)
	//space : O(1)
	public TreeNode<Key, Value> getMin(){
		if (left == null) return this;
		else return left.getMin();
	}

	//time : O(height)
	//space : O(1)
	public TreeNode<Key, Value> getMax(){
		if (right == null) return this;
		else return right.getMax();
	}

	//time : O(n)
	//space : O(height)
	public int height(){
		int h = 1;
		if (left != null) h += left.height(); // >1
		if (right != null) h = Math.max(h, right.height() + 1);
		return h;
	}

	private boolean isBSTInner(Comparable[] lastKey){
		//left
		if (left != null) if (!left.isBSTInner(lastKey)) return false;

		//self
		if (lastKey[0] == null) lastKey[0] = key;
		else{
			if (((Key)lastKey[0]).compareTo(key) >= 0) return false;
			lastKey[0] = key;	
		} 

		//right
		if (right != null) if (!right.isBSTInner(lastKey)) return false;

		return true;
	}

	//time : O(n)
	//aux space : O(1)
	public boolean isBST(){
		Comparable[] lastKey = new Comparable[1]; //a mutable container for a single Key
		lastKey[0] = null;
		return isBSTInner(lastKey);
	}
}
