for f in `find . -type f | grep \.pml$`
do
	name=`basename $f`;
	stem="${name%.*}";
	echo $name $stem;

	spin -DDO_CRASH -a "$f" 
	gcc -o "$stem-crashing" pan.c;

	spin -a "$f";
	gcc -o "$stem-crashfree" pan.c;
done
