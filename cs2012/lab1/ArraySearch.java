// -------------------------------------------------------------------------
/**
 *  This class contains only two static methods that search for an integer
 *  in an array of integers. 
 *  accumulator's value, as well as to add, subtract, multiply, and
 *  divide.  All operations leave their results in the accumulator for
 *  further manipulation.
 *
 *  @author  Vasileios Koutavas
 *  @version 2014.01.13
 */
class ArraySearch
{

	// ----------------------------------------------------------
	/**
	 * Search for an integer inside an UNSORTED array of integers.
	 * This method is static, thus it can be called as ArraySearch.searchInUnsorted(array,i)
	 * @param anArray : An UNSORTED array of integers
	 * @param anInt : an integer
	 * @return true if 'anInt' is contained in 'anArray'; false otherwise
	 */
	static boolean searchInUnsorted(int[] anArray, int anInt)
	{
		for (int elem : anArray)
			if (elem == anInt) return true;

		return false;
	}

	// ----------------------------------------------------------
	/**
	 * Search for an integer inside a SORTED array of integers.
	 * This method is static, thus it can be called as ArraySearch.searchInSorted(array,i)
	 * @param anArray : A SORTED array of integers
	 * @param anInt : an integer
	 * @return true if 'anInt' is contained in 'anArray'; false otherwise
	 */
	static boolean searchInSorted(int[] anArray, int anInt)
	{
		int l = 0;
		int r = anArray.length - 1;

		while (l < r){
			int m = l + (r - l)/2;

			if (anArray[m] < anInt)
				l = m + 1;

			else
				r = m;
		}

		if (anArray.length > 0)
			return anArray[l] == anInt;
		
		return false;
	}
}
