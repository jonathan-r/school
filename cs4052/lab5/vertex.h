#pragma once

#include <GL/glew.h>

#include "generic_shader.h"
#include "maths/mat4.h"
#include "maths/vec.h"

struct vertex {
	vec3 v;
	vec3 n;
	//vec3 ta;
	vec2 t;

	vertex(const vec3& vv, const vec3& nn, const vec2& tt) : v(vv), n(nn), t(tt) {}
	vertex() {}

	static void setAttribPointers(const GenericShader& shader) {
		//vertex shader must use these names for its inputs
		GLuint posLoc = shader.posLoc;
		GLuint norLoc = shader.norLoc;
		//GLuint tanLoc = shader.tanLoc;
		GLuint texLoc = shader.texLoc;
	
		glEnableVertexAttribArray(posLoc);
		glEnableVertexAttribArray(norLoc);
		//glEnableVertexAttribArray(tanLoc);
		glEnableVertexAttribArray(texLoc);
	
		glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, v)));
		glVertexAttribPointer(norLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, n)));
		//glVertexAttribPointer(tanLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, ta)));
		glVertexAttribPointer(texLoc, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*) (offsetof(vertex, t)));
	}
};

struct vertex_ui {
	vec2 v;
	vec2 t;
	vec3 c;

	vertex_ui(const vec2& vv, const vec2& tt, const vec3& cc) : v(vv), t(tt), c(cc) {}
	vertex_ui() {}

	static void setAttribPointers(const UIShader& shader) {
		//vertex shader must use these names for its inputs
		GLuint posLoc = shader.posLoc;
		GLuint texLoc = shader.texLoc;
		GLuint colLoc = shader.colLoc;
	
		glEnableVertexAttribArray(posLoc);
		glEnableVertexAttribArray(texLoc);
		glEnableVertexAttribArray(colLoc);
	
		glVertexAttribPointer(posLoc, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_ui), (void*) (offsetof(vertex_ui, v)));
		glVertexAttribPointer(texLoc, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_ui), (void*) (offsetof(vertex_ui, t)));
		glVertexAttribPointer(colLoc, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_ui), (void*) (offsetof(vertex_ui, c)));
	}
};

