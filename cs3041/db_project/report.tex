\documentclass[12pt,article]{article}
\usepackage[hmargin=1cm,vmargin=1.5cm]{geometry}
\usepackage[romanian]{babel}
\usepackage{combelow}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\usepackage{grffile}
\usepackage{placeins}

\renewcommand{\topfraction}{.85}
\renewcommand{\bottomfraction}{.7}
\renewcommand{\textfraction}{.15}
\renewcommand{\floatpagefraction}{.66}
\renewcommand{\dbltopfraction}{.66}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}

\lstset{language=SQL, tabsize=3, basicstyle=\small, showstringspaces=false, showspaces=false}

\begin{document}
\begin{center}
{\bf RDB Project}\\ 
\vspace{0.25 cm}
\small{\textbf{Jonathan Ro\cb{s}ca}}\\
\small{\textbf{12306261}}\\
\end{center}

\section*{Introduction}
Inspired by the game Transport Tycoon (and its more recent clone, OpenTTD), I have chosen to model a transportation company. It is likely unrealistic and I do not claim to understand the challenges a real transportation company faces. 

In brief, the company owns vehicles, which are driven or piloted by its employees, and carries out shipments from one station to another.

The company maintains a list of vehicle classes(or templates), which contain metadata for every vehicle that was owned, is currently owned, or that the company considers buying in the future. Part of the rationale is that, for vehicles currently owned that are very similar, this reduces redundancy.
Vehicles of the same make and model that have different cargo types/capacities will have separate template entries (and likely have a different price). It may be the case that a vehicle is still available and exists in the database, but should not be bought any more, so its `still\_considered' field is set to false.

The central entity in the ER model is the Vehicle entity. It represents a currently owned and operating vehicle. It has one or more drivers/pilots assigned to it, and a state. A vehicle may be either: waiting in storage (with no driver/pilot assigned), in transit to pick up a shipment, delivering a shipment, in transit to a garage / service station, or broken down.
Each vehicle is equipped to carry one or more kinds of goods, in a certain quantity. This may be changed through refitting, which is done at a garage. Changing the cars attached to a train locomotive is considered refitting. If a vehicle was never refitted (it has no entries in the RefittedCapacities table), its capacities will be taken from its corresponding vehicle template.
Each vehicle has a profit tally counter, which starts at 0 minus the acquisition cost. Running costs such as refueling and service are deducted from this tally, while gross income from delivering shipments is added to it.
Vehicles that have been sold off or are otherwise not in the possession of the company any more are not kept on record.

Vehicles are operated by drivers. Each driver is an employee of the company and has a contract. They are assigned, at any one point, to up to one vehicle and are paid a salary. Each driver is qualified to operate one or more kinds of vehicles.
When a driver's contract is terminated, a record of their employment remains.

Stations are places where vehicles load an unload shipments. Each station has one or more terminal type, which indicates what kind of vehicle can stop at it. This is needed because, for example, aeroplanes cannot land at docks.

Shipments are collections of items that are delivered by vehicles. They originate at one station and are carried to another station by a single vehicle. Shipments can be in one of three states: waiting to be picked up at a station, loaded onto a vehicle (in transit), delivered and paid for, or failed. This state isn't stored explicitly, but is inferred from the values that are filled in the Shipments table entry.

From time to time, vehicles must undergo service to prevent failure. Another reason for a service is a breakdown or an accident. A service can occur at a garage in a station, or in another place.

For simplicity, all money amounts are stored with exact $10^{-2}$ precision (that is, cents) and are assumed to be in a single (unspecified) currency. Hence, amounts in foreign currencies must be converted prior to database insertion.

Quantities are stored as integers, so an appropriately small unit must be used in order to allow for granularity. 
\newpage
\section*{Entity-Relationship Diagram}
\begin{figure}[!ht]
\centering
\includegraphics[scale=0.55]{er2.png}
\end{figure}
\FloatBarrier

\pagebreak
\section*{Relational Schema}
\begin{figure}[tbh]
\centering
\includegraphics[scale=0.55]{schema.png}
\end{figure}
\FloatBarrier

\pagebreak
\section*{Dependency Diagram}
\begin{figure}[tbh]
\centering
\includegraphics[scale=0.55]{dep.png}
\end{figure}
\FloatBarrier

\pagebreak
\section*{Semantic Cosntraints}
\subsection*{Enums}
Vehicle types, cargo types and vehicle states are implemented as strings (short varchars) limited to certain values using a $check$ constraint that is added when the tables are created.

\subsection*{Booleans}
Similarly to enums, booleans are implemented as one character strings (char(1)) and limited to either `y' or `n' to mean true or false, repectively.

\subsection*{Not Null}
Most fields in all tables can never be null. When a field can be null, it has some application significance, such as when a Vehicles table entry has its shipment\_id field null, it means that no shipment is currently assigned to the vehicle.

\section*{Triggers}
\subsection*{ID sequences}
Since Oracle SQL does not support auto-increment for its keys, a trigger implementing this functionality was made for each table with an id column. This also requires a sequence for each of these tables. When an insertion is done, the key field is replaced with the next unique id from the sequence, starting with 1.

\subsection*{Vehicle cleanup}
A trigger was added so that when a row is deleted from the Vehicles table, any corresponding entries in the RefittedCapacities table are deleted, as they are no longer needed.

\subsection*{Shipment deletion}
A trigger was implemented so that when a row is delete from a Shipments table, the trigger checks for any entries referenncing the shipment in the WaitingShipments table and deletes those entries. Without this trigger, a shipment that wasn't picked up and must be deleted (say, becuase it was cancelled), would not be possible to be deleted due to the foreign key dependency.

\section*{View}
A single view was implemented that, given a station id, returns all the vehicles that are parked and can interface (load and unload cargo) at that station. This can be used to narrow down which vehicle should be used to pick up a waiting shipment.

\section*{Security}
The database design is not suited to having roles with partial permissions. For example, it would make sense to have drivers be able to only update the state of their own vehicle, but not the profit or anything. As it stands, a central authority must manage every aspect of the database, or else it could be implemented in an application layer on top.

\section*{Appendix}
\subsection*{Database creation/reset}
\begin{lstlisting}
--drop everything
drop table VehicleClasses cascade constraints;
drop table OriginalCapacities cascade constraints;
drop table Vehicles cascade constraints;
drop table RefittedCapacities cascade constraints;
drop table Drivers cascade constraints;
drop table DriverQualifications cascade constraints;
drop table Stations cascade constraints;
drop table StationTerminals cascade constraints;
drop table ShipmentsWaiting cascade constraints;
drop table Shipments cascade constraints;
drop table ShipmentContents cascade constraints;
drop table ServiceRecord cascade constraints;

--create tables
create table VehicleClasses (
	id integer not null,
	type varchar(10) not null check(type in ('road', 'rail', 'air', 'sea')),
	make varchar(20) not null,
	model varchar(20) not null,
	date_introduced date not null,
	current_price numeric(20, 2),
	still_considered char(1) not null check(still_considered in ('y', 'n')),
	primary key(id)
);

create table OriginalCapacities (
	class_id integer not null,
	type varchar(20) not null check(type in ('passengers', 'boxed', 'liquid', 
		'powder', 'valuables')),
	quantity integer not null,
	primary key(class_id, type),
	foreign key(class_id) references VehicleClasses
);

--must be created before Vehicles table
create table Shipments (
	id integer not null,
	date_available date not null,
	date_picked_up date,
	date_delivered date,
	payment numeric(20, 2),
	primary key(id)
);

create table Vehicles (
	id integer not null,
	class_id integer not null,
	date_man date not null,
	date_acq date not null,
	acq_cost numeric(20, 2),
	state varchar(20) not null check(state in ('parked', 'picking up shipment', 
		'delivering shipment', 'transit to service', 'broken down')),
	km_travelled integer not null,
	profit numeric(20, 2),
	loc_lat float(32) not null,
	loc_lon float(32) not null,
	shipment_id integer,
	primary key(id),
	foreign key(class_id) references VehicleClasses,
	foreign key(shipment_id) references Shipments
);

create table RefittedCapacities (
	vehicle_id integer not null,
	type varchar(20) not null check(type in ('passengers', 'boxed', 'liquid', 
		'powder', 'valuables')),
	quantity integer,
	primary key(vehicle_id, type),
	foreign key(vehicle_id) references Vehicles
);

create table Drivers (
	id integer not null,
	vehicle_assigned integer,
	name varchar(50) not null,
	date_employed date not null,
	date_terminated date,
	salary numeric(20, 2),
	passenger_transport char(1) not null check(passenger_transport in ('y', 
		'n')),
	primary key(id),
	foreign key(vehicle_assigned) references Vehicles
);

create table DriverQualifications (
	driver_id integer not null,
	vehicle_type varchar(10) not null check(vehicle_type in ('road', 'rail', 
		'air', 'sea')),
	primary key(driver_id, vehicle_type),
	foreign key(driver_id) references Drivers
);

create table Stations (
	id integer not null,
	name varchar(50) not null,
	location_lat float not null,
	location_lon float not null,
	primary key(id)
);

create table StationTerminals (
	station_id integer not null,
	vehicle_type varchar(10) not null check(vehicle_type in ('road', 'rail', 
		'air', 'sea')),
	primary key(station_id, vehicle_type),
	foreign key(station_id) references Stations
);

create table ShipmentsWaiting (
	station_id integer not null,
	shipment_id integer not null, 
	primary key(station_id, shipment_id),
	foreign key(station_id) references Stations,
	foreign key(shipment_id) references Shipments
);

create table ShipmentContents (
	shipment_id integer not null,
	cargo_type varchar(20) not null check(cargo_type in ('passengers', 'boxed', 
		'liquid',
		'powder', 'valuables')),
	quantity integer not null,
	primary key(shipment_id, cargo_type),
	foreign key(shipment_id) references Shipments
);

create table ServiceRecord (
	id integer not null,
	vehicle_id integer not null,
	date_started date not null,
	reason varchar2(300) not null,
	cost numeric(20, 2) not null,
	station_id integer, 
	primary key(id),
	foreign key(vehicle_id) references Vehicles,
	foreign key(station_id) references Stations
);

--create triggers
--http://stackoverflow.com/questions/10613846/create-table-with-sequence-nextval-in-oracle
drop sequence vehicle_class_id_seq;
create sequence vehicle_class_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_vehicle_class_id
	before insert on VehicleClasses
	for each row
declare
	v_id integer;
begin
	select vehicle_class_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence vehicle_id_seq;
create sequence vehicle_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_vehicle_id
	before insert on Vehicles
	for each row
declare
	v_id integer;
begin
	select vehicle_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence driver_id_seq;
create sequence driver_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_driver_id
	before insert on Drivers
	for each row
declare
	v_id integer;
begin
	select driver_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence shipment_id_seq;
create sequence shipment_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_shipment_id
	before insert on Shipments
	for each row
declare
	v_id integer;
begin
	select shipment_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;

drop sequence station_id_seq;
create sequence station_id_seq start with 1 increment by 1 nocache nocycle;

--generate a new, unique id regardless of what is passed into the insert
create or replace trigger insert_station_id
	before insert on Stations
	for each row
declare
	v_id integer;
begin
	select station_id_seq.nextval into v_id from dual;
	:new.id := v_id;
end;
.
run;


--delete the refitted capacities when a vehicle is deleted
create or replace trigger delete_vehicle_cleanup
	before delete on Vehicles
	for each row
declare
	v_id integer;
begin
	v_id := :old.id;
	delete from RefittedCapacities where vehicle_id = v_id;
end;
.
run;

--make it possible to delete a shipment waiting in a station
--by deleting its ShipmentsWaiting entry beforehand
create or replace trigger delete_shipments_waiting
	before delete on Shipments
	for each row
declare
	v_id integer;
begin
	v_id := :old.id;
	delete from ShipmentsWaiting where shipment_id = v_id;
end;
.
run;

--view
create or replace view vehicles_at_station (station_id) as 
	select id from Vehicles where Vehicles.state = 'parked' and 
		(select type from VehicleClasses where VehicleClasses.id = 
			Vehicles.class_id) in
			(select vehicle_type from StationTerminals where 
				StationTerminals.station_id = station_id);
\end{lstlisting}
\vspace{5cm}
\subsection*{Populating the tables}
\begin{lstlisting}
insert into VehicleClasses values (0, 'road', 'ford', 'T1', 
	TO_DATE('1909/05/03', 'yyyy/mm/dd'), 25000, 'n');
insert into Stations values (null, 'connolly', 54.2, 4.4);
insert into Shipments values(null, TO_DATE('1910', 'yyyy'), null, null, null);
insert into ShipmentsWaiting values (1, 1);
insert into ShipmentsWaiting values (2, 3);
\end{lstlisting}

\end{document}
