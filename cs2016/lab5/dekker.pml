//framework
int done = 0; //how many threads didn't crash or starve
int crashed = 0; //how many threads crashed
int critical = 0; //how many threads are in the critical section

//program
int n = 0;

//system
bool flag[2];
int turn = 0;

//starvation/deadlock watchdog
ltl not_starved { always (!timeout || crashed + done == 2) };

proctype thread(int self){
	//framework
	int other = 1 - self;
	
	//program
	int temp;
	int i;
	
	for (i : 1 .. 10){
		//enter critical section
		flag[self] = true;
		do
		:: (!flag[other]) -> break;
		:: else ->
			if
			::  (turn != self) ->
				flag[self] = false;
				(turn == self); //wait
				flag[self] = true
			:: else -> skip;
			fi;
		od;
		critical++;

		//critical section
		assert(critical == 1); //exclusive
		temp = n;
		n = temp + 1;

		//exit critical section
		critical--;
		turn = other;
		flag[self] = false;

		//non critical section
#ifdef DO_CRASH
		do
		:: break; //don't crash
		:: goto crash; //do crash
		od;
#endif
	}

	done++;
	goto end;

crash:
	crashed++;

end:
}

init{
	run thread(0); run thread(1);
}