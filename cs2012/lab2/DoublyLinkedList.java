// -------------------------------------------------------------------------
/**
 *	This class contains the methods of Doubly Linked List.
 *
 *	@author  Jonathan Roșca
 *	@version 2014.01.19
 */


/**
 * Class DLLNode: implements a *generic* Doubly Linked List node.
 * @param <T> This is a type parameter. T is used as a class name in the
 * definition of class DLLNode. When creating a new DLLNode, T should be
 * instantiated with an actual class name. For example to create a new DLLNode
 * class containing string data: 
 *		DLLNode<String> myStringNode = new DLLNode<String>();
 */
class DLLNode<T extends Comparable<T>>{
	public T data;
	public DLLNode<T> next;
	public DLLNode<T> prev;

	/**
	 * Constructor - inserts the new node between prevNode and nextNode, fixing the links
	 * @param theData : data of type T, to be stored in the node
	 * @param prevNode : the previous Node in the Doubly Linked List
	 * @param nextNode : the next Node in the Doubly Linked List
	 * @return DLLNode<T>
	 */
	public DLLNode( T theData, DLLNode<T> prevNode, DLLNode<T> nextNode ){
		data = theData;
		prev = prevNode;
		next = nextNode;
		if (prevNode != null) prevNode.next = this;
		if (nextNode != null) nextNode.prev = this;
	}

	/**
	 * compareTo
	 * @param otherData : an object of class T.
	 * @return 0 if data in this.data is equal to otherData (equality is
	 * determined by the "compareTo" method of the instance of class T);
	 * otherwise returns an integer less than 0 if this.data is smaller than otherData.
	 * otherwise returns an integer greater than 0 if this.data is greater than otherData.
	 */
	//public int compareTo( T otherData ){
	//	return data.compareTo(otherData);
	//}
}


/**
 * Class DoublyLinkedList: implements a *generic* Doubly Linked List.
 * @param <T> This is a type parameter. T is used as a class name in the
 * definition of this class.
 *
 * When creating a new DoublyLinkedList, T should be instantiated with an
 * actual class name that extends the class Comparable.
 * Such classes include String and Integer.
 *
 * For example to create a new DoublyLinkedList class containing String data: 
 *		DoublyLinkedList<String> myStringList = new DoublyLinkedList<String>();
 *
 * 
 * This is a bare minimum class you would need to completely implement.
 * You can add additional methods to support your code. Each method will need
 * to be tested by your jUnit tests -- for simplicity in jUnit testing
 * introduce only public methods.
 */
class DoublyLinkedList<T extends Comparable<T>> {
	private DLLNode<T> head, tail;

	/**
	 * Constructor
	 * @return DoublyLinkedList
	 */
	public DoublyLinkedList(){
		head = tail = null;
	}

	/**
	 * Tests if the doubly linked list is empty
	 * @return true if list is empty, and false otherwise
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	If there are no elements, then head will be null. 
	 *	This can be checked in one operation regardless of the state of the array.
	 */
	public boolean isEmpty(){
		return head == null;
	}

	/**
	 * Inserts an element at the beginning of the doubly linked list
	 * @param data : The new data of class T that needs to be added to the list
	 * @return none
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	Since the current head element of the list is always known, we can simply add a new node before it.
	 */
	public void insertFirst( T data ) {
		head = new DLLNode<T>(data, null, head);
		if (tail == null) tail = head; //list was empty
	}

	/**
	 * Inserts an element at the end of the doubly linked list
	 * @param data : The new data of class T that needs to be added to the list
	 * @return none
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	Since the current tail element is kept track of, we can simply add a new node after it.
	 */
	public void insertLast( T data ) {
		tail = new DLLNode<T>(data, tail, null);
		if (head == null) head = tail; //list was empty
	}

	/**
	 * Inserts an element in the doubly linked list
	 * @param pos : The integer location at which the new data should be
	 *		inserted in the list. We assume that the head position in the list
	 *		is 0 (zero). If pos is less than 0 then add to the head of the list.
	 *		If pos is greater or equal to the size of the list then add the
	 *		element at the end of the list.
	 * @param data : The new data of class T that needs to be added to the list
	 * @return none
	 *
	 * Worst-case asymptotic runtime cost: O(n)
	 *
	 * Justification:
	 *	In case the list is not empty, we must traverse the list until the pos element, 
	 *	which may mean going over the entire list.
	 */
	public void insertBefore( int pos, T data ) {
		//traverse to the element with index pos
		int i = 0;
		DLLNode<T> it = head;
		while (i < pos && it != null){
			it = it.next;
			i++;
		}
		
		if (it != null){
			//in the middle of the array
			DLLNode<T> n = new DLLNode<T>(data, it.prev, it);
			if (it == head) head = n;
		}
		
		else if (tail != null){
			//pos >= number of elements in the list
			tail = new DLLNode<T>(data, tail, null);
		}
		
		else{
			//empty list
			head = tail = new DLLNode<T>(data, null, null);
		}
	}

	
	/**
	 * @return the data at the beginning of the list, if the list is non-empty, and null otherwise.
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	The head element is always known, so getting the data always takes one operation.
	 */
	public T getFirst(){
		if (head != null) return head.data;
		else return null;
	}

	/**
	 * @return the data at the of the list, if the list is non-empty, and null otherwise.
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	The tail element is always known, so getting the data always takes one operation.
	 */
	public T getLast(){
		if (tail != null) return tail.data;
		else return null;
	}

	/**
	 * Returns the data stored at a particular position
	 * @param pos : the position
	 * @return the data at pos, if pos is within the bounds of the list, and null otherwise.
	 *
	 * Worst-case asymptotic runtime cost: O(n)
	 *
	 * Justification:
	 *	To get to the element with position pos, we must traverse the list
	 *
	 * Worst-case precise runtime cost: 2(n + 1) + 2n + 6
	 *
	 * Justification:
	 *	The worst case involves having the while loop execute until "it" becomes "tail.next",
	 */
	public T get( int pos ) {
		if (pos < 0) return null; //2
		else{
			
			//traverse to the element with index pos
			int i = 0; //1
			DLLNode<T> it = head; //1
			while (i < pos && it != null){ //2 * (n + 1)
				it = it.next; //n
				i++; //n
			}
			
			if (it != null) return it.data; //2
			else return null; //2, but mutually exclusive with the previous one
		}
	}


	/**
	 * Deletes an element at the beginning of the doubly linked list
	 * @return true : on successful deletion, false : list has not been modified. 
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	The head element is always known. Its successor can be accessed in one operation.
	 */
	public boolean deleteFirst() {
		if (head != null) {
			head = head.next;
			if (head == null) tail = null; //that was the tail element
			else head.prev = null;
			return true;
		}
		
		else return false;
	}

	/**
	 * Deletes an element at the end of the doubly linked list
	 * @return true : on successful deletion, false : list has not been modified. 
	 *
	 * Worst-case asymptotic runtime cost: O(1)
	 *
	 * Justification:
	 *	The tail element is always known. Its predecessor can be accessed in one operation.
	 */
	public boolean deleteLast() {
		if (tail != null) {
			tail = tail.prev;
			if (tail == null) head = null; //that was the head element
			else tail.next = null;
			return true;
		}
		
		else return false;
	}

	
	/**
	 * Deletes the element of the list at position pos.
	 * head element in the list has position 0. If pos points outside the
	 * elements of the list then no modification happens to the list.
	 * @param pos : the position in the list to be deleted.
	 * @return true : on successful deletion, false : list has not been modified. 
	 *
	 * Worst-case asymptotic runtime cost: O(n)
	 *
	 * Justification:
	 *	To reach the element with index pos, we must traverse the list.
	 */
	public boolean deleteAt( int pos ) {
		if (pos < 0) return false;
		else{

			//traverse to the element with index pos
			int i = 0;
			DLLNode<T> it = head;
			while (i < pos && it != null){
				it = it.next;
				i++;
			}
			
			if (it != null) {
				if (it == head){
					deleteFirst();
				}
				else if (it == tail){
					deleteLast();
				}
				else{
					it.prev.next = it.next;
					it.next.prev = it.prev;
				}

				return true;
			}

			else return false;
		}
	}


	/**
	 * @return a string with the elements of the list as a comma-separated
	 * list, from beginning to end
	 *
	 * Worst-case asymptotic runtime cost:	 Theta(n)
	 *
	 * Justification:
	 *	Let 'n' be the size of the list.
	 *	There is no worst-/best-case here. No matter what the list contains,
	 *	the while loop will iterate over all elements of the list.
	 */
	public String toString() {
		String s = "";
		boolean isFirst = true; 

		// iterate over the list, starting from the head
		DLLNode<T> i = head;
		while (i != null){
			if (!isFirst){
				s += ",";
			}
			isFirst = false;
			s += i.data.toString();
			i = i.next;
		}

		return s;
	}


	/**
	 * @return a string with the elements of the list as a comma-separated
	 * list, from beginning to end (alternative implementation).
	 *
	 * Worst-case asymptotic runtime cost:	 Theta(n)
	 *
	 * Justification:
	 *	Let 'n' be the size of the list.
	 *	There is no worst-/best-case here. No matter what the list contains,
	 *	the while loop will iterate over all elements of the list.
	 */
	public String toStringAlt() {
		String s = "";
		boolean isFirst = true; 

		// iterate over the list, starting from the tail
		DLLNode<T> i = tail;
		while (i != null){
			if (!isFirst){
				s = "," + s;
			}
			isFirst = false;
			s = i.data.toString() + s;
			i = i.prev;
		}

		return s;
	}
}
