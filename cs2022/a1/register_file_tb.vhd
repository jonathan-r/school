library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;

entity register_file_tb is
end register_file_tb;
 
architecture behavior of register_file_tb is 
	component register_file
	port(src : in  std_logic_vector(2 downto 0);
	 des : in  std_logic_vector(2 downto 0);
	 clk : in  std_logic;
	 data_src : in  std_logic;
	 ext_data : in  std_logic_vector(15 downto 0);
	 reg_out : out  data_array
	);
	end component;

	signal src : std_logic_vector(2 downto 0) := (others => '0');
	signal des : std_logic_vector(2 downto 0) := (others => '0');
	signal clk : std_logic := '0';
	signal data_src : std_logic := '0';
	signal ext_data : std_logic_vector(15 downto 0) := (others => '0');
	signal reg_out : data_array;

	constant clk_period : time := 2 ps;
	
begin
	uut: register_file port map (src, des, clk, data_src, ext_data, reg_out);

	clk_process : process begin
		clk <= '0';
		wait for clk_period/2;

		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process begin
		wait for clk_period;
		src <= "000";

		-- external data to register transfer
		data_src <= '0'; -- external
		for i in 0 to 7 loop
			des <= std_logic_vector(to_unsigned(i, 3));
			ext_data <= x"1ba4";
			wait for clk_period; --transfer
			assert reg_out(i) = x"1ba4"; --after the transfer
		end loop;

		--register to register transfer
		for i in 0 to 7 loop
		
			--load source register with unique value
			data_src <= '0';
			des <= std_logic_vector(to_unsigned(i, 3));
			ext_data <= x"2cf7";
			wait for clk_period;

			--transfer from this register to every register, including itself
			src <= std_logic_vector(to_unsigned(i, 3));

			for j in 0 to 7 loop
				if i /= j then
					assert reg_out(i) /= reg_out(j); --before the transfer
				end if;
	
				report "tranfer from " & integer'image(i) & " to " & integer'image(j);
	
				data_src <= '1'; --register to register
				des <= std_logic_vector(to_unsigned(j, 3));
				wait for clk_period; --transfer
				assert reg_out(i) = reg_out(j); --after the transfer
	
				if i /= j then
					--reset j-th register
					data_src <= '0';
					des <= std_logic_vector(to_unsigned(j, 3));
					ext_data <= x"1ba4";
					wait for clk_period;
				end if;
			end loop;

			--reset i-th register
			data_src <= '0';
			des <= std_logic_vector(to_unsigned(i, 3));
			ext_data <= x"1ba4";
			wait for clk_period;
		end loop;
	end process;
end;
