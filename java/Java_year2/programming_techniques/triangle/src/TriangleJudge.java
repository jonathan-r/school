import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Arrays;
import java.util.Scanner;

public class TriangleJudge {
	public static final int TRIANGLE_IMPOSSIBLE = 0;
	public static final int TRIANGLE_EQUILATERAL = 1;
	public static final int TRIANGLE_RIGHT_ANGLE = 2;
	public static final int TRIANGLE_ACUTE = 3;
	public static final int TRIANGLE_OBTUSE = 4;

	private static final double epsilon = 0.00001;

	// pre: a < b < c
	// returns a symbolic constant (see above) for the type of triangle
	public static int judge(double a, double b, double c) {
		assert(a < b && b < c);
		
		// if a any side is 0, this is not a triangle
		if (Math.abs(a) < epsilon || a < 0 || c > a + b) { // only the smallest side must be checked
			return TRIANGLE_IMPOSSIBLE;
		}

		if (Math.abs(a - b) < epsilon && Math.abs(a - c) < epsilon && Math.abs(b - c) < epsilon) { // and a,b,c != 0
			return TRIANGLE_EQUILATERAL;
		}

		double diff = c * c - (a * a + b * b);

		if (Math.abs(diff) < epsilon) {
			return TRIANGLE_RIGHT_ANGLE;
		} 
		
		else {
			if (diff < 0.0)
				return TRIANGLE_ACUTE;

			else
				return TRIANGLE_OBTUSE;
		}
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the three lengths of the triangle:");
		String line = br.readLine();

		double[] sides = new double[3];

		// parse in the three numbers
		Scanner scan = new Scanner(line);
			
		int i = 0;
		while(i < 3 && scan.hasNextDouble()){
			sides[i++] = scan.nextDouble();
		}
		
		scan.close();

		if (i == 3) {
			Arrays.sort(sides);

			int type = judge(sides[0], sides[1], sides[2]);
			switch (type) {
			case TRIANGLE_IMPOSSIBLE:
				System.out.println("This is not a triangle");
				break;

			case TRIANGLE_EQUILATERAL:
				System.out.println("This is an equilateral triangle");
				break;

			case TRIANGLE_RIGHT_ANGLE:
				System.out.println("This is a right angle triangle");
				break;

			case TRIANGLE_ACUTE:
				System.out.println("This is an acute triangle");
				break;

			case TRIANGLE_OBTUSE:
				System.out.println("This is an obtuse triangle");
				break;
			}
		}

		else {
			System.out.println("invalid input");
		}
	}
}
