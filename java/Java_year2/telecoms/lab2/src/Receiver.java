import java.lang.Thread;

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;

import java.util.Date;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;


public class Receiver {
	private static class SenderInfo{
		//connection
		public InetSocketAddress address;
		public int id;
		public int windowSize;
		public int waitingOn; //which frame is expected
		public int packetsReceived;
		
		public boolean hasMetaData;
		
		//content
		public Date started;
		public String fileName;
		public File file;
		public FileOutputStream fileOut;
		public long received;
		public long fileSize;
		
		public SenderInfo(InetSocketAddress address, int windowSize){
			this.address = address;
			this.windowSize = windowSize;
			waitingOn = 0;
			packetsReceived = 0;
						
			hasMetaData = false;
			
			started = null;
			fileName = "";
			file = null;
			received = 0;
			fileSize = 0;
		}
	}

	private DatagramSocket socket; //sendind socket
	private Listener listener;
	private boolean running;
	private ArrayList<SenderInfo> senders; //record of all active senders
	
	public Receiver(int port) {
		try {
			senders = new ArrayList<SenderInfo>();
			listener = new Listener(port, null); //store packets from every source
			socket = new DatagramSocket();
			running = true;
			
			listener.start();
		}
		
		catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	//adds the sender to the senders arraylist and returns its index. the first free slot is picked
	public int addSender(InetSocketAddress address, int window){
		SenderInfo sender = new SenderInfo(address, window);
		for (int i = 0; i < senders.size(); i++){
			//find a free one
			if (senders.get(i) == null){
				senders.set(i, sender);
				return i;
			}
		}
		
		//otherwise add a new one
		senders.add(sender);
		return senders.size() - 1;
	}
	
	//remove a sender from the record
	public void finishSender(int id){
		senders.remove(id);	
	}
	
	public static String getDateDiff(Date date1, Date date2) {
		String str = "";
		long diff = date2.getTime() - date1.getTime();
		
		long hour = TimeUnit.MILLISECONDS.toHours(diff);
		diff -= hour * 60 * 60 * 1000;
		if (hour > 0){
			str = Long.toString(hour) + "h";
		}
		
		long min = TimeUnit.MILLISECONDS.toMinutes(diff);
		diff -= min * 60 * 1000;
		if (min > 0){
			if (str.length() > 0) str += " ";
			str += Long.toString(min) + "m";
		}
		
		long sec = TimeUnit.MILLISECONDS.toSeconds(diff);
		diff -= sec * 1000;
		if (sec > 0){
			if (str.length() > 0) str += " ";
			str += Long.toString(sec) + "s";
		}

		long milli = TimeUnit.MILLISECONDS.toMillis(diff);
		if (milli > 0) str += Long.toString(milli) + "ms";

		return str;
	}
	
	public void start(){
		DatagramPacket packet, sendPacket;
		byte[] sendBuffer;
		
		while (running) try{
			packet = listener.getPacket();
			if (packet == null){
				Thread.sleep(1);
				continue;
			}
			
			SenderInfo sender = null;
			
			byte[] incomingBuffer = packet.getData();
			ByteArrayInputStream inBytes = new ByteArrayInputStream(incomingBuffer);
			DataInputStream in = new DataInputStream(inBytes);
			
			int type = in.readUnsignedByte();
			int seq = in.readUnsignedShort();
			int senderId = in.readUnsignedShort();
			
			ByteArrayOutputStream outBytes = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(outBytes);
			
			if (type == Symbols.PACKET_SENDER_INIT){
				//a new sender wants to connect. this type of packet does not need a sequence number
				int destPort = in.readUnsignedShort();
				int window = in.readUnsignedShort();
				
				InetSocketAddress dest = new InetSocketAddress(((InetSocketAddress)packet.getSocketAddress()).getAddress(), destPort);
				senderId = addSender(dest, window);
				sender = senders.get(senderId);
				System.out.println("New connection from " + dest.getAddress() + ":" + destPort + ". It is given the index " + senderId);
				
				out.writeByte(Symbols.PACKET_ACK_INIT);
				out.writeShort(0); //sequence doesn't matter
				out.writeShort(senderId);
				
				byte[] outBytesArray = outBytes.toByteArray();
				sendPacket = new DatagramPacket(outBytesArray, outBytesArray.length, sender.address);
				socket.send(sendPacket);
			}
				
			else{
				if (senderId < senders.size()){
					sender = senders.get(senderId);
					if (sender != null){
						if (seq == sender.waitingOn){
							System.out.println("received packet " + sender.packetsReceived + ", with sequence id " + sender.waitingOn);
							sender.packetsReceived++;
							sender.waitingOn++;
				
							switch(type){
								case Symbols.PACKET_FILE_HEADER:
									if (!sender.hasMetaData){
										sender.fileSize = in.readLong();
										sender.fileName = in.readUTF();
										sender.file = new File(sender.fileName);
										sender.fileOut = new FileOutputStream(sender.file); //TODO: check if file already exists
										sender.hasMetaData = true;
								
										System.out.println("Sender " + sender.id + " is sending us \"" + sender.fileName + "\", " + sender.fileSize + " bytes long");
									}
					
									break;	
					
					
								case Symbols.PACKET_FILE_DATA:
									if (sender.fileOut != null){
										if (sender.started == null) sender.started = new Date(); //this is the first packet
						
										int got = packet.getLength() - Symbols.PACKET_HEADER_SIZE;
										if (sender.fileSize - sender.received < Symbols.PACKET_SIZE) got = (int)(sender.fileSize - sender.received);
										sender.received += got;
										System.out.println("Received " + sender.received + " bytes out of " + sender.fileSize);
										sender.fileOut.write(incomingBuffer, Symbols.PACKET_HEADER_SIZE, got);
					
										
										if (sender.received >= sender.fileSize){
											sender.fileOut.close();
											finishSender(senderId);
											System.out.println("Finished receiving \"" + sender.fileName + "\", took " + getDateDiff(sender.started, new Date()));
										}
									}
									
									break;
				
				
								default:
									break;
							}
					
							//send ack
							out.writeByte(Symbols.PACKET_ACK_SEQ);
							out.writeShort(sender.waitingOn);
				
							byte[] outBytesArray = outBytes.toByteArray();
							sendPacket = new DatagramPacket(outBytesArray, outBytesArray.length, sender.address);
							if (Math.random() < 0.3) socket.send(sendPacket);
							
							System.out.println("sent ack for packet " + sender.waitingOn);
							
							if (sender.waitingOn == sender.windowSize){
								sender.waitingOn = 0;
								System.out.println("Received entire window");
							}
						}
					}
				}
			}
		}
		
		catch(Exception e){
			e.printStackTrace();
		}	
	}

	public static void main(String[] args) throws java.lang.Exception {
		Receiver r;
		int port = Symbols.DEFAULT_RECEIVER_PORT;
	
		if (args.length == 1){
			port = Integer.parseInt(args[0]);	
		}
		
		else if (args.length != 0){
			System.out.println("Usage: receiver <port>");
			return;
		}
		
		System.out.println("Starting server on port " + port);
		r = new Receiver(port);
		r.start();
	}
}
