	AREA	AsmTemplate, CODE, READONLY
	IMPORT	main

; sample pressing one of the switches makes the "next" of the 4 LEDs P1.16, P1.17, P1.18, P1.19
; go on and off in sequence
; (c) Mike Brady, 2012.

; this program sample is only partically complete:
; it does not wait for the end of the switch press

	EXPORT	start
start

IO1DIR	EQU	0xE0028018
IO1SET	EQU	0xE0028014
IO1CLR	EQU	0xE002801C
IO1PIN	EQU	0xE0028010


IO0DIR	EQU	0xE0028008
IO0SET	EQU	0xE0028004
IO0CLR	EQU	0xE002800C

; The follow equates describe how each each segment is connected to a pin on port P0.
; You may have to change them to reflect the actual wiring you have chosen on your board
SEGABIT		equ	9	; this means that segment a is wired to P0.9
SEGBBIT		equ	10	; this means that segment b is wired to P0.10
SEGCBIT		equ	22;11;12
SEGDBIT		equ	12;13
SEGEBIT		equ	13;16
SEGFBIT		equ	23;14;8
SEGGBIT		equ	15;19
SEGDPBIT	equ	16;20 



		ldr	r1,=IO0DIR
		;select the pins that are driving the segments & d.p.
		ldr	r2,=(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT)
		str	r2,[r1]		;make them outputs

		ldr	r1,=IO0SET
		ldr	r2,=IO0CLR
		; r1 points to the SET register
		; r2 points to the CLEAR register
		ldr	r5,=lut		; point to the look-up table

		ldr	r0,=IO1PIN

; r1 points to the SET register
; r2 points to the CLEAR register

		ldr r7,[r0]
		mov r7, r7, lsr #20	;each bit in r7 corresponds to a button state
		and r7, r7, #0xF
		mov r8, #0

outer_loop
		
repeek
		ldr	r6,[r0]
		ldr	r7,=0x00F00000	; mask for switch bits
		and	r6,r6,r7
		cmp	r6,r7			; if any switch is pressed, at least one of the remaining bits in r6 will be zero
		beq	repeek
		mov r10, r6, lsr #20
		eor r10, r10, #0xF

repeek2
		ldr	r6,[r0]
		ldr	r7,=0x00F00000	; mask for switch bits
		and	r6,r6,r7
		cmp	r6,r7			; if any switch is pressed, at least one of the remaining bits in r6 will be zero
		bne	repeek2	

		mov r6, r6, lsr #20
		;eor r6, r6, #0xF
		and r6, r6, #0xF

		;load the last light combination
		ldr	r3,[r5,r8,lsl #2]	; pick up the 7-seg code for this number
		str	r3,[r2]		; turn segments off

		
		and r9, r10, #2_11
		cmp r9, #0

		addne r8, r8, #1
		subeq r8, r8, #1

		cmp r8, #0x10
		moveq r8, #0
		cmp r8, #-1
		moveq r8, #15

		ldr	r3,[r5,r8,lsl #2]	; pick up the 7-seg code for this number
		str	r3,[r1]	 	; turn segments on


		;delay for about a half second
		ldr	r4,=6000000
dloop	subs	r4,r4,#1
		bne	dloop

		mov r7, r6	
		b outer_loop


stop	B	stop		; never reached


lut	DCD	(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT) 				; '0'
	DCD	(1<<SEGBBIT)+(1<<SEGCBIT) 																	; '1'
	DCD	(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGGBIT) 							; '2'
	DCD	(1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGGBIT) 							; '3'
	
	DCD	(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGFBIT)+(1<<SEGGBIT) 														; 4
	DCD (1<<SEGABIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)											; 5
	DCD (1<<SEGABIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)								; 6
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT) 																		; 7
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT) 					; 8
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGFBIT)+(1<<SEGGBIT) 											; 9
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT)					; A
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT)	; B
	DCD (1<<SEGABIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGDPBIT) 											; C
	DCD (1<<SEGABIT)+(1<<SEGBBIT)+(1<<SEGCBIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGDPBIT) 				; D
	DCD (1<<SEGABIT)+(1<<SEGDBIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT) 								; E
	DCD (1<<SEGABIT)+(1<<SEGEBIT)+(1<<SEGFBIT)+(1<<SEGGBIT)+(1<<SEGDPBIT) 											; F

;etc for all 10 (or all 16) digits


	END