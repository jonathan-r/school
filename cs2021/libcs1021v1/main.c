#include "LPC213x.h"                    /* LPC23xx/LPC24xx definitions        */
#include <stdio.h>

extern void start_asm (void);
extern void init_serial (void);

int main (void)
{
	init_serial();
	printf("\r\nCalling start() ...\r\n");

	start_asm();
	printf("start() returned.\r\n");
	while (1);
}
