#define rand	pan_rand
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* PROC :init: */
	case 3: /* STATE 1 - test.pml:16 - [(run thread())] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][1] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 4: /* STATE 2 - test.pml:16 - [(run thread())] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][2] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 5: /* STATE 3 - test.pml:17 - [((done==2))] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][3] = 1;
		if (!((now.done==2)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 6: /* STATE 4 - test.pml:18 - [assert((n!=2))] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][4] = 1;
		spin_assert((now.n!=2), "(n!=2)", II, tt, t);
		_m = 3; goto P999; /* 0 */
	case 7: /* STATE 5 - test.pml:19 - [-end-] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][5] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC thread */
	case 8: /* STATE 1 - test.pml:8 - [i = 1] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][1] = 1;
		(trpt+1)->bup.oval = ((P0 *)this)->i;
		((P0 *)this)->i = 1;
#ifdef VAR_RANGES
		logval("thread:i", ((P0 *)this)->i);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 9: /* STATE 2 - test.pml:8 - [((i<=10))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][2] = 1;
		if (!((((P0 *)this)->i<=10)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 10: /* STATE 3 - test.pml:9 - [temp = n] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][3] = 1;
		(trpt+1)->bup.oval = ((P0 *)this)->temp;
		((P0 *)this)->temp = now.n;
#ifdef VAR_RANGES
		logval("thread:temp", ((P0 *)this)->temp);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 11: /* STATE 4 - test.pml:10 - [n = (temp+1)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][4] = 1;
		(trpt+1)->bup.oval = now.n;
		now.n = (((P0 *)this)->temp+1);
#ifdef VAR_RANGES
		logval("n", now.n);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 12: /* STATE 5 - test.pml:8 - [i = (i+1)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][5] = 1;
		(trpt+1)->bup.oval = ((P0 *)this)->i;
		((P0 *)this)->i = (((P0 *)this)->i+1);
#ifdef VAR_RANGES
		logval("thread:i", ((P0 *)this)->i);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 13: /* STATE 11 - test.pml:12 - [done = (done+1)] (0:0:1 - 3) */
		IfNotBlocked
		reached[0][11] = 1;
		(trpt+1)->bup.oval = now.done;
		now.done = (now.done+1);
#ifdef VAR_RANGES
		logval("done", now.done);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 14: /* STATE 12 - test.pml:13 - [-end-] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][12] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

