#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h> //usleep
#include <pthread.h> //threads

#define BLOCKS_PER_WORKER_PER_CHUNK 10

//pi.c
void pi_9_digits(int starting, char out[]);

//thread_readonly
int num_workers; //how many digit groups should each worker skip
int chunk_base; //first digit of the chunk

//collector readonly
//TODO: maybe use one buffer per thread for better cache performance?
char* chunk; //aggregation of blocks

typedef struct worker_info_s{
	int first_digit;
	int blocks; //how many iterations should it do
} worker_info;


void* worker(void* data){
	worker_info* self = data;
	char buf[10]; //9 digits + null terminator
	
	char* c = chunk;

	int n = self->first_digit;
	for (int i = 0; i < self->blocks; i++){
		pi_9_digits(n, buf);

		memcpy(chunk + (n - chunk_base), buf, 9);
		n += 9 * num_workers;
	}

	pthread_exit(NULL);
}


int main(int argc, char** argv){
	if (argc != 2 && argc != 3){
		printf("Usage: pi <digits> [threads]. Prints result to stdout\n");
		printf("Digits refers to decimal digits.\n");
		printf("If not specified, threads is the number of hardware threads\n");
	}

	else{
		int digits = strtol(argv[1], NULL, 10);

		if (argc == 3)
			num_workers = strtol(argv[2], NULL, 10);
		else
			num_workers = sysconf(_SC_NPROCESSORS_ONLN);

		if (digits > 0 || num_workers > 0){
			pthread_t* handles = malloc(num_workers * sizeof(pthread_t));
			worker_info* info = malloc(num_workers * sizeof(worker_info));

			int chunk_size = 9 * BLOCKS_PER_WORKER_PER_CHUNK * num_workers;
			chunk = malloc(chunk_size + 1); //+1 for the null terminator
			chunk[chunk_size] = '\0';

			int chunks = digits / chunk_size; //full chunks
			int extra = digits % chunk_size;

			int groups = (digits - 1) / 9;

			printf("3.");
			chunk_base = 1; //omit the 3 from the calculation

			for (int c = 0; c <= chunks; c++){
				if (c == chunks && extra == 0) break; //no need for the extra chunk at the end

				//initialize worker data
				for (int i = 0; i < num_workers; i++){
					info[i].first_digit = chunk_base + i * 9;

					if (c < chunks){
						info[i].blocks = BLOCKS_PER_WORKER_PER_CHUNK;
					}

					else{
						info[i].first_digit = chunk_base + i * 9;
						int remaining = digits - info[i].first_digit;

						info[i].blocks = remaining / (9 * num_workers);
						if (remaining % (9 * num_workers) > 0) info[i].blocks++;
					}

					pthread_create(&handles[i], NULL, worker, &info[i]);
				}
				
				for (int i = 0; i < num_workers; i++){
					pthread_join(handles[i], NULL);
				}

				if (c == chunks){
					//extra chunk - cap it
					chunk[digits - chunk_base + 1] = '\0';
				}
				printf("%s", chunk);
				chunk_base += chunk_size;
			}

			printf("\n");

			free(info);
			free(handles);
		}

		else{
			printf("digits and threads must be greater than 0\n");
		}
	}

	return 0;
}