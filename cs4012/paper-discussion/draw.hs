module LDraw where
import qualified Graphics.HGL as G

type Point = (Float, Float)
type DrawState = (Point, Float)
type DrawStateStack = [DrawState]
type Line = (Point, Point)

forward :: DrawState -> Float -> DrawState
forward ((x, y), angle) dist = ((x + dist * cos angle, y + dist * sin angle), angle)

--consume each symbol one by one, ignore pure symbols
stackDraw :: String -> Float -> DrawStateStack -> [Line]
stackDraw _ _ [] = error "empty draw state stack"
stackDraw [] _ _ = []
stackDraw ('+' : string) size (((x, y), angle) : stack) = stackDraw string size (((x, y), angle + (pi / 5)) : stack)
stackDraw ('-' : string) size (((x, y), angle) : stack) = stackDraw string size (((x, y), angle - (pi / 5)) : stack)
stackDraw ('F' : string) size ((pos, angle) : stack) = line : stackDraw string size ((newPos, newAngle) : stack)
    where (newPos, newAngle) = forward (pos, angle) size
          line = (pos, newPos)
stackDraw ('[' : string) size (state : stack) = stackDraw string size (state : state : stack)
stackDraw (']' : string) size (state : stack) = stackDraw string size stack
stackDraw (x : string) size state = stackDraw string size state

castPoint :: Point -> G.Point
castPoint (x, y) = (truncate (x + 0.5), truncate (y + 0.5))

drawLines :: [Line] -> G.Window -> IO ()
drawLines lines w = 
    case lines of
        ((p0, p1) : tail) -> do
            G.drawInWindow w $ G.line (castPoint p0) (castPoint p1) 
            drawLines tail w
            
        [] -> return ()