{-# LANGUAGE StandaloneDeriving #-}

module Main where

import Test.HUnit
import Test.Framework as TF (defaultMain, testGroup, Test)
import Test.Framework.Providers.HUnit

import HaskellGame.Datatypes
import HaskellGame.Utils
import HaskellGame.Interaction

deriving instance Show Scene
deriving instance Show Map
deriving instance Show Monster
deriving instance Show Object

deriving instance Eq Scene
deriving instance Eq Map
deriving instance Eq Monster
deriving instance Eq Object
deriving instance Eq Tile

test_no_collide_walls =
  let theMap = "###" ++
               "#.#" ++
               "###"
      theScene = (Scene (createMap 3 3 theMap) (Player (1,1)) [] [])
  in
    (handleInput 'j' theScene) @=? theScene


test_no_collide_objects =
  let theMap = "###" ++
               "#.#" ++
               "#.#" ++
               "###"
      theScene = (Scene (createMap 3 4 theMap) (Player (1,1)) [Chest (1,2)] [])
  in
    (handleInput 'k' theScene) @=? theScene

main = defaultMain tests

tests :: [TF.Test]
tests = [
          testGroup "Test Collisions" [
            testCase "Wall Collision [10 marks]" test_no_collide_walls,
            testCase "Object Collision [10 marks]" test_no_collide_objects
          ]
        ]
