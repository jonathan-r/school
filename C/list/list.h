#ifndef LIST_H
#define LIST_H

struct listnode{
	struct listnode* next;
	int data;
};

struct listset{
	struct listnode* start;
	struct listnode* end;
};

/* create a new, empty linked list set */
struct listset* listset_new();

/* safely destroy any kind of set. also frees the listset memory */
void listset_destroy(struct listset* this);

/* check to see if an item is in the set returns 1 if in the set, 0 if not */
int listset_lookup(struct listset* this, int item);

/* add an item, with number 'item' to the set has no effect if the item is already in the set */
void listset_add(struct listset* this, int item);

/* remove an item with number 'item' from the set */
void listset_remove(struct listset* this, int item);

/* place the union of src1 and src2 into dest */
void listset_union(struct listset* dest, struct listset* src1, struct listset* src2);

/* place the intersection of src1 and src2 into dest */
void listset_intersect(struct listset* dest, struct listset* src1, struct listset* src2);

#endif
