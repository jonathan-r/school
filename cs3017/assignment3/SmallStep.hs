module SmallStep where

import Syntax
import State


----------------------------------------------------------
----------------------------------------------------------
--  Small-Step Interpreter for Arithmetic Expressions  ---
----------------------------------------------------------
----------------------------------------------------------

type Configuration = (Exp, State) 

showConfig :: Configuration -> String
showConfig (c, s) = "<" ++ show c ++ ", " ++ showState s ++ ">"

smallStepExp :: Configuration -> Maybe Configuration
smallStepExp (CNum _, _)                  = Nothing     -- terminates reductions

smallStepExp (Var l, s)                   = case stateLookup s l of
                                               Just n  -> return (CNum n, s)
                                               Nothing -> return (Throw 2, s) --use exception code 2 for undefined identifier
                                               
smallStepExp (Throw _, _)                 = Nothing    -- terminates reductions

-- Propagate exceptions for any kind of operation
smallStepExp (Add (Throw t) _, s)           = do return (Throw t, s)
smallStepExp (Sub (Throw t) _, s)           = do return (Throw t, s)
smallStepExp (Div (Throw t) _, s)           = do return (Throw t, s)
smallStepExp (Add _ (Throw t), s)           = do return (Throw t, s)
smallStepExp (Sub _ (Throw t), s)           = do return (Throw t, s)
smallStepExp (Div _ (Throw t), s)           = do return (Throw t, s)


-- Addition
smallStepExp (Add (CNum n1) (CNum n2), s) = do let n3 = n1 + n2 
                                               return (CNum n3, s)

smallStepExp (Add (CNum n1) e2, s)         = do (newE2, s') <- smallStepExp (e2, s)
                                                return (Add (CNum n1) newE2, s')

smallStepExp (Add e1 e2, s)                = do (newE1, s') <- smallStepExp (e1, s)
                                                return (Add newE1 e2, s')

-- Subtraction
smallStepExp (Sub (CNum n1) (CNum n2), s)  = do let n3 = n1 - n2
                                                if n3 < 0 then return (Throw 0, s)
                                                else return (CNum n3, s)

smallStepExp (Sub (CNum n1) e2, s)         = do (newE2, s') <- smallStepExp (e2, s)
                                                return (Sub (CNum n1) newE2, s')

smallStepExp (Sub e1 e2, s)                = do (newE1, s') <- smallStepExp (e1, s)
                                                return (Sub newE1 e2, s')

-- Division
smallStepExp (Div (CNum n1) (CNum n2), s)  = do if n2 == 0 then 
	                                                return (Throw 1, s)
                                                else
                                                    if n1 /= ((n1 `div` n2) * n2) then 
                                                    	return (Throw 1, s)
                                                    else 
                                                    	return (CNum (n1 `div` n2), s)

smallStepExp (Div (CNum n1) e2, s)         = do (newE2, s') <- smallStepExp (e2, s)
                                                return (Div (CNum n1) newE2, s')

smallStepExp (Div e1 e2, s)                = do (newE1, s') <- smallStepExp (e1, s)
                                                return (Div newE1 e2, s')

-- Catch exception and excute handler with updated store
smallStepExp (TryCatch (Throw t) l e2, s)  = do let s' = stateUpdate s l t
                                                return (e2, s')

-- No exception occured, so return the result
smallStepExp (TryCatch (CNum n) l e2, s)   = return (CNum n, s)

-- Advance
smallStepExp (TryCatch e1 l e2, s)         = do (newE1, s') <- smallStepExp(e1, s)
                                                return (TryCatch newE1 l e2, s')

