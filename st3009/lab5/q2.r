data <- read.csv(file = "lab4times.csv", header = F)[,];
N <- length(data)

meanboot <- function(x, indices) {
  mean(x[indices]);
}

means <- {};
for (i in 1:10000) {
  means[i] <- mean(sample(x = data, replace = T, size = N));
}

mean(means) - sd(means)
mean(means) + sd(means)
