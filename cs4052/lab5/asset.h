#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <SDL2/SDL_image.h>

#include "vbo.h"
#include "texture.h"

#define ASSET_PATH "assets/"
Mesh* loadMesh(const char* name, vertex** keepVerts = nullptr, uint32_t** keepIndices = nullptr);
Texture* loadTexture(const char* name, SDL_Surface** keepSurface = nullptr);