#version 120

uniform sampler2D texture;

//from vertex shader
varying vec2 tex;
varying vec3 col;

void main() {
	vec3 sample = texture2D(texture, tex).xyz;
	vec3 final = /*sample * */col;
	gl_FragColor = vec4(final, 1.0);
}
