#version 120

uniform sampler2D texture;

//camera
uniform vec3 eyeWorld; //world

//point light
uniform vec3 lightDir;
uniform vec3 lightDiffuse;
uniform vec3 lightSpecular;
uniform vec3 lightAmbient;

//material
uniform float lightFraction;
uniform vec3 matDiffuse;
uniform vec3 matSpecular;
uniform vec3 matAmbient;
uniform float matShine;

//from vertex shader
varying vec3 worldVec; //world
varying vec3 interpNormal; //world
varying vec2 tex;

void main() {
	float ndcDepth = (2.0 * gl_FragCoord.z - gl_DepthRange.near - gl_DepthRange.far) / (gl_DepthRange.far - gl_DepthRange.near);
	float cd = ndcDepth / gl_FragCoord.w;

	//sum of irradience
	vec3 Ir = lightAmbient * matAmbient;
	vec3 normal = interpNormal; //normalize(interpNormal);

	float ndl = dot(normal, lightDir);

	if (ndl > 0.0) {
		vec3 Id = lightDiffuse * matDiffuse * max(ndl, 0.0);
		Ir += Id;

		vec3 eyeDirNeg = normalize(worldVec - eyeWorld);
		vec3 lightOutDir = reflect(lightDir, normal);

		float specDot = dot(lightOutDir, eyeDirNeg);
		if (specDot > 0.0) {
			vec3 Is = lightSpecular * matSpecular * pow(specDot, matShine);
			Ir += Is;
		}
	}

	vec4 sample = texture2D(texture, tex);
	vec3 final = mix(vec3(1.0), Ir, lightFraction) * sample.xyz;

	//fog
	float fog = 1.0 / exp(cd * 0.001);
	vec3 fogged = mix(lightAmbient, final, fog);

	gl_FragColor = vec4(fogged, 1.0);
}
