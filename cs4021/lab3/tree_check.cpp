#include <vector>

#include "trees.h"

struct operation {
	uint64_t val;
	bool remove; //instead of add
	int result; //add: {0:inserted, 1:existed}, remove: {0:removed, 1:didn't exist}
	operation(uint64_t v, bool r, int res) : val(v), remove(r), result(res) {}
};
//std::mutex debugLock; //all operations forced into sequence
std::unordered_set mirrorSet; //

void checkedInsert()