#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "hash.h"

int main(int argc, char** argv){
	if (argc > 1){
		int size = atoi(argv[1]);

		if (size > 0){
			struct hashtable_table* table = hashtable_new(size);
			for (int i = 2; i < argc; i++){
				hashtable_insert(table, argv[i]);
			}
		
			//hashtable_print(table);
			hashtable_print_stats(table);
			hashtable_free(table);
		}

		else {
			printf("size must be greater than 0\n");
		}
	}
	
	else{
		printf("usage: %s [size] <strings to insert>\n", argv[0]);
	}
	
	return 0;
}
