#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <math.h>

#include "hash.h"

// construct a new hash table with size elements
struct hashtable_table* hashtable_new(int size){
	struct hashtable_table* table = malloc(sizeof(struct hashtable_table));
	table->size = size;
	table->length = malloc(sizeof(int) * size);
	table->table = malloc(sizeof(struct hashtable_node*) * size);
	memset(table->length, 0, sizeof(int) * size);
	memset(table->table, 0, sizeof(struct hashtable_node*) * size);
	return table;
}

// free the memory for all parts of the hashtable
void hashtable_free(struct hashtable_table* this){
	for (int i = 0; i < this->size; i++){
		for (struct hashtable_node* n = this->table[i]; n;){
			struct hashtable_node* to_free = n;
			n = n->next;
			free(to_free);
		}
	}
	
	free(this->table);
	free(this->length);
	free(this);
}

//computes sum of (data[i] * 127^(length - i)) * 127
static unsigned string_hash(char* str){
	const unsigned multiplier = 127;
	unsigned sum = 0;

	//horner's method ftw
	char* c = str;
	while(*c) sum = sum * multiplier + *c++;
	sum *= multiplier;

	return sum;
}

// insert string into the hash table, no effect if it's already there
void hashtable_insert(struct hashtable_table* this, char * item){
	unsigned hash = string_hash(item);
	int index = hash % this->size;

	if (this->table[index]){
		for (struct hashtable_node* n = this->table[index]; n; n = n->next){
			if (n->data == item){
				//it's already there
				return;
			}
		}

		//it's not there, so add it
	}

	struct hashtable_node* n = malloc(sizeof(struct hashtable_node));
	n->data = item;
	n->next = this->table[index];

	this->table[index] = n;
	this->length[index]++; //the lengths has increased by 1
}

// remove string from the hash table, no effect if not in table
void hashtable_remove(struct hashtable_table* this, char * item){
	unsigned hash = string_hash(item);
	int index = hash % this->size;

	struct hashtable_node* last = this->table[index];

	if (last){
		if (last->data == item){
			this->table[index] = last->next; 
			free(last);
			this->length[index]--;
		}

		else{
			for (struct hashtable_node* n = last->next; n; n = n->next){
				if (n->data == item){
					//removet the node
					last->next = n->next;
					free(n);
					this->length[index]--;
					return;
				}

				last = n;
			}
		}
	}
}

// return 1 if string is already in table, 0 otherwise
int hashtable_lookup(struct hashtable_table* this, char * item){
	unsigned hash = string_hash(item);
	int index = hash % this->size;

	if (this->table[index]){
		for (struct hashtable_node* n = this->table[index]; n; n = n->next){
			if (n->data == item){
				//it's there
				return 1;
			}
		}
	}

	return 0;
}

// print out each entry in the hash table and the values
// stored at that entry
void hashtable_print(struct hashtable_table* this){
	for (int i = 0; i < this->size; i++){
		struct hashtable_node* n = this->table[i];
		if(n){
			printf("list %d:\n", i);
			for (; n; n = n->next){
				printf("\t%s\n", n->data);
			}
		}
	}
}

void hashtable_print_stats(struct hashtable_table* this){
	const int bar_len = 80;
	const int max_lines = 90;

	//find a the maximum number of buckets
	int buckets = 0;
	for (int i = 0; i < this->size; i++){
		if (this->length[i] > buckets) buckets = this->length[i];
	}
	buckets++; //so that the first one will be for empty entries

	printf("%d buckets\n", buckets);

	int per_bucket = 1;
	int extra = 0;

	if (buckets > max_lines){
		per_bucket = this->size / max_lines;
		extra = this->size % max_lines;
		buckets = max_lines;
	}
	
	int max_numbers = (int)ceil(log10(buckets)); //longest number

	//record the distribution of the length of the table entries
	int* distrib = malloc(buckets * sizeof(int));
	memset(distrib, 0, buckets * sizeof(int));

	for (int i = 0; i < this->size; i++){
		int id = this->length[i] / per_bucket;
		if (id == buckets) id--; //put the extra lengths at the end, since they should be few
		distrib[id]++; //mark it in the distribution array
	}

	//find the least and most represented lenghts
	int least = INT_MAX, least_id = distrib[0];
	int most = 0, most_id = distrib[0];

	for (int i = 0; i < buckets; i++){
		if (distrib[i] < least){
			least = distrib[i];
			least_id = i;
		}

		if (distrib[i] > most){
			most = distrib[i];
			most_id = i;
		}
	}
	
	char num_buf[64];

	if (most > 0){
		//there's at least one data item stored in the hashtable
		printf("most common length: %d (%d instances)\n", most_id, most);
		printf("least common length: %d (%d instances)\n", least_id, least);

		for (int i = 0; i < buckets; i++){
			int start = i * per_bucket;
			
			int len = snprintf(num_buf, 64, "%d", start);
			for (int s = 0; s < max_numbers - len; s++) putchar(' ');
			printf("%s", num_buf);
			
			if (per_bucket > 1){
				printf("%s", " to ");
				
				int end = (i + 1) * per_bucket - 1;
				if (i == buckets - 1) end += extra;
				len = snprintf(num_buf, 64, "%d", start);
				for (int s = 0; s < max_numbers - len; s++) putchar(' ');
				printf("%s", num_buf);
			}
				
			putchar('|');
			int filled = (distrib[i] * bar_len) / most;
			for (int b = 0; b < filled; b++) putchar('=');
			for (int b = 0; b < bar_len - filled; b++) putchar(' ');
			printf("|\n");
		}
	}
	else{
		printf("table is empty\n");
	}

	free(distrib);
}

