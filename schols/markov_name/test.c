#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "markov_name.h"

int main(int argc, char** argv){
	srand(time(NULL));
	
	if (argc == 2){
		char* filename = argv[1];
		
		markov_state state;
		markov_init(&state);
		markov_train_file(&state, filename);
		
		//now generate some words
		for (int i = 0; i < 30; i++){
			char* word = markov_name(&state, 3 + rand() % 2, 6 + rand() % 10);
			printf("%s, ", word);
			free(word);
		}
		printf("\n");
	}
	
	else printf("usage: %s <sample text file>\n", argv[0]);
	
	return 0;
}

