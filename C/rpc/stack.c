#include <stdlib.h>

#include "stack.h"


//create a new, empty stack
stack* stack_create(){
	stack* st = malloc(sizeof(stack));
	st->end = NULL;
	return st;
}

//safely delete the stack
void stack_delete(stack* st){
	stack_node* node = st->end;
	while (node){
		stack_node* prev = node->prev;
		free(node);
		node = prev;
	}

	free(st);
}

//push val onto the stack, increasing the storage if needed
void stack_push(stack* st, double val){
	stack_node* node = st->end;
	if (node){
		if (node->np < STACK_NODE_SIZE){
			node->data[node->np++] = val;
			return;
		}
	}

	else{
		//there's no space in the current node, or the current node is NULL
		stack_node* next = malloc(sizeof(stack_node));
		next->prev = node;
		st->end = next;
		next->np = 0;
		next->data[(next->np)++] = val;
	}
}

//returns 1 if there is an element and stores it in out if out is not NULL.
//returns 0 if there is no element in the stack.
int stack_pop(stack* st, double* out){
	stack_node* node = st->end;
	if (node){
		if (out) *out = node->data[--(node->np)];
		if (node->np == 0){
			//remove the node off the end of the array
			st->end = node->prev;
			free(node);
		}

		return 1;
	}

	else return 0;
}

//return 1 if there is one element, 2 if there are more, 0 if none
int stack_contains(stack* st){
	if (st->end){
		if (st->end->np > 1 || st->end->prev) return 2;
		else return 1;
	}
	return 0;
}

