#include <iostream>                             // cout
#include "helper.h"                             //
#include "locks.h"

using namespace std;

#define K           1024                        //
#define GB          (K*K*K)                     //
#define NOPS        20                          //
#define NSECONDS    1                           // run each test for NSECONDS

#define COUNTER64                               // comment for 32 bit counter
#define USEPMS                                  // use PMS counters

#define ALIGNED_MALLOC(sz, align) _aligned_malloc(sz, align)

UINT64 tstart;                                  // start of test in ms
int lineSz;                                     // cache line size
int maxThread;                                  // max # of threads

THREADH *threadH;                               // thread handles
UINT64 *ops;                                    // for ops per thread

volatile UINT64 counter;                        // value to update

// locks and worker
#define NUM_LOCK_TYPES 5
BakeryLock* bakeryLock;
TestAndSetLock* testAndSetLock;
TestAndTestAndSetLock* testAndTestAndSetLock;
MCSLock* mcsLock;

#define WORKER_TEMPLATE(name, core) \
WORKER worker_##name(void *vthread){ \
	int thread = (int)((size_t) vthread); \
	UINT64 n = 0; \
	runThreadOnCPU(thread % ncpu); \
	while (1) { \
		for (int i = 0; i < NOPS; i++) { \
			core \
		} \
		n += NOPS; \
		if ((getWallClockMS() - tstart) > NSECONDS*1000) \
			break; \
	} \
	ops[thread] = n; \
	return 0; \
} \

#define ATOMIC_CORE InterlockedIncrement64((volatile LONG64*) &counter);
#define LOCK_CORE(lockvar) lockvar->acquire(thread); counter++; lockvar->release(thread);

WORKER_TEMPLATE(atomic, ATOMIC_CORE)
WORKER_TEMPLATE(bakery, LOCK_CORE(bakeryLock))
WORKER_TEMPLATE(testset, LOCK_CORE(testAndSetLock))
WORKER_TEMPLATE(testtestset, LOCK_CORE(testAndTestAndSetLock))
WORKER_TEMPLATE(mcs, LOCK_CORE(mcsLock))

struct lockInfo {
	const char* name;
	WORKER (*entry)(void*);
};

int main(int argc, char** argv) {
	lockInfo locks[NUM_LOCK_TYPES] = {
		"atomic increment",
		&worker_atomic,

		"bakery lock",
		&worker_bakery,

		"test and set lock",
		&worker_testset,

		"test and test and set lock",
		&worker_testtestset,

		"MCS lock",
		&worker_mcs
	};

	ncpu = getNumberOfCPUs();   // number of logical CPUs
	maxThread = 2 * ncpu;       // max number of threads

	// get cache info
	lineSz = getCacheLineSz();

	// allocate global variable
	threadH = (THREADH*) ALIGNED_MALLOC(maxThread*sizeof(THREADH), lineSz);             // thread handles
	ops = (UINT64*) ALIGNED_MALLOC(maxThread*sizeof(UINT64), lineSz);                   // for ops per thread

	// run tests
	UINT64 ops1 = 1;

	testAndSetLock = new TestAndSetLock();
	testAndTestAndSetLock = new TestAndTestAndSetLock();

	cout << "{\n\t\"data\": [" << endl; //start of JSON

	for (int nt = 1; nt <= maxThread; nt++) {
		bakeryLock = new BakeryLock(nt, lineSz);
		mcsLock = new MCSLock(nt, lineSz);

		for (int lt = 0; lt < NUM_LOCK_TYPES; lt++) {
			lockInfo& li = locks[lt];

			//  zero counter and ops counters
			counter = 0;
			memset(ops, 0, maxThread * sizeof(UINT64));

			// get start time
			tstart = getWallClockMS();

			// create worker threads
			for (int thread = 0; thread < nt; thread++)
				createThread(&threadH[thread], li.entry, (void*)(size_t)thread);

			// wait for ALL worker threads to finish
			waitForThreadsToFinish(nt, threadH);
			UINT64 rt = getWallClockMS() - tstart;

			//count total operations
			int increments = 0;
			for (int thread = 0; thread < nt; thread++) {
				increments += ops[thread];
			}

			if (counter != increments) {
				double effective = 100.0 * counter / increments;
				cerr << nt << "threads, " << li.name << ": only " << effective << "% effective" << endl;
			}

			cout << "\t\t{ \"threads\": " << nt << ", \"method\": \"" << li.name << "\", " << "\"increments\": " << increments << " }," << endl;


			// delete thread handles
			for (int thread = 0; thread < nt; thread++) {
				closeThread(threadH[thread]);
			}
		}

		delete bakeryLock;
		delete mcsLock;
	}

	delete testAndSetLock;
	delete testAndTestAndSetLock;

	cout << "\t\t{}\n\t]\n}" << endl; //dummy trailing object and end of JSON

	quit();
	return 0;
}
