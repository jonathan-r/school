{-# LANGUAGE DoAndIfThenElse #-}
{- |
  This module contains the implementation of the virtual machine which
  executes the instruction set from "TastierMachine.Instructions".

  The virtual processor has 4096 words of random-access data memory,
  and 4096 words of stack memory.

  The size of a machine word is 16 bits, and the storage format is
  big-endian (the first byte in the word is the most significant),
  or in other words, the two bytes in the word are 16 continuous
  bits in memory, going from most significant (bit 15) down to least
  significant (bit 0).

  The machine has three state registers which can be loaded onto the
  stack for manipulation, and stored back again.
  Our calling convention for procedures is as follows:

  stack frame layout and pointer locations:                 DMA
                                                            DMA
        *                         *                         DMA
  top ->*                         *                         DMA
        * local variables         *                         DMA
        ***************************                         DMA
        * dynamic link (dl)       *                         DMA
        * static link (sl)        *                         DMA
        * lexic level delta (lld) *                         DMA
  bp -> * return address          *                         DMA
        ***************************                         DMA
                                                            DMA
  dl  - rbp of calling procedure's frame for popping stack  DMA
  sl  - rbp of enclosing procedure for addressing variables DMA
  lld - ll difference (delta) between a called procedure    DMA
        and its calling procudure                           DMA

-}
module TastierMachine.Machine where
import qualified TastierMachine.Instructions as Instructions
import qualified Data.ByteString.Lazy.Char8 as B
import Data.Int (Int8, Int16)
import Data.Char (intToDigit, chr, ord)
import Numeric (showIntAtBase)
import Data.Bits (complement)
import Data.Array ((//), (!), Array, elems, accum, bounds)
import Control.Monad.RWS.Lazy (RWS, put, get, ask, tell, local)
import System.IO.Unsafe (unsafePerformIO)
import System.IO (hFlush, stdout)
import Data.List (intersperse)
import Debug.Trace
import Data.Maybe (fromJust)

debug' m@(Machine rpc rtp rbp imem _ _ _) = do {
  putStrLn $
    concat $
      intersperse "\t| " $
        (zipWith (++)
          ["rpc: ", "rtp: ", "rbp: "]
          [show rpc, show rtp, show rbp])
        ++
        [(show $ imem ! rpc)];
  hFlush stdout;
  return m
}

debug = unsafePerformIO . debug'

data HeapBlock = HeapBlock {
                         start :: Int16,   --address of the first block
                         end :: Int16,     --address after the last word
                         references :: Int --number of references to the block
                       }
                       deriving (Show)

data Machine = Machine { rpc :: Int16,  -- ^ next instruction to execute
                         rtp :: Int16,  -- ^ top of the stack
                         rbp :: Int16,  -- ^ base of the stack

                         imem :: (Array Int16 Instructions.InstructionWord),
                                                      -- ^ instruction memory
                         dmem :: (Array Int16 Int16), -- ^ data memory
                         smem :: (Array Int16 Int16),  -- ^ stack memory

                         blocks :: [HeapBlock] --allocated and free blocks, using actual addresses
                       }
                       deriving (Show)

{-
  This function implements the internal state machine executing the
  instructions.
-}

run :: RWS [String] [String] Machine ()
run = do
  machine'@(Machine rpc rtp rbp imem dmem smem blocks) <- get
  let machine = debug machine'
  let instructionWord = imem ! rpc

  case instructionWord of
    Instructions.Nullary i ->
      case i of
        Instructions.Halt -> do
          return ()

        Instructions.Dup -> do
          put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                          smem = (smem // [(rtp, smem ! (rtp-1))]) }
          run

        Instructions.Nop -> do
          put $ machine { rpc = rpc + 1 }
          run

        Instructions.Add -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = b + a
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.Sub    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = b - a
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.Mul    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = b * a
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.Div    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = b `div` a
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.Equ    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = fromIntegral $ fromEnum (b == a)
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.NotEq    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = fromIntegral $ fromEnum (b /= a)
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.Lss    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = fromIntegral $ fromEnum (b < a)
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.LssEq  -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = fromIntegral $ fromEnum (b <= a)
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.Gtr    -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = fromIntegral $ fromEnum (b > a)
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run

        Instructions.GtrEq  -> do
          let a = smem ! (rtp-1)
          let b = smem ! (rtp-2)
          let result = fromIntegral $ fromEnum (b >= a)
          put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(rtp-2, result)]) }
          run
        
        Instructions.Neg    -> do
          let a = smem ! (rtp-1)
          let result = complement a
          put $ machine { rpc = rpc + 1, smem = (smem // [(rtp-1, result)]) }
          run

        Instructions.Pop    -> do
          {-
            Pop the top of the stack into the bit bucket
          -}
          put $ trace ("Popping " ++ (show $ smem ! (rtp - 1))) $ 
            machine { rpc = rpc + 1, rtp = rtp - 1 }
          run

        Instructions.Ret    -> do
          {-
            The return address is on top of stack, set the pc to that address
          -}
          put $ machine { rpc = (smem ! (rtp-1)), rtp = rtp - 1 }
          run

        Instructions.StoS   -> do
          let ref = (smem ! (rtp - 2)) - 3 --adjust pointer
          let value = smem ! (rtp - 1)
          put $ machine { rpc = rpc + 1, rtp = rtp - 2,
                          dmem = (dmem // [(ref, value)]) }
          run

        Instructions.LoadS   -> do
          let ref = (smem ! (rtp - 1)) - 3 --adjust pointer
          let value = dmem ! ref
          put $ machine { rpc = rpc + 1, smem = (smem // [(rtp - 1, value)]) }
          run

        Instructions.UnRef   -> do --pops dmem reference
          let base = (smem ! (rtp - 1)) - 3 --adjust pointer
          case base of
            0 -> do
              -- do nothing
              put $ machine { rpc = rpc + 1, rtp = rtp - 1 }

            _ -> do
              let newBlocks = changeRef [] blocks base (-1)
              put $ trace ("UnRef on block at " ++ (show base)) $ 
                machine { rpc = rpc + 1, rtp = rtp - 1, blocks = newBlocks }
          run

        Instructions.Ref     -> do --pops dmem reference
          let base = (smem ! (rtp - 1)) - 3 --adjust pointer
          case base of
            0 -> do
              -- do nothing
              put $ machine { rpc = rpc + 1, rtp = rtp - 1 }

            _ -> do
              let newBlocks = changeRef [] blocks base 1
              put $ trace ("Ref on block at " ++ (show base)) $ 
                machine { rpc = rpc + 1, rtp = rtp - 1, blocks = newBlocks }
          run

        Instructions.AllocS  -> do --pops size off the stack, pushes new reference
          let size = smem ! (rtp - 1)
          let (blockStart, newBlocks) = allocate [] blocks size
          put $ trace ("Allocated " ++ (show size) ++ " words at " ++ (show $ blockStart + 3)) $
            machine { rpc = rpc + 1, rtp = rtp + 1, 
              smem = smem // [(rtp, blockStart + 3)],
              dmem = dmem // (zip [blockStart .. blockStart + size - 1] (repeat 0)) }--zero-fill the memory
          run

        Instructions.NullCheck -> do --peeks at the top of the stack for reference
          let ref = smem ! (rtp - 1)
          case ref of
            0 -> error $ "Access to null rray reference"
            _ -> do
              put $ machine { rpc = rpc + 1 }
          run

        Instructions.Collect -> do --force garbage collection round
          put $ machine { rpc = rpc + 1, blocks = collectGarbage [] blocks }
          run

        Instructions.InputLeft -> do
          value <- ask
          let left = length value
          put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                          smem = smem // [(rtp, fromIntegral left)] }
          run

        Instructions.ReadInt   -> do
          value <- ask
          case value of
            (i:rest) -> do 
              let val = (fromIntegral $ read $ i)
              put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                              smem = (smem // [(rtp, val)]) }
              local tail run
            [] -> error $ "ReadInt instruction issued but no data left to read"

        Instructions.WriteInt  -> do
          tell $ [show $ smem ! (rtp-1)]
          put $ machine { rpc = rpc + 1, rtp = rtp - 1 }
          run

        --reads string, puts content on stack, then puts string length
        Instructions.ReadStr   -> do
          value <- ask
          case value of
            (str : rest) -> do 
              let len = (fromIntegral $ length str) :: Int16
              let contents = (map fromIntegral $ map Data.Char.ord str) :: [Int16]

              put $ machine { rpc = rpc + 1, rtp =  rtp + len,
                              smem = smem // (zip [rtp .. rtp + len - 1] (contents ++ [fromIntegral len])) }
              local tail run
            [] -> error $ "ReadStr instruction issued but no data left to read"

        Instructions.WriteStr  -> do  --print the string starting at (ptr+1), of length *(int)ptr
          let strLenAddr = (smem ! (rtp-1)) - 3  --adjust pointer
          let strStartAddr = fromIntegral $ strLenAddr + 1
          let strLength = fromIntegral $ dmem ! strLenAddr

          tell $ [map (Data.Char.chr . fromIntegral) $ take strLength $ drop strStartAddr $ Data.Array.elems dmem]
          put $ machine { rpc = rpc + 1, rtp = rtp - 1 }
          run

        Instructions.LoadBufG  -> do
          let src = (smem ! (rtp - 2)) - 3 --adjust pointer
          let length = smem ! (rtp - 1)
          let destRange = [rtp - 2 .. rtp - 2 + length - 1]

          put $ machine { rpc = rpc + 1, rtp = rtp - length - 2,
                          smem = smem // (zip destRange (take (fromIntegral length) $ drop (fromIntegral src) $ Data.Array.elems dmem)) }
          run

        Instructions.StoBufG  -> do
          let length = smem ! (rtp - 2)
          let dest = (smem ! (rtp - 1)) - 3 --adjust pointer
          let destRange = [dest .. dest + length - 1]

          put $ machine { rpc = rpc + 1, rtp = rtp - length - 2,
                          dmem = dmem // (zip destRange (take (fromIntegral length) $ drop (fromIntegral $ rtp - length) $ Data.Array.elems smem)) }
          run

        Instructions.Leave  -> do
          {-
            When we're leaving a procedure, we have to reset rbp to the
            value it had in the calling context. Our calling convention is
            that we store the return address at the bottom of the stack
            frame (at rbp), and the old base pointer in the dynamic link
            field (at rbp+3).

            We reset rbp to whatever the value of the dynamic link field is.
            Since we created the stack frame for this procedure, the one
            from which we're now returning, at the *top* of the stack when
            it was called, we know that when we called the procedure, rtp
            was equal to whatever value rbp has *at present*. However, we
            want to leave the return address on the top of the stack for RET
            to jump to. We can simply set rtp to one past the current rbp
            (effectively popping off the local variables of this procedure,
            the dynamic link, static link, and lexical level delta fields).
          -}
          put $ machine { rpc = rpc + 1, rtp = rbp+1, rbp = (smem ! (rbp+3)) }
          run

    Instructions.Unary i a ->
      case i of
        Instructions.HeapStart -> do
          let heapStart = a - 3 --actual address
          put $ machine { rpc = rpc + 1, blocks = [(HeapBlock heapStart (snd $ bounds $ dmem) 0)] }
          run

        Instructions.StoG   -> do
          -- memory mapped control and status registers implemented here
          case a of
            0 -> put $ machine { rpc = (smem ! (rtp-1)), rtp = rtp - 1 }
            1 -> put $ machine { rpc = rpc + 1, rtp = (smem ! (rtp-1)) }
            2 -> put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                                 rbp = (smem ! (rtp-1)) }
            _ -> put $ machine { rpc = rpc + 1, rtp = rtp - 1,
                                 dmem = (dmem // [(a-3, (smem ! (rtp-1)))]) }
          run

        Instructions.LoadG  -> do
          -- memory mapped control and status registers implemented here
          case a of
            0 -> put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                                 smem = (smem // [(rtp, rpc)]) }
            1 -> put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                                 smem = (smem // [(rtp, rtp)]) }
            2 -> put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                                 smem = (smem // [(rtp, rbp)]) }
            _ -> put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                                 smem = (smem // [(rtp, (dmem ! (a-3)))]) }
          run

        Instructions.Const  -> do
          put $ machine { rpc = rpc + 1, rtp = rtp + 1,
                          smem = (smem // [(rtp, a)]) }
          run

        Instructions.Enter  -> do
          {-
            ENTER has to set up both the static and dynamic link fields of
            the stack frame which is being entered. The dynamic link (the
            base pointer of the stack frame from which this procedure was
            called) needs to go on the top of stack, as specified by our
            calling convention.

            The static link (the base pointer of the stack frame belonging
            to the *enclosing* procedure (the one where this procedure was
            defined, and of which we should have access to the local variables)
            should be placed at rbp+2. If we are calling a procedure
            which is defined in the same scope, then the static link is just
            a copy of whatever the current static link is. However, if that
            isn't the case, then we need to call followChain to find out
            what the base pointer was in the stack frame where the procedure
            we're entering was defined.

            Also zero-fill the local variables.
          -}
          let lexicalLevelDelta = (smem ! (rtp-1))
          let staticLink = (followChain 0 lexicalLevelDelta rbp smem)
          put $ machine { rpc = rpc + 1, rtp = rtp+a+2, rbp = rtp-2,
                          smem = (smem // ([(rtp, staticLink), (rtp+1, rbp)] ++ 
                            (zip [rtp+2 .. rtp+2+a-1] (repeat 0)) )) }
          run

        Instructions.Jmp  -> do
          put $ machine { rpc = a }
          run

        Instructions.FJmp -> do
          let jump = smem ! (rtp-1)
          if jump == 0 then do
            put $ machine { rtp = rtp - 1, rpc = a }
            run
          else do
            put $ machine { rtp = rtp - 1, rpc = rpc + 1 }
            run

    Instructions.Binary i a b ->
      case i of
        Instructions.Load   -> do
          {-
            Load gets a variable from a calling frame onto the top of the
            stack. We follow the chain of links to find the stack frame the
            variable is in, add b (the address of the variable in that
            frame) and add two, because each frame has the dynamic link and
            static link stored just before the start of the real stack
            variables, but it's the static link whose address we get from
            followChain.
          -}
          let loadAddr = (followChain 0 a rbp smem) + 4 + b
          put $ trace ("load " ++ (show loadAddr) ++ ", found " ++ (show $ smem ! loadAddr)) $
            machine { rpc = rpc + 1, rtp = rtp + 1,
                          smem = (smem // [(rtp, (smem ! loadAddr))]) }
          run

        Instructions.Sto    -> do --Store updates a variable in a calling frame
          let storeAddr = (followChain 0 a rbp smem) + 4 + b
          put $ trace ("putting " ++ (show $ smem ! (rtp-1)) ++ ", into " ++ (show $ storeAddr)) $ 
            machine { rpc = rpc + 1, rtp = rtp - 1,
                          smem = (smem // [(storeAddr, (smem ! (rtp-1)))]) }
          run

        Instructions.Call   -> do
          {-
            CALL gets passed the lexical level delta in slot a, and the
            address of the procedure in slot b. CALL pushes the return
            address onto the stack, then the lexical level delta, so when
            the called procedure does ENTER, the stack contains the lexical
            level delta at (rtp - 1) and the return address at (rtp - 2).
          -}
          put $ machine { rpc = b, rtp = rtp + 2,
                          smem = (smem // [(rtp, (rpc+1)), (rtp+1, a)]) }
          run

{-
  followChain follows the static link chain to find the absolute address in
  stack memory of the base of the stack frame (n-limit) levels down the call
  stack. Each time we unwind one call, we recurse with rbp set to the base
  pointer of the stack frame which we just unwound. When we've unwound the
  correct number of stack frames, as indicated by the argument *limit*, we
  return the base pointer of the stack frame we've unwound our way into.
-}

followChain :: Int16 -> Int16 -> Int16 -> (Array Int16 Int16) -> Int16
followChain limit n rbp smem =
  if n > limit then
    followChain limit (n-1) (smem ! (rbp+2)) smem
  else rbp

{-
  allocate walks through the block list to find an unused block
  that's large enough to fit the desired new block
  On success, it returns the start of the allocated block as well
  as the new block list
  On failure, it throws an error
-}
allocate :: [HeapBlock] -> [HeapBlock] -> Int16 -> (Int16, [HeapBlock])

allocate _ [] size =
  error $ "Cannot allocate block of size " ++ (show size)

allocate blocksBefore (block : blockTail) size =
  if (references block) == 0 && (end block) - (start block) >= size then
    if (end block) - (start block) - size > 0 then
      ((start block), blocksBefore ++ [(HeapBlock (start block) ((start block) + size) 0), (HeapBlock ((start block) + size) (end block) 0)] ++ blockTail)
    else
      ((start block), blocksBefore ++ [(HeapBlock (start block) (end block) 0)] ++ blockTail)
  else
    allocate (blocksBefore ++ [block]) blockTail size

{-
  changeRef looks for a reference in the blocks list and
  changes its reference count by offset.
  throws error if it can't find the block
-}
changeRef :: [HeapBlock] -> [HeapBlock] -> Int16 -> Int16 -> [HeapBlock]

changeRef _ [] addrStart _ =
  error $ "Cannot find block starting at " ++ (show addrStart)

changeRef blocksBefore (block : blockTail) addrStart offset =
  if (start block) == addrStart then
    blocksBefore ++ [block { references = (references block) + (fromIntegral offset) }] ++ blockTail
  else
    changeRef (blocksBefore ++ [block]) blockTail addrStart offset

{-
  collectGarbage looks for consecutive blocks that have no references to them
  and joins them
-}
collectGarbage :: [HeapBlock] -> [HeapBlock] -> [HeapBlock]
collectGarbage before [] = 
  before

collectGarbage before (first : (second : blockTail)) =
  if (references first) == 0 && (references second == 0) then
    collectGarbage (before ++ [(HeapBlock (start first) (end second) 0)]) blockTail
  else --move only one block to the before list
    collectGarbage (before ++ [first]) (second : blockTail)

collectGarbage before [one] =
  before ++ [one]