import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.util.Random;
import java.util.Arrays;

//-------------------------------------------------------------------------
/**
 *  Test class for Array Linear Search
 *
 *  @author  Vasileios Koutavas
 *  @version 2014.01.13
 */
@RunWith(JUnit4.class)
	public class ArraySearchTest
	{
		Random rand;
		//~ Constructor ........................................................
		@Test
		public void testConstructor()
		{
			new ArraySearch();
		}
	
		//~ Public Methods ........................................................
	
		// ----------------------------------------------------------
		/**
		 * Check that the search works for empty unsorted arrays
		 */
		@Test
		public void testUnsortedEmpty()
		{
			assertEquals( ArraySearch.searchInUnsorted(new int[0], 0), false );
		}
	
		/**
		 * Check that the search works for empty "sorted" arrays
		 */
		@Test
		public void testSortedEmpty()
		{
			assertEquals( ArraySearch.searchInSorted(new int[0], 0), false );
		}
		
		/**
		 * Check for no false positives in a single-element array
		 */
		@Test
		public void testUnsortedSingleFalse()
		{
			int[] anArray = { 10 };
			assertEquals( ArraySearch.searchInUnsorted(anArray, 0), false );
		}

		/**
		 * Check for no false positives in a single-element array,
		 * using the sorted searching method
		 */
		@Test
		public void testSsortedSingleFalse()
		{
			int[] anArray = { 10 };
			assertEquals( ArraySearch.searchInSorted(anArray, 0), false );
		}
		
		/**
		 * Check for no false positives in a large random array
		 */
		@Test
		public void testUnsortedManyFalse()
		{
			Random rand = new Random();
			int value = rand.nextInt();
			int[] array = randomArrayExclude(50000, value);

			assertEquals( ArraySearch.searchInUnsorted(array, value), false);
		}

		/**
		 * Check for an existing element(at least one instance) in a large random array
		 */
		@Test
		public void testUnsortedManyTrue()
		{
			Random rand = new Random();
			int[] array = randomArray(60000);
			int value = array[rand.nextInt(array.length)];

			assertEquals( ArraySearch.searchInUnsorted(array, value), true);
		}

		/**
		 * Check for no false positives in a large sorted random array
		 */
		@Test
		public void testSortedManyFalse()
		{
			Random rand = new Random();
			int value = rand.nextInt(100);
			int[] array = sortedRandomArrayExclude(60000, value);

			assertEquals( ArraySearch.searchInSorted(array, value), false);
		}

		/**
		 * Check for at least one instance of an existing element,
		 * in the middle of a large sorted random array
		 * (not the first or last element)
		 */
		@Test
		public void testSortedManyMiddleTrue()
		{
			Random rand = new Random();
			int[] array = sortedRandomArray(60000);
			int value = array[1 + rand.nextInt(array.length - 1)];

			assertEquals( ArraySearch.searchInSorted(array, value), true);
		}
	
		/**
		 * Check for the existance of the first and last element
		 * of a large sorted random array
		 */
		@Test
		public void testSortedManyEdgesTrue()
		{
			int[] array = sortedRandomArray(60000);
			int firstValue = array[0];
			int lastValue = array[array.length - 1];

			assertEquals( ArraySearch.searchInSorted(array, firstValue), true);
			assertEquals( ArraySearch.searchInSorted(array, lastValue), true);
		}


		//~ Private Methods
		private int[] randomArray(int size)
		{
			Random rand = new Random();

			int[] array = new int[size];
			for (int i = 0; i < size; i++){
				array[i] = rand.nextInt();
			}

			return array;
		}

		private int[] sortedRandomArray(int size)
		{
			int[] array = randomArray(size);
			Arrays.sort(array);
			return array;
		}


		private int[] randomArrayExclude(int size, int excludedValue)
		{
			Random rand = new Random();
			int[] array = new int[size];
			int r;
			for (int i = 0; i < size; i++){
				r = rand.nextInt(100); //little hack to make sure this will loop at least once
				while (r == excludedValue) 
					r = rand.nextInt(); 

				array[i] = r;
			}
	
			return array;
		}

		private int[] sortedRandomArrayExclude(int size, int excludedValue){
			int[] array = randomArrayExclude(size, excludedValue);
			Arrays.sort(array);
			return array;
		}
	
				
		// TODO: add more tests here. Each line of code in ArraySearch.java should
		// be executed at least once from at least one test.
	
	
	
	}

