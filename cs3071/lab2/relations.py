import itertools

def all(li):
	table = dict(zip(li, [set() for i in range(len(li))]))
	ret = []

	done = 0
	for p in itertools.permutations(li):
		print p
		last = p[0]
		for e in p[1:]:
			if last not in table[e]:
				table[e].add(last)
				if len(table[e]) == len(li) - 1:
					done += 1

			last = e

		ret.append(p)
		if done == len(li):
			return ret

	print table, done
	print 'got here'
	return perms

