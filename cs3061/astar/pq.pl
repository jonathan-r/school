% insert(+Item, +Pqueue, ?NewPQ)
insert(Item, [], t(Item, [], [])).
insert(Item, t(T, L, R), NPQ) :-
	Item > T, !,
	insert(Item, R, NR), NPQ = t(T, NR, L);
	insert(T, R, NR), NPQ = t(Item, NR, L).

% remove(+PQueue, ?Item, ?NewPQ)
remove(t(Item, L, []), Item, L).
remove(t(T, t(LT, LL, LR), t(RT, RL, RR)), T, NPQ) :-
	LT < RT, !,
	remove(t(LT, LL, LR), NT, NL), NPQ = t(NT, t(RT, RL, RR), NL);
	remove(t(RT, RL, RR), NT, NR), NPQ = t(NT, t(LT, LL, LR), NR).

listToPq([], PQ, PQ).
listToPq([H | T], PQIn, PQOut) :-
	insert(H, PQIn, NPQ),
	listToPq(T, NPQ, PQOut).

% listToPq(+List, ?PQueue)
listToPq(Ar, PQ) :-
	listToPq(Ar, [], PQ).

% pqToList(+PQ, ?List)
pqToList([], []).
pqToList(PQ, Out) :-
	remove(PQ, Item, NPQ),
	pqToList(NPQ, NL),
	append([Item], NL, Out).
