library(shiny)
library(ggplot2)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  # Application title
  titlePanel("Minimum sample size for testing for an important difference"),
  
  # Sidebar with a slider input for the number of bins
  sidebarLayout(
    sidebarPanel(
      h3("Data Standard Deviation"),
      selectInput("source_type", labe="", choices = c("Upload sample csv" = "csv", 
        "estimate (linear)" = "sd_lin", "estimate (log10)" = "sd_log10"), "sd_lin"),
      
      #http://shiny.rstudio.com/gallery/upload-file.html
      conditionalPanel(condition = "input.source_type == 'csv'",
                       fileInput("csv_input", "", multiple = F, accept = 
                        c('text/csv', 'text/comma-separated-values', 'text/tab-separated-values', 
                          'text/plain', '.csv', '.tsv'))
      ),
      
      conditionalPanel(condition = "input.source_type == 'csv'",
                       textInput("csv_input_column", "column (optional)", "")
      ),
      
      conditionalPanel(condition = "input.source_type == 'csv'",
                       checkboxInput('csv_input_header', 'Header', T)
      ),
      
      conditionalPanel(condition = "input.source_type == 'csv'",
                       radioButtons('csv_input_sep', 'Separator', c("comma" =',', "semicolon"=';', 
                        "tab"='\t'), ',')
      ),
      
      conditionalPanel(condition = "input.source_type == 'csv'",
                       radioButtons('csv_input_quote', 'Quote', c(None='', 'Double Quote'='"', 
                        'Single Quote'="'"), '"')
      ),
      
      
      conditionalPanel(condition = "input.source_type == 'sd_lin'",
                       sliderInput("sd_input_lin", "", min = 0, max = 100, value = 1, step = 0.5)
      ),
      
      conditionalPanel(condition = "input.source_type == 'sd_log10'",
                       sliderInput("sd_input_log", "", min = -2, max = 10, value = 0, step = 0.1)
      ),
      
      textOutput("sd_val"),
      
      tags$hr(),
      
      h3("Significant Difference"),
      radioButtons("diff_input_type", "Scale:", c("Linear" = "diff_lin", "Log10" = "diff_log10")),
      
      conditionalPanel(condition = "input.diff_input_type == 'diff_lin'",
                       sliderInput("diff_input_lin", "", min = 0, max = 100, value = 1, step = 0.5)
      ),
      
      conditionalPanel(condition = "input.diff_input_type == 'diff_log10'",
                       sliderInput("diff_input_log", "", min = -2, max = 10, value = 0, step = 0.1)
      ),
      
      textOutput("diff_val"),
      
      tags$hr(),
      
      sliderInput("alpha_input", "α (confidence level = 1-α)", min = 0.001, max = 0.2, value = 0.01, 
        step = 0.001),
      sliderInput("beta_input", "β (power = 1-β)", min = 0.01, max = 9.99, value = 0.2, step = 0.01)
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      h3("Sampling distribution"),
      plotOutput("plot"),
      
      tags$hr(),
      
      h3("Percentage area"),
      textOutput("percentage_area"),
      
      tags$hr(),
      
      h3("Minimum sample size"),
      textOutput("sample_size")
    )
  )
))


