mkList(0, []).
mkList(X, [X | TL]) :- X > 0, Y is X-1, mkList(Y, TL). %X > 0 to cover all cases

%prepends X onto each member of the input list of lists
prepended(X, [], []).
prepended(X, [HL | TL], [[X | HL] | TLD]) :- prepended(X, TL, TLD).

splitSum(1, [[1]]).
splitSum(X, [[X] | LS]) :-
	X_dec is X - 1, mkList(X_dec, X_dec_to_1),
	member(I, X_dec_to_1),
	X_min_I is X - I, 
	splitSum(I, SRT),
	prepended(X_min_I, SRT, LS).

s         --> result(X), Sum, {X > 0}, {splitSum(X, L)}, {member(Sum, L)}.
result(X) --> [X], {X > 0}.
