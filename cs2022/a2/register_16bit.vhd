library ieee;
use ieee.std_logic_1164.all;


entity register_16bit is
    port ( load : in  std_logic;
           clk  : in  std_logic;
           d    : in  std_logic_vector (15 downto 0);
           q    : out std_logic_vector (15 downto 0) );
end register_16bit;

architecture behavioral of register_16bit is begin 
	process(clk) begin
		if clk'event and clk = '1' then
			if load = '1' then
				q <= d;
			end if;
		end if;
	end process;
end behavioral;

