#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <SDL2/SDL_image.h>

#include "vbo.h"
#include "texture.h"
#include "asset.h"

Mesh* loadMesh(const char* name, vertex** keepVerts, uint32_t** keepIndices) {
	char path[256];
	sprintf(path, "%s%s", ASSET_PATH, name);

	//import plane obj
	const aiScene* scene = aiImportFile(path, 
		aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_GenSmoothNormals);
		//use index buffer, ensure 3 verts per face, generate normals if missing

	if (!scene) {
		fprintf(stderr, "could not load mesh %s, because: %s\n", path, aiGetErrorString());
		return nullptr;
		//exit(0);
	}

	//make tangents (needs to be done as post process if normals are missing from original data)
	const aiScene* postScene = aiApplyPostProcessing (scene, aiProcess_CalcTangentSpace);
	if (!postScene) {
		fprintf(stderr, "could not post process mesh %s, because: %s\n", path, aiGetErrorString());
		return nullptr;
	}

	assert(scene->mNumMeshes > 0);
	const aiMesh* mesh = scene->mMeshes[0];
	vertex* vbuf = (vertex*) malloc(mesh->mNumVertices * sizeof(vertex));
	uint32_t* ibuf = (uint32_t*) malloc(mesh->mNumFaces * 3 * sizeof(uint32_t));

	for (int vi = 0; vi < mesh->mNumVertices; vi++) {
		const aiVector3D* v = &(mesh->mVertices[vi]);
		const aiVector3D* n = &(mesh->mNormals[vi]);
		//const aiVector3D* t = &(mesh->mTangents[vi]);
		//const aiVector3D* b = &(mesh->mBitangents[vi]);	
		vbuf[vi].v.set(v->x, v->y, v->z);
		vbuf[vi].n.set(n->x, n->y, n->z);
		//vbuf[vi].ta.set(t->x, t->y, t->z);
	}

	if (mesh->HasTextureCoords(0)) {
		for (int vi = 0; vi < mesh->mNumVertices; vi++) {
			const aiVector3D* t = &(mesh->mTextureCoords[0][vi]);
			vbuf[vi].t.set(t->x, t->y);
		}
	}

	for (int fi = 0; fi < mesh->mNumFaces; fi++) {
		aiFace& f = mesh->mFaces[fi];
		assert(f.mNumIndices == 3);
		ibuf[fi*3 + 0] = f.mIndices[0];
		ibuf[fi*3 + 1] = f.mIndices[1];
		ibuf[fi*3 + 2] = f.mIndices[2];
	}

	Mesh* out = new Mesh();
	out->assign(mesh->mNumVertices, vbuf, mesh->mNumFaces*3, ibuf);

	if (keepVerts)
		*keepVerts = vbuf;
	else 
		free(vbuf);

	if (keepIndices)
		*keepIndices = ibuf;
	else 
		free(ibuf);

	aiReleaseImport(scene);

	printf("loaded first model of %s\n", path);
	return out;
}

Texture* loadTexture(const char* name, SDL_Surface** keepSurface) {
	char path[256];
	sprintf(path, "%s%s", ASSET_PATH, name);
	SDL_Surface* surf = IMG_Load(path);

	if (!surf) {
		fprintf(stderr, "could not load mesh %s, because: %s\n", path, aiGetErrorString());
		return nullptr;
	}

	Texture* tex = new Texture(surf->format->BytesPerPixel, GL_UNSIGNED_BYTE, surf->w, surf->h, surf->pixels, GL_LINEAR, GL_LINEAR);

	if (keepSurface) 
		*keepSurface = surf;
	else
		SDL_FreeSurface(surf);

	printf("loaded texture %s\n", path);
	return tex;
}