import java.util.ArrayList;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

/**
 * This is a laucher program that will do the command line parsing and
 * instantiate runners and mazes and run them.
 *
 * @author Albert J. Wong (U Washington), Jonathan Rosca
 */
public class MazeRunnerLauncher {
	/**
	 * Parse the command line and instantiate the maze and maze
	 * runner and then run the maze runner on the maze.
	 */
	public static void main(String[] args) {
		boolean useVisualizer = false;
		boolean useTracer = false;
		long updateInterval = 100;

		MazeRunner<SquareCell> runner = null;
		Visualizer vis = null;

		int i = 0;
		for (; i < args.length; i++){
			String arg = args[i];

			if (arg.equals("-r")){
				if (runner == null)
					runner = new RandomMazeRunner<SquareCell>();
				else{
					System.err.println("Can't have more than one runner");
					return;
				}
			}

			else if (arg.equals("-bfs")){
				if (runner == null)
					runner = new BFSMazeRunner<SquareCell>();
				else{
					System.err.println("Can't have more than one runner");
					return;
				}
			}

			else if (arg.equals("-dfs")){
				if (runner == null)
					runner = new DFSMazeRunner<SquareCell>();
				else{
					System.err.println("Can't have more than one runner");
					return;
				}
			}

			else if (arg.equals("-astar")){
				if (runner == null)
					runner = new AStarMazeRunner<SquareCell>();
				else{
					System.err.println("Can't have more than one runner");
					return;
				}
			}

			else if (arg.equals("-bestfirst")){
				if (runner == null)
					runner = new BestFirstMazeRunner<SquareCell>();
				else{
					System.err.println("Can't have more than one runner");
					return;
				}
			}

			else if(arg.equals("-v")){
				useVisualizer = true;
			}

			else if (arg.equals("-t")){
				useTracer = true;
			}

			else if(args[i].equals("-p")){
				i++;

				if(i >= args.length) {
					printUsage();
					return;
				}

				try {
					updateInterval = Long.parseLong(args[i]);
				} 

				catch (NumberFormatException noe) {
					System.err.println("Bad Pause Interval. Defaulting to " + updateInterval);
				}
			}

			else break;
		}

		if(runner == null) {
			runner = new RandomMazeRunner<SquareCell>();
		}

		if(i != args.length - 1) {
			printUsage();
			return;
		}

		SquareCellMaze maze = null;

		try {
			// Create a maze from the given filename.
			maze = SquareCellMaze.SquareCellMazeFactory.parseMaze(args[i]);

			// Attach the visualizer if -v flag given
			if(useVisualizer) {
				String name = args[i];
				int lastSlash = name.lastIndexOf('/');
				if (lastSlash > 0) name = name.substring(lastSlash + 1);
				vis = new Visualizer(runner.getClass().getName(), maze, name, updateInterval);
				maze.addMazeChangeListener(vis);
			}

			// Attach the tracer if -t flag given
			if(useTracer) {
				PrintWriter debugWriter = new PrintWriter(
					new OutputStreamWriter(System.err), true);

				maze.addMazeChangeListener(
					new SquareCellMazeTracer(debugWriter));
			}

			// This is why Java I/O sucks sometimes.
			PrintWriter writer = new PrintWriter(
				new BufferedWriter(new OutputStreamWriter(System.out)), true);

			// Solve the mazer
			runner.solveMaze(maze, writer);

			// ensure the writer is closed so it flushes the output.
			writer.close();

		} 
		
		catch (FileNotFoundException fnfe) {
			System.err.println("Could not find file " + args[i]);
		} 

		catch (IOException ioe) {
			System.err.println("Error reading file " + args[i]);
		}
	}

	/**
	 * Prints the Usage to standard error.
	 */
	private static void printUsage() {
		System.err.println("Usage: java MazeRunnerLauncher [-r | -dfs | -bfs | -astar | -bestfirst] [-v] [-t] [-p milliseconds] <mazefile>");
		System.err.println("\t-r, -dfs, -bfs, -astar, -bestfirst -- Pick between the Random, Depth First, Breadth First, A* or Best First maze solver.");
		System.err.println("\t\tDefaults to the Random solver.");
		System.err.println("\t-v -- Visualise maze graphically.");
		System.err.println("\t-t -- Output tracing information.");
		System.err.println("\t-p -- Wait between moving to each cell for visualizer. Use with -v.");
	}
}
