varying vec4 color;

void main(){
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;// gl_TexCoord[0] = gl_MultiTexCoord0;
	color = gl_Color;
	gl_Position = ftransform();
}
