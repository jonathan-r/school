module HaskellGame.Actions where

import Prelude ( Num(..), Eq(..),
                 Char(), Maybe(..), String(),
                 ($), (++), (.), fst, snd, not, return )

import qualified Data.List as List
import Data.List (filter, find, any, null, head)

import HaskellGame.Datatypes
import HaskellGame.GameMonad
import HaskellGame.Messages
import HaskellGame.Battle
import HaskellGame.Collisions
import HaskellGame.Utils.Dictionary

movePlayer :: Char -> GameAction ()
movePlayer keyPressed = do
  p <- get player
  let (x, y) = position p
  let newPosition = case keyPressed of
                      'i' -> (x, (y-1))
                      'j' -> ((x-1), y)
                      'k' -> (x, (y+1))
                      'l' -> ((x+1), y)
                      _   -> (x, y)
  isCollision <- detectCollision newPosition

  if (fst isCollision) then
    change (\s -> s { collided = snd isCollision })
  else
    let newPlayer = p { pos = newPosition }
    in change (\s -> s { player = newPlayer, collided = Nothing } )

doAttack :: GameAction ()
doAttack = do
  thePlayer <- get player
  theMap    <- get currentLevel
  msgs      <- get messages

  let allMonsters = monsters theMap
  let nearbyMonsters = filter ((==1) . (distance thePlayer)) allMonsters

  if not (null nearbyMonsters) then
    attackMonsters nearbyMonsters
  else
    change (\s -> s { messages = msgs ++ missedMessage })

attackMonsters :: [Monster] -> GameAction ()
attackMonsters [] = return ()
attackMonsters (firstMonster:rest) = do
  oldPlayer   <- get player
  oldLevel    <- get currentLevel
  oldMessages <- get messages

  let (newPlayer, newMonster) = fight (oldPlayer, firstMonster)
  let (damageP, damageM) = ((health oldPlayer - health newPlayer), (health firstMonster - health newMonster))
  let battleMessages = hitMessage newMonster damageM newPlayer damageP
  let oldMonsters = monsters oldLevel
  let newMonsters = (newMonster:(List.delete firstMonster oldMonsters))
  let newMessages = oldMessages ++ battleMessages
  let newLevel = oldLevel { monsters = newMonsters }

  change (\s -> s { player = newPlayer, currentLevel = newLevel, messages = newMessages })
  attackMonsters rest


pickUpItem :: GameAction ()
pickUpItem = do
  plyr  <- get player
  level <- get currentLevel
  msgs  <- get messages

  let droppedItems = filter isDroppedItem (objects level)
  let couldPickUp = find ((==(position plyr)) . position) droppedItems

  case couldPickUp of
    Nothing ->
      change (\s -> s { messages = msgs ++ nothingToPickUpMessage })

    (Just ob@(Dropped i _)) ->
      let newPlayer = plyr { inventory = insert (itemName i) i (inventory plyr) }
          newLevel = level { objects = List.delete ob (objects level) }
          newMessages = msgs ++ pickUpMessage i
      in change (\s -> s { player = newPlayer, currentLevel = newLevel, messages = newMessages })

dropItem :: GameAction ()
dropItem = do
  plyr  <- get player
  level <- get currentLevel
  msgs  <- get messages

  let couldDrop = if empty (inventory plyr) then Nothing else Just (head (toList (inventory plyr)))
  let droppedItems = filter isDroppedItem (objects level)

  case couldDrop of
    Nothing ->
      change (\s -> s { messages = msgs ++ nothingToDropMessage } )

    (Just i@(theName, theItem)) -> do
      if any ((==(position plyr)) . position) droppedItems then
        change (\s -> s { messages = msgs ++ cannotDropMessage theItem } )
      else
        let newPlayer = plyr { inventory = delete theName (inventory plyr) }
            newObjects = (Dropped theItem (position plyr)):(objects level)
            newLevel = level { objects = newObjects }
            newMessages = msgs ++ dropMessage theItem
        in
          change (\s -> s { currentLevel = newLevel, player = newPlayer, messages = newMessages } )

doCommand :: Command -> GameAction ()
doCommand cmd = do
  plyr  <- get player
  level <- get currentLevel
  msgs  <- get messages

  case cmd of
    Equip itemName slotName ->
      case lookup itemName (inventory plyr) of
        Just item ->
          case lookup slotName (slots plyr) of
            Just maybeItem ->
              case maybeItem of
                Nothing ->
                  --all good, equip item into slot
                  change (\s -> s { player = plyr { inventory = delete itemName (inventory plyr), --remove from inventory
                                                    slots = insert slotName (Just item) (slots plyr) }, --equip in slots
                                    messages = msgs ++ equipMessage item slotName } )

                Just _ ->
                  --something's already in the slot
                  change(\s -> s { messages = msgs ++ slotFullMessage slotName } )

            Nothing ->
              --invalid slot name
              change(\s -> s { messages = msgs ++ noSuchSlotMessage slotName } )

        Nothing ->
          --item not in player's inventory
          change (\s -> s { messages = msgs ++ noSuchItemMessage itemName } )

    Unequip slotName ->
      case lookup slotName (slots plyr) of
        Just maybeItem ->
          case maybeItem of
            Just item ->
              --there's something in the slot, remove it
              change (\s -> s { player = plyr { slots = insert slotName Nothing (slots plyr), --remove from slots
                                                inventory = insert (itemName item) item (inventory plyr) }, --add to inventory
                                messages = msgs ++ unequipMessage item slotName } )
            Nothing ->
              --slot is already empty
              change (\s -> s { messages = msgs ++ slotEmptyMessage slotName } )

        Nothing ->
          change (\s -> s { messages = msgs ++ noSuchSlotMessage slotName } )
