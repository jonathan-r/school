#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h> //sysconf
#include <pthread.h> //threads

#include "twister.h" //mersenne twister

//thread_readonly
int points_per_thread;

typedef struct worker_data_s{
	uint32_t rand_seed;
	double fraction;
} worker_data;


void* worker(void* data){
	worker_data* self = data;

	//thread local random number generator
	twister* t = malloc(sizeof(twister));
	twister_init(t, self->rand_seed);

	int circle_points = 0;
	for (int i = 0; i < points_per_thread; i++){
		double x = twister_unit(t);
		double y = twister_unit(t);
		double sq_len = x * x + y * y;
		circle_points += (sq_len <= 1.0);
	}

	self->fraction = (double)circle_points / points_per_thread;
	free(t);

	pthread_exit(NULL);
}


int main(int argc, char** argv){
	if (argc != 2 && argc != 3){
		printf("Usage: pi <iterations> [threads]. Prints result to stdout\n");
		printf("If not specified, threads is the number of hardware threads\n");
	}

	else{
		int iterations = strtol(argv[1], NULL, 10);
		int num_workers;

		if (argc == 3)
			num_workers = strtol(argv[2], NULL, 10);

		else
			num_workers = sysconf(_SC_NPROCESSORS_ONLN);

		if (iterations > 0 && num_workers > 0){
			points_per_thread = iterations / num_workers;
			if (points_per_thread == 0) points_per_thread = 1;

			pthread_t* handles = malloc(num_workers * sizeof(pthread_t));
			worker_data* data = malloc(num_workers * sizeof(worker_data));

			int seed = (int)time(NULL);
			for (int i = 0; i < num_workers; i++){
				data[i].rand_seed = seed * i;
				data[i].fraction = 0.0;

				pthread_create(&handles[i], NULL, worker, &data[i]);
			}
			
			double sum = 0.0;
			for (int i = 0; i < num_workers; i++){
				pthread_join(handles[i], NULL);
				
				sum += data[i].fraction;
				printf("thread %d: %f\n", i, data[i].fraction);
			}

			free(handles);
			free(data);

			printf("%f\n", sum / (num_workers / 4.0));
		}

		else{
			printf("iterations and threads must be greater than 0\n");
		}
	}

	return 0;
}