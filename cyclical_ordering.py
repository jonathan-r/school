
#given a sequence A with indices from i to j, and an index s, such that A[s] is the smallest element in the sequence, and A[s : j+1] ++ A[i : s] is ordered. in other words, A is a sorted list where the second half (after s) was sliced and inserted at the beginning.

def is_cyclical(a):
	if len(a) < 2: return False
	
	#there must be two ordered segments in the list, the second starting at s
	seg = 1 #a[0] is part of the first segment
	
	last = a[0]
	for e in a[1: ]:
		if e < last: 
			seg += 1
			
		last = e
	
	return seg == 2 and a[-1] <= a[0]

def find_s(a):
	#a must a cyclically ordered list
	li = 0
	ri = len(a) - 1
	
	while li != ri:
		mi = (li + ri) / 2
		
		l = a[li]
		r = a[ri]
		m = a[mi]
		
		if m > r:
			li = mi + 1
		
		else:
			ri = mi
	
	#s = a[li]
	return li

#test
lists = []
from random import randint
l = [0]
for i in range(25):
	l.append(randint(l[-1], l[-1] + 10))
	
lists.append(l) #normal list

cutoff = randint(5, 20)
lists.append(l[cutoff:] + l[:cutoff]) #cyclically ordered list

for l in lists:
	print(l)
	if is_cyclical(l):
		print("a[s] is " + str(l[find_s(l)]))
