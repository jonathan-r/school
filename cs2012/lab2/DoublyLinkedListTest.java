import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

//-------------------------------------------------------------------------
/**
 *  Test class for Doubly Linked List
 *
 *  @author  Jonathan Roșca
 *  @version 2014.01.19
 */
@RunWith(JUnit4.class)
public class DoublyLinkedListTest
{
	//~ Constructor ........................................................
	@Test
	public void testConstructor()
	{
	  new DoublyLinkedList();
	}

	//~ Public Methods ........................................................

	// ----------------------------------------------------------
	/**
	 * Check if insert works at the beginning
	 */
	@Test
	public void testInsertFirst()
	{
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		testDLL.insertFirst(1);
		assertEquals( "Checking insertion at beginning", "1", testDLL.toString() );
		assertEquals( "Checking insertion at beginning", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertFirst(2);
		assertEquals( "Checking insertion at beginning", "2,1", testDLL.toString() );
		assertEquals( "Checking insertion at beginning", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertFirst(3);		
		assertEquals( "Checking insertion at beginning", "3,2,1", testDLL.toString() );
		assertEquals( "Checking insertion at beginning", testDLL.toString(), testDLL.toStringAlt() );
	}

	// ----------------------------------------------------------
	/**
	 * Check if the insert works at the end
	 */
	@Test
	public void testInsertLast()
	{
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		testDLL.insertLast(1);
		assertEquals( "Checking insertion at end", "1", testDLL.toString() );
		assertEquals( "Checking insertion at end", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertLast(2);
		assertEquals( "Checking insertion at end", "1,2", testDLL.toString() );
		assertEquals( "Checking insertion at end", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertLast(3);		
		assertEquals( "Checking insertion at end", "1,2,3", testDLL.toString() );
		assertEquals( "Checking insertion at end", testDLL.toString(), testDLL.toStringAlt() );
	}

	// ----------------------------------------------------------
	/**
	 * Check if the insert works in the middle
	 */
	@Test
	public void testInsertBefore()
	{
		// test non-empty list
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		testDLL.insertLast(1);
		testDLL.insertLast(2);
		testDLL.insertLast(3);

		testDLL.insertBefore(0,4);
		assertEquals( "Checking insertion at 0", "4,1,2,3", testDLL.toString() );
		assertEquals( "Checking insertion at 0", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertBefore(1,5);
		assertEquals( "Checking insertion at 1", "4,5,1,2,3", testDLL.toString() );
		assertEquals( "Checking insertion at 1", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertBefore(2,6);		
		assertEquals( "Checking insertion at 2", "4,5,6,1,2,3", testDLL.toString() );
		assertEquals( "Checking insertion at 2", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertBefore(-1,7);		
		assertEquals( "Checking insertion at -1", "7,4,5,6,1,2,3", testDLL.toString() );
		assertEquals( "Checking insertion at -1", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertBefore(7,8);		
		assertEquals( "Checking insertion at 7", "7,4,5,6,1,2,3,8", testDLL.toString() );
		assertEquals( "Checking insertion at 7", testDLL.toString(), testDLL.toStringAlt() );
		testDLL.insertBefore(700,9);		
		assertEquals( "Checking insertion at 700", "7,4,5,6,1,2,3,8,9", testDLL.toString() );
		assertEquals( "Checking insertion at 700", testDLL.toString(), testDLL.toStringAlt() );

		// test empty list
		testDLL = new DoublyLinkedList<Integer>();
		testDLL.insertBefore(0,1);		
		assertEquals( "Checking insertion at 2", "1", testDLL.toString() );
		assertEquals( "Checking insertion at 2", testDLL.toString(), testDLL.toStringAlt() );
		testDLL = new DoublyLinkedList<Integer>();
		testDLL.insertBefore(10,1);		
		assertEquals( "Checking insertion at 2", "1", testDLL.toString() );
		assertEquals( "Checking insertion at 2", testDLL.toString(), testDLL.toStringAlt() );
		testDLL = new DoublyLinkedList<Integer>();
		testDLL.insertBefore(-10,1);		
		assertEquals( "Checking insertion at 2", "1", testDLL.toString() );
		assertEquals( "Checking insertion at 2", testDLL.toString(), testDLL.toStringAlt() );
	}
	
	@Test
	public void testIsEmpty(){
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		assertEquals( "Checking empty list", true, testDLL.isEmpty() );
		
		testDLL.insertFirst(1);
		assertEquals( "Checking non-empty list", false, testDLL.isEmpty() );
		
		testDLL.deleteFirst();
		assertEquals( "Checking emptied list", true, testDLL.isEmpty() );
	}
	
	@Test
	public void testGetEnds(){
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		assertEquals( "Checking getFirst on empty list", null, testDLL.getFirst() );
		assertEquals( "Checking getLast on empty list", null, testDLL.getLast() );
		
		testDLL.insertFirst(3);
		testDLL.insertFirst(2);
		testDLL.insertFirst(1);
		
		assertEquals( "Checking getFirst on non-empty list", 1, (int)testDLL.getFirst() );
		assertEquals( "Checking getLast on non-empty list", 3, (int)testDLL.getLast() );
	}
	
	@Test
	public void testGetAt(){
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		assertEquals( "Checking get(below bounds) on empty list", null, testDLL.get(-500) );
		assertEquals( "Checking get(above bounds) on empty list", null, testDLL.get( 500) );
		
		testDLL.insertFirst(2);
		testDLL.insertFirst(1);
		testDLL.insertLast(3);
		
		assertEquals( "Checking get(in bounds) on non-empty list", 2, (int)testDLL.get(1) );
		assertEquals( "Checking get(edge of bounds) on non-empty list", 3, (int)testDLL.get(2) );
		
		assertEquals( "Checking get(below bounds) on empty list", null, testDLL.get(-500) );
		assertEquals( "Checking get(above bounds) on empty list", null, testDLL.get( 500) );
	}
	
	@Test
	public void testDeleteEdges(){
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		assertEquals( "Checking deleteFirst on empty list", false, testDLL.deleteFirst() );
		assertEquals( "Checking deleteLast on empty list", false, testDLL.deleteLast() );
		
		testDLL.insertFirst(3);
		testDLL.insertFirst(2);
		testDLL.insertLast(1);

		assertEquals( "Checking deleteFirst on non-empty list", true, testDLL.deleteFirst() );
        assertEquals( "Checking list state after deleteFirst", "3,1", testDLL.toString() );
        assertEquals( "Checking list state after deleteFirst", testDLL.toString(), testDLL.toStringAlt() );

		assertEquals( "Checking deleteLast on 2 element list", true, testDLL.deleteLast() );
        assertEquals( "Checking list state after deleteFirst", "3", testDLL.toString() );
        assertEquals( "Checking list state after deleteFirst", testDLL.toString(), testDLL.toStringAlt() );

		assertEquals( "Checking deleteLast on 1 element list", true, testDLL.deleteLast() );
        assertEquals( "Checking list state after deleteFirst", "", testDLL.toString() );
        assertEquals( "Checking list state after deleteFirst", testDLL.toString(), testDLL.toStringAlt() );
	}
	
	@Test
	public void testDeleteAt(){
		DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<Integer>();
		assertEquals( "Checking delete(below bounds) on empty list", false, testDLL.deleteAt(-500) );
		assertEquals( "Checking delete(above bounds) on empty list", false, testDLL.deleteAt( 500) );
		
		testDLL.insertFirst(2);
		testDLL.insertFirst(1);
		testDLL.insertLast(3);
		testDLL.insertLast(4);
		
		assertEquals( "Checking delete(first element) on non-empty list", true, testDLL.deleteAt(0) );
        assertEquals( "Checking list state after deleteAt(0)", "2,3,4", testDLL.toString() );
        assertEquals( "Checking list state after deleteAt(0)", testDLL.toString(), testDLL.toStringAlt() );

		assertEquals( "Checking delete(middle element) on non-empty list", true, testDLL.deleteAt(1) );
        assertEquals( "Checking list state after deleteAt(1)", "2,4", testDLL.toString() );
        assertEquals( "Checking list state after deleteAt(1)", testDLL.toString(), testDLL.toStringAlt() );

		assertEquals( "Checking delete(last elment) on non-empty list", true, testDLL.deleteAt(1) );
        assertEquals( "Checking list state after deleteAt(1)", "2", testDLL.toString() );
        assertEquals( "Checking list state after deleteAt(1)", testDLL.toString(), testDLL.toStringAlt() );
	}
}
