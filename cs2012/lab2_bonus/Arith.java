// -------------------------------------------------------------------------
/**
 *  Utility class containing validation/evaluation/conversion operations
 *  for prefix and postfix arithmetic expressions.
 *
 *  @author Jonathan Rosca
 *  @version 05/02/14 13:03:48
 */

import java.util.LinkedList;

public class Arith {
	public static class StackArray<E extends Comparable<E>>{
		private E[] array;
		private int it; //points at the first empty space
		
		public StackArray(int maxSize){
			array = (E[])(new Comparable[maxSize]);
			it = 0;
		}

		public int size(){
			return it;
		}

		public boolean push(E e){
			if (it < array.length){
				array[it++] = e;
				return true;
			}
			else return false;
		}

		public E peek(){
			if (it > 0) return array[it - 1];
			else return null;
		}

		public E pop(){
			if (it > 0) return array[--it];
			else return null;
		}

		//a must be an instace of type E[]
		//needed cause java is terrible with generic types
		public E[] toArray(E[] a){
			Class<?> type = a.getClass().getComponentType();
			E[] ret = (E[])java.lang.reflect.Array.newInstance(type, it);
			for (int i = 0; i < it; i++) ret[i] = array[i];
			return ret;
		}
	}

	//~ Validation methods ..........................................................


	/**
	 * Validation method for prefix notation.
	 *
	 * @param prefixLiterals : an array containing the string literals hopefully in prefix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return true if the parameter is indeed in prefix notation, and false otherwise.
	 **/
	public static boolean validatePrefixOrder(String[] prefixLiterals){
		int count = 1;
		for (String s : prefixLiterals){
			if (count <= 0) return false;
			if (s.matches("[\\*-/\\+]")) count++;
			else count--;
		}
		return (count == 0);
	}


	/**
	 * Validation method for postfix notation.
	 *
	 * @param postfixLiterals : an array containing the string literals hopefully in postfix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return true if the parameter is indeed in postfix notation, and false otherwise.
	 **/
	public static boolean validatePostfixOrder(String[] postfixLiterals){
		int count = 0;
		for (String s : postfixLiterals){
			if (s.matches("[\\*-/\\+]")) count--;
			else count++;
			if (count < 0) return false;
		}
		return (count == 1);
	}


	//~ Evaluation  methods ..........................................................


	/**
	 * Evaluation method for prefix notation.
	 *
	 * @param prefixLiterals : an array containing the string literals in prefix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return the integer result of evaluating the expression
	 **/
	public static int evaluatePrefixOrder(String prefixLiterals[]) {
		StackArray<Integer> operands = new StackArray<Integer>(prefixLiterals.length / 2 + 1);
		for (int i = prefixLiterals.length - 1; i >= 0; i--) {
			String s = prefixLiterals[i];
			if (s.matches("[\\*-/\\+]")){
				int left = operands.pop();
				int right = operands.pop();

				switch(s.charAt(0)){
					case '+': operands.push(left + right); break;
					case '-': operands.push(left - right); break;
					case '*': operands.push(left * right); break;
					case '/': operands.push(left / right); break;
				}
			}
			else {
				operands.push(Integer.parseInt(s));        
			}
		}
		return operands.pop().intValue();
	}


	/**
	 * Evaluation method for postfix notation.
	 *
	 * @param postfixLiterals : an array containing the string literals in postfix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return the integer result of evaluating the expression
	 **/
	public static int evaluatePostfixOrder(String postfixLiterals[]) {
		StackArray<Integer> operands = new StackArray<Integer>(postfixLiterals.length / 2 + 1);
		for (String s : postfixLiterals) {
			if (s.matches("[\\*-/\\+]")) {
				int right = operands.pop();
				int left = operands.pop();

				switch(s.charAt(0)){
					case '+': operands.push(left + right); break;
					case '-': operands.push(left - right); break;
					case '*': operands.push(left * right); break;
					case '/': operands.push(left / right); break;
				}
			}
			else {
				operands.push(Integer.parseInt(s));        
			}
		}
		return operands.pop().intValue();
	}


	//~ Conversion  methods ..........................................................


	/**
	 * Converts prefix to postfix.
	 *
	 * @param prefixLiterals : an array containing the string literals in prefix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return the expression in postfix order.
	 **/
	public static String[] convertPrefixToPostfix(String prefixLiterals[]) {
		StackArray<String> operators = new StackArray<String>(prefixLiterals.length / 2);
		StackArray<Integer> operatorFill = new StackArray<Integer>(prefixLiterals.length / 2);
		String[] out = new String[prefixLiterals.length];
		int oi = 0;

		for (String s : prefixLiterals){
			if (s.matches("[\\*-/\\+]")) { //operator
				operators.push(s);
				operatorFill.push(0);
			}
			else{ //number
				out[oi++] = s;
				while (operators.size() > 0){
					int next = operatorFill.pop().intValue() + 1;
					if (next == 2) out[oi++] = operators.pop(); //this one is spent
					else{
						operatorFill.push(next); //put it back
						break; //neither this nor the previous ones are spent
					}
				}
			}
		}

		return out;
	}


	/**
	 * Converts postfix to prefix.
	 *
	 * @param prefixLiterals : an array containing the string literals in postfix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return the expression in prefix order.
	 **/
	public static String[] convertPostfixToPrefix(String postfixLiterals[]){
		StackArray<String> operators = new StackArray<String>(postfixLiterals.length / 2);
		StackArray<Integer> operatorFill = new StackArray<Integer>(postfixLiterals.length / 2);
		String[] out = new String[postfixLiterals.length];
		int oi = postfixLiterals.length;

		for (int i = postfixLiterals.length - 1; i >= 0; i--){
			String s = postfixLiterals[i];
			if (s.matches("[\\*-/\\+]")) { //operator
				operators.push(s);
				operatorFill.push(0);
			}
			else { //number
				out[--oi] = s;
				while (operators.size() > 0) {
					//the number (or possible a spent previous operand, on the stack) is 'filling' the topmost operand
					int next = operatorFill.pop().intValue() + 1;
					if (next == 2) out[--oi] = operators.pop(); //this one is spent
					else {
						operatorFill.push(next); //put it back
						break; //neither this nor the previous ones are spent
					}
				}
			}
		}

		return out;
	}

	/**
	 * Converts prefix to infix.
	 *
	 * @param infixLiterals : an array containing the string literals in prefix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 *
	 * @return the expression in infix order.
	 **/
	public static String[] convertPrefixToInfix(String prefixLiterals[]){
		StackArray<String> operators = new StackArray<String>(prefixLiterals.length / 2);
		StackArray<Integer> operatorFill = new StackArray<Integer>(prefixLiterals.length / 2);
		StackArray<String> out = new StackArray<String>(prefixLiterals.length * 3);

		for (String s : prefixLiterals){
			if (s.matches("[\\*-/\\+]")) { //operator
				operators.push(s);
				operatorFill.push(0);
				out.push("(");
			}
			else{ //number
				out.push(s);

				while (operators.size() > 0) {
					int next = operatorFill.pop().intValue() + 1;
					if (next == 2) {
						operators.pop(); //this one is spent
						out.push(")");
					}
					else {
						operatorFill.push(next); //put it back
						break; //neither this nor the previous ones are spent
					}
				}
				String nextOp = operators.peek();
				if (nextOp != null) out.push(nextOp);
			}
		}

		return out.toArray(new String[0]);
	}

	/**
	 * Converts postfix to infix.
	 *
	 * @param infixLiterals : an array containing the string literals in postfix order.
	 * The method assumes that each of these literals can be one of:
	 * - "+", "-", "*", or "/"
	 * - or a valid string representation of an integer.
	 * @return the expression in infix order.
	 **/
	public static String[] convertPostfixToInfix(String postfixLiterals[]){
		StackArray<String> operators = new StackArray<String>(postfixLiterals.length / 2);
		StackArray<Integer> operatorFill = new StackArray<Integer>(postfixLiterals.length / 2);
		StackArray<String> out = new StackArray<String>(postfixLiterals.length * 3);

		for (int i = postfixLiterals.length - 1; i >= 0; i--){
			String s = postfixLiterals[i];
			if (s.matches("[\\*-/\\+]")) { //operator
				operators.push(s);
				operatorFill.push(0);
				out.push(")");
			}
			else{ //number
				out.push(s);

				while (operators.size() > 0) {
					int next = operatorFill.pop().intValue() + 1;
					if (next == 2) {
						operators.pop(); //this one is spent
						out.push("(");
					}
					else {
						operatorFill.push(next); //put it back
						break; //neither this nor the previous ones are spent
					}
				}
				String nextOp = operators.peek();
				if (nextOp != null) out.push(nextOp);
			}
		}

		String[] infix = out.toArray(new String[0]);
		for (int i = 0; i < infix.length / 2; i++){
			String temp = infix[i];
			infix[i] = infix[infix.length - i - 1];
			infix[infix.length - i - 1] = temp;
		}

		return infix;
	}
}
