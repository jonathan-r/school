{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

--standard
import Text.Read (readMaybe)
import Data.Text.Lazy (pack, unpack)

--libaries
import Web.Scotty
import Network.HTTP.Types.Status (status400)
import Text.Blaze.Html5 ((!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import qualified Text.Blaze.Html.Renderer.Text as R

--internal
import Convert (makeSVG) 
import Shapes (Drawing, testDrawing)

main :: IO ()
main =
    scotty 3030 $ do
        get "/" $ do
            setHeader "Content-Type" "text/html"
            file "./inputPage.html"

        post "/" $ do
            width :: Int <- param "shapeWidth" `rescue` (const next)
            height :: Int <- param "shapeHeight" `rescue` (const next)
            src :: String <- param "shapeSource" `rescue` (const next)
            
            let drawingMaybe = readMaybe src
            case drawingMaybe of
                Nothing -> next
                Just drawing -> do
                    let svg = makeSVG drawing (width, height)
                    html $ R.renderHtml $
                        H.html $ do
                            H.head $ H.title "Shape to SVG output"
                            H.body $ do
                                H.p "SVG output:"
                                svg ! A.style "border: solid 1px black;"
        
        --executed if previous route does next
        post "/" $ do
            status status400
            html "Filed to parse or form error"
                