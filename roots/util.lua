
function normalize(x, y)
	local d = math.sqrt(x*x + y*y)
	return x/d, y/d
end

function diffOb(a, b)
	local dx = b.x - a.x
	local dy = b.y - a.y
	return dx, dy
end

function distOb(a, b)
	local dx, dy = diffOb(a, b)
	return math.sqrt(dx*dx + dy*dy)
end

function dirOb(a, b)
	local dx, dy = diffOb(a, b)
	return normalize(dx, dy)
end

function circleRad(area)
	--a = pi * r^2
	--r^2 = a / pi
	--r = sqrt(a / pi)
	return math.sqrt(area / math.pi)
end

function sectorRad(area, ratio)
	--a = pi * r^2 * ratio
	--r^2 = a / (pi * ratio)
	--r = sqrt(a / (pi * ratio))
	return math.sqrt(area / (math.pi * ratio))
end

function clamp(v, l, h)
	return math.min(math.max(l, v), h)
end

function octaveNoise(x, y, sb, db, sf, df)
	local v = 0
	local mul = sb
	local div = db
	
	for k = 1,5 do
		v = v + love.math.noise(x * mul, y * mul) * div
		mul = mul * sf
		div = div * df
	end
	
	return v
end

function distoredNoise(x, y, dist)
	local v1 = love.math.noise(x + 13.5, y + 13.5) * dist
	local v2 = love.math.noise(x, y) * dist
	return love.math.noise(x+v1, y+v2)
end