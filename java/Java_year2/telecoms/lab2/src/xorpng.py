from PIL import Image

im = Image.new("RGBA", (64 * 4, 64 * 4), "black")

for j in xrange(-32 * 4, 32 * 4):
	for i in xrange(-32 * 4, 32 * 4):
		x = (i ^ j)
		r = x & 0xFF
		g = (x >> 8) & 0xFF
		b = (x >> 16) & 0xFF
		a = 0xFF#(x >> 24)
		im.putpixel((i + 32 * 4, j + 32 * 4), (r, g, b, a))
		
im.save('xor.png', 'PNG')
