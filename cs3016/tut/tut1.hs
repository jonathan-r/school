data Colour = Black | Green | Red | Blue

type OwnerName = String

type Address = [String]

data Details = Details OwnerName Address

type AnimalName = String

data Id = Collar AnimalName
		| Microchip Details

type Weight = Int

data Animal = Dog [Id] Colour Weight
			| Cat [Id] Colour Weight
			| Mouse Colour Weight
			| Frog Colour Weight
			| Snake [Id] Colour Weight

testAnimal = Dog [Collar "Spot", Microchip (Details "A. N. Other" ["23 Fake St.", "Faketon"])] Green 43

testAnimalCol :: Colour
testAnimalCol = 
	let 
		(Dog _ col _) = testAnimal
	in
		col

isChipped :: Animal -> Bool
isChipped animal =
	let {
		hasChip ((Microchip _) : ids) = True;
		hasChip [] = False;
		hasChip (_ : ids) = hasChip ids 
	}
	in
		case animal of 
			(Dog ids _ _) -> hasChip ids
			(Cat ids _ _) -> hasChip ids
			(Snake ids _ _) -> hasChip ids
			_ -> False

ownerDetailsString :: Animal -> String
ownerDetailsString animal =
	let {
		joinAddr c (h : (h1 : t)) = h ++ c ++ (joinAddr c (h1 : t));
		joinAddr c [h] = h
	}
	in
		let {
			ownerDetails [] = "No details";
			ownerDetails ((Microchip (Details name addr)) : ids) = name ++ ", " ++ (joinAddr ',' addr);
			ownerDetails (_ : ids) = ownerDetails ids
		}
		in
			case animal of
				(Dog ids _ _) -> ownerDetails ids
				(Cat ids _ _) -> ownerDetails ids
				(Snake ids _ _) -> ownerDetails ids
				_ -> ownerDetails []
	
	
	
