from matplotlib import pyplot
from random import random
import json, sys, math

db = json.load(sys.stdin)
plt = []

locks = {}
data = db["data"]

highest = 0

for item in data:
	if "method" in item:
		lt = item["method"]
		nt = item["threads"]
		inc = item["increments"]

		if lt not in locks:
			locks[lt] = {}

		locks[lt][nt] = inc
		highest = max((inc, highest))

cols = []
md = 2.5 / len(locks)
for i in range(len(locks)):
	while True:
		r = 0.3 + random() * 0.7
		g = 0.3 + random() * 0.7
		b = 0.3 + random() * 0.7
		good = True
	
		for c2 in cols:
			d = math.sqrt((r - c2[0])**2 + (g - c2[1])**2 + (b - c2[2])**2)
			if d < md:
				good = False
				break
		
		if good:
			cols.append((r, g, b))
			break

for i, lt in enumerate(locks):
	d = locks[lt].items()
	d.sort(key= lambda x : x[0])
	xs, ys = zip(*d)
	ys = map(lambda x : float(x) / highest, ys)

	p = pyplot.plot(xs, ys, linestyle='-', c=cols[i], marker="+.1234"[i], ms=15, label=locks.keys()[i])
	plt.append(p)

pyplot.xlabel("Threads")
pyplot.ylabel("Increments")
pyplot.legend()
pyplot.show()
