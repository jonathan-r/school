lcRule([X | Preds], KB) :- 
	append_as_lists(Preds, KB, XKB),
	print(XKB),
	cn(C, XKB), 
	member(X, C).

append_as_lists([], KB, KB).
append_as_lists([H | T], KB, Out) :-
	Mid = [[H] | KB],
	append_as_lists(T, Mid, Out).

cn(C, KB) :- cn([], C, KB).
cn(TempC, C, KB) :-
	member([H|B], KB),
	all(B, TempC),
	nonmember(H, TempC),
	cn([H|TempC], C, KB).

cn(C, C, _).

all([], _).
all([H|T], L) :-
	member(H, L), 
	all(T, L).

nonmember(_, []).
nonmember(X, [H|T]) :- 
	X \= H, 
	nonmember(X, T).
