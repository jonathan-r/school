library ieee;
use ieee.std_logic_1164.all;
 
entity mux2_16bit_tb is
end mux2_16bit_tb;
 
architecture behavior of mux2_16bit_tb is 
	component mux2_16bit
		port (in0 : in  std_logic_vector (15 downto 0);
				in1 : in  std_logic_vector (15 downto 0);
				addr : in std_logic;
				output : out  std_logic_vector (15 downto 0) );
	end component;

	signal addr : std_logic := '0';
	signal in0 : std_logic_vector(15 downto 0) := (others => '0');
	signal in1 : std_logic_vector(15 downto 0) := (others => '0');
	signal output : std_logic_vector(15 downto 0);

begin
	uut: mux2_16bit port map (in0, in1, addr, output);
	
	stim_proc: process begin		
		wait for 1 ns;
		in0 <= x"abab";
		in1 <= x"cfcf";
		
		addr <= '0';
		wait for 1 ns;
		assert (output = x"abab");

		addr <= '1';
		wait for 1 ns;
		assert (output = x"cfcf");	
		
		wait;
	end process;
end;
