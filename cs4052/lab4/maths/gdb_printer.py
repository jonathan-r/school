import gdb, re

class vecPrinter:
	def __init__(self, n, val):
		self.n = n
		self.val = val

	def to_string(self):
		return '(' + ', '.join(map(lambda x : '%.4f' % self.val[x], 'xyzw'[:self.n])) + ')'

class quatPrinter:
	def __init__(self, val):
		self.val = val

	def to_string(self):
		val = self.val
		return '(%+.4fi %+.4fj %+.4fk %+.4f)' % (val['x'], val['y'], val['z'], val['w'])

def lookup(val):
	# Get the type.
	type = val.type;

	# If it points to a reference, get the reference.
	if type.code == gdb.TYPE_CODE_REF:
		type = type.target()

	# Get the unqualified type, stripped of typedefs.
	type = type.unqualified().strip_typedefs ()

	# Get the type name.
	typename = type.tag
	if typename == None:
		return None

	print('got type', typename)

	m = vec_regex.search(typename)
	if m is not None:
		return vecPrinter(int(m.group(1)), val)

	#m = mat_regex.search(typename)
	#if m is not None:
	#	return matPrinter(int(m.group(1)), val)

	m = quat_regex.search(typename)
	if m is not None:
		return quatPrinter(val)

	return None #no match	

def register_maths_printer(obj):
	if obj is None:
		obj = gdb

	print("registering vec/mat/quat printers")
	obj.pretty_printers.append(lookup)

vec_regex = re.compile("^vec([234])T<\w+>$")
mat_regex = re.compile("^mat([234])T<\w+>$")
quat_regex = re.compile("^quatT<\w+>$")
