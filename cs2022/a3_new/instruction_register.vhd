library ieee;
use ieee.std_logic_1164.all;

entity instruction_register is port(
	input : in std_logic_vector(15 downto 0);
	il : in std_logic;
	clock, reset : in std_logic;
	opcode : out std_logic_vector(6 downto 0);
	dr, sa, sb : out std_logic_vector(2 downto 0) );
end instruction_register;

architecture behavioral of instruction_register is
begin
	process(clock, reset) begin
		if reset = '1' then
			opcode <= "0000000" after 1ns;
			dr <= "000" after 1ns;
			sa <= "000" after 1ns;
			sb <= "000" after 1ns;
		
		elsif clock'event and clock = '1' then
			if il = '1' then
				opcode <= input(15 downto 9) after 1ns;
				dr <= input(8 downto 6) after 1ns;
				sa <= input(5 downto 3) after 1ns;
				sb <= input(2 downto 0) after 1ns;
			end if;
		end if;
	end process;
end behavioral;

