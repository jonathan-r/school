 /*
 * Objects of this class represent Red/Black Tree nodes.
 */
public class TreeNode<Key extends Comparable<Key>, Value> {
	public Key key;
	public Value val;
	public int size; //the number of decendents + self
	public boolean red; //type of node
	public TreeNode<Key, Value> left, right; //if both are null, both are black leaves

	public TreeNode(Key k, Value v){
		key = k;
		val = v;
		red = true; //actual leaves are always red
		size = 1; //itself
		left = right = null;
	}

	//invalidates size
	public TreeNode<Key, Value> rotateLeft(){
		TreeNode<Key, Value> x = right;
		right = x.left;
		x.left = this;
		x.red = red;
		red = true;
		return x;
	}

	//invalidates size
	public TreeNode<Key, Value> rotateRight(){
		TreeNode<Key, Value> x = left;
		left = x.right;
		x.right = this;
		x.red = red;
		red = true;
		return x;
	}

	public void flipColors(){
		red = !red;
		left.red = !left.red;
		right.red = !right.red;
	}

	public TreeNode<Key, Value> insert(Key k, Value v){
		//flip colors when both links are red
		if (left != null && right != null && left.red && right.red) flipColors();

		int cmp = k.compareTo(key);
		if (cmp == 0){
			val = v; //update the value
			return this;
		}
		else{
			if (cmp < 0) {
				if (left != null) left = left.insert(k, v);
				else left = new TreeNode<Key, Value>(k, v);
			}

			else {
				if (right != null) right = right.insert(k, v);
				else right = new TreeNode<Key, Value>(k, v);
			}
		}

		TreeNode<Key, Value> ret = this;
		if (right != null && right.red && (left == null || left != null && !left.red)) ret = ret.rotateLeft();
		if (left != null && left.red && left.left != null && left.left.red) ret = ret.rotateRight();

		//recompute size
		size = 1; //self
		if (left != null) size += left.size; //left subtree
		if (right != null) size += right.size; //right subtree

		return ret;
	}

	//time : O(height)
	//space : O(height)
	public TreeNode<Key, Value> find(Key k){
		int cmp = k.compareTo(key);
		if (cmp == 0) return this;
		else if (cmp < 0 && left != null) return left.find(k);
		else if (cmp > 0 && right != null) return right.find(k);
		else return null;
	}

	//time : O(height)
	//space : O(1)
	public TreeNode<Key, Value> getMin(){
		if (left == null) return this;
		else return left.getMin();
	}

	//time : O(height)
	//space : O(1)
	public TreeNode<Key, Value> getMax(){
		if (right == null) return this;
		else return right.getMax();
	}

	//time : O(n)
	//space : O(height)
	public int height(){
		int h = 1;
		if (left != null) h += left.height(); // >1
		if (right != null) h = Math.max(h, right.height() + 1);
		return h;
	}

	private boolean isBSTInner(Comparable[] lastKey){
		//left
		if (left != null) if (!left.isBSTInner(lastKey)) return false;

		//self
		if (lastKey[0] == null) lastKey[0] = key;
		else{
			if (((Key)lastKey[0]).compareTo(key) >= 0) return false;
			lastKey[0] = key;	
		} 

		//right
		if (right != null) if (!right.isBSTInner(lastKey)) return false;

		return true;
	}

	//time : O(n)
	//aux space : O(1)
	public boolean isBST(){
		Comparable[] lastKey = new Comparable[1]; //a mutable container for a single Key
		lastKey[0] = null;
		return isBSTInner(lastKey);
	}

	//returns the number of black child nodes (excluding leaves)
	//time : O(n)
	//aux space : none
	private int isRBTInner(){
		if (red){
			if (left == null && right == null) return 0; //black leaves
			else if (left != null && right != null){
				if (left.red || right.red) return -1;
			}
			else return -1;
		}

		if (left != null && right != null){
			if (left.red) return -1; //red left link

			int nodesLeft = left.isRBTInner();
			int nodesRight = right.isRBTInner();

			if (nodesLeft != nodesRight) return -1;
			else if (nodesLeft == -1) return -1;
			else {
				if (red) return nodesLeft;
				else return nodesLeft + 1; //include self
			}
		}

		return (red)? 0 : 1;
	}

	public boolean isRBT(){
		return (isRBTInner() >= 0);
	}
}
