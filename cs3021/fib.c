
int fib(int n){
	int fi, fj, t;
	if (n <= 1)
		return n;

	fi = 0;
	fj = 1;
	while (n > 1){
		t = fj;
		fj = fi + fj;
		fi = t;
		n--;
	}

	return fj;
}

