
#include "maths/util.h"

#include "terrain.h"


/*void Terrain::allocate(int triangleCount) {
	if (triangleCount > vertStorage * 2) {
		vertStorage = triangleCount / 2; //3 verts per triangle, but each vert shared between 6 triangles
		indStorage = triangleCount * 3; //3 vert references per triangle
		points = (vertex*) realloc(points, vertStorage * sizeof(vertex));
		indices = (uint32_t*) realloc(indices, indStorage * sizeof(uint32_t));
	}
}*/

float Terrain::sampleHeight(const vec2& p) {
	const int octaves = 5;
	const float persistence = 0.6f;

	float s = 0.0f;
	float div = 1.0f;

	for (int oi = 0; oi < octaves; oi++) {
		s += noise.noise2(p * (1 << oi) * 0.1f) * div;
		div *= persistence;
	}

	//float s = noise.noise2Distorted(p * 0.05f, 15.0f);
	return s * 10.0f;
}

bool Terrain::testPoint(const vec2& p) {
	vec3 p3(p.x, sampleHeight(p), p.y);
	float dist;
	if (clip.test(p3, dist)) {
		return true;
	}
	return false;
}

void Terrain::splitTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se) {
	//compute cross
	/*bool nwv = testPoint(nw);
	bool nev = testPoint(ne);
	bool swv = testPoint(sw);
	bool sev = testPoint(se);*/
	vec2 c = (nw + se) / 2;
	/*bool cv = testPoint(c);*/

	vec3 pc(c.x, sampleHeight(c), c.y);
	float diag = (ne - sw).length();
	float dist = (pc - eye).length();
	float factor = diag / dist;
	
	bool visible = clip.testSphere(pc, diag/2.3);

	if (at.isLeaf() and at.level < TERRAIN_MAX_DEPTH) {
		//if ((at.level < 2) or (nwv or nev or swv or sev or cv)) {
		if (visible and factor > TERRAIN_SPLIT_RATIO) {
			at.split(); leafCount += 3; //lose current, gain 4 new
			topologyChanged = true;
		}
	}

	if (!at.isLeaf()) {
		if (!visible or factor < TERRAIN_SPLIT_RATIO) {
			//after flattening, there is one leaf gained
			leafCount -= at.flatten() - 1; 
			topologyChanged = true;
		}
		else {
			//traverse down
			vec2 n = (nw + ne) / 2;
			vec2 e = (ne + se) / 2;
			vec2 s = (sw + se) / 2;
			vec2 w = (nw + sw) / 2;

			splitTraverse(at.ch[0], nw, n, w, c);
			splitTraverse(at.ch[1], n, ne, c, e);
			splitTraverse(at.ch[2], w, c, sw, s);
			splitTraverse(at.ch[3], c, e, s, se);
		}
	}
}

void Terrain::stitchTraverse(node& at, node* nn, node* ee, node* ss, node* ww, int maxDepth, int pos) {
	travDepth++; //debug

	/*if (nn) assert(nn->level == at.level or nn->level == at.level-1);
	if (ee) assert(ee->level == at.level or ee->level == at.level-1);
	if (ss) assert(ss->level == at.level or ss->level == at.level-1);
	if (ww) assert(ww->level == at.level or ww->level == at.level-1);*/

	//must also split any neighbours that are more than 1 level away
	if (nn and nn->isLeaf() and (nn->level < at.level-1)) {
		//assert(nn->level == at.level-1);
		assert(pos == 0 or pos == 1); //we're a northern cell
		nn->split(); leafCount += 3; topologyChanged = true;
		nn = (pos == 2)? &nn->ch[2] : &nn->ch[3];
	}

	if (ee and ee->isLeaf() and (ee->level < at.level-1)) {
		//assert(ee->level == at.level-1);
		assert(pos == 1 or pos == 3); //we're an eastern cell
		ee->split(); leafCount += 3; topologyChanged = true;
		ee = (pos == 1)? &ee->ch[0] : &ee->ch[2];
	}

	if (ss and ss->isLeaf() and (ss->level < at.level-1)) {
		//assert(ss->level == at.level-1);
		assert(pos == 2 or pos == 3); //we're a southern cell
		ss->split(); leafCount += 3; topologyChanged = true;
		ss = (pos == 2)? &ss->ch[0] : &ss->ch[1];
	}

	if (ww and ww->isLeaf() and (ww->level < at.level-1)) {
		//assert(ww->level == at.level-1);
		assert(pos == 0 or pos == 2); //we're a western cell
		ww->split(); leafCount += 3; topologyChanged = true;
		ww = (pos == 0)? &ww->ch[1] : &ww->ch[3];
	}

	if (!at.isLeaf() and at.level < maxDepth) {
		//traverse down
		node *nnn=nn, *eee=ee, *sss=ss, *www=ww;

		if (nn) if (!nn->isLeaf()) nnn = &nn->ch[2];
		if (ww) if (!ww->isLeaf()) www = &ww->ch[1];
		stitchTraverse(at.ch[0], nnn, &at.ch[1], &at.ch[2], www, maxDepth, 0);

		if (nn) if (!nn->isLeaf()) nnn = &nn->ch[3];
		if (ee) if (!ee->isLeaf()) eee = &ee->ch[0];
		stitchTraverse(at.ch[1], nnn, eee, &at.ch[3], &at.ch[0], maxDepth, 1);

		if (ww) if (!ww->isLeaf()) www = &ww->ch[3];
		if (ss) if (!ss->isLeaf()) sss = &ss->ch[0];
		stitchTraverse(at.ch[2], &at.ch[0], &at.ch[3], sss, www, maxDepth, 2);

		if (ee) if (!ee->isLeaf()) eee = &ee->ch[2];
		if (ss) if (!ss->isLeaf()) sss = &ss->ch[1];
		stitchTraverse(at.ch[3], &at.ch[1], eee, sss, &at.ch[2], maxDepth, 3);
	}
	travDepth--; //debug
}

int Terrain::addVert(const vec2& p) {
	if (vertUsed == vertStorage) {
		vertStorage *= 2;
		points = (vertex*)realloc(points, vertStorage * sizeof(vertex));
	}
	int id = vertUsed++;
	vec3& p3 = points[id].v;
	p3 = vec3(p.x, sampleHeight(p), p.y);

	float dp = samplingSize.x / ((1 << travDepth) * TERRAIN_LEAF_ACROSS);

	//FIXME: really expensive normal
	/*vec3 p3a(p.x +   dp, sampleHeight(p + vec2(dp, 0.0f)), p.y + 0.0f);
	vec3 p3b(p.x + 0.0f, sampleHeight(p + vec2(0.0f, dp)), p.y +   dp);
	points[id].n = -((p3 - p3a).normal() ^ (p3 - p3b).normal()).normal();*/

	points[id].n.set(0.0f, 0.0f);
	points[id].t.set(p.x / samplingSize.x + 0.5f, p.y / samplingSize.y + 0.5f);
	return id;
}

int Terrain::addIndex(uint32_t ind) {
	if (indUsed == indStorage) {
		indStorage *= 2;
		indices = (uint32_t*) realloc(indices, indStorage * sizeof(uint32_t));
	}
	indices[indUsed++] = ind;
}

void Terrain::configTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se,
							 node* nn, node* ee, node* ss, node* ww, 
							 /*int nwi, int nei, int swi, int sei*/) {
	travDepth++; //debug

	/* TODO: this fails sometimes
	 * if (nn) assert(nn->level == at.level or nn->level == at.level-1);
	if (ee) assert(ee->level == at.level or ee->level == at.level-1);
	if (ss) assert(ss->level == at.level or ss->level == at.level-1);
	if (ww) assert(ww->level == at.level or ww->level == at.level-1);*/

	//TODO: figure out vertex indexing, with shared vertices

	if (at.isLeaf()) {
		int dn = 0, de = 0, ds = 0, dw = 0; //lack of neighbour treated like same level
		if (nn) dn = at.level != nn->level;
		if (ee) de = at.level != ee->level;
		if (ss) ds = at.level != ss->level;
		if (ww) dw = at.level != ww->level;

		//add mesh of points
		for (int j = 0; j <= TERRAIN_LEAF_ACROSS; j++) {
			float y1 = lerp(nw.y, sw.y, (float)(j) / TERRAIN_LEAF_ACROSS);
			rowStart[j] = vertUsed; //first vertex added in the next loop

			int stride = 1 + ((j == 0 and dn) or (j == TERRAIN_LEAF_ACROSS and ds));
			int start = dw and ((j & 1) == 0);
			int end = TERRAIN_LEAF_ACROSS - de;

			for (int i = 0; i <= TERRAIN_LEAF_ACROSS; i += stride) {
				float x1 = lerp(nw.x, ne.x, (float)(i) / TERRAIN_LEAF_ACROSS);
				addVert(vec2(x1, y1));
			}
		}

		//top and bottom row get special treatment

		//north row
		for (int i = 1; i < TERRAIN_LEAF_ACROSS; i += 2) {
			addIndex(rowStart[1] + i - dw); //c
			addIndex(rowStart[0] + ((i - 1) >> dn)); //nw
			if (!dn) addIndex(rowStart[0] + i); //n
			addIndex(rowStart[0] + ((i + 1) >> dn)); //ne
			if (!(de and i == TERRAIN_LEAF_ACROSS-1)) addIndex(rowStart[1] + i + 1 - dw); //e
			addIndex(rowStart[2] + i + 1); //se
			addIndex(rowStart[2] + i); //s
			addIndex(rowStart[2] + i - 1); //sw
			if (!(dw and i == 1)) addIndex(rowStart[1] + i - 1); //w
			addIndex(PRIMITIVE_RESTART_INDEX);
		}

		//south row
		for (int i = 1; i < TERRAIN_LEAF_ACROSS; i += 2) {
			addIndex(rowStart[TERRAIN_LEAF_ACROSS-1] + i - dw); //c
			addIndex(rowStart[TERRAIN_LEAF_ACROSS-2] + (i - 1)); //nw
			addIndex(rowStart[TERRAIN_LEAF_ACROSS-2] + (i + 1)); //ne
			if (!(de and i == TERRAIN_LEAF_ACROSS-1)) addIndex(rowStart[TERRAIN_LEAF_ACROSS-1] + i + 1 - dw); //e
			addIndex(rowStart[TERRAIN_LEAF_ACROSS] + i + 1); //se
			if (!ds) addIndex(rowStart[TERRAIN_LEAF_ACROSS] + i); //s
			addIndex(rowStart[TERRAIN_LEAF_ACROSS] + i - 1); //sw
			if (!(dw and i == 1)) addIndex(rowStart[TERRAIN_LEAF_ACROSS-1] + i - 1); //w
			addIndex(PRIMITIVE_RESTART_INDEX);
		}

		//other indices
		for (int j = 3; j < TERRAIN_LEAF_ACROSS-2; j += 2) {
			uint32_t r0 = rowStart[j-1];
			uint32_t r1 = rowStart[j];
			uint32_t r2 = rowStart[j+1];

			//north row
			for (int i = 1; i < TERRAIN_LEAF_ACROSS; i += 2) {
				addIndex(rowStart[j] + i - dw);
				uint32_t ci = r1 + i - dw;

				addIndex(ci);

				addIndex(r0 + (i >> (j==1 and dn)) - 1); //nw
				if (!(dn and j == 1)) addIndex(r0 + i); //n


				addIndex(PRIMITIVE_RESTART_INDEX);
			}
		}
	}
	else {
		//traverse down
		vec2 n = (nw + ne) / 2;/* int ni = addVert(n);*/
		vec2 e = (ne + se) / 2;/* int ei = addVert(e);*/
		vec2 s = (sw + se) / 2;/* int si = addVert(s);*/
		vec2 w = (nw + sw) / 2;/* int wi = addVert(w);*/
		vec2 c = (nw + se) / 2;/* int ci = addVert(c);*/

		node *nnn=nn, *eee=ee, *sss=ss, *www=ww;

		if (nn) if (!nn->isLeaf()) nnn = &nn->ch[2];
		if (ww) if (!ww->isLeaf()) www = &ww->ch[1];
		configTraverse(at.ch[0], nw, n, w, c, nnn, &at.ch[1], &at.ch[2], www/*, nwi, ni, wi, ci*/);

		if (nn) if (!nn->isLeaf()) nnn = &nn->ch[3];
		if (ee) if (!ee->isLeaf()) eee = &ee->ch[0];
		configTraverse(at.ch[1], n, ne, c, e, nnn, eee, &at.ch[3], &at.ch[0]/*, ni, nei, ci, ei*/);

		if (ww) if (!ww->isLeaf()) www = &ww->ch[3];
		if (ss) if (!ss->isLeaf()) sss = &ss->ch[0];
		configTraverse(at.ch[2], w, c, sw, s, &at.ch[0], &at.ch[3], sss, www/*, wi, ci, swi, si*/);

		if (ee) if (!ee->isLeaf()) eee = &ee->ch[2];
		if (ss) if (!ss->isLeaf()) sss = &ss->ch[1];
		configTraverse(at.ch[3], c, e, s, se, &at.ch[1], eee, sss, &at.ch[2]/*, ci, ei, si, sei*/);
	}
	travDepth--; //debug
}

void Terrain::reprTraverse(node& at, const vec2& nw, const vec2& ne, const vec2& sw, const vec2& se, int pos) {
	travDepth++;

	if (at.isLeaf()) {
		//still a leaf
		vec2 x(nw.x / samplingSize.x, nw.y / samplingSize.y);
		vec2 y(ne.x / samplingSize.x, ne.y / samplingSize.y);
		vec2 z(sw.x / samplingSize.x, sw.y / samplingSize.y);
		vec2 w(se.x / samplingSize.x, se.y / samplingSize.y);

		/*float cr = ((at.level * 7)  % 31) / 31.0f;
		float cg = ((at.level * 11) % 19) / 19.0f;
		float cb = ((at.level * 5)  % 13) / 13.0f;
		vec3(cr, cg, cb)*/

		vec3 col = reprCol[pos] / (1 << travDepth);

		vertex_ui a(x, x, col);
		vertex_ui b(y, y, col);
		vertex_ui c(z, z, col);
		vertex_ui d(w, w, col);

		debugVerts[uiVertsUsed++] = a;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = c;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = c;

		/*a.v = x; a.t = x; a.c.set(cr, cg, cb);
		b.v = y; b.t = y; b.c.set(cr, cg, cb);
		c.v = w; c.t = w; c.c.set(cr, cg, cb);
		d.v = x; d.t = x; d.c.set(cr, cg, cb);
		e.v = w; e.t = w; e.c.set(cr, cg, cb);
		f.v = z; f.t = z; f.c.set(cr, cg, cb);*/

		/*debugVerts[uiVertsUsed++] = a;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = c;

		debugVerts[uiVertsUsed++] = a;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = b;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = d;
		debugVerts[uiVertsUsed++] = c;
		debugVerts[uiVertsUsed++] = c;
		debugVerts[uiVertsUsed++] = a;*/
	}
	else {
		//traverse down
		vec2 n = (nw + ne) / 2;
		vec2 e = (ne + se) / 2;
		vec2 s = (sw + se) / 2;
		vec2 w = (nw + sw) / 2;
		vec2 c = (nw + se) / 2;

		reprTraverse(at.ch[0], nw, n, w, c, 0);
		reprTraverse(at.ch[1], n, ne, c, e, 1);
		reprTraverse(at.ch[2], w, c, sw, s, 2);
		reprTraverse(at.ch[3], c, e, s, se, 3);
	}

	travDepth--;
}

void Terrain::update(const vec3& eye3, const vec3& target3, const vec3& up3, bool drawLines) {
	eye = eye3;
	clip.update(eye3, target3, up3);

	/*//project frustum onto XZ plane
	eye.set(eye3.x, eye3.z);
	vec2 viewDir = (vec2(target3.x, target3.z) - eye).normal();
	float heading = atan2(viewDir.z, viewDir.y);
	float pitch = atan2(viewDir.y, viewDir.z);
	dfl = vec2(cos(heading-fovx/2), sin(heading-fovx/2));
	dfr = vec2(cos(heading+fovx/2), sin(heading+fovx/2));*/

	//TODO: traverse quadtree
	root.markAllInvisible();
	topologyChanged = false;

	vec2 nw(-samplingSize.x/2,  samplingSize.y/2);
	vec2 ne( samplingSize.x/2,  samplingSize.y/2);
	vec2 sw(-samplingSize.x/2, -samplingSize.y/2);
	vec2 se( samplingSize.x/2, -samplingSize.y/2);

	splitTraverse(root, nw, ne, sw, se);
	printf("terrain: %d leaves after split\n", leafCount);

	if (topologyChanged) {
		for (int i = 1; i <= TERRAIN_MAX_DEPTH; i++) {
			stitchTraverse(root,  nullptr, nullptr, nullptr, nullptr, i, 0);
		}

		printf("terrain: %d leaves after stitch\n", leafCount);

		//generate actual mesh
		vertUsed = 0;
		indUsed = 0;
		travDepth = 0;
		configTraverse(root, nw, ne, sw, se, nullptr, nullptr, nullptr, nullptr);
		printf("generated %d verts, %d elements\n", vertUsed, indUsed);
	}

	//DEBUG: just print out the quads
	debugVerts = (vertex_ui*) realloc(debugVerts, (leafCount * 12 + 2) * sizeof(vertex_ui));
	uiVertsUsed = 0;
	reprTraverse(root, nw, ne, sw, se, 0);
		
	//camera position - TOOD: project frustum onto terrain
	vec2 eye2 = vec2(eye.x / samplingSize.x, eye.z / samplingSize.y);
	vec2 dir2 = vec2(target3.x - eye.x, target3.z - eye.z).normal();
	debugVerts[uiVertsUsed++] = vertex_ui(eye2, vec2(), vec3(1.0f, 1.0f, 1.0f));
	debugVerts[uiVertsUsed++] = vertex_ui(eye2 + dir2, vec2(), vec3(1.0f, 1.0f, 1.0f));
	
	//assert(uiVertsUsed == leafCount * 12);
	overlay.assign(uiVertsUsed, debugVerts, 0, nullptr, GL_DYNAMIC_DRAW);
	
	//dummy generate mesh
	/*const int s = 100;
	vertUsed = 0;
	indUsed = 0;
	const float sizeOut = 10.0f;

	//verts
	for (int j = 0; j < s; j++) {
		for (int i = 0; i < s; i++) {
			vec2 po((float)i / s, (float)j / s);
			vec2 p(po.x / samplingSize.x, po.y / samplingSize.y);

			vertex& v = points[vertUsed++];
			v.v.set(po.x * sizeOut - sizeOut/2, sampleHeight(p) * sizeOut, po.y * sizeOut - sizeOut/2);
			v.n.set(0, 0, 0);
			v.ta.set(0, 0, 0); //not used yet
			v.t.set(po.x, po.y);
		}
	}

	//elements
	for (int j = 0; j < s-1; j++) {
		for (int i = 0; i < s-1; i++) {
			// a b
			// c d

			int ai = (j+0) * s + (i+0);  vertex& a = points[ai];
			int bi = (j+0) * s + (i+1);  vertex& b = points[bi];
			int ci = (j+1) * s + (i+0);  vertex& c = points[ci];
			int di = (j+1) * s + (i+1);  vertex& d = points[di];

			//compute normals
			vec3 n1 = ((a.v - b.v).normal() ^ (a.v - c.v).normal()).normal();
			vec3 n2 = ((d.v - b.v).normal() ^ (d.v - c.v).normal()).normal();

			a.n += n1; 
			b.n += n1; b.n += n2;
			c.n += n1; c.n += n2;
			d.n += n2;

			//generate indices
			if (drawLines) {
				indices[indUsed++] = bi;
				indices[indUsed++] = ai;

				indices[indUsed++] = ai;
				indices[indUsed++] = ci;

				indices[indUsed++] = ci;
				indices[indUsed++] = bi;

				indices[indUsed++] = ci;
				indices[indUsed++] = di;

				indices[indUsed++] = di;
				indices[indUsed++] = bi;
			}
			else {
				indices[indUsed++] = ci;
				indices[indUsed++] = bi;
				indices[indUsed++] = ai;

				indices[indUsed++] = ci;
				indices[indUsed++] = di;
				indices[indUsed++] = bi;
			}
		}
	}

	//normalize/smooth normals
	for (int i = 0; i < vertUsed; i++) {
		points[i].n.normalize();
	}*/

	vbo.assign(vertUsed, points, indUsed, indices, GL_DYNAMIC_DRAW);
	object.drawMode = (drawLines)? GL_LINES : GL_TRIANGLES;
	object.bakeAll();
}

