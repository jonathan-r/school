import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Sender {
	static final int DEFAULT_DST_PORT = 50001;
	static final String DEFAULT_HOST = "localhost";

	private DatagramSocket socket;
	private InetAddress sendToAddress;
	private int sendToPort;
	
	private byte[] data = null;
	private int lengthBits = 0;

	private boolean good = false;

	public Sender() {
		this(DEFAULT_HOST, DEFAULT_DST_PORT);
	}


	public Sender(String host, int port) {
		try {
			socket = new DatagramSocket();
			sendToAddress = InetAddress.getByName(host);
			sendToPort = port;
			good = true;
		} 
		
		catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}

	public void getData() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a bit string:");
		String line = br.readLine();

		try {
			data = Bits.toBytes(line);
			lengthBits = line.length();
		} 
		
		catch (Bits.BitStringException e) {
			System.out.println("Invalid string");
			data = null;
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void send() {
		DatagramPacket packet = null;

		try {
			//create a new buffer so that the crc can be appended to the messege
			byte[] sendData = new byte[data.length + 1];
			for (int i = 0; i < data.length; i++) sendData[i] = data[i];
			
			//compute the crc
			int crc = CRC.computeCRC(sendData, sendData.length, CRC.defaultPolynomial);
			
			//place it at the end ofthe array
			sendData[data.length] = (byte) crc;

			System.out.print("\nSending packet: ");
			for (int i = 0; i < sendData.length; i++){
				System.out.print(Bits.toBitString(sendData[i], 8));
			}
			System.out.println();
							
			packet = new DatagramPacket(sendData, sendData.length, sendToAddress, sendToPort);
			socket.send(packet);
			System.out.println("Packet sent");
		} 
		catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isGood(){
		return good;
	}
	
	public boolean isDataGood(){
		return data != null;
	}

	public static void main(String[] args) {
		Sender s;
		try {
			if (args.length == 2) {
				String dstHost = args[0];
				int sendToPort = Integer.parseInt(args[1]);
				s = new Sender(dstHost, sendToPort);
			}

			else if (args.length == 0){
				s = new Sender();
			}
			
			else{
				System.out.println("Usage: sender <destination host> <destination port>");
				return;
			}
			
			if (s.isGood()){
				s.getData();
				if (s.isDataGood()){
					s.send();
				}
			}
			System.out.println("Program completed");
		}

		catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}
}
