library ieee;
use ieee.std_logic_1164.all;

entity pc_reg is port (
	load, increment : in std_logic;
	offset : in std_logic_vector(15 downto 0);
	clock : in std_logic;
	output : inout std_logic_vector(15 downto 0) );
end pc_reg;

architecture behavioral of pc_reg is
	component full_adder is port(
		carry_in, a, b : in   std_logic;
		sum, carry_out : out  std_logic);
	end component;
	
	signal adder_b : std_logic_vector(15 downto 0);
	signal adder_carry : std_logic_vector(16 downto 0); --carry out is not used
	signal adder_sum : std_logic_vector(15 downto 0);
	
begin	
	--each clock, pc = pc + increment * 1 + load * offset
	adder_carry(0) <= increment;
	adder_b <= offset when load = '1' else x"0000";
	
	gen_adder : 
	for i in 0 to 15 generate
		adder : full_adder port map(output(i), adder_b(i), adder_carry(i), adder_sum(i), adder_carry(i + 1));
	end generate gen_adder;
	
	--load new address into register on every clock
	process(clock) begin
		if (clock'event and clock = '1') then
			if (load = '1') then
				output <= adder_sum;
			end if;
		end if;
	end process;
end behavioral;

