#include "pages.h"

using namespace std;
using namespace cv;

//text drawing
void drawText(Mat& on, const Point& p, const char* fmt, ...) {
	static char text[256];

	const Scalar textCol(255, 255, 255);
	const Scalar textBgCol(0, 0, 0);

	va_list args;
	va_start (args, fmt);
	vsprintf(text, fmt, args);
	putText(on, text, p, FONT_HERSHEY_PLAIN, 1.0, textBgCol, 3, CV_AA, false);
	putText(on, text, p, FONT_HERSHEY_PLAIN, 1.0, textCol, 1, CV_AA, false);
	va_end (args);
}

int getGroundTruth(int idx) {
	const int actualMapping[] = {
		-1, //skip 0
		1,2,3,4,5,6,7,8,9,10,11,12,13,2,3,5,4,7,9,8,7,11,13,12,2, //given
		1,3,3,4,9,9,10,12,13,1,9,6,8,8,5,7,6,9,8,10,11,3,4,5,13 //extra
	};

	if (idx > 0 and idx <= 50) return actualMapping[idx];
	else return 0;
}

void loadTemplates(NameTemplate& namePattern, vector<Mat>& cannied) {
	//load all the reference images, scaled
	int refSeq = 1;
	string refName = namePattern.makeImgName(refSeq);
	do {
		Mat refSrc = imread(refName);
		if (!refSrc.data) break;

		Mat refCropped, refScaled, refEdge;
		cvtColor(refSrc, refSrc, CV_BGR2GRAY);

		const float cropFraction = 0.03f;
		refCropped = refSrc(Rect(refSrc.cols * cropFraction, refSrc.rows * cropFraction, refSrc.cols * (1.0f - 2*cropFraction), refSrc.rows * (1.0f - 2*cropFraction)));
		resize(refCropped, refScaled, Size(CMP_SIZE * (1.0f - cropFraction/2), CMP_SIZE * (1.0f - cropFraction/2)), 0, 0, INTER_LANCZOS4);

		Canny(refScaled, refEdge, CANNY_LOW, CANNY_HIGH);
		cannied.emplace_back(refEdge);

		refName = namePattern.makeImgName(++refSeq);
	} while(namePattern.isSequence());
}

bool matchPage(const Mat& sourceImage, const vector<Mat>& referenceImages, const Mat& bluePixels, int& indexMatched, Mat* debugImg) {
	Mat transformedPage;
	if (extractPage(bluePixels, 127, 1.0, 4, 4, sourceImage, CMP_SIZE, transformedPage, debugImg)) {
		//Canny edge detection
		Mat pageEdge;
		cvtColor(transformedPage, pageEdge, CV_BGR2GRAY);
		Canny(pageEdge, pageEdge, CANNY_LOW, CANNY_HIGH);

		int bestMatchIdx = -1;
		double bestMatchVal = numeric_limits<double>::min();
		Point matchStart, matchEnd;
		Mat bestMatch;
		Mat result;

		//try every 90 degree rotated orientation
		for (int si = 0; si < 4; si++) {
			double rotAngle = (double)si * 90.0;
			Mat cmpImg;

			if (si == 0) {
				cmpImg = pageEdge.clone();
			}
			else {
				Mat rotMat = getRotationMatrix2D(Point2f(CMP_SIZE/2.0f, CMP_SIZE/2.0f), rotAngle, 1.0);
				warpAffine(pageEdge, cmpImg, rotMat, pageEdge.size());
			}

			for (int ti = 0; ti < 2; ti++) {
				//also try to transpose the image - sometimes necessary
				if (ti == 1) transpose(cmpImg, cmpImg);

				//go through all the comparison images
				for (int i = 0; i < referenceImages.size(); i++) {
					double minVal, maxVal; Point minLoc, maxLoc;
					const Mat& refImg = referenceImages[i];

					//match cropped reference images into the test image domain
					matchTemplate(cmpImg, refImg, result, CV_TM_CCOEFF);
					minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
					
					if (maxVal > bestMatchVal) {
						bestMatchIdx = i;
						bestMatchVal = maxVal;
						matchStart = maxLoc;
						matchEnd = maxLoc + Point(refImg.cols, refImg.rows);
						bestMatch = cmpImg.clone();
						//printf("rot %d, better match against %d, max %.2f, at (%d, %d)\n", si, i+1, maxVal, maxLoc.x, maxLoc.y);
					}
				}
			}
		}
		if (bestMatchIdx != -1) {
			//printf("%d matches ref %d best (actual %d)\n", trackImgId, bestMatchIdx + 1, actualMapping[trackImgId]);
			indexMatched = bestMatchIdx;
			return true;
		}
		else {
			//printf("could not find any matches\n");
			return false;
		}
	}
	
	//printf("couldn't extract page\n");
	return false;
}
