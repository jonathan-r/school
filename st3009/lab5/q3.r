library(boot);

data <- read.csv(file = "lab4times.csv", header = F)[,];
N <- length(data)

meanboot <- function(x, indices) {
  mean(x[indices]);
}

bootres <- boot(data = data, statistic = meanboot, R = 10000)
boot.ci(bootres, type = "perc")
