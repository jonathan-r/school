#libraries
import socket, base64

#internal
from base import Connection

#very very synchronous
class Proxy:
	class NetworkFile:
		#implicit open
		def __init__(self, fn, mode):
			self.fn = fn
			self.mode = mode

		def close(self):
			self.conn.writeLine("CLOSE_FILE:%s" % self.fn)
			assert self.conn.getLine("FILE_CLOSED") == self.fn

		def read(self, at, amount):
			self.conn.writeLine("READ_FILE:%s,%d,%s" % (self.fn, at, amount))
			resp, data = self.conn.getLine("FILE_READ")
			return bytes(data)

		def write(self, from, bytes, data):
			dataEncoded = base64.b64encode(data).decode('ascii')
			self.conn.writeLine("READ_FILE:%s,%d,%s" % (self.fn, from, dataEncoded))
			resp = self.conn.getLine("FILE_READ")

		def get_size():
			pass

	def __init__(self, addr, port):
		self.files_open = []
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((addr, port)) #can raise timeout or interrupt

		self.conn = Connection(self.sock)

	def open_file(self, path, mode):
		pass

	def list_dir(self, path):
		pass