import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.HashMap;

import java.io.PrintWriter;

/**
 * This implements a Breadth First Search Maze Runner for any kind of mazes.  
 *
 * @author Jonathan Rosca
 */
public class BFSMazeRunner<MC extends MazeCell> extends MazeRunner<MC>  {
	/**
	 * Tries to find a solution to the given maze.
	 *
	 * @param maze      The maze to solve.
	 * @param writer    The printwriter on which to output the
	 *          maze solution.
	 */
	public void solveMaze(Maze<MC> maze, PrintWriter writer) {
		LinkedList<MC> queue = new LinkedList<MC>();
		MC start = maze.getStart();
		MC end = maze.getDonut();

		queue.add(start);
		int cellsExpanded = 0;
		boolean solved = false;

		writer.println("BFS");

		//while there are elements in the quese, the search is not over
		while (queue.size() > 0){
			MC cell = queue.remove();
			cell.setState(MazeCell.CellState.visitInProgress);
			cellsExpanded++;

			if (cell == end){
				solved = true;
				break;
			}

			//go through the neighbours
			Iterator<MC> it = maze.getNeighbors(cell);
			while (it.hasNext()){
				MC n = it.next();
				if (n.getState() == MazeCell.CellState.notVisited){
					//tell $n how we reached it, so we can reconstruct the path at the end
					n.setExtraInfo(cell); 
					
					//add to the end of the queue
					queue.add(n);

					//since $n won't get processed, prevent it from being redundantly added
					n.setState(MazeCell.CellState.visitInProgress);
				}
			}

			cell.setState(MazeCell.CellState.visited);
		}

		if (solved){
			LinkedList<MC> path = new LinkedList<MC>();

			MC trace = end;
			while (trace != null) {
				trace = (MC)trace.getExtraInfo();
				if (trace != null) path.addFirst(trace);
			}

			path.addLast(end);

			for (MC cell : path) {
				writer.print(cell + " ");
				cell.setState(MazeCell.CellState.solutionPath);
			}

			writer.println();
			writer.println(path.size());
			writer.println(cellsExpanded);
		}

		else{
			writer.println("no solution");
		}
	}
}
