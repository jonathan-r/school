
#include "grabber.h"

const char* GrabberStateStr[] = {
	"too far",
	"reaching",
	"reached",
	"idle"
};

void Grabber::update(float dt) {
	dt = 0.05f;

	GrabberState oldState = state;
	switch(state) {
		case GRABBER_TOO_FAR:
			state = GRABBER_IDLE;
			break;

		case GRABBER_REACHING: {
			assert(target);
			vec3 baseDiff = (*target) - pos();
			vec3 endDiff = (*target) - end();
			
			float baseDist = baseDiff.length();
			float endDist = endDiff.length();

			if (baseDist > armLen) {
				state = GRABBER_TOO_FAR;
				break;
			}
			
			if (endDist < 0.01f) {
				state = GRABBER_REACED;
				break;
			}

			/*//exact
			for (int i = 0; i < 100; i++) { //hard limit
				if (simulate(dt, true) < 0.05f) break;
			}*/
			
			//continuous
			for (int i = 0; i < 100; i++) {
				float sum = simulate(dt, true);
				if (sum < 0.1f) break;
			}
		}

		case GRABBER_REACED: {
			vec3 endDiff = (*target) - end();
			float endDist = endDiff.length();
			
			if (endDist > 0.01f) {
				state = GRABBER_REACHING;
				break;
			}
			
			break;
		}

		case GRABBER_IDLE: {
			simulate(dt, false);
			break;
		}

		default: 
			break;
	}

	if (state != oldState) {
		printf("switched state from %s to %s\n", GrabberStateStr[oldState], GrabberStateStr[state]);
	}
}

float Grabber::simulate(float dt, bool chase) {
	//update IK
	const float k = 30.0f; //spring linear strength
	const float rk = 40.0f; //spring bending strength
	const float mass = 10.0f;
	float forceSum = 0.0f;

	//reset forces
	for (int i = 0; i <= segs; i++) {
		joints[i].force.set(0, 0, 0);
	}

	if (chase) {
		//set to target directly
		joints[segs].point = *target;
		joints[segs].lastPoint = *target;
	}

	//apply force based on displacement
	for (int i = 1; i < (chase ? (segs-1) : segs); i++) {
		//spring force based on displacement: F(t) = -k*l(t)
		vec3 diff = joints[i+1].point - joints[i].point;
		vec3 force = diff.normal() * (-k * (segLen - diff.length()));
		joints[i].force += force * 0.5f; //half here
		joints[i+1].force -= force * 0.5f; //half here
	}

	//special case - first and last points
	{
		vec3 diff = joints[1].point - joints[0].point;
		vec3 force = diff.normal() * (-k * (segLen - diff.length()));
		joints[1].force -= force; //all here
	}
	if (chase) {
		vec3 diff = joints[segs].point - joints[segs-1].point;
		vec3 force = diff.normal() * (-k * (segLen - diff.length()));
		joints[segs-1].force += force; //all here
	}

	if (true and !chase) {
		//now apply force based on bending
		//printf("forces 2: ");
		vec3 lastDir(0, 1, 0); //normalized
		for (int i = 0; i < segs; i++) {
			joint& j0 = joints[i-1]; //don't use when i == 0
			joint& j1 = joints[i];
			joint& j2 = joints[i+1];

			vec3 dir = (j2.point - j1.point).normal();
			float dot = lastDir * dir;

			if (fabs(dot) < 0.999f) {
				vec3 side = (lastDir ^ dir).normal();
				vec3 rest = (dir ^ side).normal();
				float ang = acos(dot);
				j2.force += rest * (ang * rk);
				j1.force -= rest * (ang * rk);
				if (i>0) j0.force += rest * (ang * rk);
			}

			lastDir = dir;
		}
	}

	joints[0].force = 0.0f;
	if (chase) {
		joints[segs].force = 0.0f; //anchored
	}

	//advance
	for (int i = 1; i < (chase? segs : (segs+1)); i++) {
		joint& j = joints[i];
		vec3 acc = j.force / mass;
		vec3 nextPoint = (j.point * 0.07f) + (((j.point*2) - j.lastPoint) + (acc * (dt * dt))) * 0.93f; //dampen 7%

		j.lastPoint = j.point;
		j.point = nextPoint;
		forceSum += j.force.length();
	}

	vec3 lastDir(0, 1, 0); //normalized
	vec3 side;
	quat rotChain; //accumulated actual rotations, (r1 * r2 * r3 * ... * rn)
	quat prevRot; //accumulated inverse rotations
	quat rot;
	for (int i = 0; i < segs; i++) {
		joint& j1 = joints[i];
		joint& j2 = joints[i+1];

		vec3 dir = (j2.point - j1.point).normal();
		vec3 dirLocal = rotChain.rotate(dir);

		float dot = lastDir * dir;
		//float dot = vec3(0, 1, 0) * dirLocal;
		float ang = acos(dot);
		
		if (dot > 0.9999f) {
			rot = quat(); //identity
			side.set(1, 0, 0);
		}
		else {
			if (fabs(dot) < 0.001f) {
				side.set(1, 0, 0);
			}
			else {
				side = (lastDir ^ dir).normal();
				//side = (vec3(0, 1, 0) ^ dir).normal();
			}
			
			//side = (~prevRot).rotate(side);
			side = rotChain.rotate(side);
			rot = quat::axisRotation(side, ang);
		}
		
		//the rotation is described in global space, 
		//so it needs to be converted to local space
		//objPool[i].qt = prevRot * rot * (~prevRot);
		objPool[i].qt = rot;
		rotChain = (rotChain * rot).normal(); //add rotations to the right

		j1.tangent = side; //rotChain.rotate(vec3(1, 0, 0));

		lastDir = dir;
		prevRot = (prevRot * (~rot)).normal(); //objPool[i].qt;
		j1.rotTangent = side * ang;
	}
	
	rootObj().bakeAll();
	return forceSum;
}

