#include <stdio.h>
#include <stdlib.h>

extern void sort(int* array, int elems);

int main(){
	const int size = 30;
	int array[size];

	for (int i = 0; i < size; i++){
		array[i] = rand() % 100;
	}

	sort(array, size);

	for (int i = 0; i < size; i++){
		printf((i == 0)? "%d" : ", %d", array[i]);
	}
	printf("\n");

	return 0;
}

