import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Test {
	public static void runTest(Class c){
		Result result = JUnitCore.runClasses(c);

		if (result.getFailureCount() == 0){
			System.out.println("test passed");
		}
		else{
			for (Failure failure : result.getFailures()) {
				System.out.println(failure.toString());
			}
		}

	}

	public static void main(String[] args) {
		runTest(DoublyLinkedListTest.class);
		runTest(StackTest.class);
	}
} 

