/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x6dd86d03 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//tholos.scss.tcd.ie/ugrad/roscaj/cs2022/a1_final/mux8_16bit_tb.vhd";
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_180853171_1035706684(char *, char *, int , int );


static void work_a_0413944129_2372691052_p_0(char *t0)
{
    char t19[16];
    char *t1;
    char *t2;
    int64 t3;
    char *t4;
    int t5;
    int t6;
    char *t7;
    char *t8;
    int t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned char t20;

LAB0:    t1 = (t0 + 1504U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(27, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(29, ng0);
    t2 = (t0 + 3607);
    *((int *)t2) = 0;
    t4 = (t0 + 3611);
    *((int *)t4) = 7;
    t5 = 0;
    t6 = 7;

LAB8:    if (t5 <= t6)
        goto LAB9;

LAB11:    xsi_set_current_line(37, ng0);
    t2 = (t0 + 3635);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB25;

LAB26:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 0U, 16U, 0LL);
    xsi_set_current_line(38, ng0);
    t2 = (t0 + 3651);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB27;

LAB28:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 16U, 16U, 0LL);
    xsi_set_current_line(39, ng0);
    t2 = (t0 + 3667);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB29;

LAB30:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 32U, 16U, 0LL);
    xsi_set_current_line(40, ng0);
    t2 = (t0 + 3683);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB31;

LAB32:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 48U, 16U, 0LL);
    xsi_set_current_line(41, ng0);
    t2 = (t0 + 3699);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB33;

LAB34:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 64U, 16U, 0LL);
    xsi_set_current_line(42, ng0);
    t2 = (t0 + 3715);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB35;

LAB36:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 80U, 16U, 0LL);
    xsi_set_current_line(43, ng0);
    t2 = (t0 + 3731);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB37;

LAB38:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 96U, 16U, 0LL);
    xsi_set_current_line(44, ng0);
    t2 = (t0 + 3747);
    t20 = (16U != 16U);
    if (t20 == 1)
        goto LAB39;

LAB40:    t7 = (t0 + 1736);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 16U);
    xsi_driver_first_trans_delta(t7, 112U, 16U, 0LL);
    xsi_set_current_line(46, ng0);
    t2 = (t0 + 3763);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(47, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB43:    *((char **)t1) = &&LAB44;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(30, ng0);
    t7 = (t0 + 948U);
    t8 = *((char **)t7);
    t7 = (t0 + 3607);
    t9 = *((int *)t7);
    t10 = (t9 - 0);
    t11 = (t10 * 1);
    t12 = (16U * t11);
    t13 = (0U + t12);
    t14 = (t0 + 1736);
    t15 = (t14 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 40U);
    t18 = *((char **)t17);
    memcpy(t18, t8, 16U);
    xsi_driver_first_trans_delta(t14, t13, 16U, 0LL);
    xsi_set_current_line(31, ng0);
    t2 = (t0 + 3607);
    t4 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t19, *((int *)t2), 3);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t4, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(32, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 3607);
    t5 = *((int *)t2);
    t4 = (t0 + 3611);
    t6 = *((int *)t4);
    if (t5 == t6)
        goto LAB11;

LAB24:    t9 = (t5 + 1);
    t5 = t9;
    t7 = (t0 + 3607);
    *((int *)t7) = t5;
    goto LAB8;

LAB12:    xsi_set_current_line(33, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 948U);
    t7 = *((char **)t2);
    t20 = 1;
    if (16U == 16U)
        goto LAB18;

LAB19:    t20 = 0;

LAB20:    if (t20 == 0)
        goto LAB16;

LAB17:    goto LAB10;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    t14 = (t0 + 3615);
    xsi_report(t14, 19U, 2);
    goto LAB17;

LAB18:    t11 = 0;

LAB21:    if (t11 < 16U)
        goto LAB22;
    else
        goto LAB20;

LAB22:    t2 = (t4 + t11);
    t8 = (t7 + t11);
    if (*((unsigned char *)t2) != *((unsigned char *)t8))
        goto LAB19;

LAB23:    t11 = (t11 + 1);
    goto LAB21;

LAB25:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB26;

LAB27:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB28;

LAB29:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB30;

LAB31:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB32;

LAB33:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB34;

LAB35:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB36;

LAB37:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB38;

LAB39:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB40;

LAB41:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 3766);
    t20 = 1;
    if (16U == 16U)
        goto LAB47;

LAB48:    t20 = 0;

LAB49:    if (t20 == 0)
        goto LAB45;

LAB46:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 3802);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(51, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB55:    *((char **)t1) = &&LAB56;
    goto LAB1;

LAB42:    goto LAB41;

LAB44:    goto LAB42;

LAB45:    t15 = (t0 + 3782);
    xsi_report(t15, 19U, 2);
    goto LAB46;

LAB47:    t11 = 0;

LAB50:    if (t11 < 16U)
        goto LAB51;
    else
        goto LAB49;

LAB51:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB48;

LAB52:    t11 = (t11 + 1);
    goto LAB50;

LAB53:    xsi_set_current_line(52, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 3805);
    t20 = 1;
    if (16U == 16U)
        goto LAB59;

LAB60:    t20 = 0;

LAB61:    if (t20 == 0)
        goto LAB57;

LAB58:    xsi_set_current_line(54, ng0);
    t2 = (t0 + 3841);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(55, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB67:    *((char **)t1) = &&LAB68;
    goto LAB1;

LAB54:    goto LAB53;

LAB56:    goto LAB54;

LAB57:    t15 = (t0 + 3821);
    xsi_report(t15, 19U, 2);
    goto LAB58;

LAB59:    t11 = 0;

LAB62:    if (t11 < 16U)
        goto LAB63;
    else
        goto LAB61;

LAB63:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB60;

LAB64:    t11 = (t11 + 1);
    goto LAB62;

LAB65:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 3844);
    t20 = 1;
    if (16U == 16U)
        goto LAB71;

LAB72:    t20 = 0;

LAB73:    if (t20 == 0)
        goto LAB69;

LAB70:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 3880);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(59, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB79:    *((char **)t1) = &&LAB80;
    goto LAB1;

LAB66:    goto LAB65;

LAB68:    goto LAB66;

LAB69:    t15 = (t0 + 3860);
    xsi_report(t15, 19U, 2);
    goto LAB70;

LAB71:    t11 = 0;

LAB74:    if (t11 < 16U)
        goto LAB75;
    else
        goto LAB73;

LAB75:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB72;

LAB76:    t11 = (t11 + 1);
    goto LAB74;

LAB77:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 3883);
    t20 = 1;
    if (16U == 16U)
        goto LAB83;

LAB84:    t20 = 0;

LAB85:    if (t20 == 0)
        goto LAB81;

LAB82:    xsi_set_current_line(62, ng0);
    t2 = (t0 + 3919);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(63, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB91:    *((char **)t1) = &&LAB92;
    goto LAB1;

LAB78:    goto LAB77;

LAB80:    goto LAB78;

LAB81:    t15 = (t0 + 3899);
    xsi_report(t15, 19U, 2);
    goto LAB82;

LAB83:    t11 = 0;

LAB86:    if (t11 < 16U)
        goto LAB87;
    else
        goto LAB85;

LAB87:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB84;

LAB88:    t11 = (t11 + 1);
    goto LAB86;

LAB89:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 3922);
    t20 = 1;
    if (16U == 16U)
        goto LAB95;

LAB96:    t20 = 0;

LAB97:    if (t20 == 0)
        goto LAB93;

LAB94:    xsi_set_current_line(66, ng0);
    t2 = (t0 + 3958);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(67, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB103:    *((char **)t1) = &&LAB104;
    goto LAB1;

LAB90:    goto LAB89;

LAB92:    goto LAB90;

LAB93:    t15 = (t0 + 3938);
    xsi_report(t15, 19U, 2);
    goto LAB94;

LAB95:    t11 = 0;

LAB98:    if (t11 < 16U)
        goto LAB99;
    else
        goto LAB97;

LAB99:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB96;

LAB100:    t11 = (t11 + 1);
    goto LAB98;

LAB101:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 3961);
    t20 = 1;
    if (16U == 16U)
        goto LAB107;

LAB108:    t20 = 0;

LAB109:    if (t20 == 0)
        goto LAB105;

LAB106:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 3997);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(71, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB115:    *((char **)t1) = &&LAB116;
    goto LAB1;

LAB102:    goto LAB101;

LAB104:    goto LAB102;

LAB105:    t15 = (t0 + 3977);
    xsi_report(t15, 19U, 2);
    goto LAB106;

LAB107:    t11 = 0;

LAB110:    if (t11 < 16U)
        goto LAB111;
    else
        goto LAB109;

LAB111:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB108;

LAB112:    t11 = (t11 + 1);
    goto LAB110;

LAB113:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 4000);
    t20 = 1;
    if (16U == 16U)
        goto LAB119;

LAB120:    t20 = 0;

LAB121:    if (t20 == 0)
        goto LAB117;

LAB118:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 4036);
    t7 = (t0 + 1772);
    t8 = (t7 + 32U);
    t14 = *((char **)t8);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(75, ng0);
    t3 = (1 * 1000LL);
    t2 = (t0 + 1404);
    xsi_process_wait(t2, t3);

LAB127:    *((char **)t1) = &&LAB128;
    goto LAB1;

LAB114:    goto LAB113;

LAB116:    goto LAB114;

LAB117:    t15 = (t0 + 4016);
    xsi_report(t15, 19U, 2);
    goto LAB118;

LAB119:    t11 = 0;

LAB122:    if (t11 < 16U)
        goto LAB123;
    else
        goto LAB121;

LAB123:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB120;

LAB124:    t11 = (t11 + 1);
    goto LAB122;

LAB125:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 776U);
    t4 = *((char **)t2);
    t2 = (t0 + 4039);
    t20 = 1;
    if (16U == 16U)
        goto LAB131;

LAB132:    t20 = 0;

LAB133:    if (t20 == 0)
        goto LAB129;

LAB130:    goto LAB2;

LAB126:    goto LAB125;

LAB128:    goto LAB126;

LAB129:    t15 = (t0 + 4055);
    xsi_report(t15, 19U, 2);
    goto LAB130;

LAB131:    t11 = 0;

LAB134:    if (t11 < 16U)
        goto LAB135;
    else
        goto LAB133;

LAB135:    t8 = (t4 + t11);
    t14 = (t2 + t11);
    if (*((unsigned char *)t8) != *((unsigned char *)t14))
        goto LAB132;

LAB136:    t11 = (t11 + 1);
    goto LAB134;

}


extern void work_a_0413944129_2372691052_init()
{
	static char *pe[] = {(void *)work_a_0413944129_2372691052_p_0};
	xsi_register_didat("work_a_0413944129_2372691052", "isim/mux8_16bit_tb_isim_beh.exe.sim/work/a_0413944129_2372691052.didat");
	xsi_register_executes(pe);
}
