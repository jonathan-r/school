#ifndef IO_H_H_H //just to be safe
#define IO_H_H_H

#include <stdint.h>
#include <cstdlib>

class IO{
public:
	//excludes EOF from buffer
	static uint8_t* readFile(const char* file_name, long* size_out){
		FILE* f = fopen(file_name, "rb");
		uint8_t* buffer = NULL;
		if (size_out) *size_out = 0;

		if (f){
			fseek(f, 0, SEEK_SET);
			long start = ftell(f);

			fseek(f, 0, SEEK_END);
			long size = ftell(f) - start;
			
			printf("opened file %s with size %ld\n", file_name, size);
	
			fseek(f, 0, SEEK_SET);

			buffer = (uint8_t*)malloc(size + 1);
			if (fread(buffer, 1, size, f) != size){
				free(buffer); //all or nothing
				buffer = NULL;
				size = 0;
			}
			
			else{
				buffer[size] = 0; //null terminator for strings
			}
			
			
			if (size_out) *size_out = size;
			fclose(f);
		}
		
		return buffer;
	}
};


#endif //IO_H_H_H
