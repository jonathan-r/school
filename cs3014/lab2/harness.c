/* Test and timing harness program for developing a dense matrix
   multiplication routine for the CS3014 module */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>
#include <mpi.h>

#include "internal.h"

/* the following two definitions of DEBUGGING control whether or not
   debugging information is written out. To put the program into
   debugging mode, uncomment the following line: */
/*#define DEBUGGING(_x) _x */
/* to stop the printing of debugging information, use the following line: */

long long time_diff(struct timeval * start, struct timeval * end) {
  return (end->tv_sec - start->tv_sec) * 1000000L + (end->tv_usec - start->tv_usec);
}

int main(int argc, char ** argv) {
  int own_rank, num_agents;
  struct matrix_size input_size[2];

  if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
    fprintf(stderr, "could not initialize mpi\n");
    exit(-1);
  }

  MPI_Comm_rank(MPI_COMM_WORLD, &own_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_agents);

  if (own_rank == MASTER_RANK) {
    /* master process */
    struct complex ** A, ** B, ** C;
    struct complex ** control_matrix;
    long long control_time, mul_time;
    double speedup;
    struct timeval pre_time, start_time, stop_time;

    if (argc != 5) {
      fprintf(stderr, 
        "Usage: matmul-harness <A nrows> <A ncols> <B nrows> <B ncols>\n");
      //kill everything
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
    else {
      input_size[0].rows = atoi(argv[1]);
      input_size[0].cols = atoi(argv[2]);
      input_size[1].rows = atoi(argv[3]);
      input_size[1].cols = atoi(argv[4]);
    }

    if (input_size[0].rows < 1 || input_size[0].cols < 1 || input_size[1].rows < 1 || 
      input_size[1].cols < 1) {
      fprintf(stderr,
        "FATAL one of the dimensions < 1\nbad user\ndon't do that\n");
        //kill everything
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
  
    /* check the matrix sizes are compatible */
    if (input_size[0].cols != input_size[1].rows) {
      fprintf(stderr,
        "FATAL number of columns of A (%d) does not match number of rows of B (%d)\n",
        input_size[0].cols, input_size[1].rows);
      //kill everything
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    /* allocate the matrices */
    A = gen_random_matrix(input_size[0].rows, input_size[0].cols);
    B = gen_random_matrix(input_size[1].rows, input_size[1].cols);
    C = new_empty_matrix(input_size[0].rows, input_size[1].cols);
    control_matrix = new_empty_matrix(input_size[0].rows, input_size[1].cols);
  
    DEBUGGING( {
      printf("matrix A:\n");
      write_out(A, input_size[0].rows, input_size[0].cols);
      printf("\nmatrix B:\n");
      write_out(B, input_size[1].rows, input_size[1].cols);
      printf("\n");
    } )

    /* record control start time */
    gettimeofday(&pre_time, NULL);
  
    /* use a simple matmul routine to produce control result */
    matmul(A, B, control_matrix, input_size[0].rows, input_size[0].cols, input_size[1].cols);
  
    /* record starting time */
    gettimeofday(&start_time, NULL);
    DEBUGGING(printf("done control matmul\n"));

    /* send matrix size to every worker process */
    MPI_Bcast(input_size, sizeof(struct matrix_size) * 2, MPI_BYTE, MASTER_RANK, MPI_COMM_WORLD);
    DEBUGGING(fprintf(stderr, "master: broadcasted input sizes\n"));

    /* wait for everyone */
    MPI_Barrier(MPI_COMM_WORLD);

    /* send the B matrix to every worker */
    MPI_Bcast(B[0], sizeof(struct complex) * input_size[1].rows * input_size[1].cols, MPI_BYTE, 
      MASTER_RANK, MPI_COMM_WORLD);
    DEBUGGING(fprintf(stderr, "master: broadcasted B matrix\n"));

    /* wait for everyone */
    MPI_Barrier(MPI_COMM_WORLD);

    /* distribute the work evenly */
    int slice_rows = input_size[0].rows / num_agents;
    int runoff_rows = input_size[0].rows % num_agents;

    /* send the number of rows to everyone */
    MPI_Bcast(&slice_rows, 1, MPI_INT, MASTER_RANK, MPI_COMM_WORLD);
    DEBUGGING(fprintf(stderr, "master: broadcasted slice size\n"));

    if (slice_rows > 0) {
      //scatter slice_rows sized slices of the A matrix to all process (first slice_rows stays in place)
      MPI_Scatter(A[0], sizeof(struct complex) * slice_rows * input_size[0].cols, MPI_BYTE, 
        MPI_IN_PLACE, 0, MPI_BYTE, MASTER_RANK, MPI_COMM_WORLD);
      DEBUGGING(fprintf(stderr, "master: scattered work\n"));

      //work on our slice_rows
      matmul(A, B, C, slice_rows, input_size[0].cols, input_size[1].cols);

      if (runoff_rows > 0) {
        matmul(&A[input_size[0].rows - runoff_rows], B, &C[input_size[0].rows - runoff_rows], 
          runoff_rows, input_size[0].cols, input_size[1].cols);
      }

      //gather the slices of the result matrix
      MPI_Gather(MPI_IN_PLACE, 0, MPI_BYTE, C[0], sizeof(struct complex) * slice_rows * 
        input_size[1].cols, MPI_BYTE, MASTER_RANK, MPI_COMM_WORLD);
      DEBUGGING(fprintf(stderr, "master: gathered work\n"));
    }

    else {
      //just do it all locally
      matmul(A, B, C, input_size[0].rows, input_size[0].cols, input_size[1].cols);
    }

    /* record finishing time */
    gettimeofday(&stop_time, NULL);
  
    /* compute elapsed times and speedup factor */
    control_time = time_diff(&pre_time, &start_time);
    mul_time = time_diff(&start_time, &stop_time);
    speedup = (float) control_time / mul_time;
  
    printf("Matmul time: %lld microseconds\n", mul_time);
    printf("control time : %lld microseconds\n", control_time);
    if (mul_time > 0 && control_time > 0) {
      printf("speedup: %.2fx\n", speedup);
    }
  
    /* now check that the team's matmul routine gives the same answer
       as the known working version */
    check_result(C, control_matrix, input_size[0].rows, input_size[1].cols);
    DEBUGGING(fprintf(stderr, "master: compared matrices\n"));

    DEBUGGING( {
      printf("result:\n");
      write_out(C, input_size[0].rows, input_size[1].cols);
      printf("\nreference:\n");
      write_out(control_matrix, input_size[0].rows, input_size[1].cols);
      printf("\n");
    } )

    /* free all matrices */
    free_matrix(A);
    free_matrix(B);
    free_matrix(C);
    free_matrix(control_matrix);
  }

  else {
    /* worker process */
    #define WORKER_DEBUG(msg, ...) DEBUGGING(fprintf(stderr, "worker %d: " msg, own_rank, \
      ##__VA_ARGS__)) //gcc extension

    struct complex ** A_rows, ** B, ** C_partial;
    int rows_work;
    MPI_Status status;

    A_rows = NULL;
    C_partial = NULL;

    MPI_Bcast(input_size, sizeof(struct matrix_size) * 2, MPI_BYTE, MASTER_RANK, MPI_COMM_WORLD);
    WORKER_DEBUG("input size broadcast\n");

    //allocate storage in anticipation of the B matrix
    B = new_empty_matrix(input_size[1].rows, input_size[1].cols);

    //synchronize with all clients
    MPI_Barrier(MPI_COMM_WORLD);
    WORKER_DEBUG("barrier\n");

    //receive B matrix data
    MPI_Bcast(B[0], sizeof(struct complex) * input_size[1].rows * input_size[1].cols, MPI_BYTE, 
      MASTER_RANK, MPI_COMM_WORLD);
    WORKER_DEBUG("B matrix broadcast\n");

    //receive work size
    MPI_Barrier(MPI_COMM_WORLD);
    WORKER_DEBUG("barrier\n");

    MPI_Bcast(&rows_work, 1, MPI_INT, MASTER_RANK, MPI_COMM_WORLD);
    WORKER_DEBUG("will receive %d rows of the A matrix\n", rows_work);

    if (rows_work > 0) {
      //allocate storage for A rows and partial C
      A_rows = new_empty_matrix(rows_work, input_size[0].cols);
      C_partial = new_empty_matrix(rows_work, input_size[1].cols);

      //receive A_rows data
      MPI_Scatter(NULL, 0, MPI_BYTE, A_rows[0], sizeof(struct complex) * rows_work * 
        input_size[0].cols, MPI_BYTE, MASTER_RANK, MPI_COMM_WORLD);
      WORKER_DEBUG("received the rows in question\n");

      //do the work
      matmul(A_rows, B, C_partial, rows_work, input_size[0].cols, input_size[1].cols);

      //send back the result
      MPI_Gather(C_partial[0], sizeof(struct complex) * rows_work * input_size[1].cols, MPI_BYTE, 
        NULL, 0, MPI_BYTE, MASTER_RANK, MPI_COMM_WORLD); 
      WORKER_DEBUG("sent work, terminating\n");

      free_matrix(A_rows);
      free_matrix(C_partial);
    }
    else {
      WORKER_DEBUG("no work to do, terminating\n");
    }

    free_matrix(B);
  }

  MPI_Finalize();
  return 0;
}