#define PROCS 2
#define ITERATIONS 5

bool choosing[PROCS];
byte number[PROCS];
bool critical[PROCS];
int timesCritical[PROCS];

active [PROCS] proctype bakery() {
	int go;
	timesCritical[_pid] = 0;
	for (go : 1 .. ITERATIONS) {
		byte max = 0;
		byte i, j;
		
		choosing[_pid] = 1;
		for (i : 0 .. PROCS-1) {
			if 
			::(number[i] > max) -> max = number[i];
			::else -> skip;
			fi;
		}
		number[_pid] = max + 1; //least priority
		choosing[_pid] = 0;
	
		for (j : 0 .. PROCS-1) {
			(!choosing[j]);
			((number[j] == 0) || ((number[j] > number[_pid]) || 
					((number[j] == number[_pid]) && (j >= _pid))));
		}
		
		//critical section
		atomic {
			critical[_pid] = 1;
			timesCritical[_pid]++;
	
			//safety check - the other thread is not in the critical section
			for (j : 0 .. PROCS-1) {
				assert ((j == _pid) || (critical[j] == 0));
			}
		}
	
		//exit critical section
		critical[_pid] = 0;
		number[_pid] = 0;
	}
}

ltl noStarvation { always eventually ((timesCritical[0] == ITERATIONS) && 
									(timesCritical[1] == ITERATIONS)) };
