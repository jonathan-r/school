library ieee;
use ieee.std_logic_1164.all;

entity mux4_16bit is
    port ( in0 : in  std_logic_vector (15 downto 0);
           in1 : in  std_logic_vector (15 downto 0);
           in2 : in  std_logic_vector (15 downto 0);
           in3 : in  std_logic_vector (15 downto 0);
           addr : in  std_logic_vector (1 downto 0);
           output : out  std_logic_vector (15 downto 0));
end mux4_16bit;

architecture behavioral of mux4_16bit is

begin
	process(in0, in1, in2, in3, addr) begin
		case addr is
			when "00" => output <= in0 after 1ns;
			when "01" => output <= in1 after 1ns;
			when "10" => output <= in2 after 1ns;
			when "11" => output <= in3 after 1ns;
			when others => output <= "UUUUUUUUUUUUUUUU";
		end case;
	end process;
end behavioral;

