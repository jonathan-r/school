def split(s):
	if s <= 1:
		return [[s]]

	o = [[s]]
	for i in range(1, s):
		for q in split(i):
			o.append([s-i] + q)
	
	return o

print split(4)

# 4
# 3 1
# 2 2
# - 1 1
# 1 3
# - 2 1
# - 1 2
# - - 1 1
