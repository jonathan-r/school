#pragma once

#include <stdint.h>
#include <assert.h>

#include <atomic>

#include "helper.h"

class BakeryLock {
	public:
		BakeryLock(int numThreads, int lineSize) {
			threads = numThreads;
			lineSz = lineSize;

			assert(sizeof(std::atomic_int) == sizeof(int)); //sanity check

			choosing = (std::atomic_int*) _aligned_malloc((threads+1) * sizeof(std::atomic_int), lineSz);
			number = (std::atomic_int*) _aligned_malloc((threads+1) * sizeof(std::atomic_int), lineSz);
			reset();
		}

		~BakeryLock() {
			_aligned_free((void*)choosing);
			_aligned_free((void*)number);
		}

		//only call when lock is free
		void reset() {
			for (int i = 0; i < threads; i++) {
				choosing[i] = 0;
				number[i] = 0;
			}
		}

		inline void acquire(const int id) {
			choosing[id] = 1; //fenced
			
			//find max of this color
			uint32_t max = 0;
			for (int j = 0; j < threads; j++) {
				if ((number[j] > max)) max = number[j];
			}

			number[id] = max+1; //fenced
			choosing[id] = 0; //fenced

			for (int j = 0; j < threads; j++) {
				while (choosing[j]) _mm_pause();
				while (!( (number[j] == 0) or ((number[j] > number[id]) or ((number[j] == number[id]) and (j >= id))) )) _mm_pause();
			}
		}

		inline void release(const int id) {
			number[id] = 0; //fenced
		}
	
	private:
		int threads, lineSz;
		volatile std::atomic_int* choosing;
		volatile std::atomic_int* number;
};

class TestAndSetLock {
	public:
		TestAndSetLock() {
			lock.store(0);
		}

		~TestAndSetLock() {
		}

		inline void acquire(const int id) {
			while(lock.exchange(1)); //xhcg
		}

		inline void release(const int id) {
			lock.store(0); //causes mfence
		}
	
	private:
		volatile std::atomic_int lock;
};

class TestAndTestAndSetLock {
	public:
		TestAndTestAndSetLock() {
			lock.store(0);
		}

		~TestAndTestAndSetLock() {
		}

		inline void acquire(const int id) {
			do {
				while (lock.load());
			} while (lock.exchange(1)); //xchg
		}

		inline void release(const int id) {
			lock.store(0); //causes mfence
		}
	
	private:
		volatile std::atomic_int lock;
};

class MCSLock {
	public:
		struct node {
			node* volatile next;
			volatile int val;
		};

		MCSLock(int threads, int lineSz) {
			nodeStride = lineSz;
			while (nodeStride < sizeof(node)) nodeStride += lineSz;
			threadLock = (volatile node*) _aligned_malloc(threads * nodeStride, lineSz);
			last = nullptr;

			for (int i = 0; i < threads; i++) {
				at(i).next = nullptr;
				at(i).val = 0;
			}
		}

		~MCSLock() {
			_aligned_free((void*)threadLock);
		}

		inline void acquire(const int id) {
			at(id).next = nullptr;

			node* pred = last.exchange((node*)&at(id));
			if (pred) {
				at(id).val = 1;
				pred->next = (node*)&at(id);
				while (at(id).val) _mm_pause();;
			}
		}

		inline void release(const int id) {
			node* a = (node*)&at(id);
			if (!at(id).next) {
				if (last.compare_exchange_weak(a, nullptr)) {
					return;
				}
				else {
					while(!at(id).next) _mm_pause();;
				}
			}

			at(id).next->val = 0;
		}

		inline volatile node& at(const int i) {
			return *((volatile node*)((volatile uint8_t*)threadLock + (nodeStride * i)));
		}

	private:
		std::atomic<node*> last;
		volatile node* threadLock;
		int nodeStride; //multiple of cache line
};

