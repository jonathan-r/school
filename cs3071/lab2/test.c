#include <stdio.h>
#include <stdint.h>

#include "literal_lexer.h"

int main(int argc, char** argv){
	if (argc == 2){
		uint32_t val;
		int error_position;
		const char* error_str;
		int type = parse(argv[1], &val, &error_str, &error_position);

		switch(type){
			case NUMERAL_DECIMAL:
			printf("decimal %d\n", (int32_t)val);
			break;

			case NUMERAL_OCTAL:
			printf("octal %oB; value in decimal: %u\n", val, val);
			break;

			case NUMERAL_HEXADECIMAL:
			printf("hexadecimal %xH; value in decimal: %u\n", val, val);
			break;

			case NUMERAL_ERROR:
			//print the input in order to show where the error occured
			printf("%s\n%*s^\nerror: %s\n", argv[1], error_position, "", error_str);
			break;
		}
	}

	return 0;
}
