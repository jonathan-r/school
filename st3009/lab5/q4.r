library(boot);

data <- read.csv(file = "lab4times.csv", header = F)[,];
N <- length(data)
data_mean<- mean(data)
data_sd <- sd(data)
low  <- data_mean - 1.96 * data_sd / sqrt(N)
high <- data_mean + 1.96 * data_sd / sqrt(N)

meanboot <- function(x, indices) {
  median(x[indices]);
}

bootres <- boot(data = data, statistic = meanboot, R = 200)
boot.ci(bootres, type = "perc")
