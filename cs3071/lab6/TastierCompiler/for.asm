.names 3
.proc Main
.proc SumUp
.var 1 i
.proc FillDataMem
FillDataMem: Enter 0
Leave
Ret
SumUp: Enter 1
Const 0
Sto 0 0
Jmp L$0
L$1: Nop
LoadG 3
Const 1
Sub
StoG 3
L$0: Nop
LoadG 3
Const 0
GtrEq
FJmp L$2
Load 0 0
LoadG 3
Add
Sto 0 0
Jmp L$1
L$2: Nop
Load 0 0
Write
Leave
Ret
Main: Enter 0
Read
StoG 3
L$3: Nop
LoadG 3
Const 0
Gtr
FJmp L$4
Call 1 SumUp
Read
StoG 3
Jmp L$3
L$4: Nop
Leave
Ret
