#pragma once

#include "vec.h"

template <typename S, typename T>
S lerp(const S& a, const S& b, T t) {
	return a * (T(1) - t) + b * t;
}

template <typename S>
S fade(S t) {
	return t*t*t*(t*(t*6-15)+10);
}

template <typename S, typename T>
S bicub(const S& a, const S& b, const S& c, const S& d, T t) {
	T rt = T(1) - t;
	return a * (rt*rt*rt) + b * (3 * t * rt*rt) + c * (3 * rt * t*t) + d * (t*t*t);
}

template <typename S>
vec3T<S> bicubDerivVec3(const S& a, const S& b, const S& c, const S& d, S t) {
	S rt = S(1) - t;
	return (a * (-3 * rt * rt)) + (b * (3 * rt * rt - 6 * t * rt)) + (c * (6 * t * rt - 3 * t * t)) + (d * (3 * t * t));
}

template <typename S>
void bicubHandlesVec3(const vec3T<S>& p0, const vec3T<S>& p1, const vec3T<S>& p2, vec3T<S>& leading, vec3T<S>& outgoing) {
	vec3T<S> d01 = (p1 - p0);
	vec3T<S> d12 = (p2 - p1);
	
	vec3T<S> m01 = (p0 + p1) / 2;
	vec3T<S> m12 = (p1 + p2) / 2;
	
	S md01 = d01.length();
	S md12 = d12.length();
	
	S d1 = md01 / (md01 + md12);
	S d2 = md12 / (md01 + md12);
	
	vec3T<S> mid = m01 * d1 + m12 * d2;
	leading = p1 + (m01 - mid);
	outgoing = p1 + (m12 - mid);
}

template <typename S>
S bicubLengthVec3(const vec3T<S>& a, const vec3T<S>& b, const vec3T<S>& c, const vec3T<S>& d) {
	/*
	with maxima:
		declare([ax, bx, cx, dx, ay, by, cy, dy], constant);
		px: ax (1-t)^3 + bx (3*t (1-t)^2) + cx (3*(1-t) t^2) + dx(t^3);
		py: ay (1-t)^3 + by (3*t (1-t)^2) + cy (3*(1-t) t^2) + dy(t^3);
		pz: az (1-t)^3 + bz (3*t (1-t)^2) + cz (3*(1-t) t^2) + dz(t^3);

		dpx: diff(px, t);
		dpy: diff(py, t);
		dpz: diff(pz, t);

		len: sqrt(dpx^2 + dpy^2 + dpz^2);

		tlen: taylor(len, t, 0.5, 2);

		integrate(tlen, t, 0, 1);

	yields

		3 * sqrt(
		 ax^2 + ay^2 + az^2 +
		 (-2*ax-2*bx+2*cx+dx)*dx + (-2*ay-2*by+2*cy+dy)*dy + (-2*az-2*bz+2*cz+dz)*dz +
		 (-2*az-2*bz+cz)*cz + (-2*ay-2*by+cy)*cy + (-2*ax-2*bx+cx)*cx +
		 (2*az+bz)*bz + (2*ay+by)*by + (2*ax+bx)*bx
		) / 4
	*/

	vec3T<S> x = d.pairMul(a*(-2) - (b*2) + (c*2) + d);
	x += c.pairMul(a*(-2) - (b*2) + c);
	x += b.pairMul((a*2) + b);
	x += a.pairMul(a);
	return 3 * sqrt(x.x + x.y + x.z) / 4;
}

//assume d1, d2 are unit length
template <typename T>
bool intersectVec2(const vec2T<T>& p1, const vec2T<T>& d1, const vec2T<T>& p2, const vec2T<T>& d2, vec2T<T>& x) {
	//find intersections between any 2 non-parallel lines:
	//for every two rays (p + d), where p and d are 2d vectors and d is unit length
	//find some t1 and t2 such that p1 + d1*t1 = p2 + d2*t2
	//t2x = (p1x - p2x + t1*d1x) / d2x
	//t2y = (p1y - p2y + t1*d1y) / d2y
	//hence t1 = (d2x * (p1y - p2y) - d2y * (p1x - p2x)) / (dx1 * d2y - d1y * d2x)
	//and t2 = (p1 - p2 + t1*d1) / d2 (either dimension)
	
	T divisor = (d1.x * d2.y - d1.y * d2.x);
	if (fabs(divisor) < 0.001) return false; //lines parallel
	
	vec2T<T> dp = p1 - p2;
	T t1 = (d2.x * dp.y - d2.y * dp.x) / divisor;

	if (t1 < 0.0f) return false; //intersection behind start of ray

	x = p1 + d1 * t1;
	return true;
}

template <typename T>
T lineSignedDistVec2(const vec2T<T>& p0, const vec2T<T>& pp1, const vec2T<T>& q) {
	vec2T<T> p1 = p0 + (pp1 - p0).normal();
	T a = p0.y - p1.y;
	T b = p1.x - p0.x;
	T c = p0.x * p1.y - p1.x * p0.y;
	return a * q.x + b * q.y + c;
}

//expect d1 to be unit length
template <typename T>
vec2T<T> projectLineVec2(const vec2T<T>& p0, const vec2T<T>& d1, const vec2T<T>& q) {
	T t1 = (q - p0) * d1;
	return p0 + t1 * d1;
}