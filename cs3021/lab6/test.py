from perfect_lru import PerfectLruCache
from tree_plru import TreePlruCache
from table_lru import TableLruCache
from bit_plru import BitPlruCache

if __name__ == "__main__":
	cacheTypes = (PerfectLruCache, TableLruCache, TreePlruCache, BitPlruCache)

	configs = (((16, 8, 1, 16), "directly mapped"),
	           ((16, 4, 2, 16), "2 way set associative"),
	           ((16, 2, 4, 16), "4 way set associative"),
	           ((16, 1, 8, 16), "fully associative"))
	
	addrs = (0x0000, 0x0004, 0x000c, 0x2200, 0x00d0, 0x00e0, 0x1130, 0x0028, 
	         0x113c, 0x2204, 0x0010, 0x0020, 0x0004, 0x0040, 0x2208, 0x0008, 
	         0x00a0, 0x0004, 0x1104, 0x0028, 0x000c, 0x0084, 0x000c, 0x3390, 
	         0x00b0, 0x1100, 0x0028, 0x0064, 0x0070, 0x00d0, 0x0008, 0x3394)

	for c_type in cacheTypes:
		print(c_type.__doc__)
		for (args, desc) in configs:
			c = c_type(*args) #apply args to constructor
			print(desc)
			for a in addrs:
				msg = c.access(a)[1]
				#print(msg) #uncoment this to see the outcome of the access
				#print(c) #uncomment this to see the state of the whole cache

			misses = c.compulsory_misses + c.conflict_misses + c.capacity_misses
			print("%d hits and %d missed, %.2f hit rate," %
				(c.hits, misses, float(c.hits) / len(addrs)))
			print("%d compulsory + %d conflict + %d capacity\n" % 
				(c.compulsory_misses, c.conflict_misses, c.capacity_misses))

		print()
