##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=lab5
ConfigurationName      :=Debug
WorkspacePath          := "/home/ferry/workspace"
ProjectPath            := "/home/ferry/workspace/school/cs4052/lab5"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=ferry
Date                   :=15/12/15
CodeLitePath           :="/home/ferry/.codelite"
LinkerName             :=/usr/bin/clang++
SharedObjectLinkerName :=/usr/bin/clang++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(ProjectPath)/lab5
Preprocessors          :=$(PreprocessorSwitch)GLSL120 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="lab5.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)/usr/include/GL $(IncludeSwitch)/usr/include/libdrm $(IncludeSwitch)/usr/include/assimp $(IncludeSwitch)/usr/include/SDL2 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)GLEW $(LibrarySwitch)GLU $(LibrarySwitch)GL $(LibrarySwitch)glut $(LibrarySwitch)assimp $(LibrarySwitch)SDL2_image $(LibrarySwitch)SDL2 
ArLibs                 :=  "GLEW" "GLU" "GL" "glut" "assimp" "SDL2_image" "SDL2" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/llvm-ar rcu
CXX      := /usr/bin/clang++
CC       := /usr/bin/clang
CXXFLAGS :=  -g3 -O0 -Wall -std=c++11 $(Preprocessors)
CFLAGS   :=  -g3 -O0 -Wall -std=c11 $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/llvm-as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/noise.cpp$(ObjectSuffix) $(IntermediateDirectory)/terrain.cpp$(ObjectSuffix) $(IntermediateDirectory)/asset.cpp$(ObjectSuffix) $(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/grabber.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/noise.cpp$(ObjectSuffix): noise.cpp $(IntermediateDirectory)/noise.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/ferry/workspace/school/cs4052/lab5/noise.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/noise.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/noise.cpp$(DependSuffix): noise.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/noise.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/noise.cpp$(DependSuffix) -MM "noise.cpp"

$(IntermediateDirectory)/noise.cpp$(PreprocessSuffix): noise.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/noise.cpp$(PreprocessSuffix) "noise.cpp"

$(IntermediateDirectory)/terrain.cpp$(ObjectSuffix): terrain.cpp $(IntermediateDirectory)/terrain.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/ferry/workspace/school/cs4052/lab5/terrain.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/terrain.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/terrain.cpp$(DependSuffix): terrain.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/terrain.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/terrain.cpp$(DependSuffix) -MM "terrain.cpp"

$(IntermediateDirectory)/terrain.cpp$(PreprocessSuffix): terrain.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/terrain.cpp$(PreprocessSuffix) "terrain.cpp"

$(IntermediateDirectory)/asset.cpp$(ObjectSuffix): asset.cpp $(IntermediateDirectory)/asset.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/ferry/workspace/school/cs4052/lab5/asset.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/asset.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/asset.cpp$(DependSuffix): asset.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/asset.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/asset.cpp$(DependSuffix) -MM "asset.cpp"

$(IntermediateDirectory)/asset.cpp$(PreprocessSuffix): asset.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/asset.cpp$(PreprocessSuffix) "asset.cpp"

$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/ferry/workspace/school/cs4052/lab5/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/grabber.cpp$(ObjectSuffix): grabber.cpp $(IntermediateDirectory)/grabber.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/ferry/workspace/school/cs4052/lab5/grabber.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/grabber.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/grabber.cpp$(DependSuffix): grabber.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/grabber.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/grabber.cpp$(DependSuffix) -MM "grabber.cpp"

$(IntermediateDirectory)/grabber.cpp$(PreprocessSuffix): grabber.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/grabber.cpp$(PreprocessSuffix) "grabber.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


