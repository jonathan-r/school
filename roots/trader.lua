require("util")
require("conf")

Trader = {}
function Trader:new(x, y)
	local ob = {
		x=x, y=y, --current
		wealth=100, units=0, state="idle", --current
		buy = nil, sell = nil, res=0, count=0, --plan
		col={
			r=love.math.randomNormal(50, 200),
			g=love.math.randomNormal(50, 200),
			b=love.math.randomNormal(50, 200)
		},
		text=love.graphics.newText(love.graphics.getFont(), "")
	}
	
	setmetatable(ob, self)
	self.__index = self
	return ob
end

function Trader:update(dt, markets)
	if self.state == "idle" then
		--new route
		local highest = 0
		for ri = 1, NUM_RES do
			for i = 1, #markets do
				local p1 = markets[i]:getPrice(ri)
				local v1 = markets[i]:getAmount(ri)
				local w1 = markets[i]:getWealth(ri)
				local sellerDist = distOb(self, markets[i])
				
				for j = 1, #markets do
					if i ~= j then
						local interDist = distOb(markets[i], markets[j])
						
						local p2 = markets[j]:getPrice(ri)
						local v2 = markets[i]:getAmount(ri)
						local w2 = markets[i]:getWealth(ri)
						
						local wealthAtSeller = self.wealth - sellerDist * TR_TRAVEL_COST
						
						if wealthAtSeller > 0 then
							local maxPurchase = math.min(wealthAtSeller / p1, w2 / p2)
							local maxUnits = math.min(maxPurchase, math.min(v1, RES_LIMIT - v2))
							
							local gain = (p2 - p1) * maxUnits
							local loss = (sellerDist + interDist) * TR_TRAVEL_COST
							
							local profit = gain - loss
							
							if profit > highest then
								self.seller = markets[i]
								self.buyer = markets[j]
								self.res = ri
								self.count = maxUnits
								highest = profit
								
								self.state = "buying"
							end
						end
					end
				end
			end
		end
		
	elseif self.state == "selling" then
		assert(self.buyer ~= nil)
		--go sell
		local dist = distOb(self, self.buyer)
		
		if dist < PS then
			--there already, sell
			local u, v = self.buyer:buy(self.res, self.units)
			self.wealth = self.wealth + v
			--self.units = 0 --!!! throw out remaining units
			local diff = self.units - u
			self.units = self.units - u
			
			self.seller = nil
			self.buyer = nil
			self.state = "idle"
			
		else
			--move towards buyer
			local dx, dy = dirOb(self, self.buyer)
			self.x = self.x + dx * TR_VEL * dt
			self.y = self.y + dy * TR_VEL * dt
			self.wealth = self.wealth - TR_TRAVEL_COST * TR_VEL * dt
		end
	
	else
		--go buy
		local dist = distOb(self, self.seller)
		
		if dist < PS then
			--there already, buy
			local u, v = self.seller:sell(self.res, self.count, self.wealth)
			
			self.units = u
			self.wealth = self.wealth - v
			
			self.seller = nil
			self.state = "selling"
			
		else
			--move towards seller
			local dx, dy = dirOb(self, self.seller)
			self.x = self.x + dx * TR_VEL * dt
			self.y = self.y + dy * TR_VEL * dt
			self.wealth = self.wealth - TR_TRAVEL_COST * TR_VEL * dt
		end
	end
	
	--update text
	self.text:set(string.format("u(%d):%.1f\nw:%.1f", self.res, self.units, self.wealth))
end

function Trader:draw()
	local rad
	if self.state == "selling" then
		--draw units
		rad = circleRad(self.units)
	else
		rad = 15
	end
	
	love.graphics.setLineWidth(5.0)
	love.graphics.setColor(self.col.r, self.col.g, self.col.b, 255)
	love.graphics.circle("line", self.x, self.y, rad+2.5, 100)
	love.graphics.setLineWidth(1.0)
		
	if self.state == "selling" then
		local uc = getResCol(self.res)
		love.graphics.setColor(uc.r, uc.g, uc.b, 255)
		love.graphics.circle("fill", self.x, self.y, rad, 8)
	end
	
	love.graphics.setColor(255, 255, 255, 255)
	local x = self.x-self.text:getWidth()/2
	local y = self.y-self.text:getHeight()*3/2
	love.graphics.draw(self.text, x, y)
end
