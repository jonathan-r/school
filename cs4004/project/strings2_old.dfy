method isPrefix(pre: string, str: string) returns (res: bool)
	requires |pre| > 0
	requires |pre| <= |str|
	ensures res ==> pre == str[0..|pre|];
{
	res := (pre == str[0..|pre|]);
}

function substr(sub: string, str: string): bool
	requires |sub| >= 1
	requires |sub| <= |str|
	decreases |str|
{
	if sub == str[0 .. |sub|] then true 
		 else if |sub| >= |str| then false
			  else substr(sub, str[1..])
}

//returns negative value in case of failure
method isSubstring(sub: string, str: string) returns (res: bool)
	requires |sub| >= 1
	requires |sub| <= |str|
	//ensures res ==> substr(sub, str);
{
	res := false;

	var i := 0;
	var maxAllowedLength := |str|-|sub|;
	while i <= maxAllowedLength
		invariant 0 <= i <= maxAllowedLength;
		invariant |str[i..]| >= |sub|;
	{
		var pre := isPrefix(sub, str[i..]);
		if pre {
			assert sub == str[i..(i+|sub|)];
			res := true;
			return res;
		}
		if i == maxAllowedLength { break; }
		i := i + 1;
	}
}



method haveCommonKSubstring(k: nat, str1: string, str2: string) returns (found: bool)
	requires |str1| >= k;
	requires |str2| >= k;
	//ensures found ==> commonksubstr(k, str1, str2);
{
	found := false;
	if (k == 0) { 
		found := true; 
	}
	else {
		var i := 0;
		while i < (|str1|-k+1)
		{
			var sub := str1[i .. (i+k)];
			var inStr2 := isSubstring(sub, str2);
			if inStr2 {
				found := true;
			}
			i := i + 1;
		}
	}
}

method maxCommonSubstringLength(str1: string, str2: string) returns (len: nat)
	//ensures commonksubstr(len, str1, str1);
{
	len := 0; //all strings (any length) have a at least a 0 length substring in common
	var i := 0;
	var maxLen := 0;
	if |str1| > |str2| {
		maxLen := |str2|;
	}
	else {
		maxLen := |str1|;
	}

	while i < maxLen {
		var common := haveCommonKSubstring(i, str1, str2);
		if common {
			len := i;
		}
		i := i + 1;
	}
}

