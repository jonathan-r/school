\documentclass[a4paper, 11pt]{report}
%\special{papersize=210mm,297mm} %a4 size
\title{Michaelmas 2013\\Programming Techniques assignment \#1\\report}
\author{Jonathan Rosca}
\date{}

%\usepackage[cm]{fullpage}
\usepackage[top=2cm, bottom=2cm, left=1cm, right=1cm]{geometry}
\usepackage{color}
\usepackage{listings}
\usepackage{multicol}

\lstset{ %
language=Java,                % choose the language of the code
basicstyle=\footnotesize,     % the size of the fonts that are used for the code
numbers=none,                   % where to put the line-numbers
numberstyle=\footnotesize,    % the size of the fonts that are used for the line-numbers
stepnumber=1,                   % the step between two line-numbers. If it is 1 each line will be numbered
numbersep=5pt,                  % how far the line-numbers are from the code
backgroundcolor=\color{white}, % choose the background color. You must add \usepackage{color}
showspaces=false,               % show spaces adding particular underscores
showstringspaces=false,         % underline spaces within strings
showtabs=false,                 % show tabs within strings adding particular underscores
frame=single,           	% adds a frame around the code
tabsize=2,          		% sets default tabsize to 2 spaces
captionpos=b,           	% sets the caption-position to bottom
breaklines=true,        	% sets automatic line breaking
breakatwhitespace=false,    	% sets if automatic breaks should only happen at whitespace
escapeinside={\%*}{*)}          % if you want to add a comment within your code
}

%\pagestyle{empty}

\begin{document}
\maketitle

\section{Introduction}
This is a command line Java program that implements Conway's game of life. When the program started, the user can specify the width and height of the board, as well the delay in milliseconds between each update. The board must be at least 5x5 large, as its initial state is set to the recognizable "glider" pattern. The delay must be greater than 0.
Once the program has started, it can be stopped by pressing the enter key. This is due to the fact that the simulation is done in a seperate thread, while the main thread waits on user input. When the user presses the enter key, the main thread quits, which also kills simulation thread it had spawned.

\section{Algorithm}
A simple algorithm is used to update the state of the board for each frame. The board is contiguous at the edges, looping back from the other edge on both axes.

\subsection{State}
Three arrays of bytes containing width * height elements are kept from frame to frame. They hold the following state for each tile:
\begin{enumerate}
\item \textbf{live}: whether tile is alive, stored as 1 or 0.
\item \textbf{neighbours}: the number of living tiles in the 8-tile neighbourhood of this tile.
\item \textbf{lastNeighbours}: like the above, but for the previous frame.
\end{enumerate}

\subsection{Initialization}
Before the start of the simulation, the initial state is filled in. The \textbf{lastNeighbours} array is filled in by looping over every tile and counting the number of living tiles (from the \textbf{live} array) in the 8-tile neighbourhood. This state is copied into the \textbf{neighbours} array.

\subsection{Update}
For each update, the \textbf{live} and \textbf{neighbours} arrays are updated as follows: for each tile, its state and its amount of living neighbours is fetched from the \textbf{alive} and \textbf{lastNeighbours} arrays, respectively. The state of the tiles can change based on these two rules:
\begin{enumerate}
\item If the tile was alive and it had fewer than 2 or more than 3 living neighbours, it is dying. 
\item If the tile was dead and it had 3 living neighbours, it is growing. 
\end{enumerate}
If the state of the tile is changing, then its state is updated in the \textbf{alive} array directly and the 8 items the \textbf{neighbours} array that correspond to a neighbour of this tile are either incremented or decremented, depending on whether the tile is dying or growing, as they are either gaining or losing a neighbour.

\subsection{Visualization}
After each update, the program clears the terminal screen using the ANSI escape sequence "ESC[2J" (where ESC is the escape character, decimal 27), then displays the state of the board by printing it line by line. An 'X' represents a living tile, while a whitespace represents a dead tile.

\subsection{Delay}
Since the update and visualization functions are no challenge to current generation computers, the program waits for a set amount of time to allow the user to behold the state of the board before the next update.

\section{Example run}
\begin{lstlisting}
java GameOfLife
 X        
  X       
XXX 
\end{lstlisting}

A second later:
\begin{lstlisting}
X X       
 XX       
 X 
\end{lstlisting}

Another second later:
\begin{lstlisting}
  X       
X X       
 XX 
\end{lstlisting}
And so on.

\section{Source code}
\begin{lstlisting}
import java.lang.Thread;

public class GameOfLife extends Thread{
	private byte[] live;
	private byte[] neighbours;
	private byte[] lastNeighbours;

	private int w;
	private int h;
	private int delay;
	
	private boolean running;

	public GameOfLife(int width, int height, int delayMilli, byte[] initialState) {
		w = width;
		h = height;
		delay = delayMilli;
		
		live = new byte[w * h];
		lastNeighbours = new byte[w * h];
		System.arraycopy(initialState, 0, live, 0, w * h); //copy the initial state
	
		//initialize neighbour count values
		for (int y = 0; y < h; y++) {
			int up = (y == 0) ? h - 1 : y - 1;
			int down = (y == h - 1) ? 0 : y + 1;

			for (int x = 0; x < w; x++) {
				int left = (x == 0) ? w - 1 : x - 1;
				int right = (x == w - 1) ? 0 : x + 1;

				if (live[x + y * w] == 1) {
					lastNeighbours[left + up * w]++;
					lastNeighbours[left + y * w]++;
					lastNeighbours[left + down * w]++;

					lastNeighbours[x + up * w]++;
					//center
					lastNeighbours[x + down * w]++;

					lastNeighbours[right + up * w]++;
					lastNeighbours[right + y * w]++;
					lastNeighbours[right + down * w]++;
				}
			}
		}
		
		neighbours = new byte[w * h];
		System.arraycopy(lastNeighbours, 0, neighbours, 0, w * h);
	}

	//updates the board one step
	private void update() {
		for (int y = 0; y < h; y++) {
			int up = (y == 0) ? h - 1 : y - 1;
			int down = (y == h - 1) ? 0 : y + 1;

			for (int x = 0; x < w; x++) {
				int left = (x == 0) ? w - 1 : x - 1;
				int right = (x == w - 1) ? 0 : x + 1;

				byte around = lastNeighbours[x + y * w]; //amount of living neighbours around this cell

				int add = 0;
				if (live[x + y * w] == 1 && (around < 2 || around > 3)){
					//wither
					add = -1;
					live[x + y * w] = 0;
				}
				
				else if (live[x + y * w] == 0 && around == 3){
					//grow
					add = 1;
					live[x + y * w] = 1;
				}

				if (add != 0) {
					neighbours[left + up * w] += add;
					neighbours[left + y * w] += add;
					neighbours[left + down * w] += add;

					neighbours[x + up * w] += add;
					//center
					neighbours[x + down * w] += add;

					neighbours[right + up * w] += add;
					neighbours[right + y * w] += add;
					neighbours[right + down * w] += add;
				}
			}
		}
		
		System.arraycopy(neighbours, 0, lastNeighbours, 0, w * h);
	}

	//clears the screen and display the board
	private void print() {
		System.out.println("\033[2J"); //clear
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				System.out.print((live[x + y * w] == 1)? 'X' : ' ');
			}
			
			System.out.println();
		}
		
	}
	
	//entry point of this thread;
	//updates and displays the board again and again, until stopped
	public void run(){
		running = true;
		while (running) {
			print();
			update();
			try {
				Thread.sleep(delay);
			}
			catch (InterruptedException e) {}
		}
	}
	
	//safe way of ending the thread from the main thread
	public synchronized void die(){
		running = false;
	}

	//main thread entry point
	public static void main(String[] args) {
		GameOfLife game;
		
		//default values
		int width = 10;
		int height = 10;
		int delay = 1000;
		
		if (args.length == 1){
			//delay only
			delay = Integer.parseInt(args[0]);
		}

		else if (args.length == 2 || args.length == 3) {
			//width and height and maybe delay
			width = Integer.parseInt(args[0]);
			height = Integer.parseInt(args[1]);
			if (args.length == 3) delay = Integer.parseInt(args[2]);
		}
		
		else if (args.length != 0) {
			//confused
			width = -1;
			System.out.println("usage: java GameOfLife [width <height>] [update delay]");
		}

		if (width > 5 && height > 5 && delay > 0) {
			byte[] initial = new byte[width * height];

			//make a glider
			System.arraycopy(new byte[] { 0, 1, 0 }, 0, initial, width * 0, 3);
			System.arraycopy(new byte[] { 0, 0, 1 }, 0, initial, width * 1, 3);
			System.arraycopy(new byte[] { 1, 1, 1 }, 0, initial, width * 2, 3);

			game = new GameOfLife(width, height, delay, initial);
			game.start();
			
			//pause this thread until a key is pressed
			try{
				System.in.read();
			}
			catch(java.io.IOException e){}
			
			//stop the simulation
			game.die();
		}
	}
}
\end{lstlisting}

\end{document}
