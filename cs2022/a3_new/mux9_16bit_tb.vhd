library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.array_types.all;
 
entity mux9_16bit_tb is
end mux9_16bit_tb;
 
architecture behavior of mux9_16bit_tb is 
	component mux9_16bit port(
		s : in std_logic_vector(3 downto 0);
		in_data : in data_array;
		z : out std_logic_vector(15 downto 0) );
	end component;
    
	signal s_in : std_logic_vector(3 downto 0) := (others => '0');
	signal data_in : data_array := (others => "0000000000000000");
	signal z_out : std_logic_vector(15 downto 0);
	
	constant test_data : std_logic_vector(15 downto 0) := "0101011001011010";
	
	type test_array_type is array (0 to 8) of std_logic_vector (15 downto 0);
	constant test_array : test_array_type := (
		x"0123", x"f12a", x"e12b", x"d12c", x"c12d", x"b12e", x"a12f", x"9120", x"ef20" );

begin
	uut: mux9_16bit port map (s_in, data_in, z_out);

	stim_proc: process
	begin
		wait for 1 ns;
		--same value on all
		for i in 0 to 8 loop
			data_in(i) <= test_data;
			s_in <= std_logic_vector(to_unsigned(i, 4));
			wait for 1 ns;
			assert z_out = test_data;
		end loop;

		--unique value on each one
		for i in 0 to 8 loop
			data_in(i) <= test_array(i);
		end loop;
		
		--first 8 registers
		for i in 0 to 7 loop
			s_in <= std_logic_vector(to_unsigned(i, 4));
			wait for 1 ns;
			assert z_out = test_array(i);
		end loop;
		
		--then the last register: all 8 selection addresses should return it
		for i in 8 to 15 loop
			s_in <= std_logic_vector(to_unsigned(i, 4));
			wait for 1 ns;
			assert z_out = test_array(8);
		end loop;
		
		wait;
	end process;
end;
