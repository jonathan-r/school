module HaskellGame.Graphics where

import Prelude ( Show(..), take, String, Int, (+), (<), not )
import qualified Data.List as List
import Data.List ((++))
import Data.Char (toUpper)

import HaskellGame.Datatypes
import HaskellGame.Utils

{- How to display active game elements -}

instance Show Player where
  show (Player _ _ _ _ _) = "☃"

instance Show Object where
  show (Chest _) = "?"

instance Show Monster where
  show (Dragon _ _ _) = "🐉"
  show (Zombie _ _ _) = "💀"

{- Displaying stats and status -}

{- Lab 3: Implement this Show instance -}
compress :: String -> Int -> String
compress _ 0 = ""
compress from len =
	if ((List.length from) < (len + 1)) then
		take len (from ++ (List.repeat ' '))
	else 
		let
			cons = List.filter (\ letter -> not (List.elem (toUpper letter) "AEIOUY")) from
		in
			take len (cons ++ (List.repeat ' '))


instance Show Stat where
  show (name, value) = (compress name 3) ++ (show value)
