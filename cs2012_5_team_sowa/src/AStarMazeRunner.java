import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import java.io.PrintWriter;

/**
 * This implements an A* Maze Runner for SC mazes
 *
 * @author Jonathan Rosca
 */
public class AStarMazeRunner<SC extends SquareCell> extends MazeRunner<SC>  {
	/**
	 * Tries to find a solution to the given maze.
	 *
	 * @param maze      The maze to solve.
	 * @param writer    The printwriter on which to output the maze solution.
	 */

	private LinkedList<SC> openQueue;
	private	int[] gScore;
	private	int[] fScore;
	private int scoreStride;

	//manhattan distance
	private int heuristic(SC at, SC target){
		return Math.abs(at.getCol() - target.getCol()) + Math.abs(at.getRow() - target.getRow());

		/*float dx = at.getCol() - target.getCol();
		float dy = at.getRow() - target.getRow();

		return (int)Math.round(Math.sqrt(dx * dx + dy * dy));*/
	}

	//insert cell into queue according to 
	private void priorityInsert(SC cell, int cellGScore, int cellFScore){
		ListIterator<SC> it = openQueue.listIterator();

		while(it.hasNext()){
			SC current = it.next();
			int currentScore = fScore[current.getCol() + current.getRow() * scoreStride];
			if (currentScore > cellFScore) break;
		}

		it.add(cell);
		gScore[cell.getCol() + cell.getRow() * scoreStride] = cellGScore;
		fScore[cell.getCol() + cell.getRow() * scoreStride] = cellFScore;
		cell.setState(MazeCell.CellState.visitInProgress); //mark as being in the open set
	}

	public void solveMaze(Maze<SC> maze, PrintWriter writer) {
		//dealing with a*s open and closed set: 
		//if a cell is in the open set, its state is visitInProgress
		//if it is in the closed set, its state is visited
		//otherwise it is in neither set, and its state is notVisited

		openQueue = new LinkedList<SC>();
		gScore = new int[maze.getNumCells()];
		fScore = new int[maze.getNumCells()];
		scoreStride = ((SquareCellMaze)maze).getWidth();

		SC start = maze.getStart();
		SC end = maze.getDonut();

		priorityInsert(start, 0, heuristic(start, end));

		int cellsExpanded = 0;
		boolean solved = false;

		while (openQueue.size() > 0){
			//most promising candidate
			SC cell = openQueue.remove();
			int cellGScore = gScore[cell.getCol() + cell.getRow() * scoreStride];
			int cellFScore = fScore[cell.getCol() + cell.getRow() * scoreStride];

			cellsExpanded++;

			if (cell == end){
				solved = true;
				break;
			}

			cell.setState(MazeCell.CellState.visited); //place in closed set

			Iterator<SC> it = maze.getNeighbors(cell);
			while (it.hasNext()){
				SC n = it.next();
				int nid = n.getCol() + n.getRow() * scoreStride;
				if (n.getState() != MazeCell.CellState.visited){
					int tenativeG = cellGScore + 1;

					if (n.getState() == MazeCell.CellState.notVisited || tenativeG < gScore[nid]){
						gScore[nid] = tenativeG;
						fScore[nid] = tenativeG + heuristic(n, end);
						if (n.getState() == MazeCell.CellState.notVisited) priorityInsert(n, gScore[nid], fScore[nid]);
						n.setExtraInfo(cell);
					}
				}
			}
		}

		writer.println("A*");

		if (solved){
			LinkedList<SC> path = new LinkedList<SC>();

			SC trace = end;
			while (trace != null) {
				trace = (SC)trace.getExtraInfo();
				if (trace != null) path.addFirst(trace);
			} 

			path.addLast(end);

			for (SC cell : path) {
				writer.print(cell + " ");
				cell.setState(MazeCell.CellState.solutionPath);
			}

			writer.println();
			writer.println(path.size());
			writer.println(cellsExpanded);
		}

		else{
			writer.println("no solution");
		}
	}
}
